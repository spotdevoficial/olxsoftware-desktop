package org.jcodec;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

public class TimecodeSampleEntry extends SampleEntry {
    private static final MyFactory FACTORY;
    private int flags;
    private int frameDuration;
    private byte numFrames;
    private int timescale;

    public static class MyFactory extends BoxFactory {
        private Map<String, Class<? extends Box>> mappings;

        public MyFactory() {
            this.mappings = new HashMap();
        }
    }

    static {
        FACTORY = new MyFactory();
    }

    public TimecodeSampleEntry(Header header) {
        super(header);
        this.factory = FACTORY;
    }

    public TimecodeSampleEntry() {
        super(new Header("tmcd"));
        this.factory = FACTORY;
    }

    public TimecodeSampleEntry(int flags, int timescale, int frameDuration, int numFrames) {
        super(new Header("tmcd"));
        this.flags = flags;
        this.timescale = timescale;
        this.frameDuration = frameDuration;
        this.numFrames = (byte) numFrames;
    }

    protected void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putInt(0);
        out.putInt(this.flags);
        out.putInt(this.timescale);
        out.putInt(this.frameDuration);
        out.put(this.numFrames);
        out.put((byte) -49);
    }

    public void dump(StringBuilder sb) {
        sb.append(this.header.getFourcc() + ": {\n");
        sb.append("entry: ");
        ToJSON.toJSON(this, sb, "flags", "timescale", "frameDuration", "numFrames");
        sb.append(",\nexts: [\n");
        dumpBoxes(sb);
        sb.append("\n]\n");
        sb.append("}\n");
    }
}
