package org.jcodec;

import java.nio.ByteBuffer;

public class LeafBox extends Box {
    private ByteBuffer data;

    public LeafBox(Header atom) {
        super(atom);
    }

    public LeafBox(Header atom, ByteBuffer data) {
        super(atom);
        this.data = data;
    }

    protected void doWrite(ByteBuffer out) {
        NIOUtils.write(out, this.data);
    }
}
