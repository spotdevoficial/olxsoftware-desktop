package org.jcodec;

import java.nio.ByteBuffer;
import org.jcodec.RefPicMarking.InstrType;
import org.jcodec.RefPicMarking.Instruction;

public class MappedH264ES {
    private ByteBuffer bb;
    private int frameNo;
    private IntObjectMap<PictureParameterSet> pps;
    private int prevFrameNum;
    private int prevFrameNumOffset;
    private int prevPicOrderCntLsb;
    private int prevPicOrderCntMsb;
    private SliceHeaderReader shr;
    private IntObjectMap<SeqParameterSet> sps;

    public MappedH264ES(ByteBuffer bb) {
        this.pps = new IntObjectMap();
        this.sps = new IntObjectMap();
        this.bb = bb;
        this.shr = new SliceHeaderReader();
        this.frameNo = 0;
    }

    public Packet nextFrame() {
        ByteBuffer result = this.bb.duplicate();
        NALUnit prevNu = null;
        SliceHeader prevSh = null;
        while (true) {
            this.bb.mark();
            ByteBuffer buf = H264Utils.nextNALUnit(this.bb);
            if (buf == null) {
                break;
            }
            NALUnit nu = NALUnit.read(buf);
            if (nu.type == NALUnitType.IDR_SLICE || nu.type == NALUnitType.NON_IDR_SLICE) {
                SliceHeader sh = readSliceHeader(buf, nu);
                if (prevNu != null && prevSh != null && !sameFrame(prevNu, nu, prevSh, sh)) {
                    break;
                }
                prevSh = sh;
                prevNu = nu;
            } else if (nu.type == NALUnitType.PPS) {
                PictureParameterSet read = PictureParameterSet.read(buf);
                this.pps.put(read.pic_parameter_set_id, read);
            } else if (nu.type == NALUnitType.SPS) {
                SeqParameterSet read2 = SeqParameterSet.read(buf);
                this.sps.put(read2.seq_parameter_set_id, read2);
            }
        }
        this.bb.reset();
        result.limit(this.bb.position());
        if (prevSh == null) {
            return null;
        }
        return detectPoc(result, prevNu, prevSh);
    }

    private SliceHeader readSliceHeader(ByteBuffer buf, NALUnit nu) {
        BitReader br = new BitReader(buf);
        SliceHeader sh = this.shr.readPart1(br);
        PictureParameterSet pp = (PictureParameterSet) this.pps.get(sh.pic_parameter_set_id);
        this.shr.readPart2(sh, nu, (SeqParameterSet) this.sps.get(pp.seq_parameter_set_id), pp, br);
        return sh;
    }

    private boolean sameFrame(NALUnit nu1, NALUnit nu2, SliceHeader sh1, SliceHeader sh2) {
        if (sh1.pic_parameter_set_id != sh2.pic_parameter_set_id || sh1.frame_num != sh2.frame_num) {
            return false;
        }
        SeqParameterSet sps = sh1.sps;
        if (sps.pic_order_cnt_type == 0 && sh1.pic_order_cnt_lsb != sh2.pic_order_cnt_lsb) {
            return false;
        }
        if (sps.pic_order_cnt_type == 1 && (sh1.delta_pic_order_cnt[0] != sh2.delta_pic_order_cnt[0] || sh1.delta_pic_order_cnt[1] != sh2.delta_pic_order_cnt[1])) {
            return false;
        }
        if ((nu1.nal_ref_idc == 0 || nu2.nal_ref_idc == 0) && nu1.nal_ref_idc != nu2.nal_ref_idc) {
            return false;
        }
        if (nu1.type == NALUnitType.IDR_SLICE) {
            int i = 1;
        } else {
            boolean z = false;
        }
        if (nu2.type == NALUnitType.IDR_SLICE) {
            int i2 = 1;
        } else {
            boolean z2 = false;
        }
        if (i == i2 && sh1.idr_pic_id == sh2.idr_pic_id) {
            return true;
        }
        return false;
    }

    private Packet detectPoc(ByteBuffer result, NALUnit nu, SliceHeader sh) {
        int maxFrameNum = 1 << (sh.sps.log2_max_frame_num_minus4 + 4);
        if (detectGap(sh, maxFrameNum)) {
            issueNonExistingPic(sh, maxFrameNum);
        }
        int absFrameNum = updateFrameNumber(sh.frame_num, maxFrameNum, detectMMCO5(sh.refPicMarkingNonIDR));
        int poc = 0;
        if (nu.type == NALUnitType.NON_IDR_SLICE) {
            poc = calcPoc(absFrameNum, nu, sh);
        }
        long j = (long) absFrameNum;
        int i = this.frameNo;
        this.frameNo = i + 1;
        return new Packet(result, j, 1, 1, (long) i, nu.type == NALUnitType.IDR_SLICE, null, poc);
    }

    private int updateFrameNumber(int frameNo, int maxFrameNum, boolean mmco5) {
        int frameNumOffset;
        if (this.prevFrameNum > frameNo) {
            frameNumOffset = this.prevFrameNumOffset + maxFrameNum;
        } else {
            frameNumOffset = this.prevFrameNumOffset;
        }
        int absFrameNum = frameNumOffset + frameNo;
        if (mmco5) {
            frameNo = 0;
        }
        this.prevFrameNum = frameNo;
        this.prevFrameNumOffset = frameNumOffset;
        return absFrameNum;
    }

    private void issueNonExistingPic(SliceHeader sh, int maxFrameNum) {
        this.prevFrameNum = (this.prevFrameNum + 1) % maxFrameNum;
    }

    private boolean detectGap(SliceHeader sh, int maxFrameNum) {
        return (sh.frame_num == this.prevFrameNum || sh.frame_num == (this.prevFrameNum + 1) % maxFrameNum) ? false : true;
    }

    private int calcPoc(int absFrameNum, NALUnit nu, SliceHeader sh) {
        if (sh.sps.pic_order_cnt_type == 0) {
            return calcPOC0(nu, sh);
        }
        if (sh.sps.pic_order_cnt_type == 1) {
            return calcPOC1(absFrameNum, nu, sh);
        }
        return calcPOC2(absFrameNum, nu, sh);
    }

    private int calcPOC2(int absFrameNum, NALUnit nu, SliceHeader sh) {
        if (nu.nal_ref_idc == 0) {
            return (absFrameNum * 2) - 1;
        }
        return absFrameNum * 2;
    }

    private int calcPOC1(int absFrameNum, NALUnit nu, SliceHeader sh) {
        int i;
        int expectedPicOrderCnt;
        if (sh.sps.num_ref_frames_in_pic_order_cnt_cycle == 0) {
            absFrameNum = 0;
        }
        if (nu.nal_ref_idc == 0 && absFrameNum > 0) {
            absFrameNum--;
        }
        int expectedDeltaPerPicOrderCntCycle = 0;
        for (i = 0; i < sh.sps.num_ref_frames_in_pic_order_cnt_cycle; i++) {
            expectedDeltaPerPicOrderCntCycle += sh.sps.offsetForRefFrame[i];
        }
        if (absFrameNum > 0) {
            expectedPicOrderCnt = ((absFrameNum - 1) / sh.sps.num_ref_frames_in_pic_order_cnt_cycle) * expectedDeltaPerPicOrderCntCycle;
            for (i = 0; i <= (absFrameNum - 1) % sh.sps.num_ref_frames_in_pic_order_cnt_cycle; i++) {
                expectedPicOrderCnt += sh.sps.offsetForRefFrame[i];
            }
        } else {
            expectedPicOrderCnt = 0;
        }
        if (nu.nal_ref_idc == 0) {
            expectedPicOrderCnt += sh.sps.offset_for_non_ref_pic;
        }
        return sh.delta_pic_order_cnt[0] + expectedPicOrderCnt;
    }

    private int calcPOC0(NALUnit nu, SliceHeader sh) {
        int picOrderCntMsb;
        int pocCntLsb = sh.pic_order_cnt_lsb;
        int maxPicOrderCntLsb = 1 << (sh.sps.log2_max_pic_order_cnt_lsb_minus4 + 4);
        if (pocCntLsb < this.prevPicOrderCntLsb && this.prevPicOrderCntLsb - pocCntLsb >= maxPicOrderCntLsb / 2) {
            picOrderCntMsb = this.prevPicOrderCntMsb + maxPicOrderCntLsb;
        } else if (pocCntLsb <= this.prevPicOrderCntLsb || pocCntLsb - this.prevPicOrderCntLsb <= maxPicOrderCntLsb / 2) {
            picOrderCntMsb = this.prevPicOrderCntMsb;
        } else {
            picOrderCntMsb = this.prevPicOrderCntMsb - maxPicOrderCntLsb;
        }
        if (nu.nal_ref_idc != 0) {
            this.prevPicOrderCntMsb = picOrderCntMsb;
            this.prevPicOrderCntLsb = pocCntLsb;
        }
        return picOrderCntMsb + pocCntLsb;
    }

    private boolean detectMMCO5(RefPicMarking refPicMarkingNonIDR) {
        if (refPicMarkingNonIDR == null) {
            return false;
        }
        for (Instruction instr : refPicMarkingNonIDR.getInstructions()) {
            if (instr.getType() == InstrType.CLEAR) {
                return true;
            }
        }
        return false;
    }

    public SeqParameterSet[] getSps() {
        return (SeqParameterSet[]) this.sps.values(new SeqParameterSet[0]);
    }

    public PictureParameterSet[] getPps() {
        return (PictureParameterSet[]) this.pps.values(new PictureParameterSet[0]);
    }
}
