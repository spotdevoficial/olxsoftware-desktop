package org.jcodec;

import java.nio.ByteBuffer;

public class SampleToChunkBox extends FullBox {
    private SampleToChunkEntry[] sampleToChunk;

    public static class SampleToChunkEntry {
        private int count;
        private int entry;
        private long first;

        public SampleToChunkEntry(long first, int count, int entry) {
            this.first = first;
            this.count = count;
            this.entry = entry;
        }

        public long getFirst() {
            return this.first;
        }

        public int getCount() {
            return this.count;
        }

        public int getEntry() {
            return this.entry;
        }
    }

    public static String fourcc() {
        return "stsc";
    }

    public SampleToChunkBox(SampleToChunkEntry[] sampleToChunk) {
        super(new Header(fourcc()));
        this.sampleToChunk = sampleToChunk;
    }

    public SampleToChunkBox() {
        super(new Header(fourcc()));
    }

    public void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putInt(this.sampleToChunk.length);
        for (SampleToChunkEntry stc : this.sampleToChunk) {
            out.putInt((int) stc.getFirst());
            out.putInt(stc.getCount());
            out.putInt(stc.getEntry());
        }
    }
}
