package org.jcodec;

import java.nio.ByteBuffer;

public class ClipRegionBox extends Box {
    private short height;
    private short rgnSize;
    private short width;
    private short f1296x;
    private short f1297y;

    public static String fourcc() {
        return "crgn";
    }

    public ClipRegionBox(Header atom) {
        super(atom);
    }

    public ClipRegionBox() {
        super(new Header(fourcc()));
    }

    public ClipRegionBox(short x, short y, short width, short height) {
        this();
        this.rgnSize = (short) 10;
        this.f1296x = x;
        this.f1297y = y;
        this.width = width;
        this.height = height;
    }

    protected void doWrite(ByteBuffer out) {
        out.putShort(this.rgnSize);
        out.putShort(this.f1297y);
        out.putShort(this.f1296x);
        out.putShort(this.height);
        out.putShort(this.width);
    }
}
