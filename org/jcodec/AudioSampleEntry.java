package org.jcodec;

import com.facebook.internal.NativeProtocol;
import com.facebook.internal.ServerProtocol;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AudioSampleEntry extends SampleEntry {
    private static final MyFactory FACTORY;
    public static Set<String> pcms;
    private int bytesPerFrame;
    private int bytesPerPkt;
    private int bytesPerSample;
    private short channelCount;
    private int compressionId;
    private int lpcmFlags;
    private int pktSize;
    private short revision;
    private float sampleRate;
    private short sampleSize;
    private int samplesPerPkt;
    private int vendor;
    private short version;

    public static class MyFactory extends BoxFactory {
        private Map<String, Class<? extends Box>> mappings;

        public MyFactory() {
            this.mappings = new HashMap();
            this.mappings.put(WaveExtension.fourcc(), WaveExtension.class);
            this.mappings.put(ChannelBox.fourcc(), ChannelBox.class);
            this.mappings.put("esds", LeafBox.class);
        }
    }

    static {
        FACTORY = new MyFactory();
        pcms = new HashSet();
        pcms.add("raw ");
        pcms.add("twos");
        pcms.add("sowt");
        pcms.add("fl32");
        pcms.add("fl64");
        pcms.add("in24");
        pcms.add("in32");
        pcms.add("lpcm");
    }

    public AudioSampleEntry(Header atom) {
        super(atom);
        this.factory = FACTORY;
    }

    public AudioSampleEntry(Header header, short drefInd, short channelCount, short sampleSize, int sampleRate, short revision, int vendor, int compressionId, int pktSize, int samplesPerPkt, int bytesPerPkt, int bytesPerFrame, int bytesPerSample, short version) {
        super(header, drefInd);
        this.channelCount = channelCount;
        this.sampleSize = sampleSize;
        this.sampleRate = (float) sampleRate;
        this.revision = revision;
        this.vendor = vendor;
        this.compressionId = compressionId;
        this.pktSize = pktSize;
        this.samplesPerPkt = samplesPerPkt;
        this.bytesPerPkt = bytesPerPkt;
        this.bytesPerFrame = bytesPerFrame;
        this.bytesPerSample = bytesPerSample;
        this.version = version;
    }

    protected void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putShort(this.version);
        out.putShort(this.revision);
        out.putInt(this.vendor);
        if (this.version < (short) 2) {
            out.putShort(this.channelCount);
            if (this.version == (short) 0) {
                out.putShort(this.sampleSize);
            } else {
                out.putShort((short) 16);
            }
            out.putShort((short) this.compressionId);
            out.putShort((short) this.pktSize);
            out.putInt((int) Math.round(((double) this.sampleRate) * 65536.0d));
            if (this.version == (short) 1) {
                out.putInt(this.samplesPerPkt);
                out.putInt(this.bytesPerPkt);
                out.putInt(this.bytesPerFrame);
                out.putInt(this.bytesPerSample);
                writeExtensions(out);
            }
        } else if (this.version == (short) 2) {
            out.putShort((short) 3);
            out.putShort((short) 16);
            out.putShort((short) -2);
            out.putShort((short) 0);
            out.putInt(NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REQUEST);
            out.putInt(72);
            out.putLong(Double.doubleToLongBits((double) this.sampleRate));
            out.putInt(this.channelCount);
            out.putInt(2130706432);
            out.putInt(this.sampleSize);
            out.putInt(this.lpcmFlags);
            out.putInt(this.bytesPerFrame);
            out.putInt(this.samplesPerPkt);
            writeExtensions(out);
        }
    }

    public void dump(StringBuilder sb) {
        sb.append(this.header.getFourcc() + ": {\n");
        sb.append("entry: ");
        ToJSON.toJSON(this, sb, "channelCount", "sampleSize", "sampleRat", "revision", "vendor", "compressionId", "pktSize", "samplesPerPkt", "bytesPerPkt", "bytesPerFrame", "bytesPerSample", ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION, "lpcmFlags");
        sb.append(",\nexts: [\n");
        dumpBoxes(sb);
        sb.append("\n]\n");
        sb.append("}\n");
    }
}
