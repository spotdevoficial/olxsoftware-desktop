package org.jcodec;

import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

public class Util {

    public static class Pair<T> {
        private T f1301a;
        private T f1302b;

        public Pair(T a, T b) {
            this.f1301a = a;
            this.f1302b = b;
        }

        public T getA() {
            return this.f1301a;
        }

        public T getB() {
            return this.f1302b;
        }
    }

    public static Pair<List<Edit>> split(List<Edit> edits, Rational trackByMv, long tvMv) {
        long total = 0;
        List<Edit> l = new ArrayList();
        List<Edit> r = new ArrayList();
        ListIterator<Edit> lit = edits.listIterator();
        while (lit.hasNext()) {
            Edit edit = (Edit) lit.next();
            if (edit.getDuration() + total > tvMv) {
                int leftDurMV = (int) (tvMv - total);
                int leftDurMedia = trackByMv.multiplyS(leftDurMV);
                Edit left = new Edit((long) leftDurMV, edit.getMediaTime(), MediaUploadState.IMAGE_PROGRESS_UPLOADED);
                Edit right = new Edit(edit.getDuration() - ((long) leftDurMV), ((long) leftDurMedia) + edit.getMediaTime(), MediaUploadState.IMAGE_PROGRESS_UPLOADED);
                lit.remove();
                if (left.getDuration() > 0) {
                    lit.add(left);
                    l.add(left);
                }
                if (right.getDuration() > 0) {
                    lit.add(right);
                    r.add(right);
                }
                while (lit.hasNext()) {
                    r.add(lit.next());
                }
                return new Pair(l, r);
            }
            l.add(edit);
            total += edit.getDuration();
        }
        while (lit.hasNext()) {
            r.add(lit.next());
        }
        return new Pair(l, r);
    }

    public static List<Edit> editsOnEdits(Rational mvByTrack, List<Edit> lower, List<Edit> higher) {
        List<Edit> result = new ArrayList();
        List<Edit> next = new ArrayList(lower);
        for (Edit edit : higher) {
            long startMv = mvByTrack.multiply(edit.getMediaTime());
            Pair<List<Edit>> split2 = split((List) split(next, mvByTrack.flip(), startMv).getB(), mvByTrack.flip(), edit.getDuration() + startMv);
            result.addAll((Collection) split2.getA());
            next = (List) split2.getB();
        }
        return result;
    }
}
