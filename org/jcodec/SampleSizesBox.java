package org.jcodec;

import java.nio.ByteBuffer;

public class SampleSizesBox extends FullBox {
    private int count;
    private int defaultSize;
    private int[] sizes;

    public static String fourcc() {
        return "stsz";
    }

    public SampleSizesBox(int defaultSize, int count) {
        this();
        this.defaultSize = defaultSize;
        this.count = count;
    }

    public SampleSizesBox(int[] sizes) {
        this();
        this.sizes = sizes;
    }

    public SampleSizesBox() {
        super(new Header(fourcc()));
    }

    public void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putInt(this.defaultSize);
        if (this.defaultSize == 0) {
            out.putInt(this.sizes.length);
            for (int i : this.sizes) {
                out.putInt((int) ((long) i));
            }
            return;
        }
        out.putInt(this.count);
    }
}
