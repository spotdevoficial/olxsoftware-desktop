package org.jcodec;

import java.nio.ByteBuffer;

public class ChunkOffsetsBox extends FullBox {
    private long[] chunkOffsets;

    public static String fourcc() {
        return "stco";
    }

    public ChunkOffsetsBox(long[] chunkOffsets) {
        super(new Header(fourcc()));
        this.chunkOffsets = chunkOffsets;
    }

    public ChunkOffsetsBox() {
        super(new Header(fourcc()));
    }

    public void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putInt(this.chunkOffsets.length);
        for (long offset : this.chunkOffsets) {
            out.putInt((int) offset);
        }
    }
}
