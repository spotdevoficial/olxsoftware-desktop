package org.jcodec;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.util.HashMap;
import java.util.Map;

public class MP4Util {
    private static Map<Codec, String> codecMapping;

    static {
        codecMapping = new HashMap();
        codecMapping.put(Codec.MPEG2, "m2v1");
        codecMapping.put(Codec.H264, "avc1");
        codecMapping.put(Codec.J2K, "mjp2");
    }

    public static void writeMovieOnFile(SeekableByteChannel out, MovieBox movie) throws IOException {
        long lastPos = out.position();
        out.close();
        RandomAccessFile raf = new RandomAccessFile(out.getFileName(), "rws");
        raf.setLength(Math.max(out.getWantedSize() * 2, 256000));
        MappedByteBuffer buf = raf.getChannel().map(MapMode.READ_WRITE, 0, raf.length());
        buf.position((int) lastPos);
        movie.write(buf);
        int writePos = buf.position();
        FileChannel rafCh = raf.getChannel();
        rafCh.truncate((long) writePos);
        out.setChannel(rafCh);
    }
}
