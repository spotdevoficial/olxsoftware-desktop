package org.jcodec;

import java.lang.reflect.Array;

public class IntObjectMap<T> {
    private int size;
    private Object[] storage;

    public IntObjectMap() {
        this.storage = new Object[128];
    }

    public void put(int key, T val) {
        if (this.storage.length <= key) {
            Object[] ns = new Object[(this.storage.length + 128)];
            System.arraycopy(this.storage, 0, ns, 0, this.storage.length);
            this.storage = ns;
        }
        if (this.storage[key] == null) {
            this.size++;
        }
        this.storage[key] = val;
    }

    public T get(int key) {
        return key >= this.storage.length ? null : this.storage[key];
    }

    public T[] values(T[] runtime) {
        Object[] result = (Object[]) ((Object[]) Array.newInstance(runtime.getClass().getComponentType(), this.size));
        int r = 0;
        for (int i = 0; i < this.storage.length; i++) {
            if (this.storage[i] != null) {
                int r2 = r + 1;
                result[r] = this.storage[i];
                r = r2;
            }
        }
        return result;
    }
}
