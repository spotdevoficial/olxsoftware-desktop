package org.jcodec;

public class CAVLCWriter {
    public static void writeUE(BitWriter out, int value) {
        int bits = 0;
        int cumul = 0;
        int i = 0;
        while (i < 15) {
            if (value < (1 << i) + cumul) {
                bits = i;
                break;
            } else {
                cumul += 1 << i;
                i++;
            }
        }
        out.writeNBit(0, bits);
        out.write1Bit(1);
        out.writeNBit(value - cumul, bits);
    }

    public static void writeUE(BitWriter out, int value, String message) {
        writeUE(out, value);
        Debug.trace(message, Integer.valueOf(value));
    }

    public static void writeSE(BitWriter out, int value, String message) {
        writeUE(out, MathUtil.golomb(value));
        Debug.trace(message, Integer.valueOf(value));
    }

    public static void writeBool(BitWriter out, boolean value, String message) {
        int i;
        int i2 = 1;
        if (value) {
            i = 1;
        } else {
            i = 0;
        }
        out.write1Bit(i);
        Object[] objArr = new Object[1];
        if (!value) {
            i2 = 0;
        }
        objArr[0] = Integer.valueOf(i2);
        Debug.trace(message, objArr);
    }

    public static void writeU(BitWriter out, int i, int n) {
        out.writeNBit(i, n);
    }

    public static void writeNBit(BitWriter out, long value, int n, String message) {
        for (int i = 0; i < n; i++) {
            out.write1Bit(((int) (value >> ((n - i) - 1))) & 1);
        }
        Debug.trace(message, Long.valueOf(value));
    }

    public static void writeTrailingBits(BitWriter out) {
        out.write1Bit(1);
        out.flush();
    }
}
