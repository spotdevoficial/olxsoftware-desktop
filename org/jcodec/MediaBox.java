package org.jcodec;

public class MediaBox extends NodeBox {
    public static String fourcc() {
        return "mdia";
    }

    public MediaBox(Header atom) {
        super(atom);
    }

    public MediaBox() {
        super(new Header(fourcc()));
    }
}
