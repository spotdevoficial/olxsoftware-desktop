package org.jcodec;

import java.nio.ByteBuffer;
import java.util.List;

public class EditListBox extends FullBox {
    private List<Edit> edits;

    public static String fourcc() {
        return "elst";
    }

    public EditListBox(Header atom) {
        super(atom);
    }

    public EditListBox() {
        this(new Header(fourcc()));
    }

    public EditListBox(List<Edit> edits) {
        this();
        this.edits = edits;
    }

    protected void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putInt(this.edits.size());
        for (Edit edit : this.edits) {
            out.putInt((int) edit.getDuration());
            out.putInt((int) edit.getMediaTime());
            out.putInt((int) (edit.getRate() * 65536.0f));
        }
    }

    protected void dump(StringBuilder sb) {
        super.dump(sb);
        sb.append(": ");
        ToJSON.toJSON(this, sb, "edits");
    }
}
