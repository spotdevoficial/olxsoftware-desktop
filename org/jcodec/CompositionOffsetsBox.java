package org.jcodec;

import java.nio.ByteBuffer;

public class CompositionOffsetsBox extends FullBox {
    private Entry[] entries;

    public static class Entry {
        public int count;
        public int offset;

        public Entry(int count, int offset) {
            this.count = count;
            this.offset = offset;
        }

        public int getOffset() {
            return this.offset;
        }
    }

    public CompositionOffsetsBox() {
        super(new Header(fourcc()));
    }

    public CompositionOffsetsBox(Entry[] entries) {
        super(new Header(fourcc()));
        this.entries = entries;
    }

    public static String fourcc() {
        return "ctts";
    }

    protected void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putInt(this.entries.length);
        for (int i = 0; i < this.entries.length; i++) {
            out.putInt(this.entries[i].count);
            out.putInt(this.entries[i].offset);
        }
    }
}
