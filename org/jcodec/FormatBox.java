package org.jcodec;

import java.nio.ByteBuffer;

public class FormatBox extends Box {
    private String fmt;

    public FormatBox(Box other) {
        super(other);
    }

    public FormatBox(Header header) {
        super(header);
    }

    public FormatBox(String fmt) {
        super(new Header(fourcc()));
        this.fmt = fmt;
    }

    public static String fourcc() {
        return "frma";
    }

    protected void doWrite(ByteBuffer out) {
        out.put(JCodecUtil.asciiString(this.fmt));
    }
}
