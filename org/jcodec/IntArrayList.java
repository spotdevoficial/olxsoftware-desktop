package org.jcodec;

public class IntArrayList {
    private int growAmount;
    private int size;
    private int[] storage;

    public IntArrayList() {
        this(128);
    }

    public IntArrayList(int growAmount) {
        this.growAmount = growAmount;
        this.storage = new int[growAmount];
    }

    public int[] toArray() {
        int[] result = new int[this.size];
        System.arraycopy(this.storage, 0, result, 0, this.size);
        return result;
    }

    public void add(int val) {
        if (this.size >= this.storage.length) {
            int[] ns = new int[(this.storage.length + this.growAmount)];
            System.arraycopy(this.storage, 0, ns, 0, this.storage.length);
            this.storage = ns;
        }
        int[] iArr = this.storage;
        int i = this.size;
        this.size = i + 1;
        iArr[i] = val;
    }

    public int size() {
        return this.size;
    }
}
