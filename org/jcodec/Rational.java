package org.jcodec;

public class Rational {
    private final int den;
    private final int num;

    public Rational(int num, int den) {
        this.num = num;
        this.den = den;
    }

    public int getNum() {
        return this.num;
    }

    public int getDen() {
        return this.den;
    }

    public int hashCode() {
        return ((this.den + 31) * 31) + this.num;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Rational other = (Rational) obj;
        if (this.den != other.den) {
            return false;
        }
        if (this.num != other.num) {
            return false;
        }
        return true;
    }

    public int multiplyS(int val) {
        return (int) ((((long) this.num) * ((long) val)) / ((long) this.den));
    }

    public long multiply(long val) {
        return (((long) this.num) * val) / ((long) this.den);
    }

    public Rational flip() {
        return new Rational(this.den, this.num);
    }
}
