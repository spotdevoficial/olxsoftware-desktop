package org.jcodec;

import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class NodeBox extends Box {
    protected List<Box> boxes;
    protected BoxFactory factory;

    public NodeBox(Header atom) {
        super(atom);
        this.boxes = new LinkedList();
        this.factory = BoxFactory.getDefault();
    }

    public NodeBox(NodeBox other) {
        super((Box) other);
        this.boxes = new LinkedList();
        this.factory = BoxFactory.getDefault();
        this.boxes = other.boxes;
        this.factory = other.factory;
    }

    public List<Box> getBoxes() {
        return this.boxes;
    }

    public void add(Box box) {
        this.boxes.add(box);
    }

    protected void doWrite(ByteBuffer out) {
        for (Box box : this.boxes) {
            box.write(out);
        }
    }

    public void addFirst(MovieHeaderBox box) {
        this.boxes.add(0, box);
    }

    public void dump(StringBuilder sb) {
        super.dump(sb);
        sb.append(": {\n");
        dumpBoxes(sb);
        sb.append("\n}");
    }

    protected void dumpBoxes(StringBuilder sb) {
        StringBuilder sb1 = new StringBuilder();
        Iterator<Box> iterator = this.boxes.iterator();
        while (iterator.hasNext()) {
            ((Box) iterator.next()).dump(sb1);
            if (iterator.hasNext()) {
                sb1.append(",\n");
            }
        }
        sb.append(sb1.toString().replaceAll("([^\n]*)\n", "  $1\n"));
    }
}
