package org.jcodec;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

public class DataRefBox extends NodeBox {
    private static final MyFactory FACTORY;

    public static class MyFactory extends BoxFactory {
        private Map<String, Class<? extends Box>> mappings;

        public MyFactory() {
            this.mappings = new HashMap();
            this.mappings.put(UrlBox.fourcc(), UrlBox.class);
            this.mappings.put(AliasBox.fourcc(), AliasBox.class);
            this.mappings.put("cios", AliasBox.class);
        }
    }

    static {
        FACTORY = new MyFactory();
    }

    public static String fourcc() {
        return "dref";
    }

    public DataRefBox() {
        this(new Header(fourcc()));
    }

    private DataRefBox(Header atom) {
        super(atom);
        this.factory = FACTORY;
    }

    public void doWrite(ByteBuffer out) {
        out.putInt(0);
        out.putInt(this.boxes.size());
        super.doWrite(out);
    }
}
