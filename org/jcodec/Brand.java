package org.jcodec;

import java.util.Arrays;

public enum Brand {
    MP4("isom", 512, new String[]{"isom", "iso2", "avc1", "mp41"});
    
    private FileTypeBox ftyp;

    private Brand(String majorBrand, int version, String[] compatible) {
        this.ftyp = new FileTypeBox(majorBrand, version, Arrays.asList(compatible));
    }

    public FileTypeBox getFileTypeBox() {
        return this.ftyp;
    }
}
