package org.jcodec;

import java.nio.ByteBuffer;

public class SampleEntry extends NodeBox {
    private short drefInd;

    public SampleEntry(Header header) {
        super(header);
    }

    public SampleEntry(Header header, short drefInd) {
        super(header);
        this.drefInd = drefInd;
    }

    protected void doWrite(ByteBuffer out) {
        out.put(new byte[]{(byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0});
        out.putShort(this.drefInd);
    }

    protected void writeExtensions(ByteBuffer out) {
        super.doWrite(out);
    }
}
