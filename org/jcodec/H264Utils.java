package org.jcodec;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;

public class H264Utils {
    public static ByteBuffer nextNALUnit(ByteBuffer buf) {
        skipToNALUnit(buf);
        return gotoNALUnit(buf);
    }

    public static final void skipToNALUnit(ByteBuffer buf) {
        if (buf.hasRemaining()) {
            int val = -1;
            while (buf.hasRemaining()) {
                val = (val << 8) | (buf.get() & 255);
                if ((16777215 & val) == 1) {
                    buf.position(buf.position());
                    return;
                }
            }
        }
    }

    public static final ByteBuffer gotoNALUnit(ByteBuffer buf) {
        if (!buf.hasRemaining()) {
            return null;
        }
        int from = buf.position();
        ByteBuffer result = buf.slice();
        result.order(ByteOrder.BIG_ENDIAN);
        int val = -1;
        while (buf.hasRemaining()) {
            val = (val << 8) | (buf.get() & 255);
            if ((16777215 & val) == 1) {
                buf.position(buf.position() - (val == 1 ? 4 : 3));
                result.limit(buf.position() - from);
                return result;
            }
        }
        return result;
    }

    public static int golomb2Signed(int val) {
        return ((val >> 1) + (val & 1)) * (((val & 1) << 1) - 1);
    }

    public static void encodeMOVPacket(ByteBuffer avcFrame) {
        ByteBuffer dup = avcFrame.duplicate();
        ByteBuffer d1 = avcFrame.duplicate();
        int tot = d1.position();
        while (true) {
            ByteBuffer buf = nextNALUnit(dup);
            if (buf != null) {
                d1.position(tot);
                d1.putInt(buf.remaining());
                tot += buf.remaining() + 4;
            } else {
                return;
            }
        }
    }

    public static void wipePS(ByteBuffer in, List<ByteBuffer> spsList, List<ByteBuffer> ppsList) {
        ByteBuffer dup = in.duplicate();
        while (dup.hasRemaining()) {
            ByteBuffer buf = nextNALUnit(dup);
            if (buf != null) {
                NALUnit nu = NALUnit.read(buf);
                if (nu.type == NALUnitType.PPS) {
                    if (ppsList != null) {
                        ppsList.add(buf);
                    }
                    in.position(dup.position());
                } else if (nu.type == NALUnitType.SPS) {
                    if (spsList != null) {
                        spsList.add(buf);
                    }
                    in.position(dup.position());
                } else {
                    return;
                }
            }
            return;
        }
    }

    public static int getPicHeightInMbs(SeqParameterSet sps) {
        return (sps.pic_height_in_map_units_minus1 + 1) << (sps.frame_mbs_only_flag ? 0 : 1);
    }
}
