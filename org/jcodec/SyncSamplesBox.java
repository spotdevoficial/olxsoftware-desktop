package org.jcodec;

import java.nio.ByteBuffer;

public class SyncSamplesBox extends FullBox {
    private int[] syncSamples;

    public static String fourcc() {
        return "stss";
    }

    public SyncSamplesBox() {
        super(new Header(fourcc()));
    }

    public SyncSamplesBox(int[] array) {
        this();
        this.syncSamples = array;
    }

    protected void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putInt(this.syncSamples.length);
        for (int putInt : this.syncSamples) {
            out.putInt(putInt);
        }
    }
}
