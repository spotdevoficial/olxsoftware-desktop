package org.jcodec;

public class MediaInfoBox extends NodeBox {
    public static String fourcc() {
        return "minf";
    }

    public MediaInfoBox(Header atom) {
        super(atom);
    }

    public MediaInfoBox() {
        super(new Header(fourcc()));
    }
}
