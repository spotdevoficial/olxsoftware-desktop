package br.com.spotdev.olxsoftware.scraper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;import javafx.scene.control.ProgressBar;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import br.com.spotdev.olxsoftware.Demo;
import br.com.spotdev.olxsoftware.OLXSoftware;
import br.com.spotdev.olxsoftware.scraper.object.Categoria;
import br.com.spotdev.olxsoftware.scraper.object.Product;
import br.com.spotdev.olxsoftware.scraper.object.Regiao;

public class Scraper {

	/**
	 * S C R A P E R    C O M U M
	 */

	List<Regiao> regioes = new ArrayList<Regiao>();
	List<Regiao> subRegioes = new ArrayList<Regiao>();
	List<Regiao> states = new ArrayList<Regiao>();
	ConcurrentHashMap<String, Product> exists = new ConcurrentHashMap<String, Product>();
	ConcurrentLinkedQueue<String> existsUrl = new ConcurrentLinkedQueue<String>();
	ConcurrentHashMap<String, Product> numeros = new ConcurrentHashMap<String, Product>();
	ConcurrentLinkedQueue<Product> needToOcread = new ConcurrentLinkedQueue<Product>();
	ConcurrentLinkedQueue<Product> productLog = new ConcurrentLinkedQueue<Product>();
	List<Thread> ocrThreads = new ArrayList<Thread>();
	List<Thread> httpThreads = new ArrayList<Thread>();
	Thread infoThread = new Thread();
	Thread exportThread = new Thread();
	String exportDir;
	long now = 0;
	int lastPage = 0;
	int fail = 0;
	int pagesSize = 0;
	String lastUrl;
	public Label infoLabel;
	public ProgressBar infoBar;
	public static final String appdata = System.getenv("APPDATA");
	
	
	public static int MAX_CONTATOS = 2000;
	public boolean abertoMsg = false;
	public boolean ultima = false;
		
	public void start(int downloadPower, int ocrPower, Label infoLabel, ProgressBar infoBar, String exportDir){
		
		
		switch (OLXSoftware.TYPE) {
			case OLXSoftware.ADMIN:
				MAX_CONTATOS = -1;
				break;
			case OLXSoftware.PROFISSIONAL:
				MAX_CONTATOS = -1;
				break;
			case OLXSoftware.MULTIFUNCIONALIDADE:
				MAX_CONTATOS = -1;
				break;
		}
		
		if(Demo.DEMOSTRATIVO){
			MAX_CONTATOS = 500;
		}
		
		this.infoLabel = infoLabel;
		this.infoBar = infoBar;
		this.exportDir = exportDir;
		
		String appdata = System.getenv("APPDATA");
		if(Files.exists(Paths.get(appdata+"/.TCache"+"0"+OLXSoftware.TYPE))){
				
			BufferedReader br = null;

			try {

				String productValue;
				br = new BufferedReader(new FileReader(appdata+"/.TCache"+"0"+OLXSoftware.TYPE));

				while ((productValue = br.readLine()) != null) {
					String[] data = productValue.split(";");
					
					if(data.length < 4)
						continue;
					
					String nome = data[0];
					String number = data[1];
					String municipio = data[2];
					String state = data[3];
					
					Product p = new Product();
					p.setCidade(municipio);
					p.setName(nome);
					p.setEstado(state);
					
					exists.put(number, p);
				}

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (br != null)br.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}
		if(Files.exists(Paths.get(appdata+"/.TCacheUrls"+"0"+OLXSoftware.TYPE))){
			
			BufferedReader br = null;

			try {

				String productValue;
				br = new BufferedReader(new FileReader(appdata+"/.TCacheUrls"+"0"+OLXSoftware.TYPE));
				while ((productValue = br.readLine()) != null) {
					existsUrl.add(productValue);
				}

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (br != null)br.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}

		//////////////////////////
		//
		//   DOWNLOAD DE DADOS
		//
		//////////////////////////
		
		// calcula a �ltima p�gina
		String urlSize = null;
		if(OLXSoftware.TYPE != OLXSoftware.COMUM && 
				OLXSoftware.TYPE != OLXSoftware.MULTIFUNCIONALIDADE)
			urlSize = lastUrl+"?f=c";
		else
			urlSize = lastUrl;
		
		Document docSizer = null;
		try {
			docSizer = Jsoup.connect(urlSize).timeout(0).userAgent("Mozilla").get();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Elements elements = docSizer.select(".module_pagination .item.last > a");
		if(elements.size() == 0){
			Alert alert = null;
			alert = new Alert(AlertType.ERROR, 
					"Lamento! Não existem dados o suficiente nessa página.");
			alert.showAndWait();
			System.exit(0);
		}
			
		Element maxSize = elements.get(0);
		String maxString = maxSize.attr("href");
		maxString = maxString.substring(maxString.indexOf("o="), maxString.length());
		maxString = maxString.replaceAll("o=", "");
		
		try{
			pagesSize = Integer.parseInt(maxString);
		}catch(Exception e){
			e.printStackTrace();
		}

		for(int i = 0; i < downloadPower; i++){

			Thread httpt = new Thread(()-> {
				int page = lastPage;
				Document doc = null;
				httpWhile: while(!Thread.currentThread().isInterrupted()){

					page=(lastPage=lastPage+1);	
					try {

						String url = page != 1 ? lastUrl+"?o="+page : lastUrl;
						if(OLXSoftware.TYPE != OLXSoftware.COMUM && 
								OLXSoftware.TYPE != OLXSoftware.MULTIFUNCIONALIDADE)
							url = page != 1 ? url+"&f=c" : url+"?f=c";

						doc = Jsoup.connect(url).timeout(0).userAgent("Mozilla").get();
						Elements elementsProducts = doc.select(".OLXad-list-link");
//						System.out.println("Pego "+elementsProducts.size()+" anúncios "+url);
						productFor: for(Element produtoElement : elementsProducts){
							// System.out.println(elementsProducts.size()+" "+page+" "+pagesSize);
							if(now == 0)
								now = System.currentTimeMillis();
							String annUrl = produtoElement.attr("href");
							
							if(existsUrl.contains(annUrl)){
								continue;
							}

							existsUrl.add(annUrl);
//							System.out.println(annUrl);
							Document docAnn = Jsoup.connect(annUrl).timeout(0).userAgent("Mozilla").get();
							Elements elementsAnn = docAnn.select("#visible_phone .number");
							String src = elementsAnn.attr("src");
							if(src.isEmpty()){
								continue;
							}

							Element nome = docAnn.select(".page_OLXad-view .site_sidebar .section_OLXad-user-info .list .item.owner p").get(0);
							Elements dados = docAnn.select(".page_OLXad-view .section_OLXad-info .atributes .list .item .text");
							Elements municipio = dados.clone();
							Elements bairro = dados.clone();
							Elements categoria = dados.clone();
							Elements cep = dados.clone();
							String tipoOuCategoria = null;
							for(Element tcElement : categoria){
								if(tcElement.text().contains("Tipo")){
									tipoOuCategoria = "Tipo";
									break;
								}
							}
							tipoOuCategoria = tipoOuCategoria == null ? "Categoria" : tipoOuCategoria; 
							final String tpF = tipoOuCategoria;
							municipio.removeIf((value)->!value.text().startsWith("Município"));
							categoria.removeIf((value)->!value.text().startsWith(tpF));
							cep.removeIf((value)->!value.text().startsWith("CEP"));
							bairro.removeIf((value)->!value.text().startsWith("Bairro"));
							String estado = lastUrl.replaceAll("http://", "").split("\\.")[0];
														
						    Product produto = new Product();
						    produto.setUrl(annUrl);
						    produto.setPage(page);
						    produto.setName(nome.text());
						    produto.setId(annUrl.substring(annUrl.lastIndexOf("-")+1, annUrl.length()));
						    	
						    if(categoria.size() > 0)
						    	produto.setCategoria(categoria.get(0).text().replaceAll(tipoOuCategoria+": ", ""));
						    else
						    	produto.setCategoria("Não citado");
						    if(produtoElement.select(".detail-category").text().contains(" - Anúncio Profissional")){
						    	produto.setTipo("Profissional");
							}else{
								produto.setTipo("Particular");
							}
						    produto.setCidade(municipio.get(0).text().replaceAll("Município: ", ""));
						    if(bairro.size() > 0)
						    	produto.setBairro(bairro.get(0).text().replaceAll("Bairro: ", ""));
						    else
						    	produto.setBairro("Não citado");
						    
						    if(cep.size() > 0)
						    	produto.setCep(cep.get(0).text().replaceAll("CEP: ", ""));
						    else
						    	produto.setCep("Não citado");
						    produto.setEstado(estado);
						    
						    
						    for(Product item : productLog){
						    	if(item.equals(produto)){
						    		continue productFor;
						    	}
						    }
						    
						    needToOcread.add(produto);
						    productLog.add(produto);
						    if(productLog.size() >= 1000){
						    	for(int l = 0; l < 500; l++){
						    		productLog.poll();
						    	}
						    }

						}
						if(pagesSize <= page){
							ultima = true;
							break httpWhile;
						}
											
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
			});
			httpt.start();
			httpThreads.add(httpt);
		}
		
		//////////////////////////
		//
		//   OCR
		//
		//////////////////////////
		
		for(int b = 0; b < ocrPower; b++){
			Thread ocrt = new Thread(()-> {
				
				while(!Thread.currentThread().isInterrupted()){
					if(numeros.size() >= 100 && Demo.DEMOSTRATIVO){
						Demo.useDemo();
					}
					if(numeros.size() >= MAX_CONTATOS && MAX_CONTATOS != -1){
						
						saveDataStoped();
						dispose();
						break;
					}
					Product product = needToOcread.poll();
					if(product == null){
						try {
							Thread.sleep(500);
						} catch (Exception e) {
							e.printStackTrace();
						}
						continue;
					}
			        try {
			        	String url = "https://nga.olx.com.br/api/v1.1/"
			        			+ "public/ads/"+product.getId()+
			        			"/phone?lang=pt";
			            String json = readUrl(url);
			        	JSONObject object = new JSONObject(json);
			        	JSONArray phones = object.getJSONArray("phones");
			        	String ocreaded = phones.optJSONObject(0).getString("value");
			        	ocreaded = ocreaded.substring(3,ocreaded.length());
			            if(!exists.containsKey(ocreaded.replaceAll("", "").replace(" ", "").replace("(", "").replaceAll("\\)", ""))){
		            		numeros.put(ocreaded, product);
			            }
		            	now = 0;
			        } catch (Exception e) {
			            e.printStackTrace();
			        }
				}
			});
			ocrt.start();
			ocrThreads.add(ocrt);

		}
		
		//////////////////////////
		//
		//   MOSTRAR INFORMA�OES
		//
		//////////////////////////
		
		infoThread = new Thread(()-> {
			
			while(!Thread.currentThread().isInterrupted()){
				int sizenow = numeros.size();
				try {
					Thread.sleep(1000);
				} catch (Exception e) {
					e.printStackTrace();
				}
				int newsize = numeros.size();
				
				int quantidadePS = newsize-sizenow;
				if(quantidadePS > 0){
					
					Platform.runLater(()->{
						String message = "Capturando "+quantidadePS+" contatos por segundo\t"+numeros.size();
						if(MAX_CONTATOS > -1){
							message+="/"+MAX_CONTATOS;
						}
						infoLabel.setText(message);
					});
					
					Platform.runLater(()->infoBar.setProgress(MAX_CONTATOS > -1 ? (double)(numeros.size()*100/MAX_CONTATOS)/(double)100 : -1));
				}
				
				if(quantidadePS <= 0 && ultima == true){
					fail++;
				}else{
					fail = 0;
				}
				
				if(fail >= 60){
					exportDataStoped(false);
					saveDataStoped();
					dispose();
					break;
				}
//				System.out.println("Capturando "+quantidadePS+" contatos por segundo\t\tTamanho da fila OCR: "+needToOcread.size()+"\t\tQuantidade total: "+numeros.size()+"\t\tImagens: "+productLog.size()+"\t\t"+"�ltima p�gina: "+lastPage);
			}   
			    
		});	
		
		infoThread.start();
		
		//////////////////////////
		//
		//   EXPORTA��O DE DADOS
		//
		//////////////////////////
		
		exportThread = new Thread(()->{
			while(!Thread.currentThread().isInterrupted()){
				try {
					Thread.sleep(5000);
				} catch (Exception e) {
					e.printStackTrace();
				}finally {
			
					exportDataStoped(false);
					
				}
				
			}
		});
		exportThread.start();
		
	}       
	
	private static String readUrl(String urlString) throws Exception {
	    BufferedReader reader = null;
	    try {
	        URL url = new URL(urlString);
	        reader = new BufferedReader(new InputStreamReader(url.openStream()));
	        StringBuffer buffer = new StringBuffer();
	        int read;
	        char[] chars = new char[1024];
	        while ((read = reader.read(chars)) != -1)
	            buffer.append(chars, 0, read); 

	        return buffer.toString();
	    } finally {
	        if (reader != null)
	            reader.close();
	    }
	}
	
	//////////////////////////
	//
	//   SALVA O CACHE
	//
	//////////////////////////
	
	public void saveDataStoped(){
		if(!abertoMsg){
			String data = exportDataStoped(true);
			
			StringBuilder dataUrl = new StringBuilder();
			for(String url : existsUrl){
				dataUrl.append(url);
				dataUrl.append("\n");
			}
			
			
			try {
				File file = new File(appdata+"/.TCache"+"0"+OLXSoftware.TYPE);
				file.createNewFile();
			    Files.write(Paths.get(appdata+"/.TCache"+"0"+OLXSoftware.TYPE), data.toString().getBytes(), StandardOpenOption.APPEND);
			}catch (IOException e) {
				e.printStackTrace();
			}
			
			try {
				File file = new File(appdata+"/.TCacheUrls"+"0"+OLXSoftware.TYPE);
				if(!file.exists())
					file.createNewFile();
			    Files.write(Paths.get(appdata+"/.TCacheUrls"+"0"+OLXSoftware.TYPE), dataUrl.toString().getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
			}catch (IOException e) {
				e.printStackTrace();
			}
			
			Platform.runLater(()->{
				this.infoLabel.setText("Completo!\tExportado "+this.numeros.size()+" contatos");
				this.infoBar.setProgress(100);
				Alert alert = null;
				if(Demo.DEMOSTRATIVO){
					alert = new Alert(AlertType.CONFIRMATION, "Dados exportados com sucesso! Obrigado por testar nossa aplicação!");
				}else{
					alert = new Alert(AlertType.CONFIRMATION, "Dados exportados com sucesso! Obrigado por utilizar nossa aplicação!");
				}
				alert.showAndWait();
				System.exit(0);
			});
		}
		
	}
	
	//////////////////////////
	//
	//   EXPORTA OS DADOS
	//
	//////////////////////////
	
	public synchronized String exportDataStoped(boolean blockNext){

		System.out.println("Exportando... "+numeros.size()+" contatos");	
		if(abertoMsg || numeros.size() <= 0) return null;
		
		if(blockNext)
			abertoMsg = true;
		if(numeros.size() >= MAX_CONTATOS/4 && MAX_CONTATOS > -1 &&
				!Demo.DEMOSTRATIVO){
			try {
				Path path = 
						Paths.get(appdata+"/.TValidate"+"0"+OLXSoftware.TYPE);
				if(!Files.exists(path))
					Files.createFile(path);
				
			    Files.write(path, (System.currentTimeMillis()+"").getBytes(), 
			    		StandardOpenOption.TRUNCATE_EXISTING);
			}catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		StringBuilder data = new StringBuilder();
		for(String number : numeros.keySet()){
			Product product = numeros.get(number);
			String numero = number.replaceAll("", "").replace(" ", "").replace("(", "").replaceAll("\\)", "");
			
			data.append(product.getName());
			data.append(";");
			data.append(numero);
			data.append(";");
			data.append(product.getCidade());
			data.append(";");
			data.append(product.getEstado().toUpperCase());
			data.append(";");
			data.append(product.getBairro());
			data.append(";");
			data.append(product.getCep());
			data.append(";");
			data.append(product.getCategoria());
			if(OLXSoftware.TYPE != OLXSoftware.COMUM){
				data.append(";");
				data.append(product.getTipo());
			}
			data.append("\n");
			
		}
		
		if(!blockNext){
			try {
			    Files.write(Paths.get(exportDir), data.toString().getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
				System.out.println("EXPORTADO "+numeros.size()+" "+data.toString());
			}catch (IOException e) {
				e.printStackTrace();
			}
		}
		return data.toString();
		
	}
	
	public void dispose(){
				
		httpThreads.forEach((value)->value.interrupt());
		ocrThreads.forEach((value)->value.interrupt());
		infoThread.interrupt();
		exportThread.interrupt();
		System.out.println("Interrompendo threads.");
		
	}
	
	public List<Regiao> getRegions(Regiao estado){

		try {
			Document doc = Jsoup.connect(estado.getUrl()).timeout(0).userAgent("Mozilla").get();
			Elements elementsRegions = doc.select(".page_listing .section_linkshelf .linkshelf-tabs .linkshelf-tabs-content .list .item .link");
			regioes.clear();
			subRegioes.clear();
			
			for(Element region : elementsRegions){
				Regiao regiaoNova = new Regiao();
				regiaoNova.setUrl(region.attr("href"));
				regiaoNova.setNome(region.text());
				this.regioes.add(regiaoNova);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return regioes;
	}
	
	public List<Regiao> getSubregions(Regiao state){
		try {
			Document doc = Jsoup.connect(state.getUrl()).timeout(0).userAgent("Mozilla").get();
			Elements elementsRegions = doc.select(".page_listing .section_linkshelf .linkshelf-tabs .linkshelf-tabs-content .list .item .link");
			subRegioes.clear();
			
			for(Element region : elementsRegions){
				Regiao regiaoNova = new Regiao();
				regiaoNova.setUrl(region.attr("href"));
				regiaoNova.setNome(region.text());
				this.subRegioes.add(regiaoNova);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return subRegioes;
	}
	
	public List<Regiao> getStates(Categoria categoria){
		try {
			Document doc = Jsoup.connect("http://olx.com.br/").timeout(0).userAgent("Mozilla").get();
			Elements elementsUrl = doc.select(".state");
			states.clear();
			regioes.clear();
			subRegioes.clear();
			for(Element state : elementsUrl){
				Regiao regiao = new Regiao();
				regiao.setUrl(state.attr("xlink:href")+"/"+categoria.getURL());
				regiao.setNome(state.child(0).text());
				this.states.add(regiao);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return states;
	}

	public String getLastUrl() {
		return lastUrl;
	}

	public void setLastUrl(String lastUrl) {
		this.lastUrl = lastUrl;
	}
	
	
	
}
