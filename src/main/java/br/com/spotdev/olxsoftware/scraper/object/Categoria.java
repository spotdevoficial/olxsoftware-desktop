package br.com.spotdev.olxsoftware.scraper.object;

/**
 * @Descri��o Enum de categorias do OLX
 * 
 * @Autor Davi Salles
 * @Data 14/04/2016
 */

public enum Categoria {

	NENHUMA(""),
	ANIMAIS("animais-e-acessorios"),
	BEBES("bebes-e-criancas"),
	CASA("para-a-sua-casa"),
	ELETRONICO("eletronicos-e-celulares"),
	EMPREGOS("empregos-e-negocios"),
	ESPORTES("esportes"),
	IMOVEIS("imoveis"),
	MODA("moda-e-beleza"),
	MUSICA("musica-e-hobbies"),
	VEICULOS("veiculos");
	
	private String url;
	
	Categoria(String url){
		this.url = url;
	}
	
	public String getURL(){
		return url;
	}
	
}
