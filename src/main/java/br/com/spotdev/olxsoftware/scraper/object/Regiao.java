package br.com.spotdev.olxsoftware.scraper.object;

public class Regiao {

	public String url = null;
	public String nome = null;
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString(){
		return nome;
	}
	
}
