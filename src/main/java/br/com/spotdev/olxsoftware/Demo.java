package br.com.spotdev.olxsoftware;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Demo {

	public static boolean DEMOSTRATIVO = false;
	public static String DIVULGADOR_PATH = getAppdata()+"/.olx_torpedos";
	
	public static boolean hasUsedDemo(){
		Path finalPath = Paths.get(DIVULGADOR_PATH+"/demo");
		return Files.exists(finalPath);
	}
	
	public static void useDemo(){
		getDivulgadorPath("demo");
	}
	
	public static String getAppdata(){
		String appdata = System.getenv("APPDATA");
	    return appdata;
	}
	
	public static Path getDivulgadorPath(String file){
		Path directory = Paths.get(DIVULGADOR_PATH);
		if(!Files.exists(directory)){
			try {
				Files.createDirectories(directory);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		Path finalPath = Paths.get(DIVULGADOR_PATH+"/"+file);
		if(!Files.exists(finalPath)){
			try {
				Files.createFile(finalPath);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return finalPath;
	}
	
}
