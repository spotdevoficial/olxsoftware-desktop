package com.j256.ormlite.field;

import com.facebook.internal.ServerProtocol;
import com.facebook.share.internal.ShareConstants;

public class DatabaseFieldConfigLoader {
    private static final DataPersister DEFAULT_DATA_PERSISTER;

    static {
        DEFAULT_DATA_PERSISTER = DatabaseFieldConfig.DEFAULT_DATA_TYPE.getDataPersister();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.j256.ormlite.field.DatabaseFieldConfig fromReader(java.io.BufferedReader r8) throws java.sql.SQLException {
        /*
        r1 = new com.j256.ormlite.field.DatabaseFieldConfig;
        r1.<init>();
        r0 = 0;
    L_0x0006:
        r3 = r8.readLine();	 Catch:{ IOException -> 0x000f }
        if (r3 != 0) goto L_0x0017;
    L_0x000c:
        if (r0 == 0) goto L_0x0064;
    L_0x000e:
        return r1;
    L_0x000f:
        r2 = move-exception;
        r5 = "Could not read DatabaseFieldConfig from stream";
        r5 = com.j256.ormlite.misc.SqlExceptionUtil.create(r5, r2);
        throw r5;
    L_0x0017:
        r5 = "# --field-end--";
        r5 = r3.equals(r5);
        if (r5 != 0) goto L_0x000c;
    L_0x001f:
        r5 = r3.length();
        if (r5 == 0) goto L_0x0006;
    L_0x0025:
        r5 = "#";
        r5 = r3.startsWith(r5);
        if (r5 != 0) goto L_0x0006;
    L_0x002d:
        r5 = "# --field-start--";
        r5 = r3.equals(r5);
        if (r5 != 0) goto L_0x0006;
    L_0x0035:
        r5 = "=";
        r6 = -2;
        r4 = r3.split(r5, r6);
        r5 = r4.length;
        r6 = 2;
        if (r5 == r6) goto L_0x0059;
    L_0x0040:
        r5 = new java.sql.SQLException;
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r7 = "DatabaseFieldConfig reading from stream cannot parse line: ";
        r6 = r6.append(r7);
        r6 = r6.append(r3);
        r6 = r6.toString();
        r5.<init>(r6);
        throw r5;
    L_0x0059:
        r5 = 0;
        r5 = r4[r5];
        r6 = 1;
        r6 = r4[r6];
        readField(r1, r5, r6);
        r0 = 1;
        goto L_0x0006;
    L_0x0064:
        r1 = 0;
        goto L_0x000e;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.j256.ormlite.field.DatabaseFieldConfigLoader.fromReader(java.io.BufferedReader):com.j256.ormlite.field.DatabaseFieldConfig");
    }

    private static void readField(DatabaseFieldConfig config, String field, String value) {
        if (field.equals("fieldName")) {
            config.setFieldName(value);
            return;
        }
        if (field.equals("columnName")) {
            config.setColumnName(value);
            return;
        }
        if (field.equals("dataPersister")) {
            config.setDataPersister(DataType.valueOf(value).getDataPersister());
            return;
        }
        if (field.equals("defaultValue")) {
            config.setDefaultValue(value);
            return;
        }
        if (field.equals("width")) {
            config.setWidth(Integer.parseInt(value));
            return;
        }
        if (field.equals("canBeNull")) {
            config.setCanBeNull(Boolean.parseBoolean(value));
            return;
        }
        if (field.equals(ShareConstants.WEB_DIALOG_PARAM_ID)) {
            config.setId(Boolean.parseBoolean(value));
            return;
        }
        if (field.equals("generatedId")) {
            config.setGeneratedId(Boolean.parseBoolean(value));
            return;
        }
        if (field.equals("generatedIdSequence")) {
            config.setGeneratedIdSequence(value);
            return;
        }
        if (field.equals("foreign")) {
            config.setForeign(Boolean.parseBoolean(value));
            return;
        }
        if (field.equals("useGetSet")) {
            config.setUseGetSet(Boolean.parseBoolean(value));
            return;
        }
        if (field.equals("unknownEnumValue")) {
            String[] parts = value.split("#", -2);
            if (parts.length != 2) {
                throw new IllegalArgumentException("Invalid value for unknownEnumValue which should be in class#name format: " + value);
            }
            try {
                Object[] consts = Class.forName(parts[0]).getEnumConstants();
                if (consts == null) {
                    throw new IllegalArgumentException("Invalid class is not an Enum for unknownEnumValue: " + value);
                }
                boolean found = false;
                for (Enum<?> enumInstance : (Enum[]) consts) {
                    if (enumInstance.name().equals(parts[1])) {
                        config.setUnknownEnumValue(enumInstance);
                        found = true;
                    }
                }
                if (!found) {
                    throw new IllegalArgumentException("Invalid enum value name for unknownEnumvalue: " + value);
                }
                return;
            } catch (ClassNotFoundException e) {
                throw new IllegalArgumentException("Unknown class specified for unknownEnumValue: " + value);
            }
        }
        if (field.equals("throwIfNull")) {
            config.setThrowIfNull(Boolean.parseBoolean(value));
            return;
        }
        if (field.equals("format")) {
            config.setFormat(value);
            return;
        }
        if (field.equals("unique")) {
            config.setUnique(Boolean.parseBoolean(value));
            return;
        }
        if (field.equals("uniqueCombo")) {
            config.setUniqueCombo(Boolean.parseBoolean(value));
            return;
        }
        if (field.equals("index")) {
            config.setIndex(Boolean.parseBoolean(value));
            return;
        }
        if (field.equals("indexName")) {
            config.setIndex(true);
            config.setIndexName(value);
            return;
        }
        if (field.equals("uniqueIndex")) {
            config.setUniqueIndex(Boolean.parseBoolean(value));
            return;
        }
        if (field.equals("uniqueIndexName")) {
            config.setUniqueIndex(true);
            config.setUniqueIndexName(value);
            return;
        }
        if (field.equals("foreignAutoRefresh")) {
            config.setForeignAutoRefresh(Boolean.parseBoolean(value));
            return;
        }
        if (field.equals("maxForeignAutoRefreshLevel")) {
            config.setMaxForeignAutoRefreshLevel(Integer.parseInt(value));
            return;
        }
        if (field.equals("persisterClass")) {
            try {
                config.setPersisterClass(Class.forName(value));
                return;
            } catch (ClassNotFoundException e2) {
                throw new IllegalArgumentException("Could not find persisterClass: " + value);
            }
        }
        if (field.equals("allowGeneratedIdInsert")) {
            config.setAllowGeneratedIdInsert(Boolean.parseBoolean(value));
            return;
        }
        if (field.equals("columnDefinition")) {
            config.setColumnDefinition(value);
            return;
        }
        if (field.equals("foreignAutoCreate")) {
            config.setForeignAutoCreate(Boolean.parseBoolean(value));
            return;
        }
        if (field.equals(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION)) {
            config.setVersion(Boolean.parseBoolean(value));
            return;
        }
        if (field.equals("foreignColumnName")) {
            config.setForeignColumnName(value);
            return;
        }
        if (field.equals("readOnly")) {
            config.setReadOnly(Boolean.parseBoolean(value));
            return;
        }
        if (field.equals("foreignCollection")) {
            config.setForeignCollection(Boolean.parseBoolean(value));
            return;
        }
        if (field.equals("foreignCollectionEager")) {
            config.setForeignCollectionEager(Boolean.parseBoolean(value));
            return;
        }
        if (field.equals("maxEagerForeignCollectionLevel")) {
            config.setForeignCollectionMaxEagerLevel(Integer.parseInt(value));
            return;
        }
        if (field.equals("foreignCollectionMaxEagerLevel")) {
            config.setForeignCollectionMaxEagerLevel(Integer.parseInt(value));
            return;
        }
        if (field.equals("foreignCollectionColumnName")) {
            config.setForeignCollectionColumnName(value);
            return;
        }
        if (field.equals("foreignCollectionOrderColumn")) {
            config.setForeignCollectionOrderColumnName(value);
            return;
        }
        if (field.equals("foreignCollectionOrderColumnName")) {
            config.setForeignCollectionOrderColumnName(value);
            return;
        }
        if (field.equals("foreignCollectionOrderAscending")) {
            config.setForeignCollectionOrderAscending(Boolean.parseBoolean(value));
            return;
        }
        if (field.equals("foreignCollectionForeignColumnName")) {
            config.setForeignCollectionForeignFieldName(value);
            return;
        }
        if (field.equals("foreignCollectionForeignFieldName")) {
            config.setForeignCollectionForeignFieldName(value);
        }
    }
}
