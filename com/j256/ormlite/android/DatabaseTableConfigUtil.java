package com.j256.ormlite.android;

import com.facebook.internal.ServerProtocol;
import com.facebook.share.internal.ShareConstants;
import com.j256.ormlite.db.DatabaseType;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DatabaseFieldConfig;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.DatabaseTableConfig;
import com.schibsted.scm.nextgenapp.C1061R;
import com.urbanairship.C1608R;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DatabaseTableConfigUtil {
    private static Class<?> annotationFactoryClazz;
    private static Class<?> annotationMemberClazz;
    private static final int[] configFieldNums;
    private static Field elementsField;
    private static Field nameField;
    private static Field valueField;
    private static int workedC;

    private static class DatabaseFieldSample {
        @DatabaseField
        String field;

        private DatabaseFieldSample() {
        }
    }

    static {
        workedC = 0;
        configFieldNums = lookupClasses();
    }

    public static <T> DatabaseTableConfig<T> fromClass(ConnectionSource connectionSource, Class<T> clazz) throws SQLException {
        DatabaseType databaseType = connectionSource.getDatabaseType();
        String tableName = DatabaseTableConfig.extractTableName(clazz);
        List fieldConfigs = new ArrayList();
        for (Class<?> classWalk = clazz; classWalk != null; classWalk = classWalk.getSuperclass()) {
            for (Field field : classWalk.getDeclaredFields()) {
                DatabaseFieldConfig config = configFromField(databaseType, tableName, field);
                if (config != null && config.isPersisted()) {
                    fieldConfigs.add(config);
                }
            }
        }
        if (fieldConfigs.size() == 0) {
            return null;
        }
        return new DatabaseTableConfig((Class) clazz, tableName, fieldConfigs);
    }

    private static int[] lookupClasses() {
        try {
            annotationFactoryClazz = Class.forName("org.apache.harmony.lang.annotation.AnnotationFactory");
            annotationMemberClazz = Class.forName("org.apache.harmony.lang.annotation.AnnotationMember");
            Class<?> annotationMemberArrayClazz = Class.forName("[Lorg.apache.harmony.lang.annotation.AnnotationMember;");
            try {
                elementsField = annotationFactoryClazz.getDeclaredField("elements");
                elementsField.setAccessible(true);
                nameField = annotationMemberClazz.getDeclaredField(ShareConstants.WEB_DIALOG_PARAM_NAME);
                nameField.setAccessible(true);
                valueField = annotationMemberClazz.getDeclaredField("value");
                valueField.setAccessible(true);
                InvocationHandler proxy = Proxy.getInvocationHandler((DatabaseField) DatabaseFieldSample.class.getDeclaredField("field").getAnnotation(DatabaseField.class));
                if (proxy.getClass() != annotationFactoryClazz) {
                    return null;
                }
                try {
                    Object elements = elementsField.get(proxy);
                    if (elements == null || elements.getClass() != annotationMemberArrayClazz) {
                        return null;
                    }
                    Object[] elementArray = (Object[]) elements;
                    int[] configNums = new int[elementArray.length];
                    for (int i = 0; i < elementArray.length; i++) {
                        configNums[i] = configFieldNameToNum((String) nameField.get(elementArray[i]));
                    }
                    return configNums;
                } catch (IllegalAccessException e) {
                    return null;
                }
            } catch (SecurityException e2) {
                return null;
            } catch (NoSuchFieldException e3) {
                return null;
            }
        } catch (ClassNotFoundException e4) {
            return null;
        }
    }

    private static int configFieldNameToNum(String configName) {
        if (configName.equals("columnName")) {
            return 1;
        }
        if (configName.equals("dataType")) {
            return 2;
        }
        if (configName.equals("defaultValue")) {
            return 3;
        }
        if (configName.equals("width")) {
            return 4;
        }
        if (configName.equals("canBeNull")) {
            return 5;
        }
        if (configName.equals(ShareConstants.WEB_DIALOG_PARAM_ID)) {
            return 6;
        }
        if (configName.equals("generatedId")) {
            return 7;
        }
        if (configName.equals("generatedIdSequence")) {
            return 8;
        }
        if (configName.equals("foreign")) {
            return 9;
        }
        if (configName.equals("useGetSet")) {
            return 10;
        }
        if (configName.equals("unknownEnumName")) {
            return 11;
        }
        if (configName.equals("throwIfNull")) {
            return 12;
        }
        if (configName.equals("persisted")) {
            return 13;
        }
        if (configName.equals("format")) {
            return 14;
        }
        if (configName.equals("unique")) {
            return 15;
        }
        if (configName.equals("uniqueCombo")) {
            return 16;
        }
        if (configName.equals("index")) {
            return 17;
        }
        if (configName.equals("uniqueIndex")) {
            return 18;
        }
        if (configName.equals("indexName")) {
            return 19;
        }
        if (configName.equals("uniqueIndexName")) {
            return 20;
        }
        if (configName.equals("foreignAutoRefresh")) {
            return 21;
        }
        if (configName.equals("maxForeignAutoRefreshLevel")) {
            return 22;
        }
        if (configName.equals("persisterClass")) {
            return 23;
        }
        if (configName.equals("allowGeneratedIdInsert")) {
            return 24;
        }
        if (configName.equals("columnDefinition")) {
            return 25;
        }
        if (configName.equals("foreignAutoCreate")) {
            return 26;
        }
        if (configName.equals(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION)) {
            return 27;
        }
        if (configName.equals("foreignColumnName")) {
            return 28;
        }
        if (configName.equals("readOnly")) {
            return 29;
        }
        throw new IllegalStateException("Could not find support for DatabaseField " + configName);
    }

    private static DatabaseFieldConfig configFromField(DatabaseType databaseType, String tableName, Field field) throws SQLException {
        if (configFieldNums == null) {
            return DatabaseFieldConfig.fromField(databaseType, tableName, field);
        }
        DatabaseField databaseField = (DatabaseField) field.getAnnotation(DatabaseField.class);
        DatabaseFieldConfig config = null;
        if (databaseField != null) {
            try {
                config = buildConfig(databaseField, tableName, field);
            } catch (Exception e) {
            }
        }
        if (config == null) {
            return DatabaseFieldConfig.fromField(databaseType, tableName, field);
        }
        workedC++;
        return config;
    }

    private static DatabaseFieldConfig buildConfig(DatabaseField databaseField, String tableName, Field field) throws Exception {
        DatabaseFieldConfig databaseFieldConfig = null;
        InvocationHandler proxy = Proxy.getInvocationHandler(databaseField);
        if (proxy.getClass() == annotationFactoryClazz) {
            Object elementsObject = elementsField.get(proxy);
            if (elementsObject != null) {
                databaseFieldConfig = new DatabaseFieldConfig(field.getName());
                Object[] objs = (Object[]) elementsObject;
                for (int i = 0; i < configFieldNums.length; i++) {
                    Object value = valueField.get(objs[i]);
                    if (value != null) {
                        assignConfigField(configFieldNums[i], databaseFieldConfig, field, value);
                    }
                }
            }
        }
        return databaseFieldConfig;
    }

    private static void assignConfigField(int configNum, DatabaseFieldConfig config, Field field, Object value) {
        switch (configNum) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                config.setColumnName(valueIfNotBlank((String) value));
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                config.setDataType((DataType) value);
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                String defaultValue = (String) value;
                if (defaultValue != null && !defaultValue.equals("__ormlite__ no default value string was specified")) {
                    config.setDefaultValue(defaultValue);
                }
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                config.setWidth(((Integer) value).intValue());
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                config.setCanBeNull(((Boolean) value).booleanValue());
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                config.setId(((Boolean) value).booleanValue());
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                config.setGeneratedId(((Boolean) value).booleanValue());
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                config.setGeneratedIdSequence(valueIfNotBlank((String) value));
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                config.setForeign(((Boolean) value).booleanValue());
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                config.setUseGetSet(((Boolean) value).booleanValue());
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                config.setUnknownEnumValue(DatabaseFieldConfig.findMatchingEnumVal(field, (String) value));
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                config.setThrowIfNull(((Boolean) value).booleanValue());
            case C1608R.styleable.MapAttrs_useViewLifecycle /*13*/:
                config.setPersisted(((Boolean) value).booleanValue());
            case C1608R.styleable.MapAttrs_zOrderOnTop /*14*/:
                config.setFormat(valueIfNotBlank((String) value));
            case C1608R.styleable.MapAttrs_uiMapToolbar /*15*/:
                config.setUnique(((Boolean) value).booleanValue());
            case C1061R.styleable.Toolbar_maxButtonHeight /*16*/:
                config.setUniqueCombo(((Boolean) value).booleanValue());
            case C1061R.styleable.Toolbar_collapseIcon /*17*/:
                config.setIndex(((Boolean) value).booleanValue());
            case C1061R.styleable.Toolbar_collapseContentDescription /*18*/:
                config.setUniqueIndex(((Boolean) value).booleanValue());
            case C1061R.styleable.Toolbar_navigationIcon /*19*/:
                config.setIndexName(valueIfNotBlank((String) value));
            case C1061R.styleable.Toolbar_navigationContentDescription /*20*/:
                config.setUniqueIndexName(valueIfNotBlank((String) value));
            case C1061R.styleable.Theme_actionBarTheme /*21*/:
                config.setForeignAutoRefresh(((Boolean) value).booleanValue());
            case C1061R.styleable.Theme_actionBarWidgetTheme /*22*/:
                config.setMaxForeignAutoRefreshLevel(((Integer) value).intValue());
            case C1061R.styleable.Theme_actionBarSize /*23*/:
                config.setPersisterClass((Class) value);
            case C1061R.styleable.Theme_actionBarDivider /*24*/:
                config.setAllowGeneratedIdInsert(((Boolean) value).booleanValue());
            case C1061R.styleable.Theme_actionBarItemBackground /*25*/:
                config.setColumnDefinition(valueIfNotBlank((String) value));
            case C1061R.styleable.Theme_actionMenuTextAppearance /*26*/:
                config.setForeignAutoCreate(((Boolean) value).booleanValue());
            case C1061R.styleable.Theme_actionMenuTextColor /*27*/:
                config.setVersion(((Boolean) value).booleanValue());
            case C1061R.styleable.Theme_actionModeStyle /*28*/:
                config.setForeignColumnName(valueIfNotBlank((String) value));
            case C1061R.styleable.Theme_actionModeCloseButtonStyle /*29*/:
                config.setReadOnly(((Boolean) value).booleanValue());
            default:
                throw new IllegalStateException("Could not find support for DatabaseField number " + configNum);
        }
    }

    private static String valueIfNotBlank(String value) {
        if (value == null || value.length() == 0) {
            return null;
        }
        return value;
    }
}
