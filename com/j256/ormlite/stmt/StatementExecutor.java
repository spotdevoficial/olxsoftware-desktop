package com.j256.ormlite.stmt;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.ObjectCache;
import com.j256.ormlite.db.DatabaseType;
import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.logger.Logger;
import com.j256.ormlite.logger.LoggerFactory;
import com.j256.ormlite.stmt.StatementBuilder.StatementType;
import com.j256.ormlite.stmt.mapped.MappedCreate;
import com.j256.ormlite.stmt.mapped.MappedDelete;
import com.j256.ormlite.stmt.mapped.MappedUpdate;
import com.j256.ormlite.support.CompiledStatement;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.support.DatabaseConnection;
import com.j256.ormlite.support.DatabaseResults;
import com.j256.ormlite.table.TableInfo;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StatementExecutor<T, ID> implements GenericRowMapper<String[]> {
    private static Logger logger;
    private static final FieldType[] noFieldTypes;
    private String countStarQuery;
    private final Dao<T, ID> dao;
    private final DatabaseType databaseType;
    private FieldType[] ifExistsFieldTypes;
    private String ifExistsQuery;
    private MappedDelete<T, ID> mappedDelete;
    private MappedCreate<T, ID> mappedInsert;
    private MappedUpdate<T, ID> mappedUpdate;
    private PreparedQuery<T> preparedQueryForAll;
    private final TableInfo<T, ID> tableInfo;

    static {
        logger = LoggerFactory.getLogger(StatementExecutor.class);
        noFieldTypes = new FieldType[0];
    }

    public StatementExecutor(DatabaseType databaseType, TableInfo<T, ID> tableInfo, Dao<T, ID> dao) {
        this.databaseType = databaseType;
        this.tableInfo = tableInfo;
        this.dao = dao;
    }

    public T queryForFirst(DatabaseConnection databaseConnection, PreparedStmt<T> preparedStmt, ObjectCache objectCache) throws SQLException {
        CompiledStatement stmt = preparedStmt.compile(databaseConnection, StatementType.SELECT);
        DatabaseResults databaseResults = null;
        try {
            T mapRow;
            databaseResults = stmt.runQuery(objectCache);
            if (databaseResults.first()) {
                logger.debug("query-for-first of '{}' returned at least 1 result", preparedStmt.getStatement());
                mapRow = preparedStmt.mapRow(databaseResults);
            } else {
                logger.debug("query-for-first of '{}' returned at 0 results", preparedStmt.getStatement());
                mapRow = null;
                if (databaseResults != null) {
                    databaseResults.close();
                }
                stmt.close();
            }
            return mapRow;
        } finally {
            if (databaseResults != null) {
                databaseResults.close();
            }
            stmt.close();
        }
    }

    public long queryForCountStar(DatabaseConnection databaseConnection) throws SQLException {
        if (this.countStarQuery == null) {
            StringBuilder sb = new StringBuilder(64);
            sb.append("SELECT COUNT(*) FROM ");
            this.databaseType.appendEscapedEntityName(sb, this.tableInfo.getTableName());
            this.countStarQuery = sb.toString();
        }
        long count = databaseConnection.queryForLong(this.countStarQuery);
        logger.debug("query of '{}' returned {}", this.countStarQuery, Long.valueOf(count));
        return count;
    }

    public long queryForLong(DatabaseConnection databaseConnection, PreparedStmt<T> preparedStmt) throws SQLException {
        CompiledStatement stmt = preparedStmt.compile(databaseConnection, StatementType.SELECT_LONG);
        DatabaseResults results = null;
        try {
            results = stmt.runQuery(null);
            if (results.first()) {
                long j = results.getLong(0);
                return j;
            }
            throw new SQLException("No result found in queryForLong: " + preparedStmt.getStatement());
        } finally {
            if (results != null) {
                results.close();
            }
            stmt.close();
        }
    }

    public List<T> query(ConnectionSource connectionSource, PreparedStmt<T> preparedStmt, ObjectCache objectCache) throws SQLException {
        SelectIterator<T, ID> iterator = buildIterator(null, connectionSource, preparedStmt, objectCache, -1);
        try {
            List<T> results = new ArrayList();
            while (iterator.hasNextThrow()) {
                results.add(iterator.nextThrow());
            }
            logger.debug("query of '{}' returned {} results", preparedStmt.getStatement(), Integer.valueOf(results.size()));
            return results;
        } finally {
            iterator.close();
        }
    }

    public SelectIterator<T, ID> buildIterator(BaseDaoImpl<T, ID> classDao, ConnectionSource connectionSource, int resultFlags, ObjectCache objectCache) throws SQLException {
        prepareQueryForAll();
        return buildIterator(classDao, connectionSource, this.preparedQueryForAll, objectCache, resultFlags);
    }

    public SelectIterator<T, ID> buildIterator(BaseDaoImpl<T, ID> classDao, ConnectionSource connectionSource, PreparedStmt<T> preparedStmt, ObjectCache objectCache, int resultFlags) throws SQLException {
        DatabaseConnection connection = connectionSource.getReadOnlyConnection();
        CompiledStatement compiledStatement = null;
        try {
            compiledStatement = preparedStmt.compile(connection, StatementType.SELECT, resultFlags);
            SelectIterator<T, ID> iterator = new SelectIterator(this.tableInfo.getDataClass(), classDao, preparedStmt, connectionSource, connection, compiledStatement, preparedStmt.getStatement(), objectCache);
            connection = null;
            compiledStatement = null;
            return iterator;
        } finally {
            if (compiledStatement != null) {
                compiledStatement.close();
            }
            if (connection != null) {
                connectionSource.releaseConnection(connection);
            }
        }
    }

    public int create(DatabaseConnection databaseConnection, T data, ObjectCache objectCache) throws SQLException {
        if (this.mappedInsert == null) {
            this.mappedInsert = MappedCreate.build(this.databaseType, this.tableInfo);
        }
        return this.mappedInsert.insert(this.databaseType, databaseConnection, data, objectCache);
    }

    public int update(DatabaseConnection databaseConnection, T data, ObjectCache objectCache) throws SQLException {
        if (this.mappedUpdate == null) {
            this.mappedUpdate = MappedUpdate.build(this.databaseType, this.tableInfo);
        }
        return this.mappedUpdate.update(databaseConnection, data, objectCache);
    }

    public int delete(DatabaseConnection databaseConnection, T data, ObjectCache objectCache) throws SQLException {
        if (this.mappedDelete == null) {
            this.mappedDelete = MappedDelete.build(this.databaseType, this.tableInfo);
        }
        return this.mappedDelete.delete(databaseConnection, data, objectCache);
    }

    public int deleteById(DatabaseConnection databaseConnection, ID id, ObjectCache objectCache) throws SQLException {
        if (this.mappedDelete == null) {
            this.mappedDelete = MappedDelete.build(this.databaseType, this.tableInfo);
        }
        return this.mappedDelete.deleteById(databaseConnection, id, objectCache);
    }

    public int delete(DatabaseConnection databaseConnection, PreparedDelete<T> preparedDelete) throws SQLException {
        CompiledStatement stmt = preparedDelete.compile(databaseConnection, StatementType.DELETE);
        try {
            int runUpdate = stmt.runUpdate();
            return runUpdate;
        } finally {
            stmt.close();
        }
    }

    public String[] mapRow(DatabaseResults results) throws SQLException {
        int columnN = results.getColumnCount();
        String[] result = new String[columnN];
        for (int colC = 0; colC < columnN; colC++) {
            result[colC] = results.getString(colC);
        }
        return result;
    }

    public boolean ifExists(DatabaseConnection connection, ID id) throws SQLException {
        if (this.ifExistsQuery == null) {
            QueryBuilder<T, ID> qb = new QueryBuilder(this.databaseType, this.tableInfo, this.dao);
            qb.selectRaw("COUNT(*)");
            qb.where().eq(this.tableInfo.getIdField().getColumnName(), new SelectArg());
            this.ifExistsQuery = qb.prepareStatementString();
            this.ifExistsFieldTypes = new FieldType[]{this.tableInfo.getIdField()};
        }
        long count = connection.queryForLong(this.ifExistsQuery, new Object[]{id}, this.ifExistsFieldTypes);
        logger.debug("query of '{}' returned {}", this.ifExistsQuery, Long.valueOf(count));
        if (count != 0) {
            return true;
        }
        return false;
    }

    private void prepareQueryForAll() throws SQLException {
        if (this.preparedQueryForAll == null) {
            this.preparedQueryForAll = new QueryBuilder(this.databaseType, this.tableInfo, this.dao).prepare();
        }
    }
}
