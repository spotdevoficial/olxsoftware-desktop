package com.j256.ormlite.misc;

import com.j256.ormlite.dao.Dao;

public abstract class BaseDaoEnabled<T, ID> {
    protected transient Dao<T, ID> dao;

    public void setDao(Dao<T, ID> dao) {
        this.dao = dao;
    }
}
