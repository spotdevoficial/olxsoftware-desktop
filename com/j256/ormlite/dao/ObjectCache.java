package com.j256.ormlite.dao;

public interface ObjectCache {
    <T, ID> T get(Class<T> cls, ID id);

    <T, ID> void put(Class<T> cls, ID id, T t);

    <T, ID> void remove(Class<T> cls, ID id);
}
