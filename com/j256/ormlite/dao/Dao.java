package com.j256.ormlite.dao;

import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedDelete;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import java.sql.SQLException;
import java.util.List;

public interface Dao<T, ID> extends CloseableIterable<T> {

    public static class CreateOrUpdateStatus {
        private boolean created;
        private int numLinesChanged;
        private boolean updated;

        public CreateOrUpdateStatus(boolean created, boolean updated, int numberLinesChanged) {
            this.created = created;
            this.updated = updated;
            this.numLinesChanged = numberLinesChanged;
        }
    }

    long countOf() throws SQLException;

    long countOf(PreparedQuery<T> preparedQuery) throws SQLException;

    int create(T t) throws SQLException;

    CreateOrUpdateStatus createOrUpdate(T t) throws SQLException;

    int delete(PreparedDelete<T> preparedDelete) throws SQLException;

    int delete(T t) throws SQLException;

    DeleteBuilder<T, ID> deleteBuilder();

    int deleteById(ID id) throws SQLException;

    ConnectionSource getConnectionSource();

    Class<T> getDataClass();

    CloseableIterator<T> iterator(PreparedQuery<T> preparedQuery, int i) throws SQLException;

    List<T> query(PreparedQuery<T> preparedQuery) throws SQLException;

    QueryBuilder<T, ID> queryBuilder();

    T queryForFirst(PreparedQuery<T> preparedQuery) throws SQLException;
}
