package com.j256.ormlite.dao;

import java.util.Collection;

public interface ForeignCollection<T> extends CloseableIterable<T>, Collection<T> {
}
