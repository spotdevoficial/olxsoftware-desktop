package com.drew.metadata.jfif;

import com.drew.lang.BufferBoundsException;
import com.drew.lang.BufferReader;
import com.drew.metadata.Metadata;

public class JfifReader {
    public void extract(BufferReader bufferReader, Metadata metadata) {
        JfifDirectory jfifDirectory = (JfifDirectory) metadata.getOrCreateDirectory(JfifDirectory.class);
        try {
            jfifDirectory.setInt(5, bufferReader.getUInt16(5));
            jfifDirectory.setInt(7, bufferReader.getUInt8(7));
            jfifDirectory.setInt(8, bufferReader.getUInt16(8));
            jfifDirectory.setInt(10, bufferReader.getUInt16(10));
        } catch (BufferBoundsException e) {
            jfifDirectory.addError(e.getMessage());
        }
    }
}
