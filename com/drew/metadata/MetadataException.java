package com.drew.metadata;

import com.drew.lang.CompoundException;

public class MetadataException extends CompoundException {
    public MetadataException(String str) {
        super(str);
    }

    public MetadataException(String str, Throwable th) {
        super(str, th);
    }

    public MetadataException(Throwable th) {
        super(th);
    }
}
