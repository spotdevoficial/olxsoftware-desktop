package com.drew.metadata;

public class Age {
    private int _days;
    private int _hours;
    private int _minutes;
    private int _months;
    private int _seconds;
    private int _years;

    public Age(int i, int i2, int i3, int i4, int i5, int i6) {
        this._years = i;
        this._months = i2;
        this._days = i3;
        this._hours = i4;
        this._minutes = i5;
        this._seconds = i6;
    }

    private static void appendAgePart(StringBuilder stringBuilder, int i, String str) {
        if (i != 0) {
            if (stringBuilder.length() != 0) {
                stringBuilder.append(' ');
            }
            stringBuilder.append(i).append(' ').append(str);
            if (i != 1) {
                stringBuilder.append('s');
            }
        }
    }

    public static Age fromPanasonicString(String str) {
        if (str == null) {
            throw new NullPointerException();
        } else if (str.length() != 19 || str.startsWith("9999:99:99")) {
            return null;
        } else {
            try {
                return new Age(Integer.parseInt(str.substring(0, 4)), Integer.parseInt(str.substring(5, 7)), Integer.parseInt(str.substring(8, 10)), Integer.parseInt(str.substring(11, 13)), Integer.parseInt(str.substring(14, 16)), Integer.parseInt(str.substring(17, 19)));
            } catch (NumberFormatException e) {
                return null;
            }
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Age age = (Age) obj;
        return this._days != age._days ? false : this._hours != age._hours ? false : this._minutes != age._minutes ? false : this._months != age._months ? false : this._seconds != age._seconds ? false : this._years == age._years;
    }

    public int hashCode() {
        return (((((((((this._years * 31) + this._months) * 31) + this._days) * 31) + this._hours) * 31) + this._minutes) * 31) + this._seconds;
    }

    public String toFriendlyString() {
        StringBuilder stringBuilder = new StringBuilder();
        appendAgePart(stringBuilder, this._years, "year");
        appendAgePart(stringBuilder, this._months, "month");
        appendAgePart(stringBuilder, this._days, "day");
        appendAgePart(stringBuilder, this._hours, "hour");
        appendAgePart(stringBuilder, this._minutes, "minute");
        appendAgePart(stringBuilder, this._seconds, "second");
        return stringBuilder.toString();
    }

    public String toString() {
        return String.format("%04d:%02d:%02d %02d:%02d:%02d", new Object[]{Integer.valueOf(this._years), Integer.valueOf(this._months), Integer.valueOf(this._days), Integer.valueOf(this._hours), Integer.valueOf(this._minutes), Integer.valueOf(this._seconds)});
    }
}
