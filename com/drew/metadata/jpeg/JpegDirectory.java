package com.drew.metadata.jpeg;

import com.drew.metadata.Directory;
import java.util.HashMap;

public class JpegDirectory extends Directory {
    protected static final HashMap<Integer, String> _tagNameMap;

    static {
        _tagNameMap = new HashMap();
        _tagNameMap.put(Integer.valueOf(-3), "Compression Type");
        _tagNameMap.put(Integer.valueOf(0), "Data Precision");
        _tagNameMap.put(Integer.valueOf(3), "Image Width");
        _tagNameMap.put(Integer.valueOf(1), "Image Height");
        _tagNameMap.put(Integer.valueOf(5), "Number of Components");
        _tagNameMap.put(Integer.valueOf(6), "Component 1");
        _tagNameMap.put(Integer.valueOf(7), "Component 2");
        _tagNameMap.put(Integer.valueOf(8), "Component 3");
        _tagNameMap.put(Integer.valueOf(9), "Component 4");
    }

    public JpegDirectory() {
        setDescriptor(new JpegDescriptor(this));
    }

    public JpegComponent getComponent(int i) {
        return (JpegComponent) getObject(i + 6);
    }

    public String getName() {
        return "Jpeg";
    }

    protected HashMap<Integer, String> getTagNameMap() {
        return _tagNameMap;
    }
}
