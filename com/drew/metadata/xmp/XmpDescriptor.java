package com.drew.metadata.xmp;

import com.drew.imaging.PhotographicConversions;
import com.drew.lang.Rational;
import com.drew.metadata.TagDescriptor;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import com.urbanairship.C1608R;
import java.text.DecimalFormat;

public class XmpDescriptor extends TagDescriptor<XmpDirectory> {
    private static final DecimalFormat SimpleDecimalFormatter;

    static {
        SimpleDecimalFormatter = new DecimalFormat("0.#");
    }

    public XmpDescriptor(XmpDirectory xmpDirectory) {
        super(xmpDirectory);
    }

    public String getApertureValueDescription() {
        Double doubleObject = ((XmpDirectory) this._directory).getDoubleObject(11);
        if (doubleObject == null) {
            return null;
        }
        return "F" + SimpleDecimalFormatter.format(PhotographicConversions.apertureToFStop(doubleObject.doubleValue()));
    }

    public String getDescription(int i) {
        switch (i) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return ((XmpDirectory) this._directory).getString(i);
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getExposureTimeDescription();
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return getShutterSpeedDescription();
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return getFNumberDescription();
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return ((XmpDirectory) this._directory).getString(i);
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return getFocalLengthDescription();
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                return getApertureValueDescription();
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                return getExposureProgramDescription();
            default:
                return super.getDescription(i);
        }
    }

    public String getExposureProgramDescription() {
        Integer integer = ((XmpDirectory) this._directory).getInteger(12);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Manual control";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Program normal";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Aperture priority";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Shutter priority";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Program creative (slow program)";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "Program action (high-speed program)";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "Portrait mode";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "Landscape mode";
            default:
                return "Unknown program (" + integer + ")";
        }
    }

    public String getExposureTimeDescription() {
        String string = ((XmpDirectory) this._directory).getString(3);
        return string == null ? null : string + " sec";
    }

    public String getFNumberDescription() {
        Rational rational = ((XmpDirectory) this._directory).getRational(5);
        return rational == null ? null : "F" + SimpleDecimalFormatter.format(rational.doubleValue());
    }

    public String getFocalLengthDescription() {
        Rational rational = ((XmpDirectory) this._directory).getRational(10);
        if (rational == null) {
            return null;
        }
        return new DecimalFormat("0.0##").format(rational.doubleValue()) + " mm";
    }

    public String getShutterSpeedDescription() {
        Float floatObject = ((XmpDirectory) this._directory).getFloatObject(4);
        if (floatObject == null) {
            return null;
        }
        if (floatObject.floatValue() <= MediaUploadState.IMAGE_PROGRESS_UPLOADED) {
            return (((float) Math.round(((double) ((float) (1.0d / Math.exp(((double) floatObject.floatValue()) * Math.log(2.0d))))) * 10.0d)) / 10.0f) + " sec";
        }
        return "1/" + ((int) Math.exp(((double) floatObject.floatValue()) * Math.log(2.0d))) + " sec";
    }
}
