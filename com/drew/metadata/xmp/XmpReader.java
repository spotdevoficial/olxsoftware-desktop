package com.drew.metadata.xmp;

import com.adobe.xmp.XMPException;
import com.adobe.xmp.XMPIterator;
import com.adobe.xmp.XMPMeta;
import com.adobe.xmp.XMPMetaFactory;
import com.adobe.xmp.properties.XMPPropertyInfo;
import com.drew.lang.BufferBoundsException;
import com.drew.lang.BufferReader;
import com.drew.lang.Rational;
import com.drew.metadata.Metadata;
import com.urbanairship.C1608R;
import java.util.Calendar;

public class XmpReader {
    private void processXmpTag(XMPMeta xMPMeta, XmpDirectory xmpDirectory, String str, String str2, int i, int i2) throws XMPException {
        String propertyString = xMPMeta.getPropertyString(str, str2);
        if (propertyString != null) {
            switch (i2) {
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    xmpDirectory.setString(i, propertyString);
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    String[] split = propertyString.split("/", 2);
                    if (split.length == 2) {
                        try {
                            xmpDirectory.setRational(i, new Rational((long) Float.parseFloat(split[0]), (long) Float.parseFloat(split[1])));
                            return;
                        } catch (NumberFormatException e) {
                            xmpDirectory.addError(String.format("Unable to parse XMP property %s as a Rational.", new Object[]{str2}));
                            return;
                        }
                    }
                    xmpDirectory.addError("Error in rational format for tag " + i);
                case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                    try {
                        xmpDirectory.setInt(i, Integer.valueOf(propertyString).intValue());
                    } catch (NumberFormatException e2) {
                        xmpDirectory.addError(String.format("Unable to parse XMP property %s as an int.", new Object[]{str2}));
                    }
                case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                    try {
                        xmpDirectory.setDouble(i, Double.valueOf(propertyString).doubleValue());
                    } catch (NumberFormatException e3) {
                        xmpDirectory.addError(String.format("Unable to parse XMP property %s as an double.", new Object[]{str2}));
                    }
                default:
                    xmpDirectory.addError(String.format("Unknown format code %d for tag %d", new Object[]{Integer.valueOf(i2), Integer.valueOf(i)}));
            }
        }
    }

    public void extract(BufferReader bufferReader, Metadata metadata) {
        if (bufferReader == null) {
            throw new NullPointerException("reader");
        } else if (metadata == null) {
            throw new NullPointerException("metadata");
        } else {
            XmpDirectory xmpDirectory = (XmpDirectory) metadata.getOrCreateDirectory(XmpDirectory.class);
            if (bufferReader.getLength() <= 30) {
                xmpDirectory.addError("Xmp data segment must contain at least 30 bytes");
                return;
            }
            try {
                if ("http://ns.adobe.com/xap/1.0/\u0000".equals(bufferReader.getString(0, 29))) {
                    try {
                        try {
                            XMPMeta parseFromBuffer = XMPMetaFactory.parseFromBuffer(bufferReader.getBytes(29, (int) (bufferReader.getLength() - 29)));
                            xmpDirectory.setXMPMeta(parseFromBuffer);
                            processXmpTag(parseFromBuffer, xmpDirectory, "http://ns.adobe.com/exif/1.0/aux/", "aux:LensInfo", 6, 1);
                            processXmpTag(parseFromBuffer, xmpDirectory, "http://ns.adobe.com/exif/1.0/aux/", "aux:Lens", 7, 1);
                            processXmpTag(parseFromBuffer, xmpDirectory, "http://ns.adobe.com/exif/1.0/aux/", "aux:SerialNumber", 8, 1);
                            processXmpTag(parseFromBuffer, xmpDirectory, "http://ns.adobe.com/exif/1.0/aux/", "aux:Firmware", 9, 1);
                            processXmpTag(parseFromBuffer, xmpDirectory, "http://ns.adobe.com/tiff/1.0/", "tiff:Make", 1, 1);
                            processXmpTag(parseFromBuffer, xmpDirectory, "http://ns.adobe.com/tiff/1.0/", "tiff:Model", 2, 1);
                            processXmpTag(parseFromBuffer, xmpDirectory, "http://ns.adobe.com/exif/1.0/", "exif:ExposureTime", 3, 1);
                            processXmpTag(parseFromBuffer, xmpDirectory, "http://ns.adobe.com/exif/1.0/", "exif:ExposureProgram", 12, 3);
                            processXmpTag(parseFromBuffer, xmpDirectory, "http://ns.adobe.com/exif/1.0/", "exif:ApertureValue", 11, 2);
                            processXmpTag(parseFromBuffer, xmpDirectory, "http://ns.adobe.com/exif/1.0/", "exif:FNumber", 5, 2);
                            processXmpTag(parseFromBuffer, xmpDirectory, "http://ns.adobe.com/exif/1.0/", "exif:FocalLength", 10, 2);
                            processXmpTag(parseFromBuffer, xmpDirectory, "http://ns.adobe.com/exif/1.0/", "exif:ShutterSpeedValue", 4, 2);
                            processXmpDateTag(parseFromBuffer, xmpDirectory, "http://ns.adobe.com/exif/1.0/", "exif:DateTimeOriginal", 13);
                            processXmpDateTag(parseFromBuffer, xmpDirectory, "http://ns.adobe.com/exif/1.0/", "exif:DateTimeDigitized", 14);
                            processXmpTag(parseFromBuffer, xmpDirectory, "http://ns.adobe.com/xap/1.0/", "xmp:Rating", 4097, 4);
                            XMPIterator it = parseFromBuffer.iterator();
                            while (it.hasNext()) {
                                XMPPropertyInfo xMPPropertyInfo = (XMPPropertyInfo) it.next();
                                String path = xMPPropertyInfo.getPath();
                                Object value = xMPPropertyInfo.getValue();
                                if (!(path == null || value == null)) {
                                    xmpDirectory.addProperty(path, value.toString());
                                }
                            }
                            return;
                        } catch (XMPException e) {
                            xmpDirectory.addError("Error parsing XMP segment: " + e.getMessage());
                            return;
                        }
                    } catch (BufferBoundsException e2) {
                        xmpDirectory.addError("Unable to read XMP data");
                        return;
                    }
                }
                xmpDirectory.addError("Xmp data segment doesn't begin with 'http://ns.adobe.com/xap/1.0/'");
            } catch (BufferBoundsException e3) {
                xmpDirectory.addError("Unable to read XMP preamble");
            }
        }
    }

    void processXmpDateTag(XMPMeta xMPMeta, XmpDirectory xmpDirectory, String str, String str2, int i) throws XMPException {
        Calendar propertyCalendar = xMPMeta.getPropertyCalendar(str, str2);
        if (propertyCalendar != null) {
            xmpDirectory.setDate(i, propertyCalendar.getTime());
        }
    }
}
