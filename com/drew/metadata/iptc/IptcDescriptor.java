package com.drew.metadata.iptc;

import com.drew.lang.StringUtil;
import com.drew.metadata.TagDescriptor;
import com.schibsted.scm.nextgenapp.C1061R;
import com.urbanairship.C1608R;

public class IptcDescriptor extends TagDescriptor<IptcDirectory> {
    public IptcDescriptor(IptcDirectory iptcDirectory) {
        super(iptcDirectory);
    }

    public String getDescription(int i) {
        switch (i) {
            case 276:
                return getFileFormatDescription();
            case 537:
                return getKeywordsDescription();
            default:
                return super.getDescription(i);
        }
    }

    public String getFileFormatDescription() {
        Integer integer = ((IptcDirectory) this._directory).getInteger(276);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "No ObjectData";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "IPTC-NAA Digital Newsphoto Parameter Record";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "IPTC7901 Recommended Message Format";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Tagged Image File Format (Adobe/Aldus Image data)";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Illustrator (Adobe Graphics data)";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "AppleSingle (Apple Computer Inc)";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "NAA 89-3 (ANPA 1312)";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "MacBinary II";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "IPTC Unstructured Character Oriented File Format (UCOFF)";
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return "United Press International ANPA 1312 variant";
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return "United Press International Down-Load Message";
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                return "JPEG File Interchange (JFIF)";
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                return "Photo-CD Image-Pac (Eastman Kodak)";
            case C1608R.styleable.MapAttrs_useViewLifecycle /*13*/:
                return "Bit Mapped Graphics File [.BMP] (Microsoft)";
            case C1608R.styleable.MapAttrs_zOrderOnTop /*14*/:
                return "Digital Audio File [.WAV] (Microsoft & Creative Labs)";
            case C1608R.styleable.MapAttrs_uiMapToolbar /*15*/:
                return "Audio plus Moving Video [.AVI] (Microsoft)";
            case C1061R.styleable.Toolbar_maxButtonHeight /*16*/:
                return "PC DOS/Windows Executable Files [.COM][.EXE]";
            case C1061R.styleable.Toolbar_collapseIcon /*17*/:
                return "Compressed Binary File [.ZIP] (PKWare Inc)";
            case C1061R.styleable.Toolbar_collapseContentDescription /*18*/:
                return "Audio Interchange File Format AIFF (Apple Computer Inc)";
            case C1061R.styleable.Toolbar_navigationIcon /*19*/:
                return "RIFF Wave (Microsoft Corporation)";
            case C1061R.styleable.Toolbar_navigationContentDescription /*20*/:
                return "Freehand (Macromedia/Aldus)";
            case C1061R.styleable.Theme_actionBarTheme /*21*/:
                return "Hypertext Markup Language [.HTML] (The Internet Society)";
            case C1061R.styleable.Theme_actionBarWidgetTheme /*22*/:
                return "MPEG 2 Audio Layer 2 (Musicom), ISO/IEC";
            case C1061R.styleable.Theme_actionBarSize /*23*/:
                return "MPEG 2 Audio Layer 3, ISO/IEC";
            case C1061R.styleable.Theme_actionBarDivider /*24*/:
                return "Portable Document File [.PDF] Adobe";
            case C1061R.styleable.Theme_actionBarItemBackground /*25*/:
                return "News Industry Text Format (NITF)";
            case C1061R.styleable.Theme_actionMenuTextAppearance /*26*/:
                return "Tape Archive [.TAR]";
            case C1061R.styleable.Theme_actionMenuTextColor /*27*/:
                return "Tidningarnas Telegrambyra NITF version (TTNITF DTD)";
            case C1061R.styleable.Theme_actionModeStyle /*28*/:
                return "Ritzaus Bureau NITF version (RBNITF DTD)";
            case C1061R.styleable.Theme_actionModeCloseButtonStyle /*29*/:
                return "Corel Draw [.CDR]";
            default:
                return String.format("Unknown (%d)", new Object[]{integer});
        }
    }

    public String getKeywordsDescription() {
        CharSequence[] stringArray = ((IptcDirectory) this._directory).getStringArray(537);
        return stringArray == null ? null : StringUtil.join(stringArray, ";");
    }
}
