package com.drew.metadata.exif;

import com.drew.metadata.TagDescriptor;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.C1061R;
import com.urbanairship.C1608R;

public class CanonMakernoteDescriptor extends TagDescriptor<CanonMakernoteDirectory> {
    public CanonMakernoteDescriptor(CanonMakernoteDirectory canonMakernoteDirectory) {
        super(canonMakernoteDirectory);
    }

    public String getAfPointSelectedDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49427);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case 12288:
                return "None (MF)";
            case 12289:
                return "Auto selected";
            case 12290:
                return "Right";
            case 12291:
                return "Centre";
            case 12292:
                return "Left";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getAfPointUsedDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49678);
        return integer == null ? null : (integer.intValue() & 7) == 0 ? "Right" : (integer.intValue() & 7) == 1 ? "Centre" : (integer.intValue() & 7) == 2 ? "Left" : "Unknown (" + integer + ")";
    }

    public String getContinuousDriveModeDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49413);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                integer = ((CanonMakernoteDirectory) this._directory).getInteger(49410);
                if (integer != null) {
                    return integer.intValue() == 0 ? "Single shot" : "Single shot with self-timer";
                }
                break;
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                break;
            default:
                return "Unknown (" + integer + ")";
        }
        return "Continuous";
    }

    public String getContrastDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49421);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "High";
            case 65535:
                return "Low";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getDescription(int i) {
        switch (i) {
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                return getSerialNumberDescription();
            case 49409:
                return getMacroModeDescription();
            case 49410:
                return getSelfTimerDelayDescription();
            case 49411:
                return getQualityDescription();
            case 49412:
                return getFlashModeDescription();
            case 49413:
                return getContinuousDriveModeDescription();
            case 49415:
                return getFocusMode1Description();
            case 49418:
                return getImageSizeDescription();
            case 49419:
                return getEasyShootingModeDescription();
            case 49420:
                return getDigitalZoomDescription();
            case 49421:
                return getContrastDescription();
            case 49422:
                return getSaturationDescription();
            case 49423:
                return getSharpnessDescription();
            case 49424:
                return getIsoDescription();
            case 49425:
                return getMeteringModeDescription();
            case 49426:
                return getFocusTypeDescription();
            case 49427:
                return getAfPointSelectedDescription();
            case 49428:
                return getExposureModeDescription();
            case 49431:
                return getLongFocalLengthDescription();
            case 49432:
                return getShortFocalLengthDescription();
            case 49433:
                return getFocalUnitsPerMillimetreDescription();
            case 49436:
                return getFlashActivityDescription();
            case 49437:
                return getFlashDetailsDescription();
            case 49440:
                return getFocusMode2Description();
            case 49671:
                return getWhiteBalanceDescription();
            case 49678:
                return getAfPointUsedDescription();
            case 49679:
                return getFlashBiasDescription();
            default:
                return super.getDescription(i);
        }
    }

    public String getDigitalZoomDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49420);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "No digital zoom";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "2x";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "4x";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getEasyShootingModeDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49419);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Full auto";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Manual";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Landscape";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Fast shutter";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Slow shutter";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Night";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "B&W";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "Sepia";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "Portrait";
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return "Sports";
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return "Macro / Closeup";
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                return "Pan focus";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getExposureModeDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49428);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Easy shooting";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Program";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Tv-priority";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Av-priority";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Manual";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "A-DEP";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getFlashActivityDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49436);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Flash did not fire";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Flash fired";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getFlashBiasDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49679);
        if (integer == null) {
            return null;
        }
        Object obj = null;
        if (integer.intValue() > 61440) {
            obj = 1;
            integer = Integer.valueOf(Integer.valueOf(65535 - integer.intValue()).intValue() + 1);
        }
        return (obj != null ? "-" : BuildConfig.VERSION_NAME) + Float.toString(((float) integer.intValue()) / 32.0f) + " EV";
    }

    public String getFlashDetailsDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49437);
        return integer == null ? null : ((integer.intValue() >> 14) & 1) > 0 ? "External E-TTL" : ((integer.intValue() >> 13) & 1) > 0 ? "Internal flash" : ((integer.intValue() >> 11) & 1) > 0 ? "FP sync used" : ((integer.intValue() >> 4) & 1) > 0 ? "FP sync enabled" : "Unknown (" + integer + ")";
    }

    public String getFlashModeDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49412);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "No flash fired";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Auto";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "On";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Red-eye reduction";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Slow-synchro";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Auto and red-eye reduction";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "On and red-eye reduction";
            case C1061R.styleable.Toolbar_maxButtonHeight /*16*/:
                return "External flash";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getFocalUnitsPerMillimetreDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49433);
        return integer == null ? null : integer.intValue() != 0 ? Integer.toString(integer.intValue()) : BuildConfig.VERSION_NAME;
    }

    public String getFocusMode1Description() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49415);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "One-shot";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "AI Servo";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "AI Focus";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Manual Focus";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Single";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Continuous";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "Manual Focus";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getFocusMode2Description() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49440);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Single";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Continuous";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getFocusTypeDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49426);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Manual";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Auto";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Close-up (Macro)";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "Locked (Pan Mode)";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getImageSizeDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49418);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Large";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Medium";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Small";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getIsoDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49424);
        if (integer == null) {
            return null;
        }
        if ((16384 & integer.intValue()) > 0) {
            return BuildConfig.VERSION_NAME + (integer.intValue() & -16385);
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Not specified (see ISOSpeedRatings tag)";
            case C1608R.styleable.MapAttrs_uiMapToolbar /*15*/:
                return "Auto";
            case C1061R.styleable.Toolbar_maxButtonHeight /*16*/:
                return "50";
            case C1061R.styleable.Toolbar_collapseIcon /*17*/:
                return "100";
            case C1061R.styleable.Toolbar_collapseContentDescription /*18*/:
                return "200";
            case C1061R.styleable.Toolbar_navigationIcon /*19*/:
                return "400";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getLongFocalLengthDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49431);
        if (integer == null) {
            return null;
        }
        return Integer.toString(integer.intValue()) + " " + getFocalUnitsPerMillimetreDescription();
    }

    public String getMacroModeDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49409);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Macro";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Normal";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getMeteringModeDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49425);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Evaluative";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Partial";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Centre weighted";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getQualityDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49411);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Fine";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Superfine";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getSaturationDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49422);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "High";
            case 65535:
                return "Low";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getSelfTimerDelayDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49410);
        return integer == null ? null : integer.intValue() == 0 ? "Self timer not used" : Double.toString(((double) integer.intValue()) * 0.1d) + " sec";
    }

    public String getSerialNumberDescription() {
        if (((CanonMakernoteDirectory) this._directory).getInteger(12) == null) {
            return null;
        }
        return String.format("%04X%05d", new Object[]{Integer.valueOf((((CanonMakernoteDirectory) this._directory).getInteger(12).intValue() >> 8) & 255), Integer.valueOf(((CanonMakernoteDirectory) this._directory).getInteger(12).intValue() & 255)});
    }

    public String getSharpnessDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49423);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "High";
            case 65535:
                return "Low";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getShortFocalLengthDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49432);
        if (integer == null) {
            return null;
        }
        return Integer.toString(integer.intValue()) + " " + getFocalUnitsPerMillimetreDescription();
    }

    public String getWhiteBalanceDescription() {
        Integer integer = ((CanonMakernoteDirectory) this._directory).getInteger(49671);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Auto";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Sunny";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Cloudy";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Tungsten";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Florescent";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Flash";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "Custom";
            default:
                return "Unknown (" + integer + ")";
        }
    }
}
