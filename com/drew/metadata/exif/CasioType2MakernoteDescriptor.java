package com.drew.metadata.exif;

import com.drew.metadata.TagDescriptor;
import com.facebook.internal.Utility;
import com.schibsted.scm.nextgenapp.C1061R;
import com.urbanairship.C1608R;

public class CasioType2MakernoteDescriptor extends TagDescriptor<CasioType2MakernoteDirectory> {
    public CasioType2MakernoteDescriptor(CasioType2MakernoteDirectory casioType2MakernoteDirectory) {
        super(casioType2MakernoteDirectory);
    }

    public String getBestShotModeDescription() {
        Integer integer = ((CasioType2MakernoteDirectory) this._directory).getInteger(12295);
        if (integer == null) {
            return null;
        }
        integer.intValue();
        return "Unknown (" + integer + ")";
    }

    public String getCasioPreviewThumbnailDescription() {
        byte[] byteArray = ((CasioType2MakernoteDirectory) this._directory).getByteArray(Utility.DEFAULT_STREAM_BUFFER_SIZE);
        return byteArray == null ? null : "<" + byteArray.length + " bytes of image data>";
    }

    public String getCcdIsoSensitivityDescription() {
        Integer integer = ((CasioType2MakernoteDirectory) this._directory).getInteger(12308);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Off";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "On";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getColourModeDescription() {
        Integer integer = ((CasioType2MakernoteDirectory) this._directory).getInteger(12309);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Off";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getContrastDescription() {
        Integer integer = ((CasioType2MakernoteDirectory) this._directory).getInteger(32);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "-1";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Normal";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "+1";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getDescription(int i) {
        switch (i) {
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getThumbnailDimensionsDescription();
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getThumbnailSizeDescription();
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return getThumbnailOffsetDescription();
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return getQualityModeDescription();
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return getImageSizeDescription();
            case C1608R.styleable.MapAttrs_useViewLifecycle /*13*/:
                return getFocusMode1Description();
            case C1061R.styleable.Toolbar_navigationContentDescription /*20*/:
                return getIsoSensitivityDescription();
            case C1061R.styleable.Theme_actionBarItemBackground /*25*/:
                return getWhiteBalance1Description();
            case C1061R.styleable.Theme_actionModeCloseButtonStyle /*29*/:
                return getFocalLengthDescription();
            case C1061R.styleable.Theme_actionModeSplitBackground /*31*/:
                return getSaturationDescription();
            case C1061R.styleable.Theme_actionModeCloseDrawable /*32*/:
                return getContrastDescription();
            case C1061R.styleable.Theme_actionModeCutDrawable /*33*/:
                return getSharpnessDescription();
            case 3584:
                return getPrintImageMatchingInfoDescription();
            case Utility.DEFAULT_STREAM_BUFFER_SIZE /*8192*/:
                return getCasioPreviewThumbnailDescription();
            case 8209:
                return getWhiteBalanceBiasDescription();
            case 8210:
                return getWhiteBalance2Description();
            case 8226:
                return getObjectDistanceDescription();
            case 8244:
                return getFlashDistanceDescription();
            case 12288:
                return getRecordModeDescription();
            case 12289:
                return getSelfTimerDescription();
            case 12290:
                return getQualityDescription();
            case 12291:
                return getFocusMode2Description();
            case 12294:
                return getTimeZoneDescription();
            case 12295:
                return getBestShotModeDescription();
            case 12308:
                return getCcdIsoSensitivityDescription();
            case 12309:
                return getColourModeDescription();
            case 12310:
                return getEnhancementDescription();
            case 12311:
                return getFilterDescription();
            default:
                return super.getDescription(i);
        }
    }

    public String getEnhancementDescription() {
        Integer integer = ((CasioType2MakernoteDirectory) this._directory).getInteger(12310);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Off";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getFilterDescription() {
        Integer integer = ((CasioType2MakernoteDirectory) this._directory).getInteger(12311);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Off";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getFlashDistanceDescription() {
        Integer integer = ((CasioType2MakernoteDirectory) this._directory).getInteger(8244);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Off";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getFocalLengthDescription() {
        Double doubleObject = ((CasioType2MakernoteDirectory) this._directory).getDoubleObject(29);
        return doubleObject == null ? null : Double.toString(doubleObject.doubleValue() / 10.0d) + " mm";
    }

    public String getFocusMode1Description() {
        Integer integer = ((CasioType2MakernoteDirectory) this._directory).getInteger(13);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Macro";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getFocusMode2Description() {
        Integer integer = ((CasioType2MakernoteDirectory) this._directory).getInteger(12291);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Fixation";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "Multi-Area Focus";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getImageSizeDescription() {
        Integer integer = ((CasioType2MakernoteDirectory) this._directory).getInteger(9);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "640 x 480 pixels";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "1600 x 1200 pixels";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "2048 x 1536 pixels";
            case C1061R.styleable.Toolbar_navigationContentDescription /*20*/:
                return "2288 x 1712 pixels";
            case C1061R.styleable.Theme_actionBarTheme /*21*/:
                return "2592 x 1944 pixels";
            case C1061R.styleable.Theme_actionBarWidgetTheme /*22*/:
                return "2304 x 1728 pixels";
            case C1061R.styleable.Theme_actionModeSelectAllDrawable /*36*/:
                return "3008 x 2008 pixels";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getIsoSensitivityDescription() {
        Integer integer = ((CasioType2MakernoteDirectory) this._directory).getInteger(20);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "50";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "64";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "100";
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return "200";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getObjectDistanceDescription() {
        Integer integer = ((CasioType2MakernoteDirectory) this._directory).getInteger(8226);
        return integer == null ? null : Integer.toString(integer.intValue()) + " mm";
    }

    public String getPrintImageMatchingInfoDescription() {
        return ((CasioType2MakernoteDirectory) this._directory).getString(3584);
    }

    public String getQualityDescription() {
        Integer integer = ((CasioType2MakernoteDirectory) this._directory).getInteger(12290);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Fine";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getQualityModeDescription() {
        Integer integer = ((CasioType2MakernoteDirectory) this._directory).getInteger(8);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Fine";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Super Fine";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getRecordModeDescription() {
        Integer integer = ((CasioType2MakernoteDirectory) this._directory).getInteger(12288);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Normal";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getSaturationDescription() {
        Integer integer = ((CasioType2MakernoteDirectory) this._directory).getInteger(31);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "-1";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Normal";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "+1";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getSelfTimerDescription() {
        Integer integer = ((CasioType2MakernoteDirectory) this._directory).getInteger(12289);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Off";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getSharpnessDescription() {
        Integer integer = ((CasioType2MakernoteDirectory) this._directory).getInteger(33);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "-1";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Normal";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "+1";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getThumbnailDimensionsDescription() {
        int[] intArray = ((CasioType2MakernoteDirectory) this._directory).getIntArray(2);
        return (intArray == null || intArray.length != 2) ? ((CasioType2MakernoteDirectory) this._directory).getString(2) : intArray[0] + " x " + intArray[1] + " pixels";
    }

    public String getThumbnailOffsetDescription() {
        return ((CasioType2MakernoteDirectory) this._directory).getString(4);
    }

    public String getThumbnailSizeDescription() {
        Integer integer = ((CasioType2MakernoteDirectory) this._directory).getInteger(3);
        return integer == null ? null : Integer.toString(integer.intValue()) + " bytes";
    }

    public String getTimeZoneDescription() {
        return ((CasioType2MakernoteDirectory) this._directory).getString(12294);
    }

    public String getWhiteBalance1Description() {
        Integer integer = ((CasioType2MakernoteDirectory) this._directory).getInteger(25);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Auto";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Daylight";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Shade";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Tungsten";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Florescent";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Manual";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getWhiteBalance2Description() {
        Integer integer = ((CasioType2MakernoteDirectory) this._directory).getInteger(8210);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Manual";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Auto";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Flash";
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                return "Flash";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getWhiteBalanceBiasDescription() {
        return ((CasioType2MakernoteDirectory) this._directory).getString(8209);
    }
}
