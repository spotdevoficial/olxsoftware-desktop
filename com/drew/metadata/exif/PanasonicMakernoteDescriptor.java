package com.drew.metadata.exif;

import com.drew.lang.BufferBoundsException;
import com.drew.lang.BufferReader;
import com.drew.lang.ByteArrayReader;
import com.drew.metadata.Age;
import com.drew.metadata.Face;
import com.drew.metadata.TagDescriptor;
import com.schibsted.scm.nextgenapp.C1061R;
import com.urbanairship.C1608R;
import java.io.UnsupportedEncodingException;

public class PanasonicMakernoteDescriptor extends TagDescriptor<PanasonicMakernoteDirectory> {
    public PanasonicMakernoteDescriptor(PanasonicMakernoteDirectory panasonicMakernoteDirectory) {
        super(panasonicMakernoteDirectory);
    }

    private String buildFacesDescription(Face[] faceArr) {
        if (faceArr == null) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < faceArr.length; i++) {
            stringBuilder.append("Face ").append(i + 1).append(": ").append(faceArr[i].toString()).append("\n");
        }
        return stringBuilder.length() > 0 ? stringBuilder.substring(0, stringBuilder.length() - 1) : null;
    }

    private String getOnOffDescription(int i) {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(i);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Off";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "On";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    private String getTextDescription(int i) {
        byte[] byteArray = ((PanasonicMakernoteDirectory) this._directory).getByteArray(i);
        if (byteArray == null) {
            return null;
        }
        try {
            return new String(byteArray, "ASCII").trim();
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    private String getTransformDescription(int i) {
        byte[] byteArray = ((PanasonicMakernoteDirectory) this._directory).getByteArray(i);
        if (byteArray == null) {
            return null;
        }
        BufferReader byteArrayReader = new ByteArrayReader(byteArray);
        try {
            int uInt16 = byteArrayReader.getUInt16(0);
            int uInt162 = byteArrayReader.getUInt16(2);
            return (uInt16 == -1 && uInt162 == 1) ? "Slim Low" : (uInt16 == -3 && uInt162 == 2) ? "Slim High" : (uInt16 == 0 && uInt162 == 0) ? "Off" : (uInt16 == 1 && uInt162 == 1) ? "Stretch Low" : (uInt16 == 3 && uInt162 == 2) ? "Stretch High" : "Unknown (" + uInt16 + " " + uInt162 + ")";
        } catch (BufferBoundsException e) {
            return null;
        }
    }

    public String getAdvancedSceneModeDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(61);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Normal";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Outdoor/Illuminations/Flower/HDR Art";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Indoor/Architecture/Objects/HDR B&W";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Creative";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Auto";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "Expressive";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "Retro";
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return "Pure";
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return "Elegant";
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                return "Monochrome";
            case C1608R.styleable.MapAttrs_useViewLifecycle /*13*/:
                return "Dynamic Art";
            case C1608R.styleable.MapAttrs_zOrderOnTop /*14*/:
                return "Silhouette";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getAfAreaModeDescription() {
        int[] intArray = ((PanasonicMakernoteDirectory) this._directory).getIntArray(15);
        if (intArray == null || intArray.length < 2) {
            return null;
        }
        switch (intArray[0]) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                switch (intArray[1]) {
                    case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                        return "Spot Mode On";
                    case C1061R.styleable.Toolbar_maxButtonHeight /*16*/:
                        return "Spot Mode Off";
                    default:
                        return "Unknown (" + intArray[0] + " " + intArray[1] + ")";
                }
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                switch (intArray[1]) {
                    case C1608R.styleable.MapAttrs_mapType /*0*/:
                        return "Spot Focusing";
                    case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                        return "5-area";
                    default:
                        return "Unknown (" + intArray[0] + " " + intArray[1] + ")";
                }
            case C1061R.styleable.Toolbar_maxButtonHeight /*16*/:
                switch (intArray[1]) {
                    case C1608R.styleable.MapAttrs_mapType /*0*/:
                        return "1-area";
                    case C1061R.styleable.Toolbar_maxButtonHeight /*16*/:
                        return "1-area (high speed)";
                    default:
                        return "Unknown (" + intArray[0] + " " + intArray[1] + ")";
                }
            case C1061R.styleable.Theme_actionModeCloseDrawable /*32*/:
                switch (intArray[1]) {
                    case C1608R.styleable.MapAttrs_mapType /*0*/:
                        return "Auto or Face Detect";
                    case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                        return "3-area (left)";
                    case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                        return "3-area (center)";
                    case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                        return "3-area (right)";
                    default:
                        return "Unknown (" + intArray[0] + " " + intArray[1] + ")";
                }
            case C1061R.styleable.Theme_editTextBackground /*64*/:
                return "Face Detect";
            default:
                return "Unknown (" + intArray[0] + " " + intArray[1] + ")";
        }
    }

    public String getAfAssistLampDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(49);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Fired";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Enabled but not used";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Disabled but required";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Disabled and not required";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getAudioDescription() {
        return getOnOffDescription(32);
    }

    public String getBabyAge1Description() {
        Age age = ((PanasonicMakernoteDirectory) this._directory).getAge(32784);
        return age == null ? null : age.toFriendlyString();
    }

    public String getBabyAgeDescription() {
        Age age = ((PanasonicMakernoteDirectory) this._directory).getAge(51);
        return age == null ? null : age.toFriendlyString();
    }

    public String getBabyNameDescription() {
        return getTextDescription(C1061R.styleable.Theme_checkedTextViewStyle);
    }

    public String getBurstModeDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(42);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Off";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "On";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Infinite";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Unlimited";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getCityDescription() {
        return getTextDescription(109);
    }

    public String getColorEffectDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(40);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Off";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Warm";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Cool";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Black & White";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Sepia";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getColorModeDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(50);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Natural";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Vivid";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getContrastDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(57);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Normal";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getContrastModeDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(44);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Low";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "High";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "Medium Low";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "Medium High";
            case 256:
                return "Low";
            case 272:
                return "Normal";
            case 288:
                return "High";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getConversionLensDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(53);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Off";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Wide";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Telephoto";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Macro";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getCountryDescription() {
        return getTextDescription(C1061R.styleable.Theme_ratingBarStyle);
    }

    public String getDescription(int i) {
        switch (i) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getQualityModeDescription();
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getVersionDescription();
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getWhiteBalanceDescription();
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return getFocusModeDescription();
            case C1608R.styleable.MapAttrs_uiMapToolbar /*15*/:
                return getAfAreaModeDescription();
            case C1061R.styleable.Theme_actionMenuTextAppearance /*26*/:
                return getImageStabilizationDescription();
            case C1061R.styleable.Theme_actionModeStyle /*28*/:
                return getMacroModeDescription();
            case C1061R.styleable.Theme_actionModeSplitBackground /*31*/:
                return getRecordModeDescription();
            case C1061R.styleable.Theme_actionModeCloseDrawable /*32*/:
                return getAudioDescription();
            case C1061R.styleable.Theme_actionModeCutDrawable /*33*/:
                return getUnknownDataDumpDescription();
            case C1061R.styleable.Theme_actionModeShareDrawable /*37*/:
                return getInternalSerialNumberDescription();
            case C1061R.styleable.Theme_actionModeFindDrawable /*38*/:
                return getExifVersionDescription();
            case C1061R.styleable.Theme_actionModePopupWindowStyle /*40*/:
                return getColorEffectDescription();
            case C1061R.styleable.Theme_textAppearanceLargePopupMenu /*41*/:
                return getUptimeDescription();
            case C1061R.styleable.Theme_textAppearanceSmallPopupMenu /*42*/:
                return getBurstModeDescription();
            case C1061R.styleable.Theme_dialogPreferredPadding /*44*/:
                return getContrastModeDescription();
            case C1061R.styleable.Theme_listDividerAlertDialog /*45*/:
                return getNoiseReductionDescription();
            case C1061R.styleable.Theme_actionDropDownStyle /*46*/:
                return getSelfTimerDescription();
            case C1061R.styleable.Theme_spinnerDropDownItemStyle /*48*/:
                return getRotationDescription();
            case C1061R.styleable.Theme_homeAsUpIndicator /*49*/:
                return getAfAssistLampDescription();
            case C1061R.styleable.Theme_actionButtonStyle /*50*/:
                return getColorModeDescription();
            case C1061R.styleable.Theme_buttonBarStyle /*51*/:
                return getBabyAgeDescription();
            case C1061R.styleable.Theme_buttonBarButtonStyle /*52*/:
                return getOpticalZoomModeDescription();
            case C1061R.styleable.Theme_selectableItemBackground /*53*/:
                return getConversionLensDescription();
            case C1061R.styleable.Theme_dividerHorizontal /*57*/:
                return getContrastDescription();
            case C1061R.styleable.Theme_activityChooserViewStyle /*58*/:
                return getWorldTimeLocationDescription();
            case C1061R.styleable.Theme_toolbarStyle /*59*/:
                return getTextStampDescription();
            case C1061R.styleable.Theme_popupMenuStyle /*61*/:
                return getAdvancedSceneModeDescription();
            case C1061R.styleable.Theme_popupWindowStyle /*62*/:
                return getTextStamp1Description();
            case C1061R.styleable.Theme_panelBackground /*78*/:
                return getDetectedFacesDescription();
            case C1061R.styleable.Theme_colorSwitchThumbNormal /*89*/:
                return getTransformDescription();
            case C1061R.styleable.Theme_alertDialogTheme /*93*/:
                return getIntelligentExposureDescription();
            case C1061R.styleable.Theme_buttonBarNeutralButtonStyle /*97*/:
                return getRecognizedFacesDescription();
            case C1061R.styleable.Theme_autoCompleteTextViewStyle /*98*/:
                return getFlashWarningDescription();
            case C1061R.styleable.Theme_checkboxStyle /*101*/:
                return getTitleDescription();
            case C1061R.styleable.Theme_checkedTextViewStyle /*102*/:
                return getBabyNameDescription();
            case C1061R.styleable.Theme_editTextStyle /*103*/:
                return getLocationDescription();
            case C1061R.styleable.Theme_ratingBarStyle /*105*/:
                return getCountryDescription();
            case C1061R.styleable.Theme_switchStyle /*107*/:
                return getStateDescription();
            case 109:
                return getCityDescription();
            case 111:
                return getLandmarkDescription();
            case 112:
                return getIntelligentResolutionDescription();
            case 3584:
                return getPrintImageMatchingInfoDescription();
            case 32768:
                return getMakernoteVersionDescription();
            case 32769:
                return getSceneModeDescription();
            case 32775:
                return getFlashFiredDescription();
            case 32776:
                return getTextStamp2Description();
            case 32777:
                return getTextStamp3Description();
            case 32784:
                return getBabyAge1Description();
            case 32786:
                return getTransform1Description();
            default:
                return super.getDescription(i);
        }
    }

    public String getDetectedFacesDescription() {
        return buildFacesDescription(((PanasonicMakernoteDirectory) this._directory).getDetectedFaces());
    }

    public String getExifVersionDescription() {
        return TagDescriptor.convertBytesToVersionString(((PanasonicMakernoteDirectory) this._directory).getIntArray(38), 2);
    }

    public String getFlashFiredDescription() {
        return getOnOffDescription(32775);
    }

    public String getFlashWarningDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(98);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "No";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Yes (Flash required but disabled)";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getFocusModeDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(7);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Auto";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Manual";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Auto, Focus Button";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Auto, Continuous";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getImageStabilizationDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(26);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "On, Mode 1";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Off";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "On, Mode 2";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getIntelligentExposureDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(93);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Off";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Low";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Standard";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "High";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getIntelligentResolutionDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(112);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Off";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Auto";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "On";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getInternalSerialNumberDescription() {
        /*
        r6 = this;
        r1 = 0;
        r0 = r6._directory;
        r0 = (com.drew.metadata.exif.PanasonicMakernoteDirectory) r0;
        r2 = 37;
        r3 = r0.getByteArray(r2);
        if (r3 != 0) goto L_0x000f;
    L_0x000d:
        r0 = 0;
    L_0x000e:
        return r0;
    L_0x000f:
        r2 = r3.length;
        r0 = r1;
    L_0x0011:
        r4 = r3.length;
        if (r0 >= r4) goto L_0x0028;
    L_0x0014:
        r4 = r3[r0];
        r4 = r4 & 255;
        if (r4 == 0) goto L_0x001e;
    L_0x001a:
        r5 = 127; // 0x7f float:1.78E-43 double:6.27E-322;
        if (r4 <= r5) goto L_0x0025;
    L_0x001e:
        r2 = new java.lang.String;
        r2.<init>(r3, r1, r0);
        r0 = r2;
        goto L_0x000e;
    L_0x0025:
        r0 = r0 + 1;
        goto L_0x0011;
    L_0x0028:
        r0 = r2;
        goto L_0x001e;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.drew.metadata.exif.PanasonicMakernoteDescriptor.getInternalSerialNumberDescription():java.lang.String");
    }

    public String getLandmarkDescription() {
        return getTextDescription(111);
    }

    public String getLocationDescription() {
        return getTextDescription(C1061R.styleable.Theme_editTextStyle);
    }

    public String getMacroModeDescription() {
        return getOnOffDescription(28);
    }

    public String getMakernoteVersionDescription() {
        return TagDescriptor.convertBytesToVersionString(((PanasonicMakernoteDirectory) this._directory).getIntArray(32768), 2);
    }

    public String getNoiseReductionDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(45);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Standard (0)";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Low (-1)";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "High (+1)";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Lowest (-2)";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Highest (+2)";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getOpticalZoomModeDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(52);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Standard";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Extended";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getPrintImageMatchingInfoDescription() {
        byte[] byteArray = ((PanasonicMakernoteDirectory) this._directory).getByteArray(3584);
        return byteArray == null ? null : "(" + byteArray.length + " bytes)";
    }

    public String getQualityModeDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(1);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "High";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "Very High";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "Raw";
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return "Motion Picture";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getRecognizedFacesDescription() {
        return buildFacesDescription(((PanasonicMakernoteDirectory) this._directory).getRecognizedFaces());
    }

    public String getRecordModeDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(31);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Normal";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Portrait";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Scenery";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Sports";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Night Portrait";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "Program";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "Aperture Priority";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "Shutter Priority";
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return "Macro";
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return "Spot";
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                return "Manual";
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                return "Movie Preview";
            case C1608R.styleable.MapAttrs_useViewLifecycle /*13*/:
                return "Panning";
            case C1608R.styleable.MapAttrs_zOrderOnTop /*14*/:
                return "Simple";
            case C1608R.styleable.MapAttrs_uiMapToolbar /*15*/:
                return "Color Effects";
            case C1061R.styleable.Toolbar_maxButtonHeight /*16*/:
                return "Self Portrait";
            case C1061R.styleable.Toolbar_collapseIcon /*17*/:
                return "Economy";
            case C1061R.styleable.Toolbar_collapseContentDescription /*18*/:
                return "Fireworks";
            case C1061R.styleable.Toolbar_navigationIcon /*19*/:
                return "Party";
            case C1061R.styleable.Toolbar_navigationContentDescription /*20*/:
                return "Snow";
            case C1061R.styleable.Theme_actionBarTheme /*21*/:
                return "Night Scenery";
            case C1061R.styleable.Theme_actionBarWidgetTheme /*22*/:
                return "Food";
            case C1061R.styleable.Theme_actionBarSize /*23*/:
                return "Baby";
            case C1061R.styleable.Theme_actionBarDivider /*24*/:
                return "Soft Skin";
            case C1061R.styleable.Theme_actionBarItemBackground /*25*/:
                return "Candlelight";
            case C1061R.styleable.Theme_actionMenuTextAppearance /*26*/:
                return "Starry Night";
            case C1061R.styleable.Theme_actionMenuTextColor /*27*/:
                return "High Sensitivity";
            case C1061R.styleable.Theme_actionModeStyle /*28*/:
                return "Panorama Assist";
            case C1061R.styleable.Theme_actionModeCloseButtonStyle /*29*/:
                return "Underwater";
            case C1061R.styleable.Theme_actionModeBackground /*30*/:
                return "Beach";
            case C1061R.styleable.Theme_actionModeSplitBackground /*31*/:
                return "Aerial Photo";
            case C1061R.styleable.Theme_actionModeCloseDrawable /*32*/:
                return "Sunset";
            case C1061R.styleable.Theme_actionModeCutDrawable /*33*/:
                return "Pet";
            case C1061R.styleable.Theme_actionModeCopyDrawable /*34*/:
                return "Intelligent ISO";
            case C1061R.styleable.Theme_actionModePasteDrawable /*35*/:
                return "Clipboard";
            case C1061R.styleable.Theme_actionModeSelectAllDrawable /*36*/:
                return "High Speed Continuous Shooting";
            case C1061R.styleable.Theme_actionModeShareDrawable /*37*/:
                return "Intelligent Auto";
            case C1061R.styleable.Theme_actionModeWebSearchDrawable /*39*/:
                return "Multi-aspect";
            case C1061R.styleable.Theme_textAppearanceLargePopupMenu /*41*/:
                return "Transform";
            case C1061R.styleable.Theme_textAppearanceSmallPopupMenu /*42*/:
                return "Flash Burst";
            case C1061R.styleable.Theme_dialogTheme /*43*/:
                return "Pin Hole";
            case C1061R.styleable.Theme_dialogPreferredPadding /*44*/:
                return "Film Grain";
            case C1061R.styleable.Theme_listDividerAlertDialog /*45*/:
                return "My Color";
            case C1061R.styleable.Theme_actionDropDownStyle /*46*/:
                return "Photo Frame";
            case C1061R.styleable.Theme_buttonBarStyle /*51*/:
                return "HDR";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getRotationDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(48);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Horizontal";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Rotate 180";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "Rotate 90 CW";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "Rotate 270 CW";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getSceneModeDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(32769);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Normal";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Portrait";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Scenery";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Sports";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Night Portrait";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "Program";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "Aperture Priority";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "Shutter Priority";
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return "Macro";
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return "Spot";
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                return "Manual";
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                return "Movie Preview";
            case C1608R.styleable.MapAttrs_useViewLifecycle /*13*/:
                return "Panning";
            case C1608R.styleable.MapAttrs_zOrderOnTop /*14*/:
                return "Simple";
            case C1608R.styleable.MapAttrs_uiMapToolbar /*15*/:
                return "Color Effects";
            case C1061R.styleable.Toolbar_maxButtonHeight /*16*/:
                return "Self Portrait";
            case C1061R.styleable.Toolbar_collapseIcon /*17*/:
                return "Economy";
            case C1061R.styleable.Toolbar_collapseContentDescription /*18*/:
                return "Fireworks";
            case C1061R.styleable.Toolbar_navigationIcon /*19*/:
                return "Party";
            case C1061R.styleable.Toolbar_navigationContentDescription /*20*/:
                return "Snow";
            case C1061R.styleable.Theme_actionBarTheme /*21*/:
                return "Night Scenery";
            case C1061R.styleable.Theme_actionBarWidgetTheme /*22*/:
                return "Food";
            case C1061R.styleable.Theme_actionBarSize /*23*/:
                return "Baby";
            case C1061R.styleable.Theme_actionBarDivider /*24*/:
                return "Soft Skin";
            case C1061R.styleable.Theme_actionBarItemBackground /*25*/:
                return "Candlelight";
            case C1061R.styleable.Theme_actionMenuTextAppearance /*26*/:
                return "Starry Night";
            case C1061R.styleable.Theme_actionMenuTextColor /*27*/:
                return "High Sensitivity";
            case C1061R.styleable.Theme_actionModeStyle /*28*/:
                return "Panorama Assist";
            case C1061R.styleable.Theme_actionModeCloseButtonStyle /*29*/:
                return "Underwater";
            case C1061R.styleable.Theme_actionModeBackground /*30*/:
                return "Beach";
            case C1061R.styleable.Theme_actionModeSplitBackground /*31*/:
                return "Aerial Photo";
            case C1061R.styleable.Theme_actionModeCloseDrawable /*32*/:
                return "Sunset";
            case C1061R.styleable.Theme_actionModeCutDrawable /*33*/:
                return "Pet";
            case C1061R.styleable.Theme_actionModeCopyDrawable /*34*/:
                return "Intelligent ISO";
            case C1061R.styleable.Theme_actionModePasteDrawable /*35*/:
                return "Clipboard";
            case C1061R.styleable.Theme_actionModeSelectAllDrawable /*36*/:
                return "High Speed Continuous Shooting";
            case C1061R.styleable.Theme_actionModeShareDrawable /*37*/:
                return "Intelligent Auto";
            case C1061R.styleable.Theme_actionModeWebSearchDrawable /*39*/:
                return "Multi-aspect";
            case C1061R.styleable.Theme_textAppearanceLargePopupMenu /*41*/:
                return "Transform";
            case C1061R.styleable.Theme_textAppearanceSmallPopupMenu /*42*/:
                return "Flash Burst";
            case C1061R.styleable.Theme_dialogTheme /*43*/:
                return "Pin Hole";
            case C1061R.styleable.Theme_dialogPreferredPadding /*44*/:
                return "Film Grain";
            case C1061R.styleable.Theme_listDividerAlertDialog /*45*/:
                return "My Color";
            case C1061R.styleable.Theme_actionDropDownStyle /*46*/:
                return "Photo Frame";
            case C1061R.styleable.Theme_buttonBarStyle /*51*/:
                return "HDR";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getSelfTimerDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(46);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Off";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "10 s";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "2 s";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getStateDescription() {
        return getTextDescription(C1061R.styleable.Theme_switchStyle);
    }

    public String getTextStamp1Description() {
        return getOnOffDescription(62);
    }

    public String getTextStamp2Description() {
        return getOnOffDescription(32776);
    }

    public String getTextStamp3Description() {
        return getOnOffDescription(32777);
    }

    public String getTextStampDescription() {
        return getOnOffDescription(59);
    }

    public String getTitleDescription() {
        return getTextDescription(C1061R.styleable.Theme_checkboxStyle);
    }

    public String getTransform1Description() {
        return getTransformDescription(32786);
    }

    public String getTransformDescription() {
        return getTransformDescription(89);
    }

    public String getUnknownDataDumpDescription() {
        byte[] byteArray = ((PanasonicMakernoteDirectory) this._directory).getByteArray(33);
        return byteArray == null ? null : "[" + byteArray.length + " bytes]";
    }

    public String getUptimeDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(41);
        return integer == null ? null : (((float) integer.intValue()) / 100.0f) + " s";
    }

    public String getVersionDescription() {
        return TagDescriptor.convertBytesToVersionString(((PanasonicMakernoteDirectory) this._directory).getIntArray(2), 2);
    }

    public String getWhiteBalanceDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(3);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Auto";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Daylight";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Cloudy";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Incandescent";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Manual";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "Flash";
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return "Black & White";
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                return "Manual";
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                return "Shade";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getWorldTimeLocationDescription() {
        Integer integer = ((PanasonicMakernoteDirectory) this._directory).getInteger(58);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Home";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Destination";
            default:
                return "Unknown (" + integer + ")";
        }
    }
}
