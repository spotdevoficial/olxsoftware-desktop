package com.drew.metadata.exif;

import com.drew.metadata.Directory;
import java.util.HashMap;

public class PentaxMakernoteDirectory extends Directory {
    protected static final HashMap<Integer, String> _tagNameMap;

    static {
        _tagNameMap = new HashMap();
        _tagNameMap.put(Integer.valueOf(1), "Capture Mode");
        _tagNameMap.put(Integer.valueOf(2), "Quality Level");
        _tagNameMap.put(Integer.valueOf(3), "Focus Mode");
        _tagNameMap.put(Integer.valueOf(4), "Flash Mode");
        _tagNameMap.put(Integer.valueOf(7), "White Balance");
        _tagNameMap.put(Integer.valueOf(10), "Digital Zoom");
        _tagNameMap.put(Integer.valueOf(11), "Sharpness");
        _tagNameMap.put(Integer.valueOf(12), "Contrast");
        _tagNameMap.put(Integer.valueOf(13), "Saturation");
        _tagNameMap.put(Integer.valueOf(20), "ISO Speed");
        _tagNameMap.put(Integer.valueOf(23), "Colour");
        _tagNameMap.put(Integer.valueOf(3584), "Print Image Matching (PIM) Info");
        _tagNameMap.put(Integer.valueOf(4096), "Time Zone");
        _tagNameMap.put(Integer.valueOf(4097), "Daylight Savings");
    }

    public PentaxMakernoteDirectory() {
        setDescriptor(new PentaxMakernoteDescriptor(this));
    }

    public String getName() {
        return "Pentax Makernote";
    }

    protected HashMap<Integer, String> getTagNameMap() {
        return _tagNameMap;
    }
}
