package com.drew.metadata.exif;

import com.drew.metadata.Directory;
import com.facebook.internal.Utility;
import java.util.HashMap;

public class CasioType2MakernoteDirectory extends Directory {
    protected static final HashMap<Integer, String> _tagNameMap;

    static {
        _tagNameMap = new HashMap();
        _tagNameMap.put(Integer.valueOf(2), "Thumbnail Dimensions");
        _tagNameMap.put(Integer.valueOf(3), "Thumbnail Size");
        _tagNameMap.put(Integer.valueOf(4), "Thumbnail Offset");
        _tagNameMap.put(Integer.valueOf(8), "Quality Mode");
        _tagNameMap.put(Integer.valueOf(9), "Image Size");
        _tagNameMap.put(Integer.valueOf(13), "Focus Mode");
        _tagNameMap.put(Integer.valueOf(20), "ISO Sensitivity");
        _tagNameMap.put(Integer.valueOf(25), "White Balance");
        _tagNameMap.put(Integer.valueOf(29), "Focal Length");
        _tagNameMap.put(Integer.valueOf(31), "Saturation");
        _tagNameMap.put(Integer.valueOf(32), "Contrast");
        _tagNameMap.put(Integer.valueOf(33), "Sharpness");
        _tagNameMap.put(Integer.valueOf(3584), "Print Image Matching (PIM) Info");
        _tagNameMap.put(Integer.valueOf(Utility.DEFAULT_STREAM_BUFFER_SIZE), "Casio Preview Thumbnail");
        _tagNameMap.put(Integer.valueOf(8209), "White Balance Bias");
        _tagNameMap.put(Integer.valueOf(8210), "White Balance");
        _tagNameMap.put(Integer.valueOf(8226), "Object Distance");
        _tagNameMap.put(Integer.valueOf(8244), "Flash Distance");
        _tagNameMap.put(Integer.valueOf(12288), "Record Mode");
        _tagNameMap.put(Integer.valueOf(12289), "Self Timer");
        _tagNameMap.put(Integer.valueOf(12290), "Quality");
        _tagNameMap.put(Integer.valueOf(12291), "Focus Mode");
        _tagNameMap.put(Integer.valueOf(12294), "Time Zone");
        _tagNameMap.put(Integer.valueOf(12295), "BestShot Mode");
        _tagNameMap.put(Integer.valueOf(12308), "CCD ISO Sensitivity");
        _tagNameMap.put(Integer.valueOf(12309), "Colour Mode");
        _tagNameMap.put(Integer.valueOf(12310), "Enhancement");
        _tagNameMap.put(Integer.valueOf(12311), "Filter");
    }

    public CasioType2MakernoteDirectory() {
        setDescriptor(new CasioType2MakernoteDescriptor(this));
    }

    public String getName() {
        return "Casio Makernote";
    }

    protected HashMap<Integer, String> getTagNameMap() {
        return _tagNameMap;
    }
}
