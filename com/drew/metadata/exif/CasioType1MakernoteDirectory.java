package com.drew.metadata.exif;

import com.drew.metadata.Directory;
import java.util.HashMap;

public class CasioType1MakernoteDirectory extends Directory {
    protected static final HashMap<Integer, String> _tagNameMap;

    static {
        _tagNameMap = new HashMap();
        _tagNameMap.put(Integer.valueOf(20), "CCD Sensitivity");
        _tagNameMap.put(Integer.valueOf(12), "Contrast");
        _tagNameMap.put(Integer.valueOf(10), "Digital Zoom");
        _tagNameMap.put(Integer.valueOf(5), "Flash Intensity");
        _tagNameMap.put(Integer.valueOf(4), "Flash Mode");
        _tagNameMap.put(Integer.valueOf(3), "Focusing Mode");
        _tagNameMap.put(Integer.valueOf(6), "Object Distance");
        _tagNameMap.put(Integer.valueOf(2), "Quality");
        _tagNameMap.put(Integer.valueOf(1), "Recording Mode");
        _tagNameMap.put(Integer.valueOf(13), "Saturation");
        _tagNameMap.put(Integer.valueOf(11), "Sharpness");
        _tagNameMap.put(Integer.valueOf(8), "Makernote Unknown 1");
        _tagNameMap.put(Integer.valueOf(9), "Makernote Unknown 2");
        _tagNameMap.put(Integer.valueOf(14), "Makernote Unknown 3");
        _tagNameMap.put(Integer.valueOf(15), "Makernote Unknown 4");
        _tagNameMap.put(Integer.valueOf(16), "Makernote Unknown 5");
        _tagNameMap.put(Integer.valueOf(17), "Makernote Unknown 6");
        _tagNameMap.put(Integer.valueOf(18), "Makernote Unknown 7");
        _tagNameMap.put(Integer.valueOf(19), "Makernote Unknown 8");
        _tagNameMap.put(Integer.valueOf(7), "White Balance");
    }

    public CasioType1MakernoteDirectory() {
        setDescriptor(new CasioType1MakernoteDescriptor(this));
    }

    public String getName() {
        return "Casio Makernote";
    }

    protected HashMap<Integer, String> getTagNameMap() {
        return _tagNameMap;
    }
}
