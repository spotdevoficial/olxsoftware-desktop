package com.drew.metadata.exif;

import com.drew.metadata.TagDescriptor;
import com.schibsted.scm.nextgenapp.C1061R;
import com.urbanairship.C1608R;

public class SonyType1MakernoteDescriptor extends TagDescriptor<SonyType1MakernoteDirectory> {
    public SonyType1MakernoteDescriptor(SonyType1MakernoteDirectory sonyType1MakernoteDirectory) {
        super(sonyType1MakernoteDirectory);
    }

    public String getAntiBlurDescription() {
        Integer integer = ((SonyType1MakernoteDirectory) this._directory).getInteger(45131);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Off";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "On (Continuous)";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "On (Shooting)";
            case 65535:
                return "N/A";
            default:
                return String.format("Unknown (%d)", new Object[]{integer});
        }
    }

    public String getColorModeDescription() {
        Integer integer = ((SonyType1MakernoteDirectory) this._directory).getInteger(45097);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Standard";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Vivid";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Portrait";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Landscape";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Sunset";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Night Portrait";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "Black & White";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "Adobe RGB";
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
            case C1061R.styleable.Theme_buttonStyleSmall /*100*/:
                return "Neutral";
            case C1061R.styleable.Theme_checkboxStyle /*101*/:
                return "Clear";
            case C1061R.styleable.Theme_checkedTextViewStyle /*102*/:
                return "Deep";
            case C1061R.styleable.Theme_editTextStyle /*103*/:
                return "Light";
            case C1061R.styleable.Theme_radioButtonStyle /*104*/:
                return "Night View";
            case C1061R.styleable.Theme_ratingBarStyle /*105*/:
                return "Autumn Leaves";
            default:
                return String.format("Unknown (%d)", new Object[]{integer});
        }
    }

    public String getColorTemperatureDescription() {
        Integer integer = ((SonyType1MakernoteDirectory) this._directory).getInteger(45089);
        if (integer == null) {
            return null;
        }
        if (integer.intValue() == 0) {
            return "Auto";
        }
        int intValue = ((integer.intValue() & -16777216) >> 24) | ((integer.intValue() & 16711680) >> 8);
        return String.format("%d K", new Object[]{Integer.valueOf(intValue)});
    }

    public String getDescription(int i) {
        switch (i) {
            case 45089:
                return getColorTemperatureDescription();
            case 45091:
                return getSceneModeDescription();
            case 45092:
                return getZoneMatchingDescription();
            case 45093:
                return getDynamicRangeOptimizerDescription();
            case 45094:
                return getImageStabilizationDescription();
            case 45097:
                return getColorModeDescription();
            case 45120:
                return getMacroDescription();
            case 45121:
                return getExposureModeDescription();
            case 45127:
                return getQualityDescription();
            case 45131:
                return getAntiBlurDescription();
            case 45134:
                return getLongExposureNoiseReductionDescription();
            default:
                return super.getDescription(i);
        }
    }

    public String getDynamicRangeOptimizerDescription() {
        Integer integer = ((SonyType1MakernoteDirectory) this._directory).getInteger(45093);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Off";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Standard";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Advanced Auto";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "Advanced LV1";
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return "Advanced LV2";
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return "Advanced LV3";
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                return "Advanced LV4";
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                return "Advanced LV5";
            default:
                return String.format("Unknown (%d)", new Object[]{integer});
        }
    }

    public String getExposureModeDescription() {
        Integer integer = ((SonyType1MakernoteDirectory) this._directory).getInteger(45121);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Auto";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Landscape";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "Program";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "Aperture Priority";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "Shutter Priority";
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return "Night Scene";
            case C1608R.styleable.MapAttrs_uiMapToolbar /*15*/:
                return "Manual";
            case 65535:
                return "N/A";
            default:
                return String.format("Unknown (%d)", new Object[]{integer});
        }
    }

    public String getImageStabilizationDescription() {
        Integer integer = ((SonyType1MakernoteDirectory) this._directory).getInteger(45094);
        return integer == null ? null : integer.intValue() == 0 ? "Off" : "On";
    }

    public String getLongExposureNoiseReductionDescription() {
        Integer integer = ((SonyType1MakernoteDirectory) this._directory).getInteger(45134);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Off";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "On";
            case 65535:
                return "N/A";
            default:
                return String.format("Unknown (%d)", new Object[]{integer});
        }
    }

    public String getMacroDescription() {
        Integer integer = ((SonyType1MakernoteDirectory) this._directory).getInteger(45120);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Off";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "On";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Magnifying Glass/Super Macro";
            case 65535:
                return "N/A";
            default:
                return String.format("Unknown (%d)", new Object[]{integer});
        }
    }

    public String getQualityDescription() {
        Integer integer = ((SonyType1MakernoteDirectory) this._directory).getInteger(45127);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Fine";
            case 65535:
                return "N/A";
            default:
                return String.format("Unknown (%d)", new Object[]{integer});
        }
    }

    public String getSceneModeDescription() {
        Integer integer = ((SonyType1MakernoteDirectory) this._directory).getInteger(45091);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Standard";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Portrait";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Text";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Night Scene";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Sunset";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Sports";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "Landscape";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "Night Portrait";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "Macro";
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return "Super Macro";
            case C1061R.styleable.Toolbar_maxButtonHeight /*16*/:
                return "Auto";
            case C1061R.styleable.Toolbar_collapseIcon /*17*/:
                return "Night View/Portrait";
            default:
                return String.format("Unknown (%d)", new Object[]{integer});
        }
    }

    public String getZoneMatchingDescription() {
        Integer integer = ((SonyType1MakernoteDirectory) this._directory).getInteger(45092);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "ISO Setting Used";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "High Key";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Low Key";
            default:
                return String.format("Unknown (%d)", new Object[]{integer});
        }
    }
}
