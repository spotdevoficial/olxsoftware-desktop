package com.drew.metadata.exif;

import com.drew.metadata.TagDescriptor;
import com.facebook.BuildConfig;
import com.urbanairship.C1608R;

public class OlympusMakernoteDescriptor extends TagDescriptor<OlympusMakernoteDirectory> {
    public OlympusMakernoteDescriptor(OlympusMakernoteDirectory olympusMakernoteDirectory) {
        super(olympusMakernoteDirectory);
    }

    public String getDescription(int i) {
        switch (i) {
            case 512:
                return getSpecialModeDescription();
            case 513:
                return getJpegQualityDescription();
            case 514:
                return getMacroModeDescription();
            case 516:
                return getDigiZoomRatioDescription();
            default:
                return super.getDescription(i);
        }
    }

    public String getDigiZoomRatioDescription() {
        Integer integer = ((OlympusMakernoteDirectory) this._directory).getInteger(516);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Normal";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Digital 2x Zoom";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getJpegQualityDescription() {
        Integer integer = ((OlympusMakernoteDirectory) this._directory).getInteger(513);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "SQ";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "HQ";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "SHQ";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getMacroModeDescription() {
        Integer integer = ((OlympusMakernoteDirectory) this._directory).getInteger(514);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Normal (no macro)";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Macro";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getSpecialModeDescription() {
        int[] intArray = ((OlympusMakernoteDirectory) this._directory).getIntArray(512);
        if (intArray == null) {
            return null;
        }
        if (intArray.length < 1) {
            return BuildConfig.VERSION_NAME;
        }
        StringBuilder stringBuilder = new StringBuilder();
        switch (intArray[0]) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                stringBuilder.append("Normal picture taking mode");
                break;
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                stringBuilder.append("Unknown picture taking mode");
                break;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                stringBuilder.append("Fast picture taking mode");
                break;
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                stringBuilder.append("Panorama picture taking mode");
                break;
            default:
                stringBuilder.append("Unknown picture taking mode");
                break;
        }
        if (intArray.length < 2) {
            return stringBuilder.toString();
        }
        stringBuilder.append(" - ");
        switch (intArray[1]) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                stringBuilder.append("Unknown sequence number");
                break;
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                stringBuilder.append("1st in a sequence");
                break;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                stringBuilder.append("2nd in a sequence");
                break;
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                stringBuilder.append("3rd in a sequence");
                break;
            default:
                stringBuilder.append(intArray[1]);
                stringBuilder.append("th in a sequence");
                break;
        }
        if (intArray.length < 3) {
            return stringBuilder.toString();
        }
        stringBuilder.append(" - ");
        switch (intArray[2]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                stringBuilder.append("Left to right panorama direction");
                break;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                stringBuilder.append("Right to left panorama direction");
                break;
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                stringBuilder.append("Bottom to top panorama direction");
                break;
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                stringBuilder.append("Top to bottom panorama direction");
                break;
        }
        return stringBuilder.toString();
    }
}
