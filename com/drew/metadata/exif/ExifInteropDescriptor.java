package com.drew.metadata.exif;

import com.drew.metadata.TagDescriptor;
import com.urbanairship.C1608R;

public class ExifInteropDescriptor extends TagDescriptor<ExifInteropDirectory> {
    public ExifInteropDescriptor(ExifInteropDirectory exifInteropDirectory) {
        super(exifInteropDirectory);
    }

    public String getDescription(int i) {
        switch (i) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getInteropIndexDescription();
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getInteropVersionDescription();
            default:
                return super.getDescription(i);
        }
    }

    public String getInteropIndexDescription() {
        String string = ((ExifInteropDirectory) this._directory).getString(1);
        return string == null ? null : "R98".equalsIgnoreCase(string.trim()) ? "Recommended Exif Interoperability Rules (ExifR98)" : "Unknown (" + string + ")";
    }

    public String getInteropVersionDescription() {
        return TagDescriptor.convertBytesToVersionString(((ExifInteropDirectory) this._directory).getIntArray(2), 2);
    }
}
