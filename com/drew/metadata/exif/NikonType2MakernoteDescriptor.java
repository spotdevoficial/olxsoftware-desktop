package com.drew.metadata.exif;

import com.drew.lang.Rational;
import com.drew.lang.StringUtil;
import com.drew.metadata.TagDescriptor;
import com.schibsted.scm.nextgenapp.C1061R;
import com.urbanairship.C1608R;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

public class NikonType2MakernoteDescriptor extends TagDescriptor<NikonType2MakernoteDirectory> {
    public NikonType2MakernoteDescriptor(NikonType2MakernoteDirectory nikonType2MakernoteDirectory) {
        super(nikonType2MakernoteDirectory);
    }

    private static String getEVDescription(int[] iArr) {
        if (iArr == null || iArr.length < 3 || iArr[2] == 0) {
            return null;
        }
        return new DecimalFormat("0.##").format(((double) (iArr[0] * iArr[1])) / ((double) iArr[2])) + " EV";
    }

    public String getActiveDLightingDescription() {
        Integer integer = ((NikonType2MakernoteDirectory) this._directory).getInteger(34);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Off";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Light";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "High";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "Extra High";
            case 65535:
                return "Auto";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getAutoFlashCompensationDescription() {
        return getEVDescription(((NikonType2MakernoteDirectory) this._directory).getIntArray(18));
    }

    public String getAutoFocusPositionDescription() {
        int[] intArray = ((NikonType2MakernoteDirectory) this._directory).getIntArray(136);
        if (intArray == null) {
            return null;
        }
        if (intArray.length != 4 || intArray[0] != 0 || intArray[2] != 0 || intArray[3] != 0) {
            return "Unknown (" + ((NikonType2MakernoteDirectory) this._directory).getString(136) + ")";
        }
        switch (intArray[1]) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Centre";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Top";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Bottom";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Left";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Right";
            default:
                return "Unknown (" + intArray[1] + ")";
        }
    }

    public String getColorModeDescription() {
        String string = ((NikonType2MakernoteDirectory) this._directory).getString(141);
        return string == null ? null : string.startsWith("MODE1") ? "Mode I (sRGB)" : string;
    }

    public String getColorSpaceDescription() {
        Integer integer = ((NikonType2MakernoteDirectory) this._directory).getInteger(30);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "sRGB";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Adobe RGB";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getDescription(int i) {
        switch (i) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getFirmwareVersionDescription();
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getIsoSettingDescription();
            case C1608R.styleable.MapAttrs_useViewLifecycle /*13*/:
                return getProgramShiftDescription();
            case C1608R.styleable.MapAttrs_zOrderOnTop /*14*/:
                return getExposureDifferenceDescription();
            case C1061R.styleable.Toolbar_collapseContentDescription /*18*/:
                return getAutoFlashCompensationDescription();
            case C1061R.styleable.Theme_actionBarSize /*23*/:
                return getFlashExposureCompensationDescription();
            case C1061R.styleable.Theme_actionBarDivider /*24*/:
                return getFlashBracketCompensationDescription();
            case C1061R.styleable.Theme_actionModeStyle /*28*/:
                return getExposureTuningDescription();
            case C1061R.styleable.Theme_actionModeBackground /*30*/:
                return getColorSpaceDescription();
            case C1061R.styleable.Theme_actionModeCopyDrawable /*34*/:
                return getActiveDLightingDescription();
            case C1061R.styleable.Theme_textAppearanceSmallPopupMenu /*42*/:
                return getVignetteControlDescription();
            case 131:
                return getLensTypeDescription();
            case 132:
                return getLensDescription();
            case 134:
                return getDigitalZoomDescription();
            case 135:
                return getFlashUsedDescription();
            case 136:
                return getAutoFocusPositionDescription();
            case 137:
                return getShootingModeDescription();
            case 139:
                return getLensStopsDescription();
            case 141:
                return getColorModeDescription();
            case 146:
                return getHueAdjustmentDescription();
            case 147:
                return getNEFCompressionDescription();
            case 177:
                return getHighISONoiseReductionDescription();
            case 182:
                return getPowerUpTimeDescription();
            default:
                return super.getDescription(i);
        }
    }

    public String getDigitalZoomDescription() {
        Rational rational = ((NikonType2MakernoteDirectory) this._directory).getRational(134);
        return rational == null ? null : rational.intValue() == 1 ? "No digital zoom" : rational.toSimpleString(true) + "x digital zoom";
    }

    public String getExposureDifferenceDescription() {
        return getEVDescription(((NikonType2MakernoteDirectory) this._directory).getIntArray(14));
    }

    public String getExposureTuningDescription() {
        return getEVDescription(((NikonType2MakernoteDirectory) this._directory).getIntArray(28));
    }

    public String getFirmwareVersionDescription() {
        int[] intArray = ((NikonType2MakernoteDirectory) this._directory).getIntArray(1);
        return intArray == null ? null : TagDescriptor.convertBytesToVersionString(intArray, 2);
    }

    public String getFlashBracketCompensationDescription() {
        return getEVDescription(((NikonType2MakernoteDirectory) this._directory).getIntArray(24));
    }

    public String getFlashExposureCompensationDescription() {
        return getEVDescription(((NikonType2MakernoteDirectory) this._directory).getIntArray(23));
    }

    public String getFlashUsedDescription() {
        Integer integer = ((NikonType2MakernoteDirectory) this._directory).getInteger(135);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Flash Not Used";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Manual Flash";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Flash Not Ready";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "External Flash";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "Fired, Commander Mode";
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return "Fired, TTL Mode";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getHighISONoiseReductionDescription() {
        Integer integer = ((NikonType2MakernoteDirectory) this._directory).getInteger(177);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Off";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Minimal";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Low";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "High";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getHueAdjustmentDescription() {
        String string = ((NikonType2MakernoteDirectory) this._directory).getString(146);
        return string == null ? null : string + " degrees";
    }

    public String getIsoSettingDescription() {
        int[] intArray = ((NikonType2MakernoteDirectory) this._directory).getIntArray(2);
        return intArray == null ? null : (intArray[0] != 0 || intArray[1] == 0) ? "Unknown (" + ((NikonType2MakernoteDirectory) this._directory).getString(2) + ")" : "ISO " + intArray[1];
    }

    public String getLensDescription() {
        Rational[] rationalArray = ((NikonType2MakernoteDirectory) this._directory).getRationalArray(132);
        if (rationalArray == null) {
            return null;
        }
        if (rationalArray.length < 4) {
            return ((NikonType2MakernoteDirectory) this._directory).getString(132);
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(rationalArray[0].intValue());
        stringBuilder.append('-');
        stringBuilder.append(rationalArray[1].intValue());
        stringBuilder.append("mm f/");
        stringBuilder.append(rationalArray[2].floatValue());
        stringBuilder.append('-');
        stringBuilder.append(rationalArray[3].floatValue());
        return stringBuilder.toString();
    }

    public String getLensStopsDescription() {
        return getEVDescription(((NikonType2MakernoteDirectory) this._directory).getIntArray(139));
    }

    public String getLensTypeDescription() {
        Integer integer = ((NikonType2MakernoteDirectory) this._directory).getInteger(131);
        if (integer == null) {
            return null;
        }
        Iterable arrayList = new ArrayList();
        if ((integer.intValue() & 1) == 1) {
            arrayList.add("MF");
        } else {
            arrayList.add("AF");
        }
        if ((integer.intValue() & 2) == 2) {
            arrayList.add("D");
        }
        if ((integer.intValue() & 4) == 4) {
            arrayList.add("G");
        }
        if ((integer.intValue() & 8) == 8) {
            arrayList.add("VR");
        }
        return StringUtil.join(arrayList, ", ");
    }

    public String getNEFCompressionDescription() {
        Integer integer = ((NikonType2MakernoteDirectory) this._directory).getInteger(147);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Lossy (Type 1)";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Uncompressed";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "Lossless";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "Lossy (Type 2)";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getPowerUpTimeDescription() {
        Long longObject = ((NikonType2MakernoteDirectory) this._directory).getLongObject(182);
        return longObject == null ? null : new Date(longObject.longValue()).toString();
    }

    public String getProgramShiftDescription() {
        return getEVDescription(((NikonType2MakernoteDirectory) this._directory).getIntArray(13));
    }

    public String getShootingModeDescription() {
        Integer integer = ((NikonType2MakernoteDirectory) this._directory).getInteger(137);
        if (integer == null) {
            return null;
        }
        Iterable arrayList = new ArrayList();
        if ((integer.intValue() & 1) == 1) {
            arrayList.add("Continuous");
        } else {
            arrayList.add("Single Frame");
        }
        if ((integer.intValue() & 2) == 2) {
            arrayList.add("Delay");
        }
        if ((integer.intValue() & 8) == 8) {
            arrayList.add("PC Control");
        }
        if ((integer.intValue() & 16) == 16) {
            arrayList.add("Exposure Bracketing");
        }
        if ((integer.intValue() & 32) == 32) {
            arrayList.add("Auto ISO");
        }
        if ((integer.intValue() & 64) == 64) {
            arrayList.add("White-Balance Bracketing");
        }
        if ((integer.intValue() & 128) == 128) {
            arrayList.add("IR Control");
        }
        return StringUtil.join(arrayList, ", ");
    }

    public String getVignetteControlDescription() {
        Integer integer = ((NikonType2MakernoteDirectory) this._directory).getInteger(42);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Off";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Low";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "High";
            default:
                return "Unknown (" + integer + ")";
        }
    }
}
