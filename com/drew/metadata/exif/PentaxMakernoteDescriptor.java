package com.drew.metadata.exif;

import com.drew.metadata.TagDescriptor;
import com.schibsted.scm.nextgenapp.C1061R;
import com.urbanairship.C1608R;

public class PentaxMakernoteDescriptor extends TagDescriptor<PentaxMakernoteDirectory> {
    public PentaxMakernoteDescriptor(PentaxMakernoteDirectory pentaxMakernoteDirectory) {
        super(pentaxMakernoteDirectory);
    }

    public String getCaptureModeDescription() {
        Integer integer = ((PentaxMakernoteDirectory) this._directory).getInteger(1);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Auto";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Night-scene";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Manual";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Multiple";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getColourDescription() {
        Integer integer = ((PentaxMakernoteDirectory) this._directory).getInteger(23);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Normal";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Black & White";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Sepia";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getContrastDescription() {
        Integer integer = ((PentaxMakernoteDirectory) this._directory).getInteger(12);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Low";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "High";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getDescription(int i) {
        switch (i) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getCaptureModeDescription();
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getQualityLevelDescription();
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getFocusModeDescription();
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return getFlashModeDescription();
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return getWhiteBalanceDescription();
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return getDigitalZoomDescription();
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                return getSharpnessDescription();
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                return getContrastDescription();
            case C1608R.styleable.MapAttrs_useViewLifecycle /*13*/:
                return getSaturationDescription();
            case C1061R.styleable.Toolbar_navigationContentDescription /*20*/:
                return getIsoSpeedDescription();
            case C1061R.styleable.Theme_actionBarSize /*23*/:
                return getColourDescription();
            default:
                return super.getDescription(i);
        }
    }

    public String getDigitalZoomDescription() {
        Float floatObject = ((PentaxMakernoteDirectory) this._directory).getFloatObject(10);
        return floatObject == null ? null : floatObject.floatValue() == 0.0f ? "Off" : Float.toString(floatObject.floatValue());
    }

    public String getFlashModeDescription() {
        Integer integer = ((PentaxMakernoteDirectory) this._directory).getInteger(4);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Auto";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Flash On";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Flash Off";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "Red-eye Reduction";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getFocusModeDescription() {
        Integer integer = ((PentaxMakernoteDirectory) this._directory).getInteger(3);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Custom";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Auto";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getIsoSpeedDescription() {
        Integer integer = ((PentaxMakernoteDirectory) this._directory).getInteger(20);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return "ISO 100";
            case C1061R.styleable.Toolbar_maxButtonHeight /*16*/:
                return "ISO 200";
            case C1061R.styleable.Theme_buttonStyleSmall /*100*/:
                return "ISO 100";
            case 200:
                return "ISO 200";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getQualityLevelDescription() {
        Integer integer = ((PentaxMakernoteDirectory) this._directory).getInteger(2);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Good";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Better";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Best";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getSaturationDescription() {
        Integer integer = ((PentaxMakernoteDirectory) this._directory).getInteger(13);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Low";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "High";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getSharpnessDescription() {
        Integer integer = ((PentaxMakernoteDirectory) this._directory).getInteger(11);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Soft";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Hard";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getWhiteBalanceDescription() {
        Integer integer = ((PentaxMakernoteDirectory) this._directory).getInteger(7);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Auto";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Daylight";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Shade";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Tungsten";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Fluorescent";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Manual";
            default:
                return "Unknown (" + integer + ")";
        }
    }
}
