package com.drew.metadata.exif;

import com.drew.lang.GeoLocation;
import com.drew.lang.Rational;
import com.drew.metadata.TagDescriptor;
import com.schibsted.scm.nextgenapp.C1061R;
import com.urbanairship.C1608R;
import java.text.DecimalFormat;

public class GpsDescriptor extends TagDescriptor<GpsDirectory> {
    public GpsDescriptor(GpsDirectory gpsDirectory) {
        super(gpsDirectory);
    }

    private String getGpsVersionIdDescription() {
        return TagDescriptor.convertBytesToVersionString(((GpsDirectory) this._directory).getIntArray(0), 1);
    }

    public String getDescription(int i) {
        switch (i) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return getGpsVersionIdDescription();
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getGpsLatitudeDescription();
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return getGpsLongitudeDescription();
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return getGpsAltitudeRefDescription();
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return getGpsAltitudeDescription();
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return getGpsTimeStampDescription();
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return getGpsStatusDescription();
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return getGpsMeasureModeDescription();
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                return getGpsSpeedRefDescription();
            case C1608R.styleable.MapAttrs_zOrderOnTop /*14*/:
            case C1061R.styleable.Toolbar_maxButtonHeight /*16*/:
            case C1061R.styleable.Theme_actionBarSize /*23*/:
                return getGpsDirectionReferenceDescription(i);
            case C1608R.styleable.MapAttrs_uiMapToolbar /*15*/:
            case C1061R.styleable.Toolbar_collapseIcon /*17*/:
            case C1061R.styleable.Theme_actionBarDivider /*24*/:
                return getGpsDirectionDescription(i);
            case C1061R.styleable.Theme_actionBarItemBackground /*25*/:
                return getGpsDestinationReferenceDescription();
            case C1061R.styleable.Theme_actionModeBackground /*30*/:
                return getGpsDifferentialDescription();
            default:
                return super.getDescription(i);
        }
    }

    public String getGpsAltitudeDescription() {
        Rational rational = ((GpsDirectory) this._directory).getRational(6);
        return rational == null ? null : rational.intValue() + " metres";
    }

    public String getGpsAltitudeRefDescription() {
        Integer integer = ((GpsDirectory) this._directory).getInteger(5);
        return integer == null ? null : integer.intValue() == 0 ? "Sea level" : integer.intValue() == 1 ? "Below sea level" : "Unknown (" + integer + ")";
    }

    public String getGpsDestinationReferenceDescription() {
        String string = ((GpsDirectory) this._directory).getString(25);
        if (string == null) {
            return null;
        }
        string = string.trim();
        return "K".equalsIgnoreCase(string) ? "kilometers" : "M".equalsIgnoreCase(string) ? "miles" : "N".equalsIgnoreCase(string) ? "knots" : "Unknown (" + string + ")";
    }

    public String getGpsDifferentialDescription() {
        Integer integer = ((GpsDirectory) this._directory).getInteger(30);
        return integer == null ? null : integer.intValue() == 0 ? "No Correction" : integer.intValue() == 1 ? "Differential Corrected" : "Unknown (" + integer + ")";
    }

    public String getGpsDirectionDescription(int i) {
        Rational rational = ((GpsDirectory) this._directory).getRational(i);
        String format = rational != null ? new DecimalFormat("0.##").format(rational.doubleValue()) : ((GpsDirectory) this._directory).getString(i);
        return (format == null || format.trim().length() == 0) ? null : format.trim() + " degrees";
    }

    public String getGpsDirectionReferenceDescription(int i) {
        String string = ((GpsDirectory) this._directory).getString(i);
        if (string == null) {
            return null;
        }
        string = string.trim();
        return "T".equalsIgnoreCase(string) ? "True direction" : "M".equalsIgnoreCase(string) ? "Magnetic direction" : "Unknown (" + string + ")";
    }

    public String getGpsLatitudeDescription() {
        GeoLocation geoLocation = ((GpsDirectory) this._directory).getGeoLocation();
        return geoLocation == null ? null : GeoLocation.decimalToDegreesMinutesSecondsString(geoLocation.getLatitude());
    }

    public String getGpsLongitudeDescription() {
        GeoLocation geoLocation = ((GpsDirectory) this._directory).getGeoLocation();
        return geoLocation == null ? null : GeoLocation.decimalToDegreesMinutesSecondsString(geoLocation.getLongitude());
    }

    public String getGpsMeasureModeDescription() {
        String string = ((GpsDirectory) this._directory).getString(10);
        if (string == null) {
            return null;
        }
        string = string.trim();
        return "2".equalsIgnoreCase(string) ? "2-dimensional measurement" : "3".equalsIgnoreCase(string) ? "3-dimensional measurement" : "Unknown (" + string + ")";
    }

    public String getGpsSpeedRefDescription() {
        String string = ((GpsDirectory) this._directory).getString(12);
        if (string == null) {
            return null;
        }
        string = string.trim();
        return "K".equalsIgnoreCase(string) ? "kph" : "M".equalsIgnoreCase(string) ? "mph" : "N".equalsIgnoreCase(string) ? "knots" : "Unknown (" + string + ")";
    }

    public String getGpsStatusDescription() {
        String string = ((GpsDirectory) this._directory).getString(9);
        if (string == null) {
            return null;
        }
        string = string.trim();
        return "A".equalsIgnoreCase(string) ? "Active (Measurement in progress)" : "V".equalsIgnoreCase(string) ? "Void (Measurement Interoperability)" : "Unknown (" + string + ")";
    }

    public String getGpsTimeStampDescription() {
        int[] intArray = ((GpsDirectory) this._directory).getIntArray(7);
        if (intArray == null) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(intArray[0]);
        stringBuilder.append(":");
        stringBuilder.append(intArray[1]);
        stringBuilder.append(":");
        stringBuilder.append(intArray[2]);
        stringBuilder.append(" UTC");
        return stringBuilder.toString();
    }
}
