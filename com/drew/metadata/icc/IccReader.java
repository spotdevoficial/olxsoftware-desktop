package com.drew.metadata.icc;

import com.drew.lang.BufferBoundsException;
import com.drew.lang.BufferReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import java.util.Calendar;
import java.util.TimeZone;

public class IccReader {
    public static String getStringFromInt32(int i) {
        return new String(new byte[]{(byte) ((-16777216 & i) >> 24), (byte) ((16711680 & i) >> 16), (byte) ((65280 & i) >> 8), (byte) (i & 255)});
    }

    private void set4ByteString(Directory directory, int i, BufferReader bufferReader) throws BufferBoundsException {
        int int32 = bufferReader.getInt32(i);
        if (int32 != 0) {
            directory.setString(i, getStringFromInt32(int32));
        }
    }

    private void setDate(IccDirectory iccDirectory, int i, BufferReader bufferReader) throws BufferBoundsException {
        int uInt16 = bufferReader.getUInt16(i);
        int uInt162 = bufferReader.getUInt16(i + 2);
        int uInt163 = bufferReader.getUInt16(i + 4);
        int uInt164 = bufferReader.getUInt16(i + 6);
        int uInt165 = bufferReader.getUInt16(i + 8);
        int uInt166 = bufferReader.getUInt16(i + 10);
        Calendar instance = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        instance.set(uInt16, uInt162, uInt163, uInt164, uInt165, uInt166);
        iccDirectory.setDate(i, instance.getTime());
    }

    private void setInt32(Directory directory, int i, BufferReader bufferReader) throws BufferBoundsException {
        int int32 = bufferReader.getInt32(i);
        if (int32 != 0) {
            directory.setInt(i, int32);
        }
    }

    private void setInt64(Directory directory, int i, BufferReader bufferReader) throws BufferBoundsException {
        long int64 = bufferReader.getInt64(i);
        if (int64 != 0) {
            directory.setLong(i, int64);
        }
    }

    public void extract(BufferReader bufferReader, Metadata metadata) {
        IccDirectory iccDirectory = (IccDirectory) metadata.getOrCreateDirectory(IccDirectory.class);
        try {
            iccDirectory.setInt(0, bufferReader.getInt32(0));
            set4ByteString(iccDirectory, 4, bufferReader);
            setInt32(iccDirectory, 8, bufferReader);
            set4ByteString(iccDirectory, 12, bufferReader);
            set4ByteString(iccDirectory, 16, bufferReader);
            set4ByteString(iccDirectory, 20, bufferReader);
            setDate(iccDirectory, 24, bufferReader);
            set4ByteString(iccDirectory, 36, bufferReader);
            set4ByteString(iccDirectory, 40, bufferReader);
            setInt32(iccDirectory, 44, bufferReader);
            set4ByteString(iccDirectory, 48, bufferReader);
            int int32 = bufferReader.getInt32(52);
            if (int32 != 0) {
                if (int32 <= 538976288) {
                    iccDirectory.setInt(52, int32);
                } else {
                    iccDirectory.setString(52, getStringFromInt32(int32));
                }
            }
            setInt32(iccDirectory, 64, bufferReader);
            setInt64(iccDirectory, 56, bufferReader);
            iccDirectory.setObject(68, new float[]{bufferReader.getS15Fixed16(68), bufferReader.getS15Fixed16(72), bufferReader.getS15Fixed16(76)});
            int int322 = bufferReader.getInt32(128);
            iccDirectory.setInt(128, int322);
            for (int32 = 0; int32 < int322; int32++) {
                int i = (int32 * 12) + 132;
                iccDirectory.setByteArray(bufferReader.getInt32(i), bufferReader.getBytes(bufferReader.getInt32(i + 4), bufferReader.getInt32(i + 8)));
            }
        } catch (BufferBoundsException e) {
            iccDirectory.addError(String.format("Reading ICC Header %s:%s", new Object[]{e.getClass().getSimpleName(), e.getMessage()}));
        }
    }
}
