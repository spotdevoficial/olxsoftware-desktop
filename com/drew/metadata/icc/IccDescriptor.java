package com.drew.metadata.icc;

import com.drew.lang.BufferBoundsException;
import com.drew.lang.BufferReader;
import com.drew.lang.ByteArrayReader;
import com.drew.metadata.TagDescriptor;
import com.facebook.BuildConfig;
import com.facebook.internal.AnalyticsEvents;
import com.schibsted.scm.nextgenapp.C1061R;
import com.urbanairship.C1608R;
import java.io.UnsupportedEncodingException;

public class IccDescriptor extends TagDescriptor<IccDirectory> {
    public IccDescriptor(IccDirectory iccDirectory) {
        super(iccDirectory);
    }

    public static String formatDoubleAsString(double d, int i, boolean z) {
        if (i < 1) {
            return BuildConfig.VERSION_NAME + Math.round(d);
        }
        long abs = Math.abs((long) d);
        long round = (long) ((int) Math.round((Math.abs(d) - ((double) abs)) * Math.pow(10.0d, (double) i)));
        String str = BuildConfig.VERSION_NAME;
        long j = round;
        while (i > 0) {
            byte abs2 = (byte) ((int) Math.abs(j % 10));
            j /= 10;
            if (str.length() > 0 || z || abs2 != null || i == 1) {
                str = abs2 + str;
            }
            i--;
        }
        j += abs;
        Object obj = (d >= 0.0d || (j == 0 && round == 0)) ? null : 1;
        return (obj != null ? "-" : BuildConfig.VERSION_NAME) + j + "." + str;
    }

    private static int getInt32FromString(String str) throws BufferBoundsException {
        return new ByteArrayReader(str.getBytes()).getInt32(0);
    }

    private String getPlatformDescription() {
        String string = ((IccDirectory) this._directory).getString(40);
        if (string == null) {
            return null;
        }
        try {
            switch (getInt32FromString(string)) {
                case 1095782476:
                    return "Apple Computer, Inc.";
                case 1297303124:
                    return "Microsoft Corporation";
                case 1397180704:
                    return "Silicon Graphics, Inc.";
                case 1398099543:
                    return "Sun Microsystems, Inc.";
                case 1413959252:
                    return "Taligent, Inc.";
                default:
                    return String.format("Unknown (%s)", new Object[]{string});
            }
        } catch (BufferBoundsException e) {
            return string;
        }
    }

    private String getProfileClassDescription() {
        String string = ((IccDirectory) this._directory).getString(12);
        if (string == null) {
            return null;
        }
        try {
            switch (getInt32FromString(string)) {
                case 1633842036:
                    return "Abstract";
                case 1818848875:
                    return "DeviceLink";
                case 1835955314:
                    return "Display Device";
                case 1852662636:
                    return "Named Color";
                case 1886549106:
                    return "Output Device";
                case 1935896178:
                    return "Input Device";
                case 1936744803:
                    return "ColorSpace Conversion";
                default:
                    return String.format("Unknown (%s)", new Object[]{string});
            }
        } catch (BufferBoundsException e) {
            return string;
        }
    }

    private String getProfileVersionDescription() {
        Integer integer = ((IccDirectory) this._directory).getInteger(8);
        if (integer == null) {
            return null;
        }
        int intValue = (integer.intValue() & -16777216) >> 24;
        int intValue2 = (integer.intValue() & 15728640) >> 20;
        int intValue3 = (integer.intValue() & 983040) >> 16;
        return String.format("%d.%d.%d", new Object[]{Integer.valueOf(intValue), Integer.valueOf(intValue2), Integer.valueOf(intValue3)});
    }

    private String getRenderingIntentDescription() {
        Integer integer = ((IccDirectory) this._directory).getInteger(64);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Perceptual";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Media-Relative Colorimetric";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Saturation";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "ICC-Absolute Colorimetric";
            default:
                return String.format("Unknown (%d)", new Object[]{integer});
        }
    }

    private String getTagDataString(int i) {
        int int32;
        int i2 = 0;
        byte[] byteArray = ((IccDirectory) this._directory).getByteArray(i);
        BufferReader byteArrayReader = new ByteArrayReader(byteArray);
        int length;
        float s15Fixed16;
        float s15Fixed162;
        float s15Fixed163;
        String str;
        switch (byteArrayReader.getInt32(0)) {
            case 1482250784:
                StringBuilder stringBuilder = new StringBuilder();
                length = (byteArray.length - 8) / 12;
                while (i2 < length) {
                    s15Fixed16 = byteArrayReader.getS15Fixed16((i2 * 12) + 8);
                    s15Fixed162 = byteArrayReader.getS15Fixed16(((i2 * 12) + 8) + 4);
                    s15Fixed163 = byteArrayReader.getS15Fixed16(((i2 * 12) + 8) + 8);
                    if (i2 > 0) {
                        stringBuilder.append(", ");
                    }
                    stringBuilder.append("(").append(s15Fixed16).append(", ").append(s15Fixed162).append(", ").append(s15Fixed163).append(")");
                    i2++;
                }
                return stringBuilder.toString();
            case 1668641398:
                length = byteArrayReader.getInt32(8);
                StringBuilder stringBuilder2 = new StringBuilder();
                for (int i3 = 0; i3 < length; i3++) {
                    if (i3 != 0) {
                        stringBuilder2.append(", ");
                    }
                    stringBuilder2.append(formatDoubleAsString(((double) ((float) byteArrayReader.getUInt16((i3 * 2) + 12))) / 65535.0d, 7, false));
                }
                return stringBuilder2.toString();
            case 1684370275:
                return new String(byteArray, 12, byteArrayReader.getInt32(8) - 1);
            case 1835360627:
                Object format;
                Object format2;
                i2 = byteArrayReader.getInt32(8);
                s15Fixed16 = byteArrayReader.getS15Fixed16(12);
                s15Fixed162 = byteArrayReader.getS15Fixed16(16);
                s15Fixed163 = byteArrayReader.getS15Fixed16(20);
                int32 = byteArrayReader.getInt32(24);
                float s15Fixed164 = byteArrayReader.getS15Fixed16(28);
                int int322 = byteArrayReader.getInt32(32);
                String str2;
                switch (i2) {
                    case C1608R.styleable.MapAttrs_mapType /*0*/:
                        str2 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
                        break;
                    case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                        str2 = "1931 2\u00b0";
                        break;
                    case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                        str2 = "1964 10\u00b0";
                        break;
                    default:
                        format = String.format("Unknown %d", new Object[]{Integer.valueOf(i2)});
                        break;
                }
                String str3;
                switch (int32) {
                    case C1608R.styleable.MapAttrs_mapType /*0*/:
                        str3 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
                        break;
                    case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                        str3 = "0/45 or 45/0";
                        break;
                    case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                        str3 = "0/d or d/0";
                        break;
                    default:
                        format2 = String.format("Unknown %d", new Object[]{Integer.valueOf(i2)});
                        break;
                }
                switch (int322) {
                    case C1608R.styleable.MapAttrs_mapType /*0*/:
                        str = AnalyticsEvents.PARAMETER_SHARE_OUTCOME_UNKNOWN;
                        break;
                    case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                        str = "D50";
                        break;
                    case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                        str = "D65";
                        break;
                    case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                        str = "D93";
                        break;
                    case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                        str = "F2";
                        break;
                    case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                        str = "D55";
                        break;
                    case C1608R.styleable.MapAttrs_liteMode /*6*/:
                        str = "A";
                        break;
                    case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                        str = "Equi-Power (E)";
                        break;
                    case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                        str = "F8";
                        break;
                    default:
                        str = String.format("Unknown %d", new Object[]{Integer.valueOf(int322)});
                        break;
                }
                return String.format("%s Observer, Backing (%s, %s, %s), Geometry %s, Flare %d%%, Illuminant %s", new Object[]{format, Float.valueOf(s15Fixed16), Float.valueOf(s15Fixed162), Float.valueOf(s15Fixed163), format2, Integer.valueOf(Math.round(100.0f * s15Fixed164)), str});
            case 1835824483:
                int int323 = byteArrayReader.getInt32(8);
                StringBuilder stringBuilder3 = new StringBuilder();
                stringBuilder3.append(int323);
                while (i2 < int323) {
                    String stringFromInt32 = IccReader.getStringFromInt32(byteArrayReader.getInt32((i2 * 12) + 16));
                    int32 = byteArrayReader.getInt32(((i2 * 12) + 16) + 4);
                    int int324 = byteArrayReader.getInt32(((i2 * 12) + 16) + 8);
                    try {
                        str = new String(byteArray, int324, int32, "UTF-16BE");
                    } catch (UnsupportedEncodingException e) {
                        str = new String(byteArray, int324, int32);
                    }
                    try {
                        stringBuilder3.append(" ").append(stringFromInt32).append("(").append(str).append(")");
                        i2++;
                    } catch (BufferBoundsException e2) {
                        return null;
                    }
                }
                return stringBuilder3.toString();
            case 1936287520:
                return IccReader.getStringFromInt32(byteArrayReader.getInt32(8));
            case 1952807028:
                try {
                    return new String(byteArray, 8, (byteArray.length - 8) - 1, "ASCII");
                } catch (UnsupportedEncodingException e3) {
                    return new String(byteArray, 8, (byteArray.length - 8) - 1);
                }
            default:
                return String.format("%s(0x%08X): %d bytes", new Object[]{IccReader.getStringFromInt32(byteArrayReader.getInt32(0)), Integer.valueOf(byteArrayReader.getInt32(0)), Integer.valueOf(byteArray.length)});
        }
        return null;
    }

    public String getDescription(int i) {
        switch (i) {
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return getProfileVersionDescription();
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                return getProfileClassDescription();
            case C1061R.styleable.Theme_actionModePopupWindowStyle /*40*/:
                return getPlatformDescription();
            case C1061R.styleable.Theme_editTextBackground /*64*/:
                return getRenderingIntentDescription();
            default:
                return (i <= 538976288 || i >= 2054847098) ? super.getDescription(i) : getTagDataString(i);
        }
    }
}
