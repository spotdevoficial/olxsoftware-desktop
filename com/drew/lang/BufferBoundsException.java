package com.drew.lang;

import com.facebook.BuildConfig;
import java.io.IOException;

public final class BufferBoundsException extends Exception {
    public BufferBoundsException(String str) {
        super(str);
    }

    public BufferBoundsException(String str, IOException iOException) {
        super(str, iOException);
    }

    public BufferBoundsException(byte[] bArr, int i, int i2) {
        super(getMessage(bArr, i, i2));
    }

    private static String getMessage(byte[] bArr, int i, int i2) {
        if (i < 0) {
            return String.format("Attempt to read from buffer using a negative index (%s)", new Object[]{Integer.valueOf(i)});
        }
        String str = "Attempt to read %d byte%s from beyond end of buffer (requested index: %d, max index: %d)";
        Object[] objArr = new Object[4];
        objArr[0] = Integer.valueOf(i2);
        objArr[1] = i2 == 1 ? BuildConfig.VERSION_NAME : "s";
        objArr[2] = Integer.valueOf(i);
        objArr[3] = Integer.valueOf(bArr.length - 1);
        return String.format(str, objArr);
    }
}
