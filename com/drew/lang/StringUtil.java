package com.drew.lang;

import java.util.Iterator;

public class StringUtil {
    public static String join(Iterable<? extends CharSequence> iterable, String str) {
        int length = str.length();
        Iterator it = iterable.iterator();
        StringBuilder stringBuilder = new StringBuilder(it.hasNext() ? (((CharSequence) it.next()).length() + length) + 0 : 0);
        Iterator it2 = iterable.iterator();
        if (it2.hasNext()) {
            stringBuilder.append((CharSequence) it2.next());
            while (it2.hasNext()) {
                stringBuilder.append(str);
                stringBuilder.append((CharSequence) it2.next());
            }
        }
        return stringBuilder.toString();
    }

    public static <T extends CharSequence> String join(T[] tArr, String str) {
        int length = str.length();
        int i = 0;
        for (CharSequence length2 : tArr) {
            i += length2.length() + length;
        }
        StringBuilder stringBuilder = new StringBuilder(i);
        Object obj = 1;
        for (CharSequence length22 : tArr) {
            if (obj == null) {
                stringBuilder.append(str);
            } else {
                obj = null;
            }
            stringBuilder.append(length22);
        }
        return stringBuilder.toString();
    }
}
