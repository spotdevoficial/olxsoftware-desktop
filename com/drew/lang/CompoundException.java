package com.drew.lang;

import java.io.PrintStream;
import java.io.PrintWriter;

public class CompoundException extends Exception {
    private final Throwable _innerException;

    public CompoundException(String str) {
        this(str, null);
    }

    public CompoundException(String str, Throwable th) {
        super(str);
        this._innerException = th;
    }

    public CompoundException(Throwable th) {
        this(null, th);
    }

    public void printStackTrace() {
        super.printStackTrace();
        if (this._innerException != null) {
            System.err.println("--- inner exception ---");
            this._innerException.printStackTrace();
        }
    }

    public void printStackTrace(PrintStream printStream) {
        super.printStackTrace(printStream);
        if (this._innerException != null) {
            printStream.println("--- inner exception ---");
            this._innerException.printStackTrace(printStream);
        }
    }

    public void printStackTrace(PrintWriter printWriter) {
        super.printStackTrace(printWriter);
        if (this._innerException != null) {
            printWriter.println("--- inner exception ---");
            this._innerException.printStackTrace(printWriter);
        }
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(super.toString());
        if (this._innerException != null) {
            stringBuilder.append("\n");
            stringBuilder.append("--- inner exception ---");
            stringBuilder.append("\n");
            stringBuilder.append(this._innerException.toString());
        }
        return stringBuilder.toString();
    }
}
