package com.schibsted.scm.nextgenapp.nativeads.binder;

import android.support.v7.widget.RecyclerView.ViewHolder;

public interface AdBinder {
    void bind(ViewHolder viewHolder);
}
