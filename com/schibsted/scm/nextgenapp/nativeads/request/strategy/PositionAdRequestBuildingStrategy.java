package com.schibsted.scm.nextgenapp.nativeads.request.strategy;

import com.google.android.gms.ads.doubleclick.PublisherAdRequest.Builder;

public class PositionAdRequestBuildingStrategy implements AdRequestBuildingStrategy {
    private int mPosition;

    public PositionAdRequestBuildingStrategy(int position) {
        this.mPosition = position;
    }

    public Builder assignValue(Builder builder) {
        return builder.addCustomTargeting("pos", String.valueOf(this.mPosition));
    }
}
