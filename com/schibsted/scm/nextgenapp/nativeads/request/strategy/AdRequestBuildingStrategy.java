package com.schibsted.scm.nextgenapp.nativeads.request.strategy;

import com.google.android.gms.ads.doubleclick.PublisherAdRequest.Builder;

public interface AdRequestBuildingStrategy {
    Builder assignValue(Builder builder);
}
