package com.schibsted.scm.nextgenapp.nativeads.request.strategy;

import com.google.android.gms.ads.doubleclick.PublisherAdRequest.Builder;

public class KeywordAdRequestBuildingStrategy implements AdRequestBuildingStrategy {
    private String mKeyword;

    public KeywordAdRequestBuildingStrategy(String keyword) {
        this.mKeyword = keyword;
    }

    public Builder assignValue(Builder builder) {
        return builder.addCustomTargeting("search_kw", this.mKeyword);
    }
}
