package com.schibsted.scm.nextgenapp.nativeads.request;

import com.google.android.gms.ads.doubleclick.PublisherAdRequest.Builder;
import com.schibsted.scm.nextgenapp.nativeads.request.strategy.AdRequestBuildingStrategy;

public class GoogleNativeAdRequestDecorator implements NativeAdRequest {
    private NativeAdRequest mNativeAdRequest;
    private AdRequestBuildingStrategy mRequestStrategy;

    public GoogleNativeAdRequestDecorator(NativeAdRequest nativeAdRequest, AdRequestBuildingStrategy requestStrategy) {
        this.mNativeAdRequest = nativeAdRequest;
        this.mRequestStrategy = requestStrategy;
    }

    public Builder getBuilder() {
        return this.mRequestStrategy.assignValue(this.mNativeAdRequest.getBuilder());
    }
}
