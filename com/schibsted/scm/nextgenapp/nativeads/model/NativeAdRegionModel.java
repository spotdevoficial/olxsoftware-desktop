package com.schibsted.scm.nextgenapp.nativeads.model;

public class NativeAdRegionModel {
    private String abbreviatedStateName;
    private String regionCode;

    public NativeAdRegionModel(String abbreviatedStateName, String regionCode) {
        this.abbreviatedStateName = abbreviatedStateName;
        this.regionCode = regionCode;
    }

    public String getRegionCode() {
        return this.regionCode;
    }

    public String getAbbreviatedStateName() {
        return this.abbreviatedStateName;
    }
}
