package com.schibsted.scm.nextgenapp.nativeads.strategy.impl;

import com.schibsted.scm.nextgenapp.nativeads.strategy.SinglePageListingNativeAdStrategy;

public class SinglePageListingNativeAdStrategyImpl implements SinglePageListingNativeAdStrategy {
    private int[] mNativeAdsIndexes;

    public SinglePageListingNativeAdStrategyImpl(int... mNativeAdsIndexes) {
        if (mNativeAdsIndexes == null) {
            mNativeAdsIndexes = new int[0];
        }
        this.mNativeAdsIndexes = mNativeAdsIndexes;
    }

    public boolean isIndexANativeAd(int indexWithNativeAd) {
        for (int nativeAdIndex : this.mNativeAdsIndexes) {
            if (indexWithNativeAd == nativeAdIndex) {
                return true;
            }
        }
        return false;
    }

    public int shiftIndexWithoutNativeAd(int indexWithNativeAd) {
        int shiftedIndexWithoutNativeAd = indexWithNativeAd;
        for (int nativeAdIndex : this.mNativeAdsIndexes) {
            if (indexWithNativeAd >= nativeAdIndex) {
                shiftedIndexWithoutNativeAd--;
            }
        }
        return shiftedIndexWithoutNativeAd;
    }

    public int shiftIndexWithNativeAd(int indexWithoutNativeAd) {
        int shiftedIndexWithNativeAd = indexWithoutNativeAd;
        for (int nativeAdIndex : this.mNativeAdsIndexes) {
            if (shiftedIndexWithNativeAd >= nativeAdIndex) {
                shiftedIndexWithNativeAd++;
            }
        }
        return shiftedIndexWithNativeAd;
    }

    public int numberOfNativeAdsUntilIndex(int indexWithNativeAd) {
        int numberOfNativeAds = 0;
        for (int nativeAdIndex : this.mNativeAdsIndexes) {
            if (indexWithNativeAd >= nativeAdIndex) {
                numberOfNativeAds++;
            }
        }
        return numberOfNativeAds;
    }
}
