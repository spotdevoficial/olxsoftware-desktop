package com.schibsted.scm.nextgenapp.nativeads.strategy.impl;

import com.schibsted.scm.nextgenapp.nativeads.strategy.NativeAdStrategy;
import com.schibsted.scm.nextgenapp.nativeads.strategy.SinglePageListingNativeAdStrategy;

public class MultiPageListingNativeAdStrategy implements NativeAdStrategy {
    private final int mAdsByPage;
    private final SinglePageListingNativeAdStrategy mSinglePageListingStrategy;

    public MultiPageListingNativeAdStrategy(int mAdsByPage, SinglePageListingNativeAdStrategy mSinglePageListingStrategy) {
        this.mAdsByPage = mAdsByPage;
        this.mSinglePageListingStrategy = mSinglePageListingStrategy;
    }

    private int numberOfNativeAdsPerPage() {
        return this.mSinglePageListingStrategy.numberOfNativeAdsUntilIndex(this.mAdsByPage - 1);
    }

    private int quantityOfAdsAndNativeAdsPerFullPage() {
        return this.mAdsByPage + numberOfNativeAdsPerPage();
    }

    public boolean isIndexANativeAd(int indexWithNativeAd) {
        return this.mSinglePageListingStrategy.isIndexANativeAd(indexWithNativeAd % quantityOfAdsAndNativeAdsPerFullPage());
    }

    public int shiftIndexWithoutNativeAd(int indexWithNativeAd) {
        int quantityOfAdsAndNativeAdsPerFullPage = quantityOfAdsAndNativeAdsPerFullPage();
        int quantityOfPages = indexWithNativeAd / quantityOfAdsAndNativeAdsPerFullPage;
        return (this.mAdsByPage * quantityOfPages) + this.mSinglePageListingStrategy.shiftIndexWithoutNativeAd(indexWithNativeAd % quantityOfAdsAndNativeAdsPerFullPage);
    }

    public int shiftIndexWithNativeAd(int indexWithoutNativeAd) {
        int quantityOfPages = indexWithoutNativeAd / this.mAdsByPage;
        return (quantityOfAdsAndNativeAdsPerFullPage() * quantityOfPages) + this.mSinglePageListingStrategy.shiftIndexWithNativeAd(indexWithoutNativeAd % this.mAdsByPage);
    }

    public int numberOfNativeAdsUntilIndex(int indexWithNativeAd) {
        int quantityOfAdsAndNativeAdsPerFullPage = quantityOfAdsAndNativeAdsPerFullPage();
        return (this.mSinglePageListingStrategy.numberOfNativeAdsUntilIndex(quantityOfAdsAndNativeAdsPerFullPage - 1) * (indexWithNativeAd / quantityOfAdsAndNativeAdsPerFullPage)) + this.mSinglePageListingStrategy.numberOfNativeAdsUntilIndex(indexWithNativeAd % quantityOfAdsAndNativeAdsPerFullPage);
    }
}
