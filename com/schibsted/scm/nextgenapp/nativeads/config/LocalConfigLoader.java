package com.schibsted.scm.nextgenapp.nativeads.config;

import android.content.res.Resources;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class LocalConfigLoader implements ConfigLoader {
    private final ObjectMapper mObjectMapper;
    private final Resources mResources;

    /* renamed from: com.schibsted.scm.nextgenapp.nativeads.config.LocalConfigLoader.1 */
    class C13091 extends TypeReference<HashMap<String, String>> {
        C13091() {
        }
    }

    public LocalConfigLoader(Resources resources, ObjectMapper objectMapper) {
        this.mResources = resources;
        this.mObjectMapper = objectMapper;
    }

    public Map<String, Map<String, String>> fetchAdUnits() {
        try {
            return parseAdUnitStream(this.mResources.openRawResource(2131099650));
        } catch (IOException e) {
            Logger.error("LocalConfigLoader", "Failed to parse stream of AdUnits", e);
            return new HashMap();
        }
    }

    public Map<String, String> fetchStates() {
        return fetchMap("states");
    }

    public Map<String, String> fetchTargeting() {
        return fetchMap("targeting");
    }

    private Map<String, String> fetchMap(String jsonNodeName) {
        Exception e;
        try {
            return parseStringMap(this.mResources.openRawResource(2131099650), jsonNodeName);
        } catch (IOException e2) {
            e = e2;
            Logger.error("LocalConfigLoader", "Failed to parse stream of data", e);
            return new HashMap();
        } catch (IllegalArgumentException e3) {
            e = e3;
            Logger.error("LocalConfigLoader", "Failed to parse stream of data", e);
            return new HashMap();
        }
    }

    private Map<String, Map<String, String>> parseAdUnitStream(InputStream inputStream) throws IOException {
        Map<String, Map<String, String>> adUnitMap = new HashMap();
        Iterator<Entry<String, JsonNode>> nodeIterator = this.mObjectMapper.readTree(inputStream).path("categories").fields();
        while (nodeIterator.hasNext()) {
            Entry<String, JsonNode> entry = (Entry) nodeIterator.next();
            String key = (String) entry.getKey();
            JsonNode specificsAdUnitsMap = (JsonNode) entry.getValue();
            Map<String, String> map = new HashMap();
            String listingAdUnit = specificsAdUnitsMap.get(ScreenAdUnits.LISTING.getScreenName()).textValue();
            String adViewAdUnit = specificsAdUnitsMap.get(ScreenAdUnits.AD_VIEW.getScreenName()).textValue();
            map.put(ScreenAdUnits.LISTING.getScreenName(), listingAdUnit);
            map.put(ScreenAdUnits.AD_VIEW.getScreenName(), adViewAdUnit);
            adUnitMap.put(key, map);
        }
        return adUnitMap;
    }

    private Map<String, String> parseStringMap(InputStream inputStream, String nodeName) throws IOException, IllegalArgumentException {
        return (Map) this.mObjectMapper.convertValue(this.mObjectMapper.readTree(inputStream).path(nodeName), new C13091());
    }
}
