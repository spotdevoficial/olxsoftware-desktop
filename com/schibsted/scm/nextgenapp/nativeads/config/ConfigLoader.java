package com.schibsted.scm.nextgenapp.nativeads.config;

import java.util.Map;

public interface ConfigLoader {
    Map<String, Map<String, String>> fetchAdUnits();

    Map<String, String> fetchStates();

    Map<String, String> fetchTargeting();
}
