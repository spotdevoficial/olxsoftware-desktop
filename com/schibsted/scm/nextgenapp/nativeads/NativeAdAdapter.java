package com.schibsted.scm.nextgenapp.nativeads;

import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.AdapterDataObserver;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.schibsted.scm.nextgenapp.adapters.RecyclerViewAdapterDecorator;
import com.schibsted.scm.nextgenapp.nativeads.strategy.NativeAdStrategy;

public class NativeAdAdapter extends RecyclerViewAdapterDecorator implements OnAdBinderUpdatedListener {
    private AsyncAdBinderFactory mAdBinderFactory;
    private final NativeAdStrategy mNativeAdStrategy;
    private AdapterDataObserver mNativeAdapterDataObserver;
    private int mTotalOfNativeAds;

    /* renamed from: com.schibsted.scm.nextgenapp.nativeads.NativeAdAdapter.1 */
    class C13081 extends AdapterDataObserver {
        C13081() {
        }

        public void onChanged() {
            NativeAdAdapter nativeAdAdapter = NativeAdAdapter.this;
            Adapter decoratedAdapter = nativeAdAdapter.getDecoratedAdapter();
            NativeAdAdapter.this.mTotalOfNativeAds = nativeAdAdapter.mNativeAdStrategy.numberOfNativeAdsUntilIndex((decoratedAdapter != null ? decoratedAdapter.getItemCount() : 0) - 1);
            nativeAdAdapter.notifyDataSetChanged();
        }
    }

    public NativeAdAdapter(Adapter<? extends ViewHolder> decoratedAdapter, NativeAdStrategy mNativeAdStrategy) {
        super(decoratedAdapter);
        this.mNativeAdapterDataObserver = new C13081();
        this.mNativeAdStrategy = mNativeAdStrategy;
        this.mTotalOfNativeAds = 0;
        registerAdapterDataObserver(this.mNativeAdapterDataObserver);
    }

    public int getItemCount() {
        return super.getItemCount() + this.mTotalOfNativeAds;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == vd.f504D) {
            return new NativeAdViewHolder((FrameLayout) LayoutInflater.from(parent.getContext()).inflate(2130903203, parent, false));
        }
        return super.onCreateViewHolder(parent, viewType);
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder.getItemViewType() == vd.f504D) {
            onBindNativeAdViewHolder((NativeAdViewHolder) holder, position);
        } else {
            super.onBindViewHolder(holder, this.mNativeAdStrategy.shiftIndexWithoutNativeAd(position));
        }
    }

    private void onBindNativeAdViewHolder(NativeAdViewHolder nativeAdViewHolder, int position) {
        if (this.mAdBinderFactory != null) {
            this.mAdBinderFactory.getAdBinder(position).bind(nativeAdViewHolder);
        }
    }

    public int getItemViewType(int position) {
        if (this.mNativeAdStrategy.isIndexANativeAd(position)) {
            return vd.f504D;
        }
        return super.getItemViewType(this.mNativeAdStrategy.shiftIndexWithoutNativeAd(position));
    }

    public void onAdBinderUpdated(int pos) {
        notifyItemChanged(pos);
    }

    public void onViewRecycled(ViewHolder holder) {
        if (holder.getItemViewType() == vd.f504D) {
            ((FrameLayout) ((NativeAdViewHolder) holder).itemView).removeAllViews();
        }
    }

    public void setAdBinderFactory(AsyncAdBinderFactory adBinderFactory) {
        if (this.mAdBinderFactory != null) {
            this.mAdBinderFactory.setOnAdBinderUpdatedListener(null);
        }
        adBinderFactory.setOnAdBinderUpdatedListener(this);
        this.mAdBinderFactory = adBinderFactory;
    }
}
