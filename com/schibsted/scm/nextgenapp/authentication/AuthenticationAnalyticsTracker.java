package com.schibsted.scm.nextgenapp.authentication;

import com.android.volley.VolleyError;
import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.messages.authentication.LoginByEmailMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.authentication.LoginByFacebookMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.authentication.RegisterByEmailMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.authentication.RegisterByFacebookMessage;

public class AuthenticationAnalyticsTracker {
    private MessageBus mMessageBus;

    public AuthenticationAnalyticsTracker(MessageBus messageBus) {
        this.mMessageBus = messageBus;
    }

    public void onEmailChanged(String email) {
        this.mMessageBus.post(new EventBuilder().setEventType(EventType.CREDENTIALS_CHANGE_EMAIL).setEmail(email).build());
    }

    public void onPasswordChanged() {
        this.mMessageBus.post(new EventBuilder().setEventType(EventType.CREDENTIALS_CHANGE_PASSWORD).build());
    }

    public void onLoginPageView() {
        this.mMessageBus.post(new EventBuilder().setEventType(EventType.CREDENTIALS_VIEW_LOGIN_PAGE).build());
    }

    public void onRegistrationPageView() {
        this.mMessageBus.post(new EventBuilder().setEventType(EventType.PAGE_REGISTER_PAGE).build());
        this.mMessageBus.post(new EventBuilder().setEventType(EventType.CREDENTIALS_VIEW_REGISTER_PAGE).build());
    }

    public void onForgotPasswordClicked() {
        this.mMessageBus.post(new EventBuilder().setEventType(EventType.CLICK_FORGOT_PASSWORD).build());
    }

    public void onRegistrationSubmitFormClicked() {
        this.mMessageBus.post(new RegisterByEmailMessage());
        this.mMessageBus.post(new EventBuilder().setEventType(EventType.CREDENTIALS_CLICK_REGISTER).build());
    }

    public void onLoginSubmitFormClicked() {
        this.mMessageBus.post(new LoginByEmailMessage());
        this.mMessageBus.post(new EventBuilder().setEventType(EventType.CREDENTIALS_CLICK_LOGIN).build());
    }

    public void onAuthorizationError(VolleyError error) {
        this.mMessageBus.post(new EventBuilder().setEventType(EventType.PAGE_REGISTER_PAGE_ERROR).setError(error).build());
    }

    public void onFacebookLoginClicked() {
        this.mMessageBus.post(new LoginByFacebookMessage());
        this.mMessageBus.post(new EventBuilder().setEventType(EventType.CREDENTIALS_CLICK_FACEBOOK).build());
    }

    public void onFacebookRegisterClicked() {
        this.mMessageBus.post(new RegisterByFacebookMessage());
        this.mMessageBus.post(new EventBuilder().setEventType(EventType.CREDENTIALS_CLICK_FACEBOOK).build());
    }
}
