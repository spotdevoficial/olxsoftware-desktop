package com.schibsted.scm.nextgenapp.authentication.registrationwebview;

import android.content.Context;
import android.net.http.SslError;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import com.schibsted.scm.nextgenapp.authentication.registrationwebview.RegisterWebViewContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.authentication.registrationwebview.RegisterWebViewContract.ViewContract;

public class RegisterWebViewView implements ViewContract {
    private PresenterViewContract mPresenter;
    private ProgressBar mProgressBar;
    private View mView;
    private WebView mWebView;

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.registrationwebview.RegisterWebViewView.1 */
    class C11511 extends WebViewClient {
        C11511() {
        }

        public void onPageFinished(WebView view, String url) {
            RegisterWebViewView.this.mPresenter.onUrlLoadingFinished();
            view.scrollTo(0, 0);
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            RegisterWebViewView.this.mPresenter.onUrlLoadingFailed();
        }

        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            RegisterWebViewView.this.mPresenter.onUrlLoadingFailed();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return RegisterWebViewView.this.mPresenter.shouldOverrideUrlLoading(url);
        }
    }

    public RegisterWebViewView(Context context) {
        this.mView = LayoutInflater.from(context).inflate(2130903167, null);
        this.mWebView = (WebView) this.mView.findViewById(2131558815);
        this.mProgressBar = (ProgressBar) this.mView.findViewById(2131558814);
    }

    public View getView() {
        return this.mView;
    }

    public void showProgress() {
        this.mProgressBar.setVisibility(0);
    }

    public void showWebView() {
        this.mProgressBar.setVisibility(8);
        this.mWebView.setVisibility(0);
    }

    public void setPresenter(PresenterViewContract presenter) {
        this.mPresenter = presenter;
    }

    public void loadUrl(String url) {
        this.mWebView.setWebViewClient(new C11511());
        this.mWebView.getSettings().setUseWideViewPort(true);
        this.mWebView.getSettings().setLoadWithOverviewMode(true);
        this.mWebView.getSettings().setBuiltInZoomControls(true);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.getSettings().setDomStorageEnabled(true);
        this.mWebView.loadUrl(url);
    }
}
