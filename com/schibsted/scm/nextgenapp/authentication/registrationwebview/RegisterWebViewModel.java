package com.schibsted.scm.nextgenapp.authentication.registrationwebview;

import com.schibsted.scm.nextgenapp.authentication.registrationwebview.RegisterWebViewContract.ModelContract;
import com.schibsted.scm.nextgenapp.authentication.registrationwebview.RegisterWebViewContract.PresenterModelContract;

public class RegisterWebViewModel implements ModelContract {
    private PresenterModelContract mPresenter;
    private UrlParser mUrlParser;

    public RegisterWebViewModel(UrlParser urlParser) {
        this.mUrlParser = urlParser;
    }

    public void setPresenter(PresenterModelContract presenter) {
        this.mPresenter = presenter;
    }

    public void prepareWebViewData() {
        this.mPresenter.onWebViewDataPrepared("https://m2.olx.com.br/contas/wvcadastro");
    }

    public boolean checkInterceptedURL(String url) {
        if (isTermsOfUseURL(url)) {
            this.mPresenter.onTermsOfUseUrlIntercepted(url);
            return true;
        } else if (!isRegistrationSuccessURL(url)) {
            return false;
        } else {
            this.mPresenter.onRegistrationWithSuccess(getEmail(url));
            return false;
        }
    }

    private boolean isTermsOfUseURL(String url) {
        return url.contains("copyright");
    }

    private String getEmail(String url) {
        return this.mUrlParser.getQueryParameter(url, "email");
    }

    private boolean isRegistrationSuccessURL(String url) {
        return url.startsWith("olx-webview://signup/success");
    }
}
