package com.schibsted.scm.nextgenapp.authentication.registration;

import android.view.View;
import com.facebook.login.widget.LoginButton;

public interface RegisterContract {

    public interface FragmentContract {
        void finishSuccessfully();

        void startTermsOfUseWebViewActivity();
    }

    public interface ModelContract {
        void cancelRegistration();

        void setPresenter(PresenterModelContract presenterModelContract);

        void submitRegistration(String str, String str2, String str3, boolean z);
    }

    public interface PresenterFragmentContract {
        void onUserChangedEmail();
    }

    public interface PresenterModelContract {
        void onAccountCreatedWithoutEmail(String str, String str2);

        void onAuthenticationError();

        void onBadCredentialsError();

        void onEmailNotVerifiedError();

        void onEmailRegistrationError(String str);

        void onEmptyEmailValidationFailed();

        void onEmptyPasswordConfirmationValidationFailed();

        void onEmptyPasswordValidationFailed();

        void onFacebookRegistrationFail();

        void onGenericLoginError();

        void onGenericRegistrationError(String str);

        void onInvalidEmailValidationFailed();

        void onLoginSuccessful();

        void onLogout();

        void onPasswordConfirmationNeeded(String str, String str2);

        void onPasswordDoesNotMatchWithConfirmationValidationFailed();

        void onPasswordRegistrationError(String str);

        void onPasswordTooShortValidationFailed();

        void onRegistrationRequestStarted();

        void onTermsOfUseAcceptationRequired();
    }

    public interface PresenterViewContract {
        void onEmailChanged(String str);

        void onFacebookRegisterClicked();

        void onPasswordChanged(String str);

        void onRegisterSubmitFormClicked();

        void onRegistrationDialogCancelClicked();

        void onTermsOfUseClicked();
    }

    public interface ViewFragmentContract {
        LoginButton getFacebookLoginButton();

        View getView();

        void setEmail(String str);
    }

    public interface ViewPresenterContract {
        void dismissAllDialogs();

        String getEmail();

        String getPassword();

        String getPasswordConfirmation();

        boolean isShowingEmailConfirmationDialog();

        boolean isTermsOfUseAccepted();

        void removeRegisteringDialog();

        void setPasswordError(String str);

        void showAlertCrouton(int i);

        void showAlertCrouton(String str);

        void showEmailConfirmationDialog(String str, String str2);

        void showEmailEmptyError();

        void showEmailInvalidError();

        void showFacebookRegistrationError();

        void showPasswordConfirmationDialog(String str, String str2);

        void showPasswordConfirmationEmptyError();

        void showPasswordEmptyError();

        void showPasswordTooShortError();

        void showPasswordsMustMatchError();

        void showRegisteringDialog();

        void showTermsOfUseAcceptationRequiredDialog();

        void showVerifyInProgressDialog();

        void showVerifyYourEmailDialog();
    }
}
