package com.schibsted.scm.nextgenapp.authentication.login;

import android.view.View;
import com.facebook.login.widget.LoginButton;

public interface LoginContract {

    public interface FragmentContract {
        void finishSuccessfully();

        void showStaticPage(int i, String str);

        void startRegisterActivity(String str);
    }

    public interface ModelContract {
        void cancelLogin();

        String getTermsOfUseUrl();

        void setPresenter(PresenterModelContract presenterModelContract);

        void submitLogin(String str, String str2);
    }

    public interface PresenterFragmentContract {
        void onEmailConfirmed();

        void onRegistrationFailure();

        void onRegistrationSuccessful();

        void onRegistrationSuccessful(String str);
    }

    public interface PresenterModelContract {
        void onAccountCreatedWithoutEmail(String str, String str2);

        void onAuthenticationError();

        void onBadCredentialsError();

        void onEmailLoginError(String str);

        void onEmailNotVerifiedError();

        void onEmptyEmailValidationFailed();

        void onEmptyPasswordValidationFailed();

        void onFacebookLoginFail();

        void onGenericLoginError();

        void onGenericLoginError(String str);

        void onInvalidEmailValidationFailed();

        void onLoginRequestStarted();

        void onLoginSuccessful();

        void onLogout();

        void onPasswordConfirmationNeeded(String str, String str2);

        void onPasswordLoginError(String str);
    }

    public interface PresenterViewContract {
        void onCreateAccountButtonClicked();

        void onEmailChanged(String str);

        void onFacebookLoginClicked();

        void onForgotPasswordButtonClicked();

        void onLoginDialogCancelClicked();

        void onLoginSubmitFormClicked();

        void onPasswordChanged(String str);

        void onTermsOfUseClicked();
    }

    public interface ViewFragmentContract {
        LoginButton getFacebookLoginButton();

        View getView();
    }

    public interface ViewPresenterContract {
        void dismissAllDialogs();

        String getEmail();

        String getPassword();

        boolean isShowingEmailConfirmationDialog();

        void removeLoggingInDialog();

        void setEmail(String str);

        void setPasswordError(String str);

        void showAlertCrouton(int i);

        void showAlertCrouton(String str);

        void showEmailConfirmationDialog(String str, String str2);

        void showEmailEmptyError();

        void showEmailInvalidError();

        void showFacebookLoginError();

        void showForgotPasswordDialog();

        void showLoggingInDialog();

        void showPasswordConfirmationDialog(String str, String str2);

        void showPasswordEmptyError();

        void showRegistrationFailureDialog();

        void showRegistrationSuccessDialog();

        void showVerifyYourEmailDialog();
    }
}
