package com.schibsted.scm.nextgenapp.authentication.login;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.TextView;
import com.facebook.BuildConfig;
import com.facebook.login.widget.LoginButton;
import com.schibsted.scm.nextgenapp.authentication.login.LoginContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.authentication.login.LoginContract.ViewFragmentContract;
import com.schibsted.scm.nextgenapp.authentication.login.LoginContract.ViewPresenterContract;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ConfirmEmailDialogFragment;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ConfirmPasswordDialogFragment;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ForgotPasswordDialogFragment;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.HaltingOperationDialog;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.HaltingOperationDialog.OnAbortListener;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.InfoDialogFragment;
import com.schibsted.scm.nextgenapp.ui.views.FaceliftCrouton;
import com.schibsted.scm.nextgenapp.ui.views.facelift.FaceliftLabelEditText;
import com.schibsted.scm.nextgenapp.ui.views.facelift.FaceliftLinkSpan;
import com.schibsted.scm.nextgenapp.utils.Utils;

public class LoginView implements ViewFragmentContract, ViewPresenterContract {
    private final FaceliftLabelEditText mEmailField;
    private final LoginButton mFacebookLoginButton;
    private final Fragment mFragment;
    private final Button mGoToCreateAccountButton;
    private final FaceliftLabelEditText mPasswordField;
    private PresenterViewContract mPresenter;
    private String mPreviousEmail;
    private String mPreviousPassword;
    private final Button mSubmitButton;
    private final View mView;

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.login.LoginView.1 */
    class C11321 implements OnClickListener {
        C11321() {
        }

        public void onClick(View v) {
            LoginView.this.mPresenter.onFacebookLoginClicked();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.login.LoginView.2 */
    class C11332 implements OnFocusChangeListener {
        C11332() {
        }

        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                Utils.showSoftKeyboard(LoginView.this.getActivity());
                return;
            }
            String email = LoginView.this.mEmailField.getText().toString();
            if (!TextUtils.isEmpty(email)) {
                LoginView.this.mEmailField.hideError();
            }
            if (!email.equals(LoginView.this.mPreviousEmail)) {
                LoginView.this.mPreviousEmail = email;
                LoginView.this.mPresenter.onEmailChanged(email);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.login.LoginView.3 */
    class C11343 implements OnFocusChangeListener {
        C11343() {
        }

        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                String password = LoginView.this.mPasswordField.getText().toString();
                if (!TextUtils.isEmpty(password)) {
                    LoginView.this.mPasswordField.hideError();
                }
                if (!password.equals(LoginView.this.mPreviousPassword)) {
                    LoginView.this.mPreviousPassword = password;
                    LoginView.this.mPresenter.onPasswordChanged(password);
                }
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.login.LoginView.4 */
    class C11354 implements OnClickListener {
        C11354() {
        }

        public void onClick(View v) {
            LoginView.this.mPresenter.onCreateAccountButtonClicked();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.login.LoginView.5 */
    class C11365 implements OnClickListener {
        C11365() {
        }

        public void onClick(View v) {
            LoginView.this.clearErrors();
            LoginView.this.mPresenter.onLoginSubmitFormClicked();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.login.LoginView.6 */
    class C11376 implements OnClickListener {
        C11376() {
        }

        public void onClick(View v) {
            LoginView.this.mPresenter.onForgotPasswordButtonClicked();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.login.LoginView.7 */
    class C11387 extends FaceliftLinkSpan {
        C11387(Context x0) {
            super(x0);
        }

        public void onClick(View view) {
            LoginView.this.mPresenter.onTermsOfUseClicked();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.login.LoginView.8 */
    class C11398 implements OnAbortListener {
        C11398() {
        }

        public void onAbortOperation() {
            LoginView.this.mPresenter.onLoginDialogCancelClicked();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.login.LoginView.9 */
    class C11409 implements OnClickListener {
        final /* synthetic */ InfoDialogFragment val$dialog;

        C11409(InfoDialogFragment infoDialogFragment) {
            this.val$dialog = infoDialogFragment;
        }

        public void onClick(View view) {
            this.val$dialog.dismiss();
        }
    }

    @SuppressLint({"InflateParams"})
    public LoginView(Fragment fragment) {
        this.mPreviousEmail = BuildConfig.VERSION_NAME;
        this.mPreviousPassword = BuildConfig.VERSION_NAME;
        this.mFragment = fragment;
        this.mView = LayoutInflater.from(getActivity()).inflate(2130903164, null);
        this.mEmailField = (FaceliftLabelEditText) this.mView.findViewById(2131558805);
        this.mPasswordField = (FaceliftLabelEditText) this.mView.findViewById(2131558806);
        this.mGoToCreateAccountButton = (Button) this.mView.findViewById(2131558808);
        this.mSubmitButton = (Button) this.mView.findViewById(2131558807);
        this.mFacebookLoginButton = (LoginButton) this.mView.findViewById(2131558618);
        this.mFacebookLoginButton.setOnClickListener(new C11321());
        this.mEmailField.setOnFocusChangeListener(new C11332());
        this.mPasswordField.setOnFocusChangeListener(new C11343());
        this.mGoToCreateAccountButton.setOnClickListener(new C11354());
        this.mSubmitButton.setOnClickListener(new C11365());
        this.mPasswordField.setOnLinkClickListener(new C11376());
        configureTermsOfUseLink();
    }

    public String getEmail() {
        return this.mEmailField.getText().toString().trim();
    }

    public String getPassword() {
        return this.mPasswordField.getText().toString();
    }

    public void showEmailEmptyError() {
        showEmailError(2131165478);
    }

    public void showEmailInvalidError() {
        showEmailError(2131165477);
    }

    private void showEmailError(int messageRes) {
        this.mEmailField.showError(messageRes);
    }

    public void showPasswordEmptyError() {
        this.mPasswordField.showError(2131165481);
    }

    private void configureTermsOfUseLink() {
        TextView termsOfUseTextView = (TextView) this.mView.findViewById(2131558809);
        String link = getActivity().getString(2131165537);
        String termsOfUseText = String.format(getActivity().getString(2131165536), new Object[]{link});
        int indexLink = termsOfUseText.indexOf(link);
        SpannableString termsOfUseSpannable = new SpannableString(termsOfUseText);
        termsOfUseSpannable.setSpan(new C11387(getActivity()), indexLink, link.length() + indexLink, 0);
        termsOfUseTextView.setMovementMethod(LinkMovementMethod.getInstance());
        termsOfUseTextView.setText(termsOfUseSpannable);
    }

    public void showLoggingInDialog() {
        if (getLoggingInDialog() == null && getActivity() != null) {
            HaltingOperationDialog loggingInDialog = HaltingOperationDialog.newInstance();
            loggingInDialog.setTitle(2131165451);
            loggingInDialog.setMessage(2131165450);
            loggingInDialog.setButtonText(2131165340);
            loggingInDialog.setTime(500);
            loggingInDialog.setOnAbortListener(new C11398());
            loggingInDialog.setCancelable(false);
            loggingInDialog.show(getChildFragmentManager(), HaltingOperationDialog.TAG);
        }
    }

    public void removeLoggingInDialog() {
        Fragment loggingInDialog = getLoggingInDialog();
        if (loggingInDialog != null) {
            getChildFragmentManager().beginTransaction().remove(loggingInDialog).commit();
        }
    }

    public void showForgotPasswordDialog() {
        ForgotPasswordDialogFragment dialog = ForgotPasswordDialogFragment.newInstance(getEmail());
        dialog.show(getChildFragmentManager(), dialog.getTag());
    }

    public void showEmailConfirmationDialog(String email, String registrationToken) {
        ConfirmEmailDialogFragment dialog = ConfirmEmailDialogFragment.newInstance(email, registrationToken);
        dialog.setTargetFragment(this.mFragment, 555);
        dialog.show(getChildFragmentManager(), dialog.getFragmentTag());
    }

    public void dismissEmailConfirmationDialog() {
        Fragment confirmEmailDialog = getChildFragmentManager().findFragmentByTag(ConfirmEmailDialogFragment.TAG);
        if (confirmEmailDialog instanceof ConfirmEmailDialogFragment) {
            ((ConfirmEmailDialogFragment) confirmEmailDialog).dismiss();
        }
    }

    public boolean isShowingEmailConfirmationDialog() {
        Fragment confirmEmailDialog = getChildFragmentManager().findFragmentByTag(ConfirmEmailDialogFragment.TAG);
        return (confirmEmailDialog == null || confirmEmailDialog.getView() == null) ? false : true;
    }

    public void showPasswordConfirmationDialog(String email, String registrationToken) {
        ConfirmPasswordDialogFragment dialog = ConfirmPasswordDialogFragment.newInstance(email, registrationToken);
        dialog.setTargetFragment(this.mFragment, 555);
        dialog.show(getChildFragmentManager(), dialog.getFragmentTag());
    }

    public void dismissPasswordConfirmationDialog() {
        Fragment confirmPasswordDialog = getChildFragmentManager().findFragmentByTag(ConfirmPasswordDialogFragment.TAG);
        if (confirmPasswordDialog instanceof ConfirmPasswordDialogFragment) {
            ((ConfirmPasswordDialogFragment) confirmPasswordDialog).dismiss();
        }
    }

    public void showFacebookLoginError() {
        FaceliftCrouton.showAlertText(getActivity(), 2131165594);
    }

    public void dismissAllDialogs() {
        removeLoggingInDialog();
        dismissEmailConfirmationDialog();
        dismissPasswordConfirmationDialog();
    }

    public void showAlertCrouton(int messageRes) {
        showAlertCrouton(this.mFragment.getString(messageRes));
    }

    public void showVerifyYourEmailDialog() {
        showInfoDialog(2131165427, 2131165426);
    }

    private void showInfoDialog(int title, int text) {
        InfoDialogFragment dialog = InfoDialogFragment.newInstance(getActivity().getResources().getString(title), getActivity().getResources().getString(text), 1);
        dialog.setOKClickListener(new C11409(dialog));
        dialog.show(getChildFragmentManager(), "dialog");
    }

    public void setEmail(String email) {
        this.mEmailField.setText(email);
    }

    public void showRegistrationFailureDialog() {
        showInfoDialog(2131165443, 2131165442);
    }

    public void showRegistrationSuccessDialog() {
        showInfoDialog(2131165445, 2131165444);
    }

    public void showAlertCrouton(String message) {
        if (message != null) {
            FaceliftCrouton.showAlertText(getActivity(), message);
        }
    }

    public void setPasswordError(String message) {
        this.mPasswordField.showError(message);
    }

    private HaltingOperationDialog getLoggingInDialog() {
        return (HaltingOperationDialog) getChildFragmentManager().findFragmentByTag(HaltingOperationDialog.TAG);
    }

    private FragmentManager getChildFragmentManager() {
        return this.mFragment.getChildFragmentManager();
    }

    public LoginButton getFacebookLoginButton() {
        return this.mFacebookLoginButton;
    }

    private void clearErrors() {
        if (this.mEmailField != null) {
            this.mEmailField.hideError();
        }
        if (this.mPasswordField != null) {
            this.mPasswordField.hideError();
        }
    }

    private Activity getActivity() {
        return this.mFragment.getActivity();
    }

    public View getView() {
        return this.mView;
    }

    public void setPresenter(PresenterViewContract presenter) {
        this.mPresenter = presenter;
    }
}
