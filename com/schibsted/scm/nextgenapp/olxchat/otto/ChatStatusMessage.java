package com.schibsted.scm.nextgenapp.olxchat.otto;

public class ChatStatusMessage {
    private Status mStatus;

    public enum Status {
        DELETED
    }

    public ChatStatusMessage(Status status) {
        this.mStatus = status;
    }

    public Status getStatus() {
        return this.mStatus;
    }
}
