package com.schibsted.scm.nextgenapp.olxchat.socket;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class NaiveSSLContext {

    private static class NaiveTrustManager implements X509TrustManager {
        private NaiveTrustManager() {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        public void checkClientTrusted(X509Certificate[] certs, String authType) {
        }

        public void checkServerTrusted(X509Certificate[] certs, String authType) {
        }
    }

    public static SSLContext getTlsInstance() throws NoSuchAlgorithmException {
        return init(SSLContext.getInstance("TLS"));
    }

    private static SSLContext init(SSLContext context) {
        try {
            context.init(null, new TrustManager[]{new NaiveTrustManager()}, null);
            return context;
        } catch (KeyManagementException e) {
            throw new RuntimeException("Failed to initialize an SSLContext.", e);
        }
    }
}
