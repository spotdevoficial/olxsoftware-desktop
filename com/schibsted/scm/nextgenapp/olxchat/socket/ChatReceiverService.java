package com.schibsted.scm.nextgenapp.olxchat.socket;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.schibsted.scm.nextgenapp.olxchat.otto.ChatBus;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;
import com.squareup.otto.Subscribe;
import com.urbanairship.C1608R;

public class ChatReceiverService extends Service {
    private static final String TAG;
    private ChatWebSocket mChatWebSocket;
    private final Object mSocketLock;

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.socket.ChatReceiverService.1 */
    class C13611 implements Runnable {
        C13611() {
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            r8 = this;
            r4 = com.schibsted.scm.nextgenapp.C1049M.getAccountManager();	 Catch:{ RuntimeException -> 0x0029, WebSocketException -> 0x006e, NoSuchAlgorithmException -> 0x006a, IOException -> 0x006c, JSONException -> 0x0068 }
            r0 = r4.getAccessToken();	 Catch:{ RuntimeException -> 0x0029, WebSocketException -> 0x006e, NoSuchAlgorithmException -> 0x006a, IOException -> 0x006c, JSONException -> 0x0068 }
            r1 = "10.6.1.0";
            r2 = new com.schibsted.scm.nextgenapp.olxchat.socket.SocketAuthJSONObject;	 Catch:{ RuntimeException -> 0x0029, WebSocketException -> 0x006e, NoSuchAlgorithmException -> 0x006a, IOException -> 0x006c, JSONException -> 0x0068 }
            r2.<init>(r0, r1);	 Catch:{ RuntimeException -> 0x0029, WebSocketException -> 0x006e, NoSuchAlgorithmException -> 0x006a, IOException -> 0x006c, JSONException -> 0x0068 }
            r4 = com.schibsted.scm.nextgenapp.olxchat.socket.ChatReceiverService.this;	 Catch:{ RuntimeException -> 0x0029, WebSocketException -> 0x006e, NoSuchAlgorithmException -> 0x006a, IOException -> 0x006c, JSONException -> 0x0068 }
            r5 = r4.mSocketLock;	 Catch:{ RuntimeException -> 0x0029, WebSocketException -> 0x006e, NoSuchAlgorithmException -> 0x006a, IOException -> 0x006c, JSONException -> 0x0068 }
            monitor-enter(r5);	 Catch:{ RuntimeException -> 0x0029, WebSocketException -> 0x006e, NoSuchAlgorithmException -> 0x006a, IOException -> 0x006c, JSONException -> 0x0068 }
            r4 = com.schibsted.scm.nextgenapp.olxchat.socket.ChatReceiverService.this;	 Catch:{ all -> 0x0026 }
            r6 = new com.schibsted.scm.nextgenapp.olxchat.socket.ChatWebSocket;	 Catch:{ all -> 0x0026 }
            r7 = com.schibsted.scm.nextgenapp.olxchat.otto.ChatBus.getInstance();	 Catch:{ all -> 0x0026 }
            r6.<init>(r2, r7);	 Catch:{ all -> 0x0026 }
            r4.mChatWebSocket = r6;	 Catch:{ all -> 0x0026 }
            monitor-exit(r5);	 Catch:{ all -> 0x0026 }
        L_0x0025:
            return;
        L_0x0026:
            r4 = move-exception;
            monitor-exit(r5);	 Catch:{ all -> 0x0026 }
            throw r4;	 Catch:{ RuntimeException -> 0x0029, WebSocketException -> 0x006e, NoSuchAlgorithmException -> 0x006a, IOException -> 0x006c, JSONException -> 0x0068 }
        L_0x0029:
            r3 = move-exception;
        L_0x002a:
            r4 = com.schibsted.scm.nextgenapp.olxchat.socket.ChatReceiverService.TAG;
            r5 = new java.lang.StringBuilder;
            r5.<init>();
            r6 = "Could not start ";
            r5 = r5.append(r6);
            r6 = com.schibsted.scm.nextgenapp.olxchat.socket.ChatReceiverService.class;
            r6 = r6.getSimpleName();
            r5 = r5.append(r6);
            r6 = " because of a ";
            r5 = r5.append(r6);
            r6 = r3.getClass();
            r6 = r6.getSimpleName();
            r5 = r5.append(r6);
            r6 = ".";
            r5 = r5.append(r6);
            r5 = r5.toString();
            com.schibsted.scm.nextgenapp.utils.logger.Logger.error(r4, r5, r3);
            r4 = com.schibsted.scm.nextgenapp.olxchat.socket.ChatReceiverService.this;
            r4.disconnect();
            goto L_0x0025;
        L_0x0068:
            r3 = move-exception;
            goto L_0x002a;
        L_0x006a:
            r3 = move-exception;
            goto L_0x002a;
        L_0x006c:
            r3 = move-exception;
            goto L_0x002a;
        L_0x006e:
            r3 = move-exception;
            goto L_0x002a;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.schibsted.scm.nextgenapp.olxchat.socket.ChatReceiverService.1.run():void");
        }
    }

    public static class ChatServiceActionEvent {
        private final int mAction;

        public ChatServiceActionEvent(int action) {
            this.mAction = action;
        }

        public int getAction() {
            return this.mAction;
        }
    }

    public class ChatServiceCreatedMessage {
    }

    public ChatReceiverService() {
        this.mSocketLock = new Object();
    }

    static {
        TAG = ChatReceiverService.class.getSimpleName();
    }

    public void onCreate() {
        super.onCreate();
        ChatBus.getInstance().register(this);
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        ChatBus.getInstance().post(new ChatServiceCreatedMessage());
        return 1;
    }

    public void onDestroy() {
        super.onDestroy();
        ChatBus.getInstance().unregister(this);
    }

    private void connect() {
        synchronized (this.mSocketLock) {
            boolean needToCreateNewSocket = this.mChatWebSocket == null;
        }
        if (needToCreateNewSocket) {
            new Thread(new C13611()).start();
        }
    }

    private void disconnect() {
        try {
            synchronized (this.mSocketLock) {
                if (this.mChatWebSocket != null) {
                    this.mChatWebSocket.close();
                    this.mChatWebSocket = null;
                }
            }
        } catch (RuntimeException e) {
            CrashAnalytics.logException(e);
        }
    }

    @Subscribe
    public void onChatServiceActionEvent(ChatServiceActionEvent event) {
        switch (event.getAction()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                connect();
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                disconnect();
            default:
        }
    }
}
