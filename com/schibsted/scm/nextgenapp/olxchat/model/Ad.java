package com.schibsted.scm.nextgenapp.olxchat.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class Ad implements DataModel {
    public static Creator<Ad> CREATOR;
    @JsonProperty("listId")
    private String mListId;
    @JsonProperty("photo")
    private String mPhotoUrl;
    @JsonProperty("price")
    private String mPrice;
    @JsonProperty("title")
    private String mTitle;

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.model.Ad.1 */
    static class C13461 implements Creator<Ad> {
        C13461() {
        }

        public Ad createFromParcel(Parcel source) {
            return new Ad(source);
        }

        public Ad[] newArray(int size) {
            return new Ad[size];
        }
    }

    public String getTitle() {
        return this.mTitle;
    }

    public String getPrice() {
        return this.mPrice;
    }

    public String getPhotoUrl() {
        return this.mPhotoUrl;
    }

    public String getListId() {
        return this.mListId;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.mTitle).writeString(this.mPrice).writeString(this.mPhotoUrl).writeString(this.mListId);
    }

    public Ad(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.mTitle = reader.readString();
        this.mPrice = reader.readString();
        this.mPhotoUrl = reader.readString();
        this.mListId = reader.readString();
    }

    static {
        CREATOR = new C13461();
    }
}
