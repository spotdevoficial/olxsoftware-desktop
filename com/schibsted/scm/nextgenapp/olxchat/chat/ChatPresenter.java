package com.schibsted.scm.nextgenapp.olxchat.chat;

import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.bus.messages.OpenWebSocketMessage;
import com.schibsted.scm.nextgenapp.olxchat.ChatAnalyticsTracker;
import com.schibsted.scm.nextgenapp.olxchat.chat.ChatContract.FragmentContract;
import com.schibsted.scm.nextgenapp.olxchat.chat.ChatContract.ModelContract;
import com.schibsted.scm.nextgenapp.olxchat.chat.ChatContract.PresenterFragmentContract;
import com.schibsted.scm.nextgenapp.olxchat.chat.ChatContract.PresenterModelContract;
import com.schibsted.scm.nextgenapp.olxchat.chat.ChatContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.olxchat.chat.ChatContract.ViewContract;
import com.schibsted.scm.nextgenapp.olxchat.model.Ad;
import com.schibsted.scm.nextgenapp.olxchat.model.Chat;
import com.schibsted.scm.nextgenapp.olxchat.model.Message;
import com.schibsted.scm.nextgenapp.olxchat.model.error.CreateMessageError;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import java.util.List;

public class ChatPresenter implements PresenterFragmentContract, PresenterModelContract, PresenterViewContract {
    private ChatAnalyticsTracker mAnalyticsTracker;
    private FragmentContract mFragment;
    private final MessageBus mMessageBus;
    private ModelContract mModel;
    private ViewContract mView;

    public ChatPresenter(ModelContract model, ViewContract view, FragmentContract fragment, MessageBus messageBus, ChatAnalyticsTracker analyticsTracker) {
        this.mModel = model;
        this.mView = view;
        this.mFragment = fragment;
        this.mMessageBus = messageBus;
        this.mAnalyticsTracker = analyticsTracker;
    }

    public void initialize(boolean isConnected) {
        updateHeader(this.mModel.getAd());
        setFragmentTitle(this.mModel.getChat());
        if (isConnected) {
            this.mModel.requestMessagesFromChat();
            onNetworkConnected();
            return;
        }
        onNetworkDisconnected();
        onMessagesFetchingError("not connected");
    }

    public void onReportMenuButtonClicked() {
        this.mView.showReportOptions();
        postTrackingUserEvent(EventType.CHAT_TAP_REPORT_USER);
    }

    public void onViewAdMenuButtonClicked() {
        showAd();
    }

    private void updateHeader(Ad ad) {
        if (ad != null) {
            String title = ad.getTitle();
            String price = ad.getPrice();
            String photoUrl = ad.getPhotoUrl();
            this.mView.setHeaderTitle(title);
            this.mView.setHeaderPrice(price);
            this.mView.setHeaderImageUrl(photoUrl);
            this.mView.showHeader();
            return;
        }
        this.mView.hideHeader();
    }

    public void onChatFetched(Chat chat) {
        updateHeader(chat.getAd());
        setFragmentTitle(chat);
    }

    private void setFragmentTitle(Chat chat) {
        if (chat != null && chat.getAd() != null && chat.getContact() != null) {
            this.mFragment.setTitle(chat.getContact().getName());
        }
    }

    public void onChatFetchingError(String reason) {
        this.mView.hideHeader();
        reportPageError(reason);
    }

    private void reportPageError(String reason) {
        Chat chat = this.mModel.getChat();
        if (chat != null) {
            this.mAnalyticsTracker.onChatPageErrorView(chat.getListId(), chat.getId(), reason);
        }
    }

    public void onMessagesFromChatRequestStarted() {
        this.mView.showProgress();
    }

    public void onMessagesFetched(List<Message> messages) {
        this.mView.setMessages(messages);
        this.mView.showMessagesList();
    }

    public void onMessagesFetchingError(String reason) {
        this.mView.hideHeader();
        this.mView.showErrorLayout();
        reportPageError(reason);
    }

    public void onMessageReceived(Message message) {
        this.mView.onMessageAdded();
        Chat chat = this.mModel.getChat();
        if (chat != null) {
            this.mAnalyticsTracker.onChatReceiveMessageViewed(chat.getId(), message.getId());
        }
    }

    public void onNetworkConnected() {
        this.mView.setSendEnabled(true);
    }

    public void onNetworkDisconnected() {
        this.mView.setSendEnabled(false);
    }

    public void onReportUserRequestStarted() {
        this.mView.showReportProgressDialog();
    }

    public void onReportedUser() {
        this.mView.dismissReportingUserProgressDialog();
        this.mView.showReportSuccessMessage();
    }

    public void onReportUserRequestFailed() {
        this.mView.dismissReportingUserProgressDialog();
        this.mView.showReportErrorMessage();
    }

    public void onSendMessageButtonClick(String message) {
        if (!message.trim().isEmpty()) {
            this.mView.clearInputField();
            this.mModel.requestCreateMessage(message);
            postTrackingUserEvent(EventType.CHAT_MESSAGE_TAP_SEND);
        }
    }

    public void onResendMessage(Message message) {
        if (message != null) {
            this.mModel.requestResendMessage(message);
            postTrackingUserEvent(EventType.CHAT_TAP_RETRY_SEND_MESSAGE);
        }
    }

    public void onReportFraudClicked() {
        postReportTrackingEvent(EventType.CHAT_REPORT_USER_FRAUD, this.mModel.getChat());
        this.mView.showReportConfirmationDialog(this.mModel.getReportFraudCause());
    }

    public void onReportSpamClicked() {
        postReportTrackingEvent(EventType.CHAT_REPORT_USER_SPAM, this.mModel.getChat());
        this.mView.showReportConfirmationDialog(this.mModel.getReportSpamCause());
    }

    public void onReportDisrespectfulBehaviorClicked() {
        postReportTrackingEvent(EventType.CHAT_REPORT_USER_BEHAVIOR, this.mModel.getChat());
        this.mView.showReportConfirmationDialog(this.mModel.getReportDisrespectfulBehaviorCause());
    }

    private void postTrackingUserEvent(EventType event) {
        Chat chat = this.mModel.getChat();
        if (chat != null) {
            this.mMessageBus.post(new EventBuilder().setEventType(event).setChatId(chat.getId()).build());
        }
    }

    public void onReport(String cause) {
        postReportTrackingEvent(EventType.CHAT_REPORT_TAP_SEND, this.mModel.getChat());
        this.mModel.requestReportUser(cause);
    }

    public void onReportCanceled() {
        postReportTrackingEvent(EventType.CHAT_REPORT_USER_CANCEL, this.mModel.getChat());
    }

    private void postReportTrackingEvent(EventType type, Chat chat) {
        if (chat != null) {
            this.mMessageBus.post(new EventBuilder().setEventType(type).setChatId(chat.getId()).setReportedId(chat.getContact().getAccountId()).build());
        }
    }

    public boolean isCurrentUserTheAuthor(Message message) {
        return this.mModel.isAuthor(message);
    }

    public void onRetryButtonClicked() {
        this.mModel.refreshData();
    }

    public void onHeaderClicked() {
        postTrackingUserEvent(EventType.CHAT_TAP_HEADER);
        showAd();
    }

    private void showAd() {
        this.mFragment.showAd(this.mModel.getAd());
    }

    public void onCreateMessageRequestStarted(Message message) {
        this.mView.onMessageAdded();
    }

    public void onMessageCreated(Message message) {
        this.mView.refreshMessageList();
        this.mMessageBus.post(new OpenWebSocketMessage());
        Chat chat = this.mModel.getChat();
        if (chat != null) {
            this.mAnalyticsTracker.onChatSendMessageViewed(chat.getListId(), chat.getId(), message.getId());
        }
    }

    public void onMessageUpdated() {
        this.mView.refreshMessageList();
    }

    public void onMessageCreationError(CreateMessageError error, String messageId) {
        this.mView.refreshMessageList();
        if (error == CreateMessageError.RECEIVER_IS_BANNED || error == CreateMessageError.RECEIVER_IS_BLOCKED_BY_YOU) {
            this.mView.showMessageSendingError();
        }
        Chat chat = this.mModel.getChat();
        if (chat != null) {
            this.mAnalyticsTracker.onChatSendMessageFailed(chat.getListId(), chat.getId(), messageId, error.name());
        }
    }

    public void onChatAdReply(int accountId, String listId, String chatId, String messageId) {
        this.mMessageBus.post(new EventBuilder().setEventType(EventType.CHAT_AD_REPLY).setChatId(chatId).setAccountId(String.valueOf(accountId)).setMessageId(messageId).setListId(listId).build());
    }
}
