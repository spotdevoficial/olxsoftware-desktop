package com.schibsted.scm.nextgenapp.olxchat.chat;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.cocosw.bottomsheet.BottomSheet;
import com.facebook.BuildConfig;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.schibsted.scm.nextgenapp.olxchat.chat.ChatAdapter.ChatAdapterMessageAuthorListener;
import com.schibsted.scm.nextgenapp.olxchat.chat.ChatAdapter.OnMessageSendingFailedAlertClickListener;
import com.schibsted.scm.nextgenapp.olxchat.chat.ChatContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.olxchat.chat.ChatContract.ViewContract;
import com.schibsted.scm.nextgenapp.olxchat.model.Message;
import com.schibsted.scm.nextgenapp.ui.DialogCreator;
import com.schibsted.scm.nextgenapp.ui.DialogCreator.DialogButton;
import java.util.List;

public class ChatView implements ChatAdapterMessageAuthorListener, OnMessageSendingFailedAlertClickListener, ViewContract {
    private ChatAdapter mChatAdapter;
    private DisplayImageOptions mDisplayImageOptions;
    private final ViewGroup mErrorLayout;
    private final Button mErrorLayoutButton;
    private final ViewGroup mFooterLayout;
    private final ImageView mHeaderImageView;
    private final ViewGroup mHeaderLayout;
    private final TextView mHeaderPriceTextView;
    private final TextView mHeaderTitleTextView;
    private ImageLoader mImageLoader;
    private final EditText mInputEditText;
    private final RecyclerView mMessagesRecyclerView;
    private PresenterViewContract mPresenter;
    private final ViewGroup mProgressLayout;
    private boolean mReportOptionSelected;
    private Dialog mReportProgressDialog;
    private final ImageButton mSendButton;
    private final View mView;

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chat.ChatView.1 */
    class C13241 implements OnClickListener {
        C13241() {
        }

        public void onClick(View v) {
            ChatView.this.mPresenter.onSendMessageButtonClick(ChatView.this.mInputEditText.getText().toString());
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chat.ChatView.2 */
    class C13252 implements OnClickListener {
        C13252() {
        }

        public void onClick(View view) {
            ChatView.this.mPresenter.onRetryButtonClicked();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chat.ChatView.3 */
    class C13263 implements OnClickListener {
        C13263() {
        }

        public void onClick(View view) {
            ChatView.this.mPresenter.onHeaderClicked();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chat.ChatView.4 */
    class C13274 implements OnDismissListener {
        C13274() {
        }

        public void onDismiss(DialogInterface dialogInterface) {
            if (!ChatView.this.mReportOptionSelected) {
                ChatView.this.mPresenter.onReportCanceled();
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chat.ChatView.5 */
    class C13285 implements DialogInterface.OnClickListener {
        C13285() {
        }

        public void onClick(DialogInterface dialog, int which) {
            ChatView.this.mReportOptionSelected = true;
            switch (which) {
                case 2131558916:
                    ChatView.this.mPresenter.onReportFraudClicked();
                case 2131558917:
                    ChatView.this.mPresenter.onReportSpamClicked();
                case 2131558918:
                    ChatView.this.mPresenter.onReportDisrespectfulBehaviorClicked();
                default:
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chat.ChatView.6 */
    class C13296 implements OnDismissListener {
        C13296() {
        }

        public void onDismiss(DialogInterface dialogInterface) {
            ChatView.this.mPresenter.onReportCanceled();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chat.ChatView.7 */
    class C13307 implements OnClickListener {
        C13307() {
        }

        public void onClick(View v) {
            ChatView.this.mPresenter.onReportCanceled();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chat.ChatView.8 */
    class C13318 implements OnClickListener {
        final /* synthetic */ String val$cause;

        C13318(String str) {
            this.val$cause = str;
        }

        public void onClick(View v) {
            ChatView.this.mPresenter.onReport(this.val$cause);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chat.ChatView.9 */
    class C13329 implements OnClickListener {
        final /* synthetic */ Message val$message;

        C13329(Message message) {
            this.val$message = message;
        }

        public void onClick(View v) {
            ChatView.this.mPresenter.onResendMessage(this.val$message);
        }
    }

    public ChatView(Context context, ImageLoader imageLoader) {
        this.mImageLoader = imageLoader;
        this.mDisplayImageOptions = new Builder().showImageOnFail(2130837701).showImageForEmptyUri(2130837701).showImageOnLoading(2130837701).cacheInMemory(true).cacheOnDisk(true).build();
        this.mView = LayoutInflater.from(context).inflate(2130903159, null);
        this.mInputEditText = (EditText) this.mView.findViewById(2131558785);
        this.mSendButton = (ImageButton) this.mView.findViewById(2131558786);
        this.mHeaderLayout = (ViewGroup) this.mView.findViewById(2131558777);
        this.mHeaderImageView = (ImageView) this.mView.findViewById(2131558778);
        this.mHeaderTitleTextView = (TextView) this.mView.findViewById(2131558780);
        this.mHeaderPriceTextView = (TextView) this.mView.findViewById(2131558781);
        this.mProgressLayout = (ViewGroup) this.mView.findViewById(2131558784);
        this.mErrorLayout = (ViewGroup) this.mView.findViewById(2131558775);
        this.mFooterLayout = (ViewGroup) this.mView.findViewById(2131558783);
        this.mMessagesRecyclerView = (RecyclerView) this.mView.findViewById(2131558782);
        this.mErrorLayoutButton = (Button) this.mView.findViewById(2131558776);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(1);
        layoutManager.setStackFromEnd(true);
        this.mMessagesRecyclerView.setLayoutManager(layoutManager);
        this.mInputEditText.setTypeface(Typeface.createFromAsset(context.getAssets(), context.getString(2131165265)));
        this.mSendButton.setOnClickListener(new C13241());
        this.mErrorLayoutButton.setOnClickListener(new C13252());
        this.mHeaderLayout.setOnClickListener(new C13263());
        this.mHeaderLayout.setVisibility(8);
        this.mFooterLayout.setVisibility(8);
        this.mErrorLayout.setVisibility(8);
    }

    public void clearInputField() {
        this.mInputEditText.setText(BuildConfig.VERSION_NAME);
    }

    public void setHeaderTitle(String title) {
        this.mHeaderTitleTextView.setText(title);
    }

    public void setHeaderPrice(String price) {
        this.mHeaderPriceTextView.setText(price);
    }

    public void setHeaderImageUrl(String imageUrl) {
        this.mImageLoader.displayImage(imageUrl, this.mHeaderImageView, this.mDisplayImageOptions);
    }

    public void showHeader() {
        this.mHeaderLayout.setVisibility(0);
    }

    public void hideHeader() {
        this.mHeaderLayout.setVisibility(8);
    }

    public void showProgress() {
        setMessagesLayoutVisibility(8);
        this.mProgressLayout.setVisibility(0);
        this.mErrorLayout.setVisibility(8);
    }

    public void showMessagesList() {
        setMessagesLayoutVisibility(0);
        this.mProgressLayout.setVisibility(8);
        this.mErrorLayout.setVisibility(8);
    }

    public void showErrorLayout() {
        setMessagesLayoutVisibility(8);
        this.mProgressLayout.setVisibility(8);
        this.mErrorLayout.setVisibility(0);
    }

    private void setMessagesLayoutVisibility(int visibility) {
        this.mHeaderLayout.setVisibility(visibility);
        this.mFooterLayout.setVisibility(visibility);
        this.mMessagesRecyclerView.setVisibility(visibility);
    }

    public void setMessages(List<Message> messages) {
        this.mChatAdapter = new ChatAdapter(messages, this, this);
        this.mMessagesRecyclerView.setAdapter(this.mChatAdapter);
    }

    public void onMessageAdded() {
        if (this.mChatAdapter != null) {
            this.mChatAdapter.notifyDataSetChanged();
            this.mMessagesRecyclerView.scrollToPosition(this.mChatAdapter.getItemCount() - 1);
        }
    }

    public void refreshMessageList() {
        if (this.mChatAdapter != null) {
            this.mChatAdapter.notifyDataSetChanged();
        }
    }

    public void showReportProgressDialog() {
        this.mReportProgressDialog = new DialogCreator(getContext()).createCircleProgressDialog(getContext().getString(2131165376), false);
        this.mReportProgressDialog.show();
    }

    public void dismissReportingUserProgressDialog() {
        if (this.mReportProgressDialog != null && this.mReportProgressDialog.isShowing()) {
            this.mReportProgressDialog.dismiss();
        }
    }

    public void showReportSuccessMessage() {
        Snackbar.make(this.mView, 2131165378, -1).show();
    }

    public void showReportErrorMessage() {
        Snackbar.make(this.mView, 2131165377, -1).show();
    }

    public void showMessageSendingError() {
        DialogCreator dialogCreator = new DialogCreator(getContext());
        dialogCreator.getClass();
        dialogCreator.create(getContext().getString(2131165368), new DialogButton(dialogCreator, 17039370), null).show();
    }

    public void showReportOptions() {
        this.mReportOptionSelected = false;
        new BottomSheet.Builder(getContext(), 2131296532).title(2131165374).sheet(2131623941).listener(new C13285()).setOnDismissListener(new C13274()).show();
    }

    public void showReportConfirmationDialog(String cause) {
        OnDismissListener dialogDismissListener = new C13296();
        OnClickListener negativeButtonClickListener = new C13307();
        OnClickListener positiveButtonClickListener = new C13318(cause);
        DialogCreator dialogCreator = new DialogCreator(getContext());
        dialogCreator.getClass();
        DialogButton negativeButton = new DialogButton(2131165614, negativeButtonClickListener);
        dialogCreator.getClass();
        dialogCreator.create(getContext().getString(2131165370), new DialogButton(2131165658, positiveButtonClickListener), negativeButton, dialogDismissListener).show();
    }

    private Context getContext() {
        return this.mView.getContext();
    }

    public void setSendEnabled(boolean enabled) {
        this.mSendButton.setEnabled(enabled);
    }

    public View getView() {
        return this.mView;
    }

    public void setPresenter(PresenterViewContract presenter) {
        this.mPresenter = presenter;
    }

    public boolean isCurrentUserTheAuthor(Message message) {
        return this.mPresenter.isCurrentUserTheAuthor(message);
    }

    public void onMessageSendingFailedAlertClick(Message message) {
        OnClickListener positiveButtonClickListener = new C13329(message);
        Context context = this.mView.getContext();
        DialogCreator dialogCreator = new DialogCreator(context);
        dialogCreator.getClass();
        DialogButton negativeButton = new DialogButton(dialogCreator, 2131165614);
        dialogCreator.getClass();
        dialogCreator.create(context.getString(2131165379), new DialogButton(2131165658, positiveButtonClickListener), negativeButton, null).show();
    }
}
