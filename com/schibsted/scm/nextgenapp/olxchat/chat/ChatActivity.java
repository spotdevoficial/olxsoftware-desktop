package com.schibsted.scm.nextgenapp.olxchat.chat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.MenuItem;
import com.schibsted.scm.nextgenapp.activities.SingleFragmentActivity;
import com.schibsted.scm.nextgenapp.olxchat.model.Chat;

public class ChatActivity extends SingleFragmentActivity {
    private String mAuthHeader;
    private String mChatId;
    private int mSenderId;

    protected Fragment createFragment() {
        this.mAuthHeader = getIntent().getStringExtra("kIntentAuthHeader");
        this.mSenderId = getIntent().getIntExtra("kIntentSenderId", -1);
        if (getIntent().getParcelableExtra("kIntentChat") != null) {
            Chat chat = (Chat) getIntent().getParcelableExtra("kIntentChat");
            this.mChatId = chat.getId();
            return ChatFragment.newInstance(this.mAuthHeader, this.mSenderId, chat);
        }
        this.mChatId = getIntent().getStringExtra("kIntentChatId");
        return ChatFragment.newInstance(this.mAuthHeader, this.mSenderId, this.mChatId);
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("kIntentAuthHeader", this.mAuthHeader);
        outState.putInt("kIntentSenderId", this.mSenderId);
        outState.putString("kIntentChatId", this.mChatId);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static void start(Context context, String authHeader, int senderId, String chatId) {
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra("kIntentAuthHeader", authHeader);
        intent.putExtra("kIntentSenderId", senderId);
        intent.putExtra("kIntentChatId", chatId);
        context.startActivity(intent);
    }

    public static void start(Context context, String authHeader, int senderId, Chat chat) {
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra("kIntentAuthHeader", authHeader);
        intent.putExtra("kIntentSenderId", senderId);
        intent.putExtra("kIntentChat", chat);
        context.startActivity(intent);
    }
}
