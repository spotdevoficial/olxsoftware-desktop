package com.schibsted.scm.nextgenapp.olxchat.network;

public interface NetworkResponse {
    int getStatus();
}
