package com.schibsted.scm.nextgenapp.olxchat.network;

public interface NetworkError {
    <T> T getBodyAs(Class<T> cls);

    NetworkResponse getResponse();
}
