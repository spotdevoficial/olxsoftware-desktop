package com.schibsted.scm.nextgenapp.olxchat.network.parser;

import java.io.IOException;

public interface ResponseParser<T> {
    T parseData(byte[] bArr) throws IOException;
}
