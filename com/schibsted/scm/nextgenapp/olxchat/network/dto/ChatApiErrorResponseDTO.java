package com.schibsted.scm.nextgenapp.olxchat.network.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ChatApiErrorResponseDTO {
    @JsonProperty("error")
    private ChatApiErrorDTO mError;

    public static class ChatApiErrorDTO {
        @JsonProperty("code")
        private String code;
    }

    public String getCode() {
        if (this.mError != null) {
            return this.mError.code;
        }
        return null;
    }
}
