package com.schibsted.scm.nextgenapp.olxchat.network.dto;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.Date;

public class MessageTimestampDTO implements DataModel {
    public static Creator<MessageTimestampDTO> CREATOR;
    @JsonProperty("timestamp")
    private Date mMessageTimestamp;

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.network.dto.MessageTimestampDTO.1 */
    static class C13551 implements Creator<MessageTimestampDTO> {
        C13551() {
        }

        public MessageTimestampDTO createFromParcel(Parcel source) {
            return new MessageTimestampDTO(source);
        }

        public MessageTimestampDTO[] newArray(int size) {
            return new MessageTimestampDTO[size];
        }
    }

    protected MessageTimestampDTO(Parcel in) {
        this.mMessageTimestamp = new Date(new ParcelReader(in).readLong().longValue());
    }

    @JsonIgnore
    public long getMessageTimestamp() {
        return this.mMessageTimestamp.getTime();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeLong(Long.valueOf(this.mMessageTimestamp.getTime()));
    }

    static {
        CREATOR = new C13551();
    }
}
