package com.schibsted.scm.nextgenapp.olxchat.chatlist;

import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.tracking.messages.chatlist.ChatListViewError;
import com.schibsted.scm.nextgenapp.tracking.messages.chatlist.ChatListViewEvent;
import com.schibsted.scm.nextgenapp.tracking.messages.chatlist.ChatPageClickEvent;
import com.schibsted.scm.nextgenapp.tracking.messages.chatlist.ConfirmDeleteChatClick;
import com.schibsted.scm.nextgenapp.tracking.messages.chatlist.DeleteChatClick;

public class ChatListAnalyticsTracker {
    private MessageBus mMessageBus;

    public ChatListAnalyticsTracker(MessageBus messageBus) {
        this.mMessageBus = messageBus;
    }

    public void onListView() {
        this.mMessageBus.post(new ChatListViewEvent());
    }

    public void onChatPageClick(String listId, String chatId) {
        this.mMessageBus.post(new ChatPageClickEvent(listId, chatId));
    }

    public void onListViewError(String reason) {
        this.mMessageBus.post(new ChatListViewError(reason));
    }

    public void onChatAnswerDeleteChatClick(String listId, String chatId, boolean optionSelected) {
        this.mMessageBus.post(new ConfirmDeleteChatClick(listId, chatId, optionSelected));
    }

    public void onChatDeleteChatClicked(String listId, String chatId) {
        this.mMessageBus.post(new DeleteChatClick(listId, chatId));
    }
}
