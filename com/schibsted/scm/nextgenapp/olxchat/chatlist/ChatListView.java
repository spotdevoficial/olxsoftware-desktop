package com.schibsted.scm.nextgenapp.olxchat.chatlist;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.widget.Button;
import com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListAdapter.ChatListAdapterUnreadStatusListener;
import com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListContract.ViewContract;
import com.schibsted.scm.nextgenapp.olxchat.listener.RecyclerItemClickListener;
import com.schibsted.scm.nextgenapp.olxchat.listener.RecyclerItemClickListener.OnItemClickListener;
import com.schibsted.scm.nextgenapp.olxchat.model.Chat;
import com.schibsted.scm.nextgenapp.ui.DialogCreator;
import com.schibsted.scm.nextgenapp.ui.DialogCreator.DialogButton;
import com.schibsted.scm.nextgenapp.ui.decorators.DividerItemDecoration;
import java.util.List;

public class ChatListView implements ChatListAdapterUnreadStatusListener, ViewContract {
    private final ChatListAnalyticsTracker mAnalyticsTracker;
    private ChatListAdapter mChatListAdapter;
    private ViewGroup mEmptyLayout;
    private Button mEmptyLayoutButton;
    private ViewGroup mErrorLayout;
    private Button mErrorLayoutButton;
    private PresenterViewContract mPresenter;
    private Dialog mProgressDialog;
    private ViewGroup mProgressLayout;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private View mView;

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListView.1 */
    class C13361 implements OnRefreshListener {
        C13361() {
        }

        public void onRefresh() {
            ChatListView.this.mPresenter.onPullToRefresh();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListView.2 */
    class C13372 implements OnItemClickListener {
        C13372() {
        }

        public void onItemClick(View view, int position) {
            ChatListView.this.mPresenter.onChatListItemClick(position);
        }

        public void onItemLongClick(View view, int position) {
            ChatListView.this.mPresenter.onChatListItemLongClick(position);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListView.3 */
    class C13383 implements OnLayoutChangeListener {
        C13383() {
        }

        public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
            ChatListView.this.mRecyclerView.removeOnLayoutChangeListener(this);
            if (ChatListView.this.mAnalyticsTracker != null) {
                ChatListView.this.mAnalyticsTracker.onListView();
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListView.4 */
    class C13394 implements OnClickListener {
        C13394() {
        }

        public void onClick(View v) {
            ChatListView.this.mPresenter.onInsertAnAdButtonClick();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListView.5 */
    class C13405 implements OnClickListener {
        C13405() {
        }

        public void onClick(View view) {
            ChatListView.this.mPresenter.onRetryButtonClicked();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListView.6 */
    class C13416 implements OnDismissListener {
        final /* synthetic */ Chat val$chat;

        C13416(Chat chat) {
            this.val$chat = chat;
        }

        public void onDismiss(DialogInterface dialogInterface) {
            ChatListView.this.mPresenter.onDeleteCanceled(this.val$chat);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListView.7 */
    class C13427 implements OnClickListener {
        final /* synthetic */ Chat val$chat;

        C13427(Chat chat) {
            this.val$chat = chat;
        }

        public void onClick(View v) {
            ChatListView.this.mPresenter.onDeleteCanceled(this.val$chat);
            if (this.val$chat != null) {
                ChatListView.this.mAnalyticsTracker.onChatAnswerDeleteChatClick(this.val$chat.getListId(), this.val$chat.getId(), false);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListView.8 */
    class C13438 implements OnClickListener {
        final /* synthetic */ Chat val$chat;

        C13438(Chat chat) {
            this.val$chat = chat;
        }

        public void onClick(View v) {
            ChatListView.this.mPresenter.onDeleteChat(this.val$chat);
            if (this.val$chat != null) {
                ChatListView.this.mAnalyticsTracker.onChatAnswerDeleteChatClick(this.val$chat.getListId(), this.val$chat.getId(), true);
            }
        }
    }

    public ChatListView(Context context, ChatListAnalyticsTracker analyticsTracker) {
        this.mView = LayoutInflater.from(context).inflate(2130903160, null);
        this.mSwipeRefreshLayout = (SwipeRefreshLayout) this.mView.findViewById(2131558791);
        this.mRecyclerView = (RecyclerView) this.mView.findViewById(2131558792);
        this.mProgressLayout = (ViewGroup) this.mView.findViewById(2131558793);
        this.mEmptyLayout = (ViewGroup) this.mView.findViewById(2131558789);
        this.mErrorLayout = (ViewGroup) this.mView.findViewById(2131558787);
        this.mEmptyLayoutButton = (Button) this.mView.findViewById(2131558790);
        this.mErrorLayoutButton = (Button) this.mView.findViewById(2131558788);
        this.mRecyclerView.addItemDecoration(new DividerItemDecoration(context, 1));
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        this.mSwipeRefreshLayout.setOnRefreshListener(new C13361());
        this.mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this.mRecyclerView, new C13372()));
        this.mRecyclerView.addOnLayoutChangeListener(new C13383());
        this.mEmptyLayoutButton.setOnClickListener(new C13394());
        this.mErrorLayoutButton.setOnClickListener(new C13405());
        this.mSwipeRefreshLayout.setColorSchemeResources(2131493058, 2131493059, 2131493060, 2131493061);
        this.mAnalyticsTracker = analyticsTracker;
    }

    public void setChats(List<Chat> chats) {
        if (this.mView != null) {
            this.mChatListAdapter = new ChatListAdapter(chats, this);
            this.mRecyclerView.setAdapter(this.mChatListAdapter);
        }
    }

    public void showProgress() {
        if (this.mView != null) {
            this.mSwipeRefreshLayout.setVisibility(8);
            this.mProgressLayout.setVisibility(0);
            this.mEmptyLayout.setVisibility(8);
            this.mErrorLayout.setVisibility(8);
        }
    }

    public void showDeletingChatProgressDialog() {
        if (this.mView != null) {
            Context context = this.mView.getContext();
            this.mProgressDialog = new DialogCreator(context).createCircleProgressDialog(context.getString(2131165356), false);
            this.mProgressDialog.show();
        }
    }

    public void dismissDeletingChatProgressDialog() {
        if (this.mProgressDialog != null && this.mProgressDialog.isShowing()) {
            this.mProgressDialog.dismiss();
        }
    }

    public void showChatDeletionErrorMessage() {
        if (this.mView != null) {
            Snackbar.make(this.mView, 2131165357, -1).show();
        }
    }

    public void showListOfChats() {
        if (this.mView != null) {
            this.mSwipeRefreshLayout.setVisibility(0);
            this.mProgressLayout.setVisibility(8);
            this.mEmptyLayout.setVisibility(8);
            this.mErrorLayout.setVisibility(8);
            this.mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public void showChatOperationsDialog(Chat chat) {
        if (this.mView != null) {
            OnDismissListener dialogDismissListener = new C13416(chat);
            OnClickListener negativeButtonClickListener = new C13427(chat);
            OnClickListener positiveButtonClickListener = new C13438(chat);
            Context context = this.mView.getContext();
            DialogCreator dialogCreator = new DialogCreator(context);
            dialogCreator.getClass();
            DialogButton negativeButton = new DialogButton(2131165614, negativeButtonClickListener);
            dialogCreator.getClass();
            dialogCreator.create(context.getString(2131165355), new DialogButton(2131165658, positiveButtonClickListener), negativeButton, dialogDismissListener).show();
            if (chat != null) {
                this.mAnalyticsTracker.onChatDeleteChatClicked(chat.getListId(), chat.getId());
            }
        }
    }

    public View getView() {
        return this.mView;
    }

    public void setPresenter(PresenterViewContract presenter) {
        this.mPresenter = presenter;
    }

    public void showEmptyChatsLayout() {
        if (this.mView != null) {
            this.mSwipeRefreshLayout.setVisibility(8);
            this.mProgressLayout.setVisibility(8);
            this.mErrorLayout.setVisibility(8);
            this.mEmptyLayout.setVisibility(0);
            this.mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public void showErrorLayout() {
        if (this.mView != null) {
            this.mSwipeRefreshLayout.setVisibility(8);
            this.mProgressLayout.setVisibility(8);
            this.mErrorLayout.setVisibility(0);
            this.mEmptyLayout.setVisibility(8);
        }
    }

    public void showErrorNotification() {
        if (this.mView != null) {
            Snackbar.make(this.mView, this.mView.getContext().getText(2131165365), -1).show();
            this.mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public void refreshChatList() {
        if (this.mChatListAdapter != null) {
            this.mChatListAdapter.notifyDataSetChanged();
        }
    }

    public void removeChat(int chatPosition) {
        if (this.mChatListAdapter != null) {
            this.mChatListAdapter.notifyItemRemoved(chatPosition);
        }
    }

    public boolean isUnread(Chat chat) {
        return this.mPresenter.isChatUnread(chat);
    }

    public void destroyView() {
        clearInstanceReferences();
    }

    private void clearInstanceReferences() {
        this.mPresenter = null;
        this.mChatListAdapter = null;
        this.mView = null;
        this.mSwipeRefreshLayout = null;
        this.mRecyclerView = null;
        this.mProgressLayout = null;
        this.mEmptyLayout = null;
        this.mErrorLayout = null;
        this.mEmptyLayoutButton = null;
        this.mErrorLayoutButton = null;
    }
}
