package com.schibsted.scm.nextgenapp.olxchat.chatlist;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.schibsted.scm.nextgenapp.MainApplication;
import com.schibsted.scm.nextgenapp.activities.DrawerActivity;
import com.schibsted.scm.nextgenapp.activities.InsertAdActivity;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListContract.ActivityContract;
import com.schibsted.scm.nextgenapp.olxchat.model.Chat;

public class ChatListActivity extends DrawerActivity implements ActivityContract {
    private String mAuthHeader;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(2131165541);
    }

    protected Fragment createFragment() {
        this.mAuthHeader = getIntent().getStringExtra("kIntentAuthHeader");
        return ChatListFragment.newInstance(this.mAuthHeader);
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("kIntentAuthHeader", this.mAuthHeader);
    }

    public static void start(Context context, String authHeader) {
        Intent intent = new Intent(context, ChatListActivity.class);
        intent.putExtra("kIntentAuthHeader", authHeader);
        context.startActivity(intent);
    }

    public void showChat(Chat chat) {
        ((MainApplication) getApplication()).getOlxChatInstance().showChat((Context) this, chat);
    }

    public void showAdInsertion() {
        startActivity(InsertAdActivity.newIntent(this, ConfigContainer.getConfig().getAdInsertionRequiredAccountFields()));
    }

    public void onLoginCanceled() {
        finish();
    }
}
