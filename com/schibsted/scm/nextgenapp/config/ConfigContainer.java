package com.schibsted.scm.nextgenapp.config;

public class ConfigContainer {
    private static SiteConfig config;

    static {
        config = null;
    }

    public static SiteConfig getConfig() {
        if (config == null) {
            config = new SiteConfig();
        }
        return config;
    }
}
