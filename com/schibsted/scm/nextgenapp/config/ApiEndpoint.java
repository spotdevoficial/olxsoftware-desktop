package com.schibsted.scm.nextgenapp.config;

import com.schibsted.scm.nextgenapp.database.parsers.FilterCategoriesApiResponseParser;
import com.schibsted.scm.nextgenapp.database.parsers.InsertCategoriesApiResponseParser;
import com.schibsted.scm.nextgenapp.database.parsers.NetworkResponseParser;
import com.schibsted.scm.nextgenapp.database.parsers.RegionsApiResponseParser;
import com.schibsted.scm.nextgenapp.models.AccountApiModel;
import com.schibsted.scm.nextgenapp.models.AccountUpdateReply;
import com.schibsted.scm.nextgenapp.models.AdDetailsApiModel;
import com.schibsted.scm.nextgenapp.models.AdPhoneNumbersApiModel;
import com.schibsted.scm.nextgenapp.models.ConfigApiModel;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.DeleteReasonApiModel;
import com.schibsted.scm.nextgenapp.models.EmptyResponseApiModel;
import com.schibsted.scm.nextgenapp.models.FiltersApiModel;
import com.schibsted.scm.nextgenapp.models.FiltersDatabaseApiModel;
import com.schibsted.scm.nextgenapp.models.GooglePlacesApiModel;
import com.schibsted.scm.nextgenapp.models.InsertAdActionApiModel;
import com.schibsted.scm.nextgenapp.models.InsertAdReplyApiModel;
import com.schibsted.scm.nextgenapp.models.MediaResultsApiModel;
import com.schibsted.scm.nextgenapp.models.MyAdsApiModel;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.SearchResultApiModel;
import com.schibsted.scm.nextgenapp.models.SignInApiModel;
import com.schibsted.scm.nextgenapp.models.submodels.RegionNode;
import com.schibsted.scm.nextgenapp.olxchat.model.Chat;
import com.schibsted.scm.nextgenapp.olxchat.network.dto.ChatListDTO;
import com.schibsted.scm.nextgenapp.olxchat.network.dto.MessageListDTO;
import com.schibsted.scm.nextgenapp.olxchat.network.dto.MessageTimestampDTO;
import com.schibsted.scm.nextgenapp.olxchat.network.parser.ChatListParser;
import com.schibsted.scm.nextgenapp.olxchat.network.parser.MessageListParser;
import com.schibsted.scm.nextgenapp.olxchat.network.parser.ResponseParser;

public enum ApiEndpoint {
    PREFETCH_REGIONS(r3.append(SiteConfig.getApiVersion()).append("/public/regions").toString(), 0, null, false, RegionsApiResponseParser.class),
    PREFETCH_FILTER_CATEGORIES(r3.append(SiteConfig.getApiVersion()).append("/public/categories/filter").toString(), 0, null, false, FilterCategoriesApiResponseParser.class),
    PREFETCH_NEW_AD_CATEGORIES(r3.append(SiteConfig.getApiVersion()).append("/public/categories/insert").toString(), 0, null, false, InsertCategoriesApiResponseParser.class),
    LIST_FILTERS(r3.append(SiteConfig.getApiVersion()).append("/public/filters").toString(), 0, FiltersApiModel.class),
    GET_AD(r3.append(SiteConfig.getApiVersion()).append("/public/ads/{ad_id}").toString(), 0, AdDetailsApiModel.class),
    LIST_ADS(r3.append(SiteConfig.getApiVersion()).append("/public/ads").toString(), 0, SearchResultApiModel.class),
    REGIONS_SUB_TREE(r3.append(SiteConfig.getApiVersion()).append("/public/regions?from={from}").toString(), 0, (String) RegionNode.class, (int) false),
    ZIP_CODE_TO_REGION(r3.append(SiteConfig.getApiVersion()).append("/public/location/zipcode?zipcode={zip_code}").toString(), 0, RegionPathApiModel.class),
    FILTER_DATABASE(r3.append(SiteConfig.getApiVersion()).append("/public/filters/database").toString(), 0, FiltersDatabaseApiModel.class),
    REGISTER_EVENT(r3.append(SiteConfig.getApiVersion()).append("/public/ads/{ad_id}/events").toString(), 1, EmptyResponseApiModel.class),
    DELETION_REASONS(r3.append(SiteConfig.getApiVersion()).append("/public/delete_reasons").toString(), 0, DeleteReasonApiModel.class),
    CREATE_ACCOUNT(r3.append(SiteConfig.getApiVersion()).append("/private/accounts").toString(), 1, (String) SignInApiModel.class, (int) false),
    SIGN_IN(r3.append(SiteConfig.getApiVersion()).append("/private/accounts").toString(), 1, SignInApiModel.class),
    GET_ACCOUNT(r3.append(SiteConfig.getApiVersion()).append("/private/accounts/{account_id}").toString(), 0, (String) AccountApiModel.class, (int) true),
    UPDATE_ACCOUNT(r3.append(SiteConfig.getApiVersion()).append("/private/accounts/{account_id}").toString(), 7, (String) AccountUpdateReply.class, (int) true),
    VALIDATE_OR_CREATE_AD(r3.append(SiteConfig.getApiVersion()).append("/private/accounts/{account_id}/ads").toString(), 1, (String) InsertAdReplyApiModel.class, (int) true),
    UPDATE_AD(r3.append(SiteConfig.getApiVersion()).append("/private/accounts/{account_id}/ads/{ad_id}/actions").toString(), 1, (String) InsertAdReplyApiModel.class, (int) true),
    DELETE_AD(r3.append(SiteConfig.getApiVersion()).append("/private/accounts/{account_id}/ads/{ad_id}").toString(), 3, (String) InsertAdActionApiModel.class, (int) true),
    GET_USER_ADS(r3.append(SiteConfig.getApiVersion()).append("/private/accounts/{account_id}/ads").toString(), 0, (String) MyAdsApiModel.class, (int) true),
    AD_IMAGE_UPLOAD(r3.append(SiteConfig.getApiVersion()).append("/public/media/ad").toString(), 1, MediaResultsApiModel.class),
    ACCOUNT_IMAGE_UPLOAD(r3.append(SiteConfig.getApiVersion()).append("/public/media/account").toString(), 1, MediaResultsApiModel.class),
    GET_AD_PHONE_NUMBER(r3.append(SiteConfig.getApiVersion()).append("/public/ads/{ad_id}/phone").toString(), 0, AdPhoneNumbersApiModel.class),
    CONFIG(r3.append(SiteConfig.getApiVersion()).append("/public/config").toString(), 0, ConfigApiModel.class),
    GOOGLE_PLACES_LIST(r3.append(SiteConfig.getApiVersion()).append("/private/google_maps/autocomplete").toString(), 0, (String) GooglePlacesApiModel.class, (int) true),
    POST_AD_REPLY(r3.append(SiteConfig.getApiVersion()).append("/public/ads/{list_id}/messages").toString(), 1, EmptyResponseApiModel.class),
    ONE_TIME_PASSWORD(r3.append(SiteConfig.getApiVersion()).append("/private/accounts/otp").toString(), 1, EmptyResponseApiModel.class),
    GET_CHAT_LIST(r3.append(SiteConfig.getApiVersion()).append("/private/chats").toString(), 0, (String) ChatListDTO.class, (int) ChatListParser.class),
    CREATE_CHAT(r3.append(SiteConfig.getApiVersion()).append("/private/chats").toString(), 1, Chat.class),
    CREATE_MESSAGE_IN_CHAT(r3.append(SiteConfig.getApiVersion()).append("/private/chat_messages/{").append("chatId").append("}").toString(), 1, MessageTimestampDTO.class),
    GET_CHAT(r3.append(SiteConfig.getApiVersion()).append("/private/chats/{").append("chatId").append("}").toString(), 0, Chat.class),
    DELETE_CHAT(r3.append(SiteConfig.getApiVersion()).append("/private/chats/{").append("chatId").append("}").toString(), 3, EmptyResponseApiModel.class),
    GET_MESSAGES_FROM_CHAT(r3.append(SiteConfig.getApiVersion()).append("/private/chat_messages/{").append("chatId").append("}").toString(), 0, (String) MessageListDTO.class, (int) MessageListParser.class),
    REPORT_USER(r3.append(SiteConfig.getApiVersion()).append("/private/users_report/{").append("reportedUserAccountId").append("}").toString(), 1, EmptyResponseApiModel.class),
    REGISTER_DEVICE(r3.append(SiteConfig.getApiVersion()).append("/private/devices").toString(), 1, EmptyResponseApiModel.class);
    
    private Class<? extends ResponseParser<? extends DataModel>> mResponseParser;
    private int method;
    private Class<? extends DataModel> model;
    private Class<? extends NetworkResponseParser> parser;
    private String path;
    private boolean requiresAuthentication;

    private <M extends DataModel> ApiEndpoint(String path, int method, Class<M> model, boolean requiresAuthentication, Class<? extends NetworkResponseParser> parser) {
        this.path = path;
        this.method = method;
        this.model = model;
        this.requiresAuthentication = requiresAuthentication;
        this.parser = parser;
    }

    private <M extends DataModel> ApiEndpoint(String path, int method, Class<M> model, boolean requiresAuthentication) {
        this.path = path;
        this.method = method;
        this.model = model;
        this.requiresAuthentication = requiresAuthentication;
        this.parser = null;
    }

    private <M extends DataModel> ApiEndpoint(String path, int method, Class<M> model) {
        this(r8, r9, path, method, (Class) model, false);
    }

    private <M extends DataModel> ApiEndpoint(String path, int method, Class<M> model, Class<? extends ResponseParser<M>> responseParser) {
        this(r1, r2, path, method, model);
        this.mResponseParser = responseParser;
    }

    public boolean requiresAuthentication() {
        return this.requiresAuthentication;
    }

    public String getPath() {
        return this.path;
    }

    public Class<? extends DataModel> getModel() {
        return this.model;
    }

    public int getMethod() {
        return this.method;
    }

    public Class<? extends NetworkResponseParser> getParser() {
        return this.parser;
    }

    public Class<? extends ResponseParser<? extends DataModel>> getResponseParser() {
        return this.mResponseParser;
    }

    public String toString() {
        return name().replace("_", " ").toLowerCase() + " (" + this.path + ")";
    }
}
