package com.schibsted.scm.nextgenapp.receivers.action;

import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.messages.push.PushWithTrackingActionMessage;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import com.urbanairship.actions.Action;
import com.urbanairship.actions.ActionArguments;
import com.urbanairship.actions.ActionResult;

public class TrackingAction extends Action {
    private static final String TAG;

    static {
        TAG = TrackingAction.class.getSimpleName();
    }

    public ActionResult perform(ActionArguments arguments) {
        Logger.debug(TAG, "Action " + getClass().getSimpleName() + " is performing!");
        return ActionResult.newResult(arguments.getValue());
    }

    public void onFinish(ActionArguments arguments, ActionResult result) {
        super.onFinish(arguments, result);
        String trackingId = result.getValue().getString();
        MessageBus messageBus = C1049M.getMessageBus();
        messageBus.post(new PushWithTrackingActionMessage(trackingId));
        messageBus.post(new EventBuilder().setEventType(EventType.PUSH_NOTIFICATION_TRACKING_ACTION).setTrackingId(trackingId).build());
    }
}
