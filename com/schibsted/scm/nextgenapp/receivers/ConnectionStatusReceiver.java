package com.schibsted.scm.nextgenapp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.bus.messages.NetworkStatusChangedMessage;
import com.schibsted.scm.nextgenapp.utils.Utils;

public class ConnectionStatusReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        C1049M.getMessageBus().post(new NetworkStatusChangedMessage(Utils.isConnected(context)));
    }
}
