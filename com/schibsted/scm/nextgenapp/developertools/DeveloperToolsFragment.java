package com.schibsted.scm.nextgenapp.developertools;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.MainApplication;
import com.schibsted.scm.nextgenapp.backend.managers.PreferencesManager;
import com.schibsted.scm.nextgenapp.developertools.DeveloperToolsContract.ModelContract;
import com.schibsted.scm.nextgenapp.developertools.DeveloperToolsContract.ViewContract;
import com.schibsted.scm.nextgenapp.olxchat.OLXChat;
import com.urbanairship.UAirship;

public class DeveloperToolsFragment extends Fragment {
    private ModelContract mModel;
    private DeveloperToolsPresenter mPresenter;
    private ViewContract mView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PackageInfo packageInfo = null;
        try {
            packageInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
        } catch (NameNotFoundException e) {
        }
        this.mModel = new DeveloperToolsModel(packageInfo, UAirship.shared().getPushManager(), new PreferencesManager(getActivity()), getOlxChat());
        this.mView = new DeveloperToolsView(getActivity());
        this.mPresenter = new DeveloperToolsPresenter(this.mModel, this.mView);
        this.mModel.setPresenter(this.mPresenter);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.mView.getView();
    }

    public void onResume() {
        super.onResume();
        C1049M.getMessageBus().register(this.mModel);
        OLXChat olxChat = getOlxChat();
        if (olxChat != null) {
            olxChat.registerInBus(this.mModel);
        }
    }

    public void onPause() {
        super.onPause();
        C1049M.getMessageBus().unregister(this.mModel);
        OLXChat olxChat = getOlxChat();
        if (olxChat != null) {
            olxChat.unregisterInBus(this.mModel);
        }
    }

    private OLXChat getOlxChat() {
        return ((MainApplication) getActivity().getApplication()).getOlxChatInstance();
    }
}
