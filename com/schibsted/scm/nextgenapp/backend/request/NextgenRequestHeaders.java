package com.schibsted.scm.nextgenapp.backend.request;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class NextgenRequestHeaders {
    private static final Map<String, String> sHeaders;

    static {
        Map<String, String> map = new HashMap();
        map.put("X-Nga-Source", "android");
        map.put("X-Nga-Source-Version", "10.6.1.0");
        sHeaders = Collections.unmodifiableMap(map);
    }

    public static Map<String, String> getHeaders() {
        return sHeaders;
    }
}
