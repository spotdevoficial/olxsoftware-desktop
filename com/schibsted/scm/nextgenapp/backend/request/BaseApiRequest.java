package com.schibsted.scm.nextgenapp.backend.request;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public abstract class BaseApiRequest<T> extends Request<T> {
    public BaseApiRequest(int method, String url, ErrorListener listener) {
        super(method, url, listener);
    }

    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> customHeaders = new HashMap(super.getHeaders());
        for (Entry<String, String> headerPair : NextgenRequestHeaders.getHeaders().entrySet()) {
            customHeaders.put(headerPair.getKey(), headerPair.getValue());
        }
        customHeaders.put("Accept", "application/json");
        customHeaders.put("Accept-language", ConfigContainer.getConfig().getApplicationLanguage());
        return customHeaders;
    }
}
