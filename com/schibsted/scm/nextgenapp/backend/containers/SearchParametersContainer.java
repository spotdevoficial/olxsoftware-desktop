package com.schibsted.scm.nextgenapp.backend.containers;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.facebook.BuildConfig;
import com.facebook.share.internal.ShareConstants;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.managers.SearchParameterManager;
import com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode;
import com.schibsted.scm.nextgenapp.models.LatLong;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.CountersModel;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.models.submodels.Coordinate;
import com.schibsted.scm.nextgenapp.models.submodels.FilterDescription;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import com.schibsted.scm.nextgenapp.models.submodels.ParameterDefinition;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class SearchParametersContainer extends ParametersContainer {
    public static Creator<SearchParametersContainer> CREATOR;
    private long dbId;
    private List<String> mAdListingIds;
    private DbCategoryNode mCategory;
    private CountersModel mCounters;
    private Long mCreationTime;
    private Map<String, ParameterValue> mFilterParameters;
    private Map<String, ParameterDefinition> mFilterParametersDefinitions;
    private Long mLastViewedTime;
    private LatLong mLatLong;
    private Coordinate mLocationCoordinates;
    @JsonIgnore
    private int mPageNumber;
    private RegionPathApiModel mRegion;
    private boolean mSearchTermsUpdated;
    private String mTextSearch;

    /* renamed from: com.schibsted.scm.nextgenapp.backend.containers.SearchParametersContainer.1 */
    static class C11591 implements Creator<SearchParametersContainer> {
        C11591() {
        }

        public SearchParametersContainer createFromParcel(Parcel source) {
            return new SearchParametersContainer(null);
        }

        public SearchParametersContainer[] newArray(int size) {
            return new SearchParametersContainer[size];
        }
    }

    public long getDbId() {
        return this.dbId;
    }

    public void setDbId(long dbId) {
        this.dbId = dbId;
    }

    public SearchParametersContainer() {
        this.mSearchTermsUpdated = false;
        this.mPageNumber = 0;
        loadDefaultValues();
    }

    public SearchParametersContainer(Map<String, ParameterValue> parameters) {
        this.mSearchTermsUpdated = false;
        this.mPageNumber = 0;
        loadDefaultValues();
        if (parameters != null) {
            this.mFilterParameters = parameters;
        }
    }

    public String getTextSearch() {
        this.mSearchTermsUpdated = true;
        return this.mTextSearch;
    }

    public void setTextSearch(String textSearch) {
        this.mSearchTermsUpdated = true;
        this.mTextSearch = textSearch;
    }

    public void setLocationCoordinates(Coordinate mLocationCoordinates) {
        this.mSearchTermsUpdated = true;
        this.mLocationCoordinates = mLocationCoordinates;
    }

    public RegionPathApiModel getRegion() {
        return this.mRegion;
    }

    public void setRegion(RegionPathApiModel region) {
        this.mRegion = region;
        this.mSearchTermsUpdated = true;
    }

    public DbCategoryNode getCategory() {
        return this.mCategory;
    }

    public void setCategory(DbCategoryNode category) {
        this.mCategory = category;
        this.mSearchTermsUpdated = true;
    }

    public void setAdListingId(List<String> adListingIds) {
        this.mAdListingIds = adListingIds;
    }

    public Map<String, ParameterValue> getFilterParameters() {
        return this.mFilterParameters;
    }

    public void setFilterParameters(Map<String, ParameterValue> filterParameters) {
        this.mFilterParameters = filterParameters;
        this.mSearchTermsUpdated = true;
    }

    public Map<String, ParameterDefinition> getFilterParametersDefinitions() {
        return this.mFilterParametersDefinitions;
    }

    public void setFilterParametersDefinitions(Map<String, ParameterDefinition> filterParametersDefinitions) {
        this.mFilterParametersDefinitions = filterParametersDefinitions;
    }

    public void clearFilterParameters() {
        this.mFilterParameters.clear();
    }

    public String toStorageJson() {
        SearchParametersStorage searchParametersStorage = new SearchParametersStorage();
        if (this.mCreationTime == null) {
            this.mCreationTime = Long.valueOf(new Date().getTime());
        }
        if (this.mCategory != null) {
            searchParametersStorage.category = this.mCategory.getId();
        }
        if (this.mRegion != null) {
            searchParametersStorage.region = this.mRegion.getIdentifier();
        }
        searchParametersStorage.locationCoordinates = this.mLocationCoordinates;
        searchParametersStorage.textSearch = this.mTextSearch;
        if (this.mFilterParameters != null) {
            searchParametersStorage.parameters.putAll(this.mFilterParameters);
            searchParametersStorage.parameters.remove("sort");
        }
        searchParametersStorage.lastViewedTime = this.mLastViewedTime;
        searchParametersStorage.creationTime = this.mCreationTime;
        try {
            return JsonMapper.getInstance().writeValueAsString(searchParametersStorage);
        } catch (JsonProcessingException e) {
            Logger.error("SearchParametersContainer", "Error converting query string to json", e);
            return null;
        }
    }

    public static SearchParametersContainer fromStorageJson(String json) {
        try {
            SearchParametersStorage searchParametersStorage = (SearchParametersStorage) JsonMapper.getInstance().readValue(json, SearchParametersStorage.class);
            SearchParametersContainer container = new SearchParametersContainer();
            if (searchParametersStorage.region != null) {
                container.setRegion(C1049M.getDaoManager().getRegionTree().findRegion(searchParametersStorage.region));
            }
            if (!TextUtils.isEmpty(searchParametersStorage.category)) {
                container.setCategory(C1049M.getDaoManager().getCategoryTree("category_data").getNode(searchParametersStorage.category));
            }
            if (searchParametersStorage.adListingIds != null && searchParametersStorage.adListingIds.size() > 0) {
                container.setAdListingId(searchParametersStorage.adListingIds);
            }
            container.setLocationCoordinates(searchParametersStorage.locationCoordinates);
            container.setFilterParameters(searchParametersStorage.parameters);
            container.setTextSearch(searchParametersStorage.textSearch);
            container.setLastViewedTime(searchParametersStorage.lastViewedTime);
            container.setCreationTime(searchParametersStorage.creationTime);
            return container;
        } catch (IOException e) {
            return null;
        }
    }

    public TreeMap<String, List<String>> getQueryStringMap() {
        int i;
        TreeMap<String, List<String>> parameters = new TreeMap();
        if (!(this.mRegion == null || this.mRegion == null)) {
            for (i = 0; i < this.mRegion.getIdentifier().keys.size(); i++) {
                parameters.put(this.mRegion.getIdentifier().keys.get(i), Utils.value(this.mRegion.getIdentifier().values.get(i)));
            }
        }
        if (!(this.mCategory == null || this.mCategory.getPath() == null)) {
            Identifier id = new Identifier(this.mCategory.getPath());
            for (i = 0; i < id.keys.size(); i++) {
                parameters.put(id.keys.get(i), Utils.value(id.values.get(i)));
            }
        }
        if (this.mAdListingIds != null && this.mAdListingIds.size() > 0) {
            parameters.put(ShareConstants.WEB_DIALOG_PARAM_ID, this.mAdListingIds);
        }
        if (!TextUtils.isEmpty(this.mTextSearch)) {
            parameters.put("q", Utils.value(this.mTextSearch));
        }
        if (this.mLastViewedTime != null) {
            parameters.put("count_newer_than", Utils.value(BuildConfig.VERSION_NAME + this.mLastViewedTime));
        }
        if (this.mFilterParameters != null) {
            if (this.mFilterParameters.containsKey("viewby") && ((ParameterValue) this.mFilterParameters.get("viewby")).toParameter().equals(Utils.value("proximity"))) {
                if (this.mLatLong != null) {
                    double latitude = this.mLatLong.getLatitude();
                    double longitude = this.mLatLong.getLongitude();
                    parameters.put("latitude", Utils.value(Double.toString(latitude)));
                    parameters.put("longitude", Utils.value(Double.toString(longitude)));
                } else {
                    this.mFilterParameters.remove("viewby");
                }
            }
            for (Entry<String, ParameterValue> entry : this.mFilterParameters.entrySet()) {
                if (entry.getValue() != null) {
                    parameters.put(entry.getKey(), ((ParameterValue) entry.getValue()).toParameter());
                }
            }
        }
        this.mSearchTermsUpdated = false;
        return parameters;
    }

    public void loadDefaultValues() {
        this.mRegion = null;
        this.mCategory = null;
        this.mAdListingIds = null;
        this.mTextSearch = BuildConfig.VERSION_NAME;
        this.mFilterParameters = new HashMap();
        this.mSearchTermsUpdated = true;
        this.mCounters = new CountersModel();
        this.mLastViewedTime = null;
    }

    public void resetRegionFilters(FilterDescription filterDescription) {
        if (filterDescription != null) {
            for (ParameterState state : new SearchParameterManager(filterDescription, getRegion(), getCategory(), getFilterParameters()).getState().values()) {
                if (state.getDefinition().isBasedOnLocation().booleanValue()) {
                    this.mFilterParameters.remove(state.getDefinition().key);
                }
            }
        }
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SearchParametersContainer)) {
            return false;
        }
        SearchParametersContainer that = (SearchParametersContainer) o;
        if (this.mCategory == null ? that.mCategory != null : !this.mCategory.equals(that.mCategory)) {
            return false;
        }
        Map<String, ParameterValue> thatParameters = new HashMap();
        Map<String, ParameterValue> thisParameters = new HashMap();
        if (that.mFilterParameters != null) {
            thatParameters.putAll(that.mFilterParameters);
            thatParameters.remove("sort");
        }
        if (this.mFilterParameters != null) {
            thisParameters.putAll(this.mFilterParameters);
            thisParameters.remove("sort");
        }
        if (!thisParameters.equals(thatParameters)) {
            return false;
        }
        if (this.mLocationCoordinates == null ? that.mLocationCoordinates != null : !this.mLocationCoordinates.equals(that.mLocationCoordinates)) {
            return false;
        }
        if (this.mRegion == null ? that.mRegion != null : !this.mRegion.equals(that.mRegion)) {
            return false;
        }
        if (this.mTextSearch == null ? that.mTextSearch != null : !this.mTextSearch.equals(that.mTextSearch)) {
            return false;
        }
        if (this.mAdListingIds != null) {
            if (this.mAdListingIds.equals(that.mAdListingIds)) {
                return true;
            }
        } else if (that.mAdListingIds == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int result;
        int hashCode;
        int i = 0;
        if (this.mTextSearch != null) {
            result = this.mTextSearch.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.mLocationCoordinates != null) {
            hashCode = this.mLocationCoordinates.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mRegion != null) {
            hashCode = this.mRegion.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mCategory != null) {
            hashCode = this.mCategory.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mFilterParameters != null) {
            hashCode = this.mFilterParameters.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (i2 + hashCode) * 31;
        if (this.mAdListingIds != null) {
            i = this.mAdListingIds.hashCode();
        }
        return hashCode + i;
    }

    public String getLabel(Context context) {
        StringBuilder builder = new StringBuilder();
        List<String> list = new ArrayList();
        if (!TextUtils.isEmpty(this.mTextSearch)) {
            list.add(this.mTextSearch);
        }
        if (this.mCategory != null) {
            list.add(this.mCategory.getLabel());
        } else {
            list.add(context.getString(2131165307));
        }
        if (this.mRegion != null) {
            list.add(this.mRegion.getLabel());
        } else {
            list.add(context.getString(2131165309));
        }
        if (!this.mFilterParameters.isEmpty()) {
            list.add(this.mFilterParameters.size() + " " + (this.mFilterParameters.size() == 1 ? context.getResources().getString(2131165496) : context.getResources().getString(2131165498)));
        }
        if (!list.isEmpty()) {
            builder.append((String) list.get(0));
            for (int i = 1; i < list.size(); i++) {
                builder.append(", ").append((String) list.get(i));
            }
        }
        return builder.toString();
    }

    public void setLatLong(LatLong latLong) {
        this.mLatLong = latLong;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.mTextSearch).writeParcelable(this.mLocationCoordinates).writeParcelable(this.mRegion).writeString(this.mCategory.getId()).writeParcelableMap(this.mFilterParameters).writeStringList(this.mAdListingIds);
    }

    private SearchParametersContainer(Parcel in) {
        this.mSearchTermsUpdated = false;
        this.mPageNumber = 0;
        ParcelReader reader = new ParcelReader(in);
        this.mTextSearch = reader.readString();
        this.mLocationCoordinates = (Coordinate) reader.readParcelable(Coordinate.class);
        this.mRegion = (RegionPathApiModel) reader.readParcelable(RegionPathApiModel.class);
        this.mCategory = C1049M.getDaoManager().getCategoryTree("category_data").getNode(reader.readString());
        this.mFilterParameters = reader.readInheritedParcelableMap(ParameterValue.CREATOR);
        this.mAdListingIds = reader.readStringList();
    }

    static {
        CREATOR = new C11591();
    }

    public void setCreationTime(Long creationTime) {
        this.mCreationTime = creationTime;
    }

    public long getLastViewedTime() {
        return this.mLastViewedTime != null ? this.mLastViewedTime.longValue() : 0;
    }

    public void setLastViewedTime(Long lastViewedTime) {
        this.mLastViewedTime = lastViewedTime;
    }

    public CountersModel getCounters() {
        return this.mCounters;
    }

    public void setCounters(CountersModel counters) {
        this.mCounters = counters;
    }

    @JsonIgnore
    public int getPageNumber() {
        return this.mPageNumber;
    }

    @JsonIgnore
    public void setPageNumber(int pageNumber) {
        this.mPageNumber = pageNumber;
    }
}
