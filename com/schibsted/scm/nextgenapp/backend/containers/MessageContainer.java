package com.schibsted.scm.nextgenapp.backend.containers;

public class MessageContainer {
    private String mEmail;
    private String mName;
    private String mPhone;
    private String mText;

    public MessageContainer(String name, String email, String phone, String text) {
        this.mName = name;
        this.mEmail = email;
        this.mPhone = phone;
        this.mText = text;
    }

    public String getName() {
        return this.mName;
    }

    public String getEmail() {
        return this.mEmail;
    }

    public String getPhone() {
        return this.mPhone;
    }
}
