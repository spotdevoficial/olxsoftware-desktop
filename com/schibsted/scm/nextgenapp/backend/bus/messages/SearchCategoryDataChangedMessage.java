package com.schibsted.scm.nextgenapp.backend.bus.messages;

public class SearchCategoryDataChangedMessage implements Message {
    private Exception exception;
    private boolean success;

    public SearchCategoryDataChangedMessage() {
        this.exception = null;
        this.success = true;
    }

    public SearchCategoryDataChangedMessage(Exception exception) {
        this.exception = exception;
        this.success = false;
    }

    public boolean isSuccess() {
        return this.success;
    }
}
