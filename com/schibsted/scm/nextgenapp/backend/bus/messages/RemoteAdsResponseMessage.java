package com.schibsted.scm.nextgenapp.backend.bus.messages;

import com.schibsted.scm.nextgenapp.backend.containers.SearchParametersContainer;

public class RemoteAdsResponseMessage {
    private SearchParametersContainer searchParametersContainer;

    public RemoteAdsResponseMessage(SearchParametersContainer spc) {
        this.searchParametersContainer = spc;
    }

    public SearchParametersContainer getSearchParametersContainer() {
        return this.searchParametersContainer;
    }
}
