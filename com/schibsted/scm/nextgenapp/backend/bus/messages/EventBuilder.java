package com.schibsted.scm.nextgenapp.backend.bus.messages;

import com.android.volley.VolleyError;
import com.schibsted.scm.nextgenapp.backend.containers.MessageContainer;
import com.schibsted.scm.nextgenapp.backend.containers.SearchParametersContainer;
import com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode;
import com.schibsted.scm.nextgenapp.models.AccountApiModel;
import com.schibsted.scm.nextgenapp.models.interfaces.AdContainer;
import com.schibsted.scm.nextgenapp.models.requests.InsertAdRequest;
import com.schibsted.scm.nextgenapp.models.submodels.Account;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.models.submodels.RegionNode;
import com.schibsted.scm.nextgenapp.models.submodels.StaticPage;
import com.schibsted.scm.nextgenapp.tracking.EventType;

public class EventBuilder {
    private Account mAccount;
    private String mAccountId;
    private Ad mAd;
    private Integer mAdPosition;
    private String mAttributeName;
    private DbCategoryNode mCategory;
    private String mChatId;
    private String mDeepLinkReferralPage;
    private String mDeepLinkSource;
    private String mEmail;
    private VolleyError mError;
    private EventType mEventType;
    private InsertAdRequest mInsertAdRequest;
    private boolean mIsFacebookAuthCodeNullOrEmpty;
    private String mListId;
    private MessageContainer mMessageContainer;
    private String mMessageId;
    private int mNativeAdErrorType;
    private int mNativeAdPosition;
    private String mNativeAdType;
    private String mOriginPageTitle;
    private String mPhotoSelectionSource;
    private String mPrice;
    private RegionNode mRegionNode;
    private String mReportedId;
    private SearchParametersContainer mSearchParametersContainer;
    private StaticPage mStaticPage;
    private String mTrackingId;
    private String mUAirshipChannelId;
    private String mZipCode;

    public EventBuilder setEventType(EventType eventType) {
        this.mEventType = eventType;
        return this;
    }

    public EventBuilder setOriginPageTitle(String title) {
        this.mOriginPageTitle = title;
        return this;
    }

    public EventBuilder setSearchParametersContainer(SearchParametersContainer searchParametersContainer) {
        this.mSearchParametersContainer = searchParametersContainer;
        return this;
    }

    public EventBuilder setAdPosition(int position) {
        this.mAdPosition = Integer.valueOf(position);
        return this;
    }

    public EventBuilder setAd(Ad ad) {
        this.mAd = ad;
        return this;
    }

    public EventBuilder setChatId(String chatId) {
        this.mChatId = chatId;
        return this;
    }

    public EventBuilder setMessageId(String messageId) {
        this.mMessageId = messageId;
        return this;
    }

    public EventBuilder setReportedId(String reportedId) {
        this.mReportedId = reportedId;
        return this;
    }

    public EventBuilder setAdContainer(AdContainer adContainer) {
        if (adContainer != null) {
            this.mAd = adContainer.getAd();
        }
        return this;
    }

    public EventBuilder setInsertAdRequest(InsertAdRequest insertAdRequest) {
        this.mInsertAdRequest = insertAdRequest;
        return this;
    }

    public EventBuilder setAccount(Account account) {
        this.mAccount = account;
        return this;
    }

    public EventBuilder setAccount(AccountApiModel accountApiModel) {
        if (accountApiModel != null) {
            this.mAccount = accountApiModel.account;
        }
        return this;
    }

    public EventBuilder setError(VolleyError error) {
        this.mError = error;
        return this;
    }

    public EventBuilder setStaticPage(StaticPage staticPage) {
        this.mStaticPage = staticPage;
        return this;
    }

    public EventBuilder setCategory(DbCategoryNode category) {
        this.mCategory = category;
        return this;
    }

    public EventBuilder setRegionNode(RegionNode regionNode) {
        this.mRegionNode = regionNode;
        return this;
    }

    public EventBuilder setMessageContainer(MessageContainer container) {
        this.mMessageContainer = container;
        return this;
    }

    public EventBuilder setEmail(String email) {
        this.mEmail = email;
        return this;
    }

    public EventBuilder setAttributeName(String attributeName) {
        this.mAttributeName = attributeName;
        return this;
    }

    public EventBuilder setPrice(String price) {
        this.mPrice = price;
        return this;
    }

    public EventBuilder setZipCode(String zipCode) {
        this.mZipCode = zipCode;
        return this;
    }

    public EventBuilder setUAirshipChannelId(String channelId) {
        this.mUAirshipChannelId = channelId;
        return this;
    }

    public EventBuilder setTrackingId(String tntId) {
        this.mTrackingId = tntId;
        return this;
    }

    public EventBuilder setIsFacebookAuthCodeNullOrEmpty(boolean isFacebookAuthCodeNullOrEmpty) {
        this.mIsFacebookAuthCodeNullOrEmpty = isFacebookAuthCodeNullOrEmpty;
        return this;
    }

    public EventBuilder setListId(String listId) {
        this.mListId = listId;
        return this;
    }

    public EventBuilder setAccountId(String accountId) {
        this.mAccountId = accountId;
        return this;
    }

    public EventBuilder setDeepLinkSource(String source) {
        this.mDeepLinkSource = source;
        return this;
    }

    public EventBuilder setDeepLinkReferralPage(String referral) {
        this.mDeepLinkReferralPage = referral;
        return this;
    }

    public EventBuilder setNativeAdPosition(int nativeAdPosition) {
        this.mNativeAdPosition = nativeAdPosition;
        return this;
    }

    public EventBuilder setNativeAdType(String nativeAdType) {
        this.mNativeAdType = nativeAdType;
        return this;
    }

    public EventBuilder setNativeAdErrorType(int nativeAdErrorType) {
        this.mNativeAdErrorType = nativeAdErrorType;
        return this;
    }

    public EventBuilder setPhotoSelectionSource(boolean fromCamera) {
        this.mPhotoSelectionSource = fromCamera ? "camera" : "gallery";
        return this;
    }

    public EventMessage build() {
        return new EventMessage(this.mEventType, this.mSearchParametersContainer, this.mAd, this.mAdPosition, this.mInsertAdRequest, this.mAccount, this.mError, this.mStaticPage, this.mCategory, this.mRegionNode, this.mMessageContainer, this.mEmail, this.mAttributeName, this.mOriginPageTitle, this.mPrice, this.mZipCode, this.mUAirshipChannelId, this.mTrackingId, this.mIsFacebookAuthCodeNullOrEmpty, this.mChatId, this.mMessageId, this.mReportedId, this.mAccountId, this.mListId, this.mDeepLinkSource, this.mDeepLinkReferralPage, this.mNativeAdPosition, this.mNativeAdType, this.mNativeAdErrorType, this.mPhotoSelectionSource);
    }
}
