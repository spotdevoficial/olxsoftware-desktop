package com.schibsted.scm.nextgenapp.backend.bus.messages;

import com.android.volley.VolleyError;
import com.schibsted.scm.nextgenapp.models.InsertAdReplyApiModel;

public class AdInsertionSubmissionMessage implements Message {
    private VolleyError error;
    private InsertAdReplyApiModel model;

    public AdInsertionSubmissionMessage(InsertAdReplyApiModel model) {
        this.model = model;
    }

    public AdInsertionSubmissionMessage(VolleyError error) {
        this.error = error;
    }

    public boolean isError() {
        return this.error != null;
    }

    public InsertAdReplyApiModel getModel() {
        return this.model;
    }

    public VolleyError getError() {
        return this.error;
    }
}
