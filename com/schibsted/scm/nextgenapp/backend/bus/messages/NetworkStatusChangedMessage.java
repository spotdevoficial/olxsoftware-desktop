package com.schibsted.scm.nextgenapp.backend.bus.messages;

public class NetworkStatusChangedMessage {
    private boolean mIsConnected;

    public NetworkStatusChangedMessage(boolean isConnected) {
        this.mIsConnected = isConnected;
    }

    public boolean isConnected() {
        return this.mIsConnected;
    }
}
