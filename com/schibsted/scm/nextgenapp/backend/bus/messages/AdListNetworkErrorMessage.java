package com.schibsted.scm.nextgenapp.backend.bus.messages;

import com.android.volley.VolleyError;

public class AdListNetworkErrorMessage {
    private VolleyError volleyError;

    public AdListNetworkErrorMessage(VolleyError error) {
        this.volleyError = error;
    }

    public VolleyError getError() {
        return this.volleyError;
    }
}
