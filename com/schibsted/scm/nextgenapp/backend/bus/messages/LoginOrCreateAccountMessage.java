package com.schibsted.scm.nextgenapp.backend.bus.messages;

import com.android.volley.VolleyError;
import com.schibsted.scm.nextgenapp.models.SignInApiModel;

public class LoginOrCreateAccountMessage implements Message {
    private SignInApiModel mModel;
    private VolleyError mVolleyError;

    public LoginOrCreateAccountMessage(VolleyError volleyError) {
        this.mVolleyError = volleyError;
    }

    public LoginOrCreateAccountMessage(SignInApiModel model) {
        this.mModel = model;
    }

    public VolleyError getVolleyError() {
        return this.mVolleyError;
    }

    public SignInApiModel getModel() {
        return this.mModel;
    }

    public boolean isError() {
        return this.mVolleyError != null;
    }
}
