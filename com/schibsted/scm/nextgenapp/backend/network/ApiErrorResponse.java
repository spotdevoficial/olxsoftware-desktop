package com.schibsted.scm.nextgenapp.backend.network;

import com.android.volley.VolleyError;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.models.ErrorResponseApiModel;
import java.net.ConnectException;
import java.util.Map;
import java.util.Map.Entry;

public class ApiErrorResponse extends VolleyError {
    private static final String CR;
    private String mErrorMessage;
    private ErrorResponseApiModel mErrorModel;
    private int mErrorType;
    private String mRequestBody;
    private Map<String, String> mRequestHeaders;
    private String mRequestUrl;

    static {
        CR = System.getProperty("line.separator").toString();
    }

    public ApiErrorResponse(String exceptionMessage) {
        super(exceptionMessage);
        this.mErrorModel = null;
    }

    public ApiErrorResponse(VolleyError volleyError, String errorMessage, ErrorResponseApiModel errorModel, String url, String body, Map<String, String> headers) {
        super(volleyError.networkResponse);
        this.mErrorModel = null;
        this.mRequestUrl = url;
        if (body == null) {
            body = BuildConfig.VERSION_NAME;
        }
        this.mRequestBody = body;
        this.mRequestHeaders = headers;
        this.mErrorModel = errorModel;
        this.mErrorMessage = errorMessage;
        this.mErrorType = 0;
        if (this.mErrorModel != null) {
            this.mErrorType = 2;
        } else if (this.networkResponse != null) {
            this.mErrorType = 3;
        } else if (volleyError.getCause() instanceof ConnectException) {
            this.mErrorType = 1;
        }
    }

    public ErrorResponseApiModel getErrorModel() {
        return this.mErrorModel;
    }

    public int getErrorType() {
        return this.mErrorType;
    }

    public String getErrorMessage() {
        return this.mErrorMessage;
    }

    public String getRequestUrl() {
        return this.mRequestUrl;
    }

    public String getRequestBody() {
        return this.mRequestBody;
    }

    public Map<String, String> getRequestHeaders() {
        return this.mRequestHeaders;
    }

    public String getMessage() {
        StringBuilder builder = new StringBuilder().append(CR).append("Url:    \t").append(getRequestUrl()).append(CR);
        if (!(getRequestBody() == null || getRequestBody() == null)) {
            builder.append("Body:   \t");
            builder.append(getRequestBody());
            builder.append(CR);
        }
        if (!(getRequestHeaders() == null || getRequestHeaders().isEmpty())) {
            builder.append("Headers:");
            builder.append(CR);
            for (Entry<String, String> header : getRequestHeaders().entrySet()) {
                builder.append(((String) header.getKey()) + " " + ((String) header.getValue()));
                builder.append(CR);
            }
        }
        builder.append("Code:   \t").append(this.networkResponse != null ? Integer.valueOf(this.networkResponse.statusCode) : BuildConfig.VERSION_NAME).append(CR).append("Type:   \t").append(getErrorType()).append(CR).append("Message:\t").append(getErrorMessage()).append(CR);
        return builder.toString();
    }

    public boolean isErrorForEmailNotVerified() {
        if (getErrorModel() == null || getErrorModel().error == null) {
            return false;
        }
        return getErrorModel().error.isEmailNotVerified();
    }
}
