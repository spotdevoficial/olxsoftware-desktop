package com.schibsted.scm.nextgenapp.backend.network;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.schibsted.scm.nextgenapp.backend.network.uglyhack.HurlStack;
import com.schibsted.scm.nextgenapp.backend.request.NetworkRequest;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.OkUrlFactory;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicStatusLine;

public class HybridHttpStack extends HurlStack {
    private String TAG;
    private OkUrlFactory client;
    private SSLSocketFactory sslSocketFactory;

    public HybridHttpStack(SSLSocketFactory sslSocketFactory, OkHttpClient client) {
        super(null, sslSocketFactory);
        this.TAG = HybridHttpStack.class.getSimpleName();
        if (client == null) {
            throw new NullPointerException("Client must not be null.");
        }
        this.client = new OkUrlFactory(client);
        this.sslSocketFactory = sslSocketFactory;
    }

    protected HttpURLConnection createConnection(URL url) throws IOException {
        return this.client.open(url);
    }

    public HttpResponse performRequest(Request<?> request, Map<String, String> additionalHeaders) throws IOException, AuthFailureError {
        HttpResponse response;
        if ((request instanceof NetworkRequest) && ((NetworkRequest) request).isMultiPart()) {
            response = performMultiPartRequest((NetworkRequest) request, additionalHeaders);
        } else {
            response = super.performRequest(request, additionalHeaders);
        }
        fixETagHeaderIfNeeded(response);
        return response;
    }

    private void fixETagHeaderIfNeeded(HttpResponse response) {
        Header badETagHeader = response.getFirstHeader("Etag");
        if (badETagHeader == null) {
            badETagHeader = response.getFirstHeader("etag");
        }
        if (badETagHeader == null) {
            badETagHeader = response.getFirstHeader("ETAG");
        }
        if (badETagHeader != null) {
            response.addHeader("ETag", badETagHeader.getValue());
            response.removeHeader(badETagHeader);
        }
    }

    public HttpResponse performMultiPartRequest(NetworkRequest request, Map<String, String> additionalHeaders) throws IOException, AuthFailureError {
        HashMap<String, String> headers = new HashMap();
        headers.putAll(request.getHeaders());
        headers.putAll(additionalHeaders);
        URL url = new URL(request.getUrl());
        HttpURLConnection connection = createConnection(url);
        int timeoutMs = request.getTimeoutMs();
        connection.setConnectTimeout(timeoutMs);
        connection.setReadTimeout(timeoutMs);
        connection.setUseCaches(false);
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        if ("https".equals(url.getProtocol())) {
            ((HttpsURLConnection) connection).setSSLSocketFactory(this.sslSocketFactory);
        }
        for (Entry<String, String> entry : headers.entrySet()) {
            connection.addRequestProperty((String) entry.getKey(), (String) entry.getValue());
        }
        DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
        request.writeBodyTo(outputStream);
        outputStream.close();
        if (connection.getResponseCode() == -1) {
            throw new IOException("Could not retrieve response code from HttpUrlConnection.");
        }
        InputStream inputStream;
        BasicHttpResponse response = new BasicHttpResponse(new BasicStatusLine(new ProtocolVersion("HTTP", 1, 1), connection.getResponseCode(), connection.getResponseMessage()));
        BasicHttpEntity entity = new BasicHttpEntity();
        try {
            inputStream = connection.getInputStream();
        } catch (IOException e) {
            inputStream = connection.getErrorStream();
        }
        entity.setContent(inputStream);
        entity.setContentLength((long) connection.getContentLength());
        entity.setContentEncoding(connection.getContentEncoding());
        entity.setContentType(connection.getContentType());
        response.setEntity(entity);
        for (Entry<String, List<String>> header : connection.getHeaderFields().entrySet()) {
            if (header.getKey() != null) {
                response.addHeader(new BasicHeader((String) header.getKey(), (String) ((List) header.getValue()).get(0)));
            }
        }
        return response;
    }
}
