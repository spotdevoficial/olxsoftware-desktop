package com.schibsted.scm.nextgenapp.backend.network;

import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import java.io.IOException;
import java.io.InputStream;

public class ProgressInputStream extends InputStream {
    private long counter;
    private float lastPercent;
    private ProgressListener listener;
    private long size;
    private InputStream wrappedInputStream;

    public interface ProgressListener {
        void onProgress(float f);
    }

    public ProgressInputStream(InputStream in, long size, ProgressListener listener) throws IOException {
        this.wrappedInputStream = in;
        this.size = size;
        this.listener = listener;
    }

    public int read() throws IOException {
        this.counter++;
        check();
        return this.wrappedInputStream.read();
    }

    public int read(byte[] b) throws IOException {
        int retVal = this.wrappedInputStream.read(b);
        this.counter += (long) retVal;
        check();
        return retVal;
    }

    public int read(byte[] b, int offset, int length) throws IOException {
        int retVal = this.wrappedInputStream.read(b, offset, length);
        this.counter += (long) retVal;
        check();
        return retVal;
    }

    private void check() {
        float percent = ((float) this.counter) / ((float) this.size);
        if (percent > MediaUploadState.IMAGE_PROGRESS_UPLOADED) {
            percent = 0.99f;
        }
        if (percent - this.lastPercent >= MediaUploadState.IMAGE_PROGRESS_RESIZED) {
            this.lastPercent = percent;
            if (this.listener != null) {
                this.listener.onProgress(percent);
            }
        }
    }

    public void close() throws IOException {
        this.wrappedInputStream.close();
    }

    public int available() throws IOException {
        return this.wrappedInputStream.available();
    }

    public void mark(int readlimit) {
        this.wrappedInputStream.mark(readlimit);
    }

    public synchronized void reset() throws IOException {
        this.wrappedInputStream.reset();
    }

    public boolean markSupported() {
        return this.wrappedInputStream.markSupported();
    }

    public long skip(long n) throws IOException {
        return this.wrappedInputStream.skip(n);
    }
}
