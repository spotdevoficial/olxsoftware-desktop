package com.schibsted.scm.nextgenapp.backend.managers;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.android.volley.VolleyError;
import com.facebook.AccessToken;
import com.facebook.BuildConfig;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphRequest.GraphJSONObjectCallback;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.bus.messages.AccountStatusChangedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.bus.messages.LoginOrCreateAccountMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.SavedSearchesCountersUpdated;
import com.schibsted.scm.nextgenapp.backend.bus.messages.SocialSessionMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.UpdateAccountMessage;
import com.schibsted.scm.nextgenapp.backend.managers.LocalStorageManager.OnResourceLoadedListener;
import com.schibsted.scm.nextgenapp.backend.managers.LocalStorageManager.OnResourceRemovedListener;
import com.schibsted.scm.nextgenapp.backend.managers.UserProfileImagesManager.UserProfileImagesManagerFetchingListener;
import com.schibsted.scm.nextgenapp.backend.managers.list.MyAdListManager;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteListManager.OnAdListChangedListener;
import com.schibsted.scm.nextgenapp.backend.managers.list.SavedAdsManager;
import com.schibsted.scm.nextgenapp.backend.managers.list.SavedSearchesManager;
import com.schibsted.scm.nextgenapp.backend.network.APIRequest.Builder;
import com.schibsted.scm.nextgenapp.backend.network.ApiErrorResponse;
import com.schibsted.scm.nextgenapp.backend.network.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.config.ApiEndpoint;
import com.schibsted.scm.nextgenapp.database.SavedSearchesList;
import com.schibsted.scm.nextgenapp.models.AccountApiModel;
import com.schibsted.scm.nextgenapp.models.AccountUpdateReply;
import com.schibsted.scm.nextgenapp.models.AuthorizedAccount;
import com.schibsted.scm.nextgenapp.models.SignInApiModel;
import com.schibsted.scm.nextgenapp.models.internal.AuthToken;
import com.schibsted.scm.nextgenapp.models.internal.AuthToken.Type;
import com.schibsted.scm.nextgenapp.models.internal.CountersModel;
import com.schibsted.scm.nextgenapp.models.internal.OneTimeAuthToken;
import com.schibsted.scm.nextgenapp.models.requests.AccountUpdateRequest;
import com.schibsted.scm.nextgenapp.models.requests.AccountsRequest;
import com.schibsted.scm.nextgenapp.models.requests.RequestAccount;
import com.schibsted.scm.nextgenapp.models.requests.RequirePasswordRequest;
import com.schibsted.scm.nextgenapp.models.submodels.Account;
import com.schibsted.scm.nextgenapp.models.submodels.Setting;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import java.io.IOException;
import java.util.Date;
import org.json.JSONObject;

public class AccountManager implements FacebookCallback<LoginResult> {
    OnAdListChangedListener countersListener;
    private String mAccessToken;
    private AccountApiModel mAccountApiModel;
    private long mAccountInfoTimestamp;
    private Context mContext;
    private boolean mIsUsingFacebook;
    protected MyAdListManager mMyAdListManager;
    protected SavedAdsManager mSavedAdsManager;
    protected SavedSearchesList mSavedSearchesList;
    private SavedSearchesManager mSavedSearchesManager;
    private String mTokenType;
    private UserProfileImagesManager mUserProfileImagesManager;
    private PreferencesManager preferences;

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.AccountManager.1 */
    class C11601 implements OnResourceLoadedListener<AuthorizedAccount> {
        C11601() {
        }

        public void resourceNotLoaded(Exception exception) {
            Logger.error("AccountManager", exception.toString());
        }

        public void resourceLoaded(AuthorizedAccount authorizedAccount) {
            if (authorizedAccount == null || authorizedAccount.accountApiModel == null || authorizedAccount.accountApiModel.account == null) {
                AccountManager.this.deleteAccountData();
                return;
            }
            String accountId = authorizedAccount.accountApiModel.account.getCleanId();
            AccountManager.this.setAuthorization(authorizedAccount.accessToken, authorizedAccount.tokenType, accountId);
            AccountManager.this.signIn(authorizedAccount.accountApiModel);
            AccountManager.this.fetchAccount(accountId);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.AccountManager.2 */
    class C11612 implements OnResourceRemovedListener {
        C11612() {
        }

        public void resourceNotRemoved(Exception exception) {
        }

        public void resourceRemoved() {
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.AccountManager.3 */
    class C11623 extends OnNetworkResponseListener<SignInApiModel> {
        final /* synthetic */ boolean val$isUsingFacebook;

        C11623(boolean z) {
            this.val$isUsingFacebook = z;
        }

        public void onErrorResponse(VolleyError volleyError) {
            if ((volleyError instanceof ApiErrorResponse) && ((ApiErrorResponse) volleyError).isErrorForEmailNotVerified()) {
                AccountManager.this.tagEvent(EventType.CREDENTIALS_REGISTER_SUCCESS);
            }
            C1049M.getMessageBus().post(new LoginOrCreateAccountMessage(volleyError));
        }

        public void onResponse(SignInApiModel model) {
            if (this.val$isUsingFacebook) {
                AccountManager.this.tagEvent(EventType.CREDENTIALS_FACEBOOK_ACCOUNT_CREATION_SUCCESS);
            } else {
                AccountManager.this.tagEvent(EventType.CREDENTIALS_REGISTER_SUCCESS);
            }
            AccountManager.this.onSignInAuthorized(model);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.AccountManager.4 */
    class C11634 extends OnNetworkResponseListener<SignInApiModel> {
        C11634() {
        }

        public void onErrorResponse(VolleyError volleyError) {
            C1049M.getMessageBus().post(new LoginOrCreateAccountMessage(volleyError));
        }

        public void onResponse(SignInApiModel model) {
            AccountManager.this.onSignInAuthorized(model);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.AccountManager.5 */
    class C11645 extends OnNetworkResponseListener<SignInApiModel> {
        C11645() {
        }

        public void onErrorResponse(VolleyError volleyError) {
            C1049M.getMessageBus().post(new LoginOrCreateAccountMessage(volleyError));
        }

        public void onResponse(SignInApiModel model) {
            AccountManager.this.onSignInAuthorized(model);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.AccountManager.6 */
    class C11656 extends OnNetworkResponseListener<AccountApiModel> {
        C11656() {
        }

        public void onErrorResponse(VolleyError volleyError) {
            if (volleyError.networkResponse != null && volleyError.networkResponse.statusCode == 401) {
                AccountManager.this.signOut();
            }
        }

        public void onResponse(AccountApiModel model) {
            AccountManager.this.updateTimestamp();
            AccountManager.this.signIn(model);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.AccountManager.7 */
    class C11667 extends OnNetworkResponseListener<AccountUpdateReply> {
        C11667() {
        }

        public void onErrorResponse(VolleyError volleyError) {
            C1049M.getMessageBus().post(new UpdateAccountMessage(volleyError));
        }

        public void onResponse(AccountUpdateReply model) {
            AccountManager.this.onAccountUpdated(model);
            C1049M.getMessageBus().post(new UpdateAccountMessage(model));
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.AccountManager.8 */
    class C11678 implements OnAdListChangedListener {
        C11678() {
        }

        public void onNetworkError(VolleyError error) {
        }

        public void onListHalted() {
        }

        public void onListIsLoading() {
        }

        public void onListIsEmpty() {
        }

        public void onListIsComplete() {
        }

        public void onListUpdated() {
            C1049M.getMessageBus().post(new SavedSearchesCountersUpdated());
        }

        public void onListIsRefreshing() {
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.AccountManager.9 */
    class C11689 implements GraphJSONObjectCallback {
        final /* synthetic */ LoginResult val$loginResult;

        C11689(LoginResult loginResult) {
            this.val$loginResult = loginResult;
        }

        public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
            AuthToken token = new OneTimeAuthToken(this.val$loginResult.getAccessToken().getToken(), Type.FACEBOOK);
            C1049M.getMessageBus().post(new SocialSessionMessage(jsonObject.optString("email"), token));
            AccountManager.this.tagEventWithFacebookTokenInfo(EventType.CREDENTIALS_FACEBOOK_AUTH_SUCCESS, this.val$loginResult.getAccessToken().getToken());
            AccountManager.this.signInWithToken(token, true);
        }
    }

    public AccountManager(Context context) {
        this.countersListener = new C11678();
        this.mContext = context;
        this.preferences = new PreferencesManager(this.mContext);
        loginFromSavedData();
    }

    private void loginFromSavedData() {
        C1049M.getStorageManager().fromStorageAsync("ACCOUNT_FILE", AuthorizedAccount.class, this.mContext, new C11601());
    }

    private void saveAccountData() {
        if (isSignedIn()) {
            try {
                C1049M.getStorageManager().toStorage(new AuthorizedAccount(this.mAccountApiModel, this.mAccessToken, this.mTokenType), "ACCOUNT_FILE", this.mContext);
            } catch (IOException e) {
                Logger.error("AccountManager", "Could not store credentials", e);
            }
        }
    }

    private void deleteAccountData() {
        C1049M.getStorageManager().removeFromStorageAsync("ACCOUNT_FILE", new C11612());
        if (this.mUserProfileImagesManager != null) {
            this.mUserProfileImagesManager.deleteProfileImages();
            this.mUserProfileImagesManager = null;
        }
    }

    public boolean isSignedIn() {
        return (this.mAccountApiModel == null || this.mAccountApiModel.account == null) ? false : true;
    }

    public SavedSearchesList getSavedSearchesList() {
        return this.mSavedSearchesList;
    }

    public PreferencesManager getPreferences() {
        return this.preferences;
    }

    public String getAccountId() {
        if (isSignedIn()) {
            return this.mAccountApiModel.account.getCleanId();
        }
        return null;
    }

    public void updateAccountInfoIfNeeded() {
        if (isSignedIn() && new Date().getTime() - this.mAccountInfoTimestamp >= 120000) {
            fetchAccount(getAccountId());
        }
    }

    public void createAccount(AuthToken authToken, String email, boolean isUsingFacebook) {
        this.mIsUsingFacebook = isUsingFacebook;
        AccountsRequest createAccountRequest = new AccountsRequest();
        RequestAccount account = new RequestAccount();
        account.email = email;
        createAccountRequest.account = account;
        C1049M.getTrafficManager().doRequest(new Builder().requestId("CREATE_ACCOUNT_ID").endpoint(ApiEndpoint.CREATE_ACCOUNT).header("Authorization", authToken.generateToken()).body(createAccountRequest).listener(new C11623(isUsingFacebook)).build());
    }

    public void cancelCreateAccount() {
        C1049M.getTrafficManager().cancelRequest(ApiEndpoint.CREATE_ACCOUNT, "CREATE_ACCOUNT_ID");
    }

    public void signInWithToken(AuthToken authToken, boolean isUsingFacebook) {
        this.mIsUsingFacebook = isUsingFacebook;
        C1049M.getTrafficManager().doRequest(new Builder().header("Authorization", authToken.generateToken()).endpoint(ApiEndpoint.SIGN_IN).requestId("SIGN_IN_ID").cancelSameRequests(true).listener(new C11634()).build());
    }

    public void signInWithSocialToken(AuthToken authToken, RequirePasswordRequest account) {
        C1049M.getTrafficManager().doRequest(new Builder().header("Authorization", authToken.generateToken()).endpoint(ApiEndpoint.SIGN_IN).body(account).requestId("SIGN_IN_ID").cancelSameRequests(true).listener(new C11645()).build());
    }

    public void cancelSignIn() {
        C1049M.getTrafficManager().cancelRequest(ApiEndpoint.SIGN_IN, "SIGN_IN_ID");
    }

    private void onSignInAuthorized(SignInApiModel model) {
        String accountId = (model.account == null || model.account.accountId == null) ? BuildConfig.VERSION_NAME : model.account.getCleanId();
        if (!TextUtils.isEmpty(accountId) && this.mIsUsingFacebook) {
            tagEvent(EventType.CREDENTIALS_FACEBOOK_LOGIN_SUCCESS);
        } else if (!TextUtils.isEmpty(accountId)) {
            tagEvent(EventType.CREDENTIALS_LOGIN_SUCCESS);
        }
        C1049M.getMessageBus().post(new LoginOrCreateAccountMessage(model));
        setAuthorization(model.getAccessToken(), model.getTokenType(), accountId);
        fetchAccount(accountId);
    }

    private void onAccountUpdated(AccountUpdateReply model) {
        String accountId = model.account.getCleanId();
        Logger.debug("AccountManager", "cleanId " + accountId);
        setAuthorization(model.accessToken, model.tokenType, accountId);
        if (this.mAccountApiModel != null && this.mAccountApiModel.account.getCleanId().equals(model.account.getCleanId())) {
            this.mAccountApiModel.account = model.account;
            updateTimestamp();
        }
    }

    private void setAuthorization(String accessToken, String tokenType, String accountId) {
        if (!(accessToken == null || tokenType == null)) {
            this.mAccessToken = accessToken;
            this.mTokenType = tokenType;
        }
        Logger.debug("AccountManager", "accountId " + accountId);
        C1049M.getTrafficManager().setAuthorization(getAuthorization());
    }

    public String getAccessToken() {
        return this.mAccessToken;
    }

    private void fetchAccount(String accountId) {
        if (!TextUtils.isEmpty(accountId)) {
            C1049M.getTrafficManager().doRequest(new Builder().requestId("GET_ACCOUNT_ID").endpoint(ApiEndpoint.GET_ACCOUNT).parameter("account_id", accountId).parameter("ad_counts", Setting.TRUE).parameter("ad_events", Setting.TRUE).listener(new C11656()).build());
        }
    }

    private void updateTimestamp() {
        this.mAccountInfoTimestamp = new Date().getTime();
    }

    public void updateAccount(AccountUpdateRequest body) {
        C1049M.getTrafficManager().doRequest(new Builder().requestId("UPDATE_ACCOUNT_ID").endpoint(ApiEndpoint.UPDATE_ACCOUNT).parameter("account_id", getAccountId()).body(body).listener(new C11667()).build());
    }

    public void cancelUpdateAccount() {
        C1049M.getTrafficManager().cancelRequest(ApiEndpoint.UPDATE_ACCOUNT, "UPDATE_ACCOUNT_ID");
    }

    public void signIn(AccountApiModel accountApiModel) {
        if (accountApiModel != null && accountApiModel.account != null) {
            boolean statusChanged = false;
            String previousAccountId = this.mAccountApiModel == null ? null : this.mAccountApiModel.account.getCleanId();
            this.mAccountApiModel = accountApiModel;
            TrafficManager trafficManager = C1049M.getTrafficManager();
            this.mUserProfileImagesManager = new UserProfileImagesManager(this.mAccountApiModel.account, trafficManager.getImageLoader());
            this.preferences.loadUserPrefs(this.mContext, this.mAccountApiModel.account.getCleanId());
            if (!getAccountId().equals(previousAccountId)) {
                this.mSavedAdsManager = new SavedAdsManager(trafficManager, this);
                this.mSavedAdsManager.startLoading();
                this.mSavedSearchesList = new SavedSearchesList(getAccountId(), this.mContext);
                this.mSavedSearchesManager = new SavedSearchesManager(trafficManager, getSavedSearchesList());
                this.mSavedSearchesManager.setListChangeListener(this.countersListener);
                this.mMyAdListManager = new MyAdListManager(trafficManager, this);
                statusChanged = true;
            }
            saveAccountData();
            C1049M.getMessageBus().post(new AccountStatusChangedMessage(true, statusChanged));
        }
    }

    public void signOut() {
        boolean statusChanged;
        this.preferences.putSentUrbanAirshipIdToServer(false);
        if (this.mAccountApiModel == null) {
            statusChanged = true;
        } else {
            statusChanged = false;
        }
        this.mAccountApiModel = null;
        this.preferences.unloadUserPrefs();
        this.mSavedAdsManager = null;
        this.mMyAdListManager = null;
        this.mSavedSearchesList = null;
        this.mAccessToken = null;
        this.mTokenType = null;
        deleteAccountData();
        C1049M.getMessageBus().post(new AccountStatusChangedMessage(false, statusChanged));
    }

    public void loadThumbnailProfilePicture(Context context, UserProfileImagesManagerFetchingListener listener) {
        if (this.mUserProfileImagesManager != null) {
            this.mUserProfileImagesManager.fetchThumbnailImage(context, listener);
            return;
        }
        throw new RuntimeException("You cannot load a profile picture if you're not logged in.");
    }

    public void deleteProfilePictures() {
        if (this.mUserProfileImagesManager != null) {
            this.mUserProfileImagesManager.deleteProfileImages();
            return;
        }
        throw new RuntimeException("You cannot delete profile pictures if you're not logged in.");
    }

    public AccountApiModel getAccountApiModel() {
        return this.mAccountApiModel;
    }

    public String getAuthorization() {
        return this.mTokenType + " " + this.mAccessToken;
    }

    public SavedAdsManager getSavedAdsManager() {
        return this.mSavedAdsManager;
    }

    public MyAdListManager getMyAdListManager() {
        return this.mMyAdListManager;
    }

    public void startCountersUpdate() {
        this.mSavedSearchesManager.clear();
    }

    public CountersModel getCounters(int index) {
        CountersModel counters = null;
        if (isSignedIn()) {
            try {
                counters = (CountersModel) this.mSavedSearchesManager.getIndex(index, true).getModel();
            } catch (Exception e) {
                Logger.error("AccountManager", "Could not open saved searches, index " + index, e);
            }
            if (counters == null || !counters.isUpdated()) {
                this.mSavedSearchesManager.startLoading();
            }
        }
        return counters;
    }

    public boolean isRequirementsFulfilled(String[] requirements) {
        if (!C1049M.getAccountManager().isSignedIn()) {
            return false;
        }
        Account account = this.mAccountApiModel.account;
        if (account.canPublish == null || !account.canPublish.booleanValue()) {
            return false;
        }
        return true;
    }

    private void tagEventWithFacebookTokenInfo(EventType eventType, String authToken) {
        boolean isFacebookAuthCodeNullOrEmpty = authToken == null || authToken.trim().isEmpty();
        C1049M.getMessageBus().post(new EventBuilder().setIsFacebookAuthCodeNullOrEmpty(isFacebookAuthCodeNullOrEmpty).setEventType(eventType).build());
    }

    private void tagEvent(EventType eventType) {
        C1049M.getMessageBus().post(new EventBuilder().setEventType(eventType).build());
    }

    public void clearFacebookSession() {
        if (AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut();
        }
    }

    public void onSuccess(LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new C11689(loginResult));
        Bundle bundle = new Bundle();
        bundle.putString(GraphRequest.FIELDS_PARAM, "email");
        request.setParameters(bundle);
        request.executeAsync();
    }

    public void onCancel() {
        tagEvent(EventType.CREDENTIALS_FACEBOOK_AUTH_FAILURE);
    }

    public void onError(FacebookException e) {
        tagEvent(EventType.CREDENTIALS_FACEBOOK_AUTH_FAILURE);
        C1049M.getMessageBus().post(new SocialSessionMessage(null, null));
    }
}
