package com.schibsted.scm.nextgenapp.backend.managers;

import com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.models.internal.SingleParameterValue;
import com.schibsted.scm.nextgenapp.models.submodels.ErrorCause;
import com.schibsted.scm.nextgenapp.models.submodels.FilterDescription;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import com.schibsted.scm.nextgenapp.models.submodels.ParameterDefinition;
import com.schibsted.scm.nextgenapp.models.submodels.Setting;
import com.schibsted.scm.nextgenapp.models.submodels.SettingsParam;
import com.schibsted.scm.nextgenapp.utils.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class SearchParameterManager {
    private DbCategoryNode mCategory;
    private FilterDescription mFilterDesc;
    private Set<String> mKeysThatTriggerUpdate;
    private LinkedHashMap<String, ParameterState> mParamState;
    private Map<String, ParameterValue> mParamValues;
    private Map<String, ParameterDefinition> mParameterDefinitions;
    private RegionPathApiModel mRegion;

    public SearchParameterManager(FilterDescription filterDescription, RegionPathApiModel region, DbCategoryNode category, Map<String, ParameterValue> selectedValues) {
        setAllValues(filterDescription, region, category, selectedValues);
    }

    public SearchParameterManager(FilterDescription filterDescription, RegionPathApiModel region, DbCategoryNode category, Map<String, ParameterValue> selectedValues, Map<String, ParameterDefinition> selectedValuesDefinition) {
        this.mParameterDefinitions = new HashMap();
        if (selectedValuesDefinition != null) {
            this.mParameterDefinitions.putAll(selectedValuesDefinition);
        }
        setAllValues(filterDescription, region, category, selectedValues);
    }

    public void setAllValues(FilterDescription filterDescription, RegionPathApiModel region, DbCategoryNode category, Map<String, ParameterValue> selectedValues) {
        this.mFilterDesc = filterDescription;
        this.mParamState = new LinkedHashMap();
        this.mParamValues = new HashMap();
        this.mKeysThatTriggerUpdate = new HashSet();
        if (selectedValues != null) {
            this.mParamValues.putAll(selectedValues);
        }
        deleteRegionParamStates(this.mRegion);
        this.mRegion = region;
        addRegionParamStates(this.mRegion);
        setCategory(category);
        applyFiltersDescription(filterDescription);
    }

    public void applyFiltersDescription(FilterDescription filterDescription) {
        if (filterDescription != null) {
            this.mFilterDesc = filterDescription;
            for (SettingsParam settingsParam : this.mFilterDesc.settingsParam) {
                if (settingsParam.keys != null) {
                    for (String key : settingsParam.keys) {
                        String key2;
                        if (key2.startsWith(Setting.WILDCARD) || key2.startsWith("?")) {
                            key2 = key2.substring(1);
                        }
                        this.mKeysThatTriggerUpdate.add(key2);
                    }
                }
            }
            updateState(null);
        }
    }

    public void clear() {
        this.mRegion = null;
        this.mCategory = null;
        this.mParamState.clear();
        this.mParamValues.clear();
        if (this.mParameterDefinitions != null) {
            this.mParameterDefinitions.clear();
        }
        updateState(null);
    }

    public void clearFilters() {
        this.mParamState.clear();
        this.mParamValues.clear();
        if (this.mParameterDefinitions != null) {
            this.mParameterDefinitions.clear();
        }
        updateState(null);
    }

    public void setRegion(RegionPathApiModel region) {
        deleteRegionParamStates(this.mRegion);
        this.mRegion = region;
        addRegionParamStates(this.mRegion);
        updateState(null);
    }

    private void deleteRegionParamStates(RegionPathApiModel region) {
        if (region != null) {
            Identifier identifier = region.getIdentifier();
            if (identifier != null) {
                for (int i = 0; i < identifier.keys.size(); i++) {
                    this.mParamValues.remove(identifier.keys.get(i));
                }
            }
        }
    }

    private void addRegionParamStates(RegionPathApiModel region) {
        if (region != null) {
            Identifier identifier = region.getIdentifier();
            if (identifier != null) {
                for (int i = 0; i < identifier.keys.size(); i++) {
                    this.mParamValues.put(identifier.keys.get(i), new SingleParameterValue((String) identifier.values.get(i)));
                }
            }
        }
    }

    public void setCategory(DbCategoryNode category) {
        this.mCategory = category;
        updateState(null);
    }

    public DbCategoryNode getCategory() {
        return this.mCategory;
    }

    public RegionPathApiModel getRegion() {
        return this.mRegion;
    }

    public boolean update(ParameterDefinition def, ParameterValue newValue) {
        if (newValue == null || newValue.isEmpty()) {
            this.mParamValues.remove(def.key);
            if (this.mParameterDefinitions != null) {
                this.mParameterDefinitions.remove(def.key);
            }
        } else {
            this.mParamValues.put(def.key, newValue);
            if (this.mParameterDefinitions != null) {
                this.mParameterDefinitions.put(def.key, def);
            }
        }
        if (this.mKeysThatTriggerUpdate.contains(def.key)) {
            updateState(def);
            return true;
        } else if (!this.mParamState.containsKey(def.key)) {
            return false;
        } else {
            ((ParameterState) this.mParamState.get(def.key)).setValues(newValue);
            return false;
        }
    }

    public HashMap<String, ParameterValue> getUpdatedParameterValues() {
        HashMap<String, ParameterValue> parameterValues = new HashMap();
        for (Entry<String, ParameterState> set : this.mParamState.entrySet()) {
            ParameterState state = (ParameterState) set.getValue();
            if (!(state.getValues() == null || state.getValues().isEmpty())) {
                parameterValues.put(state.getDefinition().key, state.getValues());
            }
        }
        return parameterValues;
    }

    public Map<String, ParameterDefinition> getParameterDefinitions() {
        return this.mParameterDefinitions;
    }

    public boolean parametersAreEmpty() {
        for (Entry<String, ParameterState> set : this.mParamState.entrySet()) {
            ParameterState state = (ParameterState) set.getValue();
            if (!(state.getValues() == null || state.getValues().isEmpty())) {
                if (!paramMustHaveDefault(state.getDefinition()) || !state.getValues().equals(state.getDefinition().getInitialValue())) {
                    return false;
                }
            }
        }
        return true;
    }

    private Map<String, List<String>> createParameterSettings() {
        int count;
        int i;
        Map<String, List<String>> stateKeys = new HashMap();
        if (!(this.mRegion == null || this.mRegion.getIdentifier() == null)) {
            count = this.mRegion.getIdentifier().keys.size();
            for (i = 0; i < count; i++) {
                stateKeys.put(this.mRegion.getIdentifier().keys.get(i), Utils.value(this.mRegion.getIdentifier().values.get(i)));
            }
        }
        if (!(this.mCategory == null || this.mCategory.getPath() == null)) {
            Identifier id = new Identifier(this.mCategory.getPath());
            count = id.keys.size();
            for (i = 0; i < count; i++) {
                stateKeys.put(id.keys.get(i), Utils.value(id.values.get(i)));
            }
        }
        if (this.mParamValues != null) {
            for (Entry<String, ParameterValue> set : this.mParamValues.entrySet()) {
                stateKeys.put(set.getKey(), ((ParameterValue) set.getValue()).toParameter());
            }
        }
        return stateKeys;
    }

    private void updateState(ParameterDefinition def) {
        ArrayList<ParameterDefinition> parameterDefinitions = new ArrayList();
        boolean finished = false;
        int infiniteLoopCounter = 5;
        while (!finished && infiniteLoopCounter > 0) {
            parameterDefinitions = getParameterDefinitions(getParameterKeys());
            finished = transitionState(parameterDefinitions);
            infiniteLoopCounter--;
        }
        setState(parameterDefinitions);
    }

    protected ArrayList<ParameterDefinition> getParameterDefinitions(List<String> parameterKeys) {
        ArrayList<ParameterDefinition> definitions = new ArrayList();
        for (String key : parameterKeys) {
            if (this.mFilterDesc.paramMap.containsKey(key)) {
                definitions.add(((ParameterDefinition) this.mFilterDesc.paramMap.get(key)).copy());
            }
        }
        return definitions;
    }

    private List<String> getParameterKeys() {
        Map<String, List<String>> state = createParameterSettings();
        if (this.mFilterDesc.settingsParam != null) {
            for (SettingsParam settingsParam : this.mFilterDesc.settingsParam) {
                for (Setting setting : settingsParam.settings) {
                    if (matchState(settingsParam.keys, setting.values, state)) {
                        return setting.settingsResult;
                    }
                }
            }
        }
        return new ArrayList();
    }

    private boolean transitionState(ArrayList<ParameterDefinition> paramDefList) {
        Iterator i$ = paramDefList.iterator();
        while (i$.hasNext()) {
            ParameterDefinition paramDef = (ParameterDefinition) i$.next();
            boolean valueChanged = false;
            ParameterValue currentValue = (ParameterValue) this.mParamValues.get(paramDef.key);
            if (!(paramDef.isBasedOnDatabase().booleanValue() || paramDef.isBasedOnLocation().booleanValue() || paramDef.isValidValue(currentValue) || paramDef.presentation.equals("custom_category"))) {
                currentValue = null;
                valueChanged = this.mParamValues.remove(paramDef.key) != null;
            }
            if (currentValue == null && paramMustHaveDefault(paramDef)) {
                ParameterValue newInitialValue = paramDef.getInitialValue();
                if (newInitialValue != null) {
                    this.mParamValues.put(paramDef.key, newInitialValue);
                    valueChanged = true;
                }
            }
            if (valueChanged && this.mKeysThatTriggerUpdate.contains(paramDef.key)) {
                return false;
            }
        }
        return true;
    }

    private void setState(ArrayList<ParameterDefinition> paramDefList) {
        LinkedHashMap<String, ParameterState> previousStateMap = this.mParamState;
        this.mParamState = new LinkedHashMap();
        Iterator i$ = paramDefList.iterator();
        while (i$.hasNext()) {
            ParameterDefinition parameter = (ParameterDefinition) i$.next();
            ParameterState newState = new ParameterState(parameter, (ParameterValue) this.mParamValues.get(parameter.key));
            if (previousStateMap.containsKey(parameter.key)) {
                ParameterState previousState = (ParameterState) previousStateMap.get(parameter.key);
                if ((previousState.getValues() != null && previousState.getValues().equals(newState.getValues())) || (previousState.getValues() == null && newState.getValues() == null)) {
                    newState.setErrorMessage(previousState.getErrorMessage());
                }
            }
            this.mParamState.put(parameter.key, newState);
        }
    }

    public void setError(ErrorCause cause) {
        if (this.mParamState.containsKey(cause.field)) {
            ((ParameterState) this.mParamState.get(cause.field)).setErrorMessage(cause.label);
        }
    }

    private boolean paramMustHaveDefault(ParameterDefinition def) {
        return "radio".equals(def.presentation) || "sorting".equals(def.presentation);
    }

    protected boolean matchState(List<String> keys, List<String> values, Map<String, List<String>> inputState) {
        if (keys == null) {
            keys = new ArrayList();
        }
        if (values == null) {
            values = new ArrayList();
        }
        if (keys.size() != values.size()) {
            return false;
        }
        for (int i = 0; i < keys.size(); i++) {
            String subjectKey = (String) keys.get(i);
            String subjectValue = (String) values.get(i);
            if (subjectKey.startsWith("?")) {
                String actualKey = subjectKey.substring(1);
                if (subjectValue.equals(Setting.TRUE) && !inputState.containsKey(actualKey)) {
                    return false;
                }
                if (subjectValue.equals(Setting.FALSE) && inputState.containsKey(actualKey)) {
                    return false;
                }
            } else if (!inputState.containsKey(subjectKey)) {
                return false;
            } else {
                if (!(Setting.WILDCARD.equals(subjectValue) || ((List) inputState.get(subjectKey)).contains(subjectValue))) {
                    return false;
                }
            }
        }
        return true;
    }

    public LinkedHashMap<String, ParameterState> getState() {
        return this.mParamState;
    }
}
