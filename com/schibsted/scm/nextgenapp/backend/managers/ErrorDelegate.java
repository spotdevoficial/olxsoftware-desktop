package com.schibsted.scm.nextgenapp.backend.managers;

import android.content.Context;
import android.text.TextUtils;
import com.android.volley.VolleyError;
import com.schibsted.scm.nextgenapp.backend.network.ApiErrorResponse;
import com.schibsted.scm.nextgenapp.models.ErrorDescription;
import com.schibsted.scm.nextgenapp.models.ErrorResponseApiModel;
import com.schibsted.scm.nextgenapp.models.submodels.ErrorCause;
import com.schibsted.scm.nextgenapp.models.submodels.ErrorCode;
import com.schibsted.scm.nextgenapp.ui.views.ErrorView;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ErrorDelegate {
    private static final String DEFAULT_OBSERVER;
    private ErrorObserver causeObserver;
    private Map<String, CauseObserver> causeObservers;
    private String default_error;
    private ErrorDescription errorModel;
    private ErrorObserver errorObserver;
    private Map<String, ErrorView> errorObserverViews;
    private VolleyError errorResponse;
    private ErrorViewObserver errorViewObserver;
    private Map<String, ErrorView> errorViews;

    public interface ErrorViewObserver {
        void onErrorViews(Map<String, ErrorView> map);
    }

    public interface CauseObserver {
        void handleCause(ErrorCause errorCause);
    }

    public interface ErrorObserver {
        void handleError(ErrorDescription errorDescription);
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate.1 */
    static /* synthetic */ class C11741 {
        static final /* synthetic */ int[] f1283x4b8cf124;

        static {
            f1283x4b8cf124 = new int[ErrorCode.values().length];
            try {
                f1283x4b8cf124[ErrorCode.COMMUNICATION_ERROR.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f1283x4b8cf124[ErrorCode.VALIDATION_FAILED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f1283x4b8cf124[ErrorCode.ACCOUNT_EXISTS.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f1283x4b8cf124[ErrorCode.ACCOUNT_NOT_FOUND.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f1283x4b8cf124[ErrorCode.EMAIL_NOT_VERIFIED.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                f1283x4b8cf124[ErrorCode.FORBIDDEN.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                f1283x4b8cf124[ErrorCode.INVALID_LANGUAGE.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                f1283x4b8cf124[ErrorCode.LOGIN_FAILED.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                f1283x4b8cf124[ErrorCode.NOT_FOUND.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                f1283x4b8cf124[ErrorCode.NOT_IN_BOUNDS.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                f1283x4b8cf124[ErrorCode.UNAUTHORIZED.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                f1283x4b8cf124[ErrorCode.INTERNAL_ERROR.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                f1283x4b8cf124[ErrorCode.UNKNOWN_ERROR.ordinal()] = 13;
            } catch (NoSuchFieldError e13) {
            }
        }
    }

    static {
        DEFAULT_OBSERVER = null;
    }

    public ErrorDelegate(Context context) {
        this.errorObserverViews = new HashMap();
        this.causeObservers = new HashMap();
        this.default_error = context.getResources().getString(2131165577);
    }

    public ErrorDelegate onErrorWithCause(ErrorObserver observer) {
        this.causeObserver = observer;
        return this;
    }

    public ErrorDelegate onError(ErrorObserver observer) {
        this.errorObserver = observer;
        return this;
    }

    public ErrorDelegate onViewsWithErrors(ErrorViewObserver collector) {
        this.errorViewObserver = collector;
        return this;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void delegate(com.android.volley.VolleyError r3) {
        /*
        r2 = this;
        if (r3 != 0) goto L_0x000a;
    L_0x0002:
        r0 = new java.lang.IllegalArgumentException;
        r1 = "Error is null";
        r0.<init>(r1);
        throw r0;
    L_0x000a:
        r2.interpretError(r3);
        r0 = com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate.C11741.f1283x4b8cf124;
        r1 = r2.errorModel;
        r1 = r1.code;
        r1 = r1.ordinal();
        r0 = r0[r1];
        switch(r0) {
            case 1: goto L_0x0043;
            case 2: goto L_0x0043;
            default: goto L_0x001c;
        };
    L_0x001c:
        r0 = r2.errorObserver;
        if (r0 == 0) goto L_0x0027;
    L_0x0020:
        r0 = r2.errorObserver;
        r1 = r2.errorModel;
        r0.handleError(r1);
    L_0x0027:
        r0 = r2.errorModel;
        r0 = r0.code;
        r1 = com.schibsted.scm.nextgenapp.models.submodels.ErrorCode.UNAUTHORIZED;
        if (r0 != r1) goto L_0x0042;
    L_0x002f:
        r0 = com.schibsted.scm.nextgenapp.C1049M.getAccountManager();
        r0.signOut();
        r0 = com.schibsted.scm.nextgenapp.C1049M.getMessageBus();
        r1 = new com.schibsted.scm.nextgenapp.backend.bus.messages.AccountUnauthorizedMessage;
        r1.<init>();
        r0.post(r1);
    L_0x0042:
        return;
    L_0x0043:
        r0 = r2.causeObserver;
        if (r0 == 0) goto L_0x004e;
    L_0x0047:
        r0 = r2.causeObserver;
        r1 = r2.errorModel;
        r0.handleError(r1);
    L_0x004e:
        r0 = r2.errorModel;
        r0 = r0.causes;
        if (r0 == 0) goto L_0x001c;
    L_0x0054:
        r0 = r2.errorModel;
        r0 = r0.causes;
        r0 = r0.size();
        if (r0 <= 0) goto L_0x001c;
    L_0x005e:
        r0 = r2.errorModel;
        r0 = r0.causes;
        r2.delegateCauses(r0);
        goto L_0x0027;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate.delegate(com.android.volley.VolleyError):void");
    }

    private void interpretError(VolleyError volleyError) {
        this.errorResponse = volleyError;
        if (volleyError instanceof ApiErrorResponse) {
            ErrorResponseApiModel error = ((ApiErrorResponse) volleyError).getErrorModel();
            if (error != null) {
                this.errorModel = error.error;
            }
        }
        if (this.errorModel == null) {
            this.errorModel = new ErrorDescription();
            if (this.errorResponse.networkResponse != null) {
                this.errorModel.setNetworkCode(this.errorResponse.networkResponse.statusCode);
                return;
            }
            this.errorModel.code = ErrorCode.UNKNOWN_ERROR;
        }
    }

    public ErrorDelegate onCause(String field, ErrorView view) {
        this.errorObserverViews.put(field, view);
        return this;
    }

    public ErrorDelegate onUnhandledCause(CauseObserver observer) {
        this.causeObservers.put(DEFAULT_OBSERVER, observer);
        return this;
    }

    public ErrorDelegate onCause(String field, CauseObserver observer) {
        this.causeObservers.put(field, observer);
        return this;
    }

    private void delegateCauses(List<ErrorCause> causes) {
        this.errorViews = new HashMap();
        for (ErrorCause cause : causes) {
            if (TextUtils.isEmpty(cause.label)) {
                cause.label = this.default_error;
            }
            boolean observed = false;
            if (this.errorObserverViews.containsKey(cause.field)) {
                ErrorView view = (ErrorView) this.errorObserverViews.get(cause.field);
                view.setErrorMessage(cause.label);
                this.errorViews.put(cause.field, view);
                observed = true;
            }
            if (this.causeObservers.containsKey(cause.field)) {
                ((CauseObserver) this.causeObservers.get(cause.field)).handleCause(cause);
                observed = true;
            }
            if (!observed && this.causeObservers.containsKey(DEFAULT_OBSERVER)) {
                ((CauseObserver) this.causeObservers.get(DEFAULT_OBSERVER)).handleCause(cause);
            }
        }
        for (Entry<String, ErrorView> entry : this.errorObserverViews.entrySet()) {
            if (!(this.errorViews.containsKey(entry.getKey()) || entry.getValue() == null)) {
                ((ErrorView) entry.getValue()).clearErrorMessage();
            }
        }
        if (this.errorViewObserver != null) {
            this.errorViewObserver.onErrorViews(this.errorViews);
        }
    }
}
