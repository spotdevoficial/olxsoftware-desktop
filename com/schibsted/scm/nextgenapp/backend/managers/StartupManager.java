package com.schibsted.scm.nextgenapp.backend.managers;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import com.facebook.FacebookSdk;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.bus.messages.ConfigChangedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.ConfigNetworkErrorMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.MetaDataManagerCompletedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.RegionDataChangeMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.StartupProcedureStartedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.UserLocationReceivedMessage;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteAdListManager;
import com.schibsted.scm.nextgenapp.database.DaoManager;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.submodels.Coordinate;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import com.schibsted.scm.nextgenapp.tracking.EventRouter;
import com.schibsted.scm.nextgenapp.tracking.TrackingInitializer;
import com.squareup.otto.Subscribe;
import java.util.Calendar;

public class StartupManager {
    private static StartupManager instance;
    private Application mApplication;
    private Context mContext;
    private int mStatus;

    private StartupManager() {
        this.mStatus = -1;
    }

    public synchronized void initiate(Application application) {
        this.mStatus = 0;
        this.mApplication = application;
        this.mContext = application.getApplicationContext();
        FacebookSdk.sdkInitialize(this.mContext);
        C1049M.getMessageBus().register(this);
        if (C1049M.getStorageManager() == null) {
            C1049M.setStorageManager(new LocalStorageManager(this.mContext));
        }
        if (C1049M.getDaoManager() == null) {
            C1049M.setDaoManager(new DaoManager(this.mContext));
        }
        if (C1049M.getTrafficManager() == null) {
            C1049M.setTrafficManager(new TrafficManagerImpl(this.mContext, C1049M.getMessageBus()));
        }
        if (C1049M.getMainAdListManager() == null) {
            RemoteAdListManager manager = new RemoteAdListManager(C1049M.getTrafficManager());
            C1049M.setMainAdListManager(manager);
            PreferencesManager prefs = new PreferencesManager(this.mContext);
            Identifier regionId = prefs.loadDefaultRegion();
            if (regionId != null) {
                manager.getSearchParameters().setRegion(new RegionPathApiModel(regionId, prefs.loadDefaultRegionLabel()));
            }
        }
        if (C1049M.getJobManager() == null) {
            C1049M.setJobManager(new JobQueueManager(this.mContext));
        }
        if (C1049M.getEventRouter() == null) {
            EventRouter router = new EventRouter(this.mApplication);
            router.setBeforeEventsListener(new TrackingInitializer(C1049M.getMessageBus()));
            C1049M.getMessageBus().register(router);
            C1049M.setEventRouter(router);
        }
        if (C1049M.getGeolocationManager() == null) {
            C1049M.setGeolocationManager(new GeolocationManager(this.mContext));
        }
        if (C1049M.getConfigManager() == null) {
            C1049M.setConfigManager(new ConfigManager(C1049M.getTrafficManager()));
        }
        if (C1049M.getAccountManager() == null) {
            C1049M.setAccountManager(new AccountManager(this.mContext));
        }
        C1049M.getMainAdListManager().startLoading();
        C1049M.getConfigManager().start();
    }

    @Subscribe
    public void onConfigChangedMessageReceived(ConfigChangedMessage msg) {
        if (msg.hasFinishedLoadingProcess() && msg.isFailed()) {
            this.mStatus = 3;
        }
    }

    @Subscribe
    public void onRegionDataComplete(RegionDataChangeMessage msg) {
        if (this.mStatus == 0) {
            this.mStatus = 1;
        }
    }

    @Subscribe
    public void onMetaDataManagerCompleted(MetaDataManagerCompletedMessage msg) {
        this.mStatus = 2;
        C1049M.getMessageBus().post(new StartupProcedureStartedMessage());
        C1049M.getMessageBus().unregister(this);
    }

    @Subscribe
    public void onNetworkErrorMessage(ConfigNetworkErrorMessage message) {
    }

    public void notifyConnectionAvailable() {
        if (!(this.mStatus == -1 || this.mStatus == 2)) {
            C1049M.getMessageBus().unregister(this);
        }
        initiate(this.mApplication);
    }

    public int getStatus() {
        return this.mStatus;
    }

    @Subscribe
    public void onUserLocationReceivedMessage(UserLocationReceivedMessage msg) {
        Coordinate c;
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this.mContext);
        if (msg.isSuccess()) {
            c = new Coordinate(msg.getLatitude(), msg.getLongitude());
            Editor spEditor = sp.edit();
            spEditor.putFloat("LAST_KNOWN_LOC_LATITUDE", (float) c.latitude);
            spEditor.putFloat("LAST_KNOWN_LOC_LONGITUDE", (float) c.longitude);
            spEditor.putLong("LAST_KNOWN_LOC_TIMESTAMP", Calendar.getInstance().getTimeInMillis());
            spEditor.apply();
        } else {
            c = null;
        }
        C1049M.getMainAdListManager().getSearchParameters().setLocationCoordinates(c);
        C1049M.getGeolocationManager().stop();
    }

    public static synchronized StartupManager getInstance() {
        StartupManager startupManager;
        synchronized (StartupManager.class) {
            if (instance == null) {
                instance = new StartupManager();
            }
            startupManager = instance;
        }
        return startupManager;
    }
}
