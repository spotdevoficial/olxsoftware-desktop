package com.schibsted.scm.nextgenapp.backend.managers;

import android.content.Context;
import android.graphics.Bitmap;
import android.webkit.MimeTypeMap;
import com.android.volley.Cache;
import com.android.volley.Cache.Entry;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RequestQueue.RequestFilter;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageCache;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration.Builder;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.modules.MyRetryPolicy;
import com.schibsted.scm.nextgenapp.backend.network.APIRequest;
import com.schibsted.scm.nextgenapp.backend.network.HybridHttpStack;
import com.schibsted.scm.nextgenapp.backend.network.NextGenImageDownloader;
import com.schibsted.scm.nextgenapp.backend.network.ProgressInputStream;
import com.schibsted.scm.nextgenapp.backend.request.NetworkRequest;
import com.schibsted.scm.nextgenapp.backend.request.NetworkRequest.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.backend.request.PrefetchRequest;
import com.schibsted.scm.nextgenapp.config.ApiEndpoint;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import com.schibsted.scm.nextgenapp.models.submodels.Prefetch;
import com.schibsted.scm.nextgenapp.ssl.SslSocketHelper;
import com.schibsted.scm.nextgenapp.utils.BitmapLruCache;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.schibsted.scm.nextgenapp.utils.logger.AnswersLogger;
import com.schibsted.scm.nextgenapp.utils.logger.NetworkLogger;
import com.schibsted.scm.nextgenapp.utils.logger.NetworkLoggerImpl;
import com.squareup.mimecraft.Multipart;
import com.squareup.mimecraft.Multipart.Type;
import com.squareup.mimecraft.Part;
import com.squareup.okhttp.OkHttpClient;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.net.ssl.SSLSocketFactory;

public class TrafficManagerImpl implements TrafficManager {
    private String mApiUrl;
    private String mAuthorization;
    private Context mContext;
    private ImageLoader mIconImageLoader;
    private RequestQueue mJsonRequestQueue;
    private Request mLastRequest;
    private RequestQueue mPrefetchesQueue;
    private SSLSocketFactory mSSLSocketFactory;

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.TrafficManagerImpl.1 */
    class C11811 implements ImageCache {
        private final BitmapLruCache mCache;

        C11811() {
            this.mCache = new BitmapLruCache();
        }

        public void putBitmap(String url, Bitmap bitmap) {
            this.mCache.put(url, bitmap);
        }

        public Bitmap getBitmap(String url) {
            return (Bitmap) this.mCache.get(url);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.TrafficManagerImpl.2 */
    class C11822 implements RequestFilter {
        final /* synthetic */ List val$requestIds;

        C11822(List list) {
            this.val$requestIds = list;
        }

        public boolean apply(Request<?> request) {
            return this.val$requestIds.contains(request.getTag());
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.TrafficManagerImpl.3 */
    class C11833 implements RequestFilter {
        C11833() {
        }

        public boolean apply(Request<?> request) {
            return true;
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.TrafficManagerImpl.4 */
    class C11844 implements RequestFilter {
        final /* synthetic */ Object val$requestId;

        C11844(Object obj) {
            this.val$requestId = obj;
        }

        public boolean apply(Request<?> request) {
            return this.val$requestId.equals(request.getTag());
        }
    }

    public TrafficManagerImpl(Context context, MessageBus bus) {
        this.mContext = context;
        com.nostra13.universalimageloader.core.ImageLoader.getInstance().init(new Builder(context).defaultDisplayImageOptions(new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true).build()).threadPoolSize(3).memoryCache(new WeakMemoryCache()).tasksProcessingOrder(QueueProcessingType.LIFO).imageDownloader(new NextGenImageDownloader(this.mContext)).build());
        this.mSSLSocketFactory = SslSocketHelper.getSslSocketFactory(this.mContext);
        this.mJsonRequestQueue = createRequestQueue(context, "jsonCache", hp.f177M);
        this.mPrefetchesQueue = createRequestQueue(context, "jsonPrefetchesCache", 5242880);
        this.mIconImageLoader = new ImageLoader(this.mPrefetchesQueue, new C11811());
        this.mApiUrl = ConfigContainer.getConfig().getApiUrl();
        this.mContext = context;
        bus.register(this);
    }

    public void setApiUrl(String apiUrl) {
        this.mApiUrl = apiUrl;
        this.mSSLSocketFactory = SslSocketHelper.getSslSocketFactory(this.mContext);
        cancelRequests(null);
        if (this.mJsonRequestQueue != null) {
            this.mJsonRequestQueue.stop();
            this.mJsonRequestQueue.getCache().clear();
        }
        if (this.mPrefetchesQueue != null) {
            this.mPrefetchesQueue.stop();
            this.mPrefetchesQueue.getCache().clear();
        }
        this.mJsonRequestQueue = createRequestQueue(this.mContext, "jsonCache", hp.f177M);
        this.mPrefetchesQueue = createRequestQueue(this.mContext, "jsonPrefetchesCache", 5242880);
    }

    private HttpStack createNetworkStack() {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setSslSocketFactory(this.mSSLSocketFactory);
        okHttpClient.setHostnameVerifier(SslSocketHelper.getHostNameVerifier());
        return new HybridHttpStack(this.mSSLSocketFactory, okHttpClient);
    }

    public void invalidateConfigCache() {
        this.mPrefetchesQueue.getCache().remove(Utils.getUrl(ApiEndpoint.CONFIG.getPath(), null));
    }

    public void updateCache(Map<String, Prefetch> mapOfPrefetches) {
        Cache cache = this.mPrefetchesQueue.getCache();
        if (cache != null && mapOfPrefetches != null) {
            for (Prefetch prefetch : mapOfPrefetches.values()) {
                String fullPath = null;
                if (prefetch.path != null) {
                    fullPath = Utils.getUrl(prefetch.path, null);
                }
                if (fullPath != null) {
                    if (!prefetch.getNormalizedEtag().equals(getETagWithFullPath(fullPath))) {
                        cache.remove(fullPath);
                        doPrefetch(fullPath);
                    }
                }
            }
        }
    }

    public String getETag(String path) {
        return getETagWithFullPath(Utils.getUrl(path, null));
    }

    private String getETagWithFullPath(String fullPath) {
        Cache cache = this.mPrefetchesQueue.getCache();
        if (cache != null) {
            Entry entry = cache.get(fullPath);
            if (entry != null) {
                return entry.etag;
            }
        }
        return null;
    }

    private RequestQueue createRequestQueue(Context context, String cacheDirString, int size) {
        File cacheDir = new File(context.getCacheDir(), cacheDirString);
        Network network = new BasicNetwork(createNetworkStack());
        DiskBasedCache cache = new DiskBasedCache(cacheDir, size);
        cache.initialize();
        RequestQueue queue = new RequestQueue(cache, network);
        queue.start();
        return queue;
    }

    public boolean isCached(APIRequest request) {
        RequestQueue targetQueue;
        String url = Utils.getUrl(request.getEndpoint().getPath(), request.getParameters());
        if (ConfigManager.mustUseEtagCaching(request.getEndpoint().getPath(), request.getParameters())) {
            targetQueue = this.mPrefetchesQueue;
        } else {
            targetQueue = this.mJsonRequestQueue;
        }
        Entry cacheEntry = targetQueue.getCache().get(url);
        if (cacheEntry == null || cacheEntry.isExpired()) {
            return false;
        }
        return true;
    }

    public void doRequest(APIRequest request) {
        doRequest(request, false);
    }

    public void doRequest(APIRequest request, boolean forceFetch) {
        NetworkRequest networkRequest;
        RequestQueue targetQueue;
        ApiEndpoint endpoint = request.getEndpoint();
        String url = Utils.getUrl(endpoint.getPath(), request.getParameters());
        if (forceFetch) {
            invalidateCacheOf(url);
        }
        String bodyString = null;
        try {
            if (request.getBody() != null) {
                bodyString = JsonMapper.getInstance().writeValueAsString(request.getBody());
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        HashMap<String, String> headers = new HashMap();
        if (request.getHeaders() != null) {
            headers.putAll(request.getHeaders());
        }
        if (endpoint.requiresAuthentication()) {
            headers.put("Authorization", this.mAuthorization);
        }
        OnNetworkResponseListener requestListener = request.getListener();
        NetworkLogger networkLogger = new AnswersLogger(endpoint.toString(), new NetworkLoggerImpl());
        if (request.isMultiPart()) {
            Multipart.Builder builder = new Multipart.Builder();
            builder.type(Type.FORM);
            try {
                InputStream inputStream = new ProgressInputStream(this.mContext.getContentResolver().openInputStream(request.getImageUri()), Utils.getFileSize(this.mContext, request.getImageUri()), requestListener);
                if (inputStream == null) {
                    requestListener.onErrorResponse(new VolleyError("Image stream empty"));
                    return;
                }
                if (bodyString != null) {
                    builder.addPart(new Part.Builder().contentType("application/json").body(bodyString).build());
                }
                String mime = Utils.getMimeType(request.getImageUri(), this.mContext);
                if (mime == null) {
                    mime = "image/jpeg";
                }
                Multipart.Builder builder2 = builder;
                builder2.addPart(new Part.Builder().contentDisposition("form-data; name=\"data\"; filename=\"" + System.currentTimeMillis() + "." + MimeTypeMap.getSingleton().getExtensionFromMimeType(mime) + "\"").contentType(mime).body(inputStream).build());
                networkRequest = new NetworkRequest(request.getRequestId(), networkLogger, endpoint.getMethod(), url, null, builder.build(), inputStream, headers, endpoint.getModel(), requestListener, endpoint.getParser(), new MyRetryPolicy(150000, 0, MediaUploadState.IMAGE_PROGRESS_UPLOADED), endpoint.getResponseParser());
            } catch (Throwable e2) {
                requestListener.onErrorResponse(new VolleyError(e2));
                return;
            } catch (Throwable e22) {
                requestListener.onErrorResponse(new VolleyError(e22));
                return;
            } catch (Throwable e222) {
                requestListener.onErrorResponse(new VolleyError(e222));
                return;
            }
        }
        NetworkRequest networkRequest2 = new NetworkRequest(request.getRequestId(), networkLogger, endpoint.getMethod(), url, bodyString, null, null, headers, endpoint.getModel(), requestListener, endpoint.getParser(), new MyRetryPolicy(20000, 2, MediaUploadState.IMAGE_PROGRESS_UPLOADED), endpoint.getResponseParser());
        if (ConfigManager.mustUseEtagCaching(endpoint.getPath(), request.getParameters())) {
            targetQueue = this.mPrefetchesQueue;
        } else {
            targetQueue = this.mJsonRequestQueue;
        }
        if (request.shouldCancelSameRequests()) {
            cancelRequest(targetQueue, request.getRequestId());
        }
        targetQueue.add(networkRequest);
        this.mLastRequest = networkRequest;
    }

    private void invalidateCacheOf(String url) {
        Cache cache = this.mPrefetchesQueue.getCache();
        if (cache != null) {
            cache.remove(url);
        }
    }

    private void doPrefetch(String url) {
        RequestQueue targetQueue = this.mPrefetchesQueue;
        PrefetchRequest req = new PrefetchRequest(0, url, null, new MyRetryPolicy(20000, 2, MediaUploadState.IMAGE_PROGRESS_UPLOADED));
        targetQueue.add(req);
        this.mLastRequest = req;
    }

    public void cancelRequests(List<?> requestIds) {
        RequestFilter filter;
        if (requestIds != null) {
            filter = new C11822(requestIds);
        } else {
            filter = new C11833();
        }
        synchronized (this.mJsonRequestQueue) {
            this.mJsonRequestQueue.cancelAll(filter);
        }
        synchronized (this.mPrefetchesQueue) {
            this.mPrefetchesQueue.cancelAll(filter);
        }
    }

    public void setAuthorization(String authorization) {
        this.mAuthorization = authorization;
    }

    public void cancelRequest(ApiEndpoint endpoint, Object requestId) {
        cancelRequest(this.mPrefetchesQueue, requestId);
        cancelRequest(this.mJsonRequestQueue, requestId);
    }

    private void cancelRequest(RequestQueue targetQueue, Object requestId) {
        synchronized (targetQueue) {
            targetQueue.cancelAll(new C11844(requestId));
        }
    }

    public com.nostra13.universalimageloader.core.ImageLoader getImageLoader() {
        return com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    }

    public ImageLoader getIconImageLoader() {
        return this.mIconImageLoader;
    }
}
