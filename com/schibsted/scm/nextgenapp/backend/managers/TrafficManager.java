package com.schibsted.scm.nextgenapp.backend.managers;

import com.android.volley.toolbox.ImageLoader;
import com.schibsted.scm.nextgenapp.backend.network.APIRequest;
import com.schibsted.scm.nextgenapp.config.ApiEndpoint;
import com.schibsted.scm.nextgenapp.models.submodels.Prefetch;
import java.util.List;
import java.util.Map;

public interface TrafficManager {
    void cancelRequest(ApiEndpoint apiEndpoint, Object obj);

    void cancelRequests(List<?> list);

    void doRequest(APIRequest aPIRequest);

    void doRequest(APIRequest aPIRequest, boolean z);

    String getETag(String str);

    ImageLoader getIconImageLoader();

    com.nostra13.universalimageloader.core.ImageLoader getImageLoader();

    void invalidateConfigCache();

    boolean isCached(APIRequest aPIRequest);

    void setAuthorization(String str);

    void updateCache(Map<String, Prefetch> map);
}
