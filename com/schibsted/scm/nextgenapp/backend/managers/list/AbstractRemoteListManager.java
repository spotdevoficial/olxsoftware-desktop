package com.schibsted.scm.nextgenapp.backend.managers.list;

import com.android.volley.VolleyError;
import com.schibsted.scm.nextgenapp.backend.managers.TrafficManager;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteListManager.OnAdListChangedListener;
import com.schibsted.scm.nextgenapp.backend.network.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.models.ListItem;
import com.schibsted.scm.nextgenapp.utils.SingleLock;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import com.urbanairship.C1608R;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRemoteListManager<ListModel, ResponseModel> implements RemoteListManager<ListModel> {
    private SingleLock mFetchLock;
    private VolleyError mLastError;
    protected List<ListModel> mList;
    protected OnAdListChangedListener mListListener;
    protected OnNetworkResponseListener<ResponseModel> mListener;
    private String mNextPageParameter;
    protected int mState;
    protected TrafficManager mTrafficManager;

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.list.AbstractRemoteListManager.1 */
    class C11881 extends OnNetworkResponseListener<ResponseModel> {
        C11881() {
        }

        public void onErrorResponse(VolleyError volleyError) {
            Logger.error("AbstractRemoteListManager", "Failed to request list data", volleyError);
            AbstractRemoteListManager.this.mLastError = volleyError;
            AbstractRemoteListManager.this.setState(0);
            AbstractRemoteListManager.this.mFetchLock.unlock();
        }

        public void onResponse(ResponseModel model) {
            if (AbstractRemoteListManager.this.mState == 3) {
                AbstractRemoteListManager.this.mList.clear();
            }
            if (model != null) {
                List<ListModel> list = AbstractRemoteListManager.this.extractItems(model);
                if (list != null) {
                    AbstractRemoteListManager.this.mList.addAll(list);
                }
                AbstractRemoteListManager.this.mNextPageParameter = AbstractRemoteListManager.this.extractNextPage(model);
            }
            AbstractRemoteListManager.this.mFetchLock.unlock();
            if (AbstractRemoteListManager.this.mNextPageParameter == null) {
                AbstractRemoteListManager.this.setState(4);
                AbstractRemoteListManager.this.onListLoadCompleted();
            } else {
                AbstractRemoteListManager.this.setState(2);
            }
            if (AbstractRemoteListManager.this.mListListener != null) {
                AbstractRemoteListManager.this.mListListener.onListUpdated();
            }
        }
    }

    protected abstract void cancelRequest(TrafficManager trafficManager);

    protected abstract List<ListModel> extractItems(ResponseModel responseModel);

    protected abstract String extractNextPage(ResponseModel responseModel);

    protected abstract void requestNextPage(TrafficManager trafficManager, String str, OnNetworkResponseListener<ResponseModel> onNetworkResponseListener);

    protected abstract boolean shouldTriggerPrefetchAtIndex(int i);

    protected AbstractRemoteListManager(TrafficManager trafficManager) {
        this.mState = 1;
        this.mTrafficManager = trafficManager;
        this.mList = new ArrayList();
        this.mFetchLock = new SingleLock();
        this.mListener = new C11881();
    }

    protected void onListLoadCompleted() {
    }

    protected void setState(int state) {
        int oldState = this.mState;
        this.mState = state;
        if (oldState != this.mState) {
            announceState();
        }
    }

    private void announceState() {
        if (this.mListListener != null) {
            switch (this.mState) {
                case C1608R.styleable.MapAttrs_mapType /*0*/:
                    this.mListListener.onNetworkError(this.mLastError);
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    this.mListListener.onListHalted();
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    this.mListListener.onListIsLoading();
                case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                    this.mListListener.onListIsRefreshing();
                case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                    if (this.mList.isEmpty()) {
                        this.mListListener.onListIsEmpty();
                    } else {
                        this.mListListener.onListIsComplete();
                    }
                default:
            }
        }
    }

    protected void fetchNextPage() {
        if (this.mFetchLock.lock()) {
            cancelRequest(this.mTrafficManager);
            requestNextPage(this.mTrafficManager, this.mNextPageParameter, this.mListener);
        }
    }

    public void clear() {
        this.mList.clear();
        this.mNextPageParameter = null;
        setState(2);
        if (this.mListListener != null) {
            this.mListListener.onListUpdated();
        }
        this.mFetchLock.unlock();
        fetchNextPage();
    }

    public void startLoading() {
        if (this.mState == 1 || this.mState == 0) {
            setState(2);
            fetchNextPage();
        }
    }

    public void refresh() {
        cancelRequest(this.mTrafficManager);
        this.mFetchLock.unlock();
        this.mNextPageParameter = null;
        setState(3);
        fetchNextPage();
    }

    public int getCount() {
        return this.mList.size();
    }

    public ListItem<ListModel> getIndex(int index, boolean triggerPrefetch) {
        if (this.mState == 2 && triggerPrefetch && shouldTriggerPrefetchAtIndex(index)) {
            fetchNextPage();
        }
        if (index < 0) {
            throw new IndexOutOfBoundsException("Tried to access a negative index");
        } else if (index < this.mList.size()) {
            ListItem<ListModel> item = new ListItem();
            item.setIndex(index);
            item.setModel(this.mList.get(index));
            return item;
        } else {
            throw new IndexOutOfBoundsException("Tried to access index " + index + " of " + this.mList.size() + " possible. The index is not loaded or does not exist");
        }
    }

    public List<ListModel> getList() {
        return this.mList;
    }

    public boolean isLoading() {
        return this.mState == 2 || this.mState == 3;
    }

    public void setListChangeListener(OnAdListChangedListener listener) {
        this.mListListener = listener;
        announceState();
    }
}
