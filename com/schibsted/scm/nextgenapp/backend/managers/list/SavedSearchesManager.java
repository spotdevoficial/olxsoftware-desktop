package com.schibsted.scm.nextgenapp.backend.managers.list;

import com.android.volley.VolleyError;
import com.facebook.appevents.AppEventsConstants;
import com.schibsted.scm.nextgenapp.backend.managers.TrafficManager;
import com.schibsted.scm.nextgenapp.backend.network.APIRequest.Builder;
import com.schibsted.scm.nextgenapp.backend.network.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.config.ApiEndpoint;
import com.schibsted.scm.nextgenapp.database.SavedSearchesList;
import com.schibsted.scm.nextgenapp.models.ListItem;
import com.schibsted.scm.nextgenapp.models.SearchResultApiModel;
import com.schibsted.scm.nextgenapp.models.internal.CountersModel;
import com.schibsted.scm.nextgenapp.utils.Utils;
import java.util.List;

public class SavedSearchesManager extends AbstractRemoteListManager<CountersModel, SearchResultApiModel> {
    private static final ApiEndpoint ENDPOINT;
    private int mCurrentSearchIndex;
    private SavedSearchesList mSavedSearchesList;

    static {
        ENDPOINT = ApiEndpoint.LIST_ADS;
    }

    public SavedSearchesManager(TrafficManager trafficManager, SavedSearchesList savedSearchesList) {
        super(trafficManager);
        this.mSavedSearchesList = savedSearchesList;
    }

    protected void requestNextPage(TrafficManager trafficManager, String nextPage, OnNetworkResponseListener<SearchResultApiModel> listener) {
        if (nextPage != null) {
            this.mCurrentSearchIndex = Integer.parseInt(nextPage);
        } else {
            this.mCurrentSearchIndex = getNextIndex();
        }
        if (this.mCurrentSearchIndex != -1) {
            trafficManager.doRequest(new Builder().requestId("SavedSearchesManager").endpoint(ENDPOINT).parameter("lim", AppEventsConstants.EVENT_PARAM_VALUE_NO).parameters(this.mSavedSearchesList.get(this.mCurrentSearchIndex).getQueryStringMap()).cancelSameRequests(false).listener(listener).build());
            return;
        }
        listener.onErrorResponse(new VolleyError("Error requesting next page for Saved Ads: undefined mCurrentSearchIndex"));
    }

    protected List<CountersModel> extractItems(SearchResultApiModel model) {
        int newAds = 0;
        int allAds = -1;
        if (model.counters.containsKey(SearchResultApiModel.NEW_COUNTER)) {
            newAds = ((Integer) model.counters.get(SearchResultApiModel.NEW_COUNTER)).intValue();
        }
        if (model.counters.containsKey(SearchResultApiModel.ALL_COUNTER)) {
            allAds = ((Integer) model.counters.get(SearchResultApiModel.ALL_COUNTER)).intValue();
        }
        CountersModel countersModel = new CountersModel();
        countersModel.setValues(allAds, newAds);
        this.mSavedSearchesList.get(this.mCurrentSearchIndex).setCounters(countersModel);
        return Utils.value(countersModel);
    }

    protected void cancelRequest(TrafficManager trafficManager) {
        trafficManager.cancelRequest(ENDPOINT, "SavedSearchesManager");
    }

    protected String extractNextPage(SearchResultApiModel searchResultApiModel) {
        int index = getNextIndex();
        if (index != -1) {
            return String.valueOf(index);
        }
        return null;
    }

    public ListItem<CountersModel> getIndex(int index, boolean triggerPrefetch) {
        if (this.mState == 2 && triggerPrefetch && shouldTriggerPrefetchAtIndex(index)) {
            fetchNextPage();
        }
        if (this.mSavedSearchesList == null || this.mSavedSearchesList.get(index) == null) {
            return null;
        }
        ListItem<CountersModel> listItem = new ListItem();
        listItem.setModel(this.mSavedSearchesList.get(index).getCounters());
        return listItem;
    }

    protected boolean shouldTriggerPrefetchAtIndex(int index) {
        if (this.mSavedSearchesList == null || this.mSavedSearchesList.get(index) == null || this.mSavedSearchesList.get(index).getCounters().isUpdated()) {
            return false;
        }
        return true;
    }

    protected int getNextIndex() {
        if (this.mSavedSearchesList == null) {
            return -1;
        }
        for (int i = 0; i < this.mSavedSearchesList.size(); i++) {
            if (!this.mSavedSearchesList.get(i).getCounters().isUpdated()) {
                return i;
            }
        }
        return -1;
    }
}
