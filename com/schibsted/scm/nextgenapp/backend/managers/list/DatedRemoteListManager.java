package com.schibsted.scm.nextgenapp.backend.managers.list;

import com.schibsted.scm.nextgenapp.backend.containers.SearchParametersContainer;
import com.schibsted.scm.nextgenapp.backend.managers.TrafficManager;
import com.schibsted.scm.nextgenapp.models.AdDetailsApiModel;
import com.schibsted.scm.nextgenapp.models.ListItem;

public class DatedRemoteListManager extends RemoteAdListManager {
    private long mLastViewedTime;

    public DatedRemoteListManager(TrafficManager trafficManager) {
        super(trafficManager);
    }

    public DatedRemoteListManager(TrafficManager trafficManager, int pageSize) {
        super(trafficManager, pageSize);
    }

    public ListItem<AdDetailsApiModel> getIndex(int index, boolean triggerPrefetch) {
        ListItem<AdDetailsApiModel> item = super.getIndex(index, triggerPrefetch);
        ((AdDetailsApiModel) item.getModel()).ad.isNew = this.mLastViewedTime < ((AdDetailsApiModel) item.getModel()).ad.listTime.value;
        return item;
    }

    public void setSearchParameters(SearchParametersContainer parameters) {
        super.setSearchParameters(parameters);
        this.mLastViewedTime = getSearchParameters().getLastViewedTime();
    }
}
