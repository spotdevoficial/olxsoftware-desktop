package com.schibsted.scm.nextgenapp.backend.managers.list;

import com.schibsted.scm.nextgenapp.backend.managers.AccountManager;
import com.schibsted.scm.nextgenapp.backend.managers.TrafficManager;
import com.schibsted.scm.nextgenapp.backend.network.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.models.AdDetailsApiModel;
import com.schibsted.scm.nextgenapp.models.SearchResultApiModel;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;
import com.schibsted.scm.nextgenapp.utils.Utils;
import java.util.ArrayList;
import java.util.List;

public class SavedAdsManager extends RemoteAdListManager {
    private final AccountManager mAccountManager;
    private final List<String> mIdList;

    public interface OperationListener {
        void onError();

        void onSuccess();
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.list.SavedAdsManager.1 */
    class C11891 implements Runnable {
        final /* synthetic */ AdDetailsApiModel val$adDetailModel;

        C11891(AdDetailsApiModel adDetailsApiModel) {
            this.val$adDetailModel = adDetailsApiModel;
        }

        public void run() {
            if (!SavedAdsManager.this.getIdList().contains(this.val$adDetailModel.ad.getCleanId())) {
                SavedAdsManager.this.getIdList().add(0, this.val$adDetailModel.ad.getCleanId());
                SavedAdsManager.this.getList().add(0, this.val$adDetailModel);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.list.SavedAdsManager.2 */
    class C11902 implements Runnable {
        final /* synthetic */ String val$adId;

        C11902(String str) {
            this.val$adId = str;
        }

        public void run() {
            try {
                String id = Utils.getAdCleanId(this.val$adId);
                SavedAdsManager.this.getList().remove(SavedAdsManager.this.getAdFromCleanId(id));
                SavedAdsManager.this.mIdList.remove(id);
            } catch (Exception e) {
                StringBuilder sb = new StringBuilder();
                sb.append(e.getMessage()).append(" ## ");
                sb.append("adID: ").append(this.val$adId).append(" ## ");
                sb.append(SavedAdsManager.this.getIdList().toArray().toString()).append(" ## ");
                sb.append(e.getStackTrace()).append(" ## ");
                CrashAnalytics.log(0, "SavedAdsManager", sb.toString());
            }
        }
    }

    public SavedAdsManager(TrafficManager trafficManager, AccountManager accountManager) {
        super(trafficManager);
        this.mIdList = new ArrayList();
        this.mAccountManager = accountManager;
        loadList();
    }

    public List<String> getIdList() {
        return this.mIdList;
    }

    public void add(AdDetailsApiModel adDetailModel, boolean fetchModels, OperationListener listener) {
        run(new C11891(adDetailModel), fetchModels, listener);
    }

    private AdDetailsApiModel getAdFromCleanId(String id) {
        for (AdDetailsApiModel adApiModel : getList()) {
            if (adApiModel.ad.getCleanId().equals(id)) {
                return adApiModel;
            }
        }
        return null;
    }

    public void refresh() {
        super.refresh();
        this.mListListener.onListUpdated();
    }

    public void remove(String adId, boolean fetchModels, OperationListener listener) {
        run(new C11902(adId), fetchModels, listener);
    }

    private void run(Runnable op, boolean fetchModels, OperationListener listener) {
        setState(2);
        boolean success = true;
        if (this.mAccountManager.isSignedIn()) {
            List<String> idList = getIdList();
            if (idList == null) {
                loadList();
            }
            op.run();
            if (idList != null) {
                saveList();
            }
        } else {
            success = false;
        }
        if (success) {
            if (fetchModels) {
                refresh();
            } else {
                setState(4);
            }
            if (listener != null) {
                listener.onSuccess();
            }
            if (this.mListListener != null) {
                this.mListListener.onListUpdated();
                return;
            }
            return;
        }
        setState(0);
        if (listener != null) {
            listener.onError();
        }
    }

    public boolean isSaved(String adId) {
        return getIdList() != null && getIdList().contains(adId);
    }

    public void loadList() {
        getIdList().clear();
        List<String> savedIdsToLoad = this.mAccountManager.getPreferences().loadSavedAds();
        if (savedIdsToLoad != null && !savedIdsToLoad.isEmpty()) {
            getIdList().addAll(savedIdsToLoad);
            fetchAds();
        }
    }

    public void requestNextPage(TrafficManager trafficManager, String nextPage, OnNetworkResponseListener<SearchResultApiModel> listener) {
        if (getIdList().isEmpty()) {
            listener.onResponse(null);
        } else {
            super.requestNextPage(trafficManager, nextPage, listener);
        }
    }

    private void fetchAds() {
        getSearchParameters().setAdListingId(getIdList());
        clear();
    }

    private void saveList() {
        this.mAccountManager.getPreferences().saveSavedAds(getIdList());
    }

    protected void onListLoadCompleted() {
        super.onListLoadCompleted();
        List<String> deletedIds = new ArrayList();
        if (this.mList.size() != this.mIdList.size()) {
            for (String id : this.mIdList) {
                boolean wasDeleted = true;
                for (AdDetailsApiModel adDetails : this.mList) {
                    if (adDetails.getAd().getCleanId().equals(id)) {
                        wasDeleted = false;
                        break;
                    }
                }
                if (wasDeleted) {
                    deletedIds.add(id);
                }
            }
            this.mIdList.removeAll(deletedIds);
        }
    }
}
