package com.schibsted.scm.nextgenapp.tracking.GoogleMobileAppsConvertionTracking;

import android.content.Context;
import com.google.ads.conversiontracking.AdWordsConversionReporter;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.tracking.EventLogger;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import java.util.HashMap;

public class GoogleMobileAppsTrackingAdapter extends EventLogger {
    private Context mContext;
    private HashMap<String, GoogleMobileAppsTrackerEvent> mEventKeys;

    public GoogleMobileAppsTrackingAdapter(Context context) {
        this.mContext = context;
        this.mEventKeys = new HashMap();
    }

    public void addEventToTrack(EventType eventType, GoogleMobileAppsTrackerEvent event) {
        this.mEventKeys.put(eventType.getKey(), event);
    }

    public void log(EventMessage message) {
        if (message != null && this.mEventKeys.containsKey(message.getEventType().getKey())) {
            GoogleMobileAppsTrackerEvent event = (GoogleMobileAppsTrackerEvent) this.mEventKeys.get(message.getEventType().getKey());
            AdWordsConversionReporter.reportWithConversionId(this.mContext, event.getConversationId(), event.getLabel(), event.getValue(), event.isRepeatable());
        }
    }
}
