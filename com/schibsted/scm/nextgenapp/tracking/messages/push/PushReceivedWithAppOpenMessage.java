package com.schibsted.scm.nextgenapp.tracking.messages.push;

public class PushReceivedWithAppOpenMessage {
    private final String mTreatmentId;

    public PushReceivedWithAppOpenMessage(String treatmentId) {
        this.mTreatmentId = treatmentId;
    }

    public String getTreatmentId() {
        return this.mTreatmentId;
    }
}
