package com.schibsted.scm.nextgenapp.tracking.messages.adedition;

public class AdEditionMessage {
    private String mListId;

    public AdEditionMessage(String listId) {
        this.mListId = listId;
    }

    public String getListId() {
        return this.mListId;
    }
}
