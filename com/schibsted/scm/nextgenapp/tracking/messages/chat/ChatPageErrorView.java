package com.schibsted.scm.nextgenapp.tracking.messages.chat;

public class ChatPageErrorView {
    private final String mChatId;
    private final String mListId;
    private final String mReason;

    public ChatPageErrorView(String listId, String chatId, String reason) {
        this.mListId = listId;
        this.mChatId = chatId;
        this.mReason = reason;
    }

    public String getReason() {
        return this.mReason;
    }

    public String getChatId() {
        return this.mChatId;
    }

    public String getListId() {
        return this.mListId;
    }
}
