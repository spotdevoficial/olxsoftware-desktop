package com.schibsted.scm.nextgenapp.tracking.messages.chat;

public class ChatReceiveMessageView {
    private final String mChatId;
    private final String mMessageId;

    public ChatReceiveMessageView(String chatId, String messageId) {
        this.mChatId = chatId;
        this.mMessageId = messageId;
    }

    public String getChatId() {
        return this.mChatId;
    }

    public String getMessageId() {
        return this.mMessageId;
    }
}
