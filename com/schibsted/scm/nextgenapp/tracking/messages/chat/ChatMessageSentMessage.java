package com.schibsted.scm.nextgenapp.tracking.messages.chat;

public class ChatMessageSentMessage {
    private final String mChatId;
    private final String mListId;
    private final String mMessageId;

    public ChatMessageSentMessage(String listId, String chatId, String messageId) {
        this.mListId = listId;
        this.mChatId = chatId;
        this.mMessageId = messageId;
    }

    public String getListId() {
        return this.mListId;
    }

    public String getChatId() {
        return this.mChatId;
    }

    public String getMessageId() {
        return this.mMessageId;
    }
}
