package com.schibsted.scm.nextgenapp.tracking.messages.chatlist;

public class ConfirmDeleteChatClick {
    private final String mChatId;
    private final String mListId;
    private final boolean mSelectedOption;

    public ConfirmDeleteChatClick(String listId, String chatId, boolean isSelected) {
        this.mChatId = chatId;
        this.mListId = listId;
        this.mSelectedOption = isSelected;
    }

    public String getChatId() {
        return this.mChatId;
    }

    public String getListId() {
        return this.mListId;
    }

    public boolean getSelectedOption() {
        return this.mSelectedOption;
    }
}
