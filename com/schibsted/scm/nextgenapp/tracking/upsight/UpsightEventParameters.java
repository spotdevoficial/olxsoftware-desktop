package com.schibsted.scm.nextgenapp.tracking.upsight;

import com.schibsted.scm.nextgenapp.utils.Utils;
import java.util.HashMap;
import java.util.Map;

public class UpsightEventParameters {
    private String mCategory;
    private String mEvent;
    private Map<String, Object> mExtraData;
    private String mLevel;
    private String mSubEvent;
    private String mValue;

    public enum Category {
        FUNNELS("Funnels"),
        SEARCH("Search"),
        FILTERS("Filters"),
        LOCATION("Location"),
        PAGE("Page"),
        LOGIN("Login"),
        DRAWER("Drawer"),
        GEOLOCATION("Geolocation"),
        NEAR_ME("NearMe"),
        CHAT("Chat"),
        DEEPLINK("DeepLink"),
        ADVERTISING("Advertising"),
        MONETIZATION("Monetization");
        
        private final String text;

        private Category(String text) {
            this.text = text;
        }

        public String toString() {
            return this.text;
        }
    }

    public enum Event {
        REPLY("Reply"),
        MESSAGE("Message"),
        PHONE_CONTACT("Phone contact"),
        POST("Post"),
        SORT("Sort"),
        FILTER("Filter"),
        LOCATION("Location"),
        LISTING("Listing"),
        MY_ACCOUNT("My Account"),
        ITEM("Item"),
        EDIT("Edit"),
        PUSH("Push"),
        LOGIN("Login"),
        REPORT("Report"),
        DELETE("Delete"),
        CREATE("Create"),
        ALL_CHATS("All_Chats"),
        CHAT("Chat"),
        NATIVE_AD("NativeAds"),
        MY_ADS("My Ads");
        
        private final String text;

        private Event(String text) {
            this.text = text;
        }

        public String toString() {
            return this.text;
        }
    }

    public enum SubEvent {
        BROWSE("Browse"),
        SEARCH("Search"),
        REAL_ESTATE("Real Estate"),
        VEHICLES("Vehicles"),
        DELIVERY_FAILED("Delivery_Failed"),
        GOOGLE_ADX("Google_Adx");
        
        private final String text;

        private SubEvent(String text) {
            this.text = text;
        }

        public String toString() {
            return this.text;
        }
    }

    public UpsightEventParameters() {
        this.mExtraData = new HashMap();
    }

    public String getCategory() {
        return this.mCategory;
    }

    public void setCategory(Category category) {
        this.mCategory = category.toString();
    }

    public String getEvent() {
        return this.mEvent;
    }

    public void setEvent(Event event) {
        this.mEvent = event.toString();
    }

    public String getSubEvent() {
        return this.mSubEvent;
    }

    public void setSubEvent(SubEvent subEvent) {
        this.mSubEvent = subEvent.toString();
    }

    public void setSubEvent(String subEvent) {
        this.mSubEvent = subEvent;
    }

    public String getLevel() {
        return this.mLevel;
    }

    public void setLevel(String level) {
        this.mLevel = level;
    }

    public String getValue() {
        return this.mValue;
    }

    public void setValue(String value) {
        this.mValue = value;
    }

    public Map<String, Object> getExtraData() {
        return this.mExtraData;
    }

    public void putExtraData(String key, Object value) {
        if (value instanceof String) {
            value = Utils.getAsciiString((String) value);
        }
        this.mExtraData.put(key, value);
    }
}
