package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import android.content.Context;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.backend.managers.AdvertisingIdManager;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters;

public class UniversalIdentifierUpsightUseCase implements UpsightUseCase {
    private Context mContext;

    public UniversalIdentifierUpsightUseCase(Context context) {
        this.mContext = context;
    }

    public UpsightEvent getUpsightEvent(EventMessage message) {
        if (message.getEventType().equals(EventType.ADVERTISING_ID_FETCHED)) {
            return getGaidEvent();
        }
        return null;
    }

    public UpsightEvent getGaidEvent() {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.putExtraData("GAID", AdvertisingIdManager.getAdvertisingId(this.mContext));
        return new UpsightEvent("GAID", parameters);
    }
}
