package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Category;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Event;
import com.urbanairship.C1608R;

public class ReplyByMessageUpsightUseCase implements UpsightUseCase {

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.upsight.usecase.ReplyByMessageUpsightUseCase.1 */
    static /* synthetic */ class C13881 {
        static final /* synthetic */ int[] $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType;

        static {
            $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType = new int[EventType.values().length];
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.SELECT_REPLY_EMAIL_SEARCH.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_SEND_EMAIL_VIEW.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.MESSAGE_CHANGE_NAME.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.MESSAGE_CHANGE_EMAIL.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.MESSAGE_CHANGE_PHONE.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.MESSAGE_CHANGE_MESSAGE.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.PAGE_AD_REPLY_SUCCESS.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.PAGE_AD_REPLY_ERROR.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_SUBMIT_EMAIL.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
        }
    }

    public UpsightEvent getUpsightEvent(EventMessage message) {
        switch (C13881.$SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[message.getEventType().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getTapSendMessageEvent(message);
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getChangeNameEvent(message, message.getMessageContainer().getName());
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return getChangeEmailEvent(message, message.getMessageContainer().getEmail());
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return getChangePhoneEvent(message, message.getMessageContainer().getPhone());
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return getChangeMessageEvent(message);
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return getSuccessfulReplyEvent(message);
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return getValidationNotOkEvent(message);
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return getSubmitFormEvent(message);
            default:
                return null;
        }
    }

    private UpsightEvent getTapSendMessageEvent(EventMessage message) {
        return new UpsightEvent("Tap_Send_Message", getCommonParameters(message));
    }

    private UpsightEvent getChangeNameEvent(EventMessage message, String name) {
        UpsightEventParameters parameters = getCommonParameters(message);
        parameters.putExtraData("Name", name);
        return new UpsightEvent("Change_Name", parameters);
    }

    private UpsightEvent getChangeEmailEvent(EventMessage message, String email) {
        UpsightEventParameters parameters = getCommonParameters(message);
        parameters.putExtraData("Email", email);
        return new UpsightEvent("Change_Email", parameters);
    }

    private UpsightEvent getChangePhoneEvent(EventMessage message, String phone) {
        UpsightEventParameters parameters = getCommonParameters(message);
        parameters.putExtraData("Phone", phone);
        return new UpsightEvent("Change_Phone", parameters);
    }

    private UpsightEvent getChangeMessageEvent(EventMessage message) {
        return new UpsightEvent("Change_Message", getCommonParameters(message));
    }

    private UpsightEvent getSubmitFormEvent(EventMessage message) {
        return new UpsightEvent("Submit_Form", getCommonParameters(message));
    }

    private UpsightEvent getValidationNotOkEvent(EventMessage message) {
        return new UpsightEvent("Validation_NotOK", getCommonParameters(message));
    }

    private UpsightEvent getSuccessfulReplyEvent(EventMessage message) {
        return new UpsightEvent("Successful_Reply", getCommonParameters(message));
    }

    private UpsightEventParameters getCommonParameters(EventMessage message) {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.setCategory(Category.FUNNELS);
        parameters.setEvent(Event.MESSAGE);
        parameters.setValue(message.getAd().getCleanId());
        return parameters;
    }
}
