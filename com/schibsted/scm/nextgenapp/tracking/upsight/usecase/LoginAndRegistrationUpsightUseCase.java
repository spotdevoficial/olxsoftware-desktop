package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Category;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Event;
import com.urbanairship.C1608R;

public class LoginAndRegistrationUpsightUseCase implements UpsightUseCase {

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.upsight.usecase.LoginAndRegistrationUpsightUseCase.1 */
    static /* synthetic */ class C13831 {
        static final /* synthetic */ int[] $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType;

        static {
            $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType = new int[EventType.values().length];
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CREDENTIALS_CLICK_FACEBOOK.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CREDENTIALS_CLICK_LOGIN.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CREDENTIALS_CLICK_LOGOUT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CREDENTIALS_CLICK_REGISTER.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CREDENTIALS_LOGIN_SUCCESS.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CREDENTIALS_REGISTER_SUCCESS.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CREDENTIALS_CHANGE_EMAIL.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CREDENTIALS_CHANGE_PASSWORD.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CREDENTIALS_FACEBOOK_AUTH_SUCCESS.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CREDENTIALS_FACEBOOK_AUTH_FAILURE.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CREDENTIALS_FACEBOOK_LOGIN_SUCCESS.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CREDENTIALS_FACEBOOK_ACCOUNT_CREATION_SUCCESS.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CREDENTIALS_VIEW_LOGIN_PAGE.ordinal()] = 13;
            } catch (NoSuchFieldError e13) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CREDENTIALS_VIEW_REGISTER_PAGE.ordinal()] = 14;
            } catch (NoSuchFieldError e14) {
            }
        }
    }

    public UpsightEvent getUpsightEvent(EventMessage message) {
        switch (C13831.$SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[message.getEventType().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getUpsightEventWithoutExtraDataByName("Tap_Facebook");
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getUpsightEventWithoutExtraDataByName("Tap_Login");
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getUpsightEventWithoutExtraDataByName("Tap_Logout");
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return getUpsightEventWithoutExtraDataByName("Tap_Register");
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return getUpsightEventWithoutExtraDataByName("Successful_Login");
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return getUpsightEventWithoutExtraDataByName("Successful_Register");
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return getChangeEmailUpsightEvent(message);
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return getUpsightEventWithoutExtraDataByName("Change_Password");
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return getUpsightEventWithDataAboutToken("Successful_FB_Auth", message.isFacebookAuthCodeNullOrEmpty());
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return getUpsightEventWithoutExtraDataByName("Unsuccessful_FB_Auth");
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                return getUpsightEventWithEventParameterValue("Successful_FB", Event.LOGIN);
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                return getUpsightEventWithEventParameterValue("Successful_FB", Event.CREATE);
            case C1608R.styleable.MapAttrs_useViewLifecycle /*13*/:
                return getUpsightEventWithoutExtraDataByName("View_Login");
            case C1608R.styleable.MapAttrs_zOrderOnTop /*14*/:
                return getUpsightEventWithoutExtraDataByName("View_Register");
            default:
                return null;
        }
    }

    private UpsightEvent getUpsightEventWithoutExtraDataByName(String name) {
        return new UpsightEvent(name, getCommonParameters());
    }

    private UpsightEvent getChangeEmailUpsightEvent(EventMessage message) {
        UpsightEventParameters parameters = getCommonParameters();
        parameters.putExtraData("Email", message.getEmail());
        return new UpsightEvent("Change_Email", parameters);
    }

    private UpsightEventParameters getCommonParameters() {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.setCategory(Category.LOGIN);
        return parameters;
    }

    private UpsightEvent getUpsightEventWithDataAboutToken(String name, boolean isFacebookTokenEmpty) {
        UpsightEventParameters parameters = getCommonParameters();
        parameters.putExtraData("facebookTokenEmpty", String.valueOf(isFacebookTokenEmpty));
        return new UpsightEvent(name, parameters);
    }

    private UpsightEvent getUpsightEventWithEventParameterValue(String name, Event event) {
        UpsightEventParameters parameters = getCommonParameters();
        parameters.setEvent(event);
        return new UpsightEvent(name, parameters);
    }
}
