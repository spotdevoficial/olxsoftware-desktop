package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.backend.containers.SearchParametersContainer;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Category;
import com.urbanairship.C1608R;

public class SearchUpsightUseCase implements UpsightUseCase {

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.upsight.usecase.SearchUpsightUseCase.1 */
    static /* synthetic */ class C13911 {
        static final /* synthetic */ int[] $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType;

        static {
            $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType = new int[EventType.values().length];
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.SEARCH_WITH_NO_CATEGORY.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.SEARCH_WITH_CATEGORY.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.SEARCH_WITH_SUBCATEGORY.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.SEARCH_RESULT_NUMBER.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    public UpsightEvent getUpsightEvent(EventMessage message) {
        switch (C13911.$SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[message.getEventType().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getSearchWithNoCategoryEvent(message);
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getSearchWithCategoryEvent(message);
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getSearchWithSubCategoryEvent(message);
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return getSearchResultNumberEvent(message);
            default:
                return null;
        }
    }

    private UpsightEvent getSearchWithNoCategoryEvent(EventMessage message) {
        return new UpsightEvent("Searched_CategoryAll", getCommonParameters(message));
    }

    private UpsightEvent getSearchWithCategoryEvent(EventMessage message) {
        UpsightEventParameters parameters = getCommonParameters(message);
        parameters.setValue(message.getSearchParametersContainer().getCategory().getCode());
        return new UpsightEvent("Searched_Category", parameters);
    }

    private UpsightEvent getSearchWithSubCategoryEvent(EventMessage message) {
        UpsightEventParameters parameters = getCommonParameters(message);
        parameters.setValue(message.getSearchParametersContainer().getCategory().getCode());
        return new UpsightEvent("Searched_Subcategory", parameters);
    }

    private UpsightEvent getSearchResultNumberEvent(EventMessage message) {
        UpsightEventParameters parameters = getCommonParameters(message);
        SearchParametersContainer searchParams = message.getSearchParametersContainer();
        if (searchParams.getCounters() == null) {
            return null;
        }
        parameters.setValue(String.valueOf(searchParams.getCounters().getTotalAds()));
        return new UpsightEvent("Searched_ResultNumber", parameters);
    }

    private UpsightEventParameters getCommonParameters(EventMessage message) {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.setCategory(Category.SEARCH);
        String searchTerms = message.getSearchParametersContainer().getTextSearch();
        if (searchTerms != null) {
            parameters.putExtraData("Search-terms", searchTerms);
        }
        return parameters;
    }
}
