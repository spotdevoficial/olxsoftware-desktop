package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Category;
import com.urbanairship.C1608R;

public class LocationUpsightUseCase implements UpsightUseCase {

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.upsight.usecase.LocationUpsightUseCase.1 */
    static /* synthetic */ class C13821 {
        static final /* synthetic */ int[] $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType;

        static {
            $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType = new int[EventType.values().length];
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.LOCATION_CHANGED_BRAZIL.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.LOCATION_CHANGED_STATE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.LOCATION_CHANGED_REGION.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    public UpsightEvent getUpsightEvent(EventMessage message) {
        switch (C13821.$SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[message.getEventType().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getLocationChangedBrazilEvent();
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getLocationChangedStateEvent(message.getRegionNode().code);
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getLocationChangedRegionEvent(message.getRegionNode().getChildren()[0].code);
            default:
                return null;
        }
    }

    private UpsightEvent getLocationChangedBrazilEvent() {
        return new UpsightEvent("Country", getCommonParameters());
    }

    private UpsightEvent getLocationChangedStateEvent(String stateCode) {
        UpsightEventParameters parameters = getCommonParameters();
        parameters.setValue(stateCode);
        return new UpsightEvent("State", parameters);
    }

    private UpsightEvent getLocationChangedRegionEvent(String regionCode) {
        UpsightEventParameters parameters = getCommonParameters();
        parameters.setValue(regionCode);
        return new UpsightEvent("Region", parameters);
    }

    private UpsightEventParameters getCommonParameters() {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.setCategory(Category.LOCATION);
        return parameters;
    }
}
