package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.C1061R;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.backend.containers.SearchParametersContainer;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Category;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Event;
import com.urbanairship.C1608R;

public class PageEventsUpsightUseCase implements UpsightUseCase {

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.upsight.usecase.PageEventsUpsightUseCase.1 */
    static /* synthetic */ class C13851 {
        static final /* synthetic */ int[] $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType;

        static {
            $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType = new int[EventType.values().length];
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_OPEN_ACCOUNT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_MY_ACCOUNTS_ADS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_MY_ACCOUNTS_SAVED_ADS.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_MY_ACCOUNTS_SAVED_SEARCHES.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_SAVE_SEARCH_UNSUCCESSFUL.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_AD_DETAIL_SHARE.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_AD_SAVE.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_AD_UNSAVE.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_REPORT_AD.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_AD_PHOTO.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_AD_MAP.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.PAGE_AD_PHOTO_SWIPE_RIGHT.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.PAGE_AD_PHOTO_SWIPE_LEFT.ordinal()] = 13;
            } catch (NoSuchFieldError e13) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.PAGE_AD_SWIPE_RIGHT.ordinal()] = 14;
            } catch (NoSuchFieldError e14) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.PAGE_AD_SWIPE_LEFT.ordinal()] = 15;
            } catch (NoSuchFieldError e15) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.MY_ADS_VIEW_MY_ADS.ordinal()] = 16;
            } catch (NoSuchFieldError e16) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_MY_AD_DELETE.ordinal()] = 17;
            } catch (NoSuchFieldError e17) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_MY_AD_EDIT.ordinal()] = 18;
            } catch (NoSuchFieldError e18) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHATLIST_CONTENT_REFRESH.ordinal()] = 19;
            } catch (NoSuchFieldError e19) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHATLIST_TAP_OPEN_CHAT.ordinal()] = 20;
            } catch (NoSuchFieldError e20) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_TAP_REPORT_USER.ordinal()] = 21;
            } catch (NoSuchFieldError e21) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_TAP_RETRY_SEND_MESSAGE.ordinal()] = 22;
            } catch (NoSuchFieldError e22) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_TAP_HEADER.ordinal()] = 23;
            } catch (NoSuchFieldError e23) {
            }
        }
    }

    public UpsightEvent getUpsightEvent(EventMessage message) {
        switch (C13851.$SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[message.getEventType().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getTapOpenAccountEvent();
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getTapMyAdsEvent();
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getTapMySavedAdsEvent();
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return getTapMySavedSearchesEvent();
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return getSaveSearchEvent(message);
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return getShareAdEvent(message);
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return getBookmarkAdEvent(message);
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return getUnBookmarkAdEvent(message);
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return getReportAdEvent(message);
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return getZoomInPhotoEvent(message);
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                return getTapMapEvent(message);
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                return getNextPhotoSwipeEvent(message);
            case C1608R.styleable.MapAttrs_useViewLifecycle /*13*/:
                return getPreviousPhotoSwipeEvent(message);
            case C1608R.styleable.MapAttrs_zOrderOnTop /*14*/:
                return getNextItemSwipeEvent(message);
            case C1608R.styleable.MapAttrs_uiMapToolbar /*15*/:
                return getPreviousItemSwipeEvent(message);
            case C1061R.styleable.Toolbar_maxButtonHeight /*16*/:
                return getMyAdsPageViewEvent();
            case C1061R.styleable.Toolbar_collapseIcon /*17*/:
                return getDeleteAdEvent(message);
            case C1061R.styleable.Toolbar_collapseContentDescription /*18*/:
                return getEditAdEvent(message);
            case C1061R.styleable.Toolbar_navigationIcon /*19*/:
                return getChatListContentRefreshEvent();
            case C1061R.styleable.Toolbar_navigationContentDescription /*20*/:
                return getChatListTapOpenChatEvent(message);
            case C1061R.styleable.Theme_actionBarTheme /*21*/:
                return getChatReportUser(message);
            case C1061R.styleable.Theme_actionBarWidgetTheme /*22*/:
                return getChatRetrySendMessage(message);
            case C1061R.styleable.Theme_actionBarSize /*23*/:
                return getChatTapHeader(message);
            default:
                return null;
        }
    }

    private UpsightEvent getChatTapHeader(EventMessage message) {
        UpsightEventParameters params = getCommonParameters(Event.CHAT);
        params.putExtraData("Chat-id", message.getChatId());
        return new UpsightEvent("Tap_Header", params);
    }

    private UpsightEvent getChatRetrySendMessage(EventMessage message) {
        UpsightEventParameters params = getCommonParameters(Event.CHAT);
        params.putExtraData("Chat-id", message.getChatId());
        return new UpsightEvent("Tap_Retry_Send_Message", params);
    }

    private UpsightEvent getChatReportUser(EventMessage message) {
        UpsightEventParameters params = getCommonParameters(Event.CHAT);
        params.putExtraData("Chat-id", message.getChatId());
        return new UpsightEvent("Tap_Report_User", params);
    }

    private UpsightEvent getChatListTapOpenChatEvent(EventMessage message) {
        UpsightEventParameters params = getCommonParameters(Event.ALL_CHATS);
        params.putExtraData("Chat-id", message.getChatId());
        return new UpsightEvent("Tap_Open_Chat", params);
    }

    private UpsightEvent getChatListContentRefreshEvent() {
        return new UpsightEvent("Content_Refresh", getCommonParameters(Event.ALL_CHATS));
    }

    private UpsightEvent getTapOpenAccountEvent() {
        return new UpsightEvent("Tap_My_Account", getCommonParameters(Event.LISTING));
    }

    private UpsightEvent getTapMyAdsEvent() {
        return new UpsightEvent("Tap_My_Ads", getCommonParameters(Event.MY_ACCOUNT));
    }

    private UpsightEvent getTapMySavedAdsEvent() {
        return new UpsightEvent("Tap_Saved_Ads", getCommonParameters(Event.MY_ACCOUNT));
    }

    private UpsightEvent getTapMySavedSearchesEvent() {
        return new UpsightEvent("Tap_Saved_Searches", getCommonParameters(Event.MY_ACCOUNT));
    }

    private UpsightEvent getDeleteAdEvent(EventMessage message) {
        return getEventWithListIdExtra("Tap_Delete_Ad", Event.MY_ACCOUNT, message);
    }

    private UpsightEvent getEditAdEvent(EventMessage message) {
        return getEventWithListIdExtra("Tap_Edit_Ad", Event.MY_ACCOUNT, message);
    }

    private UpsightEvent getShareAdEvent(EventMessage message) {
        return getEventWithListIdExtra("Tap_Share", Event.ITEM, message);
    }

    private UpsightEvent getBookmarkAdEvent(EventMessage message) {
        return getEventWithListIdExtra("Tap_Bookmark", Event.ITEM, message);
    }

    private UpsightEvent getUnBookmarkAdEvent(EventMessage message) {
        return getEventWithListIdExtra("Tap_Remove_Bookmark", Event.ITEM, message);
    }

    private UpsightEvent getReportAdEvent(EventMessage message) {
        return getEventWithListIdExtra("Tap_Report_Add", Event.ITEM, message);
    }

    private UpsightEvent getZoomInPhotoEvent(EventMessage message) {
        return getEventWithListIdExtra("Zoom_In_Photo", Event.ITEM, message);
    }

    private UpsightEvent getTapMapEvent(EventMessage message) {
        return getEventWithListIdExtra("Tap_Map", Event.ITEM, message);
    }

    private UpsightEvent getNextPhotoSwipeEvent(EventMessage message) {
        return getEventWithListIdExtra("Swipe_Next_Photo", Event.ITEM, message);
    }

    private UpsightEvent getPreviousPhotoSwipeEvent(EventMessage message) {
        return getEventWithListIdExtra("Swipe_Previous_Photo", Event.ITEM, message);
    }

    private UpsightEvent getNextItemSwipeEvent(EventMessage message) {
        return getEventWithListIdExtra("Swipe_Next_Item", Event.ITEM, message);
    }

    private UpsightEvent getPreviousItemSwipeEvent(EventMessage message) {
        return getEventWithListIdExtra("Swipe_Previous_Item", Event.ITEM, message);
    }

    private UpsightEvent getMyAdsPageViewEvent() {
        return new UpsightEvent("View_My_Ads", getCommonParameters(Event.MY_ADS));
    }

    private UpsightEvent getSaveSearchEvent(EventMessage message) {
        UpsightEventParameters params = getCommonParameters(Event.LISTING);
        SearchParametersContainer searchParametersContainer = message.getSearchParametersContainer();
        String searchTerms = BuildConfig.VERSION_NAME;
        if (searchParametersContainer != null) {
            searchTerms = searchParametersContainer.getTextSearch();
        }
        params.putExtraData("Search-terms", searchTerms);
        return new UpsightEvent("Save_Search", params);
    }

    private UpsightEvent getEventWithListIdExtra(String name, Event type, EventMessage message) {
        UpsightEventParameters params = getCommonParameters(type);
        params.setValue(message.getAd().getCleanId());
        return new UpsightEvent(name, params);
    }

    private UpsightEventParameters getCommonParameters(Event event) {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.setCategory(Category.PAGE);
        parameters.setEvent(event);
        return parameters;
    }
}
