package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode;
import com.schibsted.scm.nextgenapp.models.internal.CountersModel;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Category;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Event;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.SubEvent;
import com.urbanairship.C1608R;

public class BrowseToReplyUpsightUseCase implements UpsightUseCase {

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.upsight.usecase.BrowseToReplyUpsightUseCase.1 */
    static /* synthetic */ class C13741 {
        static final /* synthetic */ int[] $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType;

        static {
            $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType = new int[EventType.values().length];
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CATEGORY_SELECTED_ALL.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CATEGORY_SELECTED_CATEGORY.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CATEGORY_SELECTED_SUBCATEGORY.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.SELECT_AD_BROWSE.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_SMS_MOBILE.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_CALL_MOBILE.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_SEND_EMAIL_VIEW.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
        }
    }

    public UpsightEvent getUpsightEvent(EventMessage message) {
        switch (C13741.$SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[message.getEventType().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getChooseCategoryAllEvent(message);
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getChooseCategoryCategoryEvent(message);
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getChooseCategorySubcategoryEvent(message);
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return getSelectAdEvent(message);
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return getSelectReplyEvent(message);
            default:
                return null;
        }
    }

    private UpsightEvent getChooseCategoryAllEvent(EventMessage message) {
        UpsightEventParameters parameters = getCommonParameters();
        setNumberOfResultsLevelParameter(parameters, message);
        return new UpsightEvent("Choose_CategoryAll", parameters);
    }

    private UpsightEvent getChooseCategoryCategoryEvent(EventMessage message) {
        DbCategoryNode category = message.getCategory();
        UpsightEventParameters parameters = getCommonParameters();
        setNumberOfResultsLevelParameter(parameters, message);
        parameters.setValue(category.getCode());
        return new UpsightEvent("Choose_Category", parameters);
    }

    private UpsightEvent getChooseCategorySubcategoryEvent(EventMessage message) {
        DbCategoryNode subCategory = message.getCategory();
        UpsightEventParameters parameters = getCommonParameters();
        setNumberOfResultsLevelParameter(parameters, message);
        parameters.setValue(subCategory.getCode());
        return new UpsightEvent("Choose_Subcategory", parameters);
    }

    private UpsightEvent getSelectReplyEvent(EventMessage message) {
        Ad ad = message.getAd();
        if (ad == null) {
            return null;
        }
        UpsightEventParameters parameters = getCommonParameters();
        parameters.setValue(ad.getCleanId());
        return new UpsightEvent("Select_Reply", parameters);
    }

    private UpsightEvent getSelectAdEvent(EventMessage message) {
        Ad ad = message.getAd();
        if (ad == null) {
            return null;
        }
        UpsightEventParameters parameters = getCommonParameters();
        parameters.setValue(ad.getCleanId());
        parameters.setLevel(String.valueOf(message.getAdPosition()));
        return new UpsightEvent("Select_Ad", parameters);
    }

    private void setNumberOfResultsLevelParameter(UpsightEventParameters parameters, EventMessage message) {
        CountersModel counters = message.getSearchParametersContainer().getCounters();
        String numberOfResults = null;
        if (counters != null) {
            numberOfResults = String.valueOf(counters.getTotalAds());
        }
        parameters.setLevel(numberOfResults);
    }

    private UpsightEventParameters getCommonParameters() {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.setCategory(Category.FUNNELS);
        parameters.setEvent(Event.REPLY);
        parameters.setSubEvent(SubEvent.BROWSE);
        return parameters;
    }
}
