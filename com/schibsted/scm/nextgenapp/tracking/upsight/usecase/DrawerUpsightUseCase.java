package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.models.submodels.StaticPage;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Category;
import com.urbanairship.C1608R;

public class DrawerUpsightUseCase implements UpsightUseCase {

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.upsight.usecase.DrawerUpsightUseCase.1 */
    static /* synthetic */ class C13771 {
        static final /* synthetic */ int[] $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType;

        static {
            $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType = new int[EventType.values().length];
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.DRAWER_OPEN.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.DRAWER_CLOSE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_OPEN_CHAT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_USER_SECTION.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_OPEN_FACEBOOK.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_OPEN_GOOGLE_PLAY.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_OPEN_CUSTOMER_SERVICE.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.OPEN_DEVELOPER_TOOLS.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_OPEN_STATIC_PAGE.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
        }
    }

    public UpsightEvent getUpsightEvent(EventMessage message) {
        switch (C13771.$SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[message.getEventType().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getUpsightEventByName("Open");
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getUpsightEventByName("Close");
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getUpsightEventByName("Open_Chat");
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return getUpsightEventByName("Header");
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return getUpsightEventByName("Like_Facebook");
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return getUpsightEventByName("Review_Google_Play");
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return getUpsightEventByName("Contact_Us");
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return getUpsightEventByName("Open_Developer_Tools");
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return getUpsightEventByName(getUpsightEventName(message.getStaticPage()));
            default:
                return null;
        }
    }

    private String getUpsightEventName(StaticPage staticPage) {
        if (staticPage == null) {
            return null;
        }
        String str = staticPage.id;
        Object obj = -1;
        switch (str.hashCode()) {
            case -871999053:
                if (str.equals(StaticPage.ID_BUY_SAFELY)) {
                    obj = 2;
                    break;
                }
                break;
            case 101142:
                if (str.equals(StaticPage.ID_FAQ)) {
                    obj = null;
                    break;
                }
                break;
            case 110250375:
                if (str.equals(StaticPage.ID_TOS)) {
                    obj = 1;
                    break;
                }
                break;
        }
        switch (obj) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "FAQ";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Terms_Conditions";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Security_Tips";
            default:
                return null;
        }
    }

    private UpsightEvent getUpsightEventByName(String name) {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.setCategory(Category.DRAWER);
        return new UpsightEvent(name, parameters);
    }
}
