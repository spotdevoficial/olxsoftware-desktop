package com.schibsted.scm.nextgenapp.tracking.upsight;

import android.content.Context;
import com.kontagent.Kontagent;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.tracking.EventLogger;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.AppsFlyerIdentifierUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.BrowseToReplyUpsightUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.ChatUpsightUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.DeepLinkUpsightUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.DrawerUpsightUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.EditAnAdUpsightUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.FiltersUpsightUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.GeolocationUpsightUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.GoogleNativeAdUpsightUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.LocationUpsightUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.LoginAndRegistrationUpsightUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.MonetizationUpsightUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.NearMeUpsightUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.PageEventsUpsightUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.PostAnAdUpsightUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.PushUpsightUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.ReplyByMessageUpsightUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.ReplyByPhoneContactUpsightUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.SearchToReplyUpsightUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.SearchUpsightUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.UniversalIdentifierUpsightUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.UpsightUseCase;
import com.schibsted.scm.nextgenapp.tracking.upsight.usecase.UrbanAirshipDeviceIdUpsightUseCase;
import java.util.ArrayList;
import java.util.List;

public class UpsightEventLoggerAdapter extends EventLogger {
    private boolean mDeviceInformationSent;
    private List<UpsightUseCase> mUpsightUseCases;

    public UpsightEventLoggerAdapter(String apiKey, Context context) {
        this.mDeviceInformationSent = false;
        init(apiKey, context);
        setUseCases(context);
    }

    private void init(String apiKey, Context context) {
        if (!ConfigContainer.getConfig().isReleaseBuildType().booleanValue()) {
            Kontagent.enableDebug();
        }
        Kontagent.startSession(apiKey, context, ConfigContainer.getConfig().isReleaseBuildType().booleanValue() ? "production" : "test");
        Kontagent.stopHeartbeatTimer();
    }

    private void setUseCases(Context context) {
        this.mUpsightUseCases = new ArrayList();
        this.mUpsightUseCases.add(new BrowseToReplyUpsightUseCase());
        this.mUpsightUseCases.add(new SearchToReplyUpsightUseCase());
        this.mUpsightUseCases.add(new EditAnAdUpsightUseCase());
        this.mUpsightUseCases.add(new FiltersUpsightUseCase());
        this.mUpsightUseCases.add(new LocationUpsightUseCase());
        this.mUpsightUseCases.add(new LoginAndRegistrationUpsightUseCase());
        this.mUpsightUseCases.add(new PageEventsUpsightUseCase());
        this.mUpsightUseCases.add(new PostAnAdUpsightUseCase());
        this.mUpsightUseCases.add(new ReplyByMessageUpsightUseCase());
        this.mUpsightUseCases.add(new ReplyByPhoneContactUpsightUseCase());
        this.mUpsightUseCases.add(new SearchUpsightUseCase());
        this.mUpsightUseCases.add(new UniversalIdentifierUpsightUseCase(context));
        this.mUpsightUseCases.add(new AppsFlyerIdentifierUseCase(context));
        this.mUpsightUseCases.add(new PushUpsightUseCase());
        this.mUpsightUseCases.add(new UrbanAirshipDeviceIdUpsightUseCase());
        this.mUpsightUseCases.add(new DrawerUpsightUseCase());
        this.mUpsightUseCases.add(new GeolocationUpsightUseCase());
        this.mUpsightUseCases.add(new NearMeUpsightUseCase());
        this.mUpsightUseCases.add(new ChatUpsightUseCase());
        this.mUpsightUseCases.add(new DeepLinkUpsightUseCase());
        this.mUpsightUseCases.add(new GoogleNativeAdUpsightUseCase());
        this.mUpsightUseCases.add(new MonetizationUpsightUseCase());
    }

    public void log(EventMessage message) {
        for (UpsightUseCase useCase : this.mUpsightUseCases) {
            UpsightEvent event = useCase.getUpsightEvent(message);
            if (event != null) {
                sendDeviceInformationIfNeeded();
                Kontagent.customEvent(event.getName(), event.getParametersMap());
            }
        }
    }

    private void sendDeviceInformationIfNeeded() {
        if (!this.mDeviceInformationSent) {
            Kontagent.sendDeviceInformation(null);
            this.mDeviceInformationSent = true;
        }
    }

    public void onTerminate() {
        Kontagent.stopSession();
    }
}
