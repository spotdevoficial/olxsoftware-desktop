package com.schibsted.scm.nextgenapp.tracking.facebook;

import android.app.Activity;
import android.os.Bundle;
import com.facebook.AccessToken;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode;
import com.schibsted.scm.nextgenapp.models.requests.InsertAdRequest;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.tracking.EventLogger;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.urbanairship.C1608R;

public class FacebookEventLoggerAdapter extends EventLogger {
    private AppEventsLogger mAppEventsLogger;
    private final String mAppId;

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.facebook.FacebookEventLoggerAdapter.1 */
    static /* synthetic */ class C13731 {
        static final /* synthetic */ int[] $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType;

        static {
            $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType = new int[EventType.values().length];
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.PAGE_INSERT_AD_SUBMIT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.PAGE_AD_REPLY_SUBMIT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_AD_REPLY.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_CALL_MOBILE.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_SMS_MOBILE.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_CREATE_SUCCESSFUL.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.PAGE_AD_VIEW.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_AD_SHARE.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_AD_SAVE.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.PAGE_INSERT_AD_SHOW.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.AD_INSERTION_CHOOSE_CATEGORY.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.AD_INSERTION_CHOOSE_SUBCATEGORY.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
        }
    }

    public FacebookEventLoggerAdapter(String appId) {
        this.mAppId = appId;
    }

    private void createLogger(Activity activity) {
        if (this.mAppEventsLogger == null) {
            this.mAppEventsLogger = AppEventsLogger.newLogger(activity, this.mAppId, AccessToken.getCurrentAccessToken());
        }
    }

    public void onCreate(Activity activity, Bundle savedInstanceState) {
        createLogger(activity);
    }

    public void onResume(Activity activity) {
        AppEventsLogger.activateApp(activity, this.mAppId);
        createLogger(activity);
    }

    public void onPause(Activity activity) {
        AppEventsLogger.deactivateApp(activity, this.mAppId);
    }

    public void log(EventMessage message) {
        Bundle parameters = new Bundle();
        switch (C13731.$SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[message.getEventType().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                InsertAdRequest insertAd = message.getInsertAdRequest();
                if (insertAd != null) {
                    parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, insertAd.ad.category.label);
                }
                this.mAppEventsLogger.logEvent("CompleteRegistration", parameters);
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                fillParametersWithAdInfo(parameters, message.getAd());
                this.mAppEventsLogger.logEvent("Purchase", parameters);
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                fillParametersWithPurchaseIntentInfo(parameters, message.getListId());
                this.mAppEventsLogger.logEvent(AppEventsConstants.EVENT_NAME_PURCHASED, 1.0d, parameters);
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                fillParametersWithAdInfo(parameters, message.getAd());
                this.mAppEventsLogger.logEvent("Purchase", parameters);
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                fillParametersWithAdInfo(parameters, message.getAd());
                this.mAppEventsLogger.logEvent("Purchase", parameters);
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                fillParametersWithPurchaseIntentInfo(parameters, message.getAd());
                this.mAppEventsLogger.logEvent(AppEventsConstants.EVENT_NAME_ADDED_TO_CART, 1.0d, parameters);
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                fillParametersWithPurchaseIntentInfo(parameters, message.getAd());
                this.mAppEventsLogger.logEvent(AppEventsConstants.EVENT_NAME_VIEWED_CONTENT, 1.0d, parameters);
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                this.mAppEventsLogger.logEvent("ad_share");
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                this.mAppEventsLogger.logEvent("save_ad");
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                this.mAppEventsLogger.logEvent("AddPaymentInfo");
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                fillParametersWithCategoryInfo(parameters, message.getCategory());
                this.mAppEventsLogger.logEvent("AddPaymentInfo_Category", parameters);
            default:
        }
    }

    private void fillParametersWithCategoryInfo(Bundle parameters, DbCategoryNode category) {
        if (category != null) {
            parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, category.getCode());
        }
    }

    private void fillParametersWithAdInfo(Bundle parameters, Ad ad) {
        if (ad != null) {
            parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, ad.category.label);
            parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, ad.getCleanId());
        }
    }

    private void fillParametersWithPurchaseIntentInfo(Bundle parameters, Ad ad) {
        if (ad != null) {
            parameters.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, "BRL");
            parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "product");
            parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, ad.getCleanId());
            parameters.putString("categoria", ad.category.label);
            parameters.putString("regiao", ad.getRegion().getLabel());
        }
    }

    private void fillParametersWithPurchaseIntentInfo(Bundle parameters, String listId) {
        if (listId != null) {
            parameters.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, "BRL");
            parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "product");
            parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, listId);
        }
    }
}
