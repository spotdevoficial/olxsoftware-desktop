package com.schibsted.scm.nextgenapp.tracking.appsflyer.model;

import android.content.Context;
import android.text.TextUtils;
import com.appsflyer.AppsFlyerLib;
import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.managers.PreferencesManager;
import com.schibsted.scm.nextgenapp.tracking.EventType;

public class AppsFlyerIdModel {
    private String mAppsFlyerId;
    private MessageBus mBus;
    private Context mContext;

    public AppsFlyerIdModel(Context context, MessageBus bus) {
        this.mContext = context;
        this.mBus = bus;
    }

    public static String getAppsFlyerID(Context context) {
        return new PreferencesManager(context).getAppsFlyerId();
    }

    public void tryToSendTagEventWithId() {
        this.mAppsFlyerId = AppsFlyerLib.getInstance().getAppsFlyerUID(this.mContext);
        if (!TextUtils.isEmpty(this.mAppsFlyerId)) {
            saveId();
            postEvent();
        }
    }

    private void saveId() {
        new PreferencesManager(this.mContext).saveAppsFlyerId(this.mAppsFlyerId);
    }

    private void postEvent() {
        this.mBus.post(new EventBuilder().setEventType(EventType.APPSFLYER_ID_FETCHED).build());
    }
}
