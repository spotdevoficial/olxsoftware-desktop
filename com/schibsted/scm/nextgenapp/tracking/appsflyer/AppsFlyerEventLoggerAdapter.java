package com.schibsted.scm.nextgenapp.tracking.appsflyer;

import android.app.Application;
import android.content.Context;
import com.appsflyer.AppsFlyerLib;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.tracking.EventLogger;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.urbanairship.C1608R;
import java.util.HashSet;

public class AppsFlyerEventLoggerAdapter extends EventLogger {
    private Context mContext;
    private HashSet<EventType> mEventsToTrack;
    private boolean mTrackEvents;

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.appsflyer.AppsFlyerEventLoggerAdapter.1 */
    static /* synthetic */ class C13711 {
        static final /* synthetic */ int[] $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType;

        static {
            $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType = new int[EventType.values().length];
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.PAGE_AD_REPLY_SUBMIT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_AD_REPLY.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    public AppsFlyerEventLoggerAdapter(Application application, String devKey, boolean trackEvents) {
        AppsFlyerLib.getInstance().startTracking(application, devKey);
        this.mContext = application.getApplicationContext();
        this.mTrackEvents = trackEvents;
        this.mEventsToTrack = new HashSet();
    }

    public void log(EventMessage message) {
        if (this.mContext != null && message != null && this.mTrackEvents && this.mEventsToTrack.contains(message.getEventType())) {
            AppsFlyerLib.getInstance().trackEvent(this.mContext, bindEventTypeToEventName(message.getEventType()), null);
        }
    }

    private String bindEventTypeToEventName(EventType eventType) {
        switch (C13711.$SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[eventType.ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "ad_reply_mail_submit";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "ad_reply_chat_submit";
            default:
                return eventType.getKey();
        }
    }

    public void addEventToTrack(EventType eventType) {
        this.mEventsToTrack.add(eventType);
    }
}
