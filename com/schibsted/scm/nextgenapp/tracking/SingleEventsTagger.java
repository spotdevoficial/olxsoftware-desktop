package com.schibsted.scm.nextgenapp.tracking;

import android.content.Context;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.managers.AdvertisingIdManager;
import com.schibsted.scm.nextgenapp.tracking.appsflyer.model.AppsFlyerIdModel;
import com.urbanairship.UAirship;

public class SingleEventsTagger {
    private static boolean singleEventsSent;

    static {
        singleEventsSent = false;
    }

    public static void tagSingleStartupEvents(Context context) {
        if (!singleEventsSent) {
            tagGoogleAdvertisingId(context);
            tagAppsflyerId(context);
            tagUrbanAirshipChannelId();
            singleEventsSent = true;
        }
    }

    private static void tagGoogleAdvertisingId(Context context) {
        AdvertisingIdManager.fetchAdvertisingId(context);
    }

    private static void tagAppsflyerId(Context context) {
        new AppsFlyerIdModel(context, C1049M.getMessageBus()).tryToSendTagEventWithId();
    }

    private static void tagUrbanAirshipChannelId() {
        String channelId = UAirship.shared().getPushManager().getChannelId();
        if (channelId != null && !channelId.isEmpty()) {
            C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.PUSH_CHANNEL_ID).setUAirshipChannelId(channelId).build());
        }
    }
}
