package com.schibsted.scm.nextgenapp.tracking.fabric;

import android.content.Context;
import com.crashlytics.android.answers.Answers;

public class AnswersAnalytics extends FabricAnalytics {
    public static void init(Context context) {
        FabricAnalytics.init(context, new Answers());
    }

    public static void send(TrackableEvent event) {
        Answers.getInstance().logCustom(event.getData());
    }
}
