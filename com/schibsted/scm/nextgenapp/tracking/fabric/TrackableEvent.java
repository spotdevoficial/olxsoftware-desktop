package com.schibsted.scm.nextgenapp.tracking.fabric;

import com.crashlytics.android.answers.CustomEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class TrackableEvent {
    private CustomEvent mCustomEvent;

    public static class TrackableEventBuilder {
        private String mEventName;
        private Map<String, Number> mNumberAttributes;
        private Map<String, String> mStringAttributes;

        public TrackableEventBuilder(String eventName) {
            this.mEventName = eventName;
            this.mStringAttributes = new HashMap();
            this.mNumberAttributes = new HashMap();
        }

        public TrackableEventBuilder withCustomAttribute(String key, String value) {
            this.mStringAttributes.put(key, value);
            return this;
        }

        public TrackableEvent build() {
            if (this.mEventName == null) {
                throw new IllegalStateException("You must provide an event name.");
            }
            CustomEvent customEvent = new CustomEvent(this.mEventName);
            putStringAttributesInEvent(customEvent);
            putNumberAttributesInEvent(customEvent);
            return new TrackableEvent(null);
        }

        private void putStringAttributesInEvent(CustomEvent customEvent) {
            for (Entry<String, String> entry : this.mStringAttributes.entrySet()) {
                customEvent.putCustomAttribute((String) entry.getKey(), (String) entry.getValue());
            }
        }

        private void putNumberAttributesInEvent(CustomEvent customEvent) {
            for (Entry<String, Number> entry : this.mNumberAttributes.entrySet()) {
                customEvent.putCustomAttribute((String) entry.getKey(), (Number) entry.getValue());
            }
        }
    }

    private TrackableEvent(CustomEvent customEvent) {
        this.mCustomEvent = customEvent;
    }

    public CustomEvent getData() {
        return this.mCustomEvent;
    }
}
