package com.schibsted.scm.nextgenapp.tracking.lurker.metrics;

import com.schibsted.scm.android.lurker.Lurker;
import com.schibsted.scm.android.lurker.model.LurkerEvent;
import com.schibsted.scm.android.lurker.model.identifier.EventIdentifier;
import com.schibsted.scm.android.lurker.model.identifier.SystemIdentifier;
import com.schibsted.scm.nextgenapp.tracking.lurker.UserIdentifierFactory;
import com.schibsted.scm.nextgenapp.tracking.messages.adedition.AdEditionMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.adedition.AdEditionPageView;
import com.squareup.otto.Subscribe;

public class AdEditionMetrics extends Metrics {
    public AdEditionMetrics(UserIdentifierFactory userIdentifierFactory, SystemIdentifier systemIdentifier) {
        super(userIdentifierFactory, systemIdentifier);
    }

    @Subscribe
    public void onAdEditionPageViewed(AdEditionPageView adEditionPageView) {
        LurkerEvent event = event(new EventIdentifier("view", "ai", "edit"));
        event.put("list_id", adEditionPageView.getListId());
        Lurker.event(event);
    }

    @Subscribe
    public void onAdEdited(AdEditionMessage message) {
        LurkerEvent adEditionEvent = event(new EventIdentifier("view", "ai_successful", "edit"));
        adEditionEvent.put("list_id", message.getListId());
        Lurker.event(adEditionEvent);
    }
}
