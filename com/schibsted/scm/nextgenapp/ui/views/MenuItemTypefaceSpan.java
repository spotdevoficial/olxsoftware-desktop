package com.schibsted.scm.nextgenapp.ui.views;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.TypefaceSpan;
import android.view.MenuItem;
import com.facebook.BuildConfig;

public class MenuItemTypefaceSpan extends TypefaceSpan {
    private final Typeface newType;

    private MenuItemTypefaceSpan(Typeface type) {
        super(BuildConfig.VERSION_NAME);
        this.newType = type;
    }

    public void updateDrawState(TextPaint ds) {
        applyCustomTypeFace(ds, this.newType);
    }

    public void updateMeasureState(TextPaint paint) {
        applyCustomTypeFace(paint, this.newType);
    }

    private static void applyCustomTypeFace(Paint paint, Typeface tf) {
        int oldStyle;
        Typeface oldTypeface = paint.getTypeface();
        if (oldTypeface == null) {
            oldStyle = 0;
        } else {
            oldStyle = oldTypeface.getStyle();
        }
        int fake = oldStyle & (tf.getStyle() ^ -1);
        if ((fake & 1) != 0) {
            paint.setFakeBoldText(true);
        }
        if ((fake & 2) != 0) {
            paint.setTextSkewX(-0.25f);
        }
        paint.setTypeface(tf);
    }

    public static void apply(Typeface typeface, MenuItem mi) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new MenuItemTypefaceSpan(typeface), 0, mNewTitle.length(), 18);
        mi.setTitle(mNewTitle);
    }
}
