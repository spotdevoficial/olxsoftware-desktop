package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import uk.co.senab.photoview.PhotoViewAttacher;

public class ImageViewZoomable extends ImageView {
    private String TAG;
    private PhotoViewAttacher mAttacher;

    public ImageViewZoomable(Context context) {
        super(context, null);
        this.TAG = ImageViewZoomable.class.getSimpleName();
    }

    public ImageViewZoomable(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
        this.TAG = ImageViewZoomable.class.getSimpleName();
    }

    public ImageViewZoomable(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.TAG = ImageViewZoomable.class.getSimpleName();
    }

    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        if (this.mAttacher != null) {
            this.mAttacher.update();
        }
    }

    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
        if (this.mAttacher != null) {
            this.mAttacher.update();
        }
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mAttacher.cleanup();
        this.mAttacher = null;
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mAttacher = new PhotoViewAttacher(this);
    }
}
