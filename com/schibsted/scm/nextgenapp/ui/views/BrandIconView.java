package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.util.AttributeSet;
import com.android.volley.toolbox.NetworkImageView;
import com.schibsted.scm.nextgenapp.C1061R;

public class BrandIconView extends NetworkImageView {
    private static final ColorFilter defaultColorFilter;
    private ColorFilter mColorFilter;

    static {
        defaultColorFilter = new PorterDuffColorFilter(-1, Mode.MULTIPLY);
    }

    public BrandIconView(Context context) {
        this(context, null);
    }

    public BrandIconView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BrandIconView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        loadStateFromAttrs(context.getResources(), attrs);
    }

    protected void drawableStateChanged() {
        super.drawableStateChanged();
        invalidate();
    }

    public void setHighLightColor(int color) {
        this.mColorFilter = new PorterDuffColorFilter(color, Mode.SRC_ATOP);
    }

    private void loadStateFromAttrs(Resources res, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray ta = null;
            try {
                ta = getContext().obtainStyledAttributes(attrs, C1061R.styleable.BrandIcon);
                this.mColorFilter = new PorterDuffColorFilter(ta.getColor(0, -1), Mode.SRC_ATOP);
                if (ta != null) {
                    ta.recycle();
                }
            } catch (Throwable th) {
                if (ta != null) {
                    ta.recycle();
                }
            }
        }
    }
}
