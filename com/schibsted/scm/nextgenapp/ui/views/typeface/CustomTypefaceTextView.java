package com.schibsted.scm.nextgenapp.ui.views.typeface;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTypefaceTextView extends TextView {
    public CustomTypefaceTextView(Context context) {
        this(context, null);
    }

    public CustomTypefaceTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomTypefaceTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (!isInEditMode()) {
            CustomTypefaceHelper.setCustomFont(this, attrs);
        }
    }

    public void setTypeface(int typefaceAsset) {
        setTypeface(getContext().getString(typefaceAsset));
    }

    public void setTypeface(String typefaceAsset) {
        CustomTypefaceHelper.setTypeface(this, typefaceAsset);
    }
}
