package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import com.schibsted.scm.nextgenapp.models.AdDetailsApiModel;
import com.schibsted.scm.nextgenapp.models.ListItem;

public abstract class DynamicAdView extends FrameLayout {
    private static String TAG;
    protected ListItem<AdDetailsApiModel> mItem;

    public abstract void bindData(ListItem<AdDetailsApiModel> listItem);

    protected abstract View setupView(LayoutInflater layoutInflater);

    static {
        TAG = "DynamicAdView";
    }

    public DynamicAdView(Context context) {
        super(context);
        init();
    }

    public DynamicAdView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ListItem<AdDetailsApiModel> getItem() {
        return this.mItem;
    }

    public void populate(ListItem<AdDetailsApiModel> item, boolean existingAd) {
        this.mItem = item;
        boolean z = (this.mItem == null || this.mItem.getModel() == null || !existingAd) ? false : true;
        setEnabled(z);
        if (this.mItem != null && this.mItem.getModel() != null) {
            bindData(item);
        }
    }

    private void init() {
        addView(setupView((LayoutInflater) getContext().getSystemService("layout_inflater")));
    }
}
