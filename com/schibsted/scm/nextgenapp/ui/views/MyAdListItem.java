package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.appcompat.C0086R;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.pnikosis.materialishprogress.ProgressWheel;

public class MyAdListItem extends FrameLayout implements OnClickListener {
    private StatefulImageView mAdPictureView;
    private LinearLayout mContainerView;
    private TextView mDateView;
    private ImageButton mImageButtonOverflow;
    private ProgressWheel mPictureProgressBar;
    private PopupMenu mPopupMenu;
    private TextView mPriceDateDividerView;
    private TextView mPriceView;
    private Button mProductOfferButton;
    private TextView mProductOfferTextView;
    private ViewGroup mProductOfferViewGroup;
    private TextView mRepliesCountLabel;
    private TextView mRepliesCountView;
    private FrameLayout mTextShade;
    private TextView mTitleView;
    private TextView mViewCountView;
    private TextView mViewsCountLabel;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.MyAdListItem.1 */
    class C15221 implements ImageLoadingListener {
        C15221() {
        }

        public void onLoadingStarted(String imageUri, View view) {
            MyAdListItem.this.performViewLoading();
        }

        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            MyAdListItem.this.performViewNoImage();
        }

        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            MyAdListItem.this.performViewImage();
        }

        public void onLoadingCancelled(String imageUri, View view) {
        }
    }

    public MyAdListItem(Context context) {
        super(context);
        init();
    }

    public MyAdListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        View view = LayoutInflater.from(getContext()).inflate(2130903186, null);
        this.mTitleView = (TextView) view.findViewById(C0086R.id.title);
        this.mDateView = (TextView) view.findViewById(2131558847);
        this.mPriceView = (TextView) view.findViewById(2131558744);
        this.mPriceDateDividerView = (TextView) view.findViewById(2131558848);
        this.mAdPictureView = (StatefulImageView) view.findViewById(2131558849);
        this.mViewCountView = (TextView) view.findViewById(2131558566);
        this.mRepliesCountView = (TextView) view.findViewById(2131558568);
        this.mContainerView = (LinearLayout) view.findViewById(2131558652);
        this.mPictureProgressBar = (ProgressWheel) view.findViewById(2131558576);
        this.mTextShade = (FrameLayout) view.findViewById(2131558850);
        this.mViewsCountLabel = (TextView) view.findViewById(2131558851);
        this.mRepliesCountLabel = (TextView) view.findViewById(2131558852);
        this.mImageButtonOverflow = (ImageButton) view.findViewById(2131558846);
        this.mProductOfferViewGroup = (ViewGroup) view.findViewById(2131558853);
        this.mProductOfferTextView = (TextView) view.findViewById(2131558854);
        this.mProductOfferButton = (Button) view.findViewById(2131558855);
        this.mImageButtonOverflow.setOnClickListener(this);
        addView(view);
    }

    private void performViewNoImage() {
        this.mPictureProgressBar.setVisibility(4);
        this.mAdPictureView.setScaleType(ScaleType.CENTER);
        this.mTextShade.setVisibility(4);
        this.mAdPictureView.setImageDrawable(getContext().getResources().getDrawable(2130837650));
        setCountsColor(getContext().getResources().getColor(2131493084));
    }

    private void performViewLoading() {
        this.mPictureProgressBar.setVisibility(0);
        this.mAdPictureView.setScaleType(ScaleType.CENTER);
        this.mTextShade.setVisibility(4);
        this.mAdPictureView.setImageDrawable(null);
        this.mPictureProgressBar.setVisibility(0);
        setCountsColor(getContext().getResources().getColor(2131493084));
    }

    private void performViewImage() {
        this.mPictureProgressBar.setVisibility(4);
        this.mAdPictureView.setScaleType(ScaleType.CENTER_CROP);
        this.mAdPictureView.setVisibility(0);
        this.mTextShade.setVisibility(0);
        setCountsColor(getContext().getResources().getColor(2131493094));
    }

    private void setCountsColor(int color) {
        this.mViewsCountLabel.setTextColor(color);
        this.mRepliesCountLabel.setTextColor(color);
        this.mRepliesCountView.setTextColor(color);
        this.mViewCountView.setTextColor(color);
    }

    public PopupMenu getPopupMenu() {
        if (this.mPopupMenu == null) {
            this.mPopupMenu = new PopupMenu(new ContextThemeWrapper(getContext(), 2131296521), this.mImageButtonOverflow);
            this.mPopupMenu.getMenuInflater().inflate(2131623946, this.mPopupMenu.getMenu());
        }
        return this.mPopupMenu;
    }

    public void onClick(View v) {
        if (this.mPopupMenu != null) {
            this.mPopupMenu.show();
        }
    }

    public void setTitleText(String title) {
        this.mTitleView.setText(title);
    }

    public void setDateText(String date) {
        this.mDateView.setText(date);
    }

    public void setPriceText(String price) {
        this.mPriceView.setText(price);
    }

    public void setPriceDateDividerVisibility(int visibility) {
        this.mPriceDateDividerView.setVisibility(visibility);
    }

    public void setAdPictureImageUrl(String imageUrl) {
        if (TextUtils.isEmpty(imageUrl)) {
            performViewNoImage();
        } else {
            this.mAdPictureView.setImageUrl(imageUrl, new C15221());
        }
    }

    public void setViewsCount(Integer views) {
        this.mViewCountView.setText(String.valueOf(views));
    }

    public void setRepliesCount(Integer replies) {
        this.mRepliesCountView.setText(String.valueOf(replies));
    }

    public void setContainerOnClickListener(OnClickListener onClickListener) {
        this.mContainerView.setOnClickListener(onClickListener);
    }

    public void showProductOfferLayout() {
        setProductOfferViewGroupVisibility(0);
    }

    public void hideProductOfferLayout() {
        setProductOfferViewGroupVisibility(8);
    }

    private void setProductOfferViewGroupVisibility(int visibility) {
        this.mProductOfferViewGroup.setVisibility(visibility);
    }

    public void setProductOfferText(String text) {
        this.mProductOfferTextView.setText(text);
    }

    public void setProductOfferButtonText(String text) {
        this.mProductOfferButton.setText(text);
    }

    public void setProductOfferButtonOnClickListener(OnClickListener listener) {
        this.mProductOfferButton.setOnClickListener(listener);
    }
}
