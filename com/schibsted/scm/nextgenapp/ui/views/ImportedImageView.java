package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.support.v7.appcompat.C0086R;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.models.submodels.MediaData;
import com.urbanairship.C1608R;

public class ImportedImageView extends FrameLayout {
    private TextView mAddLabel;
    private FrameLayout mContainer;
    private Animation mHide;
    private Bitmap mImage;
    private RoundedImageView mImageView;
    private ImageView mPlaceholderView;
    private ProgressWheel mProgressBar;
    private ProgressImageView mProgressView;
    private Animation mShow;
    private int mState;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.ImportedImageView.1 */
    class C15121 implements AnimationListener {
        C15121() {
        }

        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
            ImportedImageView.this.mImageView.setVisibility(4);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.ImportedImageView.2 */
    class C15132 implements AnimationListener {
        C15132() {
        }

        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
            ImportedImageView.this.mPlaceholderView.setVisibility(4);
            ImportedImageView.this.mAddLabel.setVisibility(4);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.ImportedImageView.3 */
    class C15143 implements ImageLoadingListener {
        C15143() {
        }

        public void onLoadingStarted(String imageUri, View view) {
        }

        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            if (ImportedImageView.this.mProgressBar != null) {
                ImportedImageView.this.mProgressBar.setVisibility(8);
            }
            ImportedImageView.this.setImage((Bitmap) null);
        }

        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (ImportedImageView.this.mProgressBar != null) {
                ImportedImageView.this.mProgressBar.setVisibility(8);
            }
            ImportedImageView.this.setImage(loadedImage);
        }

        public void onLoadingCancelled(String imageUri, View view) {
            if (ImportedImageView.this.mProgressBar != null) {
                ImportedImageView.this.mProgressBar.setVisibility(8);
            }
        }
    }

    public ImportedImageView(Context context) {
        super(context);
        init(context);
    }

    public ImportedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        this.mContainer = (FrameLayout) ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(2130903173, this);
        this.mPlaceholderView = (ImageView) this.mContainer.findViewById(2131558823);
        this.mImageView = (RoundedImageView) this.mContainer.findViewById(C0086R.id.image);
        this.mProgressView = (ProgressImageView) this.mContainer.findViewById(2131558820);
        this.mProgressBar = (ProgressWheel) this.mContainer.findViewById(2131558824);
        this.mAddLabel = (TextView) this.mContainer.findViewById(2131558825);
        this.mImage = null;
        this.mHide = AnimationUtils.loadAnimation(context, 2130968593);
        this.mShow = AnimationUtils.loadAnimation(context, 2130968592);
        this.mHide.setAnimationListener(new C15121());
        this.mShow.setAnimationListener(new C15132());
        setState(0);
    }

    public int getProgressState() {
        return this.mProgressView.getState();
    }

    public void setProgressState(int state, float progress) {
        if (state == 1 || state == 2 || state == 4) {
            this.mProgressView.setProgress(progress);
        }
        this.mProgressView.setState(state);
    }

    public void setState(int state) {
        this.mState = state;
        switch (state) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                this.mPlaceholderView.setVisibility(0);
                this.mAddLabel.setVisibility(0);
                if (null != null) {
                    this.mHide.cancel();
                    this.mHide.reset();
                    this.mImageView.startAnimation(this.mHide);
                    return;
                }
                this.mImageView.setVisibility(4);
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                if (null != null) {
                    this.mShow.cancel();
                    this.mShow.reset();
                    this.mImageView.startAnimation(this.mShow);
                    return;
                }
                this.mPlaceholderView.setVisibility(4);
                this.mAddLabel.setVisibility(4);
                this.mImageView.setVisibility(0);
                this.mProgressBar.setVisibility(4);
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                if (null != null) {
                    this.mShow.cancel();
                    this.mShow.reset();
                    this.mImageView.startAnimation(this.mShow);
                    return;
                }
                this.mPlaceholderView.setVisibility(4);
                this.mAddLabel.setVisibility(4);
                this.mImageView.setVisibility(0);
                this.mProgressBar.setVisibility(4);
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                if (null != null) {
                    this.mShow.cancel();
                    this.mShow.reset();
                    this.mImageView.startAnimation(this.mShow);
                    return;
                }
                this.mPlaceholderView.setVisibility(0);
                this.mAddLabel.setVisibility(4);
                this.mImageView.setVisibility(0);
                this.mProgressBar.setVisibility(0);
            default:
        }
    }

    public int getState() {
        return this.mState;
    }

    public void setImage(Bitmap image) {
        this.mImage = image;
        this.mImageView.setImageBitmap(image);
        setState(image == null ? 2 : 1);
    }

    public void setImage(String uri) {
        DisplayImageOptions options = new Builder().bitmapConfig(Config.RGB_565).imageScaleType(ImageScaleType.IN_SAMPLE_INT).displayer(new FadeInBitmapDisplayer(200)).considerExifParams(true).cacheOnDisc(true).cacheInMemory(true).build();
        this.mProgressBar.setVisibility(0);
        C1049M.getTrafficManager().getImageLoader().displayImage(uri, this.mImageView, options, new C15143());
    }

    public RoundedImageView getView() {
        return this.mImageView;
    }

    public void setImage(MediaData image) {
    }

    public Bitmap getImage() {
        return this.mImage;
    }

    public View getProgressBar() {
        return this.mProgressBar;
    }
}
