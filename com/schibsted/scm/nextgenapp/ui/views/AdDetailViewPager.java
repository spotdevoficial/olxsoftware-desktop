package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.urbanairship.C1608R;

public class AdDetailViewPager extends CustomViewPager {
    private int dragDirection;
    private float mStartDragPosX;

    public AdDetailViewPager(Context context) {
        super(context);
        this.dragDirection = 2;
    }

    public AdDetailViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.dragDirection = 2;
    }

    protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
        if (v == this || !(v instanceof ViewPager)) {
            return super.canScroll(v, checkV, dx, x, y);
        }
        ViewPager childViewPager = (ViewPager) v;
        if (this.dragDirection == 0 && childViewPager.getCurrentItem() == 0) {
            return false;
        }
        if (this.dragDirection == 1 && childViewPager.getCurrentItem() == childViewPager.getAdapter().getCount() - 1) {
            return false;
        }
        return true;
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        float x = ev.getX();
        switch (ev.getAction()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                this.dragDirection = 2;
                this.mStartDragPosX = x;
                break;
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                this.dragDirection = 2;
                break;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                if (this.mStartDragPosX >= x) {
                    if (this.mStartDragPosX > x) {
                        this.dragDirection = 1;
                        break;
                    }
                }
                this.dragDirection = 0;
                break;
                break;
        }
        return super.onInterceptTouchEvent(ev);
    }
}
