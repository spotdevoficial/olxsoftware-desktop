package com.schibsted.scm.nextgenapp.ui.views;

import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.TypefaceSpan;
import android.view.MenuItem;
import com.facebook.BuildConfig;

public class MenuItemTextSizeSpan extends TypefaceSpan {
    private final float mTextSize;

    private MenuItemTextSizeSpan(float textSize) {
        super(BuildConfig.VERSION_NAME);
        this.mTextSize = textSize;
    }

    public void updateDrawState(TextPaint ds) {
        ds.setTextSize(this.mTextSize);
    }

    public void updateMeasureState(TextPaint paint) {
        paint.setTextSize(this.mTextSize);
    }

    public static void apply(float textSize, MenuItem mi) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new MenuItemTextSizeSpan(textSize), 0, mNewTitle.length(), 18);
        mi.setTitle(mNewTitle);
    }
}
