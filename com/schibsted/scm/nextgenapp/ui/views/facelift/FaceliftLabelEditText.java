package com.schibsted.scm.nextgenapp.ui.views.facelift;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.LinearLayout;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.C1061R;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import com.schibsted.scm.nextgenapp.ui.views.typeface.CustomTypefaceEditText;
import com.schibsted.scm.nextgenapp.ui.views.typeface.CustomTypefaceTextView;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;

public class FaceliftLabelEditText extends LinearLayout {
    private static final String TAG;
    private Button mAction;
    private CustomTypefaceEditText mEditText;
    private CustomTypefaceTextView mErrorText;
    private View mErrorView;
    private View mInputView;
    private CustomTypefaceTextView mLabelView;
    private View mRootView;
    private TextWatcher onTextChanged;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.facelift.FaceliftLabelEditText.1 */
    class C15281 implements TextWatcher {
        C15281() {
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public void afterTextChanged(Editable editable) {
            FaceliftLabelEditText.this.setEnableLabel(editable.length() != 0);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.facelift.FaceliftLabelEditText.2 */
    class C15292 extends AnimatorListenerAdapter {
        final /* synthetic */ boolean val$show;

        C15292(boolean z) {
            this.val$show = z;
        }

        public void onAnimationStart(Animator animation) {
            super.onAnimationStart(animation);
            FaceliftLabelEditText.this.mLabelView.setVisibility(0);
        }

        public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            FaceliftLabelEditText.this.mLabelView.setVisibility(this.val$show ? 0 : 8);
        }
    }

    static {
        TAG = FaceliftLabelEditText.class.getSimpleName();
    }

    public FaceliftLabelEditText(Context context) {
        this(context, null);
    }

    public FaceliftLabelEditText(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FaceliftLabelEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.onTextChanged = new C15281();
        init(context);
        initAttributes(context, attrs);
    }

    public void setOnFocusChangeListener(OnFocusChangeListener onFocusChangeListener) {
        this.mEditText.setOnFocusChangeListener(onFocusChangeListener);
    }

    public boolean hasFocus() {
        return this.mEditText.hasFocus();
    }

    public Editable getText() {
        return this.mEditText.getText();
    }

    public void setLabel(String label) {
        this.mLabelView.setText(label);
        this.mEditText.setHint(label);
    }

    public void showError(int errorMessage) {
        this.mErrorView.setVisibility(0);
        this.mErrorText.setText(errorMessage);
    }

    public void showError(String errorMessage) {
        this.mErrorView.setVisibility(0);
        this.mErrorText.setText(errorMessage);
    }

    public void hideError() {
        this.mErrorView.setVisibility(8);
        this.mErrorText.setText(BuildConfig.VERSION_NAME);
    }

    public void setText(String text) {
        this.mEditText.setText(text);
    }

    public void setSelection(int index) {
        this.mEditText.setSelection(index);
    }

    public OnFocusChangeListener getOnFocusChangeListener() {
        return super.getOnFocusChangeListener();
    }

    public void setOnLinkClickListener(OnClickListener listener) {
        this.mAction.setOnClickListener(listener);
    }

    private void initAttributes(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, C1061R.styleable.FaceliftLabelEditText, 0, 0);
        try {
            setLabel(a.getString(0));
            setBackground(a.getInt(1, -1));
            String action = a.getString(2);
            if (action != null) {
                this.mAction.setVisibility(0);
                this.mAction.setText(action);
            }
            initEditTextAttributes(attrs);
        } catch (Exception e) {
            Logger.error(TAG, "Erro gettin attributes", e);
            CrashAnalytics.logException(e);
        } finally {
            a.recycle();
        }
    }

    private void initEditTextAttributes(AttributeSet attrs) {
        if (!isInEditMode()) {
            int inputType = getIntValue(attrs, "inputType", 0);
            int minLines = getIntValue(attrs, "minLines", -1);
            int maxLines = getIntValue(attrs, "maxLines", -1);
            int imeOptions = getIntValue(attrs, "imeOptions", 0);
            if (inputType != 0) {
                this.mEditText.setInputType(inputType);
            }
            if (minLines != -1) {
                this.mEditText.setMinLines(minLines);
            }
            if (maxLines != -1) {
                this.mEditText.setMaxLines(maxLines);
            }
            if (imeOptions != 0) {
                this.mEditText.setImeOptions(imeOptions);
            }
        }
    }

    private int getIntValue(AttributeSet attrs, String name, int defaultValue) {
        String stringValue = attrs.getAttributeValue("http://schemas.android.com/apk/res/android", name);
        int intValue = defaultValue;
        if (stringValue != null) {
            return Integer.decode(stringValue).intValue();
        }
        return intValue;
    }

    private void setBackground(int position) {
        Resources res = getResources();
        if (position == 0) {
            this.mInputView.setBackgroundResource(2130837714);
        } else if (position == 1) {
            this.mInputView.setBackgroundResource(2130837716);
        } else if (position == 2) {
            this.mInputView.setBackgroundResource(2130837715);
            setRoundedErrorBackground();
        } else {
            this.mInputView.setBackgroundResource(2130837717);
            setRoundedErrorBackground();
        }
        int horPadding = res.getDimensionPixelSize(2131230913);
        int verPadding = res.getDimensionPixelSize(2131230914);
        this.mInputView.setPadding(horPadding, verPadding, horPadding, verPadding);
    }

    private void setRoundedErrorBackground() {
        Resources res = getResources();
        this.mErrorView.setBackgroundResource(2130837629);
        this.mErrorView.setPadding(0, res.getDimensionPixelSize(2131230910), 0, res.getDimensionPixelSize(2131230909));
    }

    private void init(Context context) {
        this.mRootView = inflate(context, 2130903099, this);
        this.mInputView = this.mRootView.findViewById(2131558629);
        this.mLabelView = (CustomTypefaceTextView) this.mRootView.findViewById(2131558630);
        this.mEditText = (CustomTypefaceEditText) this.mRootView.findViewById(2131558631);
        this.mErrorText = (CustomTypefaceTextView) this.mRootView.findViewById(2131558634);
        this.mErrorView = this.mRootView.findViewById(2131558633);
        this.mAction = (Button) this.mRootView.findViewById(2131558632);
        this.mAction.setVisibility(8);
        this.mErrorView.setVisibility(8);
        this.mLabelView.setVisibility(8);
        this.mEditText.addTextChangedListener(this.onTextChanged);
        this.mEditText.setFocusableInTouchMode(true);
        this.mEditText.setFocusable(true);
        this.mEditText.setEnabled(true);
    }

    public boolean onTouchEvent(MotionEvent event) {
        return this.mEditText.onTouchEvent(event);
    }

    public boolean performClick() {
        return this.mEditText.performClick();
    }

    private void setEnableLabel(boolean show) {
        AnimatorSet animation = null;
        ObjectAnimator move;
        ObjectAnimator fade;
        if (this.mLabelView.getVisibility() == 0 && !show) {
            animation = new AnimatorSet();
            move = ObjectAnimator.ofFloat(this.mLabelView, "translationY", new float[]{0.0f, (float) (this.mLabelView.getHeight() / 8)});
            fade = ObjectAnimator.ofFloat(this.mLabelView, "alpha", new float[]{MediaUploadState.IMAGE_PROGRESS_UPLOADED, 0.0f});
            animation.playTogether(new Animator[]{move, fade});
        } else if (this.mLabelView.getVisibility() != 0 && show) {
            animation = new AnimatorSet();
            move = ObjectAnimator.ofFloat(this.mLabelView, "translationY", new float[]{(float) (this.mLabelView.getHeight() / 8), 0.0f});
            fade = ObjectAnimator.ofFloat(this.mLabelView, "alpha", new float[]{0.0f, MediaUploadState.IMAGE_PROGRESS_UPLOADED});
            animation.playTogether(new Animator[]{move, fade});
        }
        if (animation != null) {
            animation.addListener(new C15292(show));
            animation.start();
        }
    }
}
