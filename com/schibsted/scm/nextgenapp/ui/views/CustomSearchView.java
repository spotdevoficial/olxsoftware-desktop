package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.util.AttributeSet;
import com.schibsted.scm.nextgenapp.adapters.SearchHistoryAdapter;
import com.schibsted.scm.nextgenapp.database.SearchHistoryDatabase;

public class CustomSearchView extends SearchView {
    private SearchHistoryAdapter mAdapter;
    private Cursor mCursor;
    private SearchHistoryDatabase mDatabase;
    private OnSubmitListener mListener;

    public interface OnSubmitListener {
        void onSearchError(String str);

        void onSearchSubmit(String str);
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.CustomSearchView.1 */
    class C15041 implements OnQueryTextListener {
        C15041() {
        }

        public boolean onQueryTextSubmit(String query) {
            if (CustomSearchView.this.mListener != null) {
                CustomSearchView.this.mListener.onSearchSubmit(query);
            }
            return true;
        }

        public boolean onQueryTextChange(String newText) {
            return false;
        }
    }

    public CustomSearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomSearchView(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            this.mDatabase = new SearchHistoryDatabase(getContext());
        }
        setQueryHint(getResources().getString(2131165308));
        show();
        setIconifiedByDefault(false);
        setOnQueryTextListener(new C15041());
    }

    public void setSubmitListener(OnSubmitListener mListener) {
        this.mListener = mListener;
    }

    private void show() {
        this.mCursor = this.mDatabase.getRecentSearches();
        this.mAdapter = new SearchHistoryAdapter(getContext(), this.mCursor);
        setSuggestionsAdapter(this.mAdapter);
    }

    public void setText(String text) {
        setQuery(text, false);
    }

    public String getText() {
        return getQuery().toString();
    }

    public void submit(String s) {
        if (100 < s.length()) {
            this.mListener.onSearchError(getContext().getString(2131165586));
        } else {
            setQuery(s, true);
        }
    }

    public void setQuery(CharSequence query, boolean submit) {
        super.setQuery(query, submit);
        if (submit && this.mListener != null) {
            this.mListener.onSearchSubmit(query.toString());
        }
    }
}
