package com.schibsted.scm.nextgenapp.ui.views;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;
import de.keyboardsurfer.android.widget.crouton.Crouton;

public class FaceliftCrouton {
    public static void showAlertText(Activity activity, int message) {
        showAlertText(activity, activity.getString(message));
    }

    public static void showAlertText(Activity activity, String message) {
        showText(activity, message, 2131492945);
    }

    @SuppressLint({"InflateParams"})
    public static void showText(Activity activity, String message, int color) {
        View view = activity.getLayoutInflater().inflate(2130903103, null);
        TextView textView = (TextView) view.findViewById(2131558642);
        ((CardView) view.findViewById(2131558641)).setCardBackgroundColor(activity.getResources().getColor(color));
        textView.setText(message);
        Crouton.show(activity, view);
    }
}
