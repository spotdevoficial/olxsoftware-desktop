package com.schibsted.scm.nextgenapp.ui.views.parameters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.facebook.C0256R;
import com.schibsted.scm.nextgenapp.models.internal.MultiParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.models.internal.ValueListItem;
import com.schibsted.scm.nextgenapp.models.submodels.MultipleSelectionParameterDefinition;
import com.schibsted.scm.nextgenapp.ui.DialogCreator;
import com.schibsted.scm.nextgenapp.ui.DialogCreator.DialogButton;
import com.schibsted.scm.nextgenapp.ui.DialogCreator.DialogMultiChoiceList;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MultiListParameterView extends ParameterView {
    protected List<Integer> currentSelectedItems;
    protected boolean[] currentSelection;
    protected String[] labelList;
    private Button mButton;
    protected MultipleSelectionParameterDefinition multiDefinition;
    protected MultiParameterValue newValue;
    protected MultiParameterValue oldValue;
    protected List<Integer> previousSelectedItems;
    protected boolean[] previousSelection;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.parameters.MultiListParameterView.1 */
    class C15401 implements OnClickListener {
        C15401() {
        }

        public void onClick(View v) {
            MultiListParameterView.this.createDialog();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.parameters.MultiListParameterView.2 */
    class C15412 implements OnMultiChoiceClickListener {
        C15412() {
        }

        public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {
            if (isChecked) {
                if (!MultiListParameterView.this.currentSelectedItems.contains(Integer.valueOf(indexSelected))) {
                    MultiListParameterView.this.currentSelectedItems.add(Integer.valueOf(indexSelected));
                }
            } else if (MultiListParameterView.this.currentSelectedItems.contains(Integer.valueOf(indexSelected))) {
                MultiListParameterView.this.currentSelectedItems.remove(Integer.valueOf(indexSelected));
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.parameters.MultiListParameterView.3 */
    class C15423 implements OnCancelListener {
        C15423() {
        }

        public void onCancel(DialogInterface dialog) {
            MultiListParameterView.this.currentSelection = MultiListParameterView.this.previousSelection;
            MultiListParameterView.this.currentSelectedItems = MultiListParameterView.this.previousSelectedItems;
            dialog.dismiss();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.parameters.MultiListParameterView.4 */
    class C15434 implements OnClickListener {
        C15434() {
        }

        public void onClick(View v) {
            MultiListParameterView.this.setErrorMessage(null);
            int count = MultiListParameterView.this.currentSelectedItems.size();
            List selectedKeys = new ArrayList(count);
            StringBuilder builder = new StringBuilder();
            if (MultiListParameterView.this.multiDefinition.valueList != null) {
                for (int selection = 0; selection < count; selection++) {
                    ValueListItem item = (ValueListItem) MultiListParameterView.this.multiDefinition.valueList.get(((Integer) MultiListParameterView.this.currentSelectedItems.get(selection)).intValue());
                    selectedKeys.add(item.key);
                    if (selection != 0) {
                        builder.append(", ");
                    }
                    builder.append(item.label);
                }
            }
            if (count == 0) {
                builder.append(MultiListParameterView.this.multiDefinition.getLabelWithRequirementIndicator());
            }
            MultiListParameterView.this.mButton.setText(builder.toString());
            MultiListParameterView.this.newValue = new MultiParameterValue(selectedKeys);
            if (!MultiListParameterView.this.newValue.equals(MultiListParameterView.this.oldValue)) {
                MultiListParameterView.this.notifyValueChanged(MultiListParameterView.this.newValue, MultiListParameterView.this.oldValue);
            }
            MultiListParameterView.this.oldValue = MultiListParameterView.this.newValue;
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.parameters.MultiListParameterView.5 */
    class C15445 implements OnClickListener {
        C15445() {
        }

        public void onClick(View v) {
            MultiListParameterView.this.currentSelection = MultiListParameterView.this.previousSelection;
            MultiListParameterView.this.currentSelectedItems = MultiListParameterView.this.previousSelectedItems;
        }
    }

    public MultiListParameterView(Context context) {
        super(context);
    }

    public MultiListParameterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mButton = (Button) findViewById(C0256R.id.button);
    }

    public void setState(ParameterState state) {
        super.setState(state);
        this.mButton.setText(state.getDefinition().getLabelWithRequirementIndicator());
        this.currentSelectedItems = new LinkedList();
        this.multiDefinition = (MultipleSelectionParameterDefinition) state.getDefinition();
        if (this.multiDefinition.valueList != null) {
            int i;
            int count = this.multiDefinition.valueList.size();
            this.oldValue = (MultiParameterValue) state.getValues();
            this.currentSelection = new boolean[count];
            if (this.oldValue != null) {
                StringBuilder builder = new StringBuilder();
                for (int c = 0; c < this.oldValue.getValue().size(); c++) {
                    String value = (String) this.oldValue.getValue().get(c);
                    i = 0;
                    while (i < count) {
                        if (((ValueListItem) this.multiDefinition.valueList.get(i)).key.equals(value)) {
                            this.currentSelection[i] = true;
                            if (this.currentSelectedItems.size() > 0) {
                                builder.append(", ");
                            }
                            this.currentSelectedItems.add(Integer.valueOf(i));
                            builder.append(((ValueListItem) this.multiDefinition.valueList.get(i)).label);
                        } else {
                            i++;
                        }
                    }
                }
                if (this.currentSelectedItems.size() > 0) {
                    this.mButton.setText(builder.toString());
                }
            }
            this.labelList = new String[count];
            for (i = 0; i < count; i++) {
                this.labelList[i] = ((ValueListItem) this.multiDefinition.valueList.get(i)).label;
            }
            this.mButton.setOnClickListener(new C15401());
        }
    }

    private void createDialog() {
        this.previousSelection = new boolean[this.currentSelection.length];
        for (int i = 0; i < this.currentSelection.length; i++) {
            this.previousSelection[i] = this.currentSelection[i];
        }
        this.previousSelectedItems = new ArrayList(this.currentSelectedItems);
        DialogCreator dialogCreator = new DialogCreator(getContext(), this.multiDefinition.getLabelWithRequirementIndicator());
        dialogCreator.getClass();
        DialogMultiChoiceList multiChoiceList = new DialogMultiChoiceList(this.labelList, this.currentSelection, new C15412());
        OnCancelListener cancelListener = new C15423();
        dialogCreator.getClass();
        DialogButton positiveButton = new DialogButton(2131165349, new C15434());
        dialogCreator.getClass();
        dialogCreator.create(multiChoiceList, positiveButton, new DialogButton(2131165341, new C15445()), cancelListener).show();
    }
}
