package com.schibsted.scm.nextgenapp.ui.views.parameters;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.models.internal.SingleParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ValueListItem;
import com.schibsted.scm.nextgenapp.models.submodels.SingleSelectionParameterDefinition;
import java.util.ArrayList;
import java.util.List;

public class SpinnerParameterView extends ParameterView {
    private TextView mLabel;
    private Spinner mSpinner;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.parameters.SpinnerParameterView.1 */
    class C15501 implements OnItemSelectedListener {
        private boolean initiated;
        final /* synthetic */ SingleSelectionParameterDefinition val$def;
        final /* synthetic */ SingleParameterValue val$newValue;
        final /* synthetic */ SingleParameterValue val$oldValue;

        C15501(SingleSelectionParameterDefinition singleSelectionParameterDefinition, SingleParameterValue singleParameterValue, SingleParameterValue singleParameterValue2) {
            this.val$def = singleSelectionParameterDefinition;
            this.val$newValue = singleParameterValue;
            this.val$oldValue = singleParameterValue2;
            this.initiated = false;
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            String itemKey = null;
            if (position > 0 && this.val$def.valueList != null) {
                itemKey = ((ValueListItem) this.val$def.valueList.get(position - 1)).key;
            }
            if (this.initiated) {
                this.val$newValue.setValue(itemKey);
                if (!this.val$newValue.equals(this.val$oldValue)) {
                    SpinnerParameterView.this.notifyValueChanged(new SingleParameterValue(this.val$newValue), new SingleParameterValue(this.val$oldValue));
                    this.val$oldValue.setValue(this.val$newValue.getValue());
                }
            }
            this.initiated = true;
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    public SpinnerParameterView(Context context) {
        super(context);
    }

    public SpinnerParameterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mLabel = (TextView) findViewById(2131558724);
        this.mSpinner = (Spinner) findViewById(2131558712);
    }

    public void setState(ParameterState state) {
        super.setState(state);
        SingleSelectionParameterDefinition def = (SingleSelectionParameterDefinition) state.getDefinition();
        SingleParameterValue value = (SingleParameterValue) state.getValues();
        String currentValue = null;
        if (value != null) {
            currentValue = value.getValue();
        }
        this.mLabel.setText(def.getLabelWithRequirementIndicator());
        List<String> labelList = new ArrayList();
        int selected = 0;
        if (def.valueList != null) {
            labelList.add(getContext().getString(2131165471));
            int count = def.valueList.size();
            for (int i = 0; i < count; i++) {
                ValueListItem item = (ValueListItem) def.valueList.get(i);
                if (item.key.equals(currentValue)) {
                    selected = i + 1;
                }
                labelList.add(item.label);
            }
        }
        SingleParameterValue oldValue = new SingleParameterValue(currentValue);
        SingleParameterValue newValue = new SingleParameterValue(currentValue);
        ArrayAdapter<String> adapter = new ArrayAdapter(getContext(), 17367048, labelList);
        adapter.setDropDownViewResource(17367049);
        this.mSpinner.setAdapter(adapter);
        this.mSpinner.setSelection(selected);
        this.mSpinner.setOnItemSelectedListener(new C15501(def, newValue, oldValue));
    }
}
