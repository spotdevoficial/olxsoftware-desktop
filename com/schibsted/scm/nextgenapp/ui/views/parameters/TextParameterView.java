package com.schibsted.scm.nextgenapp.ui.views.parameters;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.OnFocusChangeListener;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.models.internal.SingleParameterValue;
import com.schibsted.scm.nextgenapp.models.submodels.TextParameterDefinition;
import com.schibsted.scm.nextgenapp.ui.factories.AbstractTextParameterViewFactory;
import com.schibsted.scm.nextgenapp.ui.views.LabeledEditText;

public class TextParameterView extends ParameterView {
    private LabeledEditText mLabeledEditText;
    private String previousValue;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.parameters.TextParameterView.1 */
    class C15511 implements TextWatcher {
        C15511() {
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        public void afterTextChanged(Editable s) {
            String newValue = TextParameterView.this.prepareUnformattedNewValue(s.toString());
            TextParameterView.this.setErrorMessage(null);
            SingleParameterValue previous = new SingleParameterValue(TextParameterView.this.previousValue);
            SingleParameterValue current = new SingleParameterValue(newValue);
            if (!current.equals(previous)) {
                TextParameterView.this.notifyValueChanged(current, previous);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.parameters.TextParameterView.2 */
    class C15522 implements OnFocusChangeListener {
        C15522() {
        }

        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus && TextParameterView.this.mLabeledEditText.hasTextChanged()) {
                String text = TextParameterView.this.mLabeledEditText.getText();
                SingleParameterValue previous = new SingleParameterValue(TextParameterView.this.previousValue);
                SingleParameterValue current = new SingleParameterValue(text);
                TextParameterView.this.notifyLostFocusAndValueChanged(current);
                if (!current.equals(previous)) {
                    TextParameterView.this.previousValue = text;
                }
            }
        }
    }

    protected static class SavedState extends BaseSavedState {
        public static final Creator<SavedState> CREATOR;
        private final String value;

        /* renamed from: com.schibsted.scm.nextgenapp.ui.views.parameters.TextParameterView.SavedState.1 */
        static class C15531 implements Creator<SavedState> {
            C15531() {
            }

            public SavedState createFromParcel(Parcel in) {
                return new SavedState(null);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        }

        private SavedState(Parcelable superState, String value) {
            super(superState);
            this.value = value;
        }

        private SavedState(Parcel in) {
            super(in);
            this.value = in.readString();
        }

        public String getValue() {
            return this.value;
        }

        public void writeToParcel(Parcel destination, int flags) {
            super.writeToParcel(destination, flags);
            destination.writeString(this.value);
        }

        static {
            CREATOR = new C15531();
        }
    }

    public TextParameterView(Context context) {
        super(context);
        this.previousValue = BuildConfig.VERSION_NAME;
    }

    public TextParameterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.previousValue = BuildConfig.VERSION_NAME;
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mLabeledEditText = (LabeledEditText) findViewById(2131558635);
    }

    public void setState(ParameterState state) {
        super.setState(state);
        this.mLabeledEditText.setLabel(state.getDefinition().getLabelWithRequirementIndicator());
        if (state.getValues() != null) {
            this.previousValue = (String) state.getValues().getValue();
            this.mLabeledEditText.setText(this.previousValue);
        }
        String format = ((TextParameterDefinition) state.getDefinition()).format;
        if (AbstractTextParameterViewFactory.formats.containsKey(format)) {
            this.mLabeledEditText.setInputType(((Integer) AbstractTextParameterViewFactory.formats.get(format)).intValue());
        }
        this.mLabeledEditText.addTextChangedListener(new C15511());
        this.mLabeledEditText.setOnFocusChangeListener(new C15522());
    }

    protected String prepareUnformattedNewValue(String newValue) {
        return newValue;
    }

    protected Parcelable onSaveInstanceState() {
        return new SavedState(this.mLabeledEditText.getText(), null);
    }

    protected void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.mLabeledEditText.setText(savedState.getValue());
    }

    protected void dispatchSaveInstanceState(SparseArray<Parcelable> container) {
        super.dispatchFreezeSelfOnly(container);
    }

    protected void dispatchRestoreInstanceState(SparseArray<Parcelable> container) {
        super.dispatchThawSelfOnly(container);
    }
}
