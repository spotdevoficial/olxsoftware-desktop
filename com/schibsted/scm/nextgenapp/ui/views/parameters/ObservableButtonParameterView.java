package com.schibsted.scm.nextgenapp.ui.views.parameters;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.models.internal.SingleParameterValue;
import com.schibsted.scm.nextgenapp.ui.views.FiltersListView.FilterClickListener;

public class ObservableButtonParameterView extends ParameterView {
    private Button mButton;
    private FilterClickListener mFilterClickListener;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.parameters.ObservableButtonParameterView.1 */
    class C15451 implements OnClickListener {
        C15451() {
        }

        public void onClick(View v) {
            ObservableButtonParameterView.this.notifyClick();
        }
    }

    public ObservableButtonParameterView(Context context) {
        super(context);
    }

    public ObservableButtonParameterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mButton = (Button) findViewById(2131558739);
    }

    public void setState(ParameterState state) {
        super.setState(state);
        setButtonText(this.mButton, state);
        this.mButton.setOnClickListener(new C15451());
    }

    private boolean isRootCategory(DbCategoryNode category) {
        return TextUtils.isEmpty(category.getParent());
    }

    private void setButtonText(Button button, ParameterState state) {
        DbCategoryNode category = null;
        if (state.getValues() != null) {
            category = C1049M.getDaoManager().getCategoryTree("category_data").getByCode(((SingleParameterValue) state.getValues()).getValue());
            if (isRootCategory(category)) {
                category = null;
            }
        }
        if (button != null) {
            button.setText(category == null ? getResources().getString(2131165293) : category.getLabel());
        }
    }

    private void notifyClick() {
        if (this.mFilterClickListener != null) {
            this.mFilterClickListener.onFilterClick(getState());
        }
    }

    public void setFilterClickListener(FilterClickListener filterClickListener) {
        this.mFilterClickListener = filterClickListener;
    }
}
