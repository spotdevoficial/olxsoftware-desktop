package com.schibsted.scm.nextgenapp.ui.views.parameters;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;

public class LoadingParameterView extends ParameterView {
    public LoadingParameterView(Context context) {
        super(context);
    }

    public LoadingParameterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @TargetApi(11)
    public LoadingParameterView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
