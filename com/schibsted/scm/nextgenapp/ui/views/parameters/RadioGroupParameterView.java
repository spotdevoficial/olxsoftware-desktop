package com.schibsted.scm.nextgenapp.ui.views.parameters;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.models.internal.SingleParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ValueListItem;
import com.schibsted.scm.nextgenapp.models.submodels.SingleSelectionParameterDefinition;
import java.util.ArrayList;
import java.util.List;

public class RadioGroupParameterView extends ParameterView {
    private SingleParameterValue currentValue;
    private List<RadioButton> listOfRadioButtons;
    private LinearLayout mGroup;
    private TextView mLabel;
    private SingleParameterValue oldValue;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.parameters.RadioGroupParameterView.1 */
    class C15461 implements OnClickListener {
        final /* synthetic */ SingleSelectionParameterDefinition val$def;

        C15461(SingleSelectionParameterDefinition singleSelectionParameterDefinition) {
            this.val$def = singleSelectionParameterDefinition;
        }

        public void onClick(View v) {
            RadioGroupParameterView.this.setErrorMessage(null);
            RadioButton button = (RadioButton) v;
            int checkedId = button.getId();
            for (int i = 0; i < RadioGroupParameterView.this.listOfRadioButtons.size(); i++) {
                if (RadioGroupParameterView.this.listOfRadioButtons.get(i) != button) {
                    ((RadioButton) RadioGroupParameterView.this.listOfRadioButtons.get(i)).setChecked(false);
                }
            }
            if (this.val$def.valueList != null) {
                String value = ((ValueListItem) this.val$def.valueList.get(checkedId)).key;
                if (!value.equals(RadioGroupParameterView.this.currentValue.getValue())) {
                    RadioGroupParameterView.this.currentValue.setValue(value);
                    RadioGroupParameterView.this.notifyValueChanged(new SingleParameterValue(RadioGroupParameterView.this.currentValue), new SingleParameterValue(RadioGroupParameterView.this.oldValue));
                    RadioGroupParameterView.this.oldValue = RadioGroupParameterView.this.currentValue;
                }
            }
        }
    }

    public RadioGroupParameterView(Context context) {
        super(context);
    }

    public RadioGroupParameterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mLabel = (TextView) findViewById(2131558724);
        this.mGroup = (LinearLayout) findViewById(2131558733);
    }

    public void setState(ParameterState state) {
        super.setState(state);
        SingleSelectionParameterDefinition def = (SingleSelectionParameterDefinition) state.getDefinition();
        SingleParameterValue value = (SingleParameterValue) state.getValues();
        String valueAsString = null;
        if (value != null) {
            valueAsString = value.getValue();
        }
        this.currentValue = new SingleParameterValue(valueAsString);
        this.oldValue = new SingleParameterValue(valueAsString);
        if (!TextUtils.isEmpty(def.getLabel())) {
            this.mLabel.setText(def.getLabelWithRequirementIndicator());
            this.mLabel.setVisibility(0);
        }
        if (def.valueList != null) {
            int count = def.valueList.size();
            this.listOfRadioButtons = new ArrayList();
            this.mGroup.setWeightSum((float) count);
            for (int i = 0; i < count; i++) {
                ValueListItem item = (ValueListItem) def.valueList.get(i);
                this.mGroup.addView(createRadioButton(item, item.key.equals(valueAsString), i));
            }
        }
    }

    private RadioButton createRadioButton(ValueListItem value, boolean checked, int id) {
        RadioButton button = (RadioButton) LayoutInflater.from(getContext()).inflate(2130903149, this.mGroup, false);
        SingleSelectionParameterDefinition def = (SingleSelectionParameterDefinition) getState().getDefinition();
        button.setText(value.label);
        button.setChecked(checked);
        button.setId(id);
        button.setTag(value);
        this.listOfRadioButtons.add(button);
        button.setOnClickListener(new C15461(def));
        return button;
    }
}
