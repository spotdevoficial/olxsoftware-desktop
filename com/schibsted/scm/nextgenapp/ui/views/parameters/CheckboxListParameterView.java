package com.schibsted.scm.nextgenapp.ui.views.parameters;

import android.content.Context;
import android.support.v7.appcompat.C0086R;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.schibsted.scm.nextgenapp.models.internal.MultiParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.models.internal.ValueListItem;
import com.schibsted.scm.nextgenapp.models.submodels.MultipleSelectionParameterDefinition;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CheckboxListParameterView extends ParameterView {
    private List<Integer> currentSelectedItems;
    private boolean[] currentSelection;
    private MultipleSelectionParameterDefinition def;
    private MultiParameterValue newValue;
    private MultiParameterValue oldValue;
    private boolean[] previousSelection;
    private List<String> selectedKeys;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.parameters.CheckboxListParameterView.1 */
    class C15331 implements OnCheckedChangeListener {
        C15331() {
        }

        public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
            CheckboxListParameterView.this.setErrorMessage(null);
            if (isChecked) {
                if (!CheckboxListParameterView.this.currentSelectedItems.contains(Integer.valueOf(compoundButton.getId()))) {
                    CheckboxListParameterView.this.currentSelectedItems.add(Integer.valueOf(compoundButton.getId()));
                }
            } else if (CheckboxListParameterView.this.currentSelectedItems.contains(Integer.valueOf(compoundButton.getId()))) {
                CheckboxListParameterView.this.currentSelectedItems.remove(Integer.valueOf(compoundButton.getId()));
            }
            int count = CheckboxListParameterView.this.currentSelectedItems.size();
            List selectedKeys = new ArrayList(count);
            if (CheckboxListParameterView.this.def.valueList != null) {
                for (int selection = 0; selection < count; selection++) {
                    selectedKeys.add(((ValueListItem) CheckboxListParameterView.this.def.valueList.get(((Integer) CheckboxListParameterView.this.currentSelectedItems.get(selection)).intValue())).key);
                }
            }
            CheckboxListParameterView.this.newValue = new MultiParameterValue(selectedKeys);
            if (!CheckboxListParameterView.this.newValue.equals(CheckboxListParameterView.this.oldValue)) {
                CheckboxListParameterView.this.notifyValueChanged(CheckboxListParameterView.this.newValue, CheckboxListParameterView.this.oldValue);
            }
            CheckboxListParameterView.this.oldValue = CheckboxListParameterView.this.newValue;
        }
    }

    public CheckboxListParameterView(Context context) {
        super(context);
    }

    public CheckboxListParameterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setState(ParameterState state) {
        super.setState(state);
        this.currentSelectedItems = new LinkedList();
        this.def = (MultipleSelectionParameterDefinition) state.getDefinition();
        if (this.def.valueList != null) {
            int i;
            int count = this.def.valueList.size();
            this.oldValue = (MultiParameterValue) state.getValues();
            this.currentSelection = new boolean[count];
            if (this.oldValue != null) {
                for (int c = 0; c < this.oldValue.getValue().size(); c++) {
                    String value = (String) this.oldValue.getValue().get(c);
                    for (i = 0; i < count; i++) {
                        if (((ValueListItem) this.def.valueList.get(i)).key.equals(value)) {
                            this.currentSelection[i] = true;
                            this.currentSelectedItems.add(Integer.valueOf(i));
                            break;
                        }
                    }
                }
            }
            String[] labelList = new String[count];
            for (i = 0; i < count; i++) {
                labelList[i] = ((ValueListItem) this.def.valueList.get(i)).label;
            }
            this.previousSelection = new boolean[this.currentSelection.length];
            for (i = 0; i < this.currentSelection.length; i++) {
                this.previousSelection[i] = this.currentSelection[i];
            }
            this.selectedKeys = new ArrayList(this.currentSelectedItems.size());
            TextView label = (TextView) findViewById(2131558724);
            if (!TextUtils.isEmpty(this.def.getLabel())) {
                label.setText(this.def.getLabelWithRequirementIndicator());
                label.setVisibility(0);
            }
            LinearLayout group = (LinearLayout) findViewById(2131558733);
            for (i = 0; i < count; i++) {
                group.addView(createCheckbox(group, (ValueListItem) this.def.valueList.get(i), this.currentSelection[i], i));
            }
        }
    }

    private RelativeLayout createCheckbox(ViewGroup parent, ValueListItem value, boolean checked, int id) {
        RelativeLayout checkBoxContainer = (RelativeLayout) LayoutInflater.from(getContext()).inflate(2130903180, parent, false);
        CheckBox checkbox = (CheckBox) checkBoxContainer.findViewById(C0086R.id.checkbox);
        checkbox.setText(value.label);
        checkbox.setChecked(checked);
        checkbox.setId(id);
        checkbox.setTag(value);
        checkbox.setOnCheckedChangeListener(new C15331());
        return checkBoxContainer;
    }
}
