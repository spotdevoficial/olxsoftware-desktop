package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.managers.SearchParameterManager;
import com.schibsted.scm.nextgenapp.backend.managers.ValuesDatabaseManager;
import com.schibsted.scm.nextgenapp.backend.managers.ValuesDatabaseManager.ReadDatabaseListener;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.models.internal.SingleParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ValueListItem;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import com.schibsted.scm.nextgenapp.models.submodels.ListBasedParameterDefinition;
import com.schibsted.scm.nextgenapp.models.submodels.ParameterDefinition;
import com.schibsted.scm.nextgenapp.models.submodels.RegionNode;
import com.schibsted.scm.nextgenapp.models.submodels.ValueList;
import com.schibsted.scm.nextgenapp.ui.factories.CheckboxListParameterViewFactory;
import com.schibsted.scm.nextgenapp.ui.factories.CheckboxParameterViewFactory;
import com.schibsted.scm.nextgenapp.ui.factories.DoubleSliderParameterViewFactory;
import com.schibsted.scm.nextgenapp.ui.factories.DualIntegerTextParameterViewFactory;
import com.schibsted.scm.nextgenapp.ui.factories.DualSpinnerParameterViewFactory;
import com.schibsted.scm.nextgenapp.ui.factories.EmptyParameterViewFactory;
import com.schibsted.scm.nextgenapp.ui.factories.LoadingParameterViewFactory;
import com.schibsted.scm.nextgenapp.ui.factories.MultiListParameterViewFactory;
import com.schibsted.scm.nextgenapp.ui.factories.ObservableButtonParameterViewFactory;
import com.schibsted.scm.nextgenapp.ui.factories.ParameterChangeListener;
import com.schibsted.scm.nextgenapp.ui.factories.ParameterViewFactory;
import com.schibsted.scm.nextgenapp.ui.factories.ParameterViewHandle;
import com.schibsted.scm.nextgenapp.ui.factories.RadioGroupParameterViewFactory;
import com.schibsted.scm.nextgenapp.ui.factories.SingleListParameterViewFactory;
import com.schibsted.scm.nextgenapp.ui.factories.SliderParameterViewFactory;
import com.schibsted.scm.nextgenapp.ui.factories.SpinnerParameterViewFactory;
import com.schibsted.scm.nextgenapp.ui.factories.TextParameterViewFactory;
import com.schibsted.scm.nextgenapp.utils.RegionsBrowser;
import com.schibsted.scm.nextgenapp.utils.RegionsBrowser.RegionsBrowserCallback;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class FiltersListView extends NamedViewsLayout implements ParameterChangeListener {
    private static final String TAG;
    private ReadDatabaseListener dbListener;
    private Identifier mCurrentLocationIdentifier;
    private int mCurrentLocationIndex;
    private HashMap<String, Integer> mDatabaseRetries;
    private HashMap<String, List<ParameterState>> mDatabaseStates;
    private FilterClickListener mFilterClickListener;
    private int mLocationRetries;
    private ArrayList<ParameterState> mLocationStates;
    private SearchParameterManager mManager;
    private OnChangedViewByToProximityListener mOnChangedViewByToProximityListener;
    private ParameterChangeListener mOnParameterChangeListener;
    private RegionsBrowser mRegionBrowser;
    private LinkedHashMap<String, ParameterViewHandle> mViewHandles;
    RegionsBrowserCallback regionListener;

    public interface FilterClickListener {
        void onFilterClick(ParameterState parameterState);
    }

    public interface OnChangedViewByToProximityListener {
        void onChangedViewByToProximity();
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.FiltersListView.1 */
    class C15071 implements ReadDatabaseListener {
        C15071() {
        }

        public void onResult(boolean success, String databaseCode) {
            synchronized (this) {
                if (success) {
                    FiltersListView.this.replaceDatabaseViews(databaseCode);
                } else {
                    FiltersListView.this.retryDatabaseRead(databaseCode);
                }
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.FiltersListView.2 */
    class C15082 implements RegionsBrowserCallback {
        C15082() {
        }

        public void beforeSelect(boolean asynchronous) {
            if (FiltersListView.this.mLocationStates.size() <= FiltersListView.this.mCurrentLocationIndex) {
                Logger.error(FiltersListView.TAG, "Unexpected error creating location views. The current index is not valid");
            } else if (asynchronous) {
                ParameterState state = (ParameterState) FiltersListView.this.mLocationStates.get(FiltersListView.this.mCurrentLocationIndex);
                ParameterViewHandle handle = new LoadingParameterViewFactory().produce(FiltersListView.this.getContext(), FiltersListView.this, state, FiltersListView.this);
                FiltersListView.this.mViewHandles.put(state.getDefinition().key, handle);
                FiltersListView.this.replaceNamedView(state.getDefinition().key, handle.getView());
            }
        }

        public void afterSelect(boolean success) {
            synchronized (this) {
                if (success) {
                    FiltersListView.this.replaceLocationView();
                } else {
                    FiltersListView.this.retryLocationRead();
                }
            }
        }
    }

    static {
        TAG = FiltersListView.class.getSimpleName();
    }

    public FiltersListView(Context context) {
        super(context);
        this.dbListener = new C15071();
        this.regionListener = new C15082();
        init();
    }

    public FiltersListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.dbListener = new C15071();
        this.regionListener = new C15082();
        init();
    }

    public Map<String, ParameterViewHandle> getViewHandles() {
        return this.mViewHandles;
    }

    private void init() {
        this.mViewHandles = new LinkedHashMap();
        this.mDatabaseStates = new HashMap();
        this.mDatabaseRetries = new HashMap();
        this.mLocationStates = new ArrayList();
        this.mRegionBrowser = new RegionsBrowser();
        this.mRegionBrowser.setCallback(this.regionListener);
    }

    public void setManager(SearchParameterManager manager) {
        this.mManager = manager;
    }

    public void setOnParameterChangeListener(ParameterChangeListener onParameterChangeListener) {
        this.mOnParameterChangeListener = onParameterChangeListener;
    }

    public void reset() {
        this.mManager = null;
        refresh();
    }

    public void onParameterChange(ParameterDefinition def, ParameterValue newValue, ParameterValue oldValue) {
        if (changedViewByToProximity(def, newValue, oldValue) && this.mOnChangedViewByToProximityListener != null) {
            this.mOnChangedViewByToProximityListener.onChangedViewByToProximity();
        }
        if (newValue == null || !newValue.equals(oldValue)) {
            if (this.mOnParameterChangeListener != null) {
                this.mOnParameterChangeListener.onParameterChange(def, newValue, oldValue);
            }
            boolean dependantFilterChanged = false;
            if (def.isBasedOnDatabase().booleanValue()) {
                dependantFilterChanged = clearDependantDatabaseValues((ListBasedParameterDefinition) def);
            } else if (def.isBasedOnLocation().booleanValue()) {
                dependantFilterChanged = clearDependantLocationValues((ListBasedParameterDefinition) def);
            }
            boolean listChanged = this.mManager.update(def, newValue);
            if (dependantFilterChanged || listChanged) {
                refresh();
            }
        }
    }

    private boolean changedViewByToProximity(ParameterDefinition def, ParameterValue newValue, ParameterValue oldValue) {
        if ("viewby".equals(def.key) && "location".equals(oldValue.getValue()) && "proximity".equals(newValue.getValue())) {
            return true;
        }
        return false;
    }

    public void onParameterViewLostFocusAfterValueChanged(ParameterDefinition definition, ParameterValue newValue) {
        if (this.mOnParameterChangeListener != null) {
            this.mOnParameterChangeListener.onParameterViewLostFocusAfterValueChanged(definition, newValue);
        }
    }

    public void refresh() {
        synchronized (this) {
            clearViewsAndLists();
            if (this.mManager == null) {
                return;
            }
            if (this.mRegionBrowser != null) {
                this.mRegionBrowser.setCallback(null);
                this.mRegionBrowser = null;
            }
            RegionPathApiModel regionPath = this.mManager.getRegion();
            for (Entry<String, ParameterState> entry : this.mManager.getState().entrySet()) {
                ParameterState state = (ParameterState) entry.getValue();
                if (state.getDefinition().isBasedOnDatabase().booleanValue()) {
                    addDatabaseView(state);
                } else if (state.getDefinition().isBasedOnLocation().booleanValue()) {
                    ListBasedParameterDefinition def = (ListBasedParameterDefinition) state.getDefinition();
                    if (regionPath == null || regionPath.findLevel(def.locationKey) == null) {
                        addLocationView(state);
                    }
                } else {
                    addRegularView(state);
                }
            }
            processPendingDatabaseViews();
            processPendingLocationViews();
        }
    }

    private void clearViewsAndLists() {
        this.mViewHandles.clear();
        this.mDatabaseStates.clear();
        this.mDatabaseRetries.clear();
        this.mLocationStates.clear();
        this.mLocationRetries = 0;
        this.mCurrentLocationIdentifier = null;
        removeAllViews();
    }

    private boolean clearDependantDatabaseValues(ListBasedParameterDefinition def) {
        boolean dependantFilterChanged = false;
        if (!def.isBasedOnDatabase().booleanValue()) {
            return false;
        }
        if (this.mDatabaseStates.containsKey(def.valuesDatabase.code)) {
            for (ParameterState state : (List) this.mDatabaseStates.get(def.valuesDatabase.code)) {
                ListBasedParameterDefinition dependantDef = (ListBasedParameterDefinition) state.getDefinition();
                if (dependantDef.valuesDatabase.keys.contains(def.key)) {
                    this.mManager.update(dependantDef, null);
                    dependantFilterChanged = true;
                }
            }
        }
        return dependantFilterChanged;
    }

    private void addDatabaseView(ParameterState state) {
        ListBasedParameterDefinition def = (ListBasedParameterDefinition) state.getDefinition();
        ParameterViewFactory factory = getDatabaseViewFactory(state);
        if (!this.mDatabaseStates.containsKey(def.valuesDatabase.code)) {
            this.mDatabaseStates.put(def.valuesDatabase.code, new ArrayList());
        }
        ((List) this.mDatabaseStates.get(def.valuesDatabase.code)).add(state);
        ParameterViewHandle handle = factory.produce(getContext(), this, state, this);
        this.mViewHandles.put(state.getDefinition().key, handle);
        addNamedView(state.getDefinition().key, handle.getView());
    }

    private ParameterViewFactory getDatabaseViewFactory(ParameterState state) {
        if (!state.getDefinition().isBasedOnDatabase().booleanValue()) {
            return new EmptyParameterViewFactory();
        }
        ListBasedParameterDefinition def = (ListBasedParameterDefinition) state.getDefinition();
        ValuesDatabaseManager dbManager = C1049M.getValuesDatabaseManager();
        if (!dbManager.isFetched(def.valuesDatabase.code)) {
            return new LoadingParameterViewFactory();
        }
        ValueList listOfValues = dbManager.getValueList(def.valuesDatabase, this.mManager.getState());
        if (listOfValues == null || listOfValues.isEmpty()) {
            return new EmptyParameterViewFactory();
        }
        def.valueList = listOfValues;
        return createFactory(state);
    }

    private void processPendingDatabaseViews() {
        ValuesDatabaseManager dbManager = C1049M.getValuesDatabaseManager();
        for (String databaseCode : this.mDatabaseStates.keySet()) {
            if (!dbManager.isFetched(databaseCode)) {
                dbManager.getDatabase(databaseCode, this.dbListener);
            }
        }
    }

    private void replaceDatabaseViews(String databaseCode) {
        ValuesDatabaseManager dbManager = C1049M.getValuesDatabaseManager();
        List<ParameterState> listOfStates = (List) this.mDatabaseStates.get(databaseCode);
        if (dbManager != null && listOfStates != null) {
            for (ParameterState state : listOfStates) {
                ListBasedParameterDefinition def = (ListBasedParameterDefinition) state.getDefinition();
                def.valueList = dbManager.getValueList(def.valuesDatabase, this.mManager.getState());
                if (def.valueList == null || def.valueList.isEmpty()) {
                    this.mViewHandles.remove(def.key);
                    removeNamedView(def.key);
                } else {
                    ParameterViewHandle handle = createFactory(state).produce(getContext(), this, state, this);
                    this.mViewHandles.put(state.getDefinition().key, handle);
                    replaceNamedView(state.getDefinition().key, handle.getView());
                }
            }
        }
    }

    private void retryDatabaseRead(String databaseCode) {
        ValuesDatabaseManager dbManager = C1049M.getValuesDatabaseManager();
        if (dbManager != null) {
            List<ParameterState> listOfStates = (List) this.mDatabaseStates.get(databaseCode);
            if (!this.mDatabaseRetries.containsKey(databaseCode)) {
                this.mDatabaseRetries.put(databaseCode, Integer.valueOf(0));
            }
            if (((Integer) this.mDatabaseRetries.get(databaseCode)).intValue() < 2) {
                dbManager.getDatabase(databaseCode, this.dbListener);
                return;
            }
            for (ParameterState state : listOfStates) {
                removeNamedView(state.getDefinition().key);
            }
        }
    }

    private boolean clearDependantLocationValues(ListBasedParameterDefinition def) {
        boolean dependantFilterChanged = false;
        boolean mustRemoveNext = false;
        if (!def.isBasedOnLocation().booleanValue()) {
            return false;
        }
        Iterator i$ = this.mLocationStates.iterator();
        while (i$.hasNext()) {
            ListBasedParameterDefinition dependantDef = (ListBasedParameterDefinition) ((ParameterState) i$.next()).getDefinition();
            if (mustRemoveNext) {
                this.mManager.update(dependantDef, null);
                dependantFilterChanged = true;
            }
            if (dependantDef.locationKey.equals(def.locationKey)) {
                mustRemoveNext = true;
            }
        }
        return dependantFilterChanged;
    }

    private void addLocationView(ParameterState state) {
        ParameterViewFactory factory = new EmptyParameterViewFactory();
        this.mLocationStates.add(state);
        ParameterViewHandle handle = factory.produce(getContext(), this, state, this);
        this.mViewHandles.put(state.getDefinition().key, handle);
        addNamedView(state.getDefinition().key, handle.getView());
    }

    private void processPendingLocationViews() {
        if (!this.mLocationStates.isEmpty()) {
            this.mRegionBrowser = new RegionsBrowser();
            this.mRegionBrowser.setCallback(this.regionListener);
            RegionPathApiModel regionPath = this.mManager.getRegion();
            this.mCurrentLocationIdentifier = new Identifier(regionPath != null ? regionPath.getIdentifier() : null);
            this.mCurrentLocationIndex = 0;
            this.mRegionBrowser.setLevel(this.mCurrentLocationIdentifier);
        }
    }

    private void replaceLocationView() {
        synchronized (this) {
            if (this.mLocationStates.size() <= this.mCurrentLocationIndex) {
                Logger.error(TAG, "Unexpected error creating location views. The current index is not valid");
                return;
            }
            ParameterState state = (ParameterState) this.mLocationStates.get(this.mCurrentLocationIndex);
            ListBasedParameterDefinition def = (ListBasedParameterDefinition) state.getDefinition();
            def.valueList = new ValueList();
            if (this.mRegionBrowser.getRegionsList() != null) {
                for (RegionNode node : this.mRegionBrowser.getRegionsList()) {
                    if (def.locationKey.equals(node.key)) {
                        def.valueList.add(new ValueListItem(node.code, node.label));
                    }
                }
            }
            if (!def.isValidValue(state.getValues())) {
                state.setValues(null);
                this.mManager.update(def, null);
            }
            ParameterViewHandle handle;
            if (def.valueList.isEmpty()) {
                handle = new EmptyParameterViewFactory().produce(getContext(), this, state, this);
                this.mViewHandles.put(state.getDefinition().key, handle);
                replaceNamedView(state.getDefinition().key, handle.getView());
            } else {
                handle = createFactory(state).produce(getContext(), this, state, this);
                this.mViewHandles.put(state.getDefinition().key, handle);
                replaceNamedView(state.getDefinition().key, handle.getView());
                this.mCurrentLocationIndex++;
                if (this.mRegionBrowser != null && def.isValidValue(state.getValues()) && this.mLocationStates.size() > this.mCurrentLocationIndex && (state.getValues() instanceof SingleParameterValue)) {
                    this.mCurrentLocationIdentifier.keys.add(def.locationKey);
                    this.mCurrentLocationIdentifier.values.add(((SingleParameterValue) state.getValues()).getValue());
                    this.mRegionBrowser.setLevel(this.mCurrentLocationIdentifier);
                }
            }
        }
    }

    private void retryLocationRead() {
        if (this.mRegionBrowser == null) {
            return;
        }
        if (this.mLocationRetries < 2) {
            this.mRegionBrowser.setLevel(this.mCurrentLocationIdentifier);
            this.mLocationRetries++;
            return;
        }
        Iterator i$ = this.mLocationStates.iterator();
        while (i$.hasNext()) {
            removeNamedView(((ParameterState) i$.next()).getDefinition().key);
        }
    }

    private void addRegularView(ParameterState state) {
        ParameterViewHandle handle = createFactory(state).produce(getContext(), this, state, this);
        this.mViewHandles.put(state.getDefinition().key, handle);
        addNamedView(state.getDefinition().key, handle.getView());
    }

    private ParameterViewFactory createFactory(ParameterState state) {
        String presentation = state.getDefinition().presentation;
        int type = state.getDefinition().getSearchFilterType();
        ParameterViewFactory factory = null;
        if (4 == type) {
            factory = new TextParameterViewFactory();
        } else if (3 == type) {
            factory = new CheckboxParameterViewFactory();
        } else if (1 == type) {
            factory = createSingleListParameterFactory(presentation);
        } else if ("slider".equals(presentation) && (type == 0 || 5 == type)) {
            factory = new DoubleSliderParameterViewFactory();
        } else if (2 == type) {
            if ("submenu".equals(presentation)) {
                factory = new MultiListParameterViewFactory();
            } else if ("checkbox".equals(presentation)) {
                factory = new CheckboxListParameterViewFactory();
            }
        } else if ("dropdown".equals(presentation) && type == 0) {
            factory = new DualSpinnerParameterViewFactory();
        } else if ("textfield".equals(presentation) && 5 == type) {
            factory = new DualIntegerTextParameterViewFactory();
        }
        if (factory == null) {
            return new EmptyParameterViewFactory();
        }
        return factory;
    }

    private ParameterViewFactory createSingleListParameterFactory(String presentation) {
        if ("custom_category".equals(presentation)) {
            return new ObservableButtonParameterViewFactory(this.mFilterClickListener);
        }
        if ("slider".equals(presentation)) {
            return new SliderParameterViewFactory();
        }
        if ("submenu".equals(presentation)) {
            return new SingleListParameterViewFactory();
        }
        if ("radio".equals(presentation) || "sorting".equals(presentation)) {
            return new RadioGroupParameterViewFactory();
        }
        if ("dropdown".equals(presentation)) {
            return new SpinnerParameterViewFactory();
        }
        return null;
    }

    public void setOnChangedViewByToProximityListener(OnChangedViewByToProximityListener onChangedViewByToProximityListener) {
        this.mOnChangedViewByToProximityListener = onChangedViewByToProximityListener;
    }

    public void setFilterClickListener(FilterClickListener filterClickListener) {
        this.mFilterClickListener = filterClickListener;
    }
}
