package com.schibsted.scm.nextgenapp.ui.holders;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public abstract class TypedViewHolder<V extends ViewGroup> extends ViewHolder implements OnClickListener {
    public TypedViewHolder(V itemView) {
        super(itemView);
    }

    public void setOnClickListener(OnClickListener listener) {
        this.itemView.setOnClickListener(listener);
    }

    public void onClick(View view) {
    }

    public V getView() {
        return (ViewGroup) this.itemView;
    }
}
