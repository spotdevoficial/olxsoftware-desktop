package com.schibsted.scm.nextgenapp.ui.listeners;

import com.schibsted.scm.nextgenapp.models.submodels.Ad;

public interface OnAdClickListener {
    void onAdClick(Ad ad, int i);
}
