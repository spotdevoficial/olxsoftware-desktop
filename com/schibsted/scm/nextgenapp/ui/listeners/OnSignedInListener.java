package com.schibsted.scm.nextgenapp.ui.listeners;

public interface OnSignedInListener {
    void onSignedIn();
}
