package com.schibsted.scm.nextgenapp.ui.factories;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.ui.views.parameters.SingleListParameterView;

public class SingleListParameterViewFactory implements ParameterViewFactory {

    /* renamed from: com.schibsted.scm.nextgenapp.ui.factories.SingleListParameterViewFactory.1 */
    class C14101 implements ParameterViewHandle {
        final /* synthetic */ ParameterChangeListener val$listener;
        final /* synthetic */ ParameterState val$state;
        final /* synthetic */ SingleListParameterView val$view;

        C14101(SingleListParameterView singleListParameterView, ParameterState parameterState, ParameterChangeListener parameterChangeListener) {
            this.val$view = singleListParameterView;
            this.val$state = parameterState;
            this.val$listener = parameterChangeListener;
        }

        public View getView() {
            return this.val$view;
        }
    }

    public ParameterViewHandle produce(Context context, ViewGroup parent, ParameterState state, ParameterChangeListener listener) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (state.getDefinition().getSearchFilterType() != 1) {
            return null;
        }
        SingleListParameterView view = (SingleListParameterView) inflater.inflate(2130903152, parent, false);
        view.setState(state);
        view.setListener(listener);
        return new C14101(view, state, listener);
    }
}
