package com.schibsted.scm.nextgenapp.ui.factories;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.ui.views.parameters.CheckboxParameterView;

public class CheckboxParameterViewFactory implements ParameterViewFactory {

    /* renamed from: com.schibsted.scm.nextgenapp.ui.factories.CheckboxParameterViewFactory.1 */
    class C14001 implements ParameterViewHandle {
        final /* synthetic */ ParameterChangeListener val$listener;
        final /* synthetic */ ParameterState val$state;
        final /* synthetic */ CheckboxParameterView val$view;

        C14001(CheckboxParameterView checkboxParameterView, ParameterState parameterState, ParameterChangeListener parameterChangeListener) {
            this.val$view = checkboxParameterView;
            this.val$state = parameterState;
            this.val$listener = parameterChangeListener;
        }

        public View getView() {
            return this.val$view;
        }
    }

    public ParameterViewHandle produce(Context context, ViewGroup parent, ParameterState state, ParameterChangeListener listener) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (state.getDefinition().getSearchFilterType() != 3) {
            return null;
        }
        CheckboxParameterView view = (CheckboxParameterView) inflater.inflate(2130903141, parent, false);
        view.setState(state);
        view.setListener(listener);
        return new C14001(view, state, listener);
    }
}
