package com.schibsted.scm.nextgenapp.ui.factories;

import android.content.Context;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;

public interface ParameterViewFactory {
    ParameterViewHandle produce(Context context, ViewGroup viewGroup, ParameterState parameterState, ParameterChangeListener parameterChangeListener);
}
