package com.schibsted.scm.nextgenapp.ui.factories;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.ui.views.parameters.SliderParameterView;

public class SliderParameterViewFactory implements ParameterViewFactory {

    /* renamed from: com.schibsted.scm.nextgenapp.ui.factories.SliderParameterViewFactory.1 */
    class C14111 implements ParameterViewHandle {
        final /* synthetic */ ParameterChangeListener val$listener;
        final /* synthetic */ ParameterState val$state;
        final /* synthetic */ SliderParameterView val$view;

        C14111(SliderParameterView sliderParameterView, ParameterState parameterState, ParameterChangeListener parameterChangeListener) {
            this.val$view = sliderParameterView;
            this.val$state = parameterState;
            this.val$listener = parameterChangeListener;
        }

        public View getView() {
            return this.val$view;
        }
    }

    public ParameterViewHandle produce(Context context, ViewGroup parent, ParameterState state, ParameterChangeListener listener) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (state.getDefinition().getSearchFilterType() != 1) {
            return null;
        }
        SliderParameterView view = (SliderParameterView) inflater.inflate(2130903222, parent, false);
        view.setState(state);
        view.setListener(listener);
        return new C14111(view, state, listener);
    }
}
