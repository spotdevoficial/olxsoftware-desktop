package com.schibsted.scm.nextgenapp.ui.factories;

import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.submodels.ParameterDefinition;

public interface ParameterChangeListener {
    void onParameterChange(ParameterDefinition parameterDefinition, ParameterValue parameterValue, ParameterValue parameterValue2);

    void onParameterViewLostFocusAfterValueChanged(ParameterDefinition parameterDefinition, ParameterValue parameterValue);
}
