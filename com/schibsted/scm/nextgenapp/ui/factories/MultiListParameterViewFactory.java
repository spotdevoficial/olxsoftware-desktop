package com.schibsted.scm.nextgenapp.ui.factories;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.ui.views.parameters.MultiListParameterView;

public class MultiListParameterViewFactory implements ParameterViewFactory {

    /* renamed from: com.schibsted.scm.nextgenapp.ui.factories.MultiListParameterViewFactory.1 */
    class C14071 implements ParameterViewHandle {
        final /* synthetic */ ParameterChangeListener val$listener;
        final /* synthetic */ ParameterState val$state;
        final /* synthetic */ MultiListParameterView val$view;

        C14071(MultiListParameterView multiListParameterView, ParameterState parameterState, ParameterChangeListener parameterChangeListener) {
            this.val$view = multiListParameterView;
            this.val$state = parameterState;
            this.val$listener = parameterChangeListener;
        }

        public View getView() {
            return this.val$view;
        }
    }

    public ParameterViewHandle produce(Context context, ViewGroup parent, ParameterState state, ParameterChangeListener listener) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (state.getDefinition().getSearchFilterType() != 2) {
            return null;
        }
        MultiListParameterView view = (MultiListParameterView) inflater.inflate(2130903147, parent, false);
        view.setState(state);
        view.setListener(listener);
        return new C14071(view, state, listener);
    }
}
