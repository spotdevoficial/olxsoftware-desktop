package com.schibsted.scm.nextgenapp.ui.fragments;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import com.android.volley.VolleyError;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.adapters.HeaderFooterAdapter;
import com.schibsted.scm.nextgenapp.backend.bus.messages.AdListNetworkErrorMessage;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteListManager;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteListManager.OnAdListChangedListener;
import com.schibsted.scm.nextgenapp.ui.views.SwipeRefreshContainer;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import com.urbanairship.C1608R;

public abstract class ListingFragment extends RecyclerViewFragment implements OnRefreshListener, OnAdListChangedListener {
    public static final String TAG;
    private RemoteListManager mAdListManager;
    protected View mCurrentFooterView;
    protected View mCurrentHeaderView;
    private HeaderFooterAdapter mHeaderAdapter;
    private State mRecyclerViewState;
    protected Bundle state;
    protected SwipeRefreshContainer swipeRefreshContainer;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.ListingFragment.1 */
    class C14461 implements OnClickListener {
        C14461() {
        }

        public void onClick(View v) {
            ListingFragment.this.getListManager().startLoading();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.ListingFragment.2 */
    static /* synthetic */ class C14472 {
        static final /* synthetic */ int[] f1288x87b6e7b3;

        static {
            f1288x87b6e7b3 = new int[State.values().length];
            try {
                f1288x87b6e7b3[State.DEFAULT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f1288x87b6e7b3[State.END_OF_LIST.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f1288x87b6e7b3[State.ERROR.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f1288x87b6e7b3[State.EMPTY.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f1288x87b6e7b3[State.LOADING.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                f1288x87b6e7b3[State.REFRESHING.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
        }
    }

    private enum State {
        DEFAULT,
        END_OF_LIST,
        ERROR,
        EMPTY,
        LOADING,
        REFRESHING
    }

    public abstract boolean activatePullToRefresh();

    public abstract void requestScrollTo(int i);

    static {
        TAG = ListingFragment.class.getSimpleName();
    }

    public ListingFragment() {
        this.mAdListManager = null;
        this.mRecyclerViewState = State.DEFAULT;
    }

    protected final RemoteListManager getListManager() {
        return this.mAdListManager;
    }

    protected final void setListManager(RemoteListManager manager) {
        this.mAdListManager = manager;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        this.swipeRefreshContainer = (SwipeRefreshContainer) v.findViewById(2131558895);
        this.swipeRefreshContainer.setRecyclerView((RecyclerView) v.findViewById(2131558896));
        this.swipeRefreshContainer.setColorSchemeResources(2131493058, 2131493059, 2131493060, 2131493061);
        return v;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setState(this.mRecyclerViewState);
        if (activatePullToRefresh()) {
            this.swipeRefreshContainer.setEnabled(true);
            this.swipeRefreshContainer.setOnRefreshListener(this);
            return;
        }
        this.swipeRefreshContainer.setEnabled(false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setState(this.mRecyclerViewState);
    }

    public void onResume() {
        super.onResume();
        if (this.mAdListManager != null) {
            this.mAdListManager.setListChangeListener(this);
        }
        if (this.mHeaderAdapter != null) {
            this.mHeaderAdapter.notifyDataSetChanged();
        }
    }

    public void onPause() {
        super.onPause();
        if (this.mAdListManager != null) {
            this.mAdListManager.setListChangeListener(null);
        }
    }

    public boolean hasCurrentHeader() {
        return false;
    }

    protected View getDefaultHeader(LayoutInflater inflater) {
        return null;
    }

    protected View getDefaultFooter(LayoutInflater inflater) {
        return inflater.inflate(2130903187, null);
    }

    protected View getEndOfListFooter(LayoutInflater inflater) {
        return null;
    }

    protected View getErrorHeader(LayoutInflater inflater) {
        return null;
    }

    protected View getErrorFooter(LayoutInflater inflater) {
        LinearLayout layout = (LinearLayout) inflater.inflate(2130903190, null);
        ((Button) layout.findViewById(2131558858)).setOnClickListener(new C14461());
        return layout;
    }

    protected View getEmptyHeader(LayoutInflater inflater) {
        return null;
    }

    protected View getEmptyFooter(LayoutInflater inflater) {
        return inflater.inflate(2130903188, null);
    }

    protected View getLoadingHeader(LayoutInflater inflater) {
        return null;
    }

    protected View getRefreshingHeader(LayoutInflater inflater) {
        return null;
    }

    protected View getRefreshingFooter(LayoutInflater inflater) {
        return null;
    }

    protected View getLoadingFooter(LayoutInflater inflater) {
        return inflater.inflate(2130903189, null);
    }

    private void setState(State state) {
        this.mRecyclerViewState = state;
        if (isAdded() && this.mHeaderAdapter != null) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            try {
                this.mHeaderAdapter.setHeaderView(null);
                this.mHeaderAdapter.setFooterView(null);
            } catch (ClassCastException e) {
                Logger.error(TAG, "list view crashed", e);
            }
            switch (C14472.f1288x87b6e7b3[state.ordinal()]) {
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    this.mCurrentHeaderView = getDefaultHeader(inflater);
                    this.mCurrentFooterView = getDefaultFooter(inflater);
                    break;
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    this.mCurrentFooterView = getEndOfListFooter(inflater);
                    break;
                case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                    this.mCurrentHeaderView = getErrorHeader(inflater);
                    this.mCurrentFooterView = getErrorFooter(inflater);
                    break;
                case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                    this.mCurrentHeaderView = getEmptyHeader(inflater);
                    this.mCurrentFooterView = getEmptyFooter(inflater);
                    break;
                case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                    this.mCurrentHeaderView = getLoadingHeader(inflater);
                    this.mCurrentFooterView = getLoadingFooter(inflater);
                    break;
                case C1608R.styleable.MapAttrs_liteMode /*6*/:
                    this.mCurrentHeaderView = getRefreshingHeader(inflater);
                    this.mCurrentFooterView = getRefreshingFooter(inflater);
                    break;
            }
            if (this.mCurrentHeaderView != null) {
                setHeaderView(this.mCurrentHeaderView);
            }
            if (this.mCurrentFooterView != null) {
                setFooterView(this.mCurrentFooterView);
            }
            if (this.swipeRefreshContainer != null) {
                boolean z;
                SwipeRefreshContainer swipeRefreshContainer = this.swipeRefreshContainer;
                if (State.REFRESHING == state) {
                    z = true;
                } else {
                    z = false;
                }
                swipeRefreshContainer.setRefreshing(z);
            }
        }
    }

    public void resumeOnError() {
        if (this.mRecyclerViewState == State.ERROR && getListManager() != null) {
            getListManager().startLoading();
        }
    }

    public void setHeaderView(View header) {
        this.mCurrentHeaderView = header;
        this.mHeaderAdapter.setHeaderView(header);
    }

    public void setFooterView(View footer) {
        this.mCurrentFooterView = footer;
        this.mHeaderAdapter.setFooterView(footer);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (this.swipeRefreshContainer != null) {
            this.swipeRefreshContainer.dispatchConfigurationChanged(newConfig);
        }
    }

    public void onRefresh() {
        if (this.swipeRefreshContainer != null) {
            this.swipeRefreshContainer.setRefreshing(true);
        }
        if (getListManager() != null) {
            getListManager().refresh();
        }
    }

    public void onNetworkError(VolleyError error) {
        setState(State.ERROR);
        if (this.swipeRefreshContainer != null) {
            this.swipeRefreshContainer.setRefreshing(false);
        }
        C1049M.getMessageBus().post(new AdListNetworkErrorMessage(error));
    }

    public void onListHalted() {
        setState(State.DEFAULT);
    }

    public void onListIsLoading() {
        setState(State.LOADING);
    }

    public void onListIsEmpty() {
        setState(State.EMPTY);
    }

    public void onListIsComplete() {
        setState(State.END_OF_LIST);
    }

    public void onListUpdated() {
        if (this.mHeaderAdapter != null) {
            this.mHeaderAdapter.notifyDataSetChanged();
        }
        if (this.swipeRefreshContainer != null) {
            this.swipeRefreshContainer.setRefreshing(false);
        }
    }

    public void onListIsRefreshing() {
        if (this.swipeRefreshContainer != null) {
            this.swipeRefreshContainer.setRefreshing(true);
        }
        setState(State.REFRESHING);
    }

    public void setAdapter(Adapter<? extends ViewHolder> adapter) {
        this.mHeaderAdapter = new HeaderFooterAdapter(adapter);
        super.setAdapter(this.mHeaderAdapter);
    }

    public Adapter getAdapter() {
        Adapter adapter = super.getAdapter();
        if (adapter == this.mHeaderAdapter) {
            return this.mHeaderAdapter.getDecoratedAdapter();
        }
        return adapter;
    }

    public Bundle getState() {
        if (this.state == null) {
            this.state = new Bundle();
        }
        return this.state;
    }

    public final void scrollTo(int scrollPositionRequested) {
        if (this.mLayoutManager != null && this.mRecyclerView != null && this.mHeaderAdapter != null && this.mHeaderAdapter.getItemCount() > 0) {
            int i;
            if (this.mCurrentHeaderView != null) {
                i = 1;
            } else {
                i = 0;
            }
            this.mLayoutManager.scrollToPositionWithOffset(scrollPositionRequested + i, 0);
        }
    }
}
