package com.schibsted.scm.nextgenapp.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.schibsted.scm.nextgenapp.ui.fragments.delegates.IStatefulFragmentImplementor;
import com.schibsted.scm.nextgenapp.ui.fragments.delegates.StatefulFragmentImplementor;

public abstract class StatefulFragment extends Fragment implements OnFragmentStateTransition, TaggableFragment {
    private IStatefulFragmentImplementor delegate;

    protected StatefulFragment() {
        this.delegate = new StatefulFragmentImplementor(this);
    }

    protected Bundle getState() {
        return this.delegate.getState();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.delegate.onAttach(activity);
    }

    public void onDetach() {
        super.onDetach();
        this.delegate.onDetach();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.delegate.onCreate(savedInstanceState);
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this.delegate.onSaveInstanceState(outState);
    }

    public void setActivityTitle(int resId) {
        setActivityTitle(getText(resId));
    }

    public void setActivityTitle(CharSequence title) {
        Activity activity = getActivity();
        if (activity != null) {
            activity.setTitle(title);
        }
    }
}
