package com.schibsted.scm.nextgenapp.ui.fragments.selectors;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.adapters.RegionSelectTreeAdapter;
import com.schibsted.scm.nextgenapp.backend.bus.messages.ConfigChangedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.ConfigNetworkErrorMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.bus.messages.RegionDataChangeMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.TreeDataRequestMessage;
import com.schibsted.scm.nextgenapp.database.dao.RegionTreeDao;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.submodels.RegionNode;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.squareup.otto.Subscribe;

public class RegionSelectFragment extends TreeSelectFragment {
    public static final String TAG;
    private String deepestLevel;
    private RegionSelectTreeAdapter mRegionSelectionListAdapter;
    protected RegionPathApiModel selectedFromSavedSate;

    static {
        TAG = RegionSelectFragment.class.getSimpleName();
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListShown(false);
    }

    public static RegionSelectFragment newInstance() {
        return new RegionSelectFragment();
    }

    public void onCreate(Bundle savedInstanceState) {
        boolean includeAllRegions;
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            includeAllRegions = args.getBoolean("INCLUDE_ALL", true);
            this.deepestLevel = args.getString("REGION_SELECT_LEVEL");
        } else {
            includeAllRegions = true;
        }
        this.mRegionSelectionListAdapter = new RegionSelectTreeAdapter(includeAllRegions, this);
        setAdapter(this.mRegionSelectionListAdapter);
    }

    @Subscribe
    public void onConfigChanged(ConfigChangedMessage msg) {
        if (msg.getConfig() != null && this.deepestLevel == null) {
            this.deepestLevel = msg.getConfig().getRegionPickerLevel();
            if (TextUtils.isEmpty(this.deepestLevel)) {
                this.deepestLevel = BuildConfig.VERSION_NAME;
            }
        }
        setContentIfReady();
    }

    @Subscribe
    public void onRegionDataChange(RegionDataChangeMessage msg) {
        setContentIfReady();
    }

    private void setContentIfReady() {
        RegionTreeDao regionTreeDao = C1049M.getDaoManager().getRegionTree();
        if (regionTreeDao.isLoaded() && this.deepestLevel != null) {
            setListShown(true);
            this.mRegionSelectionListAdapter.setDeepestLevel(this.deepestLevel);
            this.mRegionSelectionListAdapter.setWholeCountryLabel(regionTreeDao.getAllLabel());
            this.mRegionSelectionListAdapter.setTopLevel(BuildConfig.VERSION_NAME);
            this.mRegionSelectionListAdapter.setCurrentSelection(this.selectedFromSavedSate);
        }
        setTitle();
    }

    protected void deliverResult(Bundle result) {
        super.deliverResult(result);
        trackRegionSelected((RegionPathApiModel) result.getParcelable("RESULT"));
    }

    private void trackRegionSelected(RegionPathApiModel regionPath) {
        EventBuilder eventBuilder = new EventBuilder();
        if (regionPath == null) {
            eventBuilder.setEventType(EventType.LOCATION_CHANGED_BRAZIL);
        } else {
            RegionNode regionNode = regionPath.getRegionNode();
            if (regionNode.getChildren() == null || regionNode.getChildren().length == 0) {
                eventBuilder.setEventType(EventType.LOCATION_CHANGED_STATE).setRegionNode(regionNode);
            } else {
                eventBuilder.setEventType(EventType.LOCATION_CHANGED_REGION).setRegionNode(regionNode);
            }
        }
        C1049M.getMessageBus().post(eventBuilder.build());
    }

    protected View getDefaultFooter(LayoutInflater inflater) {
        return null;
    }

    public void requestScrollTo(int position) {
    }

    public boolean activatePullToRefresh() {
        return false;
    }

    public String getFragmentTag() {
        return TAG;
    }

    @Subscribe
    public void onRegionRequestEvent(TreeDataRequestMessage msg) {
        if (msg.isStarting()) {
            setListShownNoAnimation(true);
        } else if (msg.isStartingAsynchronous()) {
            setListShown(false);
        } else if (msg.hasFinished()) {
            setListShown(true);
        }
    }

    @Subscribe
    public void onNetworkError(ConfigNetworkErrorMessage msg) {
        setListShown(true);
    }

    public void onResume() {
        super.onResume();
        C1049M.getMessageBus().register(this);
    }

    public void onPause() {
        super.onPause();
        C1049M.getMessageBus().unregister(this);
    }

    public void onSaveState(Bundle state) {
        super.onSaveState(state);
        this.mRegionSelectionListAdapter.putSelection(-1, "SELECTED_PARAMETER", state);
    }

    public void onLoadState(Bundle state) {
        super.onLoadState(state);
        if (state.containsKey("SELECTED_PARAMETER")) {
            this.selectedFromSavedSate = (RegionPathApiModel) state.getParcelable("SELECTED_PARAMETER");
        }
    }
}
