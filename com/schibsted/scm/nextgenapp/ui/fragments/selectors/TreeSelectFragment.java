package com.schibsted.scm.nextgenapp.ui.fragments.selectors;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import com.schibsted.scm.nextgenapp.adapters.AbstractSelectTreeAdapter;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import com.urbanairship.C1608R;

public abstract class TreeSelectFragment extends SelectListFragment {
    protected String mTitle;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.selectors.TreeSelectFragment.1 */
    class C15021 implements AnimationListener {
        final /* synthetic */ AbstractSelectTreeAdapter val$adapter;

        C15021(AbstractSelectTreeAdapter abstractSelectTreeAdapter) {
            this.val$adapter = abstractSelectTreeAdapter;
        }

        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
            this.val$adapter.upLevel();
            TreeSelectFragment.this.setTitle();
        }

        public void onAnimationRepeat(Animation animation) {
        }
    }

    public TreeSelectFragment() {
        this.mTitle = this.mDefaultTitle;
    }

    protected void setTitle() {
        String parentTitle = ((AbstractSelectTreeAdapter) getAdapter()).getParentTitle();
        if (TextUtils.isEmpty(parentTitle)) {
            parentTitle = this.mDefaultTitle;
        }
        this.mTitle = parentTitle;
        if (isAdded()) {
            getActivity().setTitle(this.mTitle);
        }
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(this.mTitle);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                levelUp();
                return true;
            case C1608R.id.close /*2131558902*/:
                deliverNone();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        AbstractSelectTreeAdapter adapter = (AbstractSelectTreeAdapter) getAdapter();
        if (adapter.hasDownLevel(position)) {
            animate(false, null);
            adapter.downLevel(view, position, id);
            setTitle();
            return;
        }
        Bundle bundle = new Bundle();
        adapter.putSelection(position, "RESULT", bundle);
        deliverResult(bundle);
    }

    protected void animate(boolean rightToLeft, AnimationListener listener) {
        View view = getView();
        float start = MediaUploadState.IMAGE_PROGRESS_UPLOADED;
        float end = 0.0f;
        if (rightToLeft) {
            start = 0.0f;
            end = MediaUploadState.IMAGE_PROGRESS_UPLOADED;
        }
        TranslateAnimation anim = new TranslateAnimation(2, start, 2, end, 0, 0.0f, 0, 0.0f);
        anim.setDuration((long) getResources().getInteger(2131427344));
        anim.initialize(view.getWidth(), view.getHeight(), view.getWidth(), view.getHeight());
        if (listener != null) {
            anim.setAnimationListener(listener);
        }
        view.startAnimation(anim);
    }

    public String getFragmentTag() {
        return "TreeSelectFragment";
    }

    private void levelUp() {
        AbstractSelectTreeAdapter adapter = (AbstractSelectTreeAdapter) getAdapter();
        if (adapter.hasUpLevel()) {
            animate(true, new C15021(adapter));
            setTitle();
            return;
        }
        deliverNone();
    }
}
