package com.schibsted.scm.nextgenapp.ui.fragments.dialogs;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.facebook.share.internal.ShareConstants;

public class InfoDialogFragment extends FaceliftDialogFragment {
    public static final String TAG;
    private Button mCancel;
    private OnClickListener mCancelClickListener;
    private TextView mMessage;
    private Button mOK;
    private OnClickListener mOKClickListener;
    private TextView mTitle;
    private int negativeText;
    private int positiveText;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.InfoDialogFragment.1 */
    class C14731 implements OnClickListener {
        C14731() {
        }

        public void onClick(View view) {
            InfoDialogFragment.this.dismiss();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.InfoDialogFragment.2 */
    class C14742 implements OnClickListener {
        C14742() {
        }

        public void onClick(View view) {
            InfoDialogFragment.this.dismiss();
        }
    }

    static {
        TAG = InfoDialogFragment.class.getSimpleName();
    }

    public static InfoDialogFragment newInstance(String title, String message, int buttons) {
        InfoDialogFragment f = new InfoDialogFragment();
        Bundle args = new Bundle();
        args.putString(ShareConstants.WEB_DIALOG_PARAM_TITLE, title);
        args.putString(ShareConstants.WEB_DIALOG_PARAM_MESSAGE, message);
        args.putInt("buttons", buttons);
        f.setArguments(args);
        return f;
    }

    public InfoDialogFragment() {
        this.positiveText = 2131165345;
        this.negativeText = 2131165341;
        this.mOKClickListener = new C14731();
        this.mCancelClickListener = new C14742();
    }

    public void setPositiveText(int positiveText) {
        this.positiveText = positiveText;
    }

    public void setNegativeText(int negativeText) {
        this.negativeText = negativeText;
    }

    protected View createContentView(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(2130903120, null);
        this.mOK = (Button) view.findViewById(2131558686);
        this.mCancel = (Button) view.findViewById(2131558685);
        this.mTitle = (TextView) view.findViewById(2131558683);
        this.mMessage = (TextView) view.findViewById(2131558684);
        String title = getArguments().getString(ShareConstants.WEB_DIALOG_PARAM_TITLE);
        String message = getArguments().getString(ShareConstants.WEB_DIALOG_PARAM_MESSAGE);
        this.mTitle.setText(title);
        this.mMessage.setText(message);
        this.mOK.setText(this.positiveText);
        this.mOK.setOnClickListener(this.mOKClickListener);
        this.mCancel.setText(this.negativeText);
        this.mCancel.setOnClickListener(this.mCancelClickListener);
        int buttons = getArguments().getInt("buttons");
        if ((buttons & 1) == 0) {
            this.mOK.setVisibility(8);
        } else if (this.mOKClickListener == null) {
            this.mOK.setOnClickListener(this.mOKClickListener);
        }
        if ((buttons & 2) == 0) {
            this.mCancel.setVisibility(8);
        } else if (this.mCancelClickListener == null) {
            this.mCancel.setOnClickListener(this.mCancelClickListener);
        }
        return view;
    }

    public void setOKClickListener(OnClickListener okListener) {
        this.mOKClickListener = okListener;
        if (this.mOK != null) {
            this.mOK.setOnClickListener(this.mOKClickListener);
        }
    }

    public void setCancelClickListener(OnClickListener okListener) {
        this.mCancelClickListener = okListener;
        if (this.mCancel != null) {
            this.mCancel.setOnClickListener(this.mCancelClickListener);
        }
    }
}
