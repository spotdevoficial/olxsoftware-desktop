package com.schibsted.scm.nextgenapp.ui.fragments.dialogs;

import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.managers.TrafficManagerImpl;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.config.SiteConfig;
import com.schibsted.scm.nextgenapp.config.SiteConfig.TypeOfServer;
import com.schibsted.scm.nextgenapp.ui.views.ErrorView;
import com.schibsted.scm.nextgenapp.ui.views.LabeledEditText;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.urbanairship.C1608R;
import java.util.ArrayList;

public class ToggleServerDialogFragment extends CustomDialogFragment {
    private ErrorView errorView;
    private Button mBack;
    private LabeledEditText mEditText;
    private Spinner mSpinner;
    private Button mToggle;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ToggleServerDialogFragment.1 */
    class C14911 implements OnCheckedChangeListener {
        C14911() {
        }

        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {
                case 2131558709:
                    ToggleServerDialogFragment.this.mSpinner.setEnabled(false);
                    ToggleServerDialogFragment.this.mEditText.setEnabled(false);
                case 2131558710:
                    ToggleServerDialogFragment.this.mSpinner.setEnabled(false);
                    ToggleServerDialogFragment.this.mEditText.setEnabled(false);
                case 2131558711:
                    ToggleServerDialogFragment.this.mSpinner.setEnabled(true);
                    ToggleServerDialogFragment.this.mEditText.setEnabled(true);
                default:
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ToggleServerDialogFragment.2 */
    class C14922 implements OnItemSelectedListener {
        C14922() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            ToggleServerDialogFragment.this.mEditText.setLabel(ToggleServerDialogFragment.this.mSpinner.getSelectedItem().toString());
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ToggleServerDialogFragment.3 */
    class C14933 implements OnClickListener {
        final /* synthetic */ RadioGroup val$group;

        C14933(RadioGroup radioGroup) {
            this.val$group = radioGroup;
        }

        public void onClick(View v) {
            SiteConfig config = ConfigContainer.getConfig();
            String url = null;
            switch (this.val$group.getCheckedRadioButtonId()) {
                case 2131558709:
                    config.setRequestedServer(TypeOfServer.STAGING);
                    config.setCustomServerURL(BuildConfig.VERSION_NAME);
                    break;
                case 2131558710:
                    config.setRequestedServer(TypeOfServer.PRODUCTION);
                    config.setCustomServerURL(BuildConfig.VERSION_NAME);
                    break;
                case 2131558711:
                    url = ToggleServerDialogFragment.this.mSpinner.getSelectedItem().toString() + ToggleServerDialogFragment.this.mEditText.getText();
                    if (!Patterns.WEB_URL.matcher(url).matches()) {
                        ToggleServerDialogFragment.this.errorView.setErrorMessage("Invalid url");
                        break;
                    }
                    config.setRequestedServer(TypeOfServer.CUSTOM);
                    config.setCustomServerURL(url);
                    ToggleServerDialogFragment.this.errorView.clearErrorMessage();
                    break;
            }
            if (this.val$group.getCheckedRadioButtonId() != 2131558711 || Patterns.WEB_URL.matcher(url).matches()) {
                ((TrafficManagerImpl) C1049M.getTrafficManager()).setApiUrl(config.getCustomServerURL());
                Utils.hideSoftKeyboard(ToggleServerDialogFragment.this.getActivity());
                C1049M.getMainAdListManager().refresh();
                ToggleServerDialogFragment.this.dismiss();
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ToggleServerDialogFragment.4 */
    class C14944 implements OnClickListener {
        C14944() {
        }

        public void onClick(View v) {
            Utils.hideSoftKeyboard(ToggleServerDialogFragment.this.getActivity());
            ToggleServerDialogFragment.this.dismiss();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ToggleServerDialogFragment.5 */
    static /* synthetic */ class C14955 {
        static final /* synthetic */ int[] f1291x5ed751b0;

        static {
            f1291x5ed751b0 = new int[TypeOfServer.values().length];
            try {
                f1291x5ed751b0[TypeOfServer.STAGING.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f1291x5ed751b0[TypeOfServer.PRODUCTION.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f1291x5ed751b0[TypeOfServer.CUSTOM.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle("Toggle server");
        setContentView(2130903133);
        setButtonView(2130903134);
        ArrayList<String> connectionsList = new ArrayList();
        connectionsList.add("http://");
        connectionsList.add("https://");
        this.mSpinner = (Spinner) getView().findViewById(2131558712);
        this.mSpinner.setAdapter(new ArrayAdapter(getActivity(), 17367048, connectionsList));
        this.mEditText = (LabeledEditText) getView().findViewById(2131558713);
        String currentURL = ConfigContainer.getConfig().getCustomServerURL();
        this.mEditText.setText(currentURL.isEmpty() ? BuildConfig.VERSION_NAME : currentURL.substring(currentURL.lastIndexOf("//") + 2));
        if (currentURL.startsWith("https://")) {
            this.mSpinner.setSelection(1);
        }
        RadioGroup group = (RadioGroup) getView().findViewById(2131558708);
        switch (C14955.f1291x5ed751b0[ConfigContainer.getConfig().getRequestedServer().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                group.check(2131558709);
                this.mSpinner.setEnabled(false);
                this.mEditText.setEnabled(false);
                break;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                group.check(2131558710);
                this.mSpinner.setEnabled(false);
                this.mEditText.setEnabled(false);
                break;
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                group.check(2131558711);
                this.mSpinner.setEnabled(true);
                this.mEditText.setEnabled(true);
                break;
        }
        group.setOnCheckedChangeListener(new C14911());
        this.mSpinner.setOnItemSelectedListener(new C14922());
        this.errorView = (ErrorView) getView().findViewById(2131558714);
        this.mToggle = (Button) getView().findViewById(2131558715);
        this.mBack = (Button) getView().findViewById(2131558692);
        this.mToggle.setOnClickListener(new C14933(group));
        this.mBack.setOnClickListener(new C14944());
    }
}
