package com.schibsted.scm.nextgenapp.ui.fragments.dialogs;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.models.internal.AuthToken.Type;
import com.schibsted.scm.nextgenapp.models.internal.OneTimeAuthToken;
import com.schibsted.scm.nextgenapp.ui.views.facelift.FaceliftLabelEditText;
import com.schibsted.scm.nextgenapp.utils.Utils;

public class ConfirmEmailDialogFragment extends FaceliftDialogFragment {
    public static final String TAG;
    private Button mActionButton;
    private Button mCancelButton;
    private FaceliftLabelEditText mEmailField;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ConfirmEmailDialogFragment.1 */
    class C14591 implements OnClickListener {
        final /* synthetic */ String val$registrationToken;

        C14591(String str) {
            this.val$registrationToken = str;
        }

        public void onClick(View v) {
            ConfirmEmailDialogFragment.this.mEmailField.hideError();
            if (ConfirmEmailDialogFragment.this.mEmailField.getText().equals(BuildConfig.VERSION_NAME)) {
                ConfirmEmailDialogFragment.this.mEmailField.showError(2131165478);
            } else if (Utils.isValidEmail(ConfirmEmailDialogFragment.this.mEmailField.getText().toString())) {
                C1049M.getAccountManager().createAccount(new OneTimeAuthToken(this.val$registrationToken, Type.REGISTRATION), ConfirmEmailDialogFragment.this.mEmailField.getText().toString(), true);
                ConfirmEmailDialogFragment.this.setLoadingStatus(true);
            } else {
                ConfirmEmailDialogFragment.this.mEmailField.showError(ConfirmEmailDialogFragment.this.getString(2131165477));
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ConfirmEmailDialogFragment.2 */
    class C14602 implements OnClickListener {
        C14602() {
        }

        public void onClick(View v) {
            C1049M.getAccountManager().clearFacebookSession();
            ConfirmEmailDialogFragment.this.dismiss();
        }
    }

    static {
        TAG = ConfirmEmailDialogFragment.class.getSimpleName();
    }

    public static ConfirmEmailDialogFragment newInstance(String initialEmailValue, String registrationToken) {
        ConfirmEmailDialogFragment f = new ConfirmEmailDialogFragment();
        Bundle args = new Bundle();
        args.putString("prefilledEmail", initialEmailValue);
        args.putString("registrationToken", registrationToken);
        f.setArguments(args);
        return f;
    }

    public void onResume() {
        super.onResume();
        if (!(this.mContentView == null || this.mContentView.findViewById(2131558667) == null)) {
            this.mContentView.findViewById(2131558667).setVisibility(8);
            this.mEmailField.setEnabled(true);
            this.mContentView.findViewById(2131558670).setEnabled(true);
        }
        C1049M.getMessageBus().register(this);
    }

    public void onPause() {
        super.onPause();
        C1049M.getMessageBus().unregister(this);
    }

    protected View createContentView(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(2130903115, null);
        this.mActionButton = (Button) view.findViewById(2131558670);
        this.mCancelButton = (Button) view.findViewById(2131558669);
        this.mEmailField = (FaceliftLabelEditText) view.findViewById(2131558668);
        String registrationToken = getArguments().getString("registrationToken");
        this.mEmailField.setText(getArguments().getString("prefilledEmail"));
        this.mEmailField.setSelection(this.mEmailField.getText().length());
        this.mActionButton.setOnClickListener(new C14591(registrationToken));
        this.mCancelButton.setOnClickListener(new C14602());
        return view;
    }

    public void setLoadingStatus(boolean loading) {
        boolean z;
        boolean z2 = true;
        this.mContentView.findViewById(2131558667).setVisibility(loading ? 0 : 8);
        FaceliftLabelEditText faceliftLabelEditText = this.mEmailField;
        if (loading) {
            z = false;
        } else {
            z = true;
        }
        faceliftLabelEditText.setEnabled(z);
        View findViewById = this.mContentView.findViewById(2131558670);
        if (loading) {
            z2 = false;
        }
        findViewById.setEnabled(z2);
    }

    public String getFragmentTag() {
        return TAG;
    }
}
