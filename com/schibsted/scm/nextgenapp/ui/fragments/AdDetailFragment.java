package com.schibsted.scm.nextgenapp.ui.fragments;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.SimpleOnPageChangeListener;
import android.support.v7.appcompat.C0086R;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.VolleyError;
import com.facebook.BuildConfig;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.MainApplication;
import com.schibsted.scm.nextgenapp.RemoteListManagerProvider;
import com.schibsted.scm.nextgenapp.abtest.ABTest;
import com.schibsted.scm.nextgenapp.abtest.ABTestConstants.ABTestMapAdViewVariants;
import com.schibsted.scm.nextgenapp.abtest.ABTestVariant;
import com.schibsted.scm.nextgenapp.activities.FullScreenGalleryActivity;
import com.schibsted.scm.nextgenapp.activities.MapActivity;
import com.schibsted.scm.nextgenapp.adapters.MediaPagerAdapter;
import com.schibsted.scm.nextgenapp.authentication.login.LoginActivity;
import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.bus.messages.MetaDataManagerCompletedMessage;
import com.schibsted.scm.nextgenapp.backend.containers.SearchParametersContainer;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteAdListManager;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteListManager;
import com.schibsted.scm.nextgenapp.backend.managers.list.SavedAdsManager.OperationListener;
import com.schibsted.scm.nextgenapp.backend.network.APIRequest.Builder;
import com.schibsted.scm.nextgenapp.backend.network.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.config.ApiEndpoint;
import com.schibsted.scm.nextgenapp.models.AdDetailsApiModel;
import com.schibsted.scm.nextgenapp.models.AdPhoneNumbersApiModel;
import com.schibsted.scm.nextgenapp.models.ConfigApiModel;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.models.submodels.AdDetailParameter;
import com.schibsted.scm.nextgenapp.models.submodels.AdParameter;
import com.schibsted.scm.nextgenapp.models.submodels.MediaData;
import com.schibsted.scm.nextgenapp.models.submodels.PhoneNumber;
import com.schibsted.scm.nextgenapp.models.submodels.PrivateAd;
import com.schibsted.scm.nextgenapp.models.types.AdLayoutType;
import com.schibsted.scm.nextgenapp.olxchat.listener.OnShowChatListener;
import com.schibsted.scm.nextgenapp.olxchat.model.Chat;
import com.schibsted.scm.nextgenapp.olxchat.model.error.CreateChatError;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.messages.adview.DisplayTelephoneMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.adview.OpenChatButtonClickedMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.adview.OpenEmailButtonClickedMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.adview.TelephoneDisplayedMessage;
import com.schibsted.scm.nextgenapp.ui.CroutonStyle;
import com.schibsted.scm.nextgenapp.ui.DialogCreator;
import com.schibsted.scm.nextgenapp.ui.DialogCreator.DialogButton;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.CallDialog;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.QuestionDialog;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.QuestionDialog.OnAnswerListener;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ReplyToAdDialogFragment;
import com.schibsted.scm.nextgenapp.ui.views.FixedAspectFrameLayout;
import com.schibsted.scm.nextgenapp.ui.views.pageindicator.UnderlinePageIndicator;
import com.schibsted.scm.nextgenapp.utils.DateFormatUtil;
import com.schibsted.scm.nextgenapp.utils.IntentsCreator;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import com.squareup.otto.Subscribe;
import com.urbanairship.C1608R;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class AdDetailFragment extends StatefulFragment {
    public static final String TAG;
    private LinearLayout adExtraParametersView;
    private LinearLayout adMainParametersView;
    private AdDetailsApiModel mAdDetails;
    private TextView mAddress;
    private RelativeLayout mAddressAndMapContainer;
    private String mAddressKey;
    private LinearLayout mAddressLabelContainer;
    private TextView mAddressOnly;
    private LinearLayout mAddressOnlyContainer;
    private RelativeLayout mCallButton;
    private TextView mCallButtonTextView;
    private View mCallSeparator;
    private TextView mCreateTime;
    private TextView mDescription;
    private View mDescriptionSeparator;
    private boolean mHasViewPagerIndexUpdatedProgrammatically;
    private boolean mInSearchMode;
    private UnderlinePageIndicator mIndicator;
    private LayoutInflater mInflater;
    private Double mLatitude;
    private Dialog mLoadingChatProgressDialog;
    private String mLocationLabel;
    private Double mLongitude;
    private View mMapOverlay;
    private FixedAspectFrameLayout mMediaContainer;
    private ViewPager mMediaViewPager;
    private RelativeLayout mMessageButton;
    private ImageView mMessageButtonImageView;
    private TextView mMessageButtonTextView;
    private ImageView mNewBadge;
    private OnAnswerListener mOnAnswerListener;
    private AdPhoneNumbersApiModel mPhoneNumbersApiModel;
    private TextView mPrice;
    private ImageView mProBadge;
    private LinearLayout mReport;
    private MenuItem mSaveAdMenuItem;
    private TextView mSellerName;
    private boolean mShouldOpenChat;
    private TextView mTitle;
    private MenuItem mUnsaveAdMenuItem;
    private int mViewPagerIndex;
    private VolleyError mVolleyError;
    private LinearLayout oneColumnFeaturesWrapper;
    private LinearLayout twoColumnFeaturesWrapper;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.AdDetailFragment.10 */
    class AnonymousClass10 implements OnClickListener {
        final /* synthetic */ Ad val$ad;
        final /* synthetic */ ArrayList val$finalResources;

        AnonymousClass10(ArrayList arrayList, Ad ad) {
            this.val$finalResources = arrayList;
            this.val$ad = ad;
        }

        public void onClick(View v) {
            AdDetailFragment.this.tagClickAdPhoto();
            Intent intent = FullScreenGalleryActivity.newIntent(AdDetailFragment.this.getActivity(), this.val$finalResources, AdDetailFragment.this.mMediaViewPager.getCurrentItem(), false, false, this.val$ad);
            AdDetailFragment.this.getState().putInt("WAITING_FOR_ACTIVITY_RESULT", AdDetailFragment.this.getId());
            AdDetailFragment.this.getActivity().startActivityForResult(intent, 22);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.AdDetailFragment.1 */
    class C14211 implements OnAnswerListener {

        /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.AdDetailFragment.1.1 */
        class C14201 implements OperationListener {
            C14201() {
            }

            public void onError() {
                Crouton.showText(AdDetailFragment.this.getActivity(), 2131165563, CroutonStyle.CONFIRM);
            }

            public void onSuccess() {
                Crouton.showText(AdDetailFragment.this.getActivity(), 2131165555, CroutonStyle.CONFIRM);
                AdDetailFragment.this.getActivity().supportInvalidateOptionsMenu();
            }
        }

        C14211() {
        }

        public void onYes() {
            C1049M.getAccountManager().getSavedAdsManager().remove(AdDetailFragment.this.mAdDetails.ad.publicId, false, new C14201());
        }

        public void onNo() {
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.AdDetailFragment.2 */
    class C14222 implements OperationListener {
        C14222() {
        }

        public void onError() {
            Crouton.showText(AdDetailFragment.this.getActivity(), 2131165562, CroutonStyle.CONFIRM);
        }

        public void onSuccess() {
            Crouton.showText(AdDetailFragment.this.getActivity(), 2131165554, CroutonStyle.CONFIRM);
            AdDetailFragment.this.getActivity().supportInvalidateOptionsMenu();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.AdDetailFragment.3 */
    class C14233 extends ABTestVariant {
        C14233() {
        }

        public void perform() {
            AdDetailFragment.this.displayLocationText();
        }

        public String getId() {
            return ABTestMapAdViewVariants.A1.name();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.AdDetailFragment.4 */
    class C14244 extends ABTestVariant {
        C14244() {
        }

        public void perform() {
            AdDetailFragment.this.displayLocationText();
        }

        public String getId() {
            return ABTestMapAdViewVariants.A2.name();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.AdDetailFragment.5 */
    class C14255 extends ABTestVariant {
        C14255() {
        }

        public void perform() {
            if (AdDetailFragment.this.mLatitude == null || AdDetailFragment.this.mLongitude == null) {
                AdDetailFragment.this.displayLocationText();
                return;
            }
            AdDetailFragment.this.setMap();
            AdDetailFragment.this.displayLocationMap();
        }

        public String getId() {
            return ABTestMapAdViewVariants.B.name();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.AdDetailFragment.6 */
    class C14286 implements OnMapReadyCallback {

        /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.AdDetailFragment.6.1 */
        class C14261 implements OnMapClickListener {
            C14261() {
            }

            public void onMapClick(LatLng latLng) {
                AdDetailFragment.this.tagClickMap();
                AdDetailFragment.this.startMapActivity();
            }
        }

        /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.AdDetailFragment.6.2 */
        class C14272 implements Runnable {
            C14272() {
            }

            public void run() {
                AdDetailFragment.this.mMapOverlay.setVisibility(8);
            }
        }

        C14286() {
        }

        public void onMapReady(GoogleMap map) {
            AdDetailFragment.this.setMapPadding(map);
            LatLng adPosition = new LatLng(AdDetailFragment.this.mLatitude.doubleValue(), AdDetailFragment.this.mLongitude.doubleValue());
            Utils.addCircleToMap(AdDetailFragment.this.getActivity(), map, adPosition);
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(adPosition, 14.0f));
            map.setOnMapClickListener(new C14261());
            new Handler().postDelayed(new C14272(), 750);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.AdDetailFragment.7 */
    class C14297 implements OnClickListener {
        C14297() {
        }

        public void onClick(View v) {
            C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_REPORT_AD).setAdContainer(AdDetailFragment.this.mAdDetails).build());
            if (C1049M.getConfigManager().getConfig() != null) {
                AdDetailFragment.this.startActivity(Intent.createChooser(IntentsCreator.createEmailIntent(C1049M.getConfigManager().getReportAdEmail(), String.format("[id:" + AdDetailFragment.this.mAdDetails.ad.getCleanId() + "] - " + AdDetailFragment.this.getString(2131165648), new Object[]{AdDetailFragment.this.mAdDetails.ad.subject})), AdDetailFragment.this.getString(2131165649)));
                C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.PAGE_REPORT_AD).setAdContainer(AdDetailFragment.this.mAdDetails).build());
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.AdDetailFragment.8 */
    class C14308 implements OnClickListener {
        C14308() {
        }

        public void onClick(View v) {
            String listId = AdDetailFragment.this.mAdDetails.ad.getCleanId();
            if (AdDetailFragment.this.mAdDetails.ad.showChat) {
                C1049M.getMessageBus().post(new OpenChatButtonClickedMessage(listId));
            } else {
                C1049M.getMessageBus().post(new OpenEmailButtonClickedMessage(listId));
            }
            AdDetailFragment.this.onMessage();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.AdDetailFragment.9 */
    class C14319 implements OnClickListener {
        C14319() {
        }

        public void onClick(View v) {
            C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_SEE_TELEPHONE).setAdContainer(AdDetailFragment.this.mAdDetails).build());
            C1049M.getMessageBus().post(new DisplayTelephoneMessage(AdDetailFragment.this.mAdDetails.ad.getCleanId()));
            AdDetailFragment.this.mCallButtonTextView.setText(2131165291);
            AdDetailFragment.this.onCallReply();
        }
    }

    static {
        TAG = AdDetailFragment.class.getSimpleName();
    }

    public AdDetailFragment() {
        this.mViewPagerIndex = 0;
        this.mLatitude = null;
        this.mLongitude = null;
        this.mLocationLabel = BuildConfig.VERSION_NAME;
        this.mOnAnswerListener = new C14211();
    }

    public static AdDetailFragment newInstance(DataModel ad, boolean inSearchMode, boolean shouldOpenChat) {
        AdDetailFragment f = new AdDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("AD_CONTENT", ad);
        bundle.putBoolean("IN_SEARCH_MODE", inSearchMode);
        bundle.putBoolean("OPEN_CHAT", shouldOpenChat);
        f.setArguments(bundle);
        return f;
    }

    public void onResume() {
        super.onResume();
        if (this.mVolleyError != null) {
            Crouton.makeText(getActivity(), 2131165595, CroutonStyle.ALERT).show();
            this.mVolleyError = null;
        }
        if (this.mPhoneNumbersApiModel != null) {
            processOnCallResponse(this.mPhoneNumbersApiModel);
            this.mPhoneNumbersApiModel = null;
        }
        C1049M.getMessageBus().register(this);
        if (getChildFragmentManager() != null) {
            Fragment fragment = getChildFragmentManager().findFragmentByTag(QuestionDialog.TAG);
            if (fragment != null && (fragment instanceof QuestionDialog)) {
                ((QuestionDialog) fragment).setAnswerListener(this.mOnAnswerListener);
            }
        }
        Utils.hideSoftKeyboard(getActivity());
    }

    public void onPause() {
        super.onPause();
        C1049M.getMessageBus().unregister(this);
        dismissLoadingProgressDialog();
    }

    public String getFragmentTag() {
        return TAG;
    }

    public void onPrepareOptionsMenu(Menu menu) {
        boolean z = true;
        this.mSaveAdMenuItem = menu.findItem(2131558911);
        this.mUnsaveAdMenuItem = menu.findItem(2131558912);
        if (this.mSaveAdMenuItem != null && this.mUnsaveAdMenuItem != null) {
            boolean z2;
            int status = C1049M.getConfigManager().getStatus();
            this.mSaveAdMenuItem.setEnabled(status == 3);
            MenuItem menuItem = this.mUnsaveAdMenuItem;
            if (status == 3) {
                z2 = true;
            } else {
                z2 = false;
            }
            menuItem.setEnabled(z2);
            Utils.setMenuItemEnabled(this.mSaveAdMenuItem, getResources().getColor(2131493068));
            Utils.setMenuItemEnabled(this.mUnsaveAdMenuItem, getResources().getColor(2131493068));
            if (C1049M.getAccountManager().isSignedIn()) {
                boolean isSaved = C1049M.getAccountManager().getSavedAdsManager().isSaved(this.mAdDetails.ad.getCleanId());
                MenuItem menuItem2 = this.mSaveAdMenuItem;
                if (isSaved) {
                    z = false;
                }
                menuItem2.setVisible(z);
                this.mUnsaveAdMenuItem.setVisible(isSaved);
            }
            super.onPrepareOptionsMenu(menu);
        }
    }

    @Subscribe
    public void metaDataLoaded(MetaDataManagerCompletedMessage m) {
        boolean z = true;
        if (this.mSaveAdMenuItem != null && this.mUnsaveAdMenuItem != null) {
            boolean z2;
            int status = C1049M.getConfigManager().getStatus();
            MenuItem menuItem = this.mSaveAdMenuItem;
            if (status == 3) {
                z2 = true;
            } else {
                z2 = false;
            }
            menuItem.setEnabled(z2);
            MenuItem menuItem2 = this.mUnsaveAdMenuItem;
            if (status != 3) {
                z = false;
            }
            menuItem2.setEnabled(z);
            Utils.setMenuItemEnabled(this.mSaveAdMenuItem, getResources().getColor(2131493068));
            Utils.setMenuItemEnabled(this.mUnsaveAdMenuItem, getResources().getColor(2131493068));
        }
    }

    public void saveAd() {
        C1049M.getAccountManager().getSavedAdsManager().add(this.mAdDetails, false, new C14222());
    }

    public void unsaveAd() {
        QuestionDialog.newInstance(2131165455, 2131165440, this.mOnAnswerListener).show(getChildFragmentManager(), QuestionDialog.TAG);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                getActivity().finish();
                return true;
            case 2131558911:
                tagClickSaveAd();
                if (C1049M.getAccountManager().isSignedIn()) {
                    saveAd();
                    return true;
                }
                getState().putInt("WAITING_FOR_ACTIVITY_RESULT", getId());
                getActivity().startActivityForResult(LoginActivity.newIntent(getActivity()), 1);
                return true;
            case 2131558912:
                tagClickUnsaveAd();
                unsaveAd();
                return true;
            case 2131558913:
                tagClickShareAd();
                if (this.mAdDetails.ad.shareUrl == null || this.mAdDetails.ad.shareUrl.isEmpty()) {
                    Crouton.showText(getActivity(), 2131165565, CroutonStyle.INFO);
                    return true;
                }
                sendTagEventAdShare();
                try {
                    startActivity(IntentsCreator.createShareAdIntent(getActivity(), this.mAdDetails.ad.subject, this.mAdDetails.ad.shareUrl));
                    return true;
                } catch (ActivityNotFoundException e) {
                    Logger.debug(TAG, "ActivityNotFoundException: action_share_add");
                    return true;
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void sendTagEventAdShare() {
        C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_AD_SHARE).build());
    }

    private void tagClickMap() {
        tagUpsightEvent(EventType.CLICK_AD_MAP);
    }

    private void tagClickSaveAd() {
        tagUpsightEvent(EventType.CLICK_AD_SAVE);
    }

    private void tagClickUnsaveAd() {
        tagUpsightEvent(EventType.CLICK_AD_UNSAVE);
    }

    private void tagClickShareAd() {
        tagUpsightEvent(EventType.CLICK_AD_DETAIL_SHARE);
    }

    private void tagClickAdPhoto() {
        tagUpsightEvent(EventType.CLICK_AD_PHOTO);
    }

    private void tagAdPhotoSwipeRight() {
        tagUpsightEvent(EventType.PAGE_AD_PHOTO_SWIPE_RIGHT);
    }

    private void tagAdPhotoSwipeLeft() {
        tagUpsightEvent(EventType.PAGE_AD_PHOTO_SWIPE_LEFT);
    }

    private void tagCreateChatSuccessfulEvent(String chatId) {
        C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CHAT_CREATE_SUCCESSFUL).setChatId(chatId).setAdContainer(this.mAdDetails).build());
    }

    private void tagCreateChatUnsuccessfulEvent() {
        tagUpsightEvent(EventType.CHAT_CREATE_UNSUCCESSFUL);
    }

    private void tagUpsightEvent(EventType name) {
        C1049M.getMessageBus().post(new EventBuilder().setEventType(name).setAdContainer(this.mAdDetails).build());
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(2130903156, container, false);
        this.mInflater = inflater;
        this.mProBadge = (ImageView) rootView.findViewById(2131558761);
        this.mNewBadge = (ImageView) rootView.findViewById(2131558760);
        this.mTitle = (TextView) rootView.findViewById(C0086R.id.title);
        this.mPrice = (TextView) rootView.findViewById(2131558744);
        this.mMediaViewPager = (ViewPager) rootView.findViewById(2131558742);
        this.mMediaContainer = (FixedAspectFrameLayout) rootView.findViewById(2131558741);
        this.mDescriptionSeparator = rootView.findViewById(2131558755);
        this.mDescription = (TextView) rootView.findViewById(2131558756);
        this.mIndicator = (UnderlinePageIndicator) rootView.findViewById(2131558743);
        this.mCallButton = (RelativeLayout) rootView.findViewById(2131558619);
        this.mReport = (LinearLayout) rootView.findViewById(2131558757);
        this.mMessageButton = (RelativeLayout) rootView.findViewById(2131558623);
        this.adMainParametersView = (LinearLayout) rootView.findViewById(2131558753);
        this.adExtraParametersView = (LinearLayout) rootView.findViewById(2131558754);
        this.mSellerName = (TextView) rootView.findViewById(2131558695);
        this.mCreateTime = (TextView) rootView.findViewById(2131558745);
        this.mAddress = (TextView) rootView.findViewById(2131558749);
        this.mAddressOnly = (TextView) rootView.findViewById(2131558752);
        this.mCallSeparator = rootView.findViewById(2131558622);
        this.mCallButtonTextView = (TextView) rootView.findViewById(2131558621);
        this.mMessageButtonTextView = (TextView) rootView.findViewById(2131558625);
        this.mMessageButtonImageView = (ImageView) rootView.findViewById(2131558624);
        this.mAddressLabelContainer = (LinearLayout) rootView.findViewById(2131558748);
        this.mAddressOnlyContainer = (LinearLayout) rootView.findViewById(2131558751);
        this.mAddressAndMapContainer = (RelativeLayout) rootView.findViewById(2131558746);
        this.mMapOverlay = rootView.findViewById(2131558750);
        setLatitudeAndLongitude();
        configureLocationDisplay();
        this.twoColumnFeaturesWrapper = (LinearLayout) rootView.findViewById(2131558589);
        this.oneColumnFeaturesWrapper = (LinearLayout) rootView.findViewById(2131558587);
        if (C1049M.getConfigManager().getConfig() != null) {
            Map<String, AdLayoutType> layoutDetails = C1049M.getConfigManager().getConfig().getDetailLayoutMap();
            if (layoutDetails != null && layoutDetails.containsValue(AdLayoutType.LOCATION)) {
                for (Entry<String, AdLayoutType> entry : layoutDetails.entrySet()) {
                    if (entry.getValue() == AdLayoutType.LOCATION) {
                        this.mAddressKey = (String) entry.getKey();
                        break;
                    }
                }
            }
        }
        if (this.mAdDetails.ad.phoneHidden != null && this.mAdDetails.ad.phoneHidden.booleanValue()) {
            this.mCallButton.setVisibility(8);
            this.mCallSeparator.setVisibility(8);
        }
        if (this.mAdDetails.ad.showChat) {
            this.mMessageButtonImageView.setImageResource(2130837667);
            this.mMessageButtonTextView.setText(2131165294);
        } else {
            this.mMessageButtonImageView.setImageResource(2130837665);
            this.mMessageButtonTextView.setText(2131165310);
        }
        return rootView;
    }

    private void startMapActivity() {
        Intent intent = new Intent(getActivity(), MapActivity.class);
        intent.putExtra("EXTRA_ADDRESS", this.mLocationLabel);
        intent.putExtra("EXTRA_LATITUDE", this.mLatitude);
        intent.putExtra("EXTRA_LONGITUDE", this.mLongitude);
        startActivity(intent);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void setLatitudeAndLongitude() {
        /*
        r5 = this;
        r4 = 0;
        r3 = "latitude";
        r1 = r5.getSingleAdDetailParameterLabel(r3);
        r3 = "longitude";
        r2 = r5.getSingleAdDetailParameterLabel(r3);
        r3 = java.lang.Double.valueOf(r1);	 Catch:{ NumberFormatException -> 0x001a, NullPointerException -> 0x0020 }
        r5.mLatitude = r3;	 Catch:{ NumberFormatException -> 0x001a, NullPointerException -> 0x0020 }
        r3 = java.lang.Double.valueOf(r2);	 Catch:{ NumberFormatException -> 0x001a, NullPointerException -> 0x0020 }
        r5.mLongitude = r3;	 Catch:{ NumberFormatException -> 0x001a, NullPointerException -> 0x0020 }
    L_0x0019:
        return;
    L_0x001a:
        r0 = move-exception;
    L_0x001b:
        r5.mLatitude = r4;
        r5.mLongitude = r4;
        goto L_0x0019;
    L_0x0020:
        r0 = move-exception;
        goto L_0x001b;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.schibsted.scm.nextgenapp.ui.fragments.AdDetailFragment.setLatitudeAndLongitude():void");
    }

    private void configureLocationDisplay() {
        if (this.mAddressAndMapContainer != null && this.mAddressOnlyContainer != null) {
            ABTest.with(getActivity()).perform("BUY-282", new C14233(), new C14244(), new C14255());
        }
    }

    private void displayLocationMap() {
        this.mAddressAndMapContainer.setVisibility(0);
        this.mAddressOnlyContainer.setVisibility(8);
    }

    private void displayLocationText() {
        this.mAddressAndMapContainer.setVisibility(8);
        this.mAddressOnlyContainer.setVisibility(0);
    }

    private String getSingleAdDetailParameterLabel(String key) {
        if (this.mAdDetails == null || this.mAdDetails.ad == null || this.mAdDetails.ad.adDetails == null || this.mAdDetails.ad.adDetails.get(key) == null || ((AdDetailParameter) this.mAdDetails.ad.adDetails.get(key)).getSingle() == null) {
            return null;
        }
        return ((AdDetailParameter) this.mAdDetails.ad.adDetails.get(key)).getSingle().parameterLabel;
    }

    private void setMap() {
        addMapFragment().getMapAsync(new C14286());
    }

    private SupportMapFragment addMapFragment() {
        GoogleMapOptions options = new GoogleMapOptions();
        options.liteMode(true);
        options.mapToolbarEnabled(false);
        Fragment mapFragment = SupportMapFragment.newInstance(options);
        getChildFragmentManager().beginTransaction().add(2131558747, mapFragment).commit();
        return mapFragment;
    }

    private void setMapPadding(GoogleMap map) {
        MarginLayoutParams layoutParams = (MarginLayoutParams) this.mAddressLabelContainer.getLayoutParams();
        map.setPadding(layoutParams.leftMargin, 0, layoutParams.rightMargin, this.mAddressLabelContainer.getHeight() + layoutParams.bottomMargin);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        int i;
        TextView textView;
        Drawable drawable;
        Map<String, AdDetailParameter> adDetailsCopy;
        Map<String, AdLayoutType> layoutDetails;
        Map<String, List<AdParameter>> oneColumnParameters;
        Map<String, List<AdParameter>> twoColumnParameters;
        List<String> customAdDetails;
        String key;
        AdDetailParameter value;
        AdLayoutType layoutType;
        View entryLayout;
        LinearLayout col1;
        LinearLayout col2;
        int counter;
        int counter2;
        LinearLayout container;
        View text;
        LayoutParams params;
        LinearLayout content;
        View view;
        boolean showAdDetails;
        String title;
        String parameterText;
        RegionPathApiModel region;
        String stateLabel;
        super.onActivityCreated(savedInstanceState);
        setActivityTitle(2131165290);
        ConfigApiModel config = C1049M.getConfigManager().getConfig();
        ImageView imageView = this.mProBadge;
        if (this.mAdDetails.ad.companyAd != null) {
            if (this.mAdDetails.ad.companyAd.booleanValue()) {
                i = 0;
                imageView.setVisibility(i);
                this.mNewBadge.setVisibility(this.mAdDetails.ad.isNew ? 0 : 8);
                this.mTitle.setText(this.mAdDetails.ad.subject);
                if (this.mAdDetails.ad.body == null) {
                    this.mDescription.setText(Html.fromHtml(this.mAdDetails.ad.body.replaceAll("(\\r\\n|\\n\\r|\\r|\\n)", "<br />")));
                } else {
                    this.mDescription.setVisibility(8);
                    this.mDescriptionSeparator.setVisibility(8);
                }
                if (this.mAdDetails.ad.listTime == null) {
                    this.mCreateTime.setText(DateFormatUtil.getDateLabel(getActivity(), this.mAdDetails.ad.listTime.getListTimeInMillis()));
                } else {
                    this.mCreateTime.setVisibility(8);
                }
                if (this.mAdDetails.ad.listPrice != null) {
                    if (!TextUtils.isEmpty(this.mAdDetails.ad.listPrice.priceLabel)) {
                        this.mPrice.setText(this.mAdDetails.ad.listPrice.priceLabel);
                        this.mPrice.setVisibility(0);
                        textView = this.mPrice;
                        if (this.mAdDetails.ad.highlightPrice != null) {
                            if (this.mAdDetails.ad.highlightPrice.booleanValue()) {
                                drawable = getResources().getDrawable(2130837673);
                                textView.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                                adDetailsCopy = new HashMap();
                                if (this.mAdDetails.ad.adDetails != null) {
                                    for (Entry<String, AdDetailParameter> entry : this.mAdDetails.ad.adDetails.entrySet()) {
                                        adDetailsCopy.put(entry.getKey(), ((AdDetailParameter) entry.getValue()).clone());
                                    }
                                }
                                layoutDetails = null;
                                if (config != null) {
                                    layoutDetails = config.getDetailLayoutMap();
                                }
                                if (this.mAddressKey != null) {
                                    if (adDetailsCopy.containsKey(this.mAddressKey)) {
                                        this.mLocationLabel = ((AdDetailParameter) adDetailsCopy.remove(this.mAddressKey)).getSingle().parameterLabel;
                                        if (TextUtils.isEmpty(this.mLocationLabel)) {
                                            this.mAddress.setText(this.mLocationLabel);
                                            this.mAddress.setVisibility(0);
                                        } else {
                                            this.mAddress.setVisibility(8);
                                        }
                                        if (this.mAddressOnly != null) {
                                            this.mAddressOnly.setText(this.mLocationLabel);
                                        }
                                        oneColumnParameters = new HashMap();
                                        twoColumnParameters = new HashMap();
                                        customAdDetails = new ArrayList();
                                        for (Entry<String, AdDetailParameter> entry2 : adDetailsCopy.entrySet()) {
                                            key = (String) entry2.getKey();
                                            value = (AdDetailParameter) entry2.getValue();
                                            if (layoutDetails != null && layoutDetails.containsKey(key)) {
                                                layoutType = (AdLayoutType) layoutDetails.get(key);
                                                if (layoutType != AdLayoutType.TWO_COLUMNS) {
                                                    twoColumnParameters.put(key, value.getMultiple());
                                                    customAdDetails.add(key);
                                                } else if (layoutType != AdLayoutType.ONE_COLUMN) {
                                                    oneColumnParameters.put(key, value.getMultiple());
                                                    customAdDetails.add(key);
                                                }
                                            }
                                        }
                                        adDetailsCopy.keySet().removeAll(customAdDetails);
                                        if (twoColumnParameters.isEmpty()) {
                                            for (Entry<String, List<AdParameter>> entry3 : twoColumnParameters.entrySet()) {
                                                entryLayout = (LinearLayout) this.mInflater.inflate(2130903085, this.twoColumnFeaturesWrapper, false);
                                                if (!this.mAdDetails.labelMap.containsKey(entry3.getKey())) {
                                                    ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry3.getKey())));
                                                    col1 = (LinearLayout) entryLayout.findViewById(2131558590);
                                                    col2 = (LinearLayout) entryLayout.findViewById(2131558591);
                                                    counter = 0;
                                                    for (AdParameter param : (List) entry3.getValue()) {
                                                        counter2 = counter + 1;
                                                        if (counter % 2 != 0) {
                                                            container = col1;
                                                        } else {
                                                            container = col2;
                                                        }
                                                        text = (TextView) this.mInflater.inflate(2130903184, container, false);
                                                        text.setText(param.parameterLabel);
                                                        params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                                        params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                                        text.setLayoutParams(params);
                                                        container.addView(text);
                                                        counter = counter2;
                                                    }
                                                    this.twoColumnFeaturesWrapper.addView(entryLayout);
                                                }
                                            }
                                        } else {
                                            this.twoColumnFeaturesWrapper.setVisibility(8);
                                        }
                                        if (oneColumnParameters.isEmpty()) {
                                            for (Entry<String, List<AdParameter>> entry32 : oneColumnParameters.entrySet()) {
                                                entryLayout = (LinearLayout) this.mInflater.inflate(2130903084, this.oneColumnFeaturesWrapper, false);
                                                if (!this.mAdDetails.labelMap.containsKey(entry32.getKey())) {
                                                    ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry32.getKey())));
                                                    content = (LinearLayout) entryLayout.findViewById(2131558588);
                                                    for (AdParameter param2 : (List) entry32.getValue()) {
                                                        text = (TextView) this.mInflater.inflate(2130903184, content, false);
                                                        text.setText(param2.parameterLabel);
                                                        params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                                        params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                                        text.setLayoutParams(params);
                                                        content.addView(text);
                                                    }
                                                    this.oneColumnFeaturesWrapper.addView(entryLayout);
                                                }
                                            }
                                        } else {
                                            this.oneColumnFeaturesWrapper.setVisibility(8);
                                        }
                                        if (this.mAdDetails.ad.sellerInfo != null) {
                                            if (this.mAdDetails.ad.sellerInfo.getName() != null) {
                                                this.mSellerName.setText(this.mAdDetails.ad.sellerInfo.getName());
                                                this.mSellerName.setVisibility(0);
                                                this.mReport.setOnClickListener(new C14297());
                                                if (this.mAdDetails.ad.category != null) {
                                                    if (this.mAdDetails.labelMap.containsKey("category")) {
                                                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                                                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                                                        this.adMainParametersView.addView(view);
                                                    }
                                                }
                                                if (this.mAdDetails.ad.type != null) {
                                                    if (this.mAdDetails.labelMap.containsKey("type")) {
                                                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                                                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                                                        this.adMainParametersView.addView(view);
                                                    }
                                                }
                                                showAdDetails = false;
                                                if (!adDetailsCopy.isEmpty()) {
                                                    for (Entry<String, AdDetailParameter> entry22 : adDetailsCopy.entrySet()) {
                                                        if (!this.mAdDetails.labelMap.containsKey(entry22.getKey())) {
                                                            showAdDetails = true;
                                                            view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                                            title = String.valueOf(this.mAdDetails.labelMap.get(entry22.getKey()));
                                                            if (title.isEmpty()) {
                                                                ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                                                parameterText = BuildConfig.VERSION_NAME;
                                                                for (AdParameter param22 : ((AdDetailParameter) entry22.getValue()).getMultiple()) {
                                                                    parameterText = parameterText + param22.parameterLabel + "\n";
                                                                }
                                                                ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                                                this.adExtraParametersView.addView(view);
                                                            }
                                                        }
                                                    }
                                                }
                                                if (!showAdDetails) {
                                                    this.adExtraParametersView.setVisibility(8);
                                                }
                                                this.mMessageButton.setOnClickListener(new C14308());
                                                this.mCallButton.setOnClickListener(new C14319());
                                                if (savedInstanceState != null) {
                                                    this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                                                }
                                                initImagePager(this.mAdDetails.ad);
                                                if (!this.mShouldOpenChat) {
                                                    this.mShouldOpenChat = false;
                                                    startChatActivity();
                                                }
                                            }
                                        }
                                        this.mSellerName.setVisibility(8);
                                        this.mReport.setOnClickListener(new C14297());
                                        if (this.mAdDetails.ad.category != null) {
                                            if (this.mAdDetails.labelMap.containsKey("category")) {
                                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                                                this.adMainParametersView.addView(view);
                                            }
                                        }
                                        if (this.mAdDetails.ad.type != null) {
                                            if (this.mAdDetails.labelMap.containsKey("type")) {
                                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                                                this.adMainParametersView.addView(view);
                                            }
                                        }
                                        showAdDetails = false;
                                        if (adDetailsCopy.isEmpty()) {
                                            for (Entry<String, AdDetailParameter> entry222 : adDetailsCopy.entrySet()) {
                                                if (!this.mAdDetails.labelMap.containsKey(entry222.getKey())) {
                                                    showAdDetails = true;
                                                    view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                                    title = String.valueOf(this.mAdDetails.labelMap.get(entry222.getKey()));
                                                    if (title.isEmpty()) {
                                                        ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                                        parameterText = BuildConfig.VERSION_NAME;
                                                        for (AdParameter param222 : ((AdDetailParameter) entry222.getValue()).getMultiple()) {
                                                            parameterText = parameterText + param222.parameterLabel + "\n";
                                                        }
                                                        ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                                        this.adExtraParametersView.addView(view);
                                                    }
                                                }
                                            }
                                        }
                                        if (showAdDetails) {
                                            this.adExtraParametersView.setVisibility(8);
                                        }
                                        this.mMessageButton.setOnClickListener(new C14308());
                                        this.mCallButton.setOnClickListener(new C14319());
                                        if (savedInstanceState != null) {
                                            this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                                        }
                                        initImagePager(this.mAdDetails.ad);
                                        if (!this.mShouldOpenChat) {
                                            this.mShouldOpenChat = false;
                                            startChatActivity();
                                        }
                                    }
                                }
                                region = this.mAdDetails.ad.getRegion();
                                if (region != null) {
                                    this.mLocationLabel = region.getLabel(new String[]{"neighbourhood"}, ", ");
                                    stateLabel = region.getRegionNode().getLabel();
                                    if (!(TextUtils.isEmpty(this.mLocationLabel) || stateLabel == null)) {
                                        this.mLocationLabel += " - " + stateLabel;
                                    }
                                }
                                if (TextUtils.isEmpty(this.mLocationLabel)) {
                                    this.mAddress.setVisibility(8);
                                } else {
                                    this.mAddress.setText(this.mLocationLabel);
                                    this.mAddress.setVisibility(0);
                                }
                                if (this.mAddressOnly != null) {
                                    this.mAddressOnly.setText(this.mLocationLabel);
                                }
                                oneColumnParameters = new HashMap();
                                twoColumnParameters = new HashMap();
                                customAdDetails = new ArrayList();
                                for (Entry<String, AdDetailParameter> entry2222 : adDetailsCopy.entrySet()) {
                                    key = (String) entry2222.getKey();
                                    value = (AdDetailParameter) entry2222.getValue();
                                    layoutType = (AdLayoutType) layoutDetails.get(key);
                                    if (layoutType != AdLayoutType.TWO_COLUMNS) {
                                        twoColumnParameters.put(key, value.getMultiple());
                                        customAdDetails.add(key);
                                    } else if (layoutType != AdLayoutType.ONE_COLUMN) {
                                        oneColumnParameters.put(key, value.getMultiple());
                                        customAdDetails.add(key);
                                    }
                                }
                                adDetailsCopy.keySet().removeAll(customAdDetails);
                                if (twoColumnParameters.isEmpty()) {
                                    this.twoColumnFeaturesWrapper.setVisibility(8);
                                } else {
                                    for (Entry<String, List<AdParameter>> entry322 : twoColumnParameters.entrySet()) {
                                        entryLayout = (LinearLayout) this.mInflater.inflate(2130903085, this.twoColumnFeaturesWrapper, false);
                                        if (!this.mAdDetails.labelMap.containsKey(entry322.getKey())) {
                                            ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry322.getKey())));
                                            col1 = (LinearLayout) entryLayout.findViewById(2131558590);
                                            col2 = (LinearLayout) entryLayout.findViewById(2131558591);
                                            counter = 0;
                                            for (AdParameter param2222 : (List) entry322.getValue()) {
                                                counter2 = counter + 1;
                                                if (counter % 2 != 0) {
                                                    container = col2;
                                                } else {
                                                    container = col1;
                                                }
                                                text = (TextView) this.mInflater.inflate(2130903184, container, false);
                                                text.setText(param2222.parameterLabel);
                                                params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                                params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                                text.setLayoutParams(params);
                                                container.addView(text);
                                                counter = counter2;
                                            }
                                            this.twoColumnFeaturesWrapper.addView(entryLayout);
                                        }
                                    }
                                }
                                if (oneColumnParameters.isEmpty()) {
                                    this.oneColumnFeaturesWrapper.setVisibility(8);
                                } else {
                                    for (Entry<String, List<AdParameter>> entry3222 : oneColumnParameters.entrySet()) {
                                        entryLayout = (LinearLayout) this.mInflater.inflate(2130903084, this.oneColumnFeaturesWrapper, false);
                                        if (!this.mAdDetails.labelMap.containsKey(entry3222.getKey())) {
                                            ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry3222.getKey())));
                                            content = (LinearLayout) entryLayout.findViewById(2131558588);
                                            while (i$.hasNext()) {
                                                text = (TextView) this.mInflater.inflate(2130903184, content, false);
                                                text.setText(param2222.parameterLabel);
                                                params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                                params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                                text.setLayoutParams(params);
                                                content.addView(text);
                                            }
                                            this.oneColumnFeaturesWrapper.addView(entryLayout);
                                        }
                                    }
                                }
                                if (this.mAdDetails.ad.sellerInfo != null) {
                                    if (this.mAdDetails.ad.sellerInfo.getName() != null) {
                                        this.mSellerName.setText(this.mAdDetails.ad.sellerInfo.getName());
                                        this.mSellerName.setVisibility(0);
                                        this.mReport.setOnClickListener(new C14297());
                                        if (this.mAdDetails.ad.category != null) {
                                            if (this.mAdDetails.labelMap.containsKey("category")) {
                                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                                                this.adMainParametersView.addView(view);
                                            }
                                        }
                                        if (this.mAdDetails.ad.type != null) {
                                            if (this.mAdDetails.labelMap.containsKey("type")) {
                                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                                                this.adMainParametersView.addView(view);
                                            }
                                        }
                                        showAdDetails = false;
                                        if (adDetailsCopy.isEmpty()) {
                                            for (Entry<String, AdDetailParameter> entry22222 : adDetailsCopy.entrySet()) {
                                                if (!this.mAdDetails.labelMap.containsKey(entry22222.getKey())) {
                                                    showAdDetails = true;
                                                    view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                                    title = String.valueOf(this.mAdDetails.labelMap.get(entry22222.getKey()));
                                                    if (title.isEmpty()) {
                                                        ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                                        parameterText = BuildConfig.VERSION_NAME;
                                                        for (AdParameter param22222 : ((AdDetailParameter) entry22222.getValue()).getMultiple()) {
                                                            parameterText = parameterText + param22222.parameterLabel + "\n";
                                                        }
                                                        ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                                        this.adExtraParametersView.addView(view);
                                                    }
                                                }
                                            }
                                        }
                                        if (showAdDetails) {
                                            this.adExtraParametersView.setVisibility(8);
                                        }
                                        this.mMessageButton.setOnClickListener(new C14308());
                                        this.mCallButton.setOnClickListener(new C14319());
                                        if (savedInstanceState != null) {
                                            this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                                        }
                                        initImagePager(this.mAdDetails.ad);
                                        if (!this.mShouldOpenChat) {
                                            this.mShouldOpenChat = false;
                                            startChatActivity();
                                        }
                                    }
                                }
                                this.mSellerName.setVisibility(8);
                                this.mReport.setOnClickListener(new C14297());
                                if (this.mAdDetails.ad.category != null) {
                                    if (this.mAdDetails.labelMap.containsKey("category")) {
                                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                                        this.adMainParametersView.addView(view);
                                    }
                                }
                                if (this.mAdDetails.ad.type != null) {
                                    if (this.mAdDetails.labelMap.containsKey("type")) {
                                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                                        this.adMainParametersView.addView(view);
                                    }
                                }
                                showAdDetails = false;
                                if (adDetailsCopy.isEmpty()) {
                                    for (Entry<String, AdDetailParameter> entry222222 : adDetailsCopy.entrySet()) {
                                        if (!this.mAdDetails.labelMap.containsKey(entry222222.getKey())) {
                                            showAdDetails = true;
                                            view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                            title = String.valueOf(this.mAdDetails.labelMap.get(entry222222.getKey()));
                                            if (title.isEmpty()) {
                                                ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                                parameterText = BuildConfig.VERSION_NAME;
                                                for (AdParameter param222222 : ((AdDetailParameter) entry222222.getValue()).getMultiple()) {
                                                    parameterText = parameterText + param222222.parameterLabel + "\n";
                                                }
                                                ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                                this.adExtraParametersView.addView(view);
                                            }
                                        }
                                    }
                                }
                                if (showAdDetails) {
                                    this.adExtraParametersView.setVisibility(8);
                                }
                                this.mMessageButton.setOnClickListener(new C14308());
                                this.mCallButton.setOnClickListener(new C14319());
                                if (savedInstanceState != null) {
                                    this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                                }
                                initImagePager(this.mAdDetails.ad);
                                if (!this.mShouldOpenChat) {
                                    this.mShouldOpenChat = false;
                                    startChatActivity();
                                }
                            }
                        }
                        drawable = null;
                        textView.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                        adDetailsCopy = new HashMap();
                        if (this.mAdDetails.ad.adDetails != null) {
                            for (Entry<String, AdDetailParameter> entry2222222 : this.mAdDetails.ad.adDetails.entrySet()) {
                                adDetailsCopy.put(entry2222222.getKey(), ((AdDetailParameter) entry2222222.getValue()).clone());
                            }
                        }
                        layoutDetails = null;
                        if (config != null) {
                            layoutDetails = config.getDetailLayoutMap();
                        }
                        if (this.mAddressKey != null) {
                            if (adDetailsCopy.containsKey(this.mAddressKey)) {
                                this.mLocationLabel = ((AdDetailParameter) adDetailsCopy.remove(this.mAddressKey)).getSingle().parameterLabel;
                                if (TextUtils.isEmpty(this.mLocationLabel)) {
                                    this.mAddress.setText(this.mLocationLabel);
                                    this.mAddress.setVisibility(0);
                                } else {
                                    this.mAddress.setVisibility(8);
                                }
                                if (this.mAddressOnly != null) {
                                    this.mAddressOnly.setText(this.mLocationLabel);
                                }
                                oneColumnParameters = new HashMap();
                                twoColumnParameters = new HashMap();
                                customAdDetails = new ArrayList();
                                for (Entry<String, AdDetailParameter> entry22222222 : adDetailsCopy.entrySet()) {
                                    key = (String) entry22222222.getKey();
                                    value = (AdDetailParameter) entry22222222.getValue();
                                    layoutType = (AdLayoutType) layoutDetails.get(key);
                                    if (layoutType != AdLayoutType.TWO_COLUMNS) {
                                        twoColumnParameters.put(key, value.getMultiple());
                                        customAdDetails.add(key);
                                    } else if (layoutType != AdLayoutType.ONE_COLUMN) {
                                        oneColumnParameters.put(key, value.getMultiple());
                                        customAdDetails.add(key);
                                    }
                                }
                                adDetailsCopy.keySet().removeAll(customAdDetails);
                                if (twoColumnParameters.isEmpty()) {
                                    for (Entry<String, List<AdParameter>> entry32222 : twoColumnParameters.entrySet()) {
                                        entryLayout = (LinearLayout) this.mInflater.inflate(2130903085, this.twoColumnFeaturesWrapper, false);
                                        if (!this.mAdDetails.labelMap.containsKey(entry32222.getKey())) {
                                            ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry32222.getKey())));
                                            col1 = (LinearLayout) entryLayout.findViewById(2131558590);
                                            col2 = (LinearLayout) entryLayout.findViewById(2131558591);
                                            counter = 0;
                                            for (AdParameter param2222222 : (List) entry32222.getValue()) {
                                                counter2 = counter + 1;
                                                if (counter % 2 != 0) {
                                                    container = col1;
                                                } else {
                                                    container = col2;
                                                }
                                                text = (TextView) this.mInflater.inflate(2130903184, container, false);
                                                text.setText(param2222222.parameterLabel);
                                                params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                                params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                                text.setLayoutParams(params);
                                                container.addView(text);
                                                counter = counter2;
                                            }
                                            this.twoColumnFeaturesWrapper.addView(entryLayout);
                                        }
                                    }
                                } else {
                                    this.twoColumnFeaturesWrapper.setVisibility(8);
                                }
                                if (oneColumnParameters.isEmpty()) {
                                    for (Entry<String, List<AdParameter>> entry322222 : oneColumnParameters.entrySet()) {
                                        entryLayout = (LinearLayout) this.mInflater.inflate(2130903084, this.oneColumnFeaturesWrapper, false);
                                        if (!this.mAdDetails.labelMap.containsKey(entry322222.getKey())) {
                                            ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry322222.getKey())));
                                            content = (LinearLayout) entryLayout.findViewById(2131558588);
                                            while (i$.hasNext()) {
                                                text = (TextView) this.mInflater.inflate(2130903184, content, false);
                                                text.setText(param2222222.parameterLabel);
                                                params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                                params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                                text.setLayoutParams(params);
                                                content.addView(text);
                                            }
                                            this.oneColumnFeaturesWrapper.addView(entryLayout);
                                        }
                                    }
                                } else {
                                    this.oneColumnFeaturesWrapper.setVisibility(8);
                                }
                                if (this.mAdDetails.ad.sellerInfo != null) {
                                    if (this.mAdDetails.ad.sellerInfo.getName() != null) {
                                        this.mSellerName.setText(this.mAdDetails.ad.sellerInfo.getName());
                                        this.mSellerName.setVisibility(0);
                                        this.mReport.setOnClickListener(new C14297());
                                        if (this.mAdDetails.ad.category != null) {
                                            if (this.mAdDetails.labelMap.containsKey("category")) {
                                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                                                this.adMainParametersView.addView(view);
                                            }
                                        }
                                        if (this.mAdDetails.ad.type != null) {
                                            if (this.mAdDetails.labelMap.containsKey("type")) {
                                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                                                this.adMainParametersView.addView(view);
                                            }
                                        }
                                        showAdDetails = false;
                                        if (adDetailsCopy.isEmpty()) {
                                            for (Entry<String, AdDetailParameter> entry222222222 : adDetailsCopy.entrySet()) {
                                                if (!this.mAdDetails.labelMap.containsKey(entry222222222.getKey())) {
                                                    showAdDetails = true;
                                                    view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                                    title = String.valueOf(this.mAdDetails.labelMap.get(entry222222222.getKey()));
                                                    if (title.isEmpty()) {
                                                        ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                                        parameterText = BuildConfig.VERSION_NAME;
                                                        for (AdParameter param22222222 : ((AdDetailParameter) entry222222222.getValue()).getMultiple()) {
                                                            parameterText = parameterText + param22222222.parameterLabel + "\n";
                                                        }
                                                        ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                                        this.adExtraParametersView.addView(view);
                                                    }
                                                }
                                            }
                                        }
                                        if (showAdDetails) {
                                            this.adExtraParametersView.setVisibility(8);
                                        }
                                        this.mMessageButton.setOnClickListener(new C14308());
                                        this.mCallButton.setOnClickListener(new C14319());
                                        if (savedInstanceState != null) {
                                            this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                                        }
                                        initImagePager(this.mAdDetails.ad);
                                        if (!this.mShouldOpenChat) {
                                            this.mShouldOpenChat = false;
                                            startChatActivity();
                                        }
                                    }
                                }
                                this.mSellerName.setVisibility(8);
                                this.mReport.setOnClickListener(new C14297());
                                if (this.mAdDetails.ad.category != null) {
                                    if (this.mAdDetails.labelMap.containsKey("category")) {
                                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                                        this.adMainParametersView.addView(view);
                                    }
                                }
                                if (this.mAdDetails.ad.type != null) {
                                    if (this.mAdDetails.labelMap.containsKey("type")) {
                                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                                        this.adMainParametersView.addView(view);
                                    }
                                }
                                showAdDetails = false;
                                if (adDetailsCopy.isEmpty()) {
                                    for (Entry<String, AdDetailParameter> entry2222222222 : adDetailsCopy.entrySet()) {
                                        if (!this.mAdDetails.labelMap.containsKey(entry2222222222.getKey())) {
                                            showAdDetails = true;
                                            view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                            title = String.valueOf(this.mAdDetails.labelMap.get(entry2222222222.getKey()));
                                            if (title.isEmpty()) {
                                                ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                                parameterText = BuildConfig.VERSION_NAME;
                                                for (AdParameter param222222222 : ((AdDetailParameter) entry2222222222.getValue()).getMultiple()) {
                                                    parameterText = parameterText + param222222222.parameterLabel + "\n";
                                                }
                                                ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                                this.adExtraParametersView.addView(view);
                                            }
                                        }
                                    }
                                }
                                if (showAdDetails) {
                                    this.adExtraParametersView.setVisibility(8);
                                }
                                this.mMessageButton.setOnClickListener(new C14308());
                                this.mCallButton.setOnClickListener(new C14319());
                                if (savedInstanceState != null) {
                                    this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                                }
                                initImagePager(this.mAdDetails.ad);
                                if (!this.mShouldOpenChat) {
                                    this.mShouldOpenChat = false;
                                    startChatActivity();
                                }
                            }
                        }
                        region = this.mAdDetails.ad.getRegion();
                        if (region != null) {
                            this.mLocationLabel = region.getLabel(new String[]{"neighbourhood"}, ", ");
                            stateLabel = region.getRegionNode().getLabel();
                            this.mLocationLabel += " - " + stateLabel;
                        }
                        if (TextUtils.isEmpty(this.mLocationLabel)) {
                            this.mAddress.setVisibility(8);
                        } else {
                            this.mAddress.setText(this.mLocationLabel);
                            this.mAddress.setVisibility(0);
                        }
                        if (this.mAddressOnly != null) {
                            this.mAddressOnly.setText(this.mLocationLabel);
                        }
                        oneColumnParameters = new HashMap();
                        twoColumnParameters = new HashMap();
                        customAdDetails = new ArrayList();
                        for (Entry<String, AdDetailParameter> entry22222222222 : adDetailsCopy.entrySet()) {
                            key = (String) entry22222222222.getKey();
                            value = (AdDetailParameter) entry22222222222.getValue();
                            layoutType = (AdLayoutType) layoutDetails.get(key);
                            if (layoutType != AdLayoutType.TWO_COLUMNS) {
                                twoColumnParameters.put(key, value.getMultiple());
                                customAdDetails.add(key);
                            } else if (layoutType != AdLayoutType.ONE_COLUMN) {
                                oneColumnParameters.put(key, value.getMultiple());
                                customAdDetails.add(key);
                            }
                        }
                        adDetailsCopy.keySet().removeAll(customAdDetails);
                        if (twoColumnParameters.isEmpty()) {
                            this.twoColumnFeaturesWrapper.setVisibility(8);
                        } else {
                            for (Entry<String, List<AdParameter>> entry3222222 : twoColumnParameters.entrySet()) {
                                entryLayout = (LinearLayout) this.mInflater.inflate(2130903085, this.twoColumnFeaturesWrapper, false);
                                if (!this.mAdDetails.labelMap.containsKey(entry3222222.getKey())) {
                                    ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry3222222.getKey())));
                                    col1 = (LinearLayout) entryLayout.findViewById(2131558590);
                                    col2 = (LinearLayout) entryLayout.findViewById(2131558591);
                                    counter = 0;
                                    for (AdParameter param2222222222 : (List) entry3222222.getValue()) {
                                        counter2 = counter + 1;
                                        if (counter % 2 != 0) {
                                            container = col2;
                                        } else {
                                            container = col1;
                                        }
                                        text = (TextView) this.mInflater.inflate(2130903184, container, false);
                                        text.setText(param2222222222.parameterLabel);
                                        params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                        params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                        text.setLayoutParams(params);
                                        container.addView(text);
                                        counter = counter2;
                                    }
                                    this.twoColumnFeaturesWrapper.addView(entryLayout);
                                }
                            }
                        }
                        if (oneColumnParameters.isEmpty()) {
                            this.oneColumnFeaturesWrapper.setVisibility(8);
                        } else {
                            for (Entry<String, List<AdParameter>> entry32222222 : oneColumnParameters.entrySet()) {
                                entryLayout = (LinearLayout) this.mInflater.inflate(2130903084, this.oneColumnFeaturesWrapper, false);
                                if (!this.mAdDetails.labelMap.containsKey(entry32222222.getKey())) {
                                    ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry32222222.getKey())));
                                    content = (LinearLayout) entryLayout.findViewById(2131558588);
                                    while (i$.hasNext()) {
                                        text = (TextView) this.mInflater.inflate(2130903184, content, false);
                                        text.setText(param2222222222.parameterLabel);
                                        params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                        params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                        text.setLayoutParams(params);
                                        content.addView(text);
                                    }
                                    this.oneColumnFeaturesWrapper.addView(entryLayout);
                                }
                            }
                        }
                        if (this.mAdDetails.ad.sellerInfo != null) {
                            if (this.mAdDetails.ad.sellerInfo.getName() != null) {
                                this.mSellerName.setText(this.mAdDetails.ad.sellerInfo.getName());
                                this.mSellerName.setVisibility(0);
                                this.mReport.setOnClickListener(new C14297());
                                if (this.mAdDetails.ad.category != null) {
                                    if (this.mAdDetails.labelMap.containsKey("category")) {
                                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                                        this.adMainParametersView.addView(view);
                                    }
                                }
                                if (this.mAdDetails.ad.type != null) {
                                    if (this.mAdDetails.labelMap.containsKey("type")) {
                                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                                        this.adMainParametersView.addView(view);
                                    }
                                }
                                showAdDetails = false;
                                if (adDetailsCopy.isEmpty()) {
                                    for (Entry<String, AdDetailParameter> entry222222222222 : adDetailsCopy.entrySet()) {
                                        if (!this.mAdDetails.labelMap.containsKey(entry222222222222.getKey())) {
                                            showAdDetails = true;
                                            view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                            title = String.valueOf(this.mAdDetails.labelMap.get(entry222222222222.getKey()));
                                            if (title.isEmpty()) {
                                                ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                                parameterText = BuildConfig.VERSION_NAME;
                                                for (AdParameter param22222222222 : ((AdDetailParameter) entry222222222222.getValue()).getMultiple()) {
                                                    parameterText = parameterText + param22222222222.parameterLabel + "\n";
                                                }
                                                ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                                this.adExtraParametersView.addView(view);
                                            }
                                        }
                                    }
                                }
                                if (showAdDetails) {
                                    this.adExtraParametersView.setVisibility(8);
                                }
                                this.mMessageButton.setOnClickListener(new C14308());
                                this.mCallButton.setOnClickListener(new C14319());
                                if (savedInstanceState != null) {
                                    this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                                }
                                initImagePager(this.mAdDetails.ad);
                                if (!this.mShouldOpenChat) {
                                    this.mShouldOpenChat = false;
                                    startChatActivity();
                                }
                            }
                        }
                        this.mSellerName.setVisibility(8);
                        this.mReport.setOnClickListener(new C14297());
                        if (this.mAdDetails.ad.category != null) {
                            if (this.mAdDetails.labelMap.containsKey("category")) {
                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                                this.adMainParametersView.addView(view);
                            }
                        }
                        if (this.mAdDetails.ad.type != null) {
                            if (this.mAdDetails.labelMap.containsKey("type")) {
                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                                this.adMainParametersView.addView(view);
                            }
                        }
                        showAdDetails = false;
                        if (adDetailsCopy.isEmpty()) {
                            for (Entry<String, AdDetailParameter> entry2222222222222 : adDetailsCopy.entrySet()) {
                                if (!this.mAdDetails.labelMap.containsKey(entry2222222222222.getKey())) {
                                    showAdDetails = true;
                                    view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                    title = String.valueOf(this.mAdDetails.labelMap.get(entry2222222222222.getKey()));
                                    if (title.isEmpty()) {
                                        ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                        parameterText = BuildConfig.VERSION_NAME;
                                        for (AdParameter param222222222222 : ((AdDetailParameter) entry2222222222222.getValue()).getMultiple()) {
                                            parameterText = parameterText + param222222222222.parameterLabel + "\n";
                                        }
                                        ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                        this.adExtraParametersView.addView(view);
                                    }
                                }
                            }
                        }
                        if (showAdDetails) {
                            this.adExtraParametersView.setVisibility(8);
                        }
                        this.mMessageButton.setOnClickListener(new C14308());
                        this.mCallButton.setOnClickListener(new C14319());
                        if (savedInstanceState != null) {
                            this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                        }
                        initImagePager(this.mAdDetails.ad);
                        if (!this.mShouldOpenChat) {
                            this.mShouldOpenChat = false;
                            startChatActivity();
                        }
                    }
                }
                this.mPrice.setVisibility(8);
                adDetailsCopy = new HashMap();
                if (this.mAdDetails.ad.adDetails != null) {
                    for (Entry<String, AdDetailParameter> entry22222222222222 : this.mAdDetails.ad.adDetails.entrySet()) {
                        adDetailsCopy.put(entry22222222222222.getKey(), ((AdDetailParameter) entry22222222222222.getValue()).clone());
                    }
                }
                layoutDetails = null;
                if (config != null) {
                    layoutDetails = config.getDetailLayoutMap();
                }
                if (this.mAddressKey != null) {
                    if (adDetailsCopy.containsKey(this.mAddressKey)) {
                        this.mLocationLabel = ((AdDetailParameter) adDetailsCopy.remove(this.mAddressKey)).getSingle().parameterLabel;
                        if (TextUtils.isEmpty(this.mLocationLabel)) {
                            this.mAddress.setText(this.mLocationLabel);
                            this.mAddress.setVisibility(0);
                        } else {
                            this.mAddress.setVisibility(8);
                        }
                        if (this.mAddressOnly != null) {
                            this.mAddressOnly.setText(this.mLocationLabel);
                        }
                        oneColumnParameters = new HashMap();
                        twoColumnParameters = new HashMap();
                        customAdDetails = new ArrayList();
                        for (Entry<String, AdDetailParameter> entry222222222222222 : adDetailsCopy.entrySet()) {
                            key = (String) entry222222222222222.getKey();
                            value = (AdDetailParameter) entry222222222222222.getValue();
                            layoutType = (AdLayoutType) layoutDetails.get(key);
                            if (layoutType != AdLayoutType.TWO_COLUMNS) {
                                twoColumnParameters.put(key, value.getMultiple());
                                customAdDetails.add(key);
                            } else if (layoutType != AdLayoutType.ONE_COLUMN) {
                                oneColumnParameters.put(key, value.getMultiple());
                                customAdDetails.add(key);
                            }
                        }
                        adDetailsCopy.keySet().removeAll(customAdDetails);
                        if (twoColumnParameters.isEmpty()) {
                            for (Entry<String, List<AdParameter>> entry322222222 : twoColumnParameters.entrySet()) {
                                entryLayout = (LinearLayout) this.mInflater.inflate(2130903085, this.twoColumnFeaturesWrapper, false);
                                if (!this.mAdDetails.labelMap.containsKey(entry322222222.getKey())) {
                                    ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry322222222.getKey())));
                                    col1 = (LinearLayout) entryLayout.findViewById(2131558590);
                                    col2 = (LinearLayout) entryLayout.findViewById(2131558591);
                                    counter = 0;
                                    for (AdParameter param2222222222222 : (List) entry322222222.getValue()) {
                                        counter2 = counter + 1;
                                        if (counter % 2 != 0) {
                                            container = col1;
                                        } else {
                                            container = col2;
                                        }
                                        text = (TextView) this.mInflater.inflate(2130903184, container, false);
                                        text.setText(param2222222222222.parameterLabel);
                                        params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                        params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                        text.setLayoutParams(params);
                                        container.addView(text);
                                        counter = counter2;
                                    }
                                    this.twoColumnFeaturesWrapper.addView(entryLayout);
                                }
                            }
                        } else {
                            this.twoColumnFeaturesWrapper.setVisibility(8);
                        }
                        if (oneColumnParameters.isEmpty()) {
                            for (Entry<String, List<AdParameter>> entry3222222222 : oneColumnParameters.entrySet()) {
                                entryLayout = (LinearLayout) this.mInflater.inflate(2130903084, this.oneColumnFeaturesWrapper, false);
                                if (!this.mAdDetails.labelMap.containsKey(entry3222222222.getKey())) {
                                    ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry3222222222.getKey())));
                                    content = (LinearLayout) entryLayout.findViewById(2131558588);
                                    while (i$.hasNext()) {
                                        text = (TextView) this.mInflater.inflate(2130903184, content, false);
                                        text.setText(param2222222222222.parameterLabel);
                                        params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                        params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                        text.setLayoutParams(params);
                                        content.addView(text);
                                    }
                                    this.oneColumnFeaturesWrapper.addView(entryLayout);
                                }
                            }
                        } else {
                            this.oneColumnFeaturesWrapper.setVisibility(8);
                        }
                        if (this.mAdDetails.ad.sellerInfo != null) {
                            if (this.mAdDetails.ad.sellerInfo.getName() != null) {
                                this.mSellerName.setText(this.mAdDetails.ad.sellerInfo.getName());
                                this.mSellerName.setVisibility(0);
                                this.mReport.setOnClickListener(new C14297());
                                if (this.mAdDetails.ad.category != null) {
                                    if (this.mAdDetails.labelMap.containsKey("category")) {
                                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                                        this.adMainParametersView.addView(view);
                                    }
                                }
                                if (this.mAdDetails.ad.type != null) {
                                    if (this.mAdDetails.labelMap.containsKey("type")) {
                                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                                        this.adMainParametersView.addView(view);
                                    }
                                }
                                showAdDetails = false;
                                if (adDetailsCopy.isEmpty()) {
                                    for (Entry<String, AdDetailParameter> entry2222222222222222 : adDetailsCopy.entrySet()) {
                                        if (!this.mAdDetails.labelMap.containsKey(entry2222222222222222.getKey())) {
                                            showAdDetails = true;
                                            view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                            title = String.valueOf(this.mAdDetails.labelMap.get(entry2222222222222222.getKey()));
                                            if (title.isEmpty()) {
                                                ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                                parameterText = BuildConfig.VERSION_NAME;
                                                for (AdParameter param22222222222222 : ((AdDetailParameter) entry2222222222222222.getValue()).getMultiple()) {
                                                    parameterText = parameterText + param22222222222222.parameterLabel + "\n";
                                                }
                                                ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                                this.adExtraParametersView.addView(view);
                                            }
                                        }
                                    }
                                }
                                if (showAdDetails) {
                                    this.adExtraParametersView.setVisibility(8);
                                }
                                this.mMessageButton.setOnClickListener(new C14308());
                                this.mCallButton.setOnClickListener(new C14319());
                                if (savedInstanceState != null) {
                                    this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                                }
                                initImagePager(this.mAdDetails.ad);
                                if (!this.mShouldOpenChat) {
                                    this.mShouldOpenChat = false;
                                    startChatActivity();
                                }
                            }
                        }
                        this.mSellerName.setVisibility(8);
                        this.mReport.setOnClickListener(new C14297());
                        if (this.mAdDetails.ad.category != null) {
                            if (this.mAdDetails.labelMap.containsKey("category")) {
                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                                this.adMainParametersView.addView(view);
                            }
                        }
                        if (this.mAdDetails.ad.type != null) {
                            if (this.mAdDetails.labelMap.containsKey("type")) {
                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                                this.adMainParametersView.addView(view);
                            }
                        }
                        showAdDetails = false;
                        if (adDetailsCopy.isEmpty()) {
                            for (Entry<String, AdDetailParameter> entry22222222222222222 : adDetailsCopy.entrySet()) {
                                if (!this.mAdDetails.labelMap.containsKey(entry22222222222222222.getKey())) {
                                    showAdDetails = true;
                                    view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                    title = String.valueOf(this.mAdDetails.labelMap.get(entry22222222222222222.getKey()));
                                    if (title.isEmpty()) {
                                        ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                        parameterText = BuildConfig.VERSION_NAME;
                                        for (AdParameter param222222222222222 : ((AdDetailParameter) entry22222222222222222.getValue()).getMultiple()) {
                                            parameterText = parameterText + param222222222222222.parameterLabel + "\n";
                                        }
                                        ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                        this.adExtraParametersView.addView(view);
                                    }
                                }
                            }
                        }
                        if (showAdDetails) {
                            this.adExtraParametersView.setVisibility(8);
                        }
                        this.mMessageButton.setOnClickListener(new C14308());
                        this.mCallButton.setOnClickListener(new C14319());
                        if (savedInstanceState != null) {
                            this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                        }
                        initImagePager(this.mAdDetails.ad);
                        if (!this.mShouldOpenChat) {
                            this.mShouldOpenChat = false;
                            startChatActivity();
                        }
                    }
                }
                region = this.mAdDetails.ad.getRegion();
                if (region != null) {
                    this.mLocationLabel = region.getLabel(new String[]{"neighbourhood"}, ", ");
                    stateLabel = region.getRegionNode().getLabel();
                    this.mLocationLabel += " - " + stateLabel;
                }
                if (TextUtils.isEmpty(this.mLocationLabel)) {
                    this.mAddress.setVisibility(8);
                } else {
                    this.mAddress.setText(this.mLocationLabel);
                    this.mAddress.setVisibility(0);
                }
                if (this.mAddressOnly != null) {
                    this.mAddressOnly.setText(this.mLocationLabel);
                }
                oneColumnParameters = new HashMap();
                twoColumnParameters = new HashMap();
                customAdDetails = new ArrayList();
                for (Entry<String, AdDetailParameter> entry222222222222222222 : adDetailsCopy.entrySet()) {
                    key = (String) entry222222222222222222.getKey();
                    value = (AdDetailParameter) entry222222222222222222.getValue();
                    layoutType = (AdLayoutType) layoutDetails.get(key);
                    if (layoutType != AdLayoutType.TWO_COLUMNS) {
                        twoColumnParameters.put(key, value.getMultiple());
                        customAdDetails.add(key);
                    } else if (layoutType != AdLayoutType.ONE_COLUMN) {
                        oneColumnParameters.put(key, value.getMultiple());
                        customAdDetails.add(key);
                    }
                }
                adDetailsCopy.keySet().removeAll(customAdDetails);
                if (twoColumnParameters.isEmpty()) {
                    this.twoColumnFeaturesWrapper.setVisibility(8);
                } else {
                    for (Entry<String, List<AdParameter>> entry32222222222 : twoColumnParameters.entrySet()) {
                        entryLayout = (LinearLayout) this.mInflater.inflate(2130903085, this.twoColumnFeaturesWrapper, false);
                        if (!this.mAdDetails.labelMap.containsKey(entry32222222222.getKey())) {
                            ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry32222222222.getKey())));
                            col1 = (LinearLayout) entryLayout.findViewById(2131558590);
                            col2 = (LinearLayout) entryLayout.findViewById(2131558591);
                            counter = 0;
                            for (AdParameter param2222222222222222 : (List) entry32222222222.getValue()) {
                                counter2 = counter + 1;
                                if (counter % 2 != 0) {
                                    container = col2;
                                } else {
                                    container = col1;
                                }
                                text = (TextView) this.mInflater.inflate(2130903184, container, false);
                                text.setText(param2222222222222222.parameterLabel);
                                params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                text.setLayoutParams(params);
                                container.addView(text);
                                counter = counter2;
                            }
                            this.twoColumnFeaturesWrapper.addView(entryLayout);
                        }
                    }
                }
                if (oneColumnParameters.isEmpty()) {
                    this.oneColumnFeaturesWrapper.setVisibility(8);
                } else {
                    for (Entry<String, List<AdParameter>> entry322222222222 : oneColumnParameters.entrySet()) {
                        entryLayout = (LinearLayout) this.mInflater.inflate(2130903084, this.oneColumnFeaturesWrapper, false);
                        if (!this.mAdDetails.labelMap.containsKey(entry322222222222.getKey())) {
                            ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry322222222222.getKey())));
                            content = (LinearLayout) entryLayout.findViewById(2131558588);
                            while (i$.hasNext()) {
                                text = (TextView) this.mInflater.inflate(2130903184, content, false);
                                text.setText(param2222222222222222.parameterLabel);
                                params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                text.setLayoutParams(params);
                                content.addView(text);
                            }
                            this.oneColumnFeaturesWrapper.addView(entryLayout);
                        }
                    }
                }
                if (this.mAdDetails.ad.sellerInfo != null) {
                    if (this.mAdDetails.ad.sellerInfo.getName() != null) {
                        this.mSellerName.setText(this.mAdDetails.ad.sellerInfo.getName());
                        this.mSellerName.setVisibility(0);
                        this.mReport.setOnClickListener(new C14297());
                        if (this.mAdDetails.ad.category != null) {
                            if (this.mAdDetails.labelMap.containsKey("category")) {
                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                                this.adMainParametersView.addView(view);
                            }
                        }
                        if (this.mAdDetails.ad.type != null) {
                            if (this.mAdDetails.labelMap.containsKey("type")) {
                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                                this.adMainParametersView.addView(view);
                            }
                        }
                        showAdDetails = false;
                        if (adDetailsCopy.isEmpty()) {
                            for (Entry<String, AdDetailParameter> entry2222222222222222222 : adDetailsCopy.entrySet()) {
                                if (!this.mAdDetails.labelMap.containsKey(entry2222222222222222222.getKey())) {
                                    showAdDetails = true;
                                    view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                    title = String.valueOf(this.mAdDetails.labelMap.get(entry2222222222222222222.getKey()));
                                    if (title.isEmpty()) {
                                        ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                        parameterText = BuildConfig.VERSION_NAME;
                                        for (AdParameter param22222222222222222 : ((AdDetailParameter) entry2222222222222222222.getValue()).getMultiple()) {
                                            parameterText = parameterText + param22222222222222222.parameterLabel + "\n";
                                        }
                                        ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                        this.adExtraParametersView.addView(view);
                                    }
                                }
                            }
                        }
                        if (showAdDetails) {
                            this.adExtraParametersView.setVisibility(8);
                        }
                        this.mMessageButton.setOnClickListener(new C14308());
                        this.mCallButton.setOnClickListener(new C14319());
                        if (savedInstanceState != null) {
                            this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                        }
                        initImagePager(this.mAdDetails.ad);
                        if (!this.mShouldOpenChat) {
                            this.mShouldOpenChat = false;
                            startChatActivity();
                        }
                    }
                }
                this.mSellerName.setVisibility(8);
                this.mReport.setOnClickListener(new C14297());
                if (this.mAdDetails.ad.category != null) {
                    if (this.mAdDetails.labelMap.containsKey("category")) {
                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                        this.adMainParametersView.addView(view);
                    }
                }
                if (this.mAdDetails.ad.type != null) {
                    if (this.mAdDetails.labelMap.containsKey("type")) {
                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                        this.adMainParametersView.addView(view);
                    }
                }
                showAdDetails = false;
                if (adDetailsCopy.isEmpty()) {
                    for (Entry<String, AdDetailParameter> entry22222222222222222222 : adDetailsCopy.entrySet()) {
                        if (!this.mAdDetails.labelMap.containsKey(entry22222222222222222222.getKey())) {
                            showAdDetails = true;
                            view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                            title = String.valueOf(this.mAdDetails.labelMap.get(entry22222222222222222222.getKey()));
                            if (title.isEmpty()) {
                                ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                parameterText = BuildConfig.VERSION_NAME;
                                for (AdParameter param222222222222222222 : ((AdDetailParameter) entry22222222222222222222.getValue()).getMultiple()) {
                                    parameterText = parameterText + param222222222222222222.parameterLabel + "\n";
                                }
                                ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                this.adExtraParametersView.addView(view);
                            }
                        }
                    }
                }
                if (showAdDetails) {
                    this.adExtraParametersView.setVisibility(8);
                }
                this.mMessageButton.setOnClickListener(new C14308());
                this.mCallButton.setOnClickListener(new C14319());
                if (savedInstanceState != null) {
                    this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                }
                initImagePager(this.mAdDetails.ad);
                if (!this.mShouldOpenChat) {
                    this.mShouldOpenChat = false;
                    startChatActivity();
                }
            }
        }
        i = 8;
        imageView.setVisibility(i);
        if (this.mAdDetails.ad.isNew) {
        }
        this.mNewBadge.setVisibility(this.mAdDetails.ad.isNew ? 0 : 8);
        this.mTitle.setText(this.mAdDetails.ad.subject);
        if (this.mAdDetails.ad.body == null) {
            this.mDescription.setVisibility(8);
            this.mDescriptionSeparator.setVisibility(8);
        } else {
            this.mDescription.setText(Html.fromHtml(this.mAdDetails.ad.body.replaceAll("(\\r\\n|\\n\\r|\\r|\\n)", "<br />")));
        }
        if (this.mAdDetails.ad.listTime == null) {
            this.mCreateTime.setVisibility(8);
        } else {
            this.mCreateTime.setText(DateFormatUtil.getDateLabel(getActivity(), this.mAdDetails.ad.listTime.getListTimeInMillis()));
        }
        if (this.mAdDetails.ad.listPrice != null) {
            if (TextUtils.isEmpty(this.mAdDetails.ad.listPrice.priceLabel)) {
                this.mPrice.setText(this.mAdDetails.ad.listPrice.priceLabel);
                this.mPrice.setVisibility(0);
                textView = this.mPrice;
                if (this.mAdDetails.ad.highlightPrice != null) {
                    if (this.mAdDetails.ad.highlightPrice.booleanValue()) {
                        drawable = getResources().getDrawable(2130837673);
                        textView.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                        adDetailsCopy = new HashMap();
                        if (this.mAdDetails.ad.adDetails != null) {
                            for (Entry<String, AdDetailParameter> entry222222222222222222222 : this.mAdDetails.ad.adDetails.entrySet()) {
                                adDetailsCopy.put(entry222222222222222222222.getKey(), ((AdDetailParameter) entry222222222222222222222.getValue()).clone());
                            }
                        }
                        layoutDetails = null;
                        if (config != null) {
                            layoutDetails = config.getDetailLayoutMap();
                        }
                        if (this.mAddressKey != null) {
                            if (adDetailsCopy.containsKey(this.mAddressKey)) {
                                this.mLocationLabel = ((AdDetailParameter) adDetailsCopy.remove(this.mAddressKey)).getSingle().parameterLabel;
                                if (TextUtils.isEmpty(this.mLocationLabel)) {
                                    this.mAddress.setText(this.mLocationLabel);
                                    this.mAddress.setVisibility(0);
                                } else {
                                    this.mAddress.setVisibility(8);
                                }
                                if (this.mAddressOnly != null) {
                                    this.mAddressOnly.setText(this.mLocationLabel);
                                }
                                oneColumnParameters = new HashMap();
                                twoColumnParameters = new HashMap();
                                customAdDetails = new ArrayList();
                                for (Entry<String, AdDetailParameter> entry2222222222222222222222 : adDetailsCopy.entrySet()) {
                                    key = (String) entry2222222222222222222222.getKey();
                                    value = (AdDetailParameter) entry2222222222222222222222.getValue();
                                    layoutType = (AdLayoutType) layoutDetails.get(key);
                                    if (layoutType != AdLayoutType.TWO_COLUMNS) {
                                        twoColumnParameters.put(key, value.getMultiple());
                                        customAdDetails.add(key);
                                    } else if (layoutType != AdLayoutType.ONE_COLUMN) {
                                        oneColumnParameters.put(key, value.getMultiple());
                                        customAdDetails.add(key);
                                    }
                                }
                                adDetailsCopy.keySet().removeAll(customAdDetails);
                                if (twoColumnParameters.isEmpty()) {
                                    for (Entry<String, List<AdParameter>> entry3222222222222 : twoColumnParameters.entrySet()) {
                                        entryLayout = (LinearLayout) this.mInflater.inflate(2130903085, this.twoColumnFeaturesWrapper, false);
                                        if (!this.mAdDetails.labelMap.containsKey(entry3222222222222.getKey())) {
                                            ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry3222222222222.getKey())));
                                            col1 = (LinearLayout) entryLayout.findViewById(2131558590);
                                            col2 = (LinearLayout) entryLayout.findViewById(2131558591);
                                            counter = 0;
                                            for (AdParameter param2222222222222222222 : (List) entry3222222222222.getValue()) {
                                                counter2 = counter + 1;
                                                if (counter % 2 != 0) {
                                                    container = col1;
                                                } else {
                                                    container = col2;
                                                }
                                                text = (TextView) this.mInflater.inflate(2130903184, container, false);
                                                text.setText(param2222222222222222222.parameterLabel);
                                                params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                                params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                                text.setLayoutParams(params);
                                                container.addView(text);
                                                counter = counter2;
                                            }
                                            this.twoColumnFeaturesWrapper.addView(entryLayout);
                                        }
                                    }
                                } else {
                                    this.twoColumnFeaturesWrapper.setVisibility(8);
                                }
                                if (oneColumnParameters.isEmpty()) {
                                    for (Entry<String, List<AdParameter>> entry32222222222222 : oneColumnParameters.entrySet()) {
                                        entryLayout = (LinearLayout) this.mInflater.inflate(2130903084, this.oneColumnFeaturesWrapper, false);
                                        if (!this.mAdDetails.labelMap.containsKey(entry32222222222222.getKey())) {
                                            ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry32222222222222.getKey())));
                                            content = (LinearLayout) entryLayout.findViewById(2131558588);
                                            while (i$.hasNext()) {
                                                text = (TextView) this.mInflater.inflate(2130903184, content, false);
                                                text.setText(param2222222222222222222.parameterLabel);
                                                params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                                params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                                text.setLayoutParams(params);
                                                content.addView(text);
                                            }
                                            this.oneColumnFeaturesWrapper.addView(entryLayout);
                                        }
                                    }
                                } else {
                                    this.oneColumnFeaturesWrapper.setVisibility(8);
                                }
                                if (this.mAdDetails.ad.sellerInfo != null) {
                                    if (this.mAdDetails.ad.sellerInfo.getName() != null) {
                                        this.mSellerName.setText(this.mAdDetails.ad.sellerInfo.getName());
                                        this.mSellerName.setVisibility(0);
                                        this.mReport.setOnClickListener(new C14297());
                                        if (this.mAdDetails.ad.category != null) {
                                            if (this.mAdDetails.labelMap.containsKey("category")) {
                                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                                                this.adMainParametersView.addView(view);
                                            }
                                        }
                                        if (this.mAdDetails.ad.type != null) {
                                            if (this.mAdDetails.labelMap.containsKey("type")) {
                                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                                                this.adMainParametersView.addView(view);
                                            }
                                        }
                                        showAdDetails = false;
                                        if (adDetailsCopy.isEmpty()) {
                                            for (Entry<String, AdDetailParameter> entry22222222222222222222222 : adDetailsCopy.entrySet()) {
                                                if (!this.mAdDetails.labelMap.containsKey(entry22222222222222222222222.getKey())) {
                                                    showAdDetails = true;
                                                    view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                                    title = String.valueOf(this.mAdDetails.labelMap.get(entry22222222222222222222222.getKey()));
                                                    if (title.isEmpty()) {
                                                        ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                                        parameterText = BuildConfig.VERSION_NAME;
                                                        for (AdParameter param22222222222222222222 : ((AdDetailParameter) entry22222222222222222222222.getValue()).getMultiple()) {
                                                            parameterText = parameterText + param22222222222222222222.parameterLabel + "\n";
                                                        }
                                                        ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                                        this.adExtraParametersView.addView(view);
                                                    }
                                                }
                                            }
                                        }
                                        if (showAdDetails) {
                                            this.adExtraParametersView.setVisibility(8);
                                        }
                                        this.mMessageButton.setOnClickListener(new C14308());
                                        this.mCallButton.setOnClickListener(new C14319());
                                        if (savedInstanceState != null) {
                                            this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                                        }
                                        initImagePager(this.mAdDetails.ad);
                                        if (!this.mShouldOpenChat) {
                                            this.mShouldOpenChat = false;
                                            startChatActivity();
                                        }
                                    }
                                }
                                this.mSellerName.setVisibility(8);
                                this.mReport.setOnClickListener(new C14297());
                                if (this.mAdDetails.ad.category != null) {
                                    if (this.mAdDetails.labelMap.containsKey("category")) {
                                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                                        this.adMainParametersView.addView(view);
                                    }
                                }
                                if (this.mAdDetails.ad.type != null) {
                                    if (this.mAdDetails.labelMap.containsKey("type")) {
                                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                                        this.adMainParametersView.addView(view);
                                    }
                                }
                                showAdDetails = false;
                                if (adDetailsCopy.isEmpty()) {
                                    for (Entry<String, AdDetailParameter> entry222222222222222222222222 : adDetailsCopy.entrySet()) {
                                        if (!this.mAdDetails.labelMap.containsKey(entry222222222222222222222222.getKey())) {
                                            showAdDetails = true;
                                            view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                            title = String.valueOf(this.mAdDetails.labelMap.get(entry222222222222222222222222.getKey()));
                                            if (title.isEmpty()) {
                                                ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                                parameterText = BuildConfig.VERSION_NAME;
                                                for (AdParameter param222222222222222222222 : ((AdDetailParameter) entry222222222222222222222222.getValue()).getMultiple()) {
                                                    parameterText = parameterText + param222222222222222222222.parameterLabel + "\n";
                                                }
                                                ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                                this.adExtraParametersView.addView(view);
                                            }
                                        }
                                    }
                                }
                                if (showAdDetails) {
                                    this.adExtraParametersView.setVisibility(8);
                                }
                                this.mMessageButton.setOnClickListener(new C14308());
                                this.mCallButton.setOnClickListener(new C14319());
                                if (savedInstanceState != null) {
                                    this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                                }
                                initImagePager(this.mAdDetails.ad);
                                if (!this.mShouldOpenChat) {
                                    this.mShouldOpenChat = false;
                                    startChatActivity();
                                }
                            }
                        }
                        region = this.mAdDetails.ad.getRegion();
                        if (region != null) {
                            this.mLocationLabel = region.getLabel(new String[]{"neighbourhood"}, ", ");
                            stateLabel = region.getRegionNode().getLabel();
                            this.mLocationLabel += " - " + stateLabel;
                        }
                        if (TextUtils.isEmpty(this.mLocationLabel)) {
                            this.mAddress.setVisibility(8);
                        } else {
                            this.mAddress.setText(this.mLocationLabel);
                            this.mAddress.setVisibility(0);
                        }
                        if (this.mAddressOnly != null) {
                            this.mAddressOnly.setText(this.mLocationLabel);
                        }
                        oneColumnParameters = new HashMap();
                        twoColumnParameters = new HashMap();
                        customAdDetails = new ArrayList();
                        for (Entry<String, AdDetailParameter> entry2222222222222222222222222 : adDetailsCopy.entrySet()) {
                            key = (String) entry2222222222222222222222222.getKey();
                            value = (AdDetailParameter) entry2222222222222222222222222.getValue();
                            layoutType = (AdLayoutType) layoutDetails.get(key);
                            if (layoutType != AdLayoutType.TWO_COLUMNS) {
                                twoColumnParameters.put(key, value.getMultiple());
                                customAdDetails.add(key);
                            } else if (layoutType != AdLayoutType.ONE_COLUMN) {
                                oneColumnParameters.put(key, value.getMultiple());
                                customAdDetails.add(key);
                            }
                        }
                        adDetailsCopy.keySet().removeAll(customAdDetails);
                        if (twoColumnParameters.isEmpty()) {
                            this.twoColumnFeaturesWrapper.setVisibility(8);
                        } else {
                            for (Entry<String, List<AdParameter>> entry322222222222222 : twoColumnParameters.entrySet()) {
                                entryLayout = (LinearLayout) this.mInflater.inflate(2130903085, this.twoColumnFeaturesWrapper, false);
                                if (!this.mAdDetails.labelMap.containsKey(entry322222222222222.getKey())) {
                                    ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry322222222222222.getKey())));
                                    col1 = (LinearLayout) entryLayout.findViewById(2131558590);
                                    col2 = (LinearLayout) entryLayout.findViewById(2131558591);
                                    counter = 0;
                                    for (AdParameter param2222222222222222222222 : (List) entry322222222222222.getValue()) {
                                        counter2 = counter + 1;
                                        if (counter % 2 != 0) {
                                            container = col2;
                                        } else {
                                            container = col1;
                                        }
                                        text = (TextView) this.mInflater.inflate(2130903184, container, false);
                                        text.setText(param2222222222222222222222.parameterLabel);
                                        params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                        params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                        text.setLayoutParams(params);
                                        container.addView(text);
                                        counter = counter2;
                                    }
                                    this.twoColumnFeaturesWrapper.addView(entryLayout);
                                }
                            }
                        }
                        if (oneColumnParameters.isEmpty()) {
                            this.oneColumnFeaturesWrapper.setVisibility(8);
                        } else {
                            for (Entry<String, List<AdParameter>> entry3222222222222222 : oneColumnParameters.entrySet()) {
                                entryLayout = (LinearLayout) this.mInflater.inflate(2130903084, this.oneColumnFeaturesWrapper, false);
                                if (!this.mAdDetails.labelMap.containsKey(entry3222222222222222.getKey())) {
                                    ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry3222222222222222.getKey())));
                                    content = (LinearLayout) entryLayout.findViewById(2131558588);
                                    while (i$.hasNext()) {
                                        text = (TextView) this.mInflater.inflate(2130903184, content, false);
                                        text.setText(param2222222222222222222222.parameterLabel);
                                        params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                        params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                        text.setLayoutParams(params);
                                        content.addView(text);
                                    }
                                    this.oneColumnFeaturesWrapper.addView(entryLayout);
                                }
                            }
                        }
                        if (this.mAdDetails.ad.sellerInfo != null) {
                            if (this.mAdDetails.ad.sellerInfo.getName() != null) {
                                this.mSellerName.setText(this.mAdDetails.ad.sellerInfo.getName());
                                this.mSellerName.setVisibility(0);
                                this.mReport.setOnClickListener(new C14297());
                                if (this.mAdDetails.ad.category != null) {
                                    if (this.mAdDetails.labelMap.containsKey("category")) {
                                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                                        this.adMainParametersView.addView(view);
                                    }
                                }
                                if (this.mAdDetails.ad.type != null) {
                                    if (this.mAdDetails.labelMap.containsKey("type")) {
                                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                                        this.adMainParametersView.addView(view);
                                    }
                                }
                                showAdDetails = false;
                                if (adDetailsCopy.isEmpty()) {
                                    for (Entry<String, AdDetailParameter> entry22222222222222222222222222 : adDetailsCopy.entrySet()) {
                                        if (!this.mAdDetails.labelMap.containsKey(entry22222222222222222222222222.getKey())) {
                                            showAdDetails = true;
                                            view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                            title = String.valueOf(this.mAdDetails.labelMap.get(entry22222222222222222222222222.getKey()));
                                            if (title.isEmpty()) {
                                                ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                                parameterText = BuildConfig.VERSION_NAME;
                                                for (AdParameter param22222222222222222222222 : ((AdDetailParameter) entry22222222222222222222222222.getValue()).getMultiple()) {
                                                    parameterText = parameterText + param22222222222222222222222.parameterLabel + "\n";
                                                }
                                                ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                                this.adExtraParametersView.addView(view);
                                            }
                                        }
                                    }
                                }
                                if (showAdDetails) {
                                    this.adExtraParametersView.setVisibility(8);
                                }
                                this.mMessageButton.setOnClickListener(new C14308());
                                this.mCallButton.setOnClickListener(new C14319());
                                if (savedInstanceState != null) {
                                    this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                                }
                                initImagePager(this.mAdDetails.ad);
                                if (!this.mShouldOpenChat) {
                                    this.mShouldOpenChat = false;
                                    startChatActivity();
                                }
                            }
                        }
                        this.mSellerName.setVisibility(8);
                        this.mReport.setOnClickListener(new C14297());
                        if (this.mAdDetails.ad.category != null) {
                            if (this.mAdDetails.labelMap.containsKey("category")) {
                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                                this.adMainParametersView.addView(view);
                            }
                        }
                        if (this.mAdDetails.ad.type != null) {
                            if (this.mAdDetails.labelMap.containsKey("type")) {
                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                                this.adMainParametersView.addView(view);
                            }
                        }
                        showAdDetails = false;
                        if (adDetailsCopy.isEmpty()) {
                            for (Entry<String, AdDetailParameter> entry222222222222222222222222222 : adDetailsCopy.entrySet()) {
                                if (!this.mAdDetails.labelMap.containsKey(entry222222222222222222222222222.getKey())) {
                                    showAdDetails = true;
                                    view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                    title = String.valueOf(this.mAdDetails.labelMap.get(entry222222222222222222222222222.getKey()));
                                    if (title.isEmpty()) {
                                        ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                        parameterText = BuildConfig.VERSION_NAME;
                                        for (AdParameter param222222222222222222222222 : ((AdDetailParameter) entry222222222222222222222222222.getValue()).getMultiple()) {
                                            parameterText = parameterText + param222222222222222222222222.parameterLabel + "\n";
                                        }
                                        ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                        this.adExtraParametersView.addView(view);
                                    }
                                }
                            }
                        }
                        if (showAdDetails) {
                            this.adExtraParametersView.setVisibility(8);
                        }
                        this.mMessageButton.setOnClickListener(new C14308());
                        this.mCallButton.setOnClickListener(new C14319());
                        if (savedInstanceState != null) {
                            this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                        }
                        initImagePager(this.mAdDetails.ad);
                        if (!this.mShouldOpenChat) {
                            this.mShouldOpenChat = false;
                            startChatActivity();
                        }
                    }
                }
                drawable = null;
                textView.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                adDetailsCopy = new HashMap();
                if (this.mAdDetails.ad.adDetails != null) {
                    for (Entry<String, AdDetailParameter> entry2222222222222222222222222222 : this.mAdDetails.ad.adDetails.entrySet()) {
                        adDetailsCopy.put(entry2222222222222222222222222222.getKey(), ((AdDetailParameter) entry2222222222222222222222222222.getValue()).clone());
                    }
                }
                layoutDetails = null;
                if (config != null) {
                    layoutDetails = config.getDetailLayoutMap();
                }
                if (this.mAddressKey != null) {
                    if (adDetailsCopy.containsKey(this.mAddressKey)) {
                        this.mLocationLabel = ((AdDetailParameter) adDetailsCopy.remove(this.mAddressKey)).getSingle().parameterLabel;
                        if (TextUtils.isEmpty(this.mLocationLabel)) {
                            this.mAddress.setText(this.mLocationLabel);
                            this.mAddress.setVisibility(0);
                        } else {
                            this.mAddress.setVisibility(8);
                        }
                        if (this.mAddressOnly != null) {
                            this.mAddressOnly.setText(this.mLocationLabel);
                        }
                        oneColumnParameters = new HashMap();
                        twoColumnParameters = new HashMap();
                        customAdDetails = new ArrayList();
                        for (Entry<String, AdDetailParameter> entry22222222222222222222222222222 : adDetailsCopy.entrySet()) {
                            key = (String) entry22222222222222222222222222222.getKey();
                            value = (AdDetailParameter) entry22222222222222222222222222222.getValue();
                            layoutType = (AdLayoutType) layoutDetails.get(key);
                            if (layoutType != AdLayoutType.TWO_COLUMNS) {
                                twoColumnParameters.put(key, value.getMultiple());
                                customAdDetails.add(key);
                            } else if (layoutType != AdLayoutType.ONE_COLUMN) {
                                oneColumnParameters.put(key, value.getMultiple());
                                customAdDetails.add(key);
                            }
                        }
                        adDetailsCopy.keySet().removeAll(customAdDetails);
                        if (twoColumnParameters.isEmpty()) {
                            for (Entry<String, List<AdParameter>> entry32222222222222222 : twoColumnParameters.entrySet()) {
                                entryLayout = (LinearLayout) this.mInflater.inflate(2130903085, this.twoColumnFeaturesWrapper, false);
                                if (!this.mAdDetails.labelMap.containsKey(entry32222222222222222.getKey())) {
                                    ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry32222222222222222.getKey())));
                                    col1 = (LinearLayout) entryLayout.findViewById(2131558590);
                                    col2 = (LinearLayout) entryLayout.findViewById(2131558591);
                                    counter = 0;
                                    for (AdParameter param2222222222222222222222222 : (List) entry32222222222222222.getValue()) {
                                        counter2 = counter + 1;
                                        if (counter % 2 != 0) {
                                            container = col1;
                                        } else {
                                            container = col2;
                                        }
                                        text = (TextView) this.mInflater.inflate(2130903184, container, false);
                                        text.setText(param2222222222222222222222222.parameterLabel);
                                        params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                        params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                        text.setLayoutParams(params);
                                        container.addView(text);
                                        counter = counter2;
                                    }
                                    this.twoColumnFeaturesWrapper.addView(entryLayout);
                                }
                            }
                        } else {
                            this.twoColumnFeaturesWrapper.setVisibility(8);
                        }
                        if (oneColumnParameters.isEmpty()) {
                            for (Entry<String, List<AdParameter>> entry322222222222222222 : oneColumnParameters.entrySet()) {
                                entryLayout = (LinearLayout) this.mInflater.inflate(2130903084, this.oneColumnFeaturesWrapper, false);
                                if (!this.mAdDetails.labelMap.containsKey(entry322222222222222222.getKey())) {
                                    ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry322222222222222222.getKey())));
                                    content = (LinearLayout) entryLayout.findViewById(2131558588);
                                    while (i$.hasNext()) {
                                        text = (TextView) this.mInflater.inflate(2130903184, content, false);
                                        text.setText(param2222222222222222222222222.parameterLabel);
                                        params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                        params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                        text.setLayoutParams(params);
                                        content.addView(text);
                                    }
                                    this.oneColumnFeaturesWrapper.addView(entryLayout);
                                }
                            }
                        } else {
                            this.oneColumnFeaturesWrapper.setVisibility(8);
                        }
                        if (this.mAdDetails.ad.sellerInfo != null) {
                            if (this.mAdDetails.ad.sellerInfo.getName() != null) {
                                this.mSellerName.setText(this.mAdDetails.ad.sellerInfo.getName());
                                this.mSellerName.setVisibility(0);
                                this.mReport.setOnClickListener(new C14297());
                                if (this.mAdDetails.ad.category != null) {
                                    if (this.mAdDetails.labelMap.containsKey("category")) {
                                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                                        this.adMainParametersView.addView(view);
                                    }
                                }
                                if (this.mAdDetails.ad.type != null) {
                                    if (this.mAdDetails.labelMap.containsKey("type")) {
                                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                                        this.adMainParametersView.addView(view);
                                    }
                                }
                                showAdDetails = false;
                                if (adDetailsCopy.isEmpty()) {
                                    for (Entry<String, AdDetailParameter> entry222222222222222222222222222222 : adDetailsCopy.entrySet()) {
                                        if (!this.mAdDetails.labelMap.containsKey(entry222222222222222222222222222222.getKey())) {
                                            showAdDetails = true;
                                            view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                            title = String.valueOf(this.mAdDetails.labelMap.get(entry222222222222222222222222222222.getKey()));
                                            if (title.isEmpty()) {
                                                ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                                parameterText = BuildConfig.VERSION_NAME;
                                                for (AdParameter param22222222222222222222222222 : ((AdDetailParameter) entry222222222222222222222222222222.getValue()).getMultiple()) {
                                                    parameterText = parameterText + param22222222222222222222222222.parameterLabel + "\n";
                                                }
                                                ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                                this.adExtraParametersView.addView(view);
                                            }
                                        }
                                    }
                                }
                                if (showAdDetails) {
                                    this.adExtraParametersView.setVisibility(8);
                                }
                                this.mMessageButton.setOnClickListener(new C14308());
                                this.mCallButton.setOnClickListener(new C14319());
                                if (savedInstanceState != null) {
                                    this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                                }
                                initImagePager(this.mAdDetails.ad);
                                if (!this.mShouldOpenChat) {
                                    this.mShouldOpenChat = false;
                                    startChatActivity();
                                }
                            }
                        }
                        this.mSellerName.setVisibility(8);
                        this.mReport.setOnClickListener(new C14297());
                        if (this.mAdDetails.ad.category != null) {
                            if (this.mAdDetails.labelMap.containsKey("category")) {
                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                                this.adMainParametersView.addView(view);
                            }
                        }
                        if (this.mAdDetails.ad.type != null) {
                            if (this.mAdDetails.labelMap.containsKey("type")) {
                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                                this.adMainParametersView.addView(view);
                            }
                        }
                        showAdDetails = false;
                        if (adDetailsCopy.isEmpty()) {
                            for (Entry<String, AdDetailParameter> entry2222222222222222222222222222222 : adDetailsCopy.entrySet()) {
                                if (!this.mAdDetails.labelMap.containsKey(entry2222222222222222222222222222222.getKey())) {
                                    showAdDetails = true;
                                    view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                    title = String.valueOf(this.mAdDetails.labelMap.get(entry2222222222222222222222222222222.getKey()));
                                    if (title.isEmpty()) {
                                        ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                        parameterText = BuildConfig.VERSION_NAME;
                                        for (AdParameter param222222222222222222222222222 : ((AdDetailParameter) entry2222222222222222222222222222222.getValue()).getMultiple()) {
                                            parameterText = parameterText + param222222222222222222222222222.parameterLabel + "\n";
                                        }
                                        ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                        this.adExtraParametersView.addView(view);
                                    }
                                }
                            }
                        }
                        if (showAdDetails) {
                            this.adExtraParametersView.setVisibility(8);
                        }
                        this.mMessageButton.setOnClickListener(new C14308());
                        this.mCallButton.setOnClickListener(new C14319());
                        if (savedInstanceState != null) {
                            this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                        }
                        initImagePager(this.mAdDetails.ad);
                        if (!this.mShouldOpenChat) {
                            this.mShouldOpenChat = false;
                            startChatActivity();
                        }
                    }
                }
                region = this.mAdDetails.ad.getRegion();
                if (region != null) {
                    this.mLocationLabel = region.getLabel(new String[]{"neighbourhood"}, ", ");
                    stateLabel = region.getRegionNode().getLabel();
                    this.mLocationLabel += " - " + stateLabel;
                }
                if (TextUtils.isEmpty(this.mLocationLabel)) {
                    this.mAddress.setVisibility(8);
                } else {
                    this.mAddress.setText(this.mLocationLabel);
                    this.mAddress.setVisibility(0);
                }
                if (this.mAddressOnly != null) {
                    this.mAddressOnly.setText(this.mLocationLabel);
                }
                oneColumnParameters = new HashMap();
                twoColumnParameters = new HashMap();
                customAdDetails = new ArrayList();
                for (Entry<String, AdDetailParameter> entry22222222222222222222222222222222 : adDetailsCopy.entrySet()) {
                    key = (String) entry22222222222222222222222222222222.getKey();
                    value = (AdDetailParameter) entry22222222222222222222222222222222.getValue();
                    layoutType = (AdLayoutType) layoutDetails.get(key);
                    if (layoutType != AdLayoutType.TWO_COLUMNS) {
                        twoColumnParameters.put(key, value.getMultiple());
                        customAdDetails.add(key);
                    } else if (layoutType != AdLayoutType.ONE_COLUMN) {
                        oneColumnParameters.put(key, value.getMultiple());
                        customAdDetails.add(key);
                    }
                }
                adDetailsCopy.keySet().removeAll(customAdDetails);
                if (twoColumnParameters.isEmpty()) {
                    this.twoColumnFeaturesWrapper.setVisibility(8);
                } else {
                    for (Entry<String, List<AdParameter>> entry3222222222222222222 : twoColumnParameters.entrySet()) {
                        entryLayout = (LinearLayout) this.mInflater.inflate(2130903085, this.twoColumnFeaturesWrapper, false);
                        if (!this.mAdDetails.labelMap.containsKey(entry3222222222222222222.getKey())) {
                            ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry3222222222222222222.getKey())));
                            col1 = (LinearLayout) entryLayout.findViewById(2131558590);
                            col2 = (LinearLayout) entryLayout.findViewById(2131558591);
                            counter = 0;
                            for (AdParameter param2222222222222222222222222222 : (List) entry3222222222222222222.getValue()) {
                                counter2 = counter + 1;
                                if (counter % 2 != 0) {
                                    container = col2;
                                } else {
                                    container = col1;
                                }
                                text = (TextView) this.mInflater.inflate(2130903184, container, false);
                                text.setText(param2222222222222222222222222222.parameterLabel);
                                params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                text.setLayoutParams(params);
                                container.addView(text);
                                counter = counter2;
                            }
                            this.twoColumnFeaturesWrapper.addView(entryLayout);
                        }
                    }
                }
                if (oneColumnParameters.isEmpty()) {
                    this.oneColumnFeaturesWrapper.setVisibility(8);
                } else {
                    for (Entry<String, List<AdParameter>> entry32222222222222222222 : oneColumnParameters.entrySet()) {
                        entryLayout = (LinearLayout) this.mInflater.inflate(2130903084, this.oneColumnFeaturesWrapper, false);
                        if (!this.mAdDetails.labelMap.containsKey(entry32222222222222222222.getKey())) {
                            ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry32222222222222222222.getKey())));
                            content = (LinearLayout) entryLayout.findViewById(2131558588);
                            while (i$.hasNext()) {
                                text = (TextView) this.mInflater.inflate(2130903184, content, false);
                                text.setText(param2222222222222222222222222222.parameterLabel);
                                params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                text.setLayoutParams(params);
                                content.addView(text);
                            }
                            this.oneColumnFeaturesWrapper.addView(entryLayout);
                        }
                    }
                }
                if (this.mAdDetails.ad.sellerInfo != null) {
                    if (this.mAdDetails.ad.sellerInfo.getName() != null) {
                        this.mSellerName.setText(this.mAdDetails.ad.sellerInfo.getName());
                        this.mSellerName.setVisibility(0);
                        this.mReport.setOnClickListener(new C14297());
                        if (this.mAdDetails.ad.category != null) {
                            if (this.mAdDetails.labelMap.containsKey("category")) {
                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                                this.adMainParametersView.addView(view);
                            }
                        }
                        if (this.mAdDetails.ad.type != null) {
                            if (this.mAdDetails.labelMap.containsKey("type")) {
                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                                this.adMainParametersView.addView(view);
                            }
                        }
                        showAdDetails = false;
                        if (adDetailsCopy.isEmpty()) {
                            for (Entry<String, AdDetailParameter> entry222222222222222222222222222222222 : adDetailsCopy.entrySet()) {
                                if (!this.mAdDetails.labelMap.containsKey(entry222222222222222222222222222222222.getKey())) {
                                    showAdDetails = true;
                                    view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                    title = String.valueOf(this.mAdDetails.labelMap.get(entry222222222222222222222222222222222.getKey()));
                                    if (title.isEmpty()) {
                                        ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                        parameterText = BuildConfig.VERSION_NAME;
                                        for (AdParameter param22222222222222222222222222222 : ((AdDetailParameter) entry222222222222222222222222222222222.getValue()).getMultiple()) {
                                            parameterText = parameterText + param22222222222222222222222222222.parameterLabel + "\n";
                                        }
                                        ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                        this.adExtraParametersView.addView(view);
                                    }
                                }
                            }
                        }
                        if (showAdDetails) {
                            this.adExtraParametersView.setVisibility(8);
                        }
                        this.mMessageButton.setOnClickListener(new C14308());
                        this.mCallButton.setOnClickListener(new C14319());
                        if (savedInstanceState != null) {
                            this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                        }
                        initImagePager(this.mAdDetails.ad);
                        if (!this.mShouldOpenChat) {
                            this.mShouldOpenChat = false;
                            startChatActivity();
                        }
                    }
                }
                this.mSellerName.setVisibility(8);
                this.mReport.setOnClickListener(new C14297());
                if (this.mAdDetails.ad.category != null) {
                    if (this.mAdDetails.labelMap.containsKey("category")) {
                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                        this.adMainParametersView.addView(view);
                    }
                }
                if (this.mAdDetails.ad.type != null) {
                    if (this.mAdDetails.labelMap.containsKey("type")) {
                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                        this.adMainParametersView.addView(view);
                    }
                }
                showAdDetails = false;
                if (adDetailsCopy.isEmpty()) {
                    for (Entry<String, AdDetailParameter> entry2222222222222222222222222222222222 : adDetailsCopy.entrySet()) {
                        if (!this.mAdDetails.labelMap.containsKey(entry2222222222222222222222222222222222.getKey())) {
                            showAdDetails = true;
                            view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                            title = String.valueOf(this.mAdDetails.labelMap.get(entry2222222222222222222222222222222222.getKey()));
                            if (title.isEmpty()) {
                                ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                parameterText = BuildConfig.VERSION_NAME;
                                for (AdParameter param222222222222222222222222222222 : ((AdDetailParameter) entry2222222222222222222222222222222222.getValue()).getMultiple()) {
                                    parameterText = parameterText + param222222222222222222222222222222.parameterLabel + "\n";
                                }
                                ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                this.adExtraParametersView.addView(view);
                            }
                        }
                    }
                }
                if (showAdDetails) {
                    this.adExtraParametersView.setVisibility(8);
                }
                this.mMessageButton.setOnClickListener(new C14308());
                this.mCallButton.setOnClickListener(new C14319());
                if (savedInstanceState != null) {
                    this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                }
                initImagePager(this.mAdDetails.ad);
                if (!this.mShouldOpenChat) {
                    this.mShouldOpenChat = false;
                    startChatActivity();
                }
            }
        }
        this.mPrice.setVisibility(8);
        adDetailsCopy = new HashMap();
        if (this.mAdDetails.ad.adDetails != null) {
            for (Entry<String, AdDetailParameter> entry22222222222222222222222222222222222 : this.mAdDetails.ad.adDetails.entrySet()) {
                adDetailsCopy.put(entry22222222222222222222222222222222222.getKey(), ((AdDetailParameter) entry22222222222222222222222222222222222.getValue()).clone());
            }
        }
        layoutDetails = null;
        if (config != null) {
            layoutDetails = config.getDetailLayoutMap();
        }
        if (this.mAddressKey != null) {
            if (adDetailsCopy.containsKey(this.mAddressKey)) {
                this.mLocationLabel = ((AdDetailParameter) adDetailsCopy.remove(this.mAddressKey)).getSingle().parameterLabel;
                if (TextUtils.isEmpty(this.mLocationLabel)) {
                    this.mAddress.setText(this.mLocationLabel);
                    this.mAddress.setVisibility(0);
                } else {
                    this.mAddress.setVisibility(8);
                }
                if (this.mAddressOnly != null) {
                    this.mAddressOnly.setText(this.mLocationLabel);
                }
                oneColumnParameters = new HashMap();
                twoColumnParameters = new HashMap();
                customAdDetails = new ArrayList();
                for (Entry<String, AdDetailParameter> entry222222222222222222222222222222222222 : adDetailsCopy.entrySet()) {
                    key = (String) entry222222222222222222222222222222222222.getKey();
                    value = (AdDetailParameter) entry222222222222222222222222222222222222.getValue();
                    layoutType = (AdLayoutType) layoutDetails.get(key);
                    if (layoutType != AdLayoutType.TWO_COLUMNS) {
                        twoColumnParameters.put(key, value.getMultiple());
                        customAdDetails.add(key);
                    } else if (layoutType != AdLayoutType.ONE_COLUMN) {
                        oneColumnParameters.put(key, value.getMultiple());
                        customAdDetails.add(key);
                    }
                }
                adDetailsCopy.keySet().removeAll(customAdDetails);
                if (twoColumnParameters.isEmpty()) {
                    for (Entry<String, List<AdParameter>> entry322222222222222222222 : twoColumnParameters.entrySet()) {
                        entryLayout = (LinearLayout) this.mInflater.inflate(2130903085, this.twoColumnFeaturesWrapper, false);
                        if (!this.mAdDetails.labelMap.containsKey(entry322222222222222222222.getKey())) {
                            ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry322222222222222222222.getKey())));
                            col1 = (LinearLayout) entryLayout.findViewById(2131558590);
                            col2 = (LinearLayout) entryLayout.findViewById(2131558591);
                            counter = 0;
                            for (AdParameter param2222222222222222222222222222222 : (List) entry322222222222222222222.getValue()) {
                                counter2 = counter + 1;
                                if (counter % 2 != 0) {
                                    container = col1;
                                } else {
                                    container = col2;
                                }
                                text = (TextView) this.mInflater.inflate(2130903184, container, false);
                                text.setText(param2222222222222222222222222222222.parameterLabel);
                                params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                text.setLayoutParams(params);
                                container.addView(text);
                                counter = counter2;
                            }
                            this.twoColumnFeaturesWrapper.addView(entryLayout);
                        }
                    }
                } else {
                    this.twoColumnFeaturesWrapper.setVisibility(8);
                }
                if (oneColumnParameters.isEmpty()) {
                    for (Entry<String, List<AdParameter>> entry3222222222222222222222 : oneColumnParameters.entrySet()) {
                        entryLayout = (LinearLayout) this.mInflater.inflate(2130903084, this.oneColumnFeaturesWrapper, false);
                        if (!this.mAdDetails.labelMap.containsKey(entry3222222222222222222222.getKey())) {
                            ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry3222222222222222222222.getKey())));
                            content = (LinearLayout) entryLayout.findViewById(2131558588);
                            while (i$.hasNext()) {
                                text = (TextView) this.mInflater.inflate(2130903184, content, false);
                                text.setText(param2222222222222222222222222222222.parameterLabel);
                                params = (LinearLayout.LayoutParams) text.getLayoutParams();
                                params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                                text.setLayoutParams(params);
                                content.addView(text);
                            }
                            this.oneColumnFeaturesWrapper.addView(entryLayout);
                        }
                    }
                } else {
                    this.oneColumnFeaturesWrapper.setVisibility(8);
                }
                if (this.mAdDetails.ad.sellerInfo != null) {
                    if (this.mAdDetails.ad.sellerInfo.getName() != null) {
                        this.mSellerName.setText(this.mAdDetails.ad.sellerInfo.getName());
                        this.mSellerName.setVisibility(0);
                        this.mReport.setOnClickListener(new C14297());
                        if (this.mAdDetails.ad.category != null) {
                            if (this.mAdDetails.labelMap.containsKey("category")) {
                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                                this.adMainParametersView.addView(view);
                            }
                        }
                        if (this.mAdDetails.ad.type != null) {
                            if (this.mAdDetails.labelMap.containsKey("type")) {
                                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                                this.adMainParametersView.addView(view);
                            }
                        }
                        showAdDetails = false;
                        if (adDetailsCopy.isEmpty()) {
                            for (Entry<String, AdDetailParameter> entry2222222222222222222222222222222222222 : adDetailsCopy.entrySet()) {
                                if (!this.mAdDetails.labelMap.containsKey(entry2222222222222222222222222222222222222.getKey())) {
                                    showAdDetails = true;
                                    view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                                    title = String.valueOf(this.mAdDetails.labelMap.get(entry2222222222222222222222222222222222222.getKey()));
                                    if (title.isEmpty()) {
                                        ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                        parameterText = BuildConfig.VERSION_NAME;
                                        for (AdParameter param22222222222222222222222222222222 : ((AdDetailParameter) entry2222222222222222222222222222222222222.getValue()).getMultiple()) {
                                            parameterText = parameterText + param22222222222222222222222222222222.parameterLabel + "\n";
                                        }
                                        ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                        this.adExtraParametersView.addView(view);
                                    }
                                }
                            }
                        }
                        if (showAdDetails) {
                            this.adExtraParametersView.setVisibility(8);
                        }
                        this.mMessageButton.setOnClickListener(new C14308());
                        this.mCallButton.setOnClickListener(new C14319());
                        if (savedInstanceState != null) {
                            this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                        }
                        initImagePager(this.mAdDetails.ad);
                        if (!this.mShouldOpenChat) {
                            this.mShouldOpenChat = false;
                            startChatActivity();
                        }
                    }
                }
                this.mSellerName.setVisibility(8);
                this.mReport.setOnClickListener(new C14297());
                if (this.mAdDetails.ad.category != null) {
                    if (this.mAdDetails.labelMap.containsKey("category")) {
                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                        this.adMainParametersView.addView(view);
                    }
                }
                if (this.mAdDetails.ad.type != null) {
                    if (this.mAdDetails.labelMap.containsKey("type")) {
                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                        this.adMainParametersView.addView(view);
                    }
                }
                showAdDetails = false;
                if (adDetailsCopy.isEmpty()) {
                    for (Entry<String, AdDetailParameter> entry22222222222222222222222222222222222222 : adDetailsCopy.entrySet()) {
                        if (!this.mAdDetails.labelMap.containsKey(entry22222222222222222222222222222222222222.getKey())) {
                            showAdDetails = true;
                            view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                            title = String.valueOf(this.mAdDetails.labelMap.get(entry22222222222222222222222222222222222222.getKey()));
                            if (title.isEmpty()) {
                                ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                parameterText = BuildConfig.VERSION_NAME;
                                for (AdParameter param222222222222222222222222222222222 : ((AdDetailParameter) entry22222222222222222222222222222222222222.getValue()).getMultiple()) {
                                    parameterText = parameterText + param222222222222222222222222222222222.parameterLabel + "\n";
                                }
                                ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                this.adExtraParametersView.addView(view);
                            }
                        }
                    }
                }
                if (showAdDetails) {
                    this.adExtraParametersView.setVisibility(8);
                }
                this.mMessageButton.setOnClickListener(new C14308());
                this.mCallButton.setOnClickListener(new C14319());
                if (savedInstanceState != null) {
                    this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                }
                initImagePager(this.mAdDetails.ad);
                if (!this.mShouldOpenChat) {
                    this.mShouldOpenChat = false;
                    startChatActivity();
                }
            }
        }
        region = this.mAdDetails.ad.getRegion();
        if (region != null) {
            this.mLocationLabel = region.getLabel(new String[]{"neighbourhood"}, ", ");
            stateLabel = region.getRegionNode().getLabel();
            this.mLocationLabel += " - " + stateLabel;
        }
        if (TextUtils.isEmpty(this.mLocationLabel)) {
            this.mAddress.setVisibility(8);
        } else {
            this.mAddress.setText(this.mLocationLabel);
            this.mAddress.setVisibility(0);
        }
        if (this.mAddressOnly != null) {
            this.mAddressOnly.setText(this.mLocationLabel);
        }
        oneColumnParameters = new HashMap();
        twoColumnParameters = new HashMap();
        customAdDetails = new ArrayList();
        for (Entry<String, AdDetailParameter> entry222222222222222222222222222222222222222 : adDetailsCopy.entrySet()) {
            key = (String) entry222222222222222222222222222222222222222.getKey();
            value = (AdDetailParameter) entry222222222222222222222222222222222222222.getValue();
            layoutType = (AdLayoutType) layoutDetails.get(key);
            if (layoutType != AdLayoutType.TWO_COLUMNS) {
                twoColumnParameters.put(key, value.getMultiple());
                customAdDetails.add(key);
            } else if (layoutType != AdLayoutType.ONE_COLUMN) {
                oneColumnParameters.put(key, value.getMultiple());
                customAdDetails.add(key);
            }
        }
        adDetailsCopy.keySet().removeAll(customAdDetails);
        if (twoColumnParameters.isEmpty()) {
            this.twoColumnFeaturesWrapper.setVisibility(8);
        } else {
            for (Entry<String, List<AdParameter>> entry32222222222222222222222 : twoColumnParameters.entrySet()) {
                entryLayout = (LinearLayout) this.mInflater.inflate(2130903085, this.twoColumnFeaturesWrapper, false);
                if (!this.mAdDetails.labelMap.containsKey(entry32222222222222222222222.getKey())) {
                    ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry32222222222222222222222.getKey())));
                    col1 = (LinearLayout) entryLayout.findViewById(2131558590);
                    col2 = (LinearLayout) entryLayout.findViewById(2131558591);
                    counter = 0;
                    for (AdParameter param2222222222222222222222222222222222 : (List) entry32222222222222222222222.getValue()) {
                        counter2 = counter + 1;
                        if (counter % 2 != 0) {
                            container = col2;
                        } else {
                            container = col1;
                        }
                        text = (TextView) this.mInflater.inflate(2130903184, container, false);
                        text.setText(param2222222222222222222222222222222222.parameterLabel);
                        params = (LinearLayout.LayoutParams) text.getLayoutParams();
                        params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                        text.setLayoutParams(params);
                        container.addView(text);
                        counter = counter2;
                    }
                    this.twoColumnFeaturesWrapper.addView(entryLayout);
                }
            }
        }
        if (oneColumnParameters.isEmpty()) {
            this.oneColumnFeaturesWrapper.setVisibility(8);
        } else {
            for (Entry<String, List<AdParameter>> entry322222222222222222222222 : oneColumnParameters.entrySet()) {
                entryLayout = (LinearLayout) this.mInflater.inflate(2130903084, this.oneColumnFeaturesWrapper, false);
                if (!this.mAdDetails.labelMap.containsKey(entry322222222222222222222222.getKey())) {
                    ((TextView) entryLayout.findViewById(C0086R.id.title)).setText(String.valueOf(this.mAdDetails.labelMap.get(entry322222222222222222222222.getKey())));
                    content = (LinearLayout) entryLayout.findViewById(2131558588);
                    while (i$.hasNext()) {
                        text = (TextView) this.mInflater.inflate(2130903184, content, false);
                        text.setText(param2222222222222222222222222222222222.parameterLabel);
                        params = (LinearLayout.LayoutParams) text.getLayoutParams();
                        params.setMargins(0, 0, 0, (int) getResources().getDimension(2131230866));
                        text.setLayoutParams(params);
                        content.addView(text);
                    }
                    this.oneColumnFeaturesWrapper.addView(entryLayout);
                }
            }
        }
        if (this.mAdDetails.ad.sellerInfo != null) {
            if (this.mAdDetails.ad.sellerInfo.getName() != null) {
                this.mSellerName.setText(this.mAdDetails.ad.sellerInfo.getName());
                this.mSellerName.setVisibility(0);
                this.mReport.setOnClickListener(new C14297());
                if (this.mAdDetails.ad.category != null) {
                    if (this.mAdDetails.labelMap.containsKey("category")) {
                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                        this.adMainParametersView.addView(view);
                    }
                }
                if (this.mAdDetails.ad.type != null) {
                    if (this.mAdDetails.labelMap.containsKey("type")) {
                        view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                        ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                        ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                        this.adMainParametersView.addView(view);
                    }
                }
                showAdDetails = false;
                if (adDetailsCopy.isEmpty()) {
                    for (Entry<String, AdDetailParameter> entry2222222222222222222222222222222222222222 : adDetailsCopy.entrySet()) {
                        if (!this.mAdDetails.labelMap.containsKey(entry2222222222222222222222222222222222222222.getKey())) {
                            showAdDetails = true;
                            view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                            title = String.valueOf(this.mAdDetails.labelMap.get(entry2222222222222222222222222222222222222222.getKey()));
                            if (title.isEmpty()) {
                                ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                                parameterText = BuildConfig.VERSION_NAME;
                                for (AdParameter param22222222222222222222222222222222222 : ((AdDetailParameter) entry2222222222222222222222222222222222222222.getValue()).getMultiple()) {
                                    parameterText = parameterText + param22222222222222222222222222222222222.parameterLabel + "\n";
                                }
                                ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                                this.adExtraParametersView.addView(view);
                            }
                        }
                    }
                }
                if (showAdDetails) {
                    this.adExtraParametersView.setVisibility(8);
                }
                this.mMessageButton.setOnClickListener(new C14308());
                this.mCallButton.setOnClickListener(new C14319());
                if (savedInstanceState != null) {
                    this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
                }
                initImagePager(this.mAdDetails.ad);
                if (!this.mShouldOpenChat) {
                    this.mShouldOpenChat = false;
                    startChatActivity();
                }
            }
        }
        this.mSellerName.setVisibility(8);
        this.mReport.setOnClickListener(new C14297());
        if (this.mAdDetails.ad.category != null) {
            if (this.mAdDetails.labelMap.containsKey("category")) {
                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("category"));
                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.category.label);
                this.adMainParametersView.addView(view);
            }
        }
        if (this.mAdDetails.ad.type != null) {
            if (this.mAdDetails.labelMap.containsKey("type")) {
                view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                ((TextView) view.findViewById(C0086R.id.title)).setText((CharSequence) this.mAdDetails.labelMap.get("type"));
                ((TextView) view.findViewById(2131558756)).setText(this.mAdDetails.ad.type.parameterLabel);
                this.adMainParametersView.addView(view);
            }
        }
        showAdDetails = false;
        if (adDetailsCopy.isEmpty()) {
            for (Entry<String, AdDetailParameter> entry22222222222222222222222222222222222222222 : adDetailsCopy.entrySet()) {
                if (!this.mAdDetails.labelMap.containsKey(entry22222222222222222222222222222222222222222.getKey())) {
                    showAdDetails = true;
                    view = this.mInflater.inflate(2130903179, this.adExtraParametersView, false);
                    title = String.valueOf(this.mAdDetails.labelMap.get(entry22222222222222222222222222222222222222222.getKey()));
                    if (title.isEmpty()) {
                        ((TextView) view.findViewById(C0086R.id.title)).setText(title);
                        parameterText = BuildConfig.VERSION_NAME;
                        for (AdParameter param222222222222222222222222222222222222 : ((AdDetailParameter) entry22222222222222222222222222222222222222222.getValue()).getMultiple()) {
                            parameterText = parameterText + param222222222222222222222222222222222222.parameterLabel + "\n";
                        }
                        ((TextView) view.findViewById(2131558756)).setText(parameterText.substring(0, parameterText.length() - 1));
                        this.adExtraParametersView.addView(view);
                    }
                }
            }
        }
        if (showAdDetails) {
            this.adExtraParametersView.setVisibility(8);
        }
        this.mMessageButton.setOnClickListener(new C14308());
        this.mCallButton.setOnClickListener(new C14319());
        if (savedInstanceState != null) {
            this.mMediaViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR"));
        }
        initImagePager(this.mAdDetails.ad);
        if (!this.mShouldOpenChat) {
            this.mShouldOpenChat = false;
            startChatActivity();
        }
    }

    private void initImagePager(Ad ad) {
        if (ad.mediaList == null || ad.mediaList.size() <= 1) {
            this.mIndicator.setVisibility(8);
        }
        if (ad.mediaList == null || ad.mediaList.size() <= 0) {
            this.mMediaContainer.setVisibility(8);
            return;
        }
        ArrayList<String> resources = new ArrayList(ad.mediaList.size());
        ArrayList<String> galleryResources = new ArrayList(ad.mediaList.size());
        for (MediaData data : ad.mediaList) {
            resources.add(data.getResourceURL(getActivity()));
            galleryResources.add(data.getLargestResourceURL());
        }
        this.mMediaViewPager.setAdapter(new MediaPagerAdapter(getActivity(), resources, new AnonymousClass10(galleryResources, ad)));
        this.mMediaViewPager.setOffscreenPageLimit(1);
        this.mMediaViewPager.setOnTouchListener(new OnTouchListener() {
            private int downX;
            private int downY;
            private int dragThreshold;

            {
                this.dragThreshold = 300;
            }

            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case C1608R.styleable.MapAttrs_mapType /*0*/:
                        this.downX = (int) event.getRawX();
                        this.downY = (int) event.getRawY();
                        break;
                    case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                        int distanceX = Math.abs(((int) event.getRawX()) - this.downX);
                        int distanceY = Math.abs(((int) event.getRawY()) - this.downY);
                        if (distanceY > distanceX && distanceY > this.dragThreshold) {
                            AdDetailFragment.this.mMediaViewPager.getParent().requestDisallowInterceptTouchEvent(true);
                            break;
                        }
                }
                return false;
            }
        });
        this.mIndicator.setViewPager(this.mMediaViewPager);
        this.mIndicator.setOnPageChangeListener(new SimpleOnPageChangeListener() {
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                if (!AdDetailFragment.this.mHasViewPagerIndexUpdatedProgrammatically) {
                    if (AdDetailFragment.this.mViewPagerIndex > position) {
                        AdDetailFragment.this.tagAdPhotoSwipeLeft();
                    } else if (AdDetailFragment.this.mViewPagerIndex < position) {
                        AdDetailFragment.this.tagAdPhotoSwipeRight();
                    }
                }
                AdDetailFragment.this.mHasViewPagerIndexUpdatedProgrammatically = false;
                AdDetailFragment.this.mViewPagerIndex = position;
            }
        });
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(2131623939, menu);
    }

    private void onCallReply() {
        performGetAdPhoneNumberRequest(new OnNetworkResponseListener<AdPhoneNumbersApiModel>() {
            public void onErrorResponse(VolleyError volleyError) {
                AdDetailFragment.this.onGetAdPhoneNumberError(volleyError);
            }

            public void onResponse(AdPhoneNumbersApiModel model) {
                if (AdDetailFragment.this.isResumed()) {
                    AdDetailFragment.this.processOnCallResponse(model);
                } else {
                    AdDetailFragment.this.mPhoneNumbersApiModel = model;
                }
                AdDetailFragment.this.mCallButtonTextView.setText(2131165292);
            }
        });
    }

    private void onGetAdPhoneNumberError(VolleyError volleyError) {
        if (isResumed()) {
            Crouton.makeText(getActivity(), 2131165595, CroutonStyle.ALERT).show();
        } else {
            this.mVolleyError = volleyError;
        }
        this.mCallButtonTextView.setText(2131165292);
    }

    private void performGetAdPhoneNumberRequest(OnNetworkResponseListener listener) {
        C1049M.getTrafficManager().doRequest(new Builder().requestId(TAG).endpoint(ApiEndpoint.GET_AD_PHONE_NUMBER).parameter("ad_id", this.mAdDetails.ad.getCleanId()).cancelSameRequests(true).listener(listener).build());
    }

    private void processOnCallResponse(AdPhoneNumbersApiModel model) {
        PhoneNumber phoneNumber = (PhoneNumber) model.phones.get(0);
        CallDialog.newInstance(this.mAdDetails, phoneNumber.label, phoneNumber.value, phoneNumber.sms.booleanValue(), isInSearchMode()).show(getChildFragmentManager(), "dialog");
        MessageBus messageBus = C1049M.getMessageBus();
        messageBus.post(new EventBuilder().setEventType(EventType.PAGE_OPTIONS_MOBILE).setAdContainer(this.mAdDetails).build());
        messageBus.post(new TelephoneDisplayedMessage(this.mAdDetails.ad.getCleanId()));
    }

    private void onMessage() {
        if (!this.mAdDetails.ad.showChat) {
            EventType eventType = isInSearchMode() ? EventType.SELECT_REPLY_EMAIL_SEARCH : EventType.CLICK_SEND_EMAIL_VIEW;
            openReplyToAdDialog();
            C1049M.getMessageBus().post(new EventBuilder().setEventType(eventType).setAdContainer(this.mAdDetails).setSearchParametersContainer(getSearchParametersContainer()).build());
        } else if (!C1049M.getAccountManager().isSignedIn()) {
            startSignInActivity();
        } else if (C1049M.getAccountManager().getAccountApiModel().ownsAd(this.mAdDetails.getAd())) {
            showAdAuthorIsUserDialog();
        } else {
            startChatActivity();
        }
    }

    private void startChatActivity() {
        ((MainApplication) getActivity().getApplication()).getOlxChatInstance().showChat(getActivity(), Integer.parseInt(this.mAdDetails.getAd().getCleanId()), new OnShowChatListener() {
            public void onChatFetchingStarted() {
                AdDetailFragment.this.showLoadingChatProgressDialog();
            }

            public void onChatFetchingSuccessful(Chat chat) {
                AdDetailFragment.this.tagCreateChatSuccessfulEvent(chat.getId());
                AdDetailFragment.this.dismissLoadingProgressDialog();
            }

            public void onChatFetchError(CreateChatError error) {
                int errorMessageRes;
                AdDetailFragment.this.tagCreateChatUnsuccessfulEvent();
                AdDetailFragment.this.dismissLoadingProgressDialog();
                if (error == CreateChatError.AD_DATA_ACCESS_ERROR) {
                    errorMessageRes = 2131165353;
                } else {
                    errorMessageRes = 2131165354;
                }
                if (AdDetailFragment.this.getView() != null) {
                    Snackbar.make(AdDetailFragment.this.getView(), errorMessageRes, -1).show();
                }
            }
        });
    }

    private void showLoadingChatProgressDialog() {
        this.mLoadingChatProgressDialog = new DialogCreator(getActivity()).createCircleProgressDialog(getActivity().getString(2131165367), false);
        this.mLoadingChatProgressDialog.show();
    }

    private void dismissLoadingProgressDialog() {
        if (this.mLoadingChatProgressDialog != null && this.mLoadingChatProgressDialog.isShowing()) {
            this.mLoadingChatProgressDialog.dismiss();
        }
    }

    private void showAdAuthorIsUserDialog() {
        DialogCreator dialogCreator = new DialogCreator(getActivity());
        dialogCreator.getClass();
        dialogCreator.create(getString(2131165352), new DialogButton(dialogCreator, 2131165345), null).show();
    }

    private void startSignInActivity() {
        getState().putInt("WAITING_FOR_ACTIVITY_RESULT", getId());
        getActivity().startActivityForResult(LoginActivity.newIntent(getActivity()), 2);
    }

    private void openReplyToAdDialog() {
        Fragment replyDialog = ReplyToAdDialogFragment.newInstance(this.mAdDetails.ad);
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.add(replyDialog, replyDialog.getTag());
        ft.commitAllowingStateLoss();
    }

    private SearchParametersContainer getSearchParametersContainer() {
        if (!(getActivity() instanceof RemoteListManagerProvider)) {
            return null;
        }
        RemoteListManager manager = ((RemoteListManagerProvider) getActivity()).getRemoteListManager(getArguments());
        if (manager instanceof RemoteAdListManager) {
            return ((RemoteAdListManager) manager).getSearchParameters();
        }
        return null;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (getState().containsKey("WAITING_FOR_ACTIVITY_RESULT")) {
            super.onActivityResult(requestCode, resultCode, data);
            if (resultCode == -1) {
                if (requestCode == 22 && data != null) {
                    int viewPagerUpdatedIndex = data.getBundleExtra("EXTRAS_ARGUMENTS").getInt("INDEX", 0);
                    this.mHasViewPagerIndexUpdatedProgrammatically = true;
                    this.mMediaViewPager.setCurrentItem(viewPagerUpdatedIndex, false);
                    return;
                } else if (requestCode == 1) {
                    saveAd();
                } else if (requestCode == 2) {
                    onMessage();
                }
            }
            getState().remove("WAITING_FOR_ACTIVITY_RESULT");
        }
    }

    public boolean isInSearchMode() {
        return this.mInSearchMode;
    }

    public void onSaveState(Bundle state) {
        if (state.containsKey("OPEN_CHAT")) {
            state.putBoolean("OPEN_CHAT", this.mShouldOpenChat);
        }
        state.putParcelable("AD_CONTENT", this.mAdDetails);
        state.putInt("AD_INDEX", this.mViewPagerIndex);
        state.putParcelable("SAVED_INSTANCE_STATE_VIEWPAGER_INDICATOR", this.mMediaViewPager.onSaveInstanceState());
    }

    public void onLoadState(Bundle state) {
        if (state.containsKey("AD_CONTENT")) {
            DataModel model = (DataModel) state.getParcelable("AD_CONTENT");
            if (model instanceof AdDetailsApiModel) {
                this.mAdDetails = (AdDetailsApiModel) model;
            } else if (model instanceof PrivateAd) {
                this.mAdDetails = new AdDetailsApiModel();
                this.mAdDetails.labelMap = ((PrivateAd) model).labelMap;
                this.mAdDetails.ad = ((PrivateAd) model).ad;
            }
        }
        this.mViewPagerIndex = state.getInt("AD_INDEX", 0);
        this.mInSearchMode = state.getBoolean("IN_SEARCH_MODE", false);
        this.mShouldOpenChat = state.getBoolean("OPEN_CHAT", false);
    }
}
