package com.schibsted.scm.nextgenapp.admanagement.addeletion;

import com.schibsted.scm.nextgenapp.models.submodels.DeleteReason;
import java.util.List;

public class AdDeletionContract {

    public interface FragmentContract {
        void finish();

        void showAdDeletedDialog();

        void showAlert(int i);

        void showAlertInDialog(int i);

        void showInfo(int i);
    }

    public interface ModelContract {
        int getDefaultRating();

        DeleteReason getSelectedDeleteReason();

        void requestDeleteAd(DeleteReason deleteReason, String str, int i);

        void requestDeleteReasons();

        void setPresenter(PresenterModelContract presenterModelContract);

        void setSelectedDeleteReason(DeleteReason deleteReason);
    }

    public interface PresenterFragmentContract {
        void bind();
    }

    public interface PresenterModelContract {
        void onAdDeletionRequestNoReasonError();

        void onAdDeletionRequestSuccessful();

        void onAdDeletionRequestValidationError();

        void onDeleteReasonsError(String str);

        void onRecommendationLevelError(String str);

        void onRequestForbiddenError();

        void onRequestGenericError();

        void onRequestNotFoundError();

        void onRequestUnauthorizedError();

        void onRequestUnhandledError();

        void onRequestValidationError();

        void populateDeleteReasons(List<DeleteReason> list);

        void setAdTitle(String str);
    }

    public interface PresenterViewContract {
        void onCancelButtonClicked();

        void onDeleteReasonSelected(DeleteReason deleteReason);

        void onRecommendSeekBarValuesChanged(Integer num, Integer num2);

        void onSubmitButtonClicked();
    }

    public interface ViewContract {
        void enableButtons();

        int getRating();

        String getUserComments();

        void setAdTitle(String str);

        void setDeleteReasons(List<DeleteReason> list);

        void setPresenter(PresenterViewContract presenterViewContract);

        void setRating(int i);

        void showDeletionReasonError(String str);

        void showRecommendationLevelError(String str);
    }
}
