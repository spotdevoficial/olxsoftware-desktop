package com.schibsted.scm.nextgenapp.admanagement.adedition.adeditionsuccess;

import android.view.View;

public class ManagementAdSuccessEditContract {

    public interface FragmentContract {
        void onFinish();
    }

    public interface ModelContract {
        void setPresenter(PresenterModelContract presenterModelContract);
    }

    public interface PresenterFragmentContract {
    }

    public interface PresenterModelContract {
    }

    public interface PresenterViewContract {
        void onAdListButtonClicked();
    }

    public interface ViewContract {
        View getView();

        void setPresenter(PresenterViewContract presenterViewContract);
    }
}
