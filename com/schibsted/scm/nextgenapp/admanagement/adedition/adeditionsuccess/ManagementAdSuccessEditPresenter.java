package com.schibsted.scm.nextgenapp.admanagement.adedition.adeditionsuccess;

import com.schibsted.scm.nextgenapp.admanagement.adedition.adeditionsuccess.ManagementAdSuccessEditContract.FragmentContract;
import com.schibsted.scm.nextgenapp.admanagement.adedition.adeditionsuccess.ManagementAdSuccessEditContract.ModelContract;
import com.schibsted.scm.nextgenapp.admanagement.adedition.adeditionsuccess.ManagementAdSuccessEditContract.PresenterFragmentContract;
import com.schibsted.scm.nextgenapp.admanagement.adedition.adeditionsuccess.ManagementAdSuccessEditContract.PresenterModelContract;
import com.schibsted.scm.nextgenapp.admanagement.adedition.adeditionsuccess.ManagementAdSuccessEditContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.admanagement.adedition.adeditionsuccess.ManagementAdSuccessEditContract.ViewContract;

public class ManagementAdSuccessEditPresenter implements PresenterFragmentContract, PresenterModelContract, PresenterViewContract {
    private FragmentContract mFragment;
    private ModelContract mModel;
    private ViewContract mView;

    public ManagementAdSuccessEditPresenter(ModelContract model, ViewContract view, FragmentContract fragment) {
        this.mView = view;
        this.mModel = model;
        this.mFragment = fragment;
    }

    public void onAdListButtonClicked() {
        this.mFragment.onFinish();
    }
}
