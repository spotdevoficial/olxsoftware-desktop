package com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess.ManagementAdSuccessInsertContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess.ManagementAdSuccessInsertContract.ViewContract;

public class ManagementAdSuccessInsertView implements ViewContract {
    private Button mButtonAdList;
    private Button mButtonInsertAd;
    private PresenterViewContract mPresenter;
    private View mView;

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess.ManagementAdSuccessInsertView.1 */
    class C11241 implements OnClickListener {
        C11241() {
        }

        public void onClick(View v) {
            ManagementAdSuccessInsertView.this.mPresenter.onInsertNewAdButtonClicked();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess.ManagementAdSuccessInsertView.2 */
    class C11252 implements OnClickListener {
        C11252() {
        }

        public void onClick(View v) {
            ManagementAdSuccessInsertView.this.mPresenter.onAdListButtonClicked();
        }
    }

    public ManagementAdSuccessInsertView(Context context) {
        this.mView = LayoutInflater.from(context).inflate(2130903194, null);
        this.mButtonInsertAd = (Button) this.mView.findViewById(2131558865);
        this.mButtonAdList = (Button) this.mView.findViewById(2131558866);
        this.mButtonInsertAd.setOnClickListener(new C11241());
        this.mButtonAdList.setOnClickListener(new C11252());
    }

    public View getView() {
        return this.mView;
    }

    public void setPresenter(PresenterViewContract presenter) {
        this.mPresenter = presenter;
    }
}
