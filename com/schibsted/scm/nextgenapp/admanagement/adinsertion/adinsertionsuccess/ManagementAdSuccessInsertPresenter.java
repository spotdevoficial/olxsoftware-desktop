package com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess;

import com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess.ManagementAdSuccessInsertContract.FragmentContract;
import com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess.ManagementAdSuccessInsertContract.ModelContract;
import com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess.ManagementAdSuccessInsertContract.PresenterFragmentContract;
import com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess.ManagementAdSuccessInsertContract.PresenterModelContract;
import com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess.ManagementAdSuccessInsertContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess.ManagementAdSuccessInsertContract.ViewContract;

public class ManagementAdSuccessInsertPresenter implements PresenterFragmentContract, PresenterModelContract, PresenterViewContract {
    private FragmentContract mFragment;
    private ModelContract mModel;
    private ViewContract mView;

    public ManagementAdSuccessInsertPresenter(ModelContract model, ViewContract view, FragmentContract fragment) {
        this.mView = view;
        this.mModel = model;
        this.mFragment = fragment;
    }

    public void onInsertNewAdButtonClicked() {
        this.mFragment.onInsertNewAd();
    }

    public void onAdListButtonClicked() {
        this.mFragment.onFinish();
    }
}
