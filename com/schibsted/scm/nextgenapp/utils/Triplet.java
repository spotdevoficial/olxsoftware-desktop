package com.schibsted.scm.nextgenapp.utils;

public class Triplet<F, S, T> {
    public F first;
    public S second;
    public T third;

    public Triplet(F first, S second, T third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Triplet)) {
            return false;
        }
        Triplet triplet = (Triplet) o;
        if (this.first == null ? triplet.first != null : !this.first.equals(triplet.first)) {
            return false;
        }
        if (this.second == null ? triplet.second != null : !this.second.equals(triplet.second)) {
            return false;
        }
        if (this.third != null) {
            if (this.third.equals(triplet.third)) {
                return true;
            }
        } else if (triplet.third == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int result;
        int hashCode;
        int i = 0;
        if (this.first != null) {
            result = this.first.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.second != null) {
            hashCode = this.second.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (i2 + hashCode) * 31;
        if (this.third != null) {
            i = this.third.hashCode();
        }
        return hashCode + i;
    }
}
