package com.schibsted.scm.nextgenapp.utils;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Parcelable;
import com.facebook.BuildConfig;
import java.util.ArrayList;
import java.util.List;

public class IntentsCreator {
    public static Intent createContactCustomerServiceIntent(String subject, String email) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("mailto:" + email));
        intent.putExtra("android.intent.extra.SUBJECT", subject);
        intent.putExtra("android.intent.extra.TEXT", BuildConfig.VERSION_NAME);
        return intent;
    }

    public static Intent createGoToFacebookIntent(Context c) throws NullPointerException {
        if (c == null) {
            throw new NullPointerException();
        }
        String facebookPageId = c.getString(2131165493);
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("fb://page/" + facebookPageId));
        if (c.getPackageManager().queryIntentActivities(intent, 0).size() != 0) {
            return intent;
        }
        return new Intent("android.intent.action.VIEW", Uri.parse("http://touch.facebook.com/" + facebookPageId));
    }

    public static Intent createCallIntent(String phone) {
        Intent intent = new Intent("android.intent.action.DIAL");
        intent.setData(Uri.parse("tel:" + phone));
        return intent;
    }

    public static Intent createEmailIntent(String address, String subject) {
        Intent emailIntent = new Intent("android.intent.action.SENDTO", Uri.fromParts("mailto", address, null));
        emailIntent.putExtra("android.intent.extra.SUBJECT", subject);
        return emailIntent;
    }

    public static Intent createSmsIntent(String phone) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("sms:" + phone));
        return intent;
    }

    private static Intent createGoToPlayStoreIntent(Context c) throws ActivityNotFoundException {
        if (c == null) {
            throw new ActivityNotFoundException();
        }
        return new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + c.getPackageName()));
    }

    private static Intent createGoToPlayStoreWebIntent(Context c) throws ActivityNotFoundException {
        if (c == null) {
            throw new ActivityNotFoundException();
        }
        return new Intent("android.intent.action.VIEW", Uri.parse("http://play.google.com/store/apps/details?id=" + c.getPackageName()));
    }

    public static Intent createPlayStoreIntent(Context c) throws ActivityNotFoundException {
        if (c == null) {
            throw new ActivityNotFoundException();
        }
        boolean isIntentSafe;
        PackageManager packageManager = c.getPackageManager();
        Intent intent = createGoToPlayStoreIntent(c);
        if (packageManager.queryIntentActivities(intent, 0).size() > 0) {
            isIntentSafe = true;
        } else {
            isIntentSafe = false;
        }
        if (isIntentSafe) {
            return intent;
        }
        intent = createGoToPlayStoreWebIntent(c);
        if (packageManager.queryIntentActivities(intent, 0).size() > 0) {
            isIntentSafe = true;
        } else {
            isIntentSafe = false;
        }
        if (isIntentSafe) {
            return intent;
        }
        return null;
    }

    public static Intent createShareAdIntent(Context c, String subject, String body) throws ActivityNotFoundException {
        if (c == null) {
            throw new ActivityNotFoundException();
        }
        Intent share = new Intent("android.intent.action.SEND");
        share.setType("text/plain");
        share.putExtra("android.intent.extra.SUBJECT", subject);
        share.putExtra("android.intent.extra.TEXT", body);
        return Intent.createChooser(share, c.getString(2131165311));
    }

    public static Intent createImageChooserIntent(Context c, Uri imageUri) throws ActivityNotFoundException {
        if (c == null) {
            throw new ActivityNotFoundException();
        }
        Intent chooserIntent = Intent.createChooser(createGalleryIntent(), c.getResources().getString(2131165643));
        if (VERSION.SDK_INT >= 21) {
            chooserIntent.setFlags(524288);
        } else {
            chooserIntent.setFlags(524288);
        }
        List<Intent> cameraIntents = createCameraIntents(c, imageUri);
        chooserIntent.putExtra("android.intent.extra.INITIAL_INTENTS", (Parcelable[]) cameraIntents.toArray(new Parcelable[cameraIntents.size()]));
        return chooserIntent;
    }

    public static Intent createGalleryIntent() {
        Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        if (VERSION.SDK_INT >= 18) {
            galleryIntent.putExtra("android.intent.extra.ALLOW_MULTIPLE", true);
        }
        galleryIntent.setAction("android.intent.action.GET_CONTENT");
        galleryIntent.putExtra("android.intent.extra.LOCAL_ONLY", true);
        if (VERSION.SDK_INT >= 21) {
            galleryIntent.setFlags(524288);
        } else {
            galleryIntent.setFlags(524288);
        }
        return galleryIntent;
    }

    public static List<Intent> createCameraIntents(Context c, Uri file) throws ActivityNotFoundException {
        if (c == null) {
            throw new ActivityNotFoundException();
        }
        List<Intent> cameraIntents = new ArrayList();
        Intent captureIntent = new Intent("android.media.action.IMAGE_CAPTURE");
        for (ResolveInfo res : c.getPackageManager().queryIntentActivities(captureIntent, 0)) {
            String packageName = res.activityInfo.packageName;
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra("output", file);
            if (VERSION.SDK_INT >= 21) {
                intent.setFlags(524288);
            } else {
                intent.setFlags(524288);
            }
            cameraIntents.add(intent);
        }
        return cameraIntents;
    }
}
