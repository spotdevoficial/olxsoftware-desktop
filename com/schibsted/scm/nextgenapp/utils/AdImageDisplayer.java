package com.schibsted.scm.nextgenapp.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.LoadedFrom;
import com.nostra13.universalimageloader.core.assist.ViewScaleType;
import com.nostra13.universalimageloader.core.display.BitmapDisplayer;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.models.submodels.MediaData;
import com.schibsted.scm.nextgenapp.ui.drawable.FadeinDrawable;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;

public class AdImageDisplayer implements ImageAware {
    private static final String TAG;
    private AdLoaderCallback mAdLoaderCallback;
    private Context mContext;
    private int mHeight;
    private String mImageSizeRemoteDirectory;
    private Reference<ImageView> mImageViewReference;
    private String mUrl;
    private int mWidth;

    /* renamed from: com.schibsted.scm.nextgenapp.utils.AdImageDisplayer.1 */
    class C15541 extends SimpleImageLoadingListener {
        C15541() {
        }

        public void onLoadingStarted(String imageUri, View view) {
            AdImageDisplayer.this.runStartLoadCallback();
        }

        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
        }

        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            AdImageDisplayer.this.runLoadedCallback(false, false);
        }

        public void onLoadingCancelled(String imageUri, View view) {
        }
    }

    public interface AdLoaderCallback {
        void onAdLoaded(boolean z, boolean z2);

        void onAdStartLoad();
    }

    private class FadeInAdDisplayer implements BitmapDisplayer {
        private Context mContext;
        private boolean mIsPlaceholder;

        public FadeInAdDisplayer(Context context, boolean isPlaceholder) {
            this.mContext = context;
            this.mIsPlaceholder = isPlaceholder;
        }

        public void display(Bitmap bitmap, ImageAware imageAware, LoadedFrom loadedFrom) {
            boolean z = true;
            boolean z2 = false;
            ImageView imageView = (ImageView) imageAware.getWrappedView();
            if (imageView == null) {
                AdImageDisplayer.this.runLoadedCallback(false, false);
            } else if (loadedFrom != LoadedFrom.MEMORY_CACHE) {
                FadeinDrawable.setBitmap(imageView, this.mContext, bitmap, 500.0f);
                r3 = AdImageDisplayer.this;
                if (this.mIsPlaceholder) {
                    z = false;
                }
                r3.runLoadedCallback(z, false);
            } else {
                imageView.setImageBitmap(bitmap);
                r3 = AdImageDisplayer.this;
                if (!this.mIsPlaceholder) {
                    z2 = true;
                }
                r3.runLoadedCallback(z2, true);
            }
        }
    }

    static {
        TAG = AdImageDisplayer.class.getSimpleName();
    }

    public AdImageDisplayer(Context context, ImageView imageView) {
        this.mWidth = -1;
        this.mHeight = -1;
        imageView.setScaleType(ScaleType.CENTER_CROP);
        this.mImageViewReference = new WeakReference(imageView);
        this.mContext = context;
    }

    public void setImageSize(float size) {
        this.mImageSizeRemoteDirectory = Utils.calculatePreferredImageSize(size, true);
    }

    public int getWidth() {
        if (this.mWidth > 0) {
            return this.mWidth;
        }
        ImageView imageView = (ImageView) this.mImageViewReference.get();
        if (imageView == null) {
            return 0;
        }
        LayoutParams params = imageView.getLayoutParams();
        int width = 0;
        if (params != null) {
            width = params.width;
        }
        if (width <= 0) {
            return getImageViewFieldValue(imageView, "mMaxWidth");
        }
        return width;
    }

    public int getHeight() {
        if (this.mHeight > 0) {
            return this.mHeight;
        }
        ImageView imageView = (ImageView) this.mImageViewReference.get();
        if (imageView == null) {
            return 0;
        }
        LayoutParams params = imageView.getLayoutParams();
        int height = 0;
        if (params != null) {
            height = params.height;
        }
        if (height <= 0) {
            return getImageViewFieldValue(imageView, "mMaxHeight");
        }
        return height;
    }

    public ViewScaleType getScaleType() {
        ImageView imageView = (ImageView) this.mImageViewReference.get();
        if (imageView != null) {
            return ViewScaleType.fromImageView(imageView);
        }
        return null;
    }

    public ImageView getWrappedView() {
        return (ImageView) this.mImageViewReference.get();
    }

    public boolean isCollected() {
        return this.mImageViewReference.get() == null;
    }

    public int getId() {
        ImageView imageView = (ImageView) this.mImageViewReference.get();
        return imageView == null ? super.hashCode() : imageView.hashCode();
    }

    private static int getImageViewFieldValue(Object object, String fieldName) {
        try {
            Field field = ImageView.class.getDeclaredField(fieldName);
            field.setAccessible(true);
            int fieldValue = ((Integer) field.get(object)).intValue();
            if (fieldValue <= 0 || fieldValue >= Integer.MAX_VALUE) {
                return 0;
            }
            return fieldValue;
        } catch (Exception e) {
            Logger.error(TAG, "Unable to set value", e);
            return 0;
        }
    }

    public boolean setImageDrawable(Drawable drawable) {
        ImageView imageView = (ImageView) this.mImageViewReference.get();
        if (imageView == null) {
            return false;
        }
        imageView.setImageDrawable(drawable);
        return true;
    }

    public boolean setImageBitmap(Bitmap bitmap) {
        ImageView imageView = (ImageView) this.mImageViewReference.get();
        if (imageView == null) {
            return false;
        }
        imageView.setImageBitmap(bitmap);
        return true;
    }

    public void loadAd(Ad ad, int columns, int maxColumns) {
        loadAd(ad, columns, maxColumns, null);
    }

    public void loadAd(Ad ad, int columns, int maxColumns, AdLoaderCallback adLoaderCallback) {
        String imageUrl = null;
        if (!(ad == null || ad.thumbInfo == null)) {
            imageUrl = getImageUrl(ad);
        }
        if (((isCollected() || this.mUrl == null || imageUrl == null || !imageUrl.equals(this.mUrl)) && !(this.mUrl == null && imageUrl == null)) || getWrappedView().getDrawable() == null) {
            this.mAdLoaderCallback = adLoaderCallback;
            WindowManager wm = (WindowManager) this.mContext.getSystemService("window");
            DisplayMetrics metrics = new DisplayMetrics();
            wm.getDefaultDisplay().getMetrics(metrics);
            this.mWidth = (metrics.widthPixels / maxColumns) * columns;
            this.mHeight = Utils.dpToPx(this.mContext, (int) this.mContext.getResources().getDimension(2131230807));
            setPlaceholder();
            if (TextUtils.isEmpty(imageUrl)) {
                runLoadedCallback(false, false);
                return;
            }
            Logger.debug(TAG, imageUrl);
            Bitmap bm = (Bitmap) C1049M.getTrafficManager().getImageLoader().getMemoryCache().get(imageUrl);
            if (bm == null || bm.isRecycled()) {
                setImageUrl(imageUrl);
                return;
            }
            runStartLoadCallback();
            setImageBitmap(bm);
        }
    }

    private String getImageUrl(Ad ad) {
        String imageUrl = null;
        if (!(ad.mediaList == null || ad.mediaList.isEmpty())) {
            imageUrl = ((MediaData) ad.mediaList.get(0)).getResourceURL(this.mImageSizeRemoteDirectory);
        }
        if (imageUrl != null || ad.thumbInfo == null) {
            return imageUrl;
        }
        return ad.thumbInfo.getResourceURL(this.mImageSizeRemoteDirectory);
    }

    private void setImageUrl(String url) {
        this.mUrl = url;
        C1049M.getTrafficManager().getImageLoader().displayImage(url, (ImageAware) this, getDisplayImageOptions(false), new C15541());
    }

    private void setPlaceholder() {
        setImageDrawable(this.mContext.getResources().getDrawable(2130837739));
    }

    private void runLoadedCallback(boolean hasImage, boolean fromCache) {
        if (this.mAdLoaderCallback != null) {
            this.mAdLoaderCallback.onAdLoaded(hasImage, fromCache);
        }
    }

    private void runStartLoadCallback() {
        if (this.mAdLoaderCallback != null) {
            this.mAdLoaderCallback.onAdStartLoad();
        }
    }

    private DisplayImageOptions getDisplayImageOptions(boolean placeHolder) {
        return new Builder().bitmapConfig(Config.RGB_565).imageScaleType(ImageScaleType.IN_SAMPLE_INT).cacheInMemory(true).considerExifParams(true).displayer(new FadeInAdDisplayer(this.mContext, placeHolder)).cacheOnDisk(true).build();
    }
}
