package com.schibsted.scm.nextgenapp.utils;

import android.content.Context;
import com.urbanairship.C1608R;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class DateFormatUtil {
    private static final DateFormat SIMPLE_DATE_FORMAT;
    private static final DateFormat SIMPLE_DATE_FORMAT_ABBR;
    public static final SimpleDateFormat sDateFormat;
    public static final SimpleDateFormat sTimeFormat;

    static {
        SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd MMMM HH:mm", Locale.getDefault());
        SIMPLE_DATE_FORMAT_ABBR = new SimpleDateFormat("dd MM HH:mm", Locale.getDefault());
        sDateFormat = new SimpleDateFormat("dd/MM/yy", Locale.getDefault());
        sTimeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
    }

    public static synchronized String getDateLabel(Context context, long dateInMillis) {
        String dateLabel;
        synchronized (DateFormatUtil.class) {
            dateLabel = getDateLabel(context, dateInMillis, 0);
        }
        return dateLabel;
    }

    public static synchronized String getDateLabel(Context context, long dateInMillis, int daysThreshold) {
        String dateLabel;
        synchronized (DateFormatUtil.class) {
            dateLabel = getDateLabel(context, dateInMillis, daysThreshold, false);
        }
        return dateLabel;
    }

    public static synchronized String getDateLabel(Context context, long dateInMillis, int daysThreshold, boolean abbreviateMonth) {
        String string;
        synchronized (DateFormatUtil.class) {
            Calendar today = Calendar.getInstance();
            today.set(today.get(1), today.get(2), today.get(5), 0, 0, 0);
            today.add(5, 1);
            int daysAgo = (int) ((today.getTimeInMillis() - dateInMillis) / TimeUnit.DAYS.toMillis(1));
            if (daysAgo < 0) {
                daysAgo = 0;
            }
            if (daysAgo < daysThreshold) {
                switch (daysAgo) {
                    case C1608R.styleable.MapAttrs_mapType /*0*/:
                        string = context.getResources().getString(2131165404);
                        break;
                    case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                        string = context.getResources().getString(2131165405);
                        break;
                    default:
                        string = String.format(context.getResources().getString(2131165403), new Object[]{String.valueOf(daysAgo)});
                        break;
                }
            }
            string = abbreviateMonth ? SIMPLE_DATE_FORMAT_ABBR.format(new Date(dateInMillis)) : SIMPLE_DATE_FORMAT.format(new Date(dateInMillis));
        }
        return string;
    }
}
