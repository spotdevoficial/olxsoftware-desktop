package com.schibsted.scm.nextgenapp.utils;

import java.util.LinkedList;
import java.util.List;

public class DataTransferLogAverage {
    private List<int[]> elements;
    private boolean hasEnoughData;
    private int minSampleSize;
    private int totalSize;
    private int totalTime;

    public DataTransferLogAverage(int minSampleSize) {
        this.elements = new LinkedList();
        this.totalSize = 0;
        this.totalTime = 0;
        this.minSampleSize = minSampleSize;
        this.hasEnoughData = false;
    }

    public void clear() {
        this.elements.clear();
        this.totalSize = 0;
        this.totalTime = 0;
        this.hasEnoughData = false;
    }

    public void add(int size, int time) {
        this.elements.add(new int[]{size, time});
        this.totalSize += size;
        this.totalTime += time;
        while (this.elements.size() > 1 && this.totalSize - ((int[]) this.elements.get(0))[0] > this.minSampleSize) {
            int[] element = (int[]) this.elements.remove(0);
            this.totalSize -= element[0];
            this.totalTime -= element[1];
        }
        if (!this.hasEnoughData && this.totalSize > this.minSampleSize) {
            this.hasEnoughData = true;
        }
    }

    public float getAverage() {
        if (!this.hasEnoughData || this.totalTime == 0) {
            return -1.0f;
        }
        return ((float) this.totalSize) / ((float) this.totalTime);
    }
}
