package com.schibsted.scm.nextgenapp.utils.logger;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.schibsted.scm.nextgenapp.backend.request.NetworkRequest;

public class NetworkLoggerImpl implements NetworkLogger {
    private static final String TAG;

    static {
        TAG = NetworkLoggerImpl.class.getSimpleName();
    }

    public void logRequest(NetworkRequest request) {
    }

    public void logResponse(NetworkResponse networkResponse) {
    }

    public void logErrorResponse(VolleyError volleyError) {
    }
}
