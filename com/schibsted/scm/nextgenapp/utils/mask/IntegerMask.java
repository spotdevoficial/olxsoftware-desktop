package com.schibsted.scm.nextgenapp.utils.mask;

import com.facebook.BuildConfig;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class IntegerMask implements Mask {
    private int mLengthLimit;
    private DecimalFormat mNumberFormatter;

    public IntegerMask() {
        this(15);
    }

    public IntegerMask(int lengthLimit) {
        this.mNumberFormatter = (DecimalFormat) NumberFormat.getNumberInstance(new Locale("pt", "BR"));
        if (lengthLimit < 1 || lengthLimit > 15) {
            lengthLimit = 15;
        }
        this.mLengthLimit = lengthLimit;
    }

    public String mask(String unmasked) {
        unmasked = unmask(unmasked);
        if (unmasked == null || unmasked.trim().isEmpty()) {
            return BuildConfig.VERSION_NAME;
        }
        double parsed;
        if (unmasked.length() > this.mLengthLimit) {
            unmasked = unmasked.substring(0, this.mLengthLimit);
        }
        try {
            parsed = Double.parseDouble(unmasked);
        } catch (NumberFormatException e) {
            parsed = 0.0d;
        }
        return this.mNumberFormatter.format(parsed);
    }

    public String unmask(String masked) {
        if (masked == null) {
            return BuildConfig.VERSION_NAME;
        }
        return masked.replaceAll("[^0-9]*", BuildConfig.VERSION_NAME);
    }

    public int getSelectionIndex(String masked) {
        return masked.length();
    }
}
