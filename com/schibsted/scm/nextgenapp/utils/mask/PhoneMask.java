package com.schibsted.scm.nextgenapp.utils.mask;

import com.facebook.BuildConfig;
import com.urbanairship.C1608R;

public class PhoneMask implements Mask {
    public String mask(String unmaskedString) {
        String maskedString = BuildConfig.VERSION_NAME;
        if (unmaskedString != null) {
            int maxPhoneSize = 10;
            if (isCellphone(unmaskedString)) {
                maxPhoneSize = 11;
            }
            if (unmaskedString.length() > maxPhoneSize) {
                unmaskedString = unmaskedString.substring(0, maxPhoneSize);
            }
            String mask;
            switch (unmaskedString.length()) {
                case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                    mask = "(##) #### ####";
                    break;
                case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                    mask = "(##) ##### ####";
                    break;
                default:
                    mask = "(##) #### ####";
                    break;
            }
            int i = 0;
            for (char maskChar : mask.toCharArray()) {
                if (i < unmaskedString.length()) {
                    if (isNumberCharacter(maskChar)) {
                        maskedString = maskedString + unmaskedString.charAt(i);
                        i++;
                    } else {
                        maskedString = maskedString + maskChar;
                    }
                }
            }
        }
        return maskedString;
    }

    public String unmask(String s) {
        if (s == null) {
            return BuildConfig.VERSION_NAME;
        }
        return s.replaceAll("[^0-9]*", BuildConfig.VERSION_NAME);
    }

    public int getSelectionIndex(String masked) {
        return masked.length();
    }

    private boolean isNumberCharacter(char character) {
        return character == '#';
    }

    private boolean isCellphone(String unmaskedString) {
        if (unmaskedString.length() > 2) {
            return unmaskedString.substring(2, 3).equals("9");
        }
        return false;
    }
}
