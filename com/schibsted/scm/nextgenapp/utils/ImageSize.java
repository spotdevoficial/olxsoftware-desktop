package com.schibsted.scm.nextgenapp.utils;

public enum ImageSize {
    THUMBS(113, "thumbs85x125"),
    THUMBS_LI(135, "thumbsli"),
    ACCOUNT_AD(360, "account_ad"),
    IMAGES(640, "images");
    
    private String mKey;
    private int mSize;

    private ImageSize(int size, String key) {
        this.mSize = size;
        this.mKey = key;
    }

    public int getSize() {
        return this.mSize;
    }

    public String getKey() {
        return this.mKey;
    }
}
