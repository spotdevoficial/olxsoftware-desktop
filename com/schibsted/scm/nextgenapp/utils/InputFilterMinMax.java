package com.schibsted.scm.nextgenapp.utils;

import android.text.InputFilter;
import android.text.Spanned;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;

public class InputFilterMinMax implements InputFilter {
    private static final String TAG;
    private int max;
    private int min;

    static {
        TAG = InputFilterMinMax.class.getSimpleName();
    }

    public InputFilterMinMax(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public InputFilterMinMax(String min, String max) {
        this.min = Integer.parseInt(min);
        this.max = Integer.parseInt(max);
    }

    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        try {
            StringBuilder sb = new StringBuilder(dest);
            sb.replace(dstart, dend, source.toString());
            if (isInRange(this.min, this.max, Integer.parseInt(sb.toString()))) {
                return null;
            }
        } catch (NumberFormatException e) {
            Logger.warn(TAG, "The value is out of range");
        }
        return BuildConfig.VERSION_NAME;
    }

    private boolean isInRange(int a, int b, int c) {
        return b > a ? c >= a && c <= b : c >= b && c <= a;
    }
}
