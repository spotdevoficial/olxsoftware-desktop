package com.schibsted.scm.nextgenapp.utils;

import android.content.Context;
import com.crashlytics.android.Crashlytics;
import com.schibsted.scm.nextgenapp.tracking.fabric.FabricAnalytics;

public class CrashAnalytics extends FabricAnalytics {
    public static boolean sSendCrashReport;

    static {
        sSendCrashReport = true;
    }

    public static void init(Context context) {
        if (sSendCrashReport) {
            FabricAnalytics.init(context, new Crashlytics());
        }
    }

    public static void logException(Throwable t) {
        if (sSendCrashReport) {
            Crashlytics.logException(t);
        }
    }

    public static void setString(String key, String value) {
        if (sSendCrashReport) {
            Crashlytics.setString(key, value);
        }
    }

    public static void setInt(String key, int value) {
        if (sSendCrashReport) {
            Crashlytics.setInt(key, value);
        }
    }

    public static void log(int priority, String tag, String msg) {
        if (sSendCrashReport) {
            Crashlytics.log(priority, tag, msg);
        }
    }
}
