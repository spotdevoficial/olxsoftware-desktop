package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.schibsted.scm.nextgenapp.models.submodels.PhoneNumber;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.List;

public class AdPhoneNumbersApiModel implements DataModel {
    public static Creator<AdPhoneNumbersApiModel> CREATOR;
    public List<PhoneNumber> phones;

    /* renamed from: com.schibsted.scm.nextgenapp.models.AdPhoneNumbersApiModel.1 */
    static class C12031 implements Creator<AdPhoneNumbersApiModel> {
        C12031() {
        }

        public AdPhoneNumbersApiModel createFromParcel(Parcel source) {
            return new AdPhoneNumbersApiModel(null);
        }

        public AdPhoneNumbersApiModel[] newArray(int size) {
            return new AdPhoneNumbersApiModel[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelableList(this.phones);
    }

    private AdPhoneNumbersApiModel(Parcel in) {
        this.phones = new ParcelReader(in).readParcelableList(PhoneNumber.CREATOR);
    }

    static {
        CREATOR = new C12031();
    }
}
