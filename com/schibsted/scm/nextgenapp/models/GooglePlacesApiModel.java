package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.schibsted.scm.nextgenapp.models.submodels.GooglePlacesPrediction;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GooglePlacesApiModel implements DataModel {
    public static Creator<GooglePlacesApiModel> CREATOR;
    public List<GooglePlacesPrediction> predictions;
    public String status;

    /* renamed from: com.schibsted.scm.nextgenapp.models.GooglePlacesApiModel.1 */
    static class C12121 implements Creator<GooglePlacesApiModel> {
        C12121() {
        }

        public GooglePlacesApiModel createFromParcel(Parcel source) {
            return new GooglePlacesApiModel(null);
        }

        public GooglePlacesApiModel[] newArray(int size) {
            return new GooglePlacesApiModel[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.status).writeParcelableList(this.predictions);
    }

    private GooglePlacesApiModel(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.status = reader.readString();
        this.predictions = reader.readParcelableList(GooglePlacesPrediction.CREATOR);
    }

    static {
        CREATOR = new C12121();
    }
}
