package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.submodels.FilterDescription;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class FiltersApiModel implements DataModel {
    public static Creator<FiltersApiModel> CREATOR = null;
    public static final String PERSISTENT_ID = "filters";
    @JsonProperty(required = true, value = "list")
    public FilterDescription listingFilters;
    @JsonProperty(required = true, value = "newad")
    public FilterDescription newAdFilters;

    /* renamed from: com.schibsted.scm.nextgenapp.models.FiltersApiModel.1 */
    static class C12101 implements Creator<FiltersApiModel> {
        C12101() {
        }

        public FiltersApiModel createFromParcel(Parcel source) {
            return new FiltersApiModel(source);
        }

        public FiltersApiModel[] newArray(int size) {
            return new FiltersApiModel[size];
        }
    }

    static {
        CREATOR = new C12101();
    }

    public FiltersApiModel(Parcel in) {
        this.listingFilters = (FilterDescription) in.readParcelable(FilterDescription.class.getClassLoader());
        this.newAdFilters = (FilterDescription) in.readParcelable(FilterDescription.class.getClassLoader());
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelable(this.listingFilters).writeParcelable(this.newAdFilters);
    }
}
