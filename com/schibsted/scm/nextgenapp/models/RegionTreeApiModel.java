package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.BuildConfig;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.submodels.RegionNode;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class RegionTreeApiModel implements DataModel {
    public static Creator<RegionTreeApiModel> CREATOR = null;
    public static final String PERSISTENT_ID = "region_data";
    @JsonIgnore
    public final String allLabel;
    @JsonIgnore
    private final RegionNode[] regionNode;

    /* renamed from: com.schibsted.scm.nextgenapp.models.RegionTreeApiModel.1 */
    static class C12191 implements Creator<RegionTreeApiModel> {
        C12191() {
        }

        public RegionTreeApiModel createFromParcel(Parcel source) {
            return new RegionTreeApiModel(null);
        }

        public RegionTreeApiModel[] newArray(int size) {
            return new RegionTreeApiModel[size];
        }
    }

    @JsonIgnore
    public RegionNode[] getLocations() {
        return this.regionNode;
    }

    @JsonCreator
    public RegionTreeApiModel(@JsonProperty("locations") RegionNode[] locations, @JsonProperty(required = false, value = "all_label") String all_label) {
        this.regionNode = locations;
        this.allLabel = all_label;
    }

    public RegionTreeApiModel() {
        this.regionNode = new RegionNode[0];
        this.allLabel = BuildConfig.VERSION_NAME;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelableArray(this.regionNode).writeString(this.allLabel);
    }

    private RegionTreeApiModel(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.regionNode = (RegionNode[]) reader.readParcelableArray(RegionNode.class);
        this.allLabel = reader.readString();
    }

    static {
        CREATOR = new C12191();
    }
}
