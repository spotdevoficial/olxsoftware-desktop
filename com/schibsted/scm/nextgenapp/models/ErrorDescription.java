package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.submodels.ErrorCause;
import com.schibsted.scm.nextgenapp.models.submodels.ErrorCode;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.HashMap;
import java.util.List;

public class ErrorDescription implements DataModel {
    public static Creator<ErrorDescription> CREATOR;
    @JsonIgnore
    public List<ErrorCause> causes;
    @JsonIgnore
    public HashMap<String, ErrorCause> causesMap;
    @JsonProperty("code")
    public ErrorCode code;

    /* renamed from: com.schibsted.scm.nextgenapp.models.ErrorDescription.1 */
    static class C12081 implements Creator<ErrorDescription> {
        C12081() {
        }

        public ErrorDescription createFromParcel(Parcel source) {
            return new ErrorDescription(null);
        }

        public ErrorDescription[] newArray(int size) {
            return new ErrorDescription[size];
        }
    }

    @JsonProperty(required = false, value = "causes")
    public void setCauses(List<ErrorCause> causes) {
        this.causes = causes;
        if (causes != null) {
            this.causesMap = new HashMap();
            for (ErrorCause cause : this.causes) {
                this.causesMap.put(cause.field, cause);
            }
        }
    }

    @JsonProperty(required = false, value = "causes")
    public List<ErrorCause> getCauses() {
        return this.causes;
    }

    @JsonIgnore
    public ErrorCause getErrorCause(String field) {
        if (this.causesMap != null) {
            return (ErrorCause) this.causesMap.get(field);
        }
        return null;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeEnum(this.code).writeParcelableList(this.causes);
    }

    public void setNetworkCode(int errorCode) {
        switch (errorCode) {
            case 404:
                this.code = ErrorCode.NOT_FOUND;
            default:
                this.code = ErrorCode.UNKNOWN_ERROR;
        }
    }

    public boolean isEmailNotVerified() {
        return this.code.equals(ErrorCode.EMAIL_NOT_VERIFIED);
    }

    private ErrorDescription(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.code = (ErrorCode) reader.readEnum(ErrorCode.class);
        this.causes = reader.readParcelableList(ErrorCause.CREATOR);
    }

    static {
        CREATOR = new C12081();
    }
}
