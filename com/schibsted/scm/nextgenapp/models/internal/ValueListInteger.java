package com.schibsted.scm.nextgenapp.models.internal;

import com.schibsted.scm.nextgenapp.models.interfaces.ValueList;

public class ValueListInteger implements ValueList {
    private int mMax;
    private int mMin;
    private int mStep;

    public ValueListInteger(int min, int max, int step) {
        this.mMin = min;
        this.mMax = max;
        this.mStep = step;
    }

    public ValueListItem get(int pos) {
        int val = this.mMin + (this.mStep * pos);
        return new ValueListItem(String.valueOf(val), String.valueOf(val));
    }

    public int size() {
        return ((this.mMax - this.mMin) / this.mStep) + 1;
    }
}
