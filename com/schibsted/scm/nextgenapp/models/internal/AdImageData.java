package com.schibsted.scm.nextgenapp.models.internal;

public class AdImageData {
    final MediaUploadState state;
    final String uri;

    public AdImageData(String uri, MediaUploadState state) {
        this.uri = uri;
        this.state = state;
    }

    public String getUri() {
        return this.uri;
    }

    public MediaUploadState getState() {
        return this.state;
    }
}
