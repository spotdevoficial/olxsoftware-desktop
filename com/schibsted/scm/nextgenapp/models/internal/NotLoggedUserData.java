package com.schibsted.scm.nextgenapp.models.internal;

public class NotLoggedUserData {
    private final String mEmail;
    private final String mName;
    private final String mPhone;

    public NotLoggedUserData(String name, String email, String phone) {
        this.mName = name;
        this.mEmail = email;
        this.mPhone = phone;
    }

    public String getName() {
        return this.mName;
    }

    public String getEmail() {
        return this.mEmail;
    }

    public String getPhone() {
        return this.mPhone;
    }
}
