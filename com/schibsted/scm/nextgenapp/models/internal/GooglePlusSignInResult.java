package com.schibsted.scm.nextgenapp.models.internal;

public class GooglePlusSignInResult {
    private String mDisplayName;
    private String mEmail;
    private String mProfileImageUrl;
    private String mToken;

    public GooglePlusSignInResult(String token, String email, String displayName, String profileImageUrl) {
        this.mToken = token;
        this.mEmail = email;
        this.mDisplayName = displayName;
        this.mProfileImageUrl = profileImageUrl;
    }

    public String getToken() {
        return this.mToken;
    }

    public void setToken(String token) {
        this.mToken = token;
    }

    public String getEmail() {
        return this.mEmail;
    }

    public void setEmail(String email) {
        this.mEmail = email;
    }

    public String getDisplayName() {
        return this.mDisplayName;
    }

    public void setDisplayName(String displayName) {
        this.mDisplayName = displayName;
    }

    public String getProfileImageUrl() {
        return this.mProfileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.mProfileImageUrl = profileImageUrl;
    }
}
