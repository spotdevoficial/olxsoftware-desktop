package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.BuildConfig;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.schibsted.scm.nextgenapp.models.deserializers.MonetizationWebViewDeserializer;
import com.schibsted.scm.nextgenapp.models.submodels.AppVersion;
import com.schibsted.scm.nextgenapp.models.submodels.MediaConfig;
import com.schibsted.scm.nextgenapp.models.submodels.Prefetch;
import com.schibsted.scm.nextgenapp.models.submodels.StaticPage;
import com.schibsted.scm.nextgenapp.models.submodels.SupportMail;
import com.schibsted.scm.nextgenapp.models.types.AdLayoutType;
import com.schibsted.scm.nextgenapp.monetization.model.MonetizationWebViewModel;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ConfigApiModel implements DataModel {
    public static Creator<ConfigApiModel> CREATOR = null;
    public static final String PERSISTENT_ID = "config";
    @JsonProperty("config_ttl")
    public int configTtl;
    private HashMap<String, AdLayoutType> detailLayoutMap;
    @JsonIgnore
    public String infoPageUrl;
    private Map<String, Map<String, Prefetch>> mapOfPrefetches;
    private Map<String, Map<String, StaticPage>> mapOfStaticPages;
    private HashMap<String, HashMap<String, SupportMail>> mapOfSupportMails;
    @JsonProperty("media")
    public MediaConfig mediaConfig;
    @JsonProperty("monetization_webview")
    @JsonDeserialize(using = MonetizationWebViewDeserializer.class)
    public MonetizationWebViewModel monetizationWebviewModel;
    @JsonProperty("region_picker_level")
    public String regionPickerLevel;
    @JsonIgnore
    public AppVersion requiredAppVersion;
    @JsonIgnore
    public String requiredMessage;

    /* renamed from: com.schibsted.scm.nextgenapp.models.ConfigApiModel.1 */
    static class C12051 implements Creator<ConfigApiModel> {
        C12051() {
        }

        public ConfigApiModel createFromParcel(Parcel source) {
            return new ConfigApiModel(null);
        }

        public ConfigApiModel[] newArray(int size) {
            return new ConfigApiModel[size];
        }
    }

    @JsonIgnore
    public String getRegionPickerLevel() {
        return this.regionPickerLevel;
    }

    @JsonProperty("app_version")
    public void setAppVersion(JsonNode versionNode) {
        this.requiredAppVersion = null;
        this.requiredMessage = null;
        try {
            JsonNode androidNode = versionNode.get("android");
            if (androidNode != null && androidNode.has("required")) {
                this.requiredAppVersion = new AppVersion(androidNode.get("required").textValue());
                if (androidNode.has("required_message")) {
                    this.requiredMessage = androidNode.get("required_message").textValue();
                }
            }
            if (androidNode != null && androidNode.has("info_page_url")) {
                this.infoPageUrl = androidNode.get("info_page_url").textValue();
            }
        } catch (Exception e) {
            if (this.requiredAppVersion == null) {
                this.requiredAppVersion = new AppVersion(BuildConfig.VERSION_NAME);
            }
            if (this.requiredMessage == null) {
                this.requiredMessage = BuildConfig.VERSION_NAME;
            }
        }
    }

    @JsonProperty("ad_detail_layout_map")
    public void setDetailLayoutMap(JsonNode node) {
        this.detailLayoutMap = new HashMap();
        Iterator<Entry<String, JsonNode>> iterator = node.fields();
        while (iterator.hasNext()) {
            Entry<String, JsonNode> item = (Entry) iterator.next();
            this.detailLayoutMap.put(item.getKey(), AdLayoutType.getEnumFromHeader(((JsonNode) item.getValue()).textValue()));
        }
    }

    @JsonProperty("prefetches")
    public void setMapOfPrefetches(ArrayList<JsonNode> listOfNodes) {
        ObjectMapper mapper = JsonMapper.getInstance();
        this.mapOfPrefetches = new HashMap();
        Iterator it = listOfNodes.iterator();
        while (it.hasNext()) {
            JsonNode node = (JsonNode) it.next();
            if (node.has("lang") && node.has("etags")) {
                String lang = node.get("lang").textValue();
                this.mapOfPrefetches.put(lang, new HashMap());
                Iterator i$ = ((ArrayList) mapper.convertValue(node.get("etags"), mapper.getTypeFactory().constructCollectionType(ArrayList.class, Prefetch.class))).iterator();
                while (i$.hasNext()) {
                    Prefetch prefetch = (Prefetch) i$.next();
                    if (prefetch.path != null) {
                        ((Map) this.mapOfPrefetches.get(lang)).put(prefetch.path, prefetch);
                    } else {
                        ((Map) this.mapOfPrefetches.get(lang)).put(prefetch.url, prefetch);
                    }
                }
            }
        }
    }

    @JsonProperty("static_pages")
    public void setMapOfStaticPages(ArrayList<JsonNode> listOfNodes) {
        ObjectMapper mapper = JsonMapper.getInstance();
        this.mapOfStaticPages = new HashMap();
        Iterator it = listOfNodes.iterator();
        while (it.hasNext()) {
            JsonNode node = (JsonNode) it.next();
            if (node.has("lang") && node.has("pages")) {
                String lang = node.get("lang").textValue();
                this.mapOfStaticPages.put(lang, new LinkedHashMap());
                Iterator i$ = ((ArrayList) mapper.convertValue(node.get("pages"), mapper.getTypeFactory().constructCollectionType(ArrayList.class, StaticPage.class))).iterator();
                while (i$.hasNext()) {
                    StaticPage staticPage = (StaticPage) i$.next();
                    ((Map) this.mapOfStaticPages.get(lang)).put(staticPage.id, staticPage);
                }
            }
        }
    }

    @JsonProperty("support_mail")
    public void setMapOfSupportMails(ArrayList<JsonNode> listOfNodes) {
        ObjectMapper mapper = JsonMapper.getInstance();
        this.mapOfSupportMails = new HashMap();
        Iterator it = listOfNodes.iterator();
        while (it.hasNext()) {
            JsonNode node = (JsonNode) it.next();
            if (node.has("lang") && node.has("mail")) {
                String lang = node.get("lang").textValue();
                this.mapOfSupportMails.put(lang, new HashMap());
                Iterator i$ = ((ArrayList) mapper.convertValue(node.get("mail"), mapper.getTypeFactory().constructCollectionType(ArrayList.class, SupportMail.class))).iterator();
                while (i$.hasNext()) {
                    SupportMail supportMail = (SupportMail) i$.next();
                    ((HashMap) this.mapOfSupportMails.get(lang)).put(supportMail.id, supportMail);
                }
            }
        }
    }

    public Map<String, Prefetch> getMapOfPrefetches(String lang) {
        if (this.mapOfPrefetches == null) {
            return null;
        }
        Map<String, Prefetch> map = (Map) this.mapOfPrefetches.get(lang);
        if (map != null || this.mapOfPrefetches.isEmpty()) {
            return map;
        }
        return (Map) ((Entry) this.mapOfPrefetches.entrySet().iterator().next()).getValue();
    }

    public Map<String, StaticPage> getMapOfStaticPages(String lang) {
        if (this.mapOfStaticPages == null) {
            return null;
        }
        Map<String, StaticPage> map = (Map) this.mapOfStaticPages.get(lang);
        if (map != null || this.mapOfStaticPages.isEmpty()) {
            return map;
        }
        return (Map) ((Entry) this.mapOfStaticPages.entrySet().iterator().next()).getValue();
    }

    public Map<String, SupportMail> getMapOfSupportMails(String lang) {
        if (this.mapOfSupportMails == null) {
            return null;
        }
        Map<String, SupportMail> map = (Map) this.mapOfSupportMails.get(lang);
        if (map != null || this.mapOfSupportMails.isEmpty()) {
            return map;
        }
        return (Map) ((Entry) this.mapOfSupportMails.entrySet().iterator().next()).getValue();
    }

    public HashMap<String, AdLayoutType> getDetailLayoutMap() {
        return this.detailLayoutMap;
    }

    private ConfigApiModel(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.mapOfPrefetches = reader.readParcelableMapMap(Prefetch.class);
        this.mapOfStaticPages = reader.readParcelableMapMap(StaticPage.class);
        this.requiredAppVersion = (AppVersion) reader.readParcelable(AppVersion.class);
        this.configTtl = reader.readInt().intValue();
        this.requiredMessage = reader.readString();
        this.regionPickerLevel = reader.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelableMapMap(this.mapOfPrefetches).writeParcelableMapMap(this.mapOfStaticPages).writeParcelable(this.requiredAppVersion).writeInt(Integer.valueOf(this.configTtl)).writeString(this.requiredMessage).writeString(this.regionPickerLevel);
    }

    static {
        CREATOR = new C12051();
    }
}
