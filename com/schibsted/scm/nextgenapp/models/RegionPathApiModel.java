package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.facebook.BuildConfig;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import com.schibsted.scm.nextgenapp.models.submodels.RegionNode;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.io.InvalidObjectException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@JsonIgnoreProperties({"startup_level", "all_label"})
public class RegionPathApiModel implements DataModel {
    public static Creator<RegionPathApiModel> CREATOR;
    @JsonIgnore
    private RegionNode regionNode;

    /* renamed from: com.schibsted.scm.nextgenapp.models.RegionPathApiModel.1 */
    static class C12181 implements Creator<RegionPathApiModel> {
        C12181() {
        }

        public RegionPathApiModel createFromParcel(Parcel source) {
            return new RegionPathApiModel(null);
        }

        public RegionPathApiModel[] newArray(int size) {
            return new RegionPathApiModel[size];
        }
    }

    @JsonCreator
    public RegionPathApiModel(@JsonProperty("locations") RegionNode[] regions) {
        if (regions == null) {
            this.regionNode = null;
        } else if (regions.length > 0) {
            this.regionNode = regions[0];
        }
    }

    public RegionPathApiModel(RegionNode regionNode) {
        if (regionNode == null) {
            this.regionNode = null;
        } else if (regionNode.getParent() == null) {
            this.regionNode = regionNode;
        } else {
            RegionNode region = new RegionNode(regionNode);
            region.children = null;
            while (region.getParent() != null) {
                RegionNode parentRegion = new RegionNode(region.getParent());
                parentRegion.setChildren(new RegionNode[]{region});
                region = parentRegion;
            }
            this.regionNode = region;
        }
    }

    public RegionPathApiModel(RegionPathApiModel regionApiModel) {
        if (regionApiModel.regionNode != null) {
            this.regionNode = new RegionNode(regionApiModel.regionNode);
        } else {
            this.regionNode = null;
        }
    }

    public RegionPathApiModel(Identifier regionId, String label) {
        this.regionNode = new RegionNode();
        this.regionNode.identifier = regionId;
        this.regionNode.label = label;
        this.regionNode.code = regionId.getCode();
    }

    @JsonIgnore
    public RegionNode getRegionNode() {
        return this.regionNode;
    }

    @JsonProperty("locations")
    public RegionNode[] getLocations() {
        if (this.regionNode == null) {
            return null;
        }
        return new RegionNode[]{this.regionNode};
    }

    public boolean isSubset(RegionPathApiModel subset) {
        if (subset == null) {
            return false;
        }
        RegionNode thisRegionNode = this.regionNode;
        RegionNode subsetRegionNode = subset.regionNode;
        while (thisRegionNode != null) {
            if (subsetRegionNode == null || subsetRegionNode.isZipcodeNode()) {
                return true;
            }
            if (!thisRegionNode.equals(subsetRegionNode)) {
                return false;
            }
            if (thisRegionNode.children != null) {
                thisRegionNode = thisRegionNode.children[0];
            } else {
                thisRegionNode = null;
            }
            if (subsetRegionNode.children != null) {
                subsetRegionNode = subsetRegionNode.children[0];
            } else {
                subsetRegionNode = null;
            }
        }
        boolean z = subsetRegionNode == null || subsetRegionNode.isZipcodeNode();
        return z;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RegionPathApiModel)) {
            return false;
        }
        RegionPathApiModel that = (RegionPathApiModel) o;
        RegionNode thisRegionNode = this.regionNode;
        RegionNode thatRegionNode = that.regionNode;
        while (thisRegionNode != null && thatRegionNode != null) {
            if (!thisRegionNode.equals(thatRegionNode)) {
                return false;
            }
            if (thisRegionNode.children != null) {
                thisRegionNode = thisRegionNode.children[0];
            } else {
                thisRegionNode = null;
            }
            if (thatRegionNode.children != null) {
                thatRegionNode = thatRegionNode.children[0];
            } else {
                thatRegionNode = null;
            }
        }
        boolean z = thisRegionNode == null && thatRegionNode == null;
        return z;
    }

    public int hashCode() {
        return this.regionNode != null ? this.regionNode.hashCode() : 0;
    }

    @JsonIgnore
    public RegionNode getLeaf() {
        RegionNode leaf = this.regionNode;
        if (leaf != null) {
            while (leaf.children != null && !Utils.isEmpty(leaf.children) && leaf != leaf.children[0]) {
                leaf = leaf.children[0];
            }
        }
        return leaf;
    }

    @JsonIgnore
    public RegionNode findLevel(String level) {
        if (level == null) {
            return null;
        }
        RegionNode child = this.regionNode;
        while (child != null && child.children != null && !Utils.isEmpty(child.children) && !level.equals(child.key)) {
            child = child.children[0];
        }
        if (child == null || !level.equals(child.key)) {
            return null;
        }
        return child;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    @com.fasterxml.jackson.annotation.JsonIgnore
    public boolean capAtLevel(java.lang.String r6) {
        /*
        r5 = this;
        r3 = 0;
        if (r6 != 0) goto L_0x0005;
    L_0x0003:
        r0 = r3;
    L_0x0004:
        return r0;
    L_0x0005:
        r0 = 0;
        r1 = r5.regionNode;
    L_0x0008:
        if (r1 == 0) goto L_0x0013;
    L_0x000a:
        r4 = r1.key;
        r4 = r6.equals(r4);
        if (r4 == 0) goto L_0x0043;
    L_0x0012:
        r0 = 1;
    L_0x0013:
        if (r1 == 0) goto L_0x0004;
    L_0x0015:
        r2 = r5.getLeaf();
        r3 = 0;
        r1.setChildren(r3);
        if (r2 == 0) goto L_0x0004;
    L_0x001f:
        r3 = com.schibsted.scm.nextgenapp.config.ConfigContainer.getConfig();
        r3.getClass();
        r3 = "zipcode";
        r3 = r3.equals(r6);
        if (r3 != 0) goto L_0x0004;
    L_0x002e:
        r3 = com.schibsted.scm.nextgenapp.config.ConfigContainer.getConfig();
        r3.getClass();
        r3 = "zipcode";
        r4 = r2.key;
        r3 = r3.equals(r4);
        if (r3 == 0) goto L_0x0004;
    L_0x003f:
        r5.setZipCode(r2);
        goto L_0x0004;
    L_0x0043:
        r4 = r1.children;
        if (r4 == 0) goto L_0x0013;
    L_0x0047:
        r4 = r1.children;
        r4 = com.schibsted.scm.nextgenapp.utils.Utils.isEmpty(r4);
        if (r4 != 0) goto L_0x0013;
    L_0x004f:
        r4 = r1.children;
        r1 = r4[r3];
        goto L_0x0008;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.schibsted.scm.nextgenapp.models.RegionPathApiModel.capAtLevel(java.lang.String):boolean");
    }

    @JsonIgnore
    public RegionNode getZipCode() {
        RegionNode deepestChild = getLeaf();
        if (deepestChild != null) {
            ConfigContainer.getConfig().getClass();
            if ("zipcode".equals(deepestChild.key)) {
                return deepestChild;
            }
        }
        return null;
    }

    @JsonIgnore
    public void setZipCode(String zipCode) {
        RegionNode zipRegion = new RegionNode();
        ConfigContainer.getConfig().getClass();
        zipRegion.key = "zipcode";
        zipRegion.code = !TextUtils.isEmpty(zipCode) ? zipCode : "00000000";
        zipRegion.label = zipCode;
        setZipCode(zipRegion);
    }

    @JsonIgnore
    public void setZipCode(RegionNode zipCode) {
        if (this.regionNode != null) {
            RegionNode deepest = getLeaf();
            if (deepest != null) {
                ConfigContainer.getConfig().getClass();
                if ("zipcode".equals(deepest.key)) {
                    if (deepest.mParent != null) {
                        deepest = deepest.mParent;
                    } else {
                        try {
                            CrashAnalytics.logException(new InvalidObjectException("Invalid RegionPathAPiModel: " + JsonMapper.getInstance().writeValueAsString(this)));
                            return;
                        } catch (JsonProcessingException e) {
                            CrashAnalytics.logException(new InvalidObjectException("Invalid RegionPathAPiModel: Non processable object"));
                            return;
                        }
                    }
                }
                ConfigContainer.getConfig().getClass();
                if ("zipcode".equals(zipCode.key)) {
                    zipCode.children = null;
                    deepest.children = new RegionNode[]{zipCode};
                    zipCode.mParent = deepest;
                    return;
                }
                throw new IllegalArgumentException("The region must have a valid ZipCode key, his key is: " + zipCode.key);
            }
        }
    }

    @JsonIgnore
    public String getLabel() {
        if (this.regionNode == null) {
            return BuildConfig.VERSION_NAME;
        }
        String regionLabel = this.regionNode.getLabel();
        RegionNode child = this.regionNode;
        while (child.getChildren() != null && child.getChildren().length == 1) {
            child = child.getChildren()[0];
            if (!Utils.isEmptyOrBlank(child.getLabel())) {
                String str = child.key;
                ConfigContainer.getConfig().getClass();
                if (!str.equals("zipcode")) {
                    regionLabel = child.getLabel();
                }
            }
        }
        return regionLabel;
    }

    @JsonIgnore
    public String getLabel(String[] locationKeys, String separator) {
        if (!Utils.isEmpty((Object[]) locationKeys)) {
            List<String> locationList = new ArrayList();
            List<String> locationKeyList = Arrays.asList(locationKeys);
            RegionNode node = this.regionNode;
            while (node != null) {
                if (locationKeyList.contains(node.getKey()) && !Utils.isEmptyOrBlank(node.getLabel())) {
                    locationList.add(0, node.getLabel());
                }
                node = node.getChildren() != null ? node.getChildren()[0] : null;
            }
            if (!locationList.isEmpty()) {
                return TextUtils.join(separator, locationList);
            }
        }
        return getLabel();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelable(this.regionNode);
    }

    private RegionPathApiModel(Parcel in) {
        this.regionNode = (RegionNode) new ParcelReader(in).readParcelable(RegionNode.class);
    }

    static {
        CREATOR = new C12181();
    }

    @JsonIgnore
    public Identifier getIdentifier() {
        if (this.regionNode == null) {
            return null;
        }
        Identifier deeperIdentifier = this.regionNode.getIdentifier();
        RegionNode child = this.regionNode;
        while (child.getChildren() != null && child.getChildren().length == 1) {
            child = child.getChildren()[0];
            if (!(child.getIdentifier() == null || child.getIdentifier().isEmpty())) {
                deeperIdentifier = child.getIdentifier();
            }
        }
        return deeperIdentifier;
    }
}
