package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.internal.ServerProtocol;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.Map;

public class RequirementResponseApiModel extends ErrorResponseApiModel {
    public static Creator<RequirementResponseApiModel> CREATOR;
    @JsonProperty("requirement_info")
    protected Map<String, String> requirement_info;
    @JsonProperty("requirement_type")
    public String requirement_type;

    /* renamed from: com.schibsted.scm.nextgenapp.models.RequirementResponseApiModel.1 */
    static class C12201 implements Creator<RequirementResponseApiModel> {
        C12201() {
        }

        public RequirementResponseApiModel createFromParcel(Parcel source) {
            return new RequirementResponseApiModel(null);
        }

        public RequirementResponseApiModel[] newArray(int size) {
            return new RequirementResponseApiModel[size];
        }
    }

    private enum RequirementCode {
        PASSWORD_REQUIRED
    }

    public enum RequirementField {
        EMAIL("email"),
        ACCESS_TOKEN(ServerProtocol.DIALOG_PARAM_ACCESS_TOKEN);
        
        public final String field;

        private RequirementField(String field) {
            this.field = field;
        }
    }

    public boolean isRequirementTypePassword() {
        return RequirementCode.PASSWORD_REQUIRED.toString().equals(this.requirement_type);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeStringMap(this.requirement_info).writeString(this.requirement_type).writeParcelable(this.error);
    }

    private RequirementResponseApiModel(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.requirement_info = reader.readStringMap();
        this.requirement_type = reader.readString();
        this.error = (ErrorDescription) reader.readParcelable(ErrorDescription.class);
    }

    public String getRequirementField(RequirementField field, String defaultValue) {
        if (this.requirement_info == null || this.requirement_info.get(field.field) == null) {
            return defaultValue;
        }
        return (String) this.requirement_info.get(field.field);
    }

    static {
        CREATOR = new C12201();
    }
}
