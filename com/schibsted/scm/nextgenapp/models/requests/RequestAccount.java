package com.schibsted.scm.nextgenapp.models.requests;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class RequestAccount implements DataModel {
    public static Creator<RequestAccount> CREATOR;
    public String email;

    /* renamed from: com.schibsted.scm.nextgenapp.models.requests.RequestAccount.1 */
    static class C12461 implements Creator<RequestAccount> {
        C12461() {
        }

        public RequestAccount createFromParcel(Parcel source) {
            return new RequestAccount(null);
        }

        public RequestAccount[] newArray(int size) {
            return new RequestAccount[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.email);
    }

    private RequestAccount(Parcel in) {
        this.email = new ParcelReader(in).readString();
    }

    static {
        CREATOR = new C12461();
    }
}
