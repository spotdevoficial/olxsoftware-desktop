package com.schibsted.scm.nextgenapp.models.requests;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.models.submodels.AdDetailParameter;
import com.schibsted.scm.nextgenapp.models.submodels.AdParameter;
import com.schibsted.scm.nextgenapp.models.submodels.Category;
import com.schibsted.scm.nextgenapp.models.submodels.MediaData;
import com.schibsted.scm.nextgenapp.models.submodels.PriceParameter;
import com.schibsted.scm.nextgenapp.models.submodels.RegionNode;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@JsonInclude(Include.NON_EMPTY)
public class AdRequest implements DataModel {
    public static Creator<AdRequest> CREATOR;
    @JsonProperty(required = false, value = "ad_details")
    public Map<String, AdDetailParameter> adDetails;
    @JsonProperty(required = false, value = "body")
    public String body;
    @JsonProperty(required = false, value = "category")
    public GenericValue category;
    @JsonProperty(required = false, value = "images")
    public List<MediaData> images;
    @JsonProperty(required = false, value = "price")
    public PriceParameter price;
    @JsonIgnore
    public RegionPathApiModel region;
    @JsonProperty(required = false, value = "subject")
    public String subject;
    @JsonProperty(required = false, value = "type")
    public GenericValue type;

    /* renamed from: com.schibsted.scm.nextgenapp.models.requests.AdRequest.1 */
    static class C12401 implements Creator<AdRequest> {
        C12401() {
        }

        public AdRequest createFromParcel(Parcel source) {
            return new AdRequest(null);
        }

        public AdRequest[] newArray(int size) {
            return new AdRequest[size];
        }
    }

    @JsonProperty("locations")
    public RegionNode[] getChildren() {
        if (this.region != null) {
            return this.region.getLocations();
        }
        return null;
    }

    @JsonCreator
    public AdRequest(@JsonProperty("locations") RegionNode[] mChildren) {
        if (mChildren != null) {
            this.region = new RegionPathApiModel(mChildren);
        }
    }

    @JsonIgnore
    public Ad toAd(String privateId) {
        Ad ad;
        RegionNode[] regions = getChildren();
        if (regions != null) {
            ad = new Ad(regions);
        } else {
            ad = new Ad();
        }
        ad.subject = this.subject;
        ad.body = this.body;
        ad.adDetails = this.adDetails;
        if (this.category != null) {
            DbCategoryNode dbCategoryNode = C1049M.getDaoManager().getCategoryTree("insert_category_data").getByCode(this.category.code);
            if (dbCategoryNode != null) {
                ad.category = new Category(dbCategoryNode);
            }
        }
        ad.privateId = privateId;
        ad.mediaList = this.images;
        if (this.price != null) {
            ArrayList<PriceParameter> prices = new ArrayList();
            prices.add(this.price);
            ad.prices = prices;
        }
        if (this.type != null) {
            AdParameter typeParameter = new AdParameter();
            typeParameter.parameterCode = this.type.code;
            typeParameter.parameterLabel = this.type.label;
            ad.type = typeParameter;
        }
        return ad;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelableMap(this.adDetails).writeString(this.body).writeParcelable(this.category).writeParcelableList(this.images).writeParcelable(this.region).writeParcelable(this.price).writeString(this.subject).writeParcelable(this.type);
    }

    private AdRequest(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.adDetails = reader.readParcelableMap(AdDetailParameter.class);
        this.body = reader.readString();
        this.category = (GenericValue) reader.readParcelable(GenericValue.class);
        this.images = reader.readParcelableList(MediaData.CREATOR);
        this.region = (RegionPathApiModel) reader.readParcelable(RegionPathApiModel.class);
        this.price = (PriceParameter) reader.readParcelable(PriceParameter.class);
        this.subject = reader.readString();
        this.type = (GenericValue) reader.readParcelable(GenericValue.class);
    }

    static {
        CREATOR = new C12401();
    }
}
