package com.schibsted.scm.nextgenapp.models.requests;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

@JsonInclude(Include.NON_NULL)
public class GenericValue implements DataModel {
    public static Creator<GenericValue> CREATOR;
    @JsonProperty(required = false, value = "code")
    public String code;
    @JsonProperty(required = false, value = "label")
    public String label;

    /* renamed from: com.schibsted.scm.nextgenapp.models.requests.GenericValue.1 */
    static class C12431 implements Creator<GenericValue> {
        C12431() {
        }

        public GenericValue createFromParcel(Parcel source) {
            return new GenericValue(null);
        }

        public GenericValue[] newArray(int size) {
            return new GenericValue[size];
        }
    }

    public GenericValue(String code, String label) {
        this.code = code;
        this.label = label;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.label).writeString(this.code);
    }

    private GenericValue(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.label = reader.readString();
        this.code = reader.readString();
    }

    static {
        CREATOR = new C12431();
    }
}
