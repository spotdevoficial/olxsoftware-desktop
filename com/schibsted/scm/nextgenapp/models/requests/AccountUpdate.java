package com.schibsted.scm.nextgenapp.models.requests;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.submodels.MediaData;
import com.schibsted.scm.nextgenapp.models.submodels.RegionNode;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

@JsonInclude(Include.NON_NULL)
public class AccountUpdate implements DataModel {
    public static Creator<AccountUpdate> CREATOR;
    @JsonProperty(required = false, value = "facebook_account")
    public SocialMediaToken facebookAccount;
    @JsonProperty(required = false, value = "google_account")
    public SocialMediaToken googleAccount;
    @JsonProperty(required = false, value = "tax_identifier")
    public String identificationNumber;
    @JsonProperty(required = false, value = "image")
    public MediaData image;
    @JsonIgnore
    public RegionPathApiModel location;
    @JsonProperty(required = false, value = "name")
    public String name;
    @JsonProperty(required = false, value = "password")
    public String password;
    @JsonProperty(required = false, value = "phone")
    public String phone;
    @JsonProperty(required = false, value = "phone_hidden")
    public Boolean phoneHidden;
    @JsonProperty(required = false, value = "professional")
    public Boolean professional;

    /* renamed from: com.schibsted.scm.nextgenapp.models.requests.AccountUpdate.1 */
    static class C12341 implements Creator<AccountUpdate> {
        C12341() {
        }

        public AccountUpdate createFromParcel(Parcel source) {
            return new AccountUpdate(null);
        }

        public AccountUpdate[] newArray(int size) {
            return new AccountUpdate[size];
        }
    }

    public static final class Builder {
        private String facebookToken;
        private String googleToken;
        private String identificationNumber;
        private MediaData image;
        private RegionPathApiModel location;
        private String name;
        private String password;
        private String phone;
        private Boolean phoneHidden;
        private Boolean professional;

        public Builder setFacebookToken(String facebookToken) {
            this.facebookToken = facebookToken;
            return this;
        }

        public Builder setLocation(RegionPathApiModel location) {
            this.location = location;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setPassword(String password) {
            this.password = password;
            return this;
        }

        public Builder setPhone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder setIdentificationNumber(String identificationNumber) {
            this.identificationNumber = identificationNumber;
            return this;
        }

        public Builder setPhoneHidden(Boolean phoneHidden) {
            this.phoneHidden = phoneHidden;
            return this;
        }

        public Builder setProfessional(Boolean professional) {
            this.professional = professional;
            return this;
        }

        public AccountUpdate build() {
            return new AccountUpdate(this.facebookToken, this.googleToken, this.image, this.location, this.name, this.password, this.phone, this.phoneHidden, this.professional, this.identificationNumber);
        }
    }

    @JsonProperty(required = false, value = "locations")
    public RegionNode[] getLocations() {
        if (this.location != null) {
            return this.location.getLocations();
        }
        return null;
    }

    public AccountUpdate(String facebookToken, String googleToken, MediaData image, RegionPathApiModel location, String name, String password, String phone, Boolean phoneHidden, Boolean isProfessional, String identificationNumber) {
        if (facebookToken != null) {
            this.facebookAccount = new SocialMediaToken(facebookToken);
        } else {
            this.facebookAccount = null;
        }
        if (googleToken != null) {
            this.googleAccount = new SocialMediaToken(googleToken);
        } else {
            this.googleAccount = null;
        }
        if (image != null) {
            this.image = new MediaData(image);
        } else {
            this.image = null;
        }
        if (location != null) {
            this.location = new RegionPathApiModel(location);
        } else {
            this.location = null;
        }
        this.name = name;
        this.password = password;
        this.phone = phone;
        this.phoneHidden = phoneHidden;
        this.professional = isProfessional;
        this.identificationNumber = identificationNumber;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelable(this.facebookAccount).writeParcelable(this.googleAccount).writeParcelable(this.image).writeParcelable(this.location).writeString(this.name).writeString(this.password).writeString(this.phone).writeBoolean(this.phoneHidden).writeBoolean(this.professional).writeString(this.identificationNumber);
    }

    private AccountUpdate(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.facebookAccount = (SocialMediaToken) reader.readParcelable(SocialMediaToken.class);
        this.googleAccount = (SocialMediaToken) reader.readParcelable(SocialMediaToken.class);
        this.image = (MediaData) reader.readParcelable(MediaData.class);
        this.location = (RegionPathApiModel) reader.readParcelable(RegionPathApiModel.class);
        this.name = reader.readString();
        this.password = reader.readString();
        this.phone = reader.readString();
        this.phoneHidden = reader.readBoolean();
        this.professional = reader.readBoolean();
        this.identificationNumber = reader.readString();
    }

    static {
        CREATOR = new C12341();
    }
}
