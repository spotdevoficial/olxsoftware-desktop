package com.schibsted.scm.nextgenapp.models.requests;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class AdEventRequest implements DataModel {
    public static Creator<AdEventRequest> CREATOR;
    @JsonProperty("event_type")
    public String eventType;

    /* renamed from: com.schibsted.scm.nextgenapp.models.requests.AdEventRequest.1 */
    static class C12371 implements Creator<AdEventRequest> {
        C12371() {
        }

        public AdEventRequest createFromParcel(Parcel source) {
            return new AdEventRequest(source);
        }

        public AdEventRequest[] newArray(int size) {
            return new AdEventRequest[size];
        }
    }

    public AdEventRequest(String eventType) {
        this.eventType = eventType;
    }

    public AdEventRequest(Parcel in) {
        this.eventType = new ParcelReader(in).readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.eventType);
    }

    static {
        CREATOR = new C12371();
    }
}
