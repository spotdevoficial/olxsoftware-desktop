package com.schibsted.scm.nextgenapp.models.requests;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.AccountPasswordRequireModel;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

@JsonInclude(Include.NON_EMPTY)
public class RequirePasswordRequest implements DataModel {
    public static Creator<RequirePasswordRequest> CREATOR;
    @JsonProperty("account")
    public AccountPasswordRequireModel accountPasswordRequireModel;

    /* renamed from: com.schibsted.scm.nextgenapp.models.requests.RequirePasswordRequest.1 */
    static class C12471 implements Creator<RequirePasswordRequest> {
        C12471() {
        }

        public RequirePasswordRequest createFromParcel(Parcel source) {
            return new RequirePasswordRequest(null);
        }

        public RequirePasswordRequest[] newArray(int size) {
            return new RequirePasswordRequest[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelable(this.accountPasswordRequireModel);
    }

    private RequirePasswordRequest(Parcel in) {
        this.accountPasswordRequireModel = (AccountPasswordRequireModel) new ParcelReader(in).readParcelable(AccountPasswordRequireModel.class);
    }

    static {
        CREATOR = new C12471();
    }
}
