package com.schibsted.scm.nextgenapp.models.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;

public class FloatDeserializer extends JsonDeserializer<Float> {
    public Float deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return Float.valueOf(Float.parseFloat(jsonParser.getText().replace(",", ".")));
    }
}
