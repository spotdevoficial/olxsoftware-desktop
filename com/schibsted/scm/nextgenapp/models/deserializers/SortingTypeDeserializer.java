package com.schibsted.scm.nextgenapp.models.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.schibsted.scm.nextgenapp.models.types.SearchResultSortingType;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import java.io.IOException;

public class SortingTypeDeserializer extends JsonDeserializer<SearchResultSortingType> {
    public SearchResultSortingType deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        SearchResultSortingType type = SearchResultSortingType.getType(jsonParser.getValueAsString());
        if (type != null) {
            return type;
        }
        Logger.error("JsonDeserializer", "Could not parse String to Sorting type");
        throw new IOException();
    }
}
