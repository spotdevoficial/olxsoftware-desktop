package com.schibsted.scm.nextgenapp.models.deserializers;

import android.util.Pair;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;

public class PairSerializer extends JsonSerializer<Pair<String, String>> {
    public void serialize(Pair<String, String> value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeFieldName("first");
        jsonGenerator.writeString((String) value.first);
        jsonGenerator.writeFieldName("second");
        jsonGenerator.writeString((String) value.second);
        jsonGenerator.writeEndObject();
    }
}
