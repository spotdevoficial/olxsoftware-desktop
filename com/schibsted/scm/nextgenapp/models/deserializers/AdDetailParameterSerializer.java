package com.schibsted.scm.nextgenapp.models.deserializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.schibsted.scm.nextgenapp.models.submodels.AdDetailParameter;
import com.schibsted.scm.nextgenapp.models.submodels.AdParameter;
import com.schibsted.scm.nextgenapp.models.types.FilterType;
import java.io.IOException;

public class AdDetailParameterSerializer extends JsonSerializer<AdDetailParameter> {
    public void serialize(AdDetailParameter adDetailParameter, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        if (adDetailParameter.isMultiParameter() && adDetailParameter.getMultiple() != null) {
            jsonGenerator.writeFieldName(FilterType.CHOOSE_MULTIPLE);
            jsonGenerator.writeStartArray();
            for (AdParameter adParameter : adDetailParameter.getMultiple()) {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeFieldName("code");
                jsonGenerator.writeString(adParameter.parameterCode);
                jsonGenerator.writeEndObject();
            }
            jsonGenerator.writeEndArray();
        } else if (adDetailParameter.getSingle() != null) {
            jsonGenerator.writeFieldName("single");
            jsonGenerator.writeStartObject();
            jsonGenerator.writeFieldName("code");
            jsonGenerator.writeString(adDetailParameter.getSingle().parameterCode);
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndObject();
    }
}
