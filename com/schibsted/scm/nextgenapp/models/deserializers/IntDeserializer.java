package com.schibsted.scm.nextgenapp.models.deserializers;

import com.facebook.BuildConfig;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;

public class IntDeserializer extends JsonDeserializer<Integer> {
    public Integer deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String value = jsonParser.getText();
        if (value != null && !BuildConfig.VERSION_NAME.equals(value)) {
            return Integer.valueOf(Integer.parseInt(value));
        }
        throw new IOException("Expected value");
    }
}
