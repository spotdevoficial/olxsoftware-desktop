package com.schibsted.scm.nextgenapp.models.deserializers;

import android.text.TextUtils;
import com.facebook.BuildConfig;
import com.facebook.share.internal.ShareConstants;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.schibsted.scm.nextgenapp.models.requests.AccountUpdate;
import java.io.IOException;

public class AccountUpdateSerializer extends JsonSerializer<AccountUpdate> {
    public void serialize(AccountUpdate accountUpdate, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        if (accountUpdate.facebookAccount != null && TextUtils.isEmpty(accountUpdate.facebookAccount.token)) {
            jsonGenerator.writeObjectField("facebook_account", BuildConfig.VERSION_NAME);
        } else if (accountUpdate.facebookAccount != null) {
            jsonGenerator.writeObjectField("facebook_account", accountUpdate.facebookAccount);
        }
        if (accountUpdate.googleAccount != null) {
            jsonGenerator.writeObjectField("google_account", accountUpdate.googleAccount);
        }
        if (accountUpdate.name != null) {
            jsonGenerator.writeObjectField(ShareConstants.WEB_DIALOG_PARAM_NAME, accountUpdate.name);
        }
        if (accountUpdate.password != null) {
            jsonGenerator.writeObjectField("password", accountUpdate.password);
        }
        if (accountUpdate.phone != null) {
            jsonGenerator.writeObjectField("phone", accountUpdate.phone);
        }
        if (accountUpdate.phoneHidden != null) {
            jsonGenerator.writeObjectField("phoneHidden", accountUpdate.phoneHidden);
        }
        if (accountUpdate.professional != null) {
            jsonGenerator.writeObjectField("professional", accountUpdate.professional);
        }
        if (accountUpdate.identificationNumber != null) {
            jsonGenerator.writeObjectField("identificationNumber", accountUpdate.identificationNumber);
        }
        if (accountUpdate.getLocations() != null) {
            jsonGenerator.writeObjectField("locations", accountUpdate.getLocations());
        }
        if (accountUpdate.image != null) {
            jsonGenerator.writeObjectField("image", accountUpdate.image);
        }
        jsonGenerator.writeEndObject();
    }
}
