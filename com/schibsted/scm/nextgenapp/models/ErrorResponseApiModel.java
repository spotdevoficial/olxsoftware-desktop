package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class ErrorResponseApiModel implements DataModel {
    public static Creator<ErrorResponseApiModel> CREATOR;
    @JsonProperty("error")
    public ErrorDescription error;

    /* renamed from: com.schibsted.scm.nextgenapp.models.ErrorResponseApiModel.1 */
    static class C12091 implements Creator<ErrorResponseApiModel> {
        C12091() {
        }

        public ErrorResponseApiModel createFromParcel(Parcel source) {
            return new ErrorResponseApiModel(null);
        }

        public ErrorResponseApiModel[] newArray(int size) {
            return new ErrorResponseApiModel[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelable(this.error);
    }

    private ErrorResponseApiModel(Parcel in) {
        this.error = (ErrorDescription) new ParcelReader(in).readParcelable(ErrorDescription.class);
    }

    static {
        CREATOR = new C12091();
    }
}
