package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.BuildConfig;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.submodels.Account;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class SignInApiModel implements DataModel {
    public static Creator<SignInApiModel> CREATOR;
    @JsonProperty("access_token")
    String AccessToken;
    @JsonProperty("account")
    public Account account;
    @JsonProperty("token_type")
    public String tokenType;

    /* renamed from: com.schibsted.scm.nextgenapp.models.SignInApiModel.1 */
    static class C12221 implements Creator<SignInApiModel> {
        C12221() {
        }

        public SignInApiModel createFromParcel(Parcel source) {
            return new SignInApiModel(null);
        }

        public SignInApiModel[] newArray(int size) {
            return new SignInApiModel[size];
        }
    }

    public String getAccessToken() {
        return this.AccessToken;
    }

    public Account getAccount() {
        return this.account;
    }

    public String getTokenType() {
        return this.tokenType;
    }

    public SignInApiModel() {
        this.tokenType = BuildConfig.VERSION_NAME;
    }

    @JsonIgnore
    public boolean hasRegistrationToken() {
        return "tag:scmcoord.com,2013:registration".compareTo(this.tokenType) == 0;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.AccessToken).writeParcelable(this.account).writeString(this.tokenType);
    }

    private SignInApiModel(Parcel in) {
        this.tokenType = BuildConfig.VERSION_NAME;
        ParcelReader reader = new ParcelReader(in);
        this.AccessToken = reader.readString();
        this.account = (Account) reader.readParcelable(Account.class);
        this.tokenType = reader.readString();
    }

    static {
        CREATOR = new C12221();
    }
}
