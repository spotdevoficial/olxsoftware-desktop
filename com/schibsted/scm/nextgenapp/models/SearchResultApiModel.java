package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.config.SiteConfig;
import com.schibsted.scm.nextgenapp.models.deserializers.SortingTypeDeserializer;
import com.schibsted.scm.nextgenapp.models.types.SearchResultSortingType;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.List;
import java.util.Map;

public class SearchResultApiModel implements DataModel {
    public static String ALL_COUNTER;
    public static Creator<SearchResultApiModel> CREATOR;
    public static String NEW_COUNTER;
    @JsonProperty("list_ads")
    public List<AdDetailsApiModel> ads;
    @JsonProperty("config_etag")
    public String configEtag;
    @JsonProperty("counter_map")
    public Map<String, Integer> counters;
    @JsonProperty("next_page")
    public String nextPage;
    @JsonDeserialize(using = SortingTypeDeserializer.class)
    public SearchResultSortingType sorting;

    /* renamed from: com.schibsted.scm.nextgenapp.models.SearchResultApiModel.1 */
    static class C12211 implements Creator<SearchResultApiModel> {
        C12211() {
        }

        public SearchResultApiModel createFromParcel(Parcel source) {
            return new SearchResultApiModel(null);
        }

        public SearchResultApiModel[] newArray(int size) {
            return new SearchResultApiModel[size];
        }
    }

    static {
        ALL_COUNTER = "all";
        NEW_COUNTER = "new";
        CREATOR = new C12211();
    }

    @JsonIgnore
    public String getNormalizedConfigEtag() {
        String str = this.configEtag;
        ConfigContainer.getConfig();
        return Utils.normalizeEtag(str, SiteConfig.getApiVersion());
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeEnum(this.sorting).writeParcelableList(this.ads).writeString(this.nextPage).writeIntMap(this.counters).writeString(this.configEtag);
    }

    private SearchResultApiModel(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.sorting = (SearchResultSortingType) reader.readEnum(SearchResultSortingType.class);
        this.ads = reader.readParcelableList(AdDetailsApiModel.CREATOR);
        this.nextPage = reader.readString();
        this.counters = reader.readIntMap();
        this.configEtag = reader.readString();
    }
}
