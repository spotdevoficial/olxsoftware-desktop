package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class LatLong implements Parcelable {
    public static final Creator<LatLong> CREATOR;
    private double mLatitude;
    private double mLongitude;

    /* renamed from: com.schibsted.scm.nextgenapp.models.LatLong.1 */
    static class C12151 implements Creator<LatLong> {
        C12151() {
        }

        public LatLong createFromParcel(Parcel in) {
            return new LatLong(in);
        }

        public LatLong[] newArray(int size) {
            return new LatLong[size];
        }
    }

    public LatLong(double latitude, double longitude) {
        this.mLatitude = latitude;
        this.mLongitude = longitude;
    }

    public double getLatitude() {
        return this.mLatitude;
    }

    public double getLongitude() {
        return this.mLongitude;
    }

    protected LatLong(Parcel in) {
        this.mLatitude = in.readDouble();
        this.mLongitude = in.readDouble();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.mLatitude);
        dest.writeDouble(this.mLongitude);
    }

    static {
        CREATOR = new C12151();
    }
}
