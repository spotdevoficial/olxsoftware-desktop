package com.schibsted.scm.nextgenapp.models.interfaces;

public interface ListGroup {
    int getCount();
}
