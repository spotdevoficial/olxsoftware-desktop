package com.schibsted.scm.nextgenapp.models.interfaces;

import com.schibsted.scm.nextgenapp.models.submodels.Ad;

public interface AdContainer {
    Ad getAd();
}
