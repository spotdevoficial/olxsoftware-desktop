package com.schibsted.scm.nextgenapp.models.types;

import com.schibsted.scm.nextgenapp.models.DataModel;

public abstract class FilterType implements DataModel {
    public static final String CHOOSE_MULTIPLE = "multiple";
    public static final String CHOOSE_ONE = "choose_one";
    public static final String RANGE_DISTANCE = "distance_range";
    public static final String RANGE_INTEGER = "integer_range";
}
