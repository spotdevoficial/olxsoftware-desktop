package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.schibsted.scm.nextgenapp.models.deserializers.IntDeserializer;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.DualParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ValueListInteger;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class IntegerRangeParameterDefinition extends ParameterDefinition {
    public static Creator<IntegerRangeParameterDefinition> CREATOR;
    @JsonProperty("max")
    @JsonDeserialize(using = IntDeserializer.class)
    public Integer max;
    @JsonProperty(required = false, value = "max_label")
    public String maxLabel;
    @JsonProperty("min")
    @JsonDeserialize(using = IntDeserializer.class)
    public Integer min;
    @JsonProperty(required = false, value = "min_label")
    public String minLabel;
    @JsonProperty("step")
    @JsonDeserialize(using = IntDeserializer.class)
    public Integer step;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.IntegerRangeParameterDefinition.1 */
    static class C12761 implements Creator<IntegerRangeParameterDefinition> {
        C12761() {
        }

        public IntegerRangeParameterDefinition createFromParcel(Parcel source) {
            return new IntegerRangeParameterDefinition(null);
        }

        public IntegerRangeParameterDefinition[] newArray(int size) {
            return new IntegerRangeParameterDefinition[size];
        }
    }

    static {
        CREATOR = new C12761();
    }

    public ParameterValue getInitialValue() {
        return new DualParameterValue(this.min.intValue(), this.max.intValue());
    }

    public boolean isValidValue(ParameterValue value) {
        if (value == null) {
            return true;
        }
        try {
            boolean z;
            DualParameterValue parameterValue = (DualParameterValue) value;
            boolean foundFirst = true;
            boolean foundSecond = true;
            int actualMin = this.min == null ? Integer.MIN_VALUE : this.min.intValue();
            int actualMax = this.max == null ? Integer.MAX_VALUE : this.max.intValue();
            int actualStep = this.step == null ? 1 : this.step.intValue();
            if (!TextUtils.isEmpty((CharSequence) parameterValue.getValue().first)) {
                foundFirst = false;
                Integer first = Integer.valueOf(Integer.parseInt((String) parameterValue.getValue().first));
                if (first.intValue() >= actualMin && first.intValue() <= actualMax && (first.intValue() - actualMin) % actualStep == 0) {
                    foundFirst = true;
                }
            }
            if (!TextUtils.isEmpty((CharSequence) parameterValue.getValue().second)) {
                foundSecond = false;
                Integer second = Integer.valueOf(Integer.parseInt((String) parameterValue.getValue().second));
                if (second.intValue() >= actualMin && second.intValue() <= actualMax && (second.intValue() - actualMin) % actualStep == 0) {
                    foundSecond = true;
                }
            }
            if (foundFirst && foundSecond) {
                z = true;
            } else {
                z = false;
            }
            return z;
        } catch (ClassCastException e) {
            return false;
        }
    }

    public IntegerRangeParameterDefinition() {
        this.min = Integer.valueOf(Integer.MIN_VALUE);
        this.max = Integer.valueOf(Integer.MAX_VALUE);
        this.step = Integer.valueOf(1);
    }

    public int getSearchFilterType() {
        return 5;
    }

    public int describeContents() {
        return super.describeContents();
    }

    public boolean equals(Object o) {
        if (!super.equals(o)) {
            return false;
        }
        if (!Utils.compare(this.min, ((IntegerRangeParameterDefinition) o).min)) {
            return false;
        }
        if (!Utils.compare(this.max, ((IntegerRangeParameterDefinition) o).max)) {
            return false;
        }
        if (Utils.compare(this.step, ((IntegerRangeParameterDefinition) o).step)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int result;
        int hashCode;
        int i = 0;
        if (this.min != null) {
            result = this.min.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.max != null) {
            hashCode = this.max.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (i2 + hashCode) * 31;
        if (this.step != null) {
            i = this.step.hashCode();
        }
        return hashCode + i;
    }

    public ParameterDefinition copy() {
        return new IntegerRangeParameterDefinition(this);
    }

    public boolean isValid() {
        return hasLabel() && hasKey() && hasPresentation() && this.min != null && this.max != null && this.step != null && this.min.intValue() <= this.max.intValue();
    }

    private IntegerRangeParameterDefinition(Parcel in) {
        super(in);
        this.min = Integer.valueOf(Integer.MIN_VALUE);
        this.max = Integer.valueOf(Integer.MAX_VALUE);
        this.step = Integer.valueOf(1);
        ParcelReader reader = new ParcelReader(in);
        this.min = reader.readInt();
        this.max = reader.readInt();
        this.step = reader.readInt();
        this.maxLabel = reader.readString();
        this.minLabel = reader.readString();
    }

    public IntegerRangeParameterDefinition(IntegerRangeParameterDefinition definition) {
        super((ParameterDefinition) definition);
        this.min = Integer.valueOf(Integer.MIN_VALUE);
        this.max = Integer.valueOf(Integer.MAX_VALUE);
        this.step = Integer.valueOf(1);
        this.min = definition.min;
        this.max = definition.max;
        this.step = definition.step;
        this.maxLabel = definition.maxLabel;
        this.minLabel = definition.minLabel;
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        new ParcelWriter(dest, flags).writeInt(this.min).writeInt(this.max).writeInt(this.step).writeString(this.maxLabel).writeString(this.minLabel);
    }

    public ValueListInteger getValueList() {
        return new ValueListInteger(this.min.intValue(), this.max.intValue(), this.step.intValue());
    }
}
