package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.deserializers.DoubleDeserializer;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class Coordinate implements DataModel {
    public static Creator<Coordinate> CREATOR;
    @JsonProperty(required = true, value = "lat")
    @JsonDeserialize(using = DoubleDeserializer.class)
    public double latitude;
    @JsonProperty(required = true, value = "long")
    @JsonDeserialize(using = DoubleDeserializer.class)
    public double longitude;
    @JsonProperty(required = true, value = "radius")
    public int radius;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.Coordinate.1 */
    static class C12641 implements Creator<Coordinate> {
        C12641() {
        }

        public Coordinate createFromParcel(Parcel source) {
            return new Coordinate(null);
        }

        public Coordinate[] newArray(int size) {
            return new Coordinate[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public Coordinate(Coordinate coordinate) {
        this.latitude = coordinate.latitude;
        this.longitude = coordinate.longitude;
        this.radius = coordinate.radius;
    }

    public Coordinate(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeDouble(Double.valueOf(this.longitude)).writeDouble(Double.valueOf(this.latitude)).writeInt(Integer.valueOf(this.radius));
    }

    private Coordinate(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.longitude = reader.readDouble().doubleValue();
        this.latitude = reader.readDouble().doubleValue();
        this.radius = reader.readInt().intValue();
    }

    static {
        CREATOR = new C12641();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Coordinate that = (Coordinate) o;
        if (Double.compare(that.latitude, this.latitude) != 0) {
            return false;
        }
        if (Double.compare(that.longitude, this.longitude) != 0) {
            return false;
        }
        if (this.radius != that.radius) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        long temp = Double.doubleToLongBits(this.longitude);
        int result = (int) ((temp >>> 32) ^ temp);
        temp = Double.doubleToLongBits(this.latitude);
        return (((result * 31) + ((int) ((temp >>> 32) ^ temp))) * 31) + this.radius;
    }
}
