package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.BuildConfig;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.config.SiteConfig;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class Prefetch implements DataModel {
    public static Creator<Prefetch> CREATOR;
    @JsonProperty(required = false, value = "etag")
    public String etag;
    @JsonProperty("path")
    public String path;
    @JsonProperty(required = false, value = "url")
    public String url;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.Prefetch.1 */
    static class C12851 implements Creator<Prefetch> {
        C12851() {
        }

        public Prefetch createFromParcel(Parcel source) {
            return new Prefetch(null);
        }

        public Prefetch[] newArray(int size) {
            return new Prefetch[size];
        }
    }

    @JsonIgnore
    public String getNormalizedEtag() {
        String str = this.etag;
        ConfigContainer.getConfig();
        return Utils.normalizeEtag(str, SiteConfig.getApiVersion());
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.path).writeString(this.url).writeString(this.etag);
    }

    public Prefetch() {
        this.etag = BuildConfig.VERSION_NAME;
    }

    private Prefetch(Parcel in) {
        this.etag = BuildConfig.VERSION_NAME;
        ParcelReader reader = new ParcelReader(in);
        this.path = reader.readString();
        this.url = reader.readString();
        this.etag = reader.readString();
    }

    static {
        CREATOR = new C12851();
    }
}
