package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.List;

@JsonInclude(Include.NON_NULL)
public class CategoryImage implements DataModel {
    public static Creator<CategoryImage> CREATOR;
    private List<CategoryImage> children;
    public String code;
    @JsonProperty("filter_value")
    public Identifier identifier;
    public String label;
    protected CategoryImage mParent;
    @JsonProperty("ad")
    public Ad sampleAd;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.CategoryImage.1 */
    static class C12631 implements Creator<CategoryImage> {
        C12631() {
        }

        public CategoryImage createFromParcel(Parcel source) {
            return new CategoryImage(source);
        }

        public CategoryImage[] newArray(int size) {
            return new CategoryImage[size];
        }
    }

    static {
        CREATOR = new C12631();
    }

    public List<CategoryImage> getChildren() {
        return this.children;
    }

    @JsonProperty(required = false, value = "categories")
    public void setChildren(List<CategoryImage> mChildren) {
        this.children = mChildren;
        for (CategoryImage child : this.children) {
            child.setParent(this);
        }
    }

    public CategoryImage getParent() {
        return this.mParent;
    }

    public void setParent(CategoryImage parent) {
        this.mParent = parent;
    }

    public int describeContents() {
        return 0;
    }

    protected CategoryImage(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.code = reader.readString();
        this.label = reader.readString();
        this.identifier = (Identifier) reader.readParcelable(Identifier.class);
        this.children = reader.readParcelableList(CREATOR);
        this.sampleAd = (Ad) reader.readParcelable(Ad.class);
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.code).writeString(this.label).writeParcelable(this.identifier).writeParcelableList(this.children).writeParcelable(this.sampleAd);
    }
}
