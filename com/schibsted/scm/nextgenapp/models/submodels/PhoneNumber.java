package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.deserializers.BooleanDeserializer;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class PhoneNumber implements DataModel {
    public static Creator<PhoneNumber> CREATOR;
    public String label;
    @JsonDeserialize(using = BooleanDeserializer.class)
    public Boolean sms;
    public String value;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.PhoneNumber.1 */
    static class C12841 implements Creator<PhoneNumber> {
        C12841() {
        }

        public PhoneNumber createFromParcel(Parcel source) {
            return new PhoneNumber(null);
        }

        public PhoneNumber[] newArray(int size) {
            return new PhoneNumber[size];
        }
    }

    public PhoneNumber() {
        this.sms = Boolean.valueOf(true);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.label).writeString(this.value).writeBoolean(this.sms);
    }

    private PhoneNumber(Parcel in) {
        this.sms = Boolean.valueOf(true);
        ParcelReader reader = new ParcelReader(in);
        this.label = reader.readString();
        this.value = reader.readString();
        this.sms = reader.readBoolean();
    }

    static {
        CREATOR = new C12841();
    }
}
