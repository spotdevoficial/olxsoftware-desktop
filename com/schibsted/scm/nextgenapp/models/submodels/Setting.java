package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.List;

public class Setting implements DataModel {
    public static Creator<Setting> CREATOR = null;
    public static final String FALSE = "false";
    public static final String TRUE = "true";
    public static final String WILDCARD = "*";
    @JsonProperty("settings_result")
    public List<String> settingsResult;
    @JsonProperty("values")
    public List<String> values;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.Setting.1 */
    static class C12891 implements Creator<Setting> {
        C12891() {
        }

        public Setting createFromParcel(Parcel source) {
            return new Setting(null);
        }

        public Setting[] newArray(int size) {
            return new Setting[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeStringList(this.values).writeStringList(this.settingsResult);
    }

    private Setting(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.values = reader.readStringList();
        this.settingsResult = reader.readStringList();
    }

    static {
        CREATOR = new C12891();
    }
}
