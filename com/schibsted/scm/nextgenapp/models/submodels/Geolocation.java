package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.List;

public class Geolocation implements DataModel {
    public static Creator<Geolocation> CREATOR;
    @JsonProperty(required = false, value = "code")
    public String code;
    @JsonProperty(required = false, value = "coordinates")
    public Coordinate coordinates;
    @JsonProperty(required = false, value = "identifier")
    public Identifier identifier;
    @JsonProperty(required = false, value = "key")
    public String key;
    @JsonProperty(required = false, value = "label")
    public String label;
    @JsonProperty(required = false, value = "locations")
    public List<Geolocation> locations;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.Geolocation.1 */
    static class C12711 implements Creator<Geolocation> {
        C12711() {
        }

        public Geolocation createFromParcel(Parcel source) {
            return new Geolocation(null);
        }

        public Geolocation[] newArray(int size) {
            return new Geolocation[size];
        }
    }

    @JsonCreator
    public Geolocation(@JsonProperty("lat") double lat, @JsonProperty("lon") double lon) {
        this.coordinates = new Coordinate();
        this.coordinates.latitude = lat;
        this.coordinates.longitude = lon;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.code).writeParcelable(this.coordinates).writeParcelable(this.identifier).writeString(this.key).writeString(this.label).writeParcelableList(this.locations);
    }

    private Geolocation(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.code = reader.readString();
        this.coordinates = (Coordinate) reader.readParcelable(Coordinate.class);
        this.identifier = (Identifier) reader.readParcelable(Identifier.class);
        this.key = reader.readString();
        this.label = reader.readString();
        this.locations = reader.readParcelableList(CREATOR);
    }

    static {
        CREATOR = new C12711();
    }
}
