package com.schibsted.scm.nextgenapp.models.submodels;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.deserializers.IntDeserializer;
import com.schibsted.scm.nextgenapp.utils.ImageSize;
import com.schibsted.scm.nextgenapp.utils.UrlConstructor;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class MediaData implements DataModel {
    public static Creator<MediaData> CREATOR;
    @JsonProperty(required = true, value = "base_url")
    public String mediaBaseUrl;
    @JsonProperty(required = false, value = "height")
    @JsonDeserialize(using = IntDeserializer.class)
    public int mediaHeight;
    @JsonProperty(required = true, value = "media_id")
    public String mediaId;
    @JsonProperty(required = true, value = "path")
    public String mediaPath;
    @JsonProperty(required = false, value = "width")
    @JsonDeserialize(using = IntDeserializer.class)
    public int mediaWidth;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.MediaData.1 */
    static class C12801 implements Creator<MediaData> {
        C12801() {
        }

        public MediaData createFromParcel(Parcel source) {
            return new MediaData(null);
        }

        public MediaData[] newArray(int size) {
            return new MediaData[size];
        }
    }

    public MediaData(MediaData mediaData) {
        this.mediaBaseUrl = mediaData.mediaBaseUrl;
        this.mediaHeight = mediaData.mediaHeight;
        this.mediaId = mediaData.mediaId;
        this.mediaPath = mediaData.mediaPath;
        this.mediaWidth = mediaData.mediaWidth;
    }

    @JsonIgnore
    public String getResourceURL(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(metrics);
        return getResourceURL(Utils.calculatePreferredImageSize(((float) metrics.widthPixels) / metrics.density, true));
    }

    @JsonIgnore
    public String getLargestResourceURL() {
        return getResourceURL(ImageSize.values()[ImageSize.values().length - 1].getKey());
    }

    @JsonIgnore
    public String getResourceURL(String size) {
        if (size == null || this.mediaBaseUrl == null || this.mediaPath == null) {
            return null;
        }
        return UrlConstructor.concatenate(UrlConstructor.concatenate(this.mediaBaseUrl, size), this.mediaPath);
    }

    @JsonIgnore
    public String getThumbURL() {
        if (this.mediaId == null || (this.mediaBaseUrl == null && this.mediaPath == null)) {
            return null;
        }
        return UrlConstructor.concatenate(this.mediaBaseUrl.contains("staticflickr") ? this.mediaBaseUrl : UrlConstructor.concatenate(this.mediaBaseUrl, "thumbs"), this.mediaPath);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.mediaId).writeString(this.mediaBaseUrl).writeString(this.mediaPath).writeInt(Integer.valueOf(this.mediaWidth)).writeInt(Integer.valueOf(this.mediaHeight));
    }

    private MediaData(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.mediaId = reader.readString();
        this.mediaBaseUrl = reader.readString();
        this.mediaPath = reader.readString();
        this.mediaWidth = reader.readInt().intValue();
        this.mediaHeight = reader.readInt().intValue();
    }

    static {
        CREATOR = new C12801();
    }
}
