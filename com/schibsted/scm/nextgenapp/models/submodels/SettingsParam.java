package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.List;

public class SettingsParam implements DataModel {
    public static Creator<SettingsParam> CREATOR;
    @JsonProperty(required = true, value = "keys")
    public List<String> keys;
    @JsonProperty(required = true, value = "settings")
    public List<Setting> settings;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.SettingsParam.1 */
    static class C12911 implements Creator<SettingsParam> {
        C12911() {
        }

        public SettingsParam createFromParcel(Parcel source) {
            return new SettingsParam(null);
        }

        public SettingsParam[] newArray(int size) {
            return new SettingsParam[size];
        }
    }

    static {
        CREATOR = new C12911();
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeStringList(this.keys).writeParcelableList(this.settings);
    }

    private SettingsParam(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.keys = reader.readStringList();
        this.settings = reader.readParcelableList(Setting.CREATOR);
    }

    public int describeContents() {
        return 0;
    }
}
