package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.deserializers.BooleanDeserializer;
import com.schibsted.scm.nextgenapp.models.submodels.social_accounts.FacebookAccount;
import com.schibsted.scm.nextgenapp.models.submodels.social_accounts.GoogleAccount;
import com.schibsted.scm.nextgenapp.models.submodels.social_accounts.TwitterAccount;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

@JsonInclude(Include.NON_EMPTY)
public class Account implements DataModel {
    public static Creator<Account> CREATOR;
    @JsonProperty("account_id")
    public String accountId;
    @JsonProperty(required = false, value = "can_publish")
    @JsonDeserialize(using = BooleanDeserializer.class)
    public Boolean canPublish;
    @JsonProperty(required = false, value = "email")
    public String email;
    @JsonProperty(required = false, value = "facebook_account")
    public FacebookAccount facebookAccount;
    @JsonProperty(required = false, value = "google_account")
    private GoogleAccount googleAccount;
    @JsonProperty(required = false, value = "tax_identifier")
    public String identifier_number;
    @JsonProperty(required = false, value = "image")
    public MediaData image;
    @JsonProperty(required = false, value = "name")
    public String name;
    @JsonProperty(required = false, value = "phone")
    public String phone;
    @JsonProperty(required = false, value = "phone_hidden")
    @JsonDeserialize(using = BooleanDeserializer.class)
    public Boolean phoneHidden;
    @JsonProperty(required = false, value = "professional")
    @JsonDeserialize(using = BooleanDeserializer.class)
    public Boolean professional;
    @JsonIgnore
    private RegionPathApiModel region;
    @JsonProperty(required = false, value = "twitter_account")
    public TwitterAccount twitterAccount;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.Account.1 */
    static class C12491 implements Creator<Account> {
        C12491() {
        }

        public Account createFromParcel(Parcel source) {
            return new Account(source);
        }

        public Account[] newArray(int size) {
            return new Account[size];
        }
    }

    @JsonIgnore
    public RegionPathApiModel getRegion() {
        return this.region;
    }

    @JsonIgnore
    public void setRegion(RegionPathApiModel newRegion) {
        this.region = newRegion;
    }

    @JsonProperty(required = false, value = "locations")
    public RegionNode[] getLocations() {
        if (this.region != null) {
            return this.region.getLocations();
        }
        return null;
    }

    @JsonCreator
    public Account(@JsonProperty(required = false, value = "locations") RegionNode[] mChildren) {
        this.professional = Boolean.valueOf(false);
        this.canPublish = Boolean.valueOf(true);
        if (mChildren != null && mChildren.length > 0) {
            this.region = new RegionPathApiModel(mChildren);
        }
    }

    public Account() {
        this.professional = Boolean.valueOf(false);
        this.canPublish = Boolean.valueOf(true);
    }

    @JsonIgnore
    public String getCleanId() {
        String[] split = this.accountId.split("/");
        return split[split.length - 1];
    }

    public Account(Account account) {
        this.professional = Boolean.valueOf(false);
        this.canPublish = Boolean.valueOf(true);
        this.accountId = account.accountId;
        this.email = account.email;
        this.name = account.name;
        this.phone = account.phone;
        this.identifier_number = account.identifier_number;
        this.canPublish = account.canPublish;
        if (account.facebookAccount != null) {
            this.facebookAccount = new FacebookAccount(account.facebookAccount);
        }
        if (account.googleAccount != null) {
            this.googleAccount = new GoogleAccount(account.googleAccount);
        }
        if (account.twitterAccount != null) {
            this.twitterAccount = new TwitterAccount();
        }
        if (account.image != null) {
            this.image = new MediaData(account.image);
        }
        if (account.region != null) {
            this.region = new RegionPathApiModel(account.region);
        }
        if (account.phoneHidden != null) {
            this.phoneHidden = Boolean.valueOf(account.phoneHidden.booleanValue());
        }
        if (account.professional != null) {
            this.professional = Boolean.valueOf(account.professional.booleanValue());
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.accountId).writeString(this.email).writeString(this.name).writeString(this.phone).writeString(this.identifier_number).writeBoolean(this.phoneHidden).writeBoolean(this.professional).writeBoolean(this.canPublish).writeParcelable(this.twitterAccount).writeParcelable(this.facebookAccount).writeParcelable(this.googleAccount).writeParcelable(this.region).writeParcelable(this.image);
    }

    protected Account(Parcel in) {
        this.professional = Boolean.valueOf(false);
        this.canPublish = Boolean.valueOf(true);
        ParcelReader reader = new ParcelReader(in);
        this.accountId = reader.readString();
        this.email = reader.readString();
        this.name = reader.readString();
        this.phone = reader.readString();
        this.identifier_number = reader.readString();
        this.phoneHidden = reader.readBoolean();
        this.professional = reader.readBoolean();
        this.canPublish = reader.readBoolean();
        this.twitterAccount = (TwitterAccount) reader.readParcelable(TwitterAccount.class);
        this.facebookAccount = (FacebookAccount) reader.readParcelable(FacebookAccount.class);
        this.googleAccount = (GoogleAccount) reader.readParcelable(GoogleAccount.class);
        this.region = (RegionPathApiModel) reader.readParcelable(RegionPathApiModel.class);
        this.image = (MediaData) reader.readParcelable(MediaData.class);
    }

    static {
        CREATOR = new C12491();
    }
}
