package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.List;

public class SettingsChoice implements DataModel {
    public static Creator<SettingsChoice> CREATOR;
    @JsonProperty(required = false, value = "setting_result")
    public List<String> filters;
    @JsonProperty("settings")
    public List<Setting> settings;
    @JsonProperty(required = false, value = "value")
    public String value;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.SettingsChoice.1 */
    static class C12901 implements Creator<SettingsChoice> {
        C12901() {
        }

        public SettingsChoice createFromParcel(Parcel source) {
            return new SettingsChoice(null);
        }

        public SettingsChoice[] newArray(int size) {
            return new SettingsChoice[size];
        }
    }

    static {
        CREATOR = new C12901();
    }

    private SettingsChoice(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.value = reader.readString();
        this.filters = reader.readStringList();
        this.settings = reader.readParcelableList(Setting.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.value).writeStringList(this.filters).writeParcelableList(this.settings);
    }
}
