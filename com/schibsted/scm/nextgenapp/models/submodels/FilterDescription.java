package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.List;
import java.util.Map;

public class FilterDescription implements DataModel {
    public static Creator<FilterDescription> CREATOR;
    @JsonProperty("param_map")
    public Map<String, ParameterDefinition> paramMap;
    @JsonProperty("settings_param")
    public List<SettingsParam> settingsParam;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.FilterDescription.1 */
    static class C12701 implements Creator<FilterDescription> {
        C12701() {
        }

        public FilterDescription createFromParcel(Parcel source) {
            return new FilterDescription(null);
        }

        public FilterDescription[] newArray(int size) {
            return new FilterDescription[size];
        }
    }

    static {
        CREATOR = new C12701();
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelableList(this.settingsParam).writeInheritedParcelableMap(this.paramMap, ParameterDefinition.CREATOR);
    }

    private FilterDescription(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.settingsParam = reader.readParcelableList(SettingsParam.CREATOR);
        this.paramMap = reader.readInheritedParcelableMap(ParameterDefinition.CREATOR);
    }

    public int describeContents() {
        return 0;
    }
}
