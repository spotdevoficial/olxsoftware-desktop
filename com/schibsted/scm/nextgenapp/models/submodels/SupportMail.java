package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class SupportMail implements DataModel {
    public static Creator<SupportMail> CREATOR;
    @JsonProperty("email")
    public String email;
    @JsonProperty("id")
    public String id;
    @JsonProperty("label")
    public String label;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.SupportMail.1 */
    static class C12961 implements Creator<SupportMail> {
        C12961() {
        }

        public SupportMail createFromParcel(Parcel source) {
            return new SupportMail(null);
        }

        public SupportMail[] newArray(int size) {
            return new SupportMail[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.label).writeString(this.email).writeString(this.id);
    }

    private SupportMail(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.label = reader.readString();
        this.email = reader.readString();
        this.id = reader.readString();
    }

    static {
        CREATOR = new C12961();
    }
}
