package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.text.TextUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.deserializers.BooleanDeserializer;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelableHelper;
import java.security.InvalidParameterException;

@JsonTypeInfo(include = As.WRAPPER_OBJECT, use = Id.NAME)
@JsonSubTypes({@Type(name = "list_range", value = ListRangeParameterDefinition.class), @Type(name = "single_selection", value = SingleSelectionParameterDefinition.class), @Type(name = "multi_selection", value = MultipleSelectionParameterDefinition.class), @Type(name = "boolean", value = BooleanParameterDefinition.class), @Type(name = "text", value = TextParameterDefinition.class), @Type(name = "integer_range", value = IntegerRangeParameterDefinition.class), @Type(name = "date_range", value = DateRangeParameterDefinition.class)})
public abstract class ParameterDefinition implements DataModel {
    public static ParcelableHelper<ParameterDefinition> CREATOR = null;
    public static final int TYPE_BOOLEAN = 3;
    public static final int TYPE_DATE_RANGE = 6;
    public static final int TYPE_INTEGER_RANGE = 5;
    public static final int TYPE_LIST_RANGE = 0;
    public static final int TYPE_MULTI_SELECTOR = 2;
    public static final int TYPE_SINGLE_SELECTOR = 1;
    public static final int TYPE_TEXT = 4;
    @JsonProperty(required = false, value = "child")
    public String child;
    @JsonProperty(required = false, value = "data_url")
    public String dataUrl;
    @JsonProperty(required = false, value = "param_key")
    public String key;
    @JsonProperty(required = false, value = "label")
    private String label;
    @JsonProperty(required = false, value = "parent")
    public String parent;
    @JsonProperty(required = false, value = "presentation")
    public String presentation;
    @JsonProperty(required = false, value = "required")
    @JsonDeserialize(using = BooleanDeserializer.class)
    public Boolean required;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.ParameterDefinition.1 */
    static class C12831 implements ParcelableHelper<ParameterDefinition> {
        C12831() {
        }

        public ParameterDefinition createFromParcel(Parcel source) {
            return ParameterDefinition.newInstance(source.readInt());
        }

        public ParameterDefinition[] newArray(int size) {
            return new ParameterDefinition[size];
        }

        public int getType(Class<? extends ParameterDefinition> clazz) {
            if (clazz == ListRangeParameterDefinition.class) {
                return ParameterDefinition.TYPE_LIST_RANGE;
            }
            if (clazz == SingleSelectionParameterDefinition.class) {
                return ParameterDefinition.TYPE_SINGLE_SELECTOR;
            }
            if (clazz == MultipleSelectionParameterDefinition.class) {
                return ParameterDefinition.TYPE_MULTI_SELECTOR;
            }
            if (clazz == BooleanParameterDefinition.class) {
                return ParameterDefinition.TYPE_BOOLEAN;
            }
            if (clazz == TextParameterDefinition.class) {
                return ParameterDefinition.TYPE_TEXT;
            }
            if (clazz == IntegerRangeParameterDefinition.class) {
                return ParameterDefinition.TYPE_INTEGER_RANGE;
            }
            if (clazz == DateRangeParameterDefinition.class) {
                return ParameterDefinition.TYPE_DATE_RANGE;
            }
            throw new InvalidParameterException(clazz.getName());
        }
    }

    public abstract ParameterDefinition copy();

    @JsonIgnore
    public abstract boolean isValidValue(ParameterValue parameterValue);

    public String getLabel() {
        return this.label;
    }

    public String getLabelWithRequirementIndicator() {
        if (this.required == null || !this.required.booleanValue()) {
            return this.label;
        }
        StringBuilder append = new StringBuilder().append(this.label);
        ConfigContainer.getConfig().getClass();
        return append.append(" *").toString();
    }

    @JsonIgnore
    public Boolean isBasedOnDatabase() {
        return Boolean.valueOf(false);
    }

    @JsonIgnore
    public Boolean isBasedOnLocation() {
        return Boolean.valueOf(false);
    }

    @JsonIgnore
    public ParameterValue getInitialValue() {
        return null;
    }

    protected ParameterDefinition() {
        this.presentation = null;
        this.label = null;
        this.key = null;
        this.parent = null;
        this.dataUrl = null;
        this.child = null;
        this.required = Boolean.valueOf(false);
    }

    public boolean hasPresentation() {
        return !TextUtils.isEmpty(this.presentation);
    }

    public boolean hasLabel() {
        return !TextUtils.isEmpty(getLabel());
    }

    public boolean hasKey() {
        return !TextUtils.isEmpty(this.key);
    }

    @JsonIgnore
    public int getSearchFilterType() {
        return TYPE_LIST_RANGE;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.label).writeString(this.key).writeString(this.presentation).writeString(this.child).writeString(this.parent).writeString(this.dataUrl).writeBoolean(this.required);
    }

    public ParameterDefinition(Parcel in) {
        this.presentation = null;
        this.label = null;
        this.key = null;
        this.parent = null;
        this.dataUrl = null;
        this.child = null;
        this.required = Boolean.valueOf(false);
        ParcelReader reader = new ParcelReader(in);
        this.label = reader.readString();
        this.key = reader.readString();
        this.presentation = reader.readString();
        this.child = reader.readString();
        this.parent = reader.readString();
        this.dataUrl = reader.readString();
        this.required = reader.readBoolean();
    }

    public ParameterDefinition(ParameterDefinition definition) {
        this.presentation = null;
        this.label = null;
        this.key = null;
        this.parent = null;
        this.dataUrl = null;
        this.child = null;
        this.required = Boolean.valueOf(false);
        this.label = definition.label;
        this.key = definition.key;
        this.presentation = definition.presentation;
        this.child = definition.child;
        this.parent = definition.parent;
        this.dataUrl = definition.dataUrl;
        this.required = Boolean.valueOf(definition.required.booleanValue());
    }

    public int describeContents() {
        return TYPE_LIST_RANGE;
    }

    @JsonIgnore
    public boolean isValid() {
        return false;
    }

    public boolean equals(Object o) {
        if (!o.getClass().equals(getClass())) {
            return false;
        }
        ParameterDefinition output = (ParameterDefinition) o;
        if (Utils.compare(this.presentation, output.presentation) && Utils.compare(this.key, output.key) && Utils.compare(this.presentation, output.presentation) && Utils.compare(this.child, output.child) && Utils.compare(this.dataUrl, output.dataUrl) && Utils.compare(this.label, output.label) && Utils.compare(this.required, output.required) && Utils.compare(this.parent, output.parent)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int result;
        int hashCode;
        int i = TYPE_LIST_RANGE;
        if (this.presentation != null) {
            result = this.presentation.hashCode();
        } else {
            result = TYPE_LIST_RANGE;
        }
        int i2 = result * 31;
        if (this.label != null) {
            hashCode = this.label.hashCode();
        } else {
            hashCode = TYPE_LIST_RANGE;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.key != null) {
            hashCode = this.key.hashCode();
        } else {
            hashCode = TYPE_LIST_RANGE;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.parent != null) {
            hashCode = this.parent.hashCode();
        } else {
            hashCode = TYPE_LIST_RANGE;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.dataUrl != null) {
            hashCode = this.dataUrl.hashCode();
        } else {
            hashCode = TYPE_LIST_RANGE;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.child != null) {
            hashCode = this.child.hashCode();
        } else {
            hashCode = TYPE_LIST_RANGE;
        }
        hashCode = (i2 + hashCode) * 31;
        if (this.required != null) {
            i = this.required.hashCode();
        }
        return hashCode + i;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @JsonIgnore
    public static ParameterDefinition newInstance(int type) {
        switch (type) {
            case TYPE_LIST_RANGE /*0*/:
                return new ListRangeParameterDefinition();
            case TYPE_SINGLE_SELECTOR /*1*/:
                return new SingleSelectionParameterDefinition();
            case TYPE_MULTI_SELECTOR /*2*/:
                return new MultipleSelectionParameterDefinition();
            case TYPE_BOOLEAN /*3*/:
                return new BooleanParameterDefinition();
            case TYPE_TEXT /*4*/:
                return new TextParameterDefinition();
            case TYPE_INTEGER_RANGE /*5*/:
                return new IntegerRangeParameterDefinition();
            case TYPE_DATE_RANGE /*6*/:
                return new DateRangeParameterDefinition();
            default:
                return null;
        }
    }

    static {
        CREATOR = new C12831();
    }
}
