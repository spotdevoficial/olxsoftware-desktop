package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.facebook.share.internal.ShareConstants;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class AdUserInfo implements DataModel {
    public static Creator<AdUserInfo> CREATOR;
    @JsonIgnore
    public String accountName;
    @JsonIgnore
    public String userName;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.AdUserInfo.1 */
    static class C12571 implements Creator<AdUserInfo> {
        C12571() {
        }

        public AdUserInfo createFromParcel(Parcel source) {
            return new AdUserInfo(null);
        }

        public AdUserInfo[] newArray(int size) {
            return new AdUserInfo[size];
        }
    }

    @JsonProperty(required = false, value = "account")
    public void setAccountName(JsonNode node) {
        this.accountName = node.get(ShareConstants.WEB_DIALOG_PARAM_NAME).textValue();
    }

    @JsonProperty(required = false, value = "user")
    public void setUserName(JsonNode node) {
        this.userName = node.get(ShareConstants.WEB_DIALOG_PARAM_NAME).textValue();
    }

    public String getName() {
        if (TextUtils.isEmpty(this.accountName)) {
            return this.userName;
        }
        return this.accountName;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.accountName).writeString(this.userName);
    }

    private AdUserInfo(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.accountName = reader.readString();
        this.userName = reader.readString();
    }

    static {
        CREATOR = new C12571();
    }
}
