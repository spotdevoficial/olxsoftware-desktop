package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.BuildConfig;
import com.facebook.appevents.AppEventsConstants;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class AppVersion implements DataModel {
    public static Creator<AppVersion> CREATOR;
    String mVersionNumber;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.AppVersion.1 */
    static class C12591 implements Creator<AppVersion> {
        C12591() {
        }

        public AppVersion createFromParcel(Parcel source) {
            return new AppVersion(null);
        }

        public AppVersion[] newArray(int size) {
            return new AppVersion[size];
        }
    }

    public String getVersionNumber() {
        return this.mVersionNumber;
    }

    public AppVersion(String versionNumber) {
        this.mVersionNumber = versionNumber;
    }

    public int compareTo(AppVersion appVersion) {
        String thatVersionNumber;
        String thisVersionNumber;
        if (appVersion == null || appVersion.getVersionNumber() == null) {
            thatVersionNumber = AppEventsConstants.EVENT_PARAM_VALUE_NO;
        } else {
            thatVersionNumber = appVersion.getVersionNumber();
        }
        if (getVersionNumber() == null) {
            thisVersionNumber = AppEventsConstants.EVENT_PARAM_VALUE_NO;
        } else {
            thisVersionNumber = getVersionNumber();
        }
        String[] thisParts = thisVersionNumber.split("\\.");
        String[] thatParts = thatVersionNumber.split("\\.");
        int length = Math.max(thisParts.length, thatParts.length);
        for (int i = 0; i < length; i++) {
            int thisPart;
            int thatPart;
            if (i < thisParts.length) {
                thisPart = getCleanInteger(thisParts[i]);
            } else {
                thisPart = 0;
            }
            if (i < thatParts.length) {
                thatPart = getCleanInteger(thatParts[i]);
            } else {
                thatPart = 0;
            }
            if (thisPart < thatPart) {
                return -1;
            }
            if (thisPart > thatPart) {
                return 1;
            }
        }
        return 0;
    }

    private int getCleanInteger(String kindOfANumber) {
        String cleanString = kindOfANumber.replaceAll("[^0-9]+", BuildConfig.VERSION_NAME);
        if (cleanString.length() == 0) {
            return 0;
        }
        return Integer.parseInt(cleanString);
    }

    public boolean isLesserThan(AppVersion appVersion) {
        return compareTo(appVersion) < 0;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.mVersionNumber);
    }

    private AppVersion(Parcel in) {
        this.mVersionNumber = new ParcelReader(in).readString();
    }

    static {
        CREATOR = new C12591();
    }
}
