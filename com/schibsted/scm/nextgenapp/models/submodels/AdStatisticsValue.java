package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class AdStatisticsValue implements DataModel {
    public static Creator<AdStatisticsValue> CREATOR;
    public Integer mails;
    public Integer views;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.AdStatisticsValue.1 */
    static class C12561 implements Creator<AdStatisticsValue> {
        C12561() {
        }

        public AdStatisticsValue createFromParcel(Parcel source) {
            return new AdStatisticsValue(null);
        }

        public AdStatisticsValue[] newArray(int size) {
            return new AdStatisticsValue[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeInt(this.views).writeInt(this.mails);
    }

    private AdStatisticsValue(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.views = reader.readInt();
        this.mails = reader.readInt();
    }

    static {
        CREATOR = new C12561();
    }
}
