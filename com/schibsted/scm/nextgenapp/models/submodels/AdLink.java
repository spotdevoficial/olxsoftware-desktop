package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class AdLink implements DataModel {
    public static Creator<AdLink> CREATOR;
    @JsonProperty("label")
    public String label;
    @JsonProperty("url")
    public String url;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.AdLink.1 */
    static class C12521 implements Creator<AdLink> {
        C12521() {
        }

        public AdLink createFromParcel(Parcel source) {
            return new AdLink(null);
        }

        public AdLink[] newArray(int size) {
            return new AdLink[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.label).writeString(this.url);
    }

    private AdLink(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.label = reader.readString();
        this.url = reader.readString();
    }

    static {
        CREATOR = new C12521();
    }
}
