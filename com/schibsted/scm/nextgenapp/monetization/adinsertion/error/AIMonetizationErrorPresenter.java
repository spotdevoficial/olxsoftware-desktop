package com.schibsted.scm.nextgenapp.monetization.adinsertion.error;

import com.schibsted.scm.nextgenapp.monetization.adinsertion.error.AIMonetizationErrorContract.FragmentContract;
import com.schibsted.scm.nextgenapp.monetization.adinsertion.error.AIMonetizationErrorContract.PresenterViewContract;

public class AIMonetizationErrorPresenter implements PresenterViewContract {
    private FragmentContract mFragment;

    public AIMonetizationErrorPresenter(FragmentContract fragment) {
        this.mFragment = fragment;
    }

    public void onRetryButtonClicked() {
        this.mFragment.retry();
    }
}
