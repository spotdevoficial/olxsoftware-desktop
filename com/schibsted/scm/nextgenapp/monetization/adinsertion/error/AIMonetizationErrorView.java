package com.schibsted.scm.nextgenapp.monetization.adinsertion.error;

import android.content.Context;
import android.support.v7.appcompat.C0086R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.schibsted.scm.nextgenapp.monetization.adinsertion.error.AIMonetizationErrorContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.monetization.adinsertion.error.AIMonetizationErrorContract.ViewContract;

public class AIMonetizationErrorView implements ViewContract {
    private PresenterViewContract mPresenter;
    private Button mRetryButton;
    private TextView mSubtitleTextView;
    private TextView mTitleTextView;
    private View mView;

    /* renamed from: com.schibsted.scm.nextgenapp.monetization.adinsertion.error.AIMonetizationErrorView.1 */
    class C13051 implements OnClickListener {
        C13051() {
        }

        public void onClick(View v) {
            AIMonetizationErrorView.this.mPresenter.onRetryButtonClicked();
        }
    }

    public AIMonetizationErrorView(Context context) {
        this.mView = LayoutInflater.from(context).inflate(2130903163, null);
        this.mTitleTextView = (TextView) this.mView.findViewById(C0086R.id.title);
        this.mSubtitleTextView = (TextView) this.mView.findViewById(2131558804);
        this.mRetryButton = (Button) this.mView.findViewById(2131558721);
        this.mTitleTextView.setText(context.getString(2131165329));
        this.mSubtitleTextView.setText(context.getString(2131165328));
        this.mRetryButton.setText(context.getString(2131165327));
        this.mRetryButton.setOnClickListener(new C13051());
    }

    public void setPresenter(PresenterViewContract presenter) {
        this.mPresenter = presenter;
    }

    public View getView() {
        return this.mView;
    }
}
