package com.schibsted.scm.nextgenapp.monetization.adinsertion.error;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.monetization.adinsertion.error.AIMonetizationErrorContract.ActivityContract;
import com.schibsted.scm.nextgenapp.monetization.adinsertion.error.AIMonetizationErrorContract.FragmentContract;
import com.schibsted.scm.nextgenapp.monetization.adinsertion.error.AIMonetizationErrorContract.ViewContract;

public class AIMonetizationErrorFragment extends Fragment implements FragmentContract {
    private ActivityContract mActivity;
    private ViewContract mView;

    public static AIMonetizationErrorFragment newInstance() {
        return new AIMonetizationErrorFragment();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mView = new AIMonetizationErrorView(getActivity());
        this.mView.setPresenter(new AIMonetizationErrorPresenter(this));
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.mView.getView();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mActivity = (ActivityContract) activity;
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("This activity doesn't support this fragment.");
        }
    }

    public void retry() {
        this.mActivity.retry();
    }
}
