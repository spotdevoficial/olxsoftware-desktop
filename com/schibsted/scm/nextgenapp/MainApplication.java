package com.schibsted.scm.nextgenapp;

import android.app.Application;
import android.content.Intent;
import android.content.IntentFilter;
import com.android.internal.util.Predicate;
import com.schibsted.scm.nextgenapp.abtest.ABTest;
import com.schibsted.scm.nextgenapp.abtest.ABTestConstants.ABTestAppseeVariants;
import com.schibsted.scm.nextgenapp.abtest.ABTestConstants.ABTestBumpOnMyAdsLabelVariants;
import com.schibsted.scm.nextgenapp.abtest.ABTestConstants.ABTestMapAdViewVariants;
import com.schibsted.scm.nextgenapp.abtest.ABTestConstants.ABTestNativeAdsVariants;
import com.schibsted.scm.nextgenapp.abtest.picker.ABTestVariantDefinition;
import com.schibsted.scm.nextgenapp.abtest.picker.ABTestWeightedVariantDefinition;
import com.schibsted.scm.nextgenapp.abtest.picker.ABTestWeightedVariantPicker;
import com.schibsted.scm.nextgenapp.backend.bus.messages.AccountStatusChangedMessage;
import com.schibsted.scm.nextgenapp.backend.managers.PreferencesManager;
import com.schibsted.scm.nextgenapp.backend.managers.StartupManager;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import com.schibsted.scm.nextgenapp.olxchat.OLXChat;
import com.schibsted.scm.nextgenapp.olxchat.OLXChatCustomerNotifier;
import com.schibsted.scm.nextgenapp.olxchat.OLXChatCustomerNotifier.ChatCustomer;
import com.schibsted.scm.nextgenapp.olxchat.otto.ChatBus;
import com.schibsted.scm.nextgenapp.olxchat.push.UrbanAirshipChatPushHandler;
import com.schibsted.scm.nextgenapp.olxchat.socket.ChatReceiverService;
import com.schibsted.scm.nextgenapp.olxchat.socket.ChatReceiverService.ChatServiceCreatedMessage;
import com.schibsted.scm.nextgenapp.receivers.ConnectionTypeReceiver;
import com.schibsted.scm.nextgenapp.receivers.OlxNotificationFactory;
import com.schibsted.scm.nextgenapp.receivers.action.TrackingAction;
import com.schibsted.scm.nextgenapp.tracking.fabric.AnswersAnalytics;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;
import com.schibsted.scm.nextgenapp.utils.ForegroundDetector;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import com.squareup.otto.Subscribe;
import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.UAirship;
import com.urbanairship.UAirship.OnReadyCallback;
import com.urbanairship.actions.ActionArguments;
import com.urbanairship.actions.Situation;
import com.urbanairship.push.PushManager;

public class MainApplication extends Application {
    private static final String TAG;
    private ConnectionTypeReceiver connectionTypeReceiver;
    private final OLXChatCustomerNotifier mChatCustomersToNotifyOnStartup;
    private ForegroundDetector mForegroundDetector;
    private OLXChat mOlxChat;
    private PreferencesManager mPreferencesManager;

    /* renamed from: com.schibsted.scm.nextgenapp.MainApplication.1 */
    class C10511 implements OnReadyCallback {

        /* renamed from: com.schibsted.scm.nextgenapp.MainApplication.1.1 */
        class C10501 implements Predicate<ActionArguments> {
            C10501() {
            }

            public boolean apply(ActionArguments arguments) {
                Situation situation = arguments.getSituation();
                return situation.equals(Situation.PUSH_OPENED) || situation.equals(Situation.BACKGROUND_NOTIFICATION_ACTION_BUTTON) || situation.equals(Situation.FOREGROUND_NOTIFICATION_ACTION_BUTTON) || situation.equals(Situation.WEB_VIEW_INVOCATION);
            }
        }

        C10511() {
        }

        public void onAirshipReady(UAirship airship) {
            airship.getActionRegistry().registerAction(new TrackingAction(), "tracking_action").setPredicate(new C10501());
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.MainApplication.2 */
    class C10522 implements ABTestVariantDefinition {
        C10522() {
        }

        public String getId() {
            return ABTestBumpOnMyAdsLabelVariants.A.name();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.MainApplication.3 */
    class C10533 implements ABTestVariantDefinition {
        C10533() {
        }

        public String getId() {
            return ABTestBumpOnMyAdsLabelVariants.B.name();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.MainApplication.4 */
    class C10544 implements ABTestVariantDefinition {
        C10544() {
        }

        public String getId() {
            return ABTestBumpOnMyAdsLabelVariants.C.name();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.MainApplication.5 */
    class C10555 extends ABTestWeightedVariantDefinition {
        C10555() {
        }

        public float getWeight() {
            return 2.5f;
        }

        public String getId() {
            return ABTestNativeAdsVariants.A1.name();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.MainApplication.6 */
    class C10566 extends ABTestWeightedVariantDefinition {
        C10566() {
        }

        public float getWeight() {
            return 2.5f;
        }

        public String getId() {
            return ABTestNativeAdsVariants.A2.name();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.MainApplication.7 */
    class C10577 extends ABTestWeightedVariantDefinition {
        C10577() {
        }

        public float getWeight() {
            return 95.0f;
        }

        public String getId() {
            return ABTestNativeAdsVariants.B.name();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.MainApplication.8 */
    class C10588 extends ABTestWeightedVariantDefinition {
        C10588() {
        }

        public float getWeight() {
            return MediaUploadState.IMAGE_PROGRESS_UPLOADED;
        }

        public String getId() {
            return ABTestMapAdViewVariants.A1.name();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.MainApplication.9 */
    class C10599 extends ABTestWeightedVariantDefinition {
        C10599() {
        }

        public float getWeight() {
            return MediaUploadState.IMAGE_PROGRESS_UPLOADED;
        }

        public String getId() {
            return ABTestMapAdViewVariants.A2.name();
        }
    }

    public MainApplication() {
        this.mChatCustomersToNotifyOnStartup = new OLXChatCustomerNotifier();
    }

    static {
        TAG = MainApplication.class.getSimpleName();
    }

    public void onCreate() {
        super.onCreate();
        CrashAnalytics.init(this);
        AnswersAnalytics.init(this);
        Logger.debug(TAG, "--------------------------------------------------------");
        Logger.debug(TAG, "-                                                      -");
        Logger.debug(TAG, "-                                                      -");
        Logger.debug(TAG, "-                                                      -");
        Logger.debug(TAG, "-                                                      -");
        Logger.debug(TAG, "-   D E S A P E G A !                                  -");
        Logger.debug(TAG, "-                  D E S A P E G A !                   -");
        Logger.debug(TAG, "-                                 O L X !              -");
        Logger.debug(TAG, "-                                                      -");
        Logger.debug(TAG, "-                                                      -");
        Logger.debug(TAG, "-                                                      -");
        Logger.debug(TAG, "--------------------------------------------------------");
        this.mForegroundDetector = new ForegroundDetector(this);
        this.mPreferencesManager = new PreferencesManager(this);
        if (ConfigContainer.getConfig().isTesting()) {
            while (!ConfigContainer.getConfig().isTestInitializationFinished()) {
                Thread.yield();
            }
        }
        if (StartupManager.getInstance().getStatus() != 2) {
            StartupManager.getInstance().initiate(this);
        } else {
            Logger.debug("Modules", "Did not initiate managers");
        }
        this.connectionTypeReceiver = new ConnectionTypeReceiver();
        registerReceiver(this.connectionTypeReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        C1049M.getMessageBus().register(this);
        takeOffUrbanAirship();
        if (C1049M.getAccountManager().isSignedIn()) {
            initOlxChatInstance();
        }
        startChatReceiverService();
        registerAbTests();
    }

    private void startChatReceiverService() {
        ChatBus.getInstance().register(this);
        startService(new Intent(this, ChatReceiverService.class));
    }

    public void messageMeWhenChatIsReady(ChatCustomer customer) {
        if (this.mOlxChat != null) {
            customer.onChatIsReady();
        } else {
            this.mChatCustomersToNotifyOnStartup.add(customer);
        }
    }

    @Subscribe
    public void onChatServiceCreated(ChatServiceCreatedMessage chatServiceCreatedMessage) {
        ChatBus.getInstance().unregister(this);
        if (C1049M.getAccountManager().isSignedIn()) {
            this.mOlxChat.start();
        }
    }

    private void takeOffUrbanAirship() {
        AirshipConfigOptions options = AirshipConfigOptions.loadDefaultOptions(this);
        options.inProduction = true;
        UAirship.takeOff(this, options, new C10511());
        PushManager pushManager = UAirship.shared().getPushManager();
        pushManager.setUserNotificationsEnabled(true);
        pushManager.setNotificationFactory(new OlxNotificationFactory(this, C1049M.getAccountManager()));
    }

    private void registerAbTests() {
        ABTest.with(this).register("PRI-176", new C10522(), new C10533(), new C10544()).now();
        ABTest.with(this).register("BUY-386", new C10555(), new C10566(), new C10577()).withPicker(new ABTestWeightedVariantPicker()).now();
        ABTest.with(this).register("BUY-282", new C10588(), new C10599(), new ABTestWeightedVariantDefinition() {
            public float getWeight() {
                return 8.0f;
            }

            public String getId() {
                return ABTestMapAdViewVariants.B.name();
            }
        }).withPicker(new ABTestWeightedVariantPicker()).now();
        ABTest.with(this).register("BUY-376", new ABTestWeightedVariantDefinition() {
            public float getWeight() {
                return 99.0f;
            }

            public String getId() {
                return ABTestAppseeVariants.A.name();
            }
        }, new ABTestWeightedVariantDefinition() {
            public float getWeight() {
                return MediaUploadState.IMAGE_PROGRESS_UPLOADED;
            }

            public String getId() {
                return ABTestAppseeVariants.B.name();
            }
        }).withPicker(new ABTestWeightedVariantPicker()).now();
    }

    public void onTerminate() {
        if (this.connectionTypeReceiver != null) {
            unregisterReceiver(this.connectionTypeReceiver);
        }
        if (C1049M.getEventRouter() != null) {
            C1049M.getEventRouter().onTerminate();
        }
    }

    private void initOlxChatInstance() {
        if (this.mOlxChat == null) {
            this.mOlxChat = new OLXChat(C1049M.getAccountManager().getAuthorization(), Integer.parseInt(C1049M.getAccountManager().getAccountId()), this.mPreferencesManager, new UrbanAirshipChatPushHandler(UAirship.shared(), this.mPreferencesManager), this.mForegroundDetector, ChatBus.getInstance(), JsonMapper.getInstance());
            C1049M.getMessageBus().register(this.mOlxChat);
            this.mForegroundDetector.addListener(this.mOlxChat);
        }
        this.mChatCustomersToNotifyOnStartup.notifyChatCostumers();
    }

    public OLXChat getOlxChatInstance() {
        return this.mOlxChat;
    }

    public boolean isAppVisible() {
        return this.mForegroundDetector.isForeground();
    }

    @Subscribe
    public void onAccountStatusChangedEvent(AccountStatusChangedMessage message) {
        if (!message.isLoggedIn()) {
            this.mPreferencesManager.setShownChatBefore(false);
            if (this.mOlxChat != null) {
                this.mOlxChat.disconnectSocket();
                C1049M.getMessageBus().unregister(this.mOlxChat);
                this.mForegroundDetector.removeListener(this.mOlxChat);
                this.mOlxChat = null;
            }
        } else if (message.isUpdated()) {
            initOlxChatInstance();
            this.mOlxChat.start();
        }
    }
}
