package com.schibsted.scm.nextgenapp.database.parsers;

import com.facebook.BuildConfig;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.util.TokenBuffer;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.database.dao.ETagMapDao;
import com.schibsted.scm.nextgenapp.database.dao.RegionTreeDao;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import java.io.IOException;
import java.util.concurrent.Callable;

public class RegionsApiResponseParser extends AbstractApiResponseParser {
    private ETagMapDao eTagsDao;
    private RegionTreeDao regionTreeDao;

    /* renamed from: com.schibsted.scm.nextgenapp.database.parsers.RegionsApiResponseParser.1 */
    class C11951 implements Callable<Void> {
        final /* synthetic */ byte[] val$data;

        C11951(byte[] bArr) {
            this.val$data = bArr;
        }

        public Void call() throws Exception {
            RegionsApiResponseParser.this.startParsing(this.val$data);
            return null;
        }
    }

    public String getETagName() {
        return "regions_data";
    }

    public synchronized void parseData(byte[] data, String eTag) {
        this.regionTreeDao = C1049M.getDaoManager().getRegionTree();
        this.eTagsDao = C1049M.getDaoManager().getETags();
        if (!this.eTagsDao.hasETag(getETagName(), eTag)) {
            try {
                this.eTagsDao.clearETag(getETagName());
                this.regionTreeDao.clearRegionData();
                this.regionTreeDao.calInTransation(new C11951(data));
                this.eTagsDao.setETag(getETagName(), eTag);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void startParsing(byte[] data) throws Exception {
        JsonParser parser = JsonMapper.getInstance().getFactory().createParser(data);
        try {
            if (parser.nextToken() != JsonToken.START_OBJECT) {
                throw new IOException("Expected " + JsonToken.START_OBJECT + " but got " + parser.getCurrentToken());
            }
            String all_label = BuildConfig.VERSION_NAME;
            int children = 0;
            while (parser.nextToken() != JsonToken.END_OBJECT) {
                String fieldName = parser.getCurrentName();
                if ("locations".equals(fieldName)) {
                    parseRegions(parser, BuildConfig.VERSION_NAME);
                }
                if ("startup_level".equals(fieldName)) {
                    parser.nextToken();
                    parser.getValueAsString();
                }
                if ("all_label".equals(fieldName)) {
                    parser.nextToken();
                    all_label = parser.getValueAsString(all_label);
                }
                if ("children".equals(fieldName)) {
                    parser.nextToken();
                    children = parser.getValueAsInt(children);
                }
            }
            this.regionTreeDao.insertRegionNode(BuildConfig.VERSION_NAME, "null", BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME, all_label, all_label, Integer.valueOf(children), Integer.valueOf(0));
        } finally {
            parser.close();
        }
    }

    private void parseRegions(JsonParser parser, String parentId) throws Exception {
        if (parser.nextToken() != JsonToken.START_ARRAY) {
            Logger.debug("RegionsApiResponseParser", "Error: root should be array: quiting parser.");
            return;
        }
        int order = 0;
        while (parser.nextToken() != JsonToken.END_ARRAY) {
            parseRegion(parser, parentId, Integer.valueOf(order));
            order++;
        }
    }

    private void parseRegion(JsonParser parser, String pId, Integer order) throws Exception {
        String parentId = pId;
        String key = BuildConfig.VERSION_NAME;
        String code = BuildConfig.VERSION_NAME;
        String label = BuildConfig.VERSION_NAME;
        String allLabel = BuildConfig.VERSION_NAME;
        String currentId = BuildConfig.VERSION_NAME;
        int children = 0;
        TokenBuffer buffer = null;
        if (parser.getCurrentToken() != JsonToken.START_OBJECT) {
            throw new IOException("Expected " + JsonToken.START_OBJECT + " but got " + parser.getCurrentToken());
        }
        while (parser.nextToken() != JsonToken.END_OBJECT) {
            String fieldName = parser.getCurrentName();
            if ("locations".equals(fieldName)) {
                parser.nextToken();
                buffer = new TokenBuffer(null, false);
                buffer.copyCurrentStructure(parser);
            }
            if ("filter_value".equals(fieldName)) {
                parser.nextToken();
                if (parser.getCurrentToken() != JsonToken.START_OBJECT) {
                    throw new IOException("Expected " + JsonToken.START_OBJECT + " but got " + parser.getCurrentToken() + " with name " + parser.getCurrentName());
                }
                currentId = parseIdentifier(parser);
            }
            if ("label".equals(fieldName)) {
                parser.nextToken();
                label = parser.getValueAsString(label);
            }
            if ("key".equals(fieldName)) {
                parser.nextToken();
                key = parser.getValueAsString(key);
            }
            if ("code".equals(fieldName)) {
                parser.nextToken();
                code = parser.getValueAsString(code);
            }
            if ("all_label".equals(fieldName)) {
                parser.nextToken();
                allLabel = parser.getValueAsString(allLabel);
            }
            if ("children".equals(fieldName)) {
                parser.nextToken();
                children = parser.getValueAsInt(children);
            }
        }
        if (buffer != null) {
            parseRegions(buffer.asParser(), currentId);
        }
        this.regionTreeDao.insertRegionNode(currentId, parentId, key, code, label, allLabel, Integer.valueOf(children), order);
    }
}
