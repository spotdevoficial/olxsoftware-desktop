package com.schibsted.scm.nextgenapp.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SearchHistoryDatabase {
    private SQLiteDatabase mDb;
    private Helper mHelper;

    private static class Helper extends SQLiteOpenHelper {
        private static Helper mInstance;

        public static Helper getInstance(Context context) {
            if (mInstance == null) {
                mInstance = new Helper(context);
            }
            return mInstance;
        }

        private Helper(Context context) {
            super(context, "DB_SEARCH_HISTORY", null, 1);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE TABLE_SEARCH_HISTORY (_id INTEGER PRIMARY KEY AUTOINCREMENT, query TEXT UNIQUE ON CONFLICT REPLACE);");
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS TABLE_SEARCH_HISTORY");
            onCreate(db);
        }
    }

    public SearchHistoryDatabase(Context context) {
        this.mHelper = Helper.getInstance(context);
        this.mDb = this.mHelper.getWritableDatabase();
    }

    public Cursor getRecentSearches() {
        return this.mDb.query("TABLE_SEARCH_HISTORY", new String[]{"_id", "query"}, null, null, null, null, "_id DESC", "3");
    }
}
