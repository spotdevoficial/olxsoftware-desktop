package com.schibsted.scm.nextgenapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Pair;
import com.facebook.internal.NativeProtocol;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.containers.SearchParametersContainer;
import com.schibsted.scm.nextgenapp.backend.managers.list.DatedRemoteListManager;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SavedSearchesList {
    private static String[] PROJECTION;
    private static String SORTING;
    private Pair<Integer, DatedRemoteListManager> lastRemoteListManager;
    private String mAccountId;
    private SQLiteDatabase mDb;
    private Helper mHelper;
    private List<SearchParametersContainer> mList;

    private class Helper extends SQLiteOpenHelper {
        public Helper(Context context, String name, CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE TABLE_SAVED_SEARCHES (_id INTEGER PRIMARY KEY AUTOINCREMENT, account_id TEXT, params TEXT, last_visit INTEGER,created INTEGER);");
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS TABLE_SAVED_SEARCHES");
            onCreate(db);
        }
    }

    static {
        PROJECTION = new String[]{"_id", NativeProtocol.WEB_DIALOG_PARAMS, "last_visit", "created"};
        SORTING = "last_visit COLLATE LOCALIZED ASC";
    }

    private String getDatabaseName() {
        return "DB_SAVED_SEARCHES";
    }

    public SavedSearchesList(String accountId, Context context) {
        if (accountId == null) {
            throw new IllegalArgumentException("Account ID must be non-null");
        }
        this.mHelper = new Helper(context, getDatabaseName(), null, 2);
        this.mAccountId = accountId;
    }

    private SQLiteDatabase getDb() {
        if (this.mDb == null) {
            this.mDb = this.mHelper.getWritableDatabase();
        }
        return this.mDb;
    }

    public Cursor getCursor() {
        return getDb().query("TABLE_SAVED_SEARCHES", PROJECTION, "account_id=\"" + this.mAccountId + "\"", null, null, null, SORTING);
    }

    public synchronized List<SearchParametersContainer> getSearches() {
        List<SearchParametersContainer> list;
        if (C1049M.getDaoManager().getRegionTree().isLoaded() && C1049M.getDaoManager().getCategoryTree("category_data").isLoaded()) {
            if (this.mList == null) {
                this.mList = new ArrayList();
                Cursor cursor = getCursor();
                int count = cursor.getCount();
                for (int i = 0; i < count; i++) {
                    cursor.moveToPosition(i);
                    SearchParametersContainer searchParametersContainer = SearchParametersContainer.fromStorageJson(cursor.getString(cursor.getColumnIndex(NativeProtocol.WEB_DIALOG_PARAMS)));
                    searchParametersContainer.setLastViewedTime(Long.valueOf(cursor.getLong(cursor.getColumnIndex("last_visit"))));
                    searchParametersContainer.setCreationTime(Long.valueOf(cursor.getLong(cursor.getColumnIndex("created"))));
                    searchParametersContainer.setDbId((long) cursor.getInt(cursor.getColumnIndex("_id")));
                    if (searchParametersContainer != null) {
                        this.mList.add(searchParametersContainer);
                    } else {
                        Logger.error("SavedSearchesList", "Error parsing SearchParametersContainer");
                    }
                }
                cursor.close();
            }
            list = this.mList;
        } else {
            list = null;
        }
        return list;
    }

    public void close() {
        this.mDb.close();
        this.mDb = null;
    }

    public boolean add(SearchParametersContainer searchParametersContainer) {
        boolean inserted = false;
        List<SearchParametersContainer> list = getSearches();
        if (list != null) {
            ContentValues values = new ContentValues();
            long now = new Date().getTime() / 1000;
            String paramsJson = searchParametersContainer.toStorageJson();
            values.put("account_id", this.mAccountId);
            values.put(NativeProtocol.WEB_DIALOG_PARAMS, paramsJson);
            values.put("last_visit", Long.valueOf(now));
            values.put("created", Long.valueOf(now));
            long rowId = getDb().insert("TABLE_SAVED_SEARCHES", null, values);
            inserted = rowId != -1;
            close();
            if (inserted) {
                SearchParametersContainer savedSearch = SearchParametersContainer.fromStorageJson(paramsJson);
                savedSearch.setLastViewedTime(Long.valueOf(now));
                savedSearch.setDbId(rowId);
                list.add(savedSearch);
            }
        }
        return inserted;
    }

    public boolean contains(SearchParametersContainer searchParametersContainer) {
        if (getSearches() == null || searchParametersContainer == null) {
            return false;
        }
        return getSearches().contains(searchParametersContainer);
    }

    public SearchParametersContainer get(int location) {
        return (SearchParametersContainer) getSearches().get(location);
    }

    public boolean update(SearchParametersContainer searchParametersContainer) {
        boolean updated = false;
        int index = this.mList.indexOf(searchParametersContainer);
        if (index >= 0) {
            SearchParametersContainer searchToBeUpdated = (SearchParametersContainer) this.mList.get(index);
            String query = "account_id='" + this.mAccountId + "' and " + "_id" + "='" + String.valueOf(searchToBeUpdated.getDbId()) + "'";
            long now = new Date().getTime() / 1000;
            ContentValues values = new ContentValues();
            values.put("last_visit", Long.valueOf(now));
            updated = getDb().update("TABLE_SAVED_SEARCHES", values, query, null) == 1;
            if (updated) {
                searchToBeUpdated.setLastViewedTime(Long.valueOf(now));
                searchToBeUpdated.getCounters().resetTimeStamp();
            }
            close();
        }
        return updated;
    }

    public boolean remove(SearchParametersContainer searchParametersContainer) {
        boolean removed = false;
        int index = this.mList.indexOf(searchParametersContainer);
        if (index >= 0) {
            removed = getDb().delete("TABLE_SAVED_SEARCHES", new StringBuilder().append("account_id='").append(this.mAccountId).append("' and ").append("_id").append("='").append(String.valueOf(((SearchParametersContainer) this.mList.get(index)).getDbId())).append("'").toString(), null) == 1;
            close();
            if (removed) {
                this.mList.remove(searchParametersContainer);
            }
        }
        return removed;
    }

    public int size() {
        if (getSearches() == null) {
            return 0;
        }
        return getSearches().size();
    }

    public DatedRemoteListManager loadListManagerFor(int location) {
        if (this.lastRemoteListManager == null || !((Integer) this.lastRemoteListManager.first).equals(Integer.valueOf(location))) {
            return newListManagerFor(location);
        }
        return (DatedRemoteListManager) this.lastRemoteListManager.second;
    }

    public DatedRemoteListManager newListManagerFor(int location) {
        SearchParametersContainer searchParameters = get(location);
        DatedRemoteListManager adListManager = new DatedRemoteListManager(C1049M.getTrafficManager());
        adListManager.setSearchParameters(searchParameters);
        this.lastRemoteListManager = new Pair(Integer.valueOf(location), adListManager);
        return adListManager;
    }
}
