package com.schibsted.scm.nextgenapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.logger.Log.Level;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.schibsted.scm.nextgenapp.database.dao.CategoryTreeDao;
import com.schibsted.scm.nextgenapp.database.dao.ETagMapDao;
import com.schibsted.scm.nextgenapp.database.dao.RegionTreeDao;
import com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode;
import com.schibsted.scm.nextgenapp.database.vo.DbETags;
import com.schibsted.scm.nextgenapp.database.vo.DbRegionNode;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import java.sql.SQLException;

public class DaoManager extends OrmLiteSqliteOpenHelper {
    public static final String TAG;
    private Dao<DbCategoryNode, String> categoryDao;
    private Dao<DbETags, String> etagsDao;
    private Dao<DbRegionNode, String> regionDao;

    static {
        TAG = DaoManager.class.getName();
    }

    public DaoManager(Context context) {
        super(context, "metadata.db", null, 2);
        this.etagsDao = null;
        this.regionDao = null;
        this.categoryDao = null;
        System.setProperty("com.j256.ormlite.logger.level", Level.ERROR.name());
    }

    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, DbETags.class);
            TableUtils.createTable(connectionSource, DbRegionNode.class);
            TableUtils.createTable(connectionSource, DbCategoryNode.class);
        } catch (SQLException e) {
            Logger.error(TAG, "Can't create database", e);
            throw new RuntimeException(e);
        }
    }

    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, DbETags.class, true);
            TableUtils.dropTable(connectionSource, DbRegionNode.class, true);
            TableUtils.dropTable(connectionSource, DbCategoryNode.class, true);
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Logger.error(TAG, "Can't drop databases", e);
            throw new RuntimeException(e);
        }
    }

    public RegionTreeDao getRegionTree() {
        if (this.regionDao == null) {
            try {
                this.regionDao = getDao(DbRegionNode.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return new RegionTreeDao(this.regionDao);
    }

    public CategoryTreeDao getCategoryTree(String type) {
        if (this.categoryDao == null) {
            try {
                this.categoryDao = getDao(DbCategoryNode.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return new CategoryTreeDao(this.categoryDao, type);
    }

    public ETagMapDao getETags() {
        if (this.etagsDao == null) {
            try {
                this.etagsDao = getDao(DbETags.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return new ETagMapDao(this.etagsDao);
    }
}
