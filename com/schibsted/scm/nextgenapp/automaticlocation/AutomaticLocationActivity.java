package com.schibsted.scm.nextgenapp.automaticlocation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.ActivityContract;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import com.schibsted.scm.nextgenapp.tracking.EventType;

public class AutomaticLocationActivity extends AppCompatActivity implements ActivityContract {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(2130903081);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(2131558583, new AutomaticLocationFragment()).commit();
        }
    }

    public void cancel() {
        setResult(0);
        finish();
    }

    public void deliverLocation(RegionPathApiModel regionPathApiModel) {
        Intent intent = new Intent();
        intent.putExtra(Identifier.PARAMETER_REGION, regionPathApiModel);
        setResult(-1, intent);
        finish();
    }

    public void onBackPressed() {
        C1049M.getMessageBus().post(new EventMessage(EventType.GEOLOCATION_CANCEL));
        super.onBackPressed();
    }
}
