package com.schibsted.scm.nextgenapp.automaticlocation;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import com.schibsted.scm.nextgenapp.C1049M;
import java.io.IOException;
import java.util.List;

public class LatLongToZipCodeService extends IntentService {

    public static class OnZipCodeMessage {
        private String mZipCode;

        public OnZipCodeMessage(String zipCode) {
            this.mZipCode = zipCode;
        }

        public String getZipCode() {
            return this.mZipCode;
        }
    }

    public LatLongToZipCodeService() {
        super("LatLongToZipCodeService");
    }

    protected void onHandleIntent(Intent intent) {
        String zipCode = getZipCode(intent.getDoubleExtra("INTENT_EXTRA_LATITUDE", 0.0d), intent.getDoubleExtra("INTENT_EXTRA_LONGITUDE", 0.0d));
        if (zipCode != null) {
            postZipCode(zipCode);
        }
    }

    private String getZipCode(double latitude, double longitude) {
        String zipCode = null;
        try {
            List<Address> addresses = new Geocoder(this).getFromLocation(latitude, longitude, 1);
            if (!addresses.isEmpty()) {
                zipCode = ((Address) addresses.get(0)).getPostalCode();
            }
        } catch (IOException e) {
        }
        return zipCode;
    }

    private void postZipCode(String zipCode) {
        C1049M.getMessageBus().post(new OnZipCodeMessage(zipCode));
    }
}
