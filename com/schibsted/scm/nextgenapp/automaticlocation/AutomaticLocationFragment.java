package com.schibsted.scm.nextgenapp.automaticlocation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.ActivityContract;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.FragmentContract;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.ModelContract;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.PresenterFragmentContract;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.ViewContract;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.utils.RunnableScheduler;

public class AutomaticLocationFragment extends Fragment implements FragmentContract {
    private ActivityContract mActivity;
    private ModelContract mModel;
    private PresenterFragmentContract mPresenter;
    private ViewContract mView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mModel = new AutomaticLocationModel(new RunnableScheduler(), C1049M.getMessageBus(), C1049M.getGeolocationManager());
        this.mView = new AutomaticLocationView(getActivity());
        AutomaticLocationPresenter automaticLocationPresenter = new AutomaticLocationPresenter(this.mModel, this.mView, this, C1049M.getMessageBus());
        this.mPresenter = automaticLocationPresenter;
        this.mModel.setPresenter(automaticLocationPresenter);
        this.mView.setPresenter(automaticLocationPresenter);
    }

    public void onResume() {
        super.onResume();
        this.mPresenter.start();
    }

    public void onPause() {
        super.onPause();
        this.mPresenter.stop();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mActivity = (ActivityContract) activity;
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("This activity doesn't support this fragment.");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.mView.getView();
    }

    public void cancel() {
        this.mActivity.cancel();
    }

    public void deliverLocation(RegionPathApiModel regionPathApiModel) {
        this.mActivity.deliverLocation(regionPathApiModel);
    }

    public void startLatLongToZipCodeService(double latitude, double longitude) {
        Intent intent = new Intent(getActivity(), LatLongToZipCodeService.class);
        intent.putExtra("INTENT_EXTRA_LATITUDE", latitude);
        intent.putExtra("INTENT_EXTRA_LONGITUDE", longitude);
        getActivity().startService(intent);
    }
}
