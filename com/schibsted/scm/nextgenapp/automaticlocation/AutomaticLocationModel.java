package com.schibsted.scm.nextgenapp.automaticlocation;

import com.android.volley.VolleyError;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.ModelContract;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.PresenterModelContract;
import com.schibsted.scm.nextgenapp.automaticlocation.LatLongToZipCodeService.OnZipCodeMessage;
import com.schibsted.scm.nextgenapp.backend.RegionFetcherHelper;
import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.UserLocationReceivedMessage;
import com.schibsted.scm.nextgenapp.backend.managers.GeolocationManager;
import com.schibsted.scm.nextgenapp.backend.network.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.utils.RunnableScheduler;
import com.squareup.otto.Subscribe;

public class AutomaticLocationModel implements ModelContract {
    private GeolocationManager mGeolocationManager;
    private boolean mIsFetching;
    private MessageBus mMessageBus;
    private PresenterModelContract mPresenter;
    private RunnableScheduler mRunnableScheduler;
    private Runnable mTimeoutRunnable;

    /* renamed from: com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationModel.1 */
    class C11521 implements Runnable {
        C11521() {
        }

        public void run() {
            AutomaticLocationModel.this.mMessageBus.post(new EventMessage(EventType.GEOLOCATION_UNSUCCESSFUL));
            AutomaticLocationModel.this.mPresenter.onErrorFetchingLocation();
            AutomaticLocationModel.this.stopFetchUserLocation();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationModel.2 */
    class C11532 extends OnNetworkResponseListener<RegionPathApiModel> {
        C11532() {
        }

        public void onErrorResponse(VolleyError error) {
            AutomaticLocationModel.this.stopFetchUserLocation();
            AutomaticLocationModel.this.mPresenter.onErrorFetchingLocation();
        }

        public void onResponse(RegionPathApiModel region) {
            if (AutomaticLocationModel.this.mIsFetching) {
                AutomaticLocationModel.this.stopFetchUserLocation();
                if (region == null || !region.capAtLevel(Identifier.PARAMETER_REGION)) {
                    AutomaticLocationModel.this.mPresenter.onErrorFetchingLocation();
                    return;
                }
                AutomaticLocationModel.this.mMessageBus.post(new EventMessage(EventType.GEOLOCATION_SUCCESSFUL));
                AutomaticLocationModel.this.mPresenter.onLocationFetched(region);
            }
        }
    }

    public AutomaticLocationModel(RunnableScheduler runnableScheduler, MessageBus messageBus, GeolocationManager geolocationManager) {
        this.mIsFetching = false;
        this.mRunnableScheduler = runnableScheduler;
        this.mMessageBus = messageBus;
        this.mGeolocationManager = geolocationManager;
        this.mTimeoutRunnable = new C11521();
    }

    public void fetchUserCurrentLocation() {
        if (!this.mIsFetching) {
            this.mIsFetching = true;
            this.mMessageBus.register(this);
            this.mGeolocationManager.start();
            startTimeout();
        }
    }

    public void stopFetchUserLocation() {
        if (this.mIsFetching) {
            this.mIsFetching = false;
            this.mMessageBus.unregister(this);
            this.mGeolocationManager.stop();
            stopTimeout();
        }
    }

    public void setPresenter(PresenterModelContract presenter) {
        this.mPresenter = presenter;
    }

    @Subscribe
    public void onLocationReceived(UserLocationReceivedMessage userLocationReceivedMessage) {
        this.mPresenter.startLatLongToZipCodeService(userLocationReceivedMessage.getLatitude(), userLocationReceivedMessage.getLongitude());
    }

    @Subscribe
    public void onZipCode(OnZipCodeMessage onZipCodeMessage) {
        RegionFetcherHelper.getFromZipCode(onZipCodeMessage.getZipCode(), new C11532());
    }

    private void startTimeout() {
        this.mRunnableScheduler.start(this.mTimeoutRunnable, 15000);
    }

    private void stopTimeout() {
        this.mRunnableScheduler.cancel(this.mTimeoutRunnable);
    }
}
