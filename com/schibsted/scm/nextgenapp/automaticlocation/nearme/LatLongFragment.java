package com.schibsted.scm.nextgenapp.automaticlocation.nearme;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.ViewContract;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationView;
import com.schibsted.scm.nextgenapp.automaticlocation.nearme.LatLongContract.ActivityContract;
import com.schibsted.scm.nextgenapp.automaticlocation.nearme.LatLongContract.FragmentContract;
import com.schibsted.scm.nextgenapp.automaticlocation.nearme.LatLongContract.ModelContract;
import com.schibsted.scm.nextgenapp.automaticlocation.nearme.LatLongContract.PresenterFragmentContract;
import com.schibsted.scm.nextgenapp.utils.RunnableScheduler;

public class LatLongFragment extends Fragment implements FragmentContract {
    private ActivityContract mActivity;
    private ModelContract mModel;
    private PresenterFragmentContract mPresenter;
    private ViewContract mView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mModel = new LatLongModel(new RunnableScheduler(), C1049M.getMessageBus(), C1049M.getGeolocationManager());
        this.mView = new AutomaticLocationView(getActivity());
        LatLongPresenter latLongPresenter = new LatLongPresenter(this.mModel, this.mView, this, C1049M.getMessageBus());
        this.mPresenter = latLongPresenter;
        this.mModel.setPresenter(latLongPresenter);
        this.mView.setPresenter(latLongPresenter);
    }

    public void onResume() {
        super.onResume();
        this.mPresenter.start();
    }

    public void onPause() {
        super.onPause();
        this.mPresenter.stop();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mActivity = (ActivityContract) activity;
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("This activity doesn't support this fragment.");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.mView.getView();
    }

    public void cancel() {
        this.mActivity.cancel();
    }

    public void deliverLatLong(double latitude, double longitude) {
        this.mActivity.deliverLatLong(latitude, longitude);
    }
}
