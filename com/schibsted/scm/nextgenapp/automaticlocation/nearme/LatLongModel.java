package com.schibsted.scm.nextgenapp.automaticlocation.nearme;

import com.schibsted.scm.nextgenapp.automaticlocation.nearme.LatLongContract.ModelContract;
import com.schibsted.scm.nextgenapp.automaticlocation.nearme.LatLongContract.PresenterModelContract;
import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.UserLocationReceivedMessage;
import com.schibsted.scm.nextgenapp.backend.managers.GeolocationManager;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.utils.RunnableScheduler;
import com.squareup.otto.Subscribe;

public class LatLongModel implements ModelContract {
    private GeolocationManager mGeolocationManager;
    private boolean mIsFetching;
    private MessageBus mMessageBus;
    private PresenterModelContract mPresenter;
    private RunnableScheduler mRunnableScheduler;
    private Runnable mTimeoutRunnable;

    /* renamed from: com.schibsted.scm.nextgenapp.automaticlocation.nearme.LatLongModel.1 */
    class C11571 implements Runnable {
        C11571() {
        }

        public void run() {
            LatLongModel.this.mMessageBus.post(new EventMessage(EventType.NEAR_ME_UNSUCCESSFUL));
            LatLongModel.this.mPresenter.onErrorFetchingLatLong();
            LatLongModel.this.stopFetchLatLong();
        }
    }

    public LatLongModel(RunnableScheduler runnableScheduler, MessageBus messageBus, GeolocationManager geolocationManager) {
        this.mIsFetching = false;
        this.mRunnableScheduler = runnableScheduler;
        this.mMessageBus = messageBus;
        this.mGeolocationManager = geolocationManager;
        this.mTimeoutRunnable = new C11571();
    }

    public void fetchLatLong() {
        if (!this.mIsFetching) {
            this.mIsFetching = true;
            this.mMessageBus.register(this);
            this.mGeolocationManager.start();
            startTimeout();
        }
    }

    public void stopFetchLatLong() {
        if (this.mIsFetching) {
            this.mIsFetching = false;
            this.mMessageBus.unregister(this);
            this.mGeolocationManager.stop();
            stopTimeout();
        }
    }

    public void setPresenter(PresenterModelContract presenter) {
        this.mPresenter = presenter;
    }

    @Subscribe
    public void onLocationReceived(UserLocationReceivedMessage userLocationReceivedMessage) {
        if (this.mIsFetching) {
            double latitude = userLocationReceivedMessage.getLatitude();
            double longitude = userLocationReceivedMessage.getLongitude();
            this.mMessageBus.post(new EventMessage(EventType.NEAR_ME_SUCCESSFUL));
            this.mPresenter.onLatLongFetched(latitude, longitude);
        }
    }

    private void startTimeout() {
        this.mRunnableScheduler.start(this.mTimeoutRunnable, 15000);
    }

    private void stopTimeout() {
        this.mRunnableScheduler.cancel(this.mTimeoutRunnable);
    }
}
