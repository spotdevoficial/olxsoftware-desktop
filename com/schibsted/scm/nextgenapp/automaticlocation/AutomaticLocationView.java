package com.schibsted.scm.nextgenapp.automaticlocation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.ViewContract;

public class AutomaticLocationView implements ViewContract {
    private Button mManualSelectionButton;
    private PresenterViewContract mPresenter;
    private GeolocationRippleAnimation mRippleAnimation;
    private final View mView;

    /* renamed from: com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationView.1 */
    class C11541 implements OnClickListener {
        C11541() {
        }

        public void onClick(View v) {
            AutomaticLocationView.this.mPresenter.onManualSelectionButtonClick();
        }
    }

    public AutomaticLocationView(Context context) {
        this.mView = LayoutInflater.from(context).inflate(2130903158, null);
        this.mRippleAnimation = new GeolocationRippleAnimation(this.mView, context);
        loadViews();
        setListeners();
        this.mRippleAnimation.startAnimations();
    }

    private void loadViews() {
        this.mManualSelectionButton = (Button) this.mView.findViewById(2131558774);
    }

    private void setListeners() {
        this.mManualSelectionButton.setOnClickListener(new C11541());
    }

    public void setPresenter(PresenterViewContract presenter) {
        this.mPresenter = presenter;
    }

    public View getView() {
        return this.mView;
    }

    public void startAnimation() {
    }

    public void stopAnimation() {
    }

    public void showErrorMessage() {
        this.mRippleAnimation.stop();
        ErrorMessageAnimation.showErrorMessage(this.mView, getContext());
    }

    private Context getContext() {
        return this.mView.getContext();
    }
}
