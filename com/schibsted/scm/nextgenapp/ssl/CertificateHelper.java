package com.schibsted.scm.nextgenapp.ssl;

public class CertificateHelper {
    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.security.KeyStore loadKeyStoreFromRaw(android.content.res.Resources r4, int r5, char[] r6) {
        /*
        r3 = "BKS";
        r2 = java.security.KeyStore.getInstance(r3);	 Catch:{ KeyStoreException -> 0x0016 }
        r1 = r4.openRawResource(r5);	 Catch:{ KeyStoreException -> 0x0016 }
        r2.load(r1, r6);	 Catch:{ Exception -> 0x001c }
        r1.close();	 Catch:{ IOException -> 0x0011 }
    L_0x0010:
        return r2;
    L_0x0011:
        r0 = move-exception;
        r0.printStackTrace();	 Catch:{ KeyStoreException -> 0x0016 }
        goto L_0x0010;
    L_0x0016:
        r0 = move-exception;
        r0.printStackTrace();
        r2 = 0;
        goto L_0x0010;
    L_0x001c:
        r0 = move-exception;
        r0.printStackTrace();	 Catch:{ all -> 0x0029 }
        r1.close();	 Catch:{ IOException -> 0x0024 }
        goto L_0x0010;
    L_0x0024:
        r0 = move-exception;
        r0.printStackTrace();	 Catch:{ KeyStoreException -> 0x0016 }
        goto L_0x0010;
    L_0x0029:
        r3 = move-exception;
        r1.close();	 Catch:{ IOException -> 0x002e }
    L_0x002d:
        throw r3;	 Catch:{ KeyStoreException -> 0x0016 }
    L_0x002e:
        r0 = move-exception;
        r0.printStackTrace();	 Catch:{ KeyStoreException -> 0x0016 }
        goto L_0x002d;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.schibsted.scm.nextgenapp.ssl.CertificateHelper.loadKeyStoreFromRaw(android.content.res.Resources, int, char[]):java.security.KeyStore");
    }
}
