package com.schibsted.scm.nextgenapp.ssl.networktrust;

import java.security.cert.CertificateException;

public class CertificateMemorizationException extends CertificateException {
    public CertificateMemorizationException(Throwable t) {
        super(t);
    }
}
