package com.schibsted.scm.nextgenapp.ssl.networktrust;

import android.content.Context;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public class MemorizingTrustManager implements X509TrustManager {
    private KeyStore keyStore;
    private Options options;
    private X509TrustManager storeTrustManager;
    private KeyStore transientKeyStore;
    private X509TrustManager transientTrustManager;

    public static class Options {
        File store;
        String storePassword;
        String storeType;
        boolean trustOnFirstUse;
        File workingDir;

        public Options(Context ctxt, String storeRelPath, String storePassword) {
            this.workingDir = null;
            this.store = null;
            this.storeType = KeyStore.getDefaultType();
            this.trustOnFirstUse = false;
            this.workingDir = new File(ctxt.getFilesDir(), storeRelPath);
            this.workingDir.mkdirs();
            this.store = new File(this.workingDir, "memorized.bks");
            this.storePassword = storePassword;
        }
    }

    public MemorizingTrustManager(Options options) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, FileNotFoundException, IOException {
        this.keyStore = null;
        this.options = null;
        this.storeTrustManager = null;
        this.transientKeyStore = null;
        this.transientTrustManager = null;
        this.options = options;
        clear(false);
    }

    public synchronized void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        try {
            this.storeTrustManager.checkClientTrusted(chain, authType);
        } catch (CertificateException e) {
            try {
                this.transientTrustManager.checkClientTrusted(chain, authType);
            } catch (Exception e3) {
                throw new CertificateMemorizationException(e3);
            } catch (CertificateException e2) {
                if (!this.options.trustOnFirstUse || this.options.store.exists()) {
                    throw new CertificateNotMemorizedException(chain);
                }
                storeCert(chain);
            }
        }
    }

    public synchronized void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        try {
            this.storeTrustManager.checkServerTrusted(chain, authType);
        } catch (CertificateException e) {
            try {
                this.transientTrustManager.checkServerTrusted(chain, authType);
            } catch (Exception e3) {
                throw new CertificateMemorizationException(e3);
            } catch (CertificateException e2) {
                if (!this.options.trustOnFirstUse || this.options.store.exists()) {
                    throw new CertificateNotMemorizedException(chain);
                }
                storeCert(chain);
            }
        }
    }

    public X509Certificate[] getAcceptedIssuers() {
        return new X509Certificate[0];
    }

    public synchronized void storeCert(X509Certificate[] chain) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
        for (X509Certificate cert : chain) {
            this.keyStore.setCertificateEntry(cert.getSubjectDN().getName(), cert);
        }
        initTrustManager();
        FileOutputStream fos = new FileOutputStream(this.options.store);
        this.keyStore.store(fos, this.options.storePassword.toCharArray());
        fos.close();
    }

    public synchronized void clear(boolean clearPersistent) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
        if (clearPersistent) {
            this.options.store.delete();
        }
        initTransientStore();
        initPersistentStore();
        initTrustManager();
    }

    private void initTransientStore() throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
        this.transientKeyStore = KeyStore.getInstance(this.options.storeType);
        this.transientKeyStore.load(null, null);
    }

    private void initPersistentStore() throws KeyStoreException, NoSuchAlgorithmException, CertificateException, FileNotFoundException, IOException {
        this.keyStore = KeyStore.getInstance(this.options.storeType);
        if (this.options.store.exists()) {
            this.keyStore.load(new FileInputStream(this.options.store), this.options.storePassword.toCharArray());
        } else {
            this.keyStore.load(null, this.options.storePassword.toCharArray());
        }
    }

    private void initTrustManager() throws KeyStoreException, NoSuchAlgorithmException {
        TrustManagerFactory tmf = TrustManagerFactory.getInstance("X509");
        tmf.init(this.keyStore);
        for (TrustManager t : tmf.getTrustManagers()) {
            if (t instanceof X509TrustManager) {
                this.storeTrustManager = (X509TrustManager) t;
                break;
            }
        }
        tmf = TrustManagerFactory.getInstance("X509");
        tmf.init(this.transientKeyStore);
        for (TrustManager t2 : tmf.getTrustManagers()) {
            if (t2 instanceof X509TrustManager) {
                this.transientTrustManager = (X509TrustManager) t2;
                return;
            }
        }
    }
}
