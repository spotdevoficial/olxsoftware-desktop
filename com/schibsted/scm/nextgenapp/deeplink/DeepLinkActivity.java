package com.schibsted.scm.nextgenapp.deeplink;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.appsee.Appsee;
import com.facebook.share.internal.ShareConstants;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.abtest.ABTest;
import com.schibsted.scm.nextgenapp.abtest.ABTestConstants.ABTestAppseeVariants;
import com.schibsted.scm.nextgenapp.abtest.ABTestVariant;
import com.schibsted.scm.nextgenapp.activities.RemoteListActivity;
import com.schibsted.scm.nextgenapp.activities.SingleFragmentActivity;
import com.schibsted.scm.nextgenapp.authentication.login.LoginActivity;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.tracking.EventType;

public class DeepLinkActivity extends SingleFragmentActivity {
    private DeepLink mDeepLink;

    /* renamed from: com.schibsted.scm.nextgenapp.deeplink.DeepLinkActivity.1 */
    class C11961 extends ABTestVariant {
        C11961() {
        }

        public void perform() {
        }

        public String getId() {
            return ABTestAppseeVariants.A.name();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.deeplink.DeepLinkActivity.2 */
    class C11972 extends ABTestVariant {
        C11972() {
        }

        public void perform() {
            Appsee.start("f95a848d424f404c8442d9cb87b69995");
        }

        public String getId() {
            return ABTestAppseeVariants.B.name();
        }
    }

    public static class LoadingFragment extends Fragment {
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(2130903189, container, false);
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Uri data = getIntent().getData();
        if (data == null) {
            finish();
            return;
        }
        startAppSee();
        resetMainListSearchParameters();
        reportDeepLinkReferral(data);
        this.mDeepLink = new DeepLinkFactory().from(data);
        if (!this.mDeepLink.requiresLogin() || C1049M.getAccountManager().isSignedIn()) {
            handleDeepLink();
        } else {
            startLoginActivity(this);
        }
    }

    private void startAppSee() {
        ABTest.with(this).perform("BUY-376", new C11961(), new C11972());
    }

    private void handleDeepLink() {
        this.mDeepLink.handle(this);
        if (!this.mDeepLink.requiresResult()) {
            finish();
        }
    }

    private void startLoginActivity(Activity sourceActivity) {
        sourceActivity.startActivityForResult(LoginActivity.newIntent(sourceActivity), 42);
    }

    private void reportDeepLinkReferral(Uri data) {
        C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.DEEPLINK_SOURCE).setDeepLinkReferralPage(data.getQueryParameter("page")).setDeepLinkSource(data.getQueryParameter(ShareConstants.FEED_SOURCE_PARAM)).build());
    }

    private void resetMainListSearchParameters() {
        C1049M.getMainAdListManager().getSearchParameters().loadDefaultValues();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != 42) {
            this.mDeepLink.handleResult(this, requestCode, resultCode, data);
            finish();
        } else if (resultCode == -1) {
            handleDeepLink();
        } else {
            onUserCanceledLogin();
        }
    }

    private void onUserCanceledLogin() {
        RemoteListActivity.startSkippingAutoLocation(this);
        finish();
    }

    protected Fragment createFragment() {
        return new LoadingFragment();
    }
}
