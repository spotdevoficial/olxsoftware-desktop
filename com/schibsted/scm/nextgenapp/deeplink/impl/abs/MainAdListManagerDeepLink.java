package com.schibsted.scm.nextgenapp.deeplink.impl.abs;

import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.containers.SearchParametersContainer;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteAdListManager;
import com.schibsted.scm.nextgenapp.deeplink.DeepLink;
import java.util.List;

public abstract class MainAdListManagerDeepLink implements DeepLink {
    private RemoteAdListManager mRemoteAdListManager;

    public MainAdListManagerDeepLink() {
        this.mRemoteAdListManager = C1049M.getMainAdListManager();
    }

    protected void setAdIdsForMainList(List<String> ids) {
        SearchParametersContainer params = this.mRemoteAdListManager.getSearchParameters();
        params.setTextSearch(BuildConfig.VERSION_NAME);
        params.setAdListingId(ids);
        clearMainAdListManager();
    }

    protected void clearMainAdListManager() {
        this.mRemoteAdListManager.clear();
    }

    protected void clearAdListingIdSearchParameter() {
        this.mRemoteAdListManager.getSearchParameters().setAdListingId(null);
    }

    protected SearchParametersContainer getSearchParameters() {
        return this.mRemoteAdListManager.getSearchParameters();
    }

    protected void setSearchParameters(SearchParametersContainer parameters) {
        this.mRemoteAdListManager.setSearchParameters(parameters);
    }
}
