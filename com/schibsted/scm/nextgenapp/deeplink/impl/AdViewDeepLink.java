package com.schibsted.scm.nextgenapp.deeplink.impl;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import com.facebook.BuildConfig;
import com.facebook.share.internal.ShareConstants;
import com.schibsted.scm.nextgenapp.activities.RemoteDetailSearchResultActivity;
import com.schibsted.scm.nextgenapp.activities.RemoteListActivity;
import com.schibsted.scm.nextgenapp.deeplink.impl.abs.MainAdListManagerDeepLink;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public class AdViewDeepLink extends MainAdListManagerDeepLink {
    private Uri mUri;

    public AdViewDeepLink(Uri uri) {
        this.mUri = uri;
    }

    public void handle(Activity sourceActivity) {
        List<String> ids = parseAdIds(this.mUri);
        if (!ids.isEmpty()) {
            setAdIdsForMainList(ids);
            clearMainAdListManager();
            if (ids.size() == 1) {
                RemoteDetailSearchResultActivity.startForResult(sourceActivity, 1, 0, false);
                return;
            }
        }
        clearMainAdListManager();
        RemoteListActivity.startForResultSkippingAutoLocation(sourceActivity, 1);
    }

    private List<String> parseAdIds(Uri uri) {
        List<String> ids = new ArrayList();
        for (NameValuePair arg : getNameValuePairsFromUri(uri)) {
            if (ShareConstants.WEB_DIALOG_PARAM_ID.equals(arg.getName())) {
                ids.add(arg.getValue());
            }
        }
        return ids;
    }

    private static List<NameValuePair> getNameValuePairsFromUri(Uri uri) {
        List<NameValuePair> args = new ArrayList();
        try {
            args = URLEncodedUtils.parse(new URI(uri.toString().replaceAll("\\[\\d+\\]", BuildConfig.VERSION_NAME)), "UTF-8");
        } catch (Exception e) {
            CrashAnalytics.logException(e);
        }
        return args;
    }

    public void handleResult(Activity sourceActivity, int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            clearAdListingIdSearchParameter();
            clearMainAdListManager();
            if (resultCode == 0) {
                RemoteListActivity.startSkippingAutoLocation(sourceActivity);
            }
        }
    }

    public boolean requiresResult() {
        return true;
    }

    public boolean requiresLogin() {
        return false;
    }
}
