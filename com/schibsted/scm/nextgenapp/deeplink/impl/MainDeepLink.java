package com.schibsted.scm.nextgenapp.deeplink.impl;

import android.app.Activity;
import android.content.Intent;
import com.schibsted.scm.nextgenapp.activities.RemoteListActivity;
import com.schibsted.scm.nextgenapp.deeplink.impl.abs.MainAdListManagerDeepLink;

public class MainDeepLink extends MainAdListManagerDeepLink {
    public void handle(Activity sourceActivity) {
        clearMainAdListManager();
        RemoteListActivity.startSkippingAutoLocation(sourceActivity);
    }

    public void handleResult(Activity sourceActivity, int requestCode, int resultCode, Intent data) {
        throw new UnsupportedOperationException();
    }

    public boolean requiresResult() {
        return false;
    }

    public boolean requiresLogin() {
        return false;
    }
}
