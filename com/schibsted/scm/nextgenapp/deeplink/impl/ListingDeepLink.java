package com.schibsted.scm.nextgenapp.deeplink.impl;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.activities.RemoteListActivity;
import com.schibsted.scm.nextgenapp.backend.containers.SearchParametersContainer;
import com.schibsted.scm.nextgenapp.backend.managers.PreferencesManager;
import com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode;
import com.schibsted.scm.nextgenapp.deeplink.impl.abs.MainAdListManagerDeepLink;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;

public class ListingDeepLink extends MainAdListManagerDeepLink {
    private Uri mUri;

    public ListingDeepLink(Uri uri) {
        this.mUri = uri;
    }

    public void handle(Activity sourceActivity) {
        String searchKeywords = this.mUri.getQueryParameter("keyword");
        String categoryId = this.mUri.getQueryParameter("category");
        SearchParametersContainer listParams = getSearchParameters();
        listParams.setTextSearch(searchKeywords);
        removeUserSelectedDefaultRegion(sourceActivity);
        listParams.setRegion(decodePossibleRegionPathFromURL(this.mUri));
        DbCategoryNode category = C1049M.getDaoManager().getCategoryTree("category_data").getByCode(categoryId);
        if (category != null) {
            listParams.setCategory(category);
        }
        setSearchParameters(listParams);
        clearMainAdListManager();
        RemoteListActivity.startSkippingAutoLocation(sourceActivity);
    }

    private RegionPathApiModel decodePossibleRegionPathFromURL(Uri data) {
        if (C1049M.getDaoManager().getRegionTree().isLoaded()) {
            StringBuilder sb = new StringBuilder();
            String stateId = data.getQueryParameter(Identifier.PARAMETER_STATE);
            if (stateId != null) {
                sb.append(Identifier.PARAMETER_STATE);
                sb.append(Identifier.PARAMETER_ASIGNMENT);
                sb.append(stateId);
            }
            String regionId = data.getQueryParameter(Identifier.PARAMETER_REGION);
            if (regionId != null) {
                if (sb.length() > 0) {
                    sb.append(Identifier.PARAMETER_SEPARATOR);
                }
                sb.append(Identifier.PARAMETER_REGION);
                sb.append(Identifier.PARAMETER_ASIGNMENT);
                sb.append(regionId);
            }
            if (sb.length() > 0) {
                return C1049M.getDaoManager().getRegionTree().findRegion(new Identifier(sb.toString()));
            }
        }
        return null;
    }

    private void removeUserSelectedDefaultRegion(Context context) {
        PreferencesManager preferencesManager = new PreferencesManager(context);
        preferencesManager.removeDefaultRegion();
        preferencesManager.removeDefaultRegionLabel();
    }

    public void handleResult(Activity sourceActivity, int requestCode, int resultCode, Intent data) {
        throw new UnsupportedOperationException();
    }

    public boolean requiresResult() {
        return false;
    }

    public boolean requiresLogin() {
        return false;
    }
}
