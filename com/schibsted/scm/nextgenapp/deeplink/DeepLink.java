package com.schibsted.scm.nextgenapp.deeplink;

import android.app.Activity;
import android.content.Intent;

public interface DeepLink {
    void handle(Activity activity);

    void handleResult(Activity activity, int i, int i2, Intent intent);

    boolean requiresLogin();

    boolean requiresResult();
}
