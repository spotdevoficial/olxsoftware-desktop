package com.schibsted.scm.nextgenapp.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import com.appsee.Appsee;
import com.facebook.internal.NativeProtocol;
import com.schibsted.scm.nextgenapp.AdDetailIntentProvider;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.RemoteListManagerProvider;
import com.schibsted.scm.nextgenapp.abtest.ABTest;
import com.schibsted.scm.nextgenapp.abtest.ABTestConstants.ABTestAppseeVariants;
import com.schibsted.scm.nextgenapp.abtest.ABTestVariant;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationActivity;
import com.schibsted.scm.nextgenapp.backend.bus.messages.ConfigChangedMessage;
import com.schibsted.scm.nextgenapp.backend.managers.PreferencesManager;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteListManager;
import com.schibsted.scm.nextgenapp.tracking.SingleEventsTagger;
import com.schibsted.scm.nextgenapp.ui.CroutonStyle;
import com.schibsted.scm.nextgenapp.ui.fragments.AdBrowsingFragment;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.InfoDialogFragment;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.RemoveOlxCheckDialog;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.WebViewDialogFragment;
import com.schibsted.scm.nextgenapp.ui.listeners.DismissCallback;
import com.schibsted.scm.nextgenapp.utils.IntentsCreator;
import com.squareup.otto.Subscribe;
import de.keyboardsurfer.android.widget.crouton.Crouton;

public class RemoteListActivity extends DrawerActivity implements AdDetailIntentProvider, RemoteListManagerProvider {
    public static final String TAG;

    /* renamed from: com.schibsted.scm.nextgenapp.activities.RemoteListActivity.1 */
    class C10791 extends ABTestVariant {
        C10791() {
        }

        public void perform() {
        }

        public String getId() {
            return ABTestAppseeVariants.A.name();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.activities.RemoteListActivity.2 */
    class C10802 extends ABTestVariant {
        C10802() {
        }

        public void perform() {
            Appsee.start("f95a848d424f404c8442d9cb87b69995");
        }

        public String getId() {
            return ABTestAppseeVariants.B.name();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.activities.RemoteListActivity.3 */
    class C10813 implements DismissCallback {
        final /* synthetic */ PreferencesManager val$pm;
        final /* synthetic */ String val$url;

        C10813(PreferencesManager preferencesManager, String str) {
            this.val$pm = preferencesManager;
            this.val$url = str;
        }

        public void onUserDismissedDialog() {
            this.val$pm.saveLastWebInfoView(this.val$url);
            RemoteListActivity.this.executeSiteCustomTask();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.activities.RemoteListActivity.4 */
    class C10824 implements OnClickListener {
        C10824() {
        }

        public void onClick(View view) {
            Intent goToMarket = IntentsCreator.createPlayStoreIntent(RemoteListActivity.this);
            if (goToMarket != null) {
                RemoteListActivity.this.startActivity(goToMarket);
            } else {
                Crouton.showText(RemoteListActivity.this, 2131165579, CroutonStyle.ALERT);
            }
        }
    }

    static {
        TAG = RemoteListActivity.class.getSimpleName();
    }

    public static void startSkippingAutoLocation(Context context) {
        context.startActivity(getSkipAutoLocationIntent(context));
    }

    public static void startForResultSkippingAutoLocation(Activity activity, int requestCode) {
        activity.startActivityForResult(getSkipAutoLocationIntent(activity), requestCode);
    }

    private static Intent getSkipAutoLocationIntent(Context context) {
        Intent remoteListIntent = new Intent(context, RemoteListActivity.class);
        remoteListIntent.putExtra("INTENT_SKIP_AUTO_LOCATION", true);
        remoteListIntent.setFlags(NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REQUEST);
        return remoteListIntent;
    }

    public void onBackPressed() {
        if (isDrawerOpen()) {
            closeDrawer();
        } else if (closeOnBackPressed()) {
            finish();
        }
    }

    private boolean closeOnBackPressed() {
        Fragment fragment = getFragment();
        if (fragment == null || !AdBrowsingFragment.class.isInstance(fragment)) {
            return true;
        }
        return ((AdBrowsingFragment) fragment).closeActivityOnBackPressed();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (shouldShowLocationActivity()) {
            startLocationActivity();
        }
        SingleEventsTagger.tagSingleStartupEvents(getApplicationContext());
        startAppSee();
    }

    private void startAppSee() {
        ABTest.with(this).perform("BUY-376", new C10791(), new C10802());
    }

    private boolean shouldShowLocationActivity() {
        return C1049M.getMainAdListManager().getSearchParameters().getRegion() == null && !getIntent().hasExtra("INTENT_SKIP_AUTO_LOCATION");
    }

    private void startLocationActivity() {
        startActivityForResult(new Intent(this, AutomaticLocationActivity.class), vd.f504D);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == vd.f504D && resultCode == 0) {
            startRegionActivity();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void startRegionActivity() {
        startActivityForResult(SelectionListActivity.newRegionIntent(this, null), 2);
    }

    public void onResume() {
        super.onResume();
        if (getState() == null && getIntent().hasExtra("UNAUTHORIZED")) {
            Crouton.makeText((Activity) this, getString(2131165654), CroutonStyle.ALERT).show();
            getIntent().removeExtra("UNAUTHORIZED");
        }
    }

    protected Fragment createFragment() {
        Fragment fragment = AdBrowsingFragment.newInstance();
        fragment.setArguments(getState());
        return fragment;
    }

    public RemoteListManager getRemoteListManager(Bundle bundle) {
        return C1049M.getMainAdListManager();
    }

    public Intent newAdDetailIntent(Bundle extras) {
        Intent intent = new Intent(this, RemoteDetailSearchResultActivity.class);
        intent.putExtra("EXTRAS_ARGUMENTS", extras);
        return intent;
    }

    public static Intent newIntent(Context context) {
        return new Intent(context, RemoteListActivity.class);
    }

    public static Intent newIntentOnUnauthorized(Context context) {
        Intent intent = new Intent(context, RemoteListActivity.class);
        intent.putExtra("UNAUTHORIZED", true);
        intent.setFlags(335577088);
        return intent;
    }

    @Subscribe
    public void onConfigChangedMessageReceived(ConfigChangedMessage msg) {
        if (msg.getConfig() != null && !msg.isFailed()) {
            if (C1049M.getConfigManager().isVersionNumberOutdated(this)) {
                showGoToPlayStoreDialog();
            } else if (!displayInfoWebViewDialog(msg.getConfig().infoPageUrl)) {
                executeSiteCustomTask();
            }
        }
    }

    private boolean displayInfoWebViewDialog(String url) {
        PreferencesManager pm = new PreferencesManager(this);
        boolean alreadyVisible = true;
        if (TextUtils.isEmpty(url) || url.equals(pm.loadLastWebInfoView())) {
            return false;
        }
        Fragment infoDialog = (WebViewDialogFragment) getSupportFragmentManager().findFragmentByTag(WebViewDialogFragment.TAG);
        if (infoDialog == null) {
            infoDialog = WebViewDialogFragment.newInstance(url);
            alreadyVisible = false;
        }
        if (infoDialog != null) {
            infoDialog.setDismissCallback(new C10813(pm, url));
            if (!alreadyVisible) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(infoDialog, WebViewDialogFragment.TAG);
                ft.commitAllowingStateLoss();
            }
        }
        return true;
    }

    public void executeSiteCustomTask() {
        new RemoveOlxCheckDialog(this).show();
    }

    private void showGoToPlayStoreDialog() {
        InfoDialogFragment goToPlayStoreDialog = InfoDialogFragment.newInstance(getString(2131165454), C1049M.getConfigManager().getConfig().requiredMessage, 1);
        goToPlayStoreDialog.setPositiveText(2131165415);
        goToPlayStoreDialog.setOKClickListener(new C10824());
        goToPlayStoreDialog.setCancelable(false);
        goToPlayStoreDialog.show(getSupportFragmentManager(), goToPlayStoreDialog.getTag());
    }
}
