package com.schibsted.scm.nextgenapp.activities;

import android.content.Intent;
import android.os.Bundle;
import com.schibsted.scm.nextgenapp.ui.listeners.OnSignedInListener;

public abstract class RequireSignInDrawerActivity extends DrawerActivity implements OnSignedInListener {
    private RequireSignInActivityDelegate mDelegate;

    public RequireSignInDrawerActivity() {
        this.mDelegate = new RequireSignInActivityDelegate(this);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mDelegate.onCreate(savedInstanceState);
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this.mDelegate.onSaveInstanceState(outState);
    }

    public void onResume() {
        super.onResume();
        this.mDelegate.onResume();
    }

    public void onPause() {
        super.onPause();
        this.mDelegate.onPause();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.mDelegate.onActivityResult(requestCode, resultCode, data);
    }
}
