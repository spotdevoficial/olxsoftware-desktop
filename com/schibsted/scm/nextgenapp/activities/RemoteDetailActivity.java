package com.schibsted.scm.nextgenapp.activities;

import android.support.v4.app.Fragment;
import com.schibsted.scm.nextgenapp.RemoteListManagerProvider;
import com.schibsted.scm.nextgenapp.ui.fragments.AdDetailPagerFragment;

public abstract class RemoteDetailActivity extends SingleFragmentActivity implements RemoteListManagerProvider {
    protected Fragment createFragment() {
        return AdDetailPagerFragment.newInstance(getState());
    }
}
