package com.schibsted.scm.nextgenapp.activities;

import android.os.Bundle;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteListManager;

public class RemoteDetailSavedSearchActivity extends RemoteDetailActivity {
    public RemoteListManager getRemoteListManager(Bundle bundle) {
        return C1049M.getAccountManager().getSavedSearchesList().loadListManagerFor(bundle.getInt("LIST_INDEX"));
    }
}
