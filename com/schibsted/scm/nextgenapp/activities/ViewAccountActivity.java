package com.schibsted.scm.nextgenapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.schibsted.scm.nextgenapp.ui.fragments.ViewAccountFragment;
import com.schibsted.scm.nextgenapp.ui.listeners.OnSignedInListener;

public class ViewAccountActivity extends RequireSignInDrawerActivity implements OnSignedInListener {
    private static final String TAG;

    static {
        TAG = ViewAccountActivity.class.getSimpleName();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHorizontalPadding();
        setTitle(2131165539);
    }

    protected Fragment createFragment() {
        return ViewAccountFragment.newInstance();
    }

    public static Intent newIntent(Context context) {
        return new Intent(context, ViewAccountActivity.class);
    }

    public void onSignedIn() {
        ((OnSignedInListener) getFragment()).onSignedIn();
    }
}
