package com.schibsted.scm.nextgenapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.schibsted.scm.nextgenapp.ui.fragments.EditAccountFragment;
import com.schibsted.scm.nextgenapp.ui.listeners.OnSignedInListener;

public class EditAccountActivity extends RequireSignInActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHorizontalPadding();
        setTitle(2131165644);
    }

    protected Fragment createFragment() {
        Fragment fragment = EditAccountFragment.newInstance();
        fragment.setArguments(getState());
        return fragment;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getFragment().onActivityResult(requestCode, resultCode, data);
    }

    public void onSignedIn() {
        ((OnSignedInListener) getFragment()).onSignedIn();
    }

    public static Intent newIntent(Context context, String[] requirements) {
        Intent intent = new Intent(context, EditAccountActivity.class);
        if (requirements != null) {
            Bundle extras = new Bundle();
            extras.putStringArray("REQUIRED_FIELDS", requirements);
            intent.putExtra("EXTRAS_ARGUMENTS", extras);
        }
        return intent;
    }
}
