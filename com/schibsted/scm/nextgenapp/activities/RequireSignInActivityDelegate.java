package com.schibsted.scm.nextgenapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.authentication.login.LoginActivity;
import com.schibsted.scm.nextgenapp.backend.bus.messages.AccountUnauthorizedMessage;
import com.schibsted.scm.nextgenapp.ui.listeners.OnSignedInListener;
import com.squareup.otto.Subscribe;

public class RequireSignInActivityDelegate {
    private boolean hasCalledSignedIn;
    private Activity mActivity;

    public RequireSignInActivityDelegate(Activity activity) {
        this.hasCalledSignedIn = false;
        this.mActivity = activity;
    }

    public void onCreate(Bundle savedInstanceState) {
        this.hasCalledSignedIn = false;
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("signInCalled")) {
                this.hasCalledSignedIn = savedInstanceState.getBoolean("signInCalled");
            }
            if (!C1049M.getAccountManager().isSignedIn() && this.hasCalledSignedIn) {
                this.mActivity.startActivity(RemoteListActivity.newIntentOnUnauthorized(this.mActivity));
            }
        } else if (!C1049M.getAccountManager().isSignedIn()) {
            this.mActivity.startActivityForResult(LoginActivity.newIntent(this.mActivity), 200);
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("signInCalled", this.hasCalledSignedIn);
    }

    public void onResume() {
        if (C1049M.getAccountManager().isSignedIn() && !this.hasCalledSignedIn) {
            this.hasCalledSignedIn = true;
            ((OnSignedInListener) this.mActivity).onSignedIn();
        } else if (!C1049M.getAccountManager().isSignedIn() && this.hasCalledSignedIn) {
            this.mActivity.startActivity(RemoteListActivity.newIntentOnUnauthorized(this.mActivity));
        }
        C1049M.getMessageBus().register(this);
    }

    public void onPause() {
        C1049M.getMessageBus().unregister(this);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 200 && !C1049M.getAccountManager().isSignedIn()) {
            this.mActivity.finish();
        }
    }

    @Subscribe
    public void accountUnauthorized(AccountUnauthorizedMessage message) {
        this.mActivity.startActivity(RemoteListActivity.newIntentOnUnauthorized(this.mActivity));
        this.mActivity.finish();
    }
}
