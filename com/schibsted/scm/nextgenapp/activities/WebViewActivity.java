package com.schibsted.scm.nextgenapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.MenuItem;
import com.facebook.internal.NativeProtocol;
import com.schibsted.scm.nextgenapp.ui.fragments.WebViewFragment;

public class WebViewActivity extends SingleFragmentActivity {
    protected Fragment createFragment() {
        Fragment fragment = WebViewFragment.newInstance();
        fragment.setArguments(getState());
        return fragment;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().getBundleExtra("EXTRAS_ARGUMENTS") != null) {
            setTitle(getIntent().getBundleExtra("EXTRAS_ARGUMENTS").getString("ARG_TITLE"));
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static Intent newIntent(Context context, String url, String label) {
        Intent intent = new Intent(context, WebViewActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("ARG_URL", url);
        bundle.putString("ARG_TITLE", label);
        intent.putExtra("EXTRAS_ARGUMENTS", bundle);
        intent.addFlags(NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REQUEST);
        return intent;
    }
}
