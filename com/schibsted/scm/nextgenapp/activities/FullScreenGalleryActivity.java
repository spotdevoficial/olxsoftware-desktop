package com.schibsted.scm.nextgenapp.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.SimpleOnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout.LayoutParams;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.adapters.MediaPagerAdapter;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.ui.views.pageindicator.UnderlinePageIndicator;
import java.util.ArrayList;

public class FullScreenGalleryActivity extends Activity {
    private Ad mAd;
    private boolean mEnableExtraButtons;
    private ArrayList<String> mIds;
    private boolean mIsEditing;
    private ArrayList<String> mUrls;
    private int mViewPagerIndex;

    /* renamed from: com.schibsted.scm.nextgenapp.activities.FullScreenGalleryActivity.1 */
    class C10741 extends SimpleOnPageChangeListener {
        C10741() {
        }

        public void onPageSelected(int position) {
            if (FullScreenGalleryActivity.this.shouldTrackEvents()) {
                if (FullScreenGalleryActivity.this.mViewPagerIndex > position) {
                    FullScreenGalleryActivity.this.tagAdPhotoSwipeLeft();
                } else if (FullScreenGalleryActivity.this.mViewPagerIndex < position) {
                    FullScreenGalleryActivity.this.tagAdPhotoSwipeRight();
                }
            }
            FullScreenGalleryActivity.this.mViewPagerIndex = position;
            Intent intent = FullScreenGalleryActivity.this.getIntent();
            Bundle bundle = intent.getBundleExtra("EXTRAS_ARGUMENTS");
            bundle.putInt("INDEX", position);
            bundle.putString("URL", (String) FullScreenGalleryActivity.this.mIds.get(position));
            FullScreenGalleryActivity.this.setResult(-1, intent);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.activities.FullScreenGalleryActivity.2 */
    class C10752 implements OnClickListener {
        final /* synthetic */ ViewPager val$pager;

        C10752(ViewPager viewPager) {
            this.val$pager = viewPager;
        }

        public void onClick(View v) {
            FullScreenGalleryActivity.this.sendTagEventForClickOnRemove();
            Intent intent = FullScreenGalleryActivity.this.getIntent();
            Bundle bundle = intent.getBundleExtra("EXTRAS_ARGUMENTS");
            bundle.putBoolean("REMOVE_IMAGE", true);
            bundle.putString("URL", (String) FullScreenGalleryActivity.this.mIds.get(this.val$pager.getCurrentItem()));
            FullScreenGalleryActivity.this.setResult(-1, intent);
            FullScreenGalleryActivity.this.finish();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.activities.FullScreenGalleryActivity.3 */
    class C10763 implements OnClickListener {
        C10763() {
        }

        public void onClick(View v) {
            FullScreenGalleryActivity.this.finish();
        }
    }

    public static Intent newIntent(Context context, ArrayList<String> urls, int startIndex, boolean extraButtons, boolean isEditing, Ad ad) {
        return newIntent(context, urls, startIndex, extraButtons, urls, isEditing, ad);
    }

    public static Intent newIntent(Context context, ArrayList<String> urls, int startIndex, boolean extraButtons, ArrayList<String> ids, boolean isEditing, Ad ad) {
        Intent intent = new Intent(context, FullScreenGalleryActivity.class);
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("URLS", urls);
        bundle.putStringArrayList("IDS", ids);
        bundle.putInt("INDEX", startIndex);
        bundle.putBoolean("EXTRA_BUTTONS", extraButtons);
        bundle.putBoolean("IS_EDITING", isEditing);
        bundle.putParcelable("AD", ad);
        intent.putExtra("EXTRAS_ARGUMENTS", bundle);
        return intent;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(2130903168);
        Bundle state = null;
        if (savedInstanceState != null && savedInstanceState.containsKey("EXTRAS_ARGUMENTS")) {
            state = savedInstanceState.getBundle("EXTRAS_ARGUMENTS");
        } else if (getIntent().getExtras() != null) {
            state = getIntent().getExtras().getBundle("EXTRAS_ARGUMENTS");
        }
        this.mUrls = state.getStringArrayList("URLS");
        this.mIds = state.getStringArrayList("IDS");
        this.mViewPagerIndex = state.getInt("INDEX", 0);
        this.mEnableExtraButtons = state.getBoolean("EXTRA_BUTTONS", false);
        this.mIsEditing = state.getBoolean("IS_EDITING", false);
        this.mAd = (Ad) state.getParcelable("AD");
        initializeViewPager(state);
        Intent intent = getIntent();
        intent.putExtra("URL", (String) this.mUrls.get(this.mViewPagerIndex));
        setResult(-1, intent);
        initializeCloseAndRemoveButtons();
    }

    private void initializeViewPager(Bundle state) {
        PagerAdapter adapter = new MediaPagerAdapter(this, this.mUrls);
        ViewPager pager = (ViewPager) findViewById(2131558816);
        UnderlinePageIndicator indicator = (UnderlinePageIndicator) findViewById(2131558743);
        pager.setOffscreenPageLimit(1);
        pager.setAdapter(adapter);
        pager.setCurrentItem(this.mViewPagerIndex);
        if (this.mUrls.size() < 2) {
            indicator.setVisibility(8);
        }
        indicator.setViewPager(pager);
        indicator.setOnPageChangeListener(new C10741());
    }

    private boolean shouldTrackEvents() {
        return this.mAd != null;
    }

    private void initializeCloseAndRemoveButtons() {
        if (this.mEnableExtraButtons) {
            findViewById(2131558817).setVisibility(0);
            ((Button) findViewById(2131558818)).setOnClickListener(new C10752((ViewPager) findViewById(2131558816)));
            ((Button) findViewById(2131558819)).setOnClickListener(new C10763());
            return;
        }
        LayoutParams lay = new LayoutParams(-2, (int) getResources().getDimension(2131230963));
        lay.addRule(12);
        findViewById(2131558743).setLayoutParams(lay);
    }

    public boolean isEditing() {
        return this.mIsEditing;
    }

    private void sendTagEventForClickOnRemove() {
        EventMessage eventMessage;
        if (isEditing()) {
            eventMessage = new EventBuilder().setEventType(EventType.EDIT_AD_CLICK_DELETE_PHOTO).setAd(this.mAd).build();
        } else {
            eventMessage = new EventBuilder().setEventType(EventType.CLICK_REMOVE_IMAGE_FROM_AD).build();
        }
        C1049M.getMessageBus().post(eventMessage);
    }

    private void tagAdPhotoSwipeRight() {
        tagUpsightEvent(EventType.PAGE_AD_PHOTO_SWIPE_RIGHT);
    }

    private void tagAdPhotoSwipeLeft() {
        tagUpsightEvent(EventType.PAGE_AD_PHOTO_SWIPE_LEFT);
    }

    private void tagUpsightEvent(EventType name) {
        C1049M.getMessageBus().post(new EventBuilder().setEventType(name).setAd(this.mAd).build());
    }
}
