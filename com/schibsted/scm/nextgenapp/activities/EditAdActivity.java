package com.schibsted.scm.nextgenapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import com.facebook.internal.NativeProtocol;
import com.schibsted.scm.nextgenapp.AdDetailIntentProvider;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.RemoteListManagerProvider;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteListManager;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.models.submodels.PrivateAd;
import com.schibsted.scm.nextgenapp.ui.fragments.ManagementAdControllerFragment;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.InfoDialogFragment;

public class EditAdActivity extends RequireSignInActivity implements AdDetailIntentProvider, RemoteListManagerProvider {
    private static final String TAG;
    private boolean mIsRequirementsFulfilled;

    /* renamed from: com.schibsted.scm.nextgenapp.activities.EditAdActivity.1 */
    class C10721 implements OnClickListener {
        final /* synthetic */ InfoDialogFragment val$dialog;

        C10721(InfoDialogFragment infoDialogFragment) {
            this.val$dialog = infoDialogFragment;
        }

        public void onClick(View view) {
            this.val$dialog.dismiss();
            EditAdActivity.this.startActivityForResult(EditAccountActivity.newIntent(EditAdActivity.this, ConfigContainer.getConfig().getAdInsertionRequiredAccountFields()), 450);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.activities.EditAdActivity.2 */
    class C10732 implements OnClickListener {
        final /* synthetic */ InfoDialogFragment val$dialog;

        C10732(InfoDialogFragment infoDialogFragment) {
            this.val$dialog = infoDialogFragment;
        }

        public void onClick(View v) {
            this.val$dialog.dismiss();
            EditAdActivity.this.finish();
        }
    }

    static {
        TAG = EditAdActivity.class.getSimpleName();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHorizontalPadding();
        setTitle(2131165645);
    }

    protected Fragment createFragment() {
        return ManagementAdControllerFragment.newInstance();
    }

    public Intent newAdDetailIntent(Bundle extras) {
        Intent intent = new Intent(this, EditAdActivity.class);
        intent.putExtra("EXTRAS_ARGUMENTS", extras);
        return intent;
    }

    public static Intent newIntent(Context context, PrivateAd mAd) {
        Intent intent = new Intent(context, EditAdActivity.class);
        if (mAd != null) {
            Bundle bundle = new Bundle();
            bundle.putString("EDITING_AD_ID", mAd.ad.getCleanPrivateId());
            intent.putExtra("EXTRAS_ARGUMENTS", bundle);
            intent.putExtra("EDITING_AD_OBJECT", mAd);
        }
        intent.setFlags(NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REQUEST);
        return intent;
    }

    public RemoteListManager getRemoteListManager(Bundle bundle) {
        return C1049M.getAccountManager().getMyAdListManager();
    }

    public void onSignedIn() {
        fulfillRequirements(ConfigContainer.getConfig().getAdInsertionRequiredAccountFields());
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 450) {
            this.mIsRequirementsFulfilled = C1049M.getAccountManager().isRequirementsFulfilled(ConfigContainer.getConfig().getAdInsertionRequiredAccountFields());
            if (!this.mIsRequirementsFulfilled || resultCode != -1) {
                finish();
            }
        }
    }

    public void fulfillRequirements(String[] requirements) {
        this.mIsRequirementsFulfilled = C1049M.getAccountManager().isRequirementsFulfilled(requirements);
        if (!this.mIsRequirementsFulfilled) {
            InfoDialogFragment dialog = InfoDialogFragment.newInstance(getString(2131165423), getString(2131165422), 3);
            dialog.setPositiveText(2131165345);
            dialog.setNegativeText(2131165341);
            dialog.setOKClickListener(new C10721(dialog));
            dialog.setCancelClickListener(new C10732(dialog));
            dialog.setCancelable(false);
            dialog.show(getSupportFragmentManager(), dialog.getTag());
        }
    }
}
