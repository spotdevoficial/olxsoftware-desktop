package com.schibsted.scm.nextgenapp.activities;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.appcompat.C0086R;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.internal.NativeProtocol;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.MainApplication;
import com.schibsted.scm.nextgenapp.authentication.login.LoginActivity;
import com.schibsted.scm.nextgenapp.backend.bus.messages.AccountStatusChangedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.ConfigChangedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.managers.AccountManager;
import com.schibsted.scm.nextgenapp.backend.managers.UserProfileImagesManager.UserProfileImagesManagerFetchingListener;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.developertools.DeveloperToolsActivity;
import com.schibsted.scm.nextgenapp.models.ConfigApiModel;
import com.schibsted.scm.nextgenapp.models.submodels.Account;
import com.schibsted.scm.nextgenapp.models.submodels.StaticPage;
import com.schibsted.scm.nextgenapp.olxchat.OLXChat;
import com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListActivity;
import com.schibsted.scm.nextgenapp.olxchat.model.Message;
import com.schibsted.scm.nextgenapp.olxchat.otto.ChatBus;
import com.schibsted.scm.nextgenapp.olxchat.otto.ChatStatusMessage;
import com.schibsted.scm.nextgenapp.olxchat.otto.ChatStatusMessage.Status;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.messages.drawer.AdInsertionMenuClickedMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.drawer.ChatListMenuClickedMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.drawer.MainAdListMenuClickedMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.drawer.MyAccountMenuClickedMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.drawer.UserDetailMenuClickedMessage;
import com.schibsted.scm.nextgenapp.ui.CroutonStyle;
import com.schibsted.scm.nextgenapp.ui.DialogCreator;
import com.schibsted.scm.nextgenapp.ui.DialogCreator.DialogButton;
import com.schibsted.scm.nextgenapp.ui.drawable.FadeinDrawable;
import com.schibsted.scm.nextgenapp.ui.views.MenuItemTextSizeSpan;
import com.schibsted.scm.nextgenapp.ui.views.MenuItemTypefaceSpan;
import com.schibsted.scm.nextgenapp.ui.views.RoundedImageView;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;
import com.schibsted.scm.nextgenapp.utils.IntentsCreator;
import com.schibsted.scm.nextgenapp.utils.NextGenActionBarDrawerToggle;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import com.squareup.otto.Subscribe;
import com.urbanairship.C1608R;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import java.util.Map;

public abstract class DrawerActivity extends SingleFragmentActivity {
    private static final String TAG;
    private Object mBusEventListener;
    private Object mChatMessageSubscriber;
    private int mDeveloperToolsButtonClickCount;
    private long mDeveloperToolsButtonClickStartMillis;
    private ViewGroup mDrawerHeader;
    private DrawerLayout mDrawerLayout;
    private NextGenActionBarDrawerToggle mDrawerToggleManager;
    private TextView mEmailTextView;
    private StaticPage mFaqStaticPage;
    private NavigationView mNavigationView;
    private StaticPage mSecurityTipsStaticPage;
    private StaticPage mTermsOfUseStaticPage;
    private RoundedImageView mUserPictureImageView;
    private TextView mUsernameTextView;

    /* renamed from: com.schibsted.scm.nextgenapp.activities.DrawerActivity.1 */
    class C10631 {
        C10631() {
        }

        @Subscribe
        public void onAccountStatusChanged(AccountStatusChangedMessage msg) {
            DrawerActivity.this.onAccountStatusChanged(msg);
        }

        @Subscribe
        public void onConfigChangedMessageReceived(ConfigChangedMessage msg) {
            DrawerActivity.this.onConfigChanged(msg);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.activities.DrawerActivity.2 */
    class C10642 {
        C10642() {
        }

        @Subscribe
        public void onChatMessage(Message message) {
            DrawerActivity.this.updateChatMessagesIndicators();
        }

        @Subscribe
        public void onChatStatusMessage(ChatStatusMessage message) {
            if (message.getStatus() == Status.DELETED) {
                DrawerActivity.this.updateChatMessagesIndicators();
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.activities.DrawerActivity.3 */
    class C10653 implements OnNavigationItemSelectedListener {
        C10653() {
        }

        public boolean onNavigationItemSelected(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case 2131558920:
                    DrawerActivity.this.onAdsListMenuClick();
                    break;
                case 2131558921:
                    DrawerActivity.this.onAdInsertionMenuClick();
                    break;
                case 2131558922:
                    DrawerActivity.this.onChatMenuClick();
                    break;
                case 2131558923:
                    DrawerActivity.this.onMyAccountMenuClick();
                    break;
                case 2131558924:
                    DrawerActivity.this.onCustomerServiceMenuClick();
                    break;
                case 2131558926:
                    DrawerActivity.this.onFaqMenuClick();
                    break;
                case 2131558927:
                    DrawerActivity.this.onSecurityTipsMenuClick();
                    break;
                case 2131558928:
                    DrawerActivity.this.onTermsOfUseMenuClick();
                    break;
                case 2131558929:
                    DrawerActivity.this.onGooglePlayRateMenuClick();
                    break;
                case 2131558930:
                    DrawerActivity.this.onFacebookLikeMenuClick();
                    break;
            }
            DrawerActivity.this.mDrawerLayout.closeDrawers();
            return true;
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.activities.DrawerActivity.4 */
    class C10664 extends NextGenActionBarDrawerToggle {
        C10664(Activity x0, DrawerLayout x1, Toolbar x2, int x3, int x4) {
            super(x0, x1, x2, x3, x4);
        }

        public void onDrawerSlide(View drawerView, float slideOffset) {
            super.onDrawerSlide(drawerView, slideOffset);
        }

        public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
            if (DrawerActivity.this.shouldStartDeveloperToolsActivity()) {
                C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.OPEN_DEVELOPER_TOOLS).build());
                DrawerActivity.this.startDeveloperToolsActivity();
            }
            DrawerActivity.this.updateChatMessagesIndicators();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.activities.DrawerActivity.5 */
    class C10675 implements Runnable {
        C10675() {
        }

        public void run() {
            DrawerActivity.this.showAdListing();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.activities.DrawerActivity.6 */
    class C10686 implements Runnable {
        C10686() {
        }

        public void run() {
            String originPageTitle;
            if (DrawerActivity.this instanceof RemoteListActivity) {
                originPageTitle = "Anuncios";
            } else {
                originPageTitle = DrawerActivity.this.getTitle().toString();
            }
            C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_OPEN_AD_INSERTION).setOriginPageTitle(originPageTitle).build());
            DrawerActivity.this.showAdInsertion();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.activities.DrawerActivity.7 */
    class C10697 implements Runnable {
        C10697() {
        }

        public void run() {
            C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_OPEN_CHAT).build());
            DrawerActivity.this.showChatList();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.activities.DrawerActivity.8 */
    class C10708 implements Runnable {
        C10708() {
        }

        public void run() {
            C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_OPEN_ACCOUNT).build());
            DrawerActivity.this.showAccount();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.activities.DrawerActivity.9 */
    class C10719 implements Runnable {
        C10719() {
        }

        public void run() {
            C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_OPEN_CUSTOMER_SERVICE).build());
            DrawerActivity.this.contactCustomerService();
        }
    }

    static {
        TAG = DrawerActivity.class.getSimpleName();
    }

    public DrawerActivity() {
        this.mDeveloperToolsButtonClickCount = 0;
        this.mDeveloperToolsButtonClickStartMillis = 0;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mBusEventListener = new C10631();
        this.mChatMessageSubscriber = new C10642();
        configureDrawer();
    }

    public void onResume() {
        super.onResume();
        C1049M.getMessageBus().register(this.mBusEventListener);
        C1049M.getMessageBus().register(this);
        ChatBus.getInstance().register(this.mChatMessageSubscriber);
        updateDrawerHeader();
        updateChatMessagesIndicators();
    }

    public void onPause() {
        super.onPause();
        C1049M.getMessageBus().unregister(this.mBusEventListener);
        C1049M.getMessageBus().unregister(this);
        ChatBus.getInstance().unregister(this.mChatMessageSubscriber);
    }

    private void configureDrawer() {
        this.mDrawerLayout = (DrawerLayout) findViewById(2131558578);
        this.mDrawerHeader = (ViewGroup) findViewById(2131558727);
        this.mUserPictureImageView = (RoundedImageView) findViewById(2131558728);
        this.mUsernameTextView = (TextView) findViewById(2131558729);
        this.mEmailTextView = (TextView) findViewById(2131558730);
        this.mNavigationView = (NavigationView) findViewById(2131558581);
        this.mNavigationView.setItemIconTintList(null);
        this.mNavigationView.setNavigationItemSelectedListener(new C10653());
        Typeface openSansRegularTypeface = Typeface.createFromAsset(getAssets(), getString(2131165265));
        Typeface openSansSemiboldTypeface = Typeface.createFromAsset(getAssets(), getString(2131165266));
        this.mUsernameTextView.setTypeface(openSansSemiboldTypeface);
        this.mEmailTextView.setTypeface(openSansRegularTypeface);
        setNavigationMenuItemsAppearance(this.mNavigationView, openSansSemiboldTypeface, getResources().getDimension(C0086R.dimen.abc_text_size_small_material));
        this.mDrawerToggleManager = new C10664(this, (DrawerLayout) findViewById(2131558578), getToolbar(), 2131165560, 2131165559);
        this.mDrawerToggleManager.setDrawerIndicatorEnabled(true);
        this.mDrawerLayout.setDrawerShadow(2130837628, 8388613);
        this.mDrawerLayout.setDrawerListener(this.mDrawerToggleManager);
        hideStaticPagesDrawerItems();
        setMenuItemCheckedBasedOnClass();
    }

    protected void updateChatMessagesIndicators() {
        updateChatMenuItemForUnreadMessages();
        updateDrawerIconForUnreadMessages();
    }

    private void updateDrawerIconForUnreadMessages() {
        OLXChat olxChat = ((MainApplication) getApplication()).getOlxChatInstance();
        if (olxChat == null) {
            getToolbar().setNavigationIcon(2130837674);
        } else if (olxChat.getChatUnreadStatusStorage().hasUnreadMessages()) {
            getToolbar().setNavigationIcon(2130837677);
        } else {
            getToolbar().setNavigationIcon(2130837674);
        }
    }

    private void updateChatMenuItemForUnreadMessages() {
        OLXChat olxChat = ((MainApplication) getApplication()).getOlxChatInstance();
        if (olxChat == null) {
            setChatMenuItemIcon(2130837660);
        } else if (olxChat.getChatUnreadStatusStorage().hasUnreadMessages()) {
            setChatMenuItemIcon(2130837657);
        } else {
            setChatMenuItemIcon(2130837660);
        }
    }

    private void setChatMenuItemIcon(int iconRes) {
        this.mNavigationView.getMenu().findItem(2131558922).setIcon(iconRes);
    }

    private boolean shouldStartDeveloperToolsActivity() {
        long time = System.currentTimeMillis();
        if (this.mDeveloperToolsButtonClickStartMillis == 0 || time - this.mDeveloperToolsButtonClickStartMillis > 7000) {
            this.mDeveloperToolsButtonClickStartMillis = time;
            this.mDeveloperToolsButtonClickCount = 1;
        } else {
            this.mDeveloperToolsButtonClickCount++;
        }
        if (this.mDeveloperToolsButtonClickCount == 6) {
            return true;
        }
        return false;
    }

    private void startDeveloperToolsActivity() {
        startActivity(new Intent(this, DeveloperToolsActivity.class));
    }

    private void setNavigationMenuItemsAppearance(NavigationView navigationView, Typeface typeface, float textSize) {
        Menu menu = navigationView.getMenu();
        for (int i = 0; i < menu.size(); i++) {
            MenuItem menuItem = menu.getItem(i);
            SubMenu subMenu = menuItem.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    MenuItemTypefaceSpan.apply(typeface, subMenuItem);
                    MenuItemTextSizeSpan.apply(textSize, subMenuItem);
                }
            }
            MenuItemTypefaceSpan.apply(typeface, menuItem);
            MenuItemTextSizeSpan.apply(textSize, menuItem);
        }
    }

    public void setMenuItemCheckedBasedOnClass() {
        Class clazz = getClass();
        if (RemoteListActivity.class.equals(clazz)) {
            setMenuItemChecked(2131558920, true);
        }
        if (InsertAdActivity.class.equals(clazz)) {
            setMenuItemChecked(2131558921, true);
        }
        if (ViewAccountActivity.class.equals(clazz)) {
            setMenuItemChecked(2131558923, true);
        }
        if (ChatListActivity.class.equals(clazz)) {
            setMenuItemChecked(2131558922, true);
        }
    }

    private void setMenuItemChecked(int itemId, boolean checked) {
        this.mNavigationView.getMenu().findItem(itemId).setChecked(checked);
    }

    private void setDrawerItemVisible(int itemId, boolean visible) {
        this.mNavigationView.getMenu().findItem(itemId).setVisible(visible);
    }

    private void setFaqItemVisible(boolean visible) {
        setDrawerItemVisible(2131558926, visible);
    }

    private void setTermsOfUseItemVisible(boolean visible) {
        setDrawerItemVisible(2131558928, visible);
    }

    private void setSecurityTipsItemVisible(boolean visible) {
        setDrawerItemVisible(2131558927, visible);
    }

    private void hideStaticPagesDrawerItems() {
        setFaqItemVisible(false);
        setTermsOfUseItemVisible(false);
        setSecurityTipsItemVisible(false);
    }

    private void onAdsListMenuClick() {
        C1049M.getMessageBus().post(new MainAdListMenuClickedMessage());
        if (!isViewActive(RemoteListActivity.class)) {
            closeDrawerAndExecute(new C10675());
        }
    }

    private void onAdInsertionMenuClick() {
        C1049M.getMessageBus().post(new AdInsertionMenuClickedMessage());
        if (!isViewActive(InsertAdActivity.class)) {
            closeDrawerAndExecute(new C10686());
        }
    }

    private void onChatMenuClick() {
        C1049M.getMessageBus().post(new ChatListMenuClickedMessage());
        if (!isViewActive(ChatListActivity.class)) {
            closeDrawerAndExecute(new C10697());
        }
    }

    private void onMyAccountMenuClick() {
        C1049M.getMessageBus().post(new MyAccountMenuClickedMessage());
        if (!isViewActive(ViewAccountActivity.class)) {
            closeDrawerAndExecute(new C10708());
        }
    }

    private void onCustomerServiceMenuClick() {
        closeDrawerAndExecute(new C10719());
    }

    private void onFaqMenuClick() {
        closeDrawerAndExecute(new Runnable() {
            public void run() {
                C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_OPEN_STATIC_PAGE).setStaticPage(DrawerActivity.this.mFaqStaticPage).build());
                DrawerActivity.this.showStaticPage(DrawerActivity.this.mFaqStaticPage, DrawerActivity.this.mFaqStaticPage.label, 2131165484);
            }
        });
    }

    private void onSecurityTipsMenuClick() {
        closeDrawerAndExecute(new Runnable() {
            public void run() {
                C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_OPEN_STATIC_PAGE).setStaticPage(DrawerActivity.this.mSecurityTipsStaticPage).build());
                DrawerActivity.this.showStaticPage(DrawerActivity.this.mSecurityTipsStaticPage, DrawerActivity.this.mSecurityTipsStaticPage.label, 2131165484);
            }
        });
    }

    private void onTermsOfUseMenuClick() {
        closeDrawerAndExecute(new Runnable() {
            public void run() {
                C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_OPEN_STATIC_PAGE).setStaticPage(DrawerActivity.this.mTermsOfUseStaticPage).build());
                DrawerActivity.this.showStaticPage(DrawerActivity.this.mTermsOfUseStaticPage, DrawerActivity.this.mTermsOfUseStaticPage.label, 2131165484);
            }
        });
    }

    private void onGooglePlayRateMenuClick() {
        closeDrawerAndExecute(new Runnable() {
            public void run() {
                C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_OPEN_GOOGLE_PLAY).build());
                DrawerActivity.this.rateApp();
            }
        });
    }

    private void onFacebookLikeMenuClick() {
        closeDrawerAndExecute(new Runnable() {
            public void run() {
                C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_OPEN_FACEBOOK).build());
                DrawerActivity.this.sendToFacebook();
            }
        });
    }

    private void sendToFacebook() {
        try {
            startActivity(IntentsCreator.createGoToFacebookIntent(this));
        } catch (NullPointerException e) {
            Logger.error(TAG, "sendToFacebook: NullPointerException");
        }
    }

    private void contactCustomerService() {
        try {
            startActivity(IntentsCreator.createContactCustomerServiceIntent(getResources().getString(2131165402), C1049M.getConfigManager().getCustomerServiceEmail()));
        } catch (ActivityNotFoundException e) {
            DialogCreator dialogCreator = new DialogCreator(this, getString(2131165543));
            dialogCreator.getClass();
            dialogCreator.create(String.format(getString(2131165488), new Object[]{email}), new DialogButton(dialogCreator, 17039370), null).show();
        }
    }

    private void rateApp() {
        try {
            Intent goToMarket = IntentsCreator.createPlayStoreIntent(this);
            if (goToMarket != null) {
                startActivity(goToMarket);
            } else {
                Crouton.showText((Activity) this, 2131165579, CroutonStyle.ALERT);
            }
        } catch (ActivityNotFoundException e) {
            Logger.error(TAG, "rateApp: ActivityNotFoundException");
        }
    }

    private void showChatList() {
        if (C1049M.getAccountManager().isSignedIn()) {
            OLXChat olxChat = ((MainApplication) getApplication()).getOlxChatInstance();
            if (olxChat != null) {
                olxChat.showChatList(this);
                return;
            } else {
                reportChatError();
                return;
            }
        }
        startActivityForResult(LoginActivity.newIntent(this), 1042);
    }

    private void reportChatError() {
        Toast.makeText(this, 2131165377, 0).show();
        CrashAnalytics.logException(new Exception("DrawerActivity chat item was tapped, but  OLXChat is null. class of this activity is " + getClass().getSimpleName()));
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1042 && resultCode == -1) {
            showChatList();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showAdInsertion() {
        startActivity(InsertAdActivity.newIntent(this, ConfigContainer.getConfig().getAdInsertionRequiredAccountFields()));
    }

    private void showAccount() {
        Intent intent = ViewAccountActivity.newIntent(this);
        intent.setFlags(NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REQUEST);
        startActivity(intent);
    }

    private void showAdListing() {
        Intent intent = RemoteListActivity.newIntent(this);
        intent.addFlags(NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REQUEST);
        intent.addFlags(67108864);
        intent.addFlags(268435456);
        startActivity(intent);
    }

    private void showStaticPage(StaticPage staticPage, String errorTitle, int errorMessage) {
        if (staticPage == null || staticPage.url == null) {
            showErrorDialog(errorTitle, errorMessage);
            return;
        }
        C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.PAGE_STATIC_PAGES).setStaticPage(staticPage).build());
        startActivity(WebViewActivity.newIntent(this, staticPage.url, staticPage.label));
    }

    private void showErrorDialog(String title, int message) {
        DialogCreator dialogCreator = new DialogCreator(this, title);
        dialogCreator.getClass();
        dialogCreator.create(getString(message), new DialogButton(dialogCreator, 17039370), null).show();
    }

    private void updateDrawerHeader() {
        AccountManager accountManager = C1049M.getAccountManager();
        if (!accountManager.isSignedIn() || accountManager.getAccountApiModel() == null) {
            this.mUsernameTextView.setText(2131165550);
            this.mEmailTextView.setText(2131165542);
            this.mUserPictureImageView.setImageResource(2130837713);
        } else {
            Account account = accountManager.getAccountApiModel().account;
            this.mEmailTextView.setText(account.email);
            this.mUsernameTextView.setText(account.name);
            accountManager.loadThumbnailProfilePicture(this, new UserProfileImagesManagerFetchingListener() {
                public void onImageFetched(String imageUri, boolean fetchedFromCache, Bitmap bitmap) {
                    if (fetchedFromCache) {
                        DrawerActivity.this.mUserPictureImageView.setImageBitmap(bitmap);
                    } else {
                        FadeinDrawable.setBitmap(DrawerActivity.this.mUserPictureImageView, DrawerActivity.this, bitmap, 2000.0f);
                    }
                }

                public void onImageFetchingFailed() {
                    DrawerActivity.this.mUserPictureImageView.setImageResource(2130837713);
                }
            });
        }
        this.mDrawerHeader.setOnClickListener(new OnClickListener() {

            /* renamed from: com.schibsted.scm.nextgenapp.activities.DrawerActivity.16.1 */
            class C10621 implements Runnable {
                C10621() {
                }

                public void run() {
                    C1049M.getMessageBus().post(new UserDetailMenuClickedMessage());
                    if (!DrawerActivity.this.isViewActive(ViewAccountActivity.class)) {
                        C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_USER_SECTION).build());
                        DrawerActivity.this.showAccount();
                    }
                }
            }

            public void onClick(View v) {
                DrawerActivity.this.closeDrawerAndExecute(new C10621());
            }
        });
    }

    public void closeDrawerAndExecute(Runnable runnable) {
        this.mDrawerToggleManager.runOnClose(runnable);
        closeDrawer();
    }

    protected int getContentViewLayoutId() {
        return 2130903079;
    }

    protected int getFragmentContainerId() {
        return 2131558579;
    }

    protected void onStop() {
        super.onStop();
        if (isDrawerOpen()) {
            closeDrawer();
        }
    }

    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    protected void configureActionBar() {
    }

    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        this.mDrawerToggleManager.syncState();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        this.mDrawerToggleManager.onConfigurationChanged(newConfig);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (this.mDrawerToggleManager.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void setHorizontalPadding() {
        int width = getResources().getDisplayMetrics().widthPixels;
        int padding = (int) (((float) width) * Float.parseFloat(getResources().getString(2131165269)));
        findViewById(getFragmentContainerId()).setPadding(padding, 0, padding, 0);
    }

    public void closeDrawer() {
        this.mDrawerLayout.closeDrawer(8388611);
    }

    public boolean isDrawerOpen() {
        return this.mDrawerLayout.isDrawerOpen(8388611);
    }

    protected void onAccountStatusChanged(AccountStatusChangedMessage msg) {
        updateDrawerHeader();
    }

    protected void onConfigChanged(ConfigChangedMessage msg) {
        if (!msg.isFailed()) {
            ConfigApiModel configApiModel = msg.getConfig();
            if (configApiModel != null) {
                Map<String, StaticPage> staticPages = configApiModel.getMapOfStaticPages(null);
                if (staticPages != null) {
                    hideStaticPagesDrawerItems();
                    for (StaticPage staticPage : staticPages.values()) {
                        String str = staticPage.id;
                        boolean z = true;
                        switch (str.hashCode()) {
                            case -871999053:
                                if (str.equals(StaticPage.ID_BUY_SAFELY)) {
                                    z = true;
                                    break;
                                }
                                break;
                            case 101142:
                                if (str.equals(StaticPage.ID_FAQ)) {
                                    z = false;
                                    break;
                                }
                                break;
                            case 110250375:
                                if (str.equals(StaticPage.ID_TOS)) {
                                    z = true;
                                    break;
                                }
                                break;
                        }
                        switch (z) {
                            case C1608R.styleable.MapAttrs_mapType /*0*/:
                                this.mFaqStaticPage = staticPage;
                                setFaqItemVisible(true);
                                break;
                            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                                this.mTermsOfUseStaticPage = staticPage;
                                setTermsOfUseItemVisible(true);
                                break;
                            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                                this.mSecurityTipsStaticPage = staticPage;
                                setSecurityTipsItemVisible(true);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }

    private boolean isViewActive(Class<? extends DrawerActivity> clazz) {
        return clazz.equals(getClass());
    }
}
