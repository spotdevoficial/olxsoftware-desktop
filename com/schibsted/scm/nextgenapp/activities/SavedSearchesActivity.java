package com.schibsted.scm.nextgenapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.facebook.BuildConfig;
import com.facebook.share.internal.ShareConstants;
import com.schibsted.scm.nextgenapp.AdDetailIntentProvider;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.RemoteListManagerProvider;
import com.schibsted.scm.nextgenapp.backend.managers.list.DatedRemoteListManager;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteListManager;
import com.schibsted.scm.nextgenapp.database.SavedSearchesList;
import com.schibsted.scm.nextgenapp.ui.fragments.SavedSearchListFragment;

public class SavedSearchesActivity extends RequireSignInActivity implements AdDetailIntentProvider, RemoteListManagerProvider {
    private int mIndex;
    private DatedRemoteListManager mManager;
    private Bundle mState;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(2131165651);
        if (savedInstanceState != null && savedInstanceState.containsKey("EXTRAS_ARGUMENTS")) {
            this.mState = savedInstanceState.getBundle("EXTRAS_ARGUMENTS");
        } else if (getIntent().getExtras() != null) {
            this.mState = getIntent().getExtras().getBundle("EXTRAS_ARGUMENTS");
        }
        if (this.mState != null) {
            init(this.mState);
        }
    }

    public void init(Bundle bundle) {
        if (bundle.containsKey("LIST_INDEX") && this.mManager == null) {
            this.mIndex = this.mState.getInt("LIST_INDEX");
            this.mManager = C1049M.getAccountManager().getSavedSearchesList().newListManagerFor(this.mIndex);
        }
    }

    public static Intent newIntent(Context context, int index) {
        Intent intent = new Intent(context, SavedSearchesActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("LIST_INDEX", index);
        intent.putExtra("EXTRAS_ARGUMENTS", bundle);
        return intent;
    }

    public Intent newAdDetailIntent(Bundle extras) {
        Intent intent = new Intent(this, RemoteDetailSavedSearchActivity.class);
        extras.putInt("LIST_INDEX", this.mIndex);
        intent.putExtra("EXTRAS_ARGUMENTS", extras);
        return intent;
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (this.mManager != null && this.mState != null) {
            this.mState.putInt("LIST_INDEX", this.mIndex);
            outState.putBundle("EXTRAS_ARGUMENTS", this.mState);
        }
    }

    public void onSignedIn() {
    }

    protected Fragment createFragment() {
        String subtitle = BuildConfig.VERSION_NAME;
        if (C1049M.getAccountManager() != null && getIntent().hasExtra("EXTRAS_ARGUMENTS")) {
            SavedSearchesList list = C1049M.getAccountManager().getSavedSearchesList();
            this.mIndex = getIntent().getExtras().getBundle("EXTRAS_ARGUMENTS").getInt("LIST_INDEX");
            subtitle = list.get(this.mIndex).getLabel(this);
        }
        SavedSearchListFragment fragment = SavedSearchListFragment.newInstance();
        Bundle args = new Bundle();
        args.putString(ShareConstants.TITLE, subtitle);
        args.putInt("LIST_INDEX", this.mIndex);
        fragment.setArguments(args);
        return fragment;
    }

    public RemoteListManager getRemoteListManager(Bundle bundle) {
        if (this.mManager == null) {
            this.mIndex = getIntent().getExtras().getBundle("EXTRAS_ARGUMENTS").getInt("LIST_INDEX");
            this.mManager = C1049M.getAccountManager().getSavedSearchesList().loadListManagerFor(this.mIndex);
        }
        return this.mManager;
    }
}
