package com.schibsted.scm.nextgenapp.adapters;

public class ListItem<Model> {
    private Model data;
    private String imageUrl;
    private String label;

    public ListItem(Model data, String label) {
        this.data = data;
        this.label = label;
    }

    public ListItem(Model data, String label, String imageUrl) {
        this(data, label);
        this.imageUrl = imageUrl;
    }

    public String getLabel() {
        return this.label;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }
}
