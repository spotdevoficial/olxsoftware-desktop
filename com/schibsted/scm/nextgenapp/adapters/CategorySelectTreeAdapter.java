package com.schibsted.scm.nextgenapp.adapters;

import android.os.Bundle;
import android.support.v7.appcompat.C0086R;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.database.dao.CategoryTreeDao;
import com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode;
import com.schibsted.scm.nextgenapp.ui.holders.SingleViewHolder;
import com.schibsted.scm.nextgenapp.ui.holders.TypedViewHolder;
import com.schibsted.scm.nextgenapp.ui.views.BrandIconView;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;
import java.util.ArrayList;
import java.util.List;

public class CategorySelectTreeAdapter extends AbstractSelectTreeAdapter<String, String> {
    private String categoryDaoId;
    private CategoryTreeDao dao;
    private DbCategoryNode mCurrentBranch;
    private List<DbCategoryNode> mCurrentLeafs;
    private OnItemClickListener mListener;
    private int mOffset;

    /* renamed from: com.schibsted.scm.nextgenapp.adapters.CategorySelectTreeAdapter.1 */
    class C10871 implements OnClickListener {
        final /* synthetic */ int val$i;

        C10871(int i) {
            this.val$i = i;
        }

        public void onClick(View v) {
            if (CategorySelectTreeAdapter.this.mListener != null) {
                CategorySelectTreeAdapter.this.mListener.onItemClick(null, v, this.val$i, (long) v.getId());
            }
        }
    }

    private class Tag {
        public BrandIconView icon;
        public TextView text;

        private Tag() {
        }
    }

    public CategorySelectTreeAdapter(String categoryDaoId, OnItemClickListener listener) {
        this.mOffset = 0;
        this.categoryDaoId = categoryDaoId;
        this.mListener = listener;
    }

    public boolean downLevel(View view, int position, long id) {
        String nodeId = ((DbCategoryNode) this.mCurrentLeafs.get(position - this.mOffset)).getId();
        boolean wentDown = this.dao.hasChildren(nodeId);
        if (wentDown) {
            setCurrentSelection(nodeId);
        }
        return wentDown;
    }

    public void initialize(String parentCode) {
        this.dao = C1049M.getDaoManager().getCategoryTree(this.categoryDaoId);
        this.mCurrentBranch = TextUtils.isEmpty(parentCode) ? this.dao.getRoot() : this.dao.getByCode(parentCode);
        this.mCurrentLeafs = this.dao.getChildren(this.mCurrentBranch.getId());
        setListItems(createListItems());
    }

    public boolean upLevel() {
        boolean wentUp = !TextUtils.isEmpty(this.mCurrentBranch.getParent());
        if (wentUp) {
            DbCategoryNode parent = this.dao.getNode(this.mCurrentBranch.getParent());
            if (parent != null) {
                setCurrentSelection(parent.getId());
            }
        }
        return wentUp;
    }

    public boolean hasDownLevel(int position) {
        try {
            return this.mCurrentLeafs != null && position - this.mOffset >= 0 && this.dao.hasChildren(((DbCategoryNode) this.mCurrentLeafs.get(position - this.mOffset)).getId());
        } catch (IndexOutOfBoundsException e) {
            StringBuilder builder = new StringBuilder();
            for (DbCategoryNode categoryNode : this.mCurrentLeafs) {
                builder.append(categoryNode.getLabel());
                builder.append(";");
            }
            CrashAnalytics.setInt("IOB_CategorySelectTreeAdapter_position", position);
            CrashAnalytics.setString("IOB_CategorySelectTreeAdapter_list", builder.toString());
            throw e;
        }
    }

    public boolean hasUpLevel() {
        return (this.mCurrentBranch == null || TextUtils.isEmpty(this.mCurrentBranch.getParent())) ? false : true;
    }

    private List<ListItem<String>> createListItems() {
        if (this.mCurrentLeafs == null) {
            return null;
        }
        List<ListItem<String>> result = new ArrayList(this.mCurrentLeafs.size());
        this.mOffset = 0;
        if (!(this.mCurrentLeafs.isEmpty() || TextUtils.isEmpty(this.mCurrentBranch.getAllLabel()))) {
            result.add(new ListItem(this.mCurrentBranch.getId(), this.mCurrentBranch.getAllLabel(), this.mCurrentBranch.getIcon()));
            this.mOffset = 1;
        }
        for (DbCategoryNode category : this.mCurrentLeafs) {
            result.add(new ListItem(category.getId(), category.getLabel(), category.getIcon()));
        }
        return result;
    }

    public void setCurrentSelection(String selection) {
        this.mCurrentBranch = this.dao.getNode(selection);
        this.mCurrentLeafs = this.dao.getChildren(selection);
        setListItems(createListItems());
    }

    public String getParentTitle() {
        return this.mCurrentBranch != null ? this.mCurrentBranch.getLabel() : null;
    }

    public void putSelection(int index, String key, Bundle bundle) {
        if (this.mCurrentBranch != null) {
            String code = (index - this.mOffset < 0 || this.mCurrentLeafs == null) ? this.mCurrentBranch.getCode() : ((DbCategoryNode) this.mCurrentLeafs.get(index - this.mOffset)).getCode();
            bundle.putString(key, code);
        }
    }

    public TypedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(2130903185, null, false);
        Tag tag = new Tag();
        tag.icon = (BrandIconView) view.findViewById(C0086R.id.icon);
        tag.text = (TextView) view.findViewById(C0086R.id.text);
        view.setTag(tag);
        return new SingleViewHolder((ViewGroup) view);
    }

    public void onBindViewHolder(TypedViewHolder typedViewHolder, int i) {
        Tag tag = (Tag) typedViewHolder.getView().getTag();
        tag.text.setText(getItem(i).getLabel());
        if (TextUtils.isEmpty(getItem(i).getImageUrl())) {
            tag.icon.setVisibility(4);
        } else {
            ImageLoader imageLoader = C1049M.getTrafficManager().getIconImageLoader();
            if (imageLoader != null) {
                tag.icon.setImageUrl(getItem(i).getImageUrl(), imageLoader);
                tag.icon.setVisibility(0);
            }
        }
        typedViewHolder.setOnClickListener(new C10871(i));
        StringBuilder builder = new StringBuilder();
        for (int j = 0; j < this.mList.size(); j++) {
            builder.append(getItem(j).getLabel());
            builder.append(";");
        }
        CrashAnalytics.setString("IOB_CategorySelectTreeAdapter_bind_list", builder.toString());
        CrashAnalytics.setInt("IOB_CategorySelectTreeAdapter_bind_size", this.mList.size());
    }

    public int getItemCount() {
        return this.mList.size();
    }
}
