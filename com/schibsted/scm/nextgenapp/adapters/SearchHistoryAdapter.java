package com.schibsted.scm.nextgenapp.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SearchHistoryAdapter extends CursorAdapter {
    private static final String TAG;
    private LayoutInflater mLayoutInflater;

    static {
        TAG = SearchHistoryAdapter.class.getSimpleName();
    }

    public SearchHistoryAdapter(Context context, Cursor cursor) {
        super(context, cursor, true);
        this.mLayoutInflater = LayoutInflater.from(context);
    }

    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return this.mLayoutInflater.inflate(2130903223, parent, false);
    }

    public void bindView(View view, Context context, Cursor cursor) {
        ((TextView) view.findViewById(16908308)).setText(cursor.getString(cursor.getColumnIndex("query")));
    }
}
