package com.schibsted.scm.nextgenapp.abtest.picker;

public interface ABTestVariantDefinition {
    String getId();
}
