package com.schibsted.scm.nextgenapp.abtest.picker;

import java.util.Random;

public class DefaultTestPicker implements ABTestVariantPicker {
    private Random mRandomGenerator;

    public DefaultTestPicker() {
        this.mRandomGenerator = new Random();
    }

    public ABTestVariantDefinition pick(ABTestVariantDefinition... variants) {
        return variants[this.mRandomGenerator.nextInt(variants.length)];
    }
}
