package com.schibsted.scm.nextgenapp.abtest.picker;

public abstract class ABTestWeightedVariantDefinition implements ABTestVariantDefinition {
    public abstract float getWeight();
}
