package com.schibsted.scm.android.lurker.cookie;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class LurkerCookieStore implements CookieStore {
    private static final String TAG;
    private Map<URI, List<HttpCookie>> mCookiesMap;
    private OnCookieChangeListener mOnCookieChangeListener;
    private final SharedPreferences mSharedPreferences;

    static {
        TAG = LurkerCookieStore.class.getSimpleName();
    }

    public LurkerCookieStore(Context context) {
        this.mCookiesMap = new HashMap();
        this.mSharedPreferences = context.getSharedPreferences("CookiePrefsFile", 0);
        for (Entry<String, ?> entry : this.mSharedPreferences.getAll().entrySet()) {
            if (entry.getValue() instanceof HashSet) {
                String uri = (String) entry.getKey();
                try {
                    loadCookiesFromSet(new URI(uri), (HashSet) entry.getValue());
                } catch (URISyntaxException e) {
                    Log.e(TAG, "Could not load " + uri + "cookies", e);
                }
            }
        }
    }

    private void loadCookiesFromSet(URI uri, HashSet<String> cookieSet) {
        Iterator i$ = cookieSet.iterator();
        while (i$.hasNext()) {
            List<HttpCookie> cookies;
            String cookie = (String) i$.next();
            if (this.mCookiesMap.containsKey(uri)) {
                cookies = (List) this.mCookiesMap.get(uri);
            } else {
                cookies = new ArrayList();
            }
            cookies.addAll(HttpCookie.parse(cookie));
            this.mCookiesMap.put(uri, filterExpiredCookies(cookies));
        }
    }

    public List<HttpCookie> get(URI uri) {
        if (!this.mCookiesMap.containsKey(uri)) {
            this.mCookiesMap.put(uri, new ArrayList());
        }
        return filterExpiredCookies((List) this.mCookiesMap.get(uri));
    }

    private List<HttpCookie> filterExpiredCookies(List<HttpCookie> cookiesToFilter) {
        List<HttpCookie> toReturn = new ArrayList();
        for (HttpCookie cookie : cookiesToFilter) {
            if (!cookie.hasExpired()) {
                toReturn.add(cookie);
            }
        }
        return toReturn;
    }

    private Set<String> filterDuplicateCookiesByKey(List<HttpCookie> unfilteredCookies) {
        Map<String, HttpCookie> cookieMap = new HashMap(unfilteredCookies.size());
        for (HttpCookie cookie : unfilteredCookies) {
            cookieMap.put(cookie.getName(), cookie);
        }
        Set<String> cookiesSet = new HashSet(cookieMap.size());
        for (HttpCookie cookie2 : cookieMap.values()) {
            cookiesSet.add(cookie2.toString());
        }
        return cookiesSet;
    }

    public void add(URI uri, HttpCookie cookie) {
        if (uri == null || cookie == null) {
            throw new IllegalArgumentException("You can't add a cookie for a null URI or a null HttpCookie instance!");
        }
        if (hasCookieValueChanged(uri, cookie) && this.mOnCookieChangeListener != null) {
            this.mOnCookieChangeListener.onChange(cookie);
        }
        ((List) this.mCookiesMap.get(uri)).add(cookie);
        persistCookies(uri);
    }

    private void persistCookies(URI uri) {
        this.mSharedPreferences.edit().putStringSet(uri.toString(), filterDuplicateCookiesByKey(get(uri))).apply();
    }

    private boolean hasCookieValueChanged(URI uri, HttpCookie newCookie) {
        for (HttpCookie cookie : get(uri)) {
            if (cookie.getName().equals(newCookie.getName())) {
                if (cookie.getValue().equals(newCookie.getValue())) {
                    return false;
                }
                return true;
            }
        }
        return true;
    }

    public boolean removeAll() {
        this.mCookiesMap.clear();
        return true;
    }

    public List<HttpCookie> getCookies() {
        Collection<List<HttpCookie>> values = this.mCookiesMap.values();
        List<HttpCookie> result = new ArrayList();
        for (List<HttpCookie> value : values) {
            result.addAll(filterExpiredCookies(value));
        }
        return result;
    }

    public List<URI> getURIs() {
        return new ArrayList(this.mCookiesMap.keySet());
    }

    public boolean remove(URI uri, HttpCookie cookie) {
        List<HttpCookie> cookies = (List) this.mCookiesMap.get(uri);
        return cookies != null && cookies.remove(cookie);
    }
}
