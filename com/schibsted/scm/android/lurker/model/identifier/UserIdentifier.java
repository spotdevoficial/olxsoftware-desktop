package com.schibsted.scm.android.lurker.model.identifier;

import com.schibsted.scm.android.lurker.model.LurkerEvent;

public class UserIdentifier {
    private String mAccountId;
    private String mAppsflyerId;
    private String mGoogleAdvertisementId;
    private String mUrbanAirshipChannelId;

    public UserIdentifier(String accountId, String urbanAirshipChannelId, String googleAdvertisementId, String appsflyerId) {
        this.mAccountId = accountId;
        this.mUrbanAirshipChannelId = urbanAirshipChannelId;
        this.mGoogleAdvertisementId = googleAdvertisementId;
        this.mAppsflyerId = appsflyerId;
    }

    public void identify(LurkerEvent lurkerEvent) {
        identifyPropertyIfNotNull(lurkerEvent, "account_id", this.mAccountId);
        identifyPropertyIfNotNull(lurkerEvent, "channel_id", this.mUrbanAirshipChannelId);
        identifyPropertyIfNotNull(lurkerEvent, "google_ad_id", this.mGoogleAdvertisementId);
        identifyPropertyIfNotNull(lurkerEvent, "appsflyer_id", this.mAppsflyerId);
    }

    private void identifyPropertyIfNotNull(LurkerEvent lurkerEvent, String propertyKey, String propertyValue) {
        if (propertyValue != null) {
            lurkerEvent.put(propertyKey, propertyValue);
        }
    }
}
