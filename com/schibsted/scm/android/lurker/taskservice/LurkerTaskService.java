package com.schibsted.scm.android.lurker.taskservice;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;
import com.schibsted.scm.android.lurker.cookie.LurkerCookieStore;
import com.schibsted.scm.android.lurker.network.LurkerApi;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;
import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;

public class LurkerTaskService extends GcmTaskService {
    private static final String TAG;
    private static LurkerApi sLurkerApi;
    private static OnLurkerEventSentListener sOnLurkerEventSentListener;

    static {
        TAG = LurkerTaskService.class.getSimpleName();
    }

    public int onRunTask(TaskParams taskParams) {
        if (!taskParams.getTag().equals("lurkerTaskOneOff")) {
            return 2;
        }
        initialize(getApplicationContext());
        Bundle extras = taskParams.getExtras();
        if (extras == null) {
            return 2;
        }
        String json = extras.getString("extrasLurkerEventList");
        int eventListSize = extras.getInt("extrasLurkerEventListSize");
        if (!sendEvents(json)) {
            return 2;
        }
        if (sOnLurkerEventSentListener != null) {
            sOnLurkerEventSentListener.onEventsSent(eventListSize);
        }
        return 0;
    }

    private boolean sendEvents(String eventsJson) {
        try {
            Response response = sLurkerApi.post(eventsJson);
            Log.i(TAG, response.message());
            return response.isSuccessful();
        } catch (IOException e) {
            Log.e(TAG, "Error sending events", e);
            return false;
        }
    }

    private static void initialize(Context context) {
        if (sLurkerApi == null) {
            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.setCookieHandler(new CookieManager(new LurkerCookieStore(context), CookiePolicy.ACCEPT_ALL));
            sLurkerApi = new LurkerApi(okHttpClient);
        }
    }

    public static void setOnLurkerEventSentListener(OnLurkerEventSentListener listener) {
        sOnLurkerEventSentListener = listener;
    }
}
