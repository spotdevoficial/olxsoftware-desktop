package com.schibsted.scm.android.lurker.exception;

public class LurkerEventConstructionException extends RuntimeException {
    public LurkerEventConstructionException(Exception e) {
        super(e);
    }
}
