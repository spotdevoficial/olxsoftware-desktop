package com.appsflyer.cache;

import java.util.Scanner;

public class RequestCacheData {
    private String cacheKey;
    private String postData;
    private String requestURL;
    private String version;

    public RequestCacheData(String urlString, String postData, String sdkBuildNumber) {
        this.requestURL = urlString;
        this.postData = postData;
        this.version = sdkBuildNumber;
    }

    public RequestCacheData(char[] chars) {
        Scanner scanner = new Scanner(new String(chars));
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (line.startsWith("url=")) {
                this.requestURL = line.substring("url=".length()).trim();
            } else if (line.startsWith("version=")) {
                this.version = line.substring("version=".length()).trim();
            } else if (line.startsWith("data=")) {
                this.postData = line.substring("data=".length()).trim();
            }
        }
        scanner.close();
    }

    public String getVersion() {
        return this.version;
    }

    public String getPostData() {
        return this.postData;
    }

    public String getRequestURL() {
        return this.requestURL;
    }

    public void setCacheKey(String cacheKey) {
        this.cacheKey = cacheKey;
    }

    public String getCacheKey() {
        return this.cacheKey;
    }
}
