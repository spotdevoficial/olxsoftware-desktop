package com.appsee;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.Window.Callback;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.facebook.BuildConfig;
import com.urbanairship.C1608R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ac */
class sn {
    private static sn f461G;
    private String f462A;
    private boolean f463l;

    public void m662H() throws Exception {
        if (m652C()) {
            v C = kb.m432C().m432C();
            Object C2 = C.m560C();
            if (C2 == null) {
                C2 = C;
            }
            xc C3 = m653C(C2);
            switch (lo.f300l[C3.ordinal()]) {
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    if (C2 instanceof Menu) {
                        m655C(C, (Menu) C2);
                    } else {
                        m661C((v) C2, 0);
                    }
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    m659H((v) C2, 0);
                default:
                    String C4 = m654C((v) C2);
                    if (m658C(C2, C4)) {
                        zc.m965C().m970C(C4, C3, true);
                        vd.m717C(1, lb.m479C("]QnP\u007f\u001eu[l\u001ezKoQ;MxL~[u\u0004;\u001bh"), C4);
                    }
            }
        }
    }

    private /* synthetic */ xc m653C(Object arg0) throws Exception {
        if (arg0 != null && (arg0 instanceof Menu)) {
            return xc.f543M;
        }
        v vVar = (v) arg0;
        Object C = vVar.m560C();
        if (C instanceof Window) {
            Callback C2 = vVar.m560C();
            if (C2 instanceof Activity) {
                return (ab.m32C(vVar.m560C(), EditText.class) < 2 || ab.m32C(vVar.m560C(), Button.class) <= 0) ? xc.f544l : xc.f540A;
            } else if (C2 instanceof AlertDialog) {
                if ((C2 instanceof TimePickerDialog) || (C2 instanceof DatePickerDialog)) {
                    return xc.f544l;
                }
                return ab.m32C(vVar.m560C(), AbsListView.class) > 0 ? xc.f543M : xc.f541B;
            }
        } else if (C instanceof PopupWindow) {
            return xc.f543M;
        }
        return xc.f544l;
    }

    public static synchronized sn m652C() {
        sn snVar;
        synchronized (sn.class) {
            if (f461G == null) {
                f461G = new sn();
            }
            snVar = f461G;
        }
        return snVar;
    }

    private /* synthetic */ void m655C(v arg0, Menu arg1) throws Exception {
        if (arg1 != null && m652C()) {
            List<View> H = arg0.m580H();
            if (H != null && !H.isEmpty()) {
                StringBuilder stringBuilder = new StringBuilder(H.size() * 50);
                stringBuilder.append(H.size());
                for (View C : H) {
                    MenuItem C2 = ab.m39C(C);
                    if (!(C2 == null || C2.getTitle() == null)) {
                        stringBuilder.append(C2.getTitle().toString());
                    }
                }
                String format = String.format(kl.m464C("~\"]2\u00164"), new Object[]{bb.m122a(stringBuilder.toString())});
                if (m658C((Object) arg1, format)) {
                    zc.m965C().m970C(format, xc.f543M, true);
                }
            }
        }
    }

    private /* synthetic */ boolean m658C(Object arg0, String arg1) throws Exception {
        if ((this.f462A != null || arg1 == null) && (this.f462A == null || this.f462A.equals(arg1))) {
            return false;
        }
        this.f462A = arg1;
        m653C(arg0);
        return true;
    }

    private /* synthetic */ void m659H(v arg0, int arg1) throws Exception {
        Object C = arg0.m560C();
        if (arg0.m560C() && (C instanceof AlertDialog)) {
            List<View> C2 = ab.m32C(arg0.m560C(), Button.class);
            if (ab.m98a((List) C2) != null) {
                String str;
                String str2 = BuildConfig.VERSION_NAME;
                String str3 = BuildConfig.VERSION_NAME;
                C = lc.m492C(lc.m492C(C, lb.m479C("SZR~Lo")), kl.m464C("^\u0013Z3_\""));
                if (C == null || !(C instanceof CharSequence)) {
                    str = str2;
                } else {
                    str = C.toString();
                }
                TextView textView = (TextView) arg0.m560C().findViewById(16908299);
                if (textView != null) {
                    str3 = textView.getText().toString();
                } else {
                    str3 = BuildConfig.VERSION_NAME;
                }
                List arrayList = new ArrayList();
                for (View view : C2) {
                    if (view != null && (view instanceof Button) && view.isShown() && view.getWidth() > 0 && view.getHeight() > 0) {
                        arrayList.add(((Button) view).getText().toString());
                    }
                }
                yc ycVar = new yc(str, str3, arrayList);
                str = ycVar.m815C();
                if (m658C((Object) arg0, str)) {
                    zc.m965C().m970C(str, xc.f541B, true);
                    zc.m965C().m969C(ycVar, str);
                    vd.m717C(1, lb.m479C("]QnP\u007f\u001eu[l\u001ezKoQ;MxL~[u\u0004;\u001bh"), str);
                }
            }
        }
    }

    public boolean m663H() {
        return this.f463l;
    }

    private /* synthetic */ sn() {
        this.f463l = false;
    }

    public void m660C() throws Exception {
        m658C(null, null);
    }

    private /* synthetic */ boolean m657C() {
        return fc.m211C().m211C() && ye.m826C().m894M();
    }

    private /* synthetic */ String m654C(v arg0) throws Exception {
        Object C = arg0.m560C();
        if (C == null) {
            return BuildConfig.VERSION_NAME;
        }
        Callback C2 = arg0.m560C();
        if (C instanceof Window) {
            if (C2 != null) {
                String name = C2.getClass().getName();
                if (bb.m114C(name)) {
                    return kl.m464C("\u001cf\tx\t|\u0010}g`\u0004a\u0002v\t\u0013\tr\nv\u001a");
                }
                if (name.lastIndexOf(lb.m479C("5")) > 0) {
                    return name.substring(name.lastIndexOf(kl.m464C("i")) + 1);
                }
                return name;
            } else if (arg0.m560C() != null && ab.m101a(arg0.m560C(), MediaController.class)) {
                return lb.m479C("V[\u007fWz}tPoLtRw[i");
            }
        }
        return C.getClass().getName();
    }

    public void m661C(v arg0, int arg1) throws Exception {
        List<View> C = ab.m32C(arg0.m560C(), AbsListView.class);
        AbsListView absListView = (AbsListView) ab.m98a((List) C);
        if (absListView != null) {
            Iterator it;
            List arrayList = new ArrayList();
            for (View view : C) {
                if (view.isShown() && view.getWidth() > 0 && view.getHeight() > 0) {
                    AbsListView absListView2 = (AbsListView) view;
                    int i = 0;
                    int i2 = 0;
                    while (i < absListView2.getChildCount()) {
                        Iterator it2 = ab.m32C(absListView2.getChildAt(i2), TextView.class).iterator();
                        for (it = it2; it.hasNext(); it = it2) {
                            arrayList.add(((TextView) it2.next()).getText().toString());
                        }
                        i = i2 + 1;
                        i2 = i;
                    }
                }
            }
            StringBuilder stringBuilder = new StringBuilder(arrayList.size() * 50);
            stringBuilder.append(arrayList.size());
            it = arrayList.iterator();
            for (Iterator it3 = it; it3.hasNext(); it3 = it) {
                stringBuilder.append((String) it.next());
            }
            String format = String.format(kl.m464C("~\"]2\u00164"), new Object[]{bb.m122a(stringBuilder.toString())});
            if (m658C((Object) arg0, format)) {
                xc xcVar;
                if (ab.m91H(absListView)) {
                    xcVar = xc.f544l;
                } else {
                    xcVar = xc.f543M;
                }
                zc.m965C().m970C(format, xc.f543M, true);
            }
        }
    }

    private /* synthetic */ void m656C(Object arg0) throws Exception {
        if (arg0 != null && (arg0 instanceof v)) {
            Callback C = ((v) arg0).m560C();
            if (C != null && C.getClass().getName().equalsIgnoreCase(kl.m464C("P(^iR)W5\\.WiZ)G\"A)R+\u001d&C7\u001d\u0004[(\\4V5r$G.E.G>"))) {
                this.f463l = true;
                return;
            }
        }
        this.f463l = false;
    }
}
