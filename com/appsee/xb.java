package com.appsee;

import android.os.Build.VERSION;
import com.urbanairship.C1608R;
import java.security.InvalidParameterException;
import java.util.List;

/* compiled from: e */
class xb {
    private p f538A;
    private e f539l;

    public void m780C(int arg0, int arg1, int arg2, int arg3, String arg4, boolean arg5) throws Exception {
        m778C();
        boolean C = AppseeNativeExtensions.m19C(this.f539l);
        if (this.f538A == null || !C) {
            throw new Exception(sc.m650C("!h\fg\r}Be\rh\u0006)\u0014`\u0006l\r)\u0007g\u0001f\u0006l\u0010"));
        }
        this.f538A.m160C(arg0, arg1, arg2, arg3, arg4, arg5);
        vd.m713C();
    }

    xb() {
        this.f539l = e.f484A;
    }

    public static List<String> m778C() {
        if (VERSION.SDK_INT >= 16) {
            return dd.m164C();
        }
        return null;
    }

    private /* synthetic */ void m779C() {
        if (VERSION.SDK_INT >= 9) {
            if (VERSION.SDK_INT >= 9 && VERSION.SDK_INT < 11) {
                this.f539l = e.f489l;
            } else if (VERSION.SDK_INT < 11 || VERSION.SDK_INT >= 14) {
                if (VERSION.SDK_INT >= 14 && VERSION.SDK_INT < 16) {
                    this.f539l = e.f489l;
                } else if (VERSION.SDK_INT < 16 || VERSION.SDK_INT >= 18) {
                    this.f539l = e.f485B;
                    if (VERSION.SDK_INT >= 21 && !ye.m826C().m919e()) {
                        this.f539l = e.f488M;
                    }
                    List a = ye.m826C().m904a();
                    if (!(a == null || a.isEmpty())) {
                        this.f539l = e.f487G;
                    }
                } else {
                    this.f539l = e.f487G;
                }
            }
            vd.m717C(1, qc.m586C("T1k1d b0'\"n0b;'1i7h0b&=t\"'"), this.f539l.toString());
            switch (vb.f497l[this.f539l.ordinal()]) {
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    this.f538A = new gc();
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    this.f538A = new dd();
                case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                    this.f538A = new pb();
                case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                    this.f538A = new xn();
                default:
            }
        }
    }

    public void m781C(bd arg0, long arg1) throws Exception {
        if (this.f538A == null) {
            throw new Exception(sc.m650C("'g\u0001f\u0006l\u0010)\ff\u0016)\u000bg\u000b}\u000bh\u000e`\u0018l\u0006)\u0004f\u0010)\u0007g\u0001f\u0006`\fnBh\f)\u000bd\u0003n\u0007"));
        } else if (arg0 == null) {
            throw new InvalidParameterException(qc.m586C("J!t ''r$w8~tn9f3bte!a2b&"));
        } else if (arg1 < 0) {
            throw new InvalidParameterException(sc.m650C("/|\u0011}By\u0010l\u0011l\f}\u0003}\u000bf\f]\u000bd\u0007\\\u0011"));
        } else {
            arg0.m133H();
            try {
                this.f538A.m161C(arg0, arg1);
            } finally {
                arg0.m129C();
            }
        }
    }

    public void m783H() throws Exception {
        if (this.f538A == null) {
            throw new Exception(qc.m586C("\u0011i7h0b&':h '=i=s=f8n.b0'2h&'1i7h0n:`tf:'=j5`1"));
        }
        this.f538A.m159C();
        this.f538A = null;
    }

    public boolean m782C() throws Exception {
        if (this.f538A != null) {
            return this.f538A.m159C();
        }
        throw new Exception(sc.m650C("'g\u0001f\u0006l\u0010)\ff\u0016)\u000bg\u000b}\u000bh\u000e`\u0018l\u0006)\u0004f\u0010)\u0007g\u0001f\u0006`\fnBh\f)\u000bd\u0003n\u0007"));
    }
}
