package com.appsee;

import android.os.Handler;
import android.os.Looper;

/* compiled from: a */
class mb {
    private Object f303A;
    private boolean f304B;
    private Handler f305G;
    private Runnable f306l;

    public static String m507C(String arg0) {
        int length = arg0.length();
        char[] cArr = new char[length];
        length--;
        int i = length;
        while (length >= 0) {
            int i2 = i - 1;
            cArr[i] = (char) (arg0.charAt(i) ^ 7);
            if (i2 < 0) {
                break;
            }
            length = i2 - 1;
            cArr[i2] = (char) (arg0.charAt(i2) ^ 68);
            i = length;
        }
        return new String(cArr);
    }

    public synchronized void m509C() {
        synchronized (this.f303A) {
            if (this.f304B) {
                this.f304B = false;
                this.f305G.removeCallbacks(this.f306l);
            }
        }
    }

    public mb(i arg0, int arg1) {
        this.f305G = new Handler(Looper.getMainLooper());
        this.f306l = null;
        this.f304B = false;
        this.f303A = new Object();
        this.f306l = new db(this, arg0, arg1);
    }

    public synchronized void m510H() {
        synchronized (this.f303A) {
            if (!this.f304B) {
                this.f304B = true;
                this.f305G.post(this.f306l);
            }
        }
    }
}
