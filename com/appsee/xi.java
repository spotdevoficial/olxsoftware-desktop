package com.appsee;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: eb */
class xi {
    private long f549A;
    private String f550G;
    private Map<String, Object> f551l;

    public void m794C(Map<String, Object> arg0) {
        this.f551l = arg0;
    }

    public JSONObject m791C() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(rd.m626C("K"), m788C());
        jSONObject.put(qi.m603C(" "), m788C());
        if (!(this.f551l == null || this.f551l.isEmpty())) {
            JSONObject jSONObject2 = new JSONObject();
            Iterator it = this.f551l.entrySet().iterator();
            for (Iterator it2 = it; it2.hasNext(); it2 = it) {
                Entry entry = (Entry) it.next();
                jSONObject2.put((String) entry.getKey(), entry.getValue());
            }
            jSONObject.put(rd.m626C("U"), jSONObject2);
        }
        return jSONObject;
    }

    public long m788C() {
        return this.f549A;
    }

    public xi(String arg0, long arg1, Map<String, Object> arg2) {
        this.f550G = arg0;
        this.f549A = arg1;
        this.f551l = arg2;
    }

    public void m793C(String arg0) {
        this.f550G = arg0;
    }

    public Map<String, Object> m790C() {
        return this.f551l;
    }

    public void m792C(long arg0) {
        this.f549A = arg0;
    }

    public String m789C() {
        return this.f550G;
    }
}
