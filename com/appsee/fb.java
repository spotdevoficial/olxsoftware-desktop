package com.appsee;

import com.facebook.BuildConfig;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;

/* compiled from: g */
class fb {
    private static final String f104A = "Appsee_3p";
    private static fb f105G;
    private HashMap<String, tb> f106l;

    private /* synthetic */ JSONArray m205C(boolean arg0) throws JSONException {
        JSONArray jSONArray;
        synchronized (this.f106l) {
            jSONArray = new JSONArray();
            Iterator it = this.f106l.values().iterator();
            for (Iterator it2 = it; it2.hasNext(); it2 = it) {
                jSONArray.put(((tb) it.next()).m665C());
            }
        }
        return jSONArray;
    }

    public JSONArray m207C() throws JSONException {
        return m205C(true);
    }

    public static String m204C(String arg0, boolean arg1, boolean arg2) {
        int i = 1;
        String C = mb.m507C("at\u001b\"7X!\" wac");
        Object[] objArr = new Object[4];
        objArr[0] = f104A;
        objArr[1] = arg0;
        objArr[2] = Integer.valueOf(arg1 ? 1 : 0);
        if (!arg2) {
            i = 0;
        }
        objArr[3] = Integer.valueOf(i);
        return String.format(C, objArr);
    }

    public void m209C(String arg0, String arg1, boolean arg2) {
        if (bb.m114C(arg0)) {
            vd.m724H(3, kl.m464C("\u0004R)\u00143\u00134V3\u0013tA#\u00137R5G>\u0013\u000ewg\u001egz)E&_.Wg]&^\""));
            return;
        }
        arg0 = arg0.trim().toLowerCase();
        synchronized (this.f106l) {
            String C = m204C(arg0, true, arg2);
            if (bb.m114C(arg1)) {
                this.f106l.remove(arg0);
            } else {
                this.f106l.put(arg0, new tb(arg0, arg1, true, arg2));
            }
            if (arg2) {
                gb.m282C(C, arg1);
            }
        }
    }

    public void m210H() {
        synchronized (this.f106l) {
            this.f106l.clear();
            m202C();
        }
    }

    private /* synthetic */ fb() {
        this.f106l = new HashMap();
    }

    public String m206C(String arg0, boolean arg1) {
        if (bb.m114C(arg0)) {
            vd.m724H(3, mb.m507C("\u0007f* 0'6b0u-b2bdN\u0000'\"h6'wu '4f6s='i'\ri2f(n '*f)b"));
            return null;
        }
        String C;
        arg0 = arg0.trim().toLowerCase();
        synchronized (this.f106l) {
            C = m204C(arg0, false, arg1);
            if (!this.f106l.containsKey(C)) {
                String replace = UUID.randomUUID().toString().replace(kl.m464C("j"), BuildConfig.VERSION_NAME);
                this.f106l.put(C, new tb(arg0, replace, false, arg1));
                if (arg1) {
                    gb.m282C(C, replace);
                }
            }
            C = ((tb) this.f106l.get(C)).m670H();
        }
        return C;
    }

    public void m208C() {
        synchronized (this.f106l) {
            this.f106l.putAll(gb.m278C(f104A));
        }
    }

    public static tb m203C(String arg0) {
        boolean z;
        boolean z2 = true;
        tb tbVar = new tb();
        String substring = arg0.substring(f104A.length() + 1);
        int lastIndexOf = substring.lastIndexOf(mb.m507C("X"));
        tbVar.m671H(substring.substring(0, lastIndexOf));
        String substring2 = substring.substring(lastIndexOf + 1);
        if (substring2.charAt(1) == '1') {
            z = true;
        } else {
            z = false;
        }
        tbVar.m668C(z);
        if (substring2.charAt(3) != '1') {
            z2 = false;
        }
        tbVar.m672H(z2);
        return tbVar;
    }

    public static synchronized fb m202C() {
        fb fbVar;
        synchronized (fb.class) {
            if (f105G == null) {
                f105G = new fb();
            }
            fbVar = f105G;
        }
        return fbVar;
    }
}
