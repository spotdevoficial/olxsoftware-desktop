package com.appsee;

public class AppseeSessionEndInfo {
    private String sessionId;

    public AppseeSessionEndInfo(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return this.sessionId;
    }
}
