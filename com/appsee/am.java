package com.appsee;

import java.lang.Thread.UncaughtExceptionHandler;

/* compiled from: dc */
class am implements UncaughtExceptionHandler {
    private static boolean f41G;
    private static Object f42l;
    private UncaughtExceptionHandler f43A;

    private static /* synthetic */ void m110K(he arg0) throws Exception {
        if (ei.f98B) {
            Thread thread = new Thread(new xl(arg0), kl.m464C("\u0006C7@\"V\u0004A&@/f7_(R#g/A\"R#"));
            thread.start();
            thread.join();
            return;
        }
        m111a(arg0);
    }

    public static void m108C(he arg0, boolean arg1) {
        try {
            ue.m680C(arg0.m314C(), kl.m464C("p&F [3\u00132]/R)W+V#\u0013\"K$V7G.\\)"));
            synchronized (f42l) {
                if (f41G) {
                    vd.m724H(3, rd.m626C("\u000bN1U(L6BxF*D+MxM9K<I1K?\u0005:@;D-V=\u00059I*@9A!\u00050D6A4L6BxD6J,M=WxF*D+M"));
                    f42l.wait(3000);
                    synchronized (f42l) {
                        f41G = false;
                        f42l.notify();
                    }
                    return;
                }
                f41G = true;
                if (arg1) {
                    m110K(arg0);
                } else {
                    ei.m199H(new en(arg0));
                }
                synchronized (f42l) {
                    f41G = false;
                    f42l.notify();
                }
            }
        } catch (Throwable e) {
            try {
                ue.m680C(e, kl.m464C("v5A(Ag[&]#_.] \u00132]$R(F [3\u00132]/R)W+V#\u0013\"K$V7G.\\)"));
                synchronized (f42l) {
                }
                f41G = false;
                f42l.notify();
            } catch (Throwable th) {
                synchronized (f42l) {
                }
                f41G = false;
                f42l.notify();
            }
        }
    }

    public void uncaughtException(Thread arg0, Throwable arg1) {
        m108C(new he(arg1), ei.f98B);
        try {
            if (this.f43A != null) {
                this.f43A.uncaughtException(arg0, arg1);
            }
        } catch (Throwable e) {
            ue.m680C(e, rd.m626C("\u001dW*J*\u00050D6A4L6BxP6F9J-B0QxP6M9K<I=Ax@ F=U,L7KxR1Q0\u00057W1B1K9IxM9K<I=W"));
        }
    }

    public static void m106C() {
        try {
            UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
            if (!(defaultUncaughtExceptionHandler instanceof am)) {
                Thread.setDefaultUncaughtExceptionHandler(new am(defaultUncaughtExceptionHandler));
            }
        } catch (Throwable e) {
            ue.m680C(e, kl.m464C("v5A(AgZ)\u00135V Z4G\"Agr7C4V\"v?P\"C3Z(]\u000fR)W+V5"));
        }
    }

    static {
        f42l = new Object();
        f41G = false;
    }

    am(UncaughtExceptionHandler arg0) {
        this.f43A = arg0;
    }

    private static /* synthetic */ void m111a(he arg0) {
        try {
            kb.m432C().m458D();
            fd.m245C().m251C(arg0);
        } catch (Throwable e) {
            ue.m680C(e, rd.m626C("\u001dW*J*\u0005>L6D4L\"L6BxF*D+M=AxV=V+L7K"));
        }
    }
}
