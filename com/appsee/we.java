package com.appsee;

import android.graphics.Rect;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: mc */
class we {
    private long f522A;
    private Rect f523l;

    public long m735C() {
        return this.f522A;
    }

    public void m738C(long arg0) {
        this.f522A = arg0;
    }

    public JSONObject m737C() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(mb.m507C("s"), this.f522A);
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(this.f523l.left);
        jSONArray.put(this.f523l.top);
        jSONArray.put(this.f523l.width());
        jSONArray.put(this.f523l.height());
        jSONObject.put(wc.m725C("P"), jSONArray);
        return jSONObject;
    }

    public void m739C(Rect arg0) {
        this.f523l = arg0;
    }

    public Rect m736C() {
        return this.f523l;
    }

    public we(Rect arg0, long arg1) {
        this.f522A = arg1;
        this.f523l = arg0;
    }
}
