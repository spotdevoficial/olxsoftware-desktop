package com.appsee;

import android.os.DeadObjectException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/* compiled from: c */
class wd implements InvocationHandler {
    final /* synthetic */ rd f520A;
    final /* synthetic */ Object f521l;

    public Object invoke(Object arg0, Method arg1, Object[] arg2) throws Throwable {
        ei.m191C(new kd(this, arg1, arg2));
        try {
            Object C = lc.m494C(this.f521l, arg1.getName(), arg1.getParameterTypes(), arg2);
            this.f520A.f422M = 0;
            return C;
        } catch (DeadObjectException e) {
            this.f327l.f422M = this.f520A.f422M + 1;
            if (!ye.m826C().m914b() && this.f520A.f422M <= 3) {
                return arg1.getReturnType() != Void.TYPE ? arg1.getReturnType().newInstance() : null;
            } else {
                throw e;
            }
        }
    }

    wd(rd arg0, Object arg1) {
        this.f520A = arg0;
        this.f521l = arg1;
    }
}
