package com.appsee;

import android.app.Activity;
import android.app.Application;
import android.os.DeadObjectException;
import com.facebook.BuildConfig;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: xb */
class lc {
    public static boolean m500C() {
        try {
            return Application.class.isAssignableFrom(Class.forName(m496C(Appsee.class, qi.m603C("'\u001a5\u001c "), 1).getClassName()));
        } catch (Exception e) {
            return false;
        }
    }

    private static /* synthetic */ Object m491C(Class<?> arg0, Object arg1, String arg2, Class<?>[] arg3, Object... arg4) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        try {
            Method declaredMethod = arg0.getDeclaredMethod(arg2, arg3);
            declaredMethod.setAccessible(true);
            return declaredMethod.invoke(arg1, arg4);
        } catch (NoSuchMethodException e) {
            Class superclass = arg0.getSuperclass();
            if (superclass != null) {
                return m491C(superclass, arg1, arg2, arg3, arg4);
            }
            throw e;
        }
    }

    public static void m499C(Object arg0, String arg1, Object arg2) throws Exception {
        Field C = m498C(arg0.getClass(), arg1);
        if (C != null) {
            C.setAccessible(true);
            C.set(arg0, arg2);
        }
    }

    private static /* synthetic */ Class<?> m490C(Class<?> arg0) {
        if (arg0.isAssignableFrom(Integer.class)) {
            return Integer.TYPE;
        }
        if (arg0.isAssignableFrom(Float.class)) {
            return Float.TYPE;
        }
        if (arg0.isAssignableFrom(Double.class)) {
            return Double.TYPE;
        }
        if (arg0.isAssignableFrom(Boolean.class)) {
            return Boolean.TYPE;
        }
        return arg0;
    }

    public static Object m494C(Object arg0, String arg1, Class<?>[] arg2, Object... arg3) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, DeadObjectException {
        return m491C(arg0.getClass(), arg0, arg1, arg2, arg3);
    }

    public static boolean m501C(String arg0) {
        if (bb.m114C(arg0)) {
            return false;
        }
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        int length = stackTrace.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            if (stackTrace[i2].getMethodName().equalsIgnoreCase(arg0)) {
                return true;
            }
            i = i2 + 1;
            i2 = i;
        }
        return false;
    }

    public static <T> T m492C(Object arg0, String arg1) throws Exception {
        Field C = m498C(arg0.getClass(), arg1);
        if (C == null) {
            return null;
        }
        C.setAccessible(true);
        return C.get(arg0);
    }

    public static Object m493C(Object arg0, String arg1, int arg2, Object... arg3) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        Object[] objArr = new Object[arg2];
        Class[] clsArr = new Class[arg2];
        int i = 0;
        int i2 = 0;
        while (i < arg2) {
            Class cls;
            objArr[i2] = arg3[i2];
            if (arg3[i2] == null) {
                cls = Object.class;
            } else {
                cls = m490C(arg3[i2].getClass());
            }
            clsArr[i2] = cls;
            i = i2 + 1;
            i2 = i;
        }
        return m491C(arg0.getClass(), arg0, arg1, clsArr, arg3);
    }

    public static String m497C(Class<?> arg0) {
        Method[] methods = arg0.getMethods();
        StringBuilder stringBuilder = new StringBuilder();
        int length = methods.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            Method method = methods[i2];
            Class[] parameterTypes = method.getParameterTypes();
            String str = BuildConfig.VERSION_NAME;
            int i3 = 0;
            int i4 = 0;
            while (i3 < parameterTypes.length) {
                if (i4 > 0) {
                    str = 0 + qi.m603C("Bt");
                }
                StringBuilder append = new StringBuilder().insert(0, str).append(parameterTypes[i4].getName()).append(qi.m603C("N5\u001c3"));
                i = i4 + 1;
                i4 = i;
                int i5 = i;
                str = append.append(i4).toString();
                i3 = i5;
            }
            i = i2 + 1;
            stringBuilder.append(0 + qi.m603C("t") + method.getName() + qi.m603C("N|") + str + qi.m603C("G^"));
            i2 = i;
        }
        return stringBuilder.toString();
    }

    public static StackTraceElement m502H() {
        return m496C(Thread.class, qi.m603C("3\u000b = \u000f7\u0005\u0000\u001c5\r1"), 3);
    }

    public static boolean m503H() {
        if (m496C(Activity.class, qi.m603C("\u0001:-&\u000b5\u001a1"), 0) == null && m496C(Activity.class, qi.m603C("\u0001:<1\u001d!\u00031"), 0) == null) {
            return false;
        }
        return true;
    }

    private static /* synthetic */ StackTraceElement m496C(Class<?> arg0, String arg1, int arg2) {
        Class cls;
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        if (stackTrace == null) {
            return null;
        }
        int i = 0;
        int i2 = 0;
        while (i < stackTrace.length) {
            Class cls2;
            StackTraceElement stackTraceElement = stackTrace[i2];
            try {
                cls2 = Class.forName(stackTraceElement.getClassName());
                cls = cls2;
            } catch (ClassNotFoundException e) {
                vd.m717C(1, qi.m603C("\u0017\u000f:\u0000;\u001at\b=\u00000N7\u00025\u001d'TtK'"), stackTraceElement.getClassName());
                cls2 = null;
                cls = null;
            }
            if (cls2 != null && arg0.isAssignableFrom(r4) && stackTraceElement.getMethodName().equalsIgnoreCase(arg1) && i2 + arg2 < stackTrace.length) {
                return stackTrace[i2 + arg2];
            }
            i = i2 + 1;
            i2 = i;
        }
        return null;
    }

    private static /* synthetic */ Field m498C(Class<?> arg0, String arg1) throws Exception {
        try {
            return arg0.getDeclaredField(arg1);
        } catch (NoSuchFieldException e) {
            Class superclass = arg0.getSuperclass();
            if (superclass != null) {
                return m498C(superclass, arg1);
            }
            throw e;
        }
    }

    lc() {
    }
}
