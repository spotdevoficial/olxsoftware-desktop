package com.appsee;

import android.graphics.Rect;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: uc */
class kl {
    private String f282A;
    private long f283B;
    private boolean f284D;
    private tl f285G;
    private String f286M;
    private String f287f;
    private Rect f288l;

    public static String m464C(String arg0) {
        int length = arg0.length();
        char[] cArr = new char[length];
        length--;
        int i = length;
        while (length >= 0) {
            int i2 = i - 1;
            cArr[i] = (char) (arg0.charAt(i) ^ 71);
            if (i2 < 0) {
                break;
            }
            length = i2 - 1;
            cArr[i2] = (char) (arg0.charAt(i2) ^ 51);
            i = length;
        }
        return new String(cArr);
    }

    public kl(tl arg0, String arg1, String arg2, long arg3, Rect arg4) {
        this.f285G = arg0;
        this.f282A = arg1;
        this.f287f = arg2;
        this.f283B = arg3;
        this.f288l = arg4;
    }

    public void m470C(tl arg0) {
        this.f285G = arg0;
    }

    public tl m467C() {
        return this.f285G;
    }

    public void m475H(String arg0) {
        this.f282A = arg0;
    }

    public void m471C(Boolean arg0) {
        this.f284D = arg0.booleanValue();
    }

    public long m466C() {
        return this.f283B;
    }

    public void m476a(String arg0) {
        this.f286M = arg0;
    }

    public String m468C() {
        return this.f282A;
    }

    public JSONObject m474H() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(mb.m507C("s"), this.f285G.ordinal());
        jSONObject.put(zb.m951C("o"), this.f283B);
        jSONObject.put(mb.m507C("n"), this.f287f);
        jSONObject.put(zb.m951C("l"), this.f282A);
        if (this.f285G == tl.f481f) {
            if (!bb.m114C(this.f286M)) {
                int i;
                jSONObject.put(mb.m507C("-c"), this.f286M);
                String C = zb.m951C("Fi");
                if (this.f284D) {
                    i = 1;
                } else {
                    i = 0;
                }
                jSONObject.put(C, i);
            }
            jSONObject.put(mb.m507C("e"), m465C());
        }
        return jSONObject;
    }

    public void m469C(long arg0) {
        this.f283B = arg0;
    }

    public void m472C(String arg0) {
        this.f287f = arg0;
    }

    private /* synthetic */ JSONObject m465C() throws JSONException {
        if (this.f288l == null || this.f288l.width() == 0 || this.f288l.height() == 0) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(zb.m951C("d"), this.f288l.left);
        jSONObject.put(mb.m507C("~"), this.f288l.top);
        jSONObject.put(zb.m951C("k"), this.f288l.right - this.f288l.left);
        jSONObject.put(mb.m507C("o"), this.f288l.bottom - this.f288l.top);
        return jSONObject;
    }

    public String m473H() {
        return this.f287f;
    }
}
