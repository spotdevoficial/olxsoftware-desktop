package com.appsee;

import android.view.MotionEvent;
import com.urbanairship.C1608R;
import java.util.HashMap;

/* compiled from: q */
class xd {
    private q f545A;
    short f546B;
    private HashMap<Integer, jd> f547G;
    int f548l;

    public xd(q arg0) {
        this.f547G = new HashMap();
        this.f548l = 30;
        this.f546B = (short) 1500;
        this.f545A = arg0;
    }

    private /* synthetic */ void m785C(int arg0, float arg1, float arg2, long arg3, MotionEvent arg4) {
        jd jdVar = (jd) this.f547G.get(Integer.valueOf(arg0));
        if (this.f545A != null && wn.m742C(Math.abs(arg1 - jdVar.f207l)) < ((float) this.f548l) && wn.m742C(Math.abs(arg2 - jdVar.f205B)) < ((float) this.f548l) && arg3 - jdVar.f206G >= ((long) this.f546B)) {
            this.f545A.m137C(arg4);
        }
    }

    public boolean m786C(MotionEvent arg0) {
        int actionIndex = arg0.getActionIndex();
        int pointerId = arg0.getPointerId(actionIndex);
        switch (arg0.getActionMasked()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                m784C(pointerId, arg0.getX(actionIndex), arg0.getY(actionIndex), arg0.getEventTime());
                break;
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                m785C(pointerId, arg0.getX(actionIndex), arg0.getY(actionIndex), arg0.getEventTime(), arg0);
                break;
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                m784C(pointerId, arg0.getX(actionIndex), arg0.getY(actionIndex), arg0.getEventTime());
                break;
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                m785C(pointerId, arg0.getX(actionIndex), arg0.getY(actionIndex), arg0.getEventTime(), arg0);
                break;
        }
        return false;
    }

    private /* synthetic */ void m784C(int arg0, float arg1, float arg2, long arg3) {
        this.f547G.put(Integer.valueOf(arg0), new jd(this, arg1, arg2, arg3));
    }
}
