package com.appsee;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;

@TargetApi(14)
/* compiled from: j */
class sd implements ActivityLifecycleCallbacks {
    final /* synthetic */ id f458l;

    private /* synthetic */ sd(id arg0) {
        this.f458l = arg0;
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityResumed(Activity arg0) {
        ei.m191C(new td(this, arg0));
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity arg0) {
        ei.m191C(new ed(this, arg0));
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityPaused(Activity arg0) {
        ei.m191C(new cd(this));
    }

    public void onActivityStopped(Activity arg0) {
        ei.m191C(new gd(this, arg0));
    }
}
