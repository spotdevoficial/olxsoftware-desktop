package com.appsee;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: qc */
class ri {
    private boolean f429A;
    final /* synthetic */ he f430B;
    private StackTraceElement[] f431G;
    private String f432M;
    private long f433l;

    public ri(he arg0, long arg1, String arg2, boolean arg3, StackTraceElement[] arg4) {
        this.f430B = arg0;
        this.f433l = arg1;
        this.f432M = arg2;
        this.f429A = arg3;
        this.f431G = arg4;
    }

    public JSONObject m641C() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(lb.m479C("r"), this.f433l);
        jSONObject.put(fc.m213C("i"), this.f432M);
        jSONObject.put(lb.m479C("x"), this.f429A);
        JSONArray jSONArray = new JSONArray();
        int i = 0;
        int i2 = 0;
        while (i < this.f431G.length) {
            i = i2 + 1;
            jSONArray.put(this.f430B.m310C(this.f431G[i2]));
            i2 = i;
        }
        jSONObject.put(fc.m213C("ns"), jSONArray);
        return jSONObject;
    }
}
