package com.appsee;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* compiled from: db */
class ei {
    static final String f97A = "com.appsee.Action.Mode";
    private static final Object f98B;
    static final String f99G = "com.appsee.Action.UploadMode";
    private static List<AppseeListener> f100M;
    private static HashMap<String, ExecutorService> f101l;

    public static void m193C(kj arg0, Bundle arg1) {
        Context C = bp.m151C();
        Intent intent = new Intent(C, AppseeBackgroundUploader.class);
        intent.putExtra(f97A, arg0.ordinal());
        if (arg1 != null) {
            intent.putExtras(arg1);
        }
        C.startService(intent);
        vd.m717C(1, bc.m124C("=2\u00009\u001a(T\u000f\u0011.\u00025\u00179T\r\u00019\u00019\u0010pT1\u001b8\u0011|I|Q/"), arg0.toString());
    }

    public static synchronized void m196C(String arg0, i arg1) {
        synchronized (ei.class) {
            if (!f101l.containsKey(arg0)) {
                f101l.put(arg0, Executors.newSingleThreadExecutor());
            }
            ((ExecutorService) f101l.get(arg0)).submit(new sk(arg1, arg0));
        }
    }

    public static void m195C(l arg0, boolean arg1) {
        Object obj;
        Object obj2 = f98B;
        synchronized (obj2) {
        }
        if (!(f100M == null || f100M.isEmpty())) {
            if (arg1) {
                m196C(bc.m124C("\u001d\u0004,\u00079\u0011q'9\u0007/\u001d3\u001aq85\u0007(\u00112\u0011."), new re(arg0));
                obj = obj2;
            }
            m200H(arg0);
        }
        obj = obj2;
    }

    static {
        f101l = new HashMap();
        f98B = new Object();
    }

    public static void m199H(i arg0) {
        if (f98B) {
            m191C(arg0);
            return;
        }
        try {
            Handler handler = new Handler(Looper.getMainLooper());
            Object obj = new Object();
            synchronized (obj) {
                handler.post(new vn(arg0, obj));
                obj.wait();
            }
        } catch (Throwable e) {
            ue.m680C(e, bc.m124C("\u0019\u0006.\u001b.T.\u00012\u001a5\u001a;T/\r2\u00174\u00063\u001a5\u000e9\u0010|\u001b2T1\u00155\u001a|\u00004\u00069\u00158"));
        }
    }

    public static void m190C(AppseeListener arg0) {
        synchronized (f98B) {
            if (f100M == null) {
                f100M = new ArrayList();
            }
            if (!f100M.contains(arg0)) {
                f100M.add(arg0);
            }
        }
    }

    public static void m191C(i arg0) {
        try {
            arg0.m156C();
        } catch (Throwable e) {
            ue.m680C(e, bc.m124C("2=\u0000=\u0018|1$\u00179\u0004(\u001d3\u001a|\u00004\u00063\u00032T5\u001a|\u0000=\u00077N|"));
        }
    }

    ei() {
    }

    public static void m192C(kj arg0) {
        m193C(arg0, null);
    }

    public static void m198H(AppseeListener arg0) {
        synchronized (f98B) {
            if (f100M != null && f100M.contains(arg0)) {
                f100M.remove(arg0);
            }
        }
    }

    public static boolean m197C() {
        return Thread.currentThread().equals(Looper.getMainLooper().getThread());
    }

    private static /* synthetic */ void m200H(l arg0) {
        if (f100M != null && !f100M.isEmpty()) {
            for (AppseeListener C : new ArrayList(f100M)) {
                try {
                    arg0.m461C(C);
                } catch (Throwable e) {
                    ue.m680C(e, bc.m124C("1.\u0006.\u001b.T.\u00012\u001a5\u001a;T\u001d\u0004,\u00079\u0011\u0010\u001d/\u00009\u001a9\u0006|\u0017=\u00180\u0016=\u00177"));
                }
            }
        }
    }
}
