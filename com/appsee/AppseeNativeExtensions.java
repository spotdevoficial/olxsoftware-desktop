package com.appsee;

import android.annotation.TargetApi;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Build.VERSION;
import com.urbanairship.C1608R;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/* compiled from: lc */
class AppseeNativeExtensions {
    private static final List<String> f15A;
    private static final int f16B = 1024;
    private static boolean f17G;
    private static boolean f18M;
    private static boolean f19l;

    private static native /* synthetic */ String changeEGLSwapBehavior(boolean z);

    private static native /* synthetic */ void convertAndEncode(int i, int i2, Bitmap bitmap, long j, boolean z);

    private static native /* synthetic */ void convertToYuv(int i, int i2, Bitmap bitmap, ByteBuffer byteBuffer, boolean z, boolean z2, boolean z3);

    private static native /* synthetic */ String copyEglImageBufferToBitmap(Bitmap bitmap, boolean z, boolean z2);

    public static native void crashNative();

    private static native /* synthetic */ String createAndAttachEglImage(int i, int i2, int i3);

    private static native /* synthetic */ String disposeEglImage();

    private static native /* synthetic */ boolean finishEncoding();

    private static native /* synthetic */ boolean initEncoder(String str, int i, int i2, int i3, int i4, String[] strArr, int[] iArr, boolean z, boolean z2, boolean z3);

    private static native /* synthetic */ void installSignalHandlers(String str, String str2);

    private static native /* synthetic */ void saveMainThreadId();

    @TargetApi(21)
    private static /* synthetic */ boolean m26K() {
        return VERSION.SDK_INT >= 21 && Build.SUPPORTED_64_BIT_ABIS != null && Build.SUPPORTED_64_BIT_ABIS.length > 0;
    }

    public static boolean m21C(String arg0, int arg1, int arg2, int arg3, int arg4, String[] arg5, int[] arg6, boolean arg7, boolean arg8, boolean arg9) {
        return !f19l ? false : initEncoder(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
    }

    private static /* synthetic */ boolean m20C(String arg0) {
        for (String str : m6C()) {
            if (!str.contains(arg0)) {
                if (str.equalsIgnoreCase(arg0)) {
                }
            }
            return true;
        }
        return false;
    }

    private static /* synthetic */ void m17C(boolean arg0) throws Exception {
        AssetManager assets = bp.m151C().getAssets();
        String str = null;
        List C = m6C();
        if (!C.isEmpty()) {
            int i;
            String C2 = bc.m124C("=\u0004,\u00079\u0011\u001d\u0007/\u0011(\u0007sQ/");
            Object[] objArr = new Object[1];
            if (arg0) {
                i = 1;
            } else {
                i = 0;
            }
            objArr[0] = C.get(i);
            str = String.format(C2, objArr);
        }
        if (bb.m114C(str)) {
            throw new Exception(sc.m650C("!h\fg\r}Bo\u000bg\u0006)\u0011|\u0012y\r{\u0016l\u0006)!Y7)\u0003{\u0001a\u000b}\u0007j\u0016|\u0010lBo\r{Bh\u0012y\u0011l\u0007)\fh\u0016`\u0014lBl\u001a}\u0007g\u0011`\rg\u0011"));
        }
        String[] list = assets.list(str);
        int length = list.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            Object obj = list[i3];
            File C3 = hp.m326C(hp.f172B, String.format(bc.m124C("y\u0007r\u00073"), new Object[]{obj}));
            if (!C3.exists()) {
                vd.m717C(1, sc.m650C("!f\u0012p\u000bg\u0005)GzGzGzB}\r)\u000ef\u0001h\u000e)\u0003y\u0012z\u0007lBo\re\u0006l\u0010"), str, File.separator, obj);
                m14C(new FileOutputStream(C3, false), assets.open(String.format(bc.m124C("Q/Q/Q/"), new Object[]{str, File.separator, obj})));
            }
            i2 = i3 + 1;
            i3 = i2;
        }
    }

    private static /* synthetic */ boolean m5A() {
        try {
            if (f19l) {
                return true;
            }
            m15C(m6C());
            f19l = true;
            return true;
        } catch (Throwable th) {
            ue.m680C(new Exception(th), sc.m650C("'{\u0010f\u0010)\u000ef\u0003m\u000bg\u0005)\fh\u0016`\u0014lBl\fj\rm\u0007{Bg\u0003}\u000b\u007f\u0007)\u0007q\u0016l\fz\u0016`\rg\u0011"));
            return false;
        }
    }

    public static void m13C(int arg0, int arg1, Bitmap arg2, ByteBuffer arg3, boolean arg4, boolean arg5, boolean arg6) {
        if (f17G) {
            convertToYuv(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
        }
    }

    public static String m8C(Bitmap arg0, boolean arg1, boolean arg2) {
        return !f18M ? null : copyEglImageBufferToBitmap(arg0, arg1, arg2);
    }

    public static void m11C() {
        if (f17G) {
            ei.m199H(new lc());
        }
    }

    public static boolean m19C(e arg0) {
        switch (xk.f552l[arg0.ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return m27a();
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return m5A();
            default:
                return true;
        }
    }

    public static boolean m18C() {
        return !f19l ? false : finishEncoding();
    }

    public static boolean m24H() {
        try {
            if (f18M) {
                return true;
            }
            String C;
            if (VERSION.SDK_INT < 21) {
                C = sc.m650C("#y\u0012z\u0007l'n\u000e@\u000fh\u0005l5{\u0003y\u0012l\u0010");
            } else {
                C = bc.m124C("5,\u0004/\u001191;\u0018\u0015\u0019=\u00139#.\u0015,\u00049\u0006qFm");
            }
            m15C(C);
            f18M = true;
            return true;
        } catch (Throwable th) {
            ue.m680C(new Exception(th), sc.m650C("L\u0010{\r{Be\rh\u0006`\fnBg\u0003}\u000b\u007f\u0007)\u0007n\u000e)\u000bd\u0003n\u0007)\u0015{\u0003y\u0012l\u0010"));
            return false;
        }
    }

    private static /* synthetic */ void m15C(String arg0) throws Exception {
        vd.m717C(1, bc.m124C("\u0010\u001b=\u00105\u001a;Ty\u0007|\\2\u0015(\u001d*\u0011uX|\u001d/05\u00069\u0017(N|Q>"), arg0, Boolean.valueOf(false));
        m9C(false);
        File C = hp.m326C(hp.f172B, String.format(sc.m650C("e\u000bkGzLz\r"), new Object[]{arg0}));
        try {
            System.load(C.getAbsolutePath());
        } catch (Throwable e) {
            Throwable th = e;
            List C2 = m6C();
            if (vd.m717C(1, sc.m650C("J\u0017{\u0010l\f}BJ2\\BH @BH\u0010j\n`\u0016l\u0001}\u0017{\u00073B,\u0011"), bb.m113C(m6C(), bc.m124C("p"))) || (C2.size() > 1 && ((String) C2.get(0)).contains(bc.m124C("$Lj")))) {
                ue.m680C(th, sc.m650C("'{\u0010f\u0010)\u000ef\u0003m\u000bg\u0005)\u0003y\u0012z\u0007lBg\u0003}\u000b\u007f\u0007)\u000e`\u0000{\u0003{\u001b"));
                vd.m724H(2, bc.m124C("\u001a\u00155\u00189\u0010|\u00003T0\u001b=\u0010|\u0015,\u0004/\u00119T2\u0015(\u001d*\u0011|\u00185\u0016.\u0015.\rpT(\u0006%\u001d2\u0013|\u00003T0\u001b=\u0010|\u001d(T5\u001a|GnY>\u001d(T*\u0011.\u00075\u001b2"));
                hp.m322C(C);
                m9C(true);
                System.load(C.getAbsolutePath());
                return;
            }
            throw th;
        }
    }

    private static /* synthetic */ String m6C() {
        if (VERSION.SDK_INT < 9 || VERSION.SDK_INT >= 11) {
            return (VERSION.SDK_INT < 14 || VERSION.SDK_INT >= 16) ? null : sc.m650C("#y\u0012z\u0007l4`\u0006l\rL\fj\rm\u0007{O8V");
        } else {
            return bc.m124C("5,\u0004/\u00119\"5\u00109\u001b\u0019\u001a?\u001b8\u0011.Ye");
        }
    }

    static {
        f17G = false;
        f19l = false;
        f18M = false;
        f15A = new ArrayList();
    }

    public static boolean m28a() {
        try {
            if (!f17G) {
                vd.m717C(1, sc.m650C("J\u0017{\u0010l\f}BJ2\\BH @BH\u0010j\n`\u0016l\u0001}\u0017{\u00073B,\u0011"), bb.m113C(m6C(), bc.m124C("p")));
                if (m15C(sc.m650C("\u0003{\u000f")) || m15C(bc.m124C("\u00195\u0004/")) || m15C(sc.m650C("1T"))) {
                    m15C(sc.m650C("H\u0012y\u0011l\u0007G\u0003}\u000b\u007f\u0007L\u001a}\u0007g\u0011`\rg\u0011"));
                    f17G = true;
                    m6C();
                } else {
                    vd.m716C(1, bc.m124C("'7\u001d,\u00045\u001a;T0\u001b=\u00105\u001a;T=\u0004,\u00079\u0011|\u001a=\u00005\u00029T9\f(\u00112\u00075\u001b2T0\u001d>T8\u00019T(\u001b|\u001d2\u0002=\u00185\u0010|7\f!|5\u001e=|\u0015.\u00174\u001d(\u0011?\u0000)\u00069"));
                    return false;
                }
            }
            return true;
        } catch (Throwable th) {
            ue.m680C(new Exception(th), bc.m124C("\u0019\u0006.\u001b.T0\u001b=\u00105\u001a;T=\u0004,\u00079\u0011{\u0007|\u001a=\u00005\u00029T9\f(\u00112\u0007(\u001d3\u001a/"));
            return false;
        }
    }

    @TargetApi(21)
    private static /* synthetic */ List<String> m10C() {
        if (f15A.isEmpty()) {
            if (VERSION.SDK_INT < 21) {
                if (!bb.m114C(Build.CPU_ABI)) {
                    f15A.add(Build.CPU_ABI);
                }
                if (!bb.m114C(Build.CPU_ABI2)) {
                    f15A.add(Build.CPU_ABI2);
                }
            } else {
                String[] strArr = Build.SUPPORTED_ABIS;
                int length = strArr.length;
                int i = 0;
                int i2 = 0;
                while (i < length) {
                    i = i2 + 1;
                    f15A.add(strArr[i2]);
                    i2 = i;
                }
            }
        }
        return f15A;
    }

    public static String m22H() {
        return !f18M ? null : disposeEglImage();
    }

    private static /* synthetic */ void m14C(OutputStream arg0, InputStream arg1) throws Exception {
        byte[] bArr = new byte[f16B];
        InputStream arg12 = arg1;
        while (true) {
            int read = arg12.read(bArr);
            if (read != -1) {
                arg0.write(bArr, 0, read);
                arg12 = arg1;
            } else {
                arg1.close();
                arg0.flush();
                arg0.close();
                return;
            }
        }
    }

    public static void m12C(int arg0, int arg1, Bitmap arg2, long arg3, boolean arg4) {
        if (f19l) {
            convertAndEncode(arg0, arg1, arg2, arg3, arg4);
        }
    }

    public static String m7C(int arg0, int arg1, int arg2) {
        return !f18M ? null : createAndAttachEglImage(arg0, arg1, arg2);
    }

    AppseeNativeExtensions() {
    }

    public static void m27a() {
        if (f17G) {
            ei.m199H(new xh());
        }
    }

    public static String m9C(boolean arg0) {
        return !f18M ? null : changeEGLSwapBehavior(arg0);
    }
}
