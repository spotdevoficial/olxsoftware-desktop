package com.appsee;

import android.view.View;
import com.facebook.BuildConfig;
import java.util.Map;

/* compiled from: tc */
public final class Appsee {
    static final boolean f10A = false;
    static String f11B = null;
    static String f12G = null;
    static int f13M = 0;
    static final String f14l = "https://%s.api.appsee.com";

    public static void appendSDKType(String sdkType) {
        if (!bb.m114C(sdkType) && !f11B.endsWith(sdkType)) {
            f11B += sdkType;
        }
    }

    public static void forceNewSession() {
        try {
            ei.m192C(kj.f279M);
        } catch (Exception a) {
            ue.m680C(a, "Fatal error in Appsee:forceNewSession.");
        }
    }

    public static void setDebugToLogcat(boolean log) {
        int i;
        if (log) {
            i = 2;
        } else {
            i = 3;
        }
        try {
            vd.m715C(i);
        } catch (Exception a) {
            ue.m680C(a, "Fatal error in Appsee:setDebugToLog.");
        }
    }

    public static void removeAppseeListener(AppseeListener appseeListener) {
        try {
            ei.m198H(appseeListener);
        } catch (Exception a) {
            ue.m680C(a, "Fatal error in Appsee:removeAppseeListener.");
        }
    }

    public static void setUserId(String userId) {
        try {
            fc.m211C().m233H(userId);
        } catch (Exception a) {
            ue.m680C(a, "Fatal error in Appsee:setUserId.");
        }
    }

    public static void addEvent(String eventName) {
        try {
            fc.m211C().m227C(eventName, null);
        } catch (Exception a) {
            ue.m680C(a, "Fatal error in Appsee:addEvent.");
        }
    }

    public static void setLocation(double latitude, double longitude, float horizontalAccuracy, float verticalAccuracy) {
        try {
            fc.m211C().m225C(new od(latitude, longitude, horizontalAccuracy, verticalAccuracy));
        } catch (Exception a) {
            ue.m680C(a, "Fatal error in Appsee:setLocation.");
        }
    }

    public static void start(String apiKey) {
        if (bb.m114C(apiKey)) {
            throw new NullPointerException("apiKey cannot be null or empty");
        }
        try {
            f12G = String.format(f14l, new Object[]{bb.m122a(apiKey)});
            vd.m717C(2, "Starting Appsee v%s%s", f11B, BuildConfig.VERSION_NAME);
            fd.m245C().m259H(apiKey);
        } catch (Exception a) {
            ue.m680C(a, "Fatal error in Appsee:start.");
        }
    }

    public static void stop() {
        try {
            fd.m245C().m272j();
        } catch (Exception a) {
            ue.m680C(a, "Fatal error in Appsee:stop.");
        }
    }

    public static void unmarkViewAsSensitive(View view) {
        try {
            rc.m610C().m613C(view);
        } catch (Exception a) {
            ue.m680C(a, "Fatal error in Appsee:unmarkViewAsSensitive.");
        }
    }

    public static String generate3rdPartyId(String systemName, boolean isPersistent) {
        try {
            return fb.m202C().m206C(systemName, isPersistent);
        } catch (Exception a) {
            ue.m680C(a, "Fatal error in Appsee:generate3rdPartyId.");
            return null;
        }
    }

    public static void stopAndUpload() {
        try {
            fd.m245C().m249A(true);
        } catch (Exception a) {
            ue.m680C(a, "Fatal error in Appsee:stopAndUpload.");
        }
    }

    public static void markViewAsSensitive(View view) {
        try {
            rc.m610C().m615H(view);
        } catch (Exception a) {
            ue.m680C(a, "Fatal error in Appsee:markViewAsSensitive.");
        }
    }

    private /* synthetic */ Appsee() {
    }

    public static void addEvent(String eventName, Map<String, Object> properties) {
        try {
            fc.m211C().m227C(eventName, (Map) properties);
        } catch (Exception a) {
            ue.m680C(a, "Fatal error in Appsee:addEvent.");
        }
    }

    public static void addAppseeListener(AppseeListener appseeListener) {
        try {
            ei.m190C(appseeListener);
        } catch (Exception a) {
            ue.m680C(a, "Fatal error in Appsee:addAppseeListener.");
        }
    }

    public static void resume() {
        try {
            fd.m245C().m271i();
        } catch (Exception a) {
            ue.m680C(a, "Fatal error in Appsee:resume.");
        }
    }

    public static void setSkipStartValidation(boolean skipValidation) {
        fd.m245C().m264K(skipValidation);
    }

    public static void startScreen(String screenName) {
        try {
            zc.m965C().m970C(screenName, xc.f544l, f10A);
        } catch (Exception a) {
            ue.m680C(a, "Fatal error in Appsee:startScreen.");
        }
    }

    public static boolean getOptOutStatus() {
        try {
            return fd.m245C().m266a();
        } catch (Exception a) {
            ue.m680C(a, "Fatal error in Appsee:getOptOutStatus.");
            return f10A;
        }
    }

    public static void setLocationDescription(String description) {
        try {
            fc.m211C().m213C(description);
        } catch (Exception a) {
            ue.m680C(a, "Fatal error in Appsee:setLocationDescription.");
        }
    }

    static {
        f11B = "2.2";
        f13M = 3;
        f12G = "https://api.appsee.com";
    }

    public static void pause() {
        try {
            fd.m245C().m269c();
        } catch (Exception a) {
            ue.m680C(a, "Fatal error in Appsee:pause.");
        }
    }

    public static void setOptOutStatus(boolean isOptOut) {
        try {
            fd.m245C().m267a(isOptOut);
        } catch (Exception a) {
            ue.m680C(a, "Fatal error in Appsee:setOptOutStatus.");
        }
    }

    public static void set3rdPartyId(String systemName, String externalId, boolean isPersistent) {
        try {
            fb.m202C().m209C(systemName, externalId, isPersistent);
        } catch (Exception a) {
            ue.m680C(a, "Fatal error in Appsee:set3rdPartyId.");
        }
    }
}
