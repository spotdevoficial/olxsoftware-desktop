package com.appsee;

import android.graphics.Rect;
import com.facebook.BuildConfig;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: tb */
class fc {
    private static final int f107G = 2000;
    private static fc f108c;
    private final List<ec> f109A;
    private String f110B;
    private String f111D;
    private final List<we> f112E;
    private Rect f113F;
    private long f114M;
    private ml f115e;
    private final List<xi> f116f;
    private boolean f117g;
    private boolean f118h;
    private od f119j;
    private he f120l;

    public static String m213C(String arg0) {
        int length = arg0.length();
        char[] cArr = new char[length];
        length--;
        int i = length;
        while (length >= 0) {
            int i2 = i - 1;
            cArr[i] = (char) (arg0.charAt(i) ^ 7);
            if (i2 < 0) {
                break;
            }
            length = i2 - 1;
            cArr[i2] = (char) (arg0.charAt(i2) ^ 4);
            i = length;
        }
        return new String(cArr);
    }

    public Rect m217C() {
        return this.f113F;
    }

    private /* synthetic */ fc() {
        this.f118h = false;
        this.f117g = false;
        this.f120l = null;
        this.f109A = new ArrayList();
        this.f116f = new ArrayList();
        this.f112E = new ArrayList();
        this.f115e = ml.f314D;
    }

    private /* synthetic */ boolean m215C(Object arg0) {
        if (arg0 == null) {
            return true;
        }
        if (arg0.getClass().equals(Double.class)) {
            double doubleValue = ((Double) arg0).doubleValue();
            if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
                return false;
            }
            return true;
        } else if (arg0.getClass().equals(Float.class)) {
            float floatValue = ((Float) arg0).floatValue();
            if (Float.isNaN(floatValue) || Float.isInfinite(floatValue)) {
                return false;
            }
            return true;
        } else if (arg0.getClass().equals(Integer.class) || arg0.getClass().equals(String.class) || arg0.getClass().equals(Date.class) || arg0.getClass().equals(URL.class) || arg0.getClass().equals(Boolean.class)) {
            return true;
        } else {
            return false;
        }
    }

    public void m225C(od arg0) {
        this.f119j = arg0;
    }

    public void m233H(String arg0) {
        this.f110B = arg0;
    }

    public void m226C(String arg0) {
        this.f111D = arg0;
    }

    public String m230H() {
        return this.f111D;
    }

    public void m234H(boolean arg0) {
        this.f114M = 0;
        this.f118h = false;
        synchronized (this.f109A) {
            this.f109A.clear();
        }
        synchronized (this.f112E) {
            this.f112E.clear();
        }
        lb.m478C().m478C();
        bh.m138C().m138C();
        uj.m685C().m703H();
        bc.m123C().m123C();
        if (arg0) {
            m237a();
        }
    }

    public static synchronized fc m212C() {
        fc fcVar;
        synchronized (fc.class) {
            if (f108c == null) {
                f108c = new fc();
            }
            fcVar = f108c;
        }
        return fcVar;
    }

    public boolean m235H() {
        return this.f117g;
    }

    public void m232H(Rect arg0) {
        this.f113F = arg0;
    }

    public void m236K() {
        this.f114M = wn.m741C();
        this.f118h = true;
        this.f117g = false;
        this.f120l = null;
        if (rd.m625C().m625C()) {
            uj.m685C().m700C(tl.f476E, rd.m625C().m639H() ? wc.m725C("\u0000D\u0001S") : kl.m464C("!R+@\""), null, null);
        }
        this.f115e = wn.m741C().m974C(true);
    }

    private /* synthetic */ void m214C(Rect arg0, long arg1) {
        if (m211C() && this.f113F != null) {
            synchronized (this.f112E) {
                Object obj = this.f113F;
                if (!this.f112E.isEmpty()) {
                    obj = ((we) this.f112E.get(this.f112E.size() - 1)).m735C();
                }
                if (!arg0.equals(obj)) {
                    vd.m717C(1, wc.m725C("5R\u0010_\u001aQTW\u0004FTP\u0006W\u0019STS\u0002S\u001aBN\u0016QE"), arg0.toString());
                    we weVar = new we(arg0, arg1);
                    if (weVar.m735C() < 0) {
                        weVar.m738C(0);
                    }
                    this.f112E.add(weVar);
                }
            }
        }
    }

    public he m218C() {
        return this.f120l;
    }

    public void m223C(he arg0) {
        this.f120l = arg0;
    }

    public void m224C(ml arg0) {
        long j = 0;
        if (this.f118h) {
            long C = m211C();
            if (wn.m758H() && !ye.m826C().m920f()) {
                m214C(ab.m40C(kb.m432C().m432C()), C);
            }
            synchronized (this.f109A) {
                ml mlVar;
                ml arg02;
                if (this.f109A.isEmpty()) {
                    mlVar = this.f115e;
                    arg02 = arg0;
                } else {
                    mlVar = ((ec) this.f109A.get(this.f109A.size() - 1)).m183C();
                    arg02 = arg0;
                }
                if (arg02 != mlVar) {
                    vd.m717C(1, wc.m725C("5R\u0010_\u001aQTY\u0006_\u0011X\u0000W\u0000_\u001bXTS\u0002S\u001aBN\u0016QE"), arg0.toString());
                    List list = this.f109A;
                    if (C >= 0) {
                        j = C;
                    }
                    list.add(new ec(j, arg0));
                }
            }
        }
    }

    private /* synthetic */ int m211C() {
        int i;
        synchronized (this.f116f) {
            i = 0;
            for (xi xiVar : this.f116f) {
                if (xiVar.m788C() == null) {
                    i++;
                } else {
                    i = xiVar.m788C().size() + i;
                }
            }
        }
        return i;
    }

    public void m237a() {
        zc.m965C().m965C();
        fb.m202C().m210H();
        m211C();
    }

    public void m221C() {
        synchronized (this.f116f) {
            this.f116f.clear();
        }
    }

    public void m222C(Rect arg0) {
        m214C(arg0, m211C());
    }

    public void m231H() throws JSONException, IOException {
        try {
            long j;
            long C = m211C();
            if (C < 0) {
                j = 0;
            } else {
                j = C;
            }
            JSONArray C2 = bh.m138C().m138C();
            JSONArray C3 = uj.m685C().m685C();
            JSONArray C4 = zc.m965C().m965C();
            JSONArray H = zc.m965C().m971H();
            JSONArray C5 = fb.m202C().m202C();
            JSONArray jSONArray = new JSONArray();
            synchronized (this.f116f) {
                Iterator it = this.f116f.iterator();
                for (Iterator it2 = it; it2.hasNext(); it2 = it) {
                    jSONArray.put(((xi) it.next()).m788C());
                }
            }
            JSONArray jSONArray2 = new JSONArray();
            HashSet hashSet = new HashSet();
            synchronized (this.f109A) {
                for (ec ecVar : this.f109A) {
                    if (j - ecVar.m183C() > 2000) {
                        jSONArray2.put(ecVar.m183C());
                    } else {
                        hashSet.add(Long.valueOf(ecVar.m183C()));
                        vd.m717C(1, kl.m464C("\u000eT)\\5V#\u0013(A.V)G&G.\\)\u0013\"E\"]3\tg\u00164"), ecVar.m183C().toString());
                    }
                }
            }
            JSONArray jSONArray3 = new JSONArray();
            if (this.f113F == null) {
                this.f113F = ab.m37C();
            }
            jSONArray3.put(this.f113F.left);
            jSONArray3.put(this.f113F.top);
            jSONArray3.put(this.f113F.width());
            jSONArray3.put(this.f113F.height());
            this.f113F = null;
            JSONArray jSONArray4 = new JSONArray();
            synchronized (this.f112E) {
                for (we weVar : this.f112E) {
                    if (!hashSet.contains(Long.valueOf(weVar.m735C()))) {
                        jSONArray4.put(weVar.m735C());
                    }
                }
            }
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(wc.m725C("=X\u001dB\u001dW\u0018y\u0006_\u0011X\u0000W\u0000_\u001bX"), this.f115e.ordinal());
            jSONObject.put(kl.m464C("\u000e].G.R+r7C\u0001A&^\""), jSONArray3);
            jSONObject.put(wc.m725C("u\u0006W\u0007^\u0011R"), this.f117g);
            jSONObject.put(kl.m464C("w2A&G.\\)"), j);
            jSONObject.put(wc.m725C("`\u001dR\u0011Y&S\u0017Y\u0006R\u0011R"), ye.m826C().m866F());
            jSONObject.put(kl.m464C("p2@3\\*v1V)G4"), jSONArray);
            jSONObject.put(wc.m725C("e\u0017D\u0011S\u001aE"), C4);
            jSONObject.put(kl.m464C("c(C2C4"), H);
            jSONObject.put(wc.m725C("3S\u0007B\u0001D\u0011E"), C2);
            jSONObject.put(kl.m464C("\u0006P3Z(]4"), C3);
            jSONObject.put(wc.m725C("y\u0006_\u0011X\u0000W\u0000_\u001bX1@\u0011X\u0000E"), jSONArray2);
            jSONObject.put(kl.m464C("r7C\u0001A&^\"v1V)G4"), jSONArray4);
            jSONObject.put(wc.m725C("b\u001c_\u0006R$W\u0006B\r\u007f\u0010E"), C5);
            if (!bb.m114C(this.f110B)) {
                jSONObject.put(kl.m464C("\u0006C7f4V5z#"), this.f110B);
            }
            if (this.f119j != null) {
                jSONObject.put(wc.m725C("8Y\u0017W\u0000_\u001bX"), this.f119j.m533C());
            }
            if (!bb.m114C(this.f111D)) {
                jSONObject.put(kl.m464C("\u000b\\$R3Z(]\u0003V4P5Z7G.\\)"), this.f111D);
            }
            if (ye.m826C().m892L()) {
                jSONObject.put(wc.m725C("b\u001bC\u0017^1@\u0011X\u0000E"), lb.m478C().m478C());
            }
            if (ye.m826C().m826C() != null) {
                jSONObject.put(kl.m464C("\u0004\\)W.G.\\)@\nV3"), ye.m826C().m826C());
            }
            if (this.f120l != null) {
                jSONObject.put(wc.m725C("7D\u0015E\u001cz\u001bQ"), this.f120l.m314C());
            }
            if (ye.m826C().m826C()) {
                jSONObject.put(kl.m464C("q\"]$[*R5X4"), bc.m123C().m123C());
            }
            JSONObject C6 = dl.m177C().m177C();
            if (C6 != null) {
                jSONObject.put(wc.m725C("1N\u0000S\u0006X\u0015Z'r?E"), C6);
            }
            String jSONObject2 = jSONObject.toString();
            vd.m717C(1, kl.m464C("-@(]g\u000eg\u00164"), jSONObject2);
            hp.m326C(jSONObject2, ye.m826C().m904a());
            m234H(true);
        } catch (Throwable th) {
            m234H(true);
        }
    }

    public boolean m229C() {
        return this.f118h;
    }

    public void m228C(boolean arg0) {
        this.f117g = arg0;
    }

    public long m216C() {
        if (ye.m826C().m866F()) {
            if (f.m391C().m391C() == 0) {
                return -1;
            }
            return wn.m741C() - f.m391C().m391C();
        } else if (this.f114M != 0) {
            return wn.m741C() - this.f114M;
        } else {
            return -1;
        }
    }

    public void m227C(String arg0, Map<String, Object> arg1) {
        if (bb.m114C(arg0)) {
            vd.m724H(3, kl.m464C("&W#v1V)G}d.G/c5\\7V5G.V4\tgz)E&_.WgV1V)Gg]&^\"\tg\u0014`"));
            return;
        }
        String str;
        Map map;
        if (arg1 == null || arg1.isEmpty()) {
            str = BuildConfig.VERSION_NAME;
        } else {
            str = bb.m113C(arg1.values(), wc.m725C("X\u0016"));
        }
        vd.m717C(1, kl.m464C("r#W.] \u0013$F4G(^gV1V)G}\u0013b@g\u00164"), arg0, str);
        Map hashMap = new HashMap();
        if (!(arg1 == null || arg1.isEmpty())) {
            Iterator it = arg1.entrySet().iterator();
            Iterator it2 = it;
            while (it2.hasNext()) {
                Entry entry = (Entry) it.next();
                Object value = entry.getValue();
                if (bb.m114C((String) entry.getKey())) {
                    vd.m717C(3, wc.m725C("=X\u0002W\u0018_\u0010\u0016\u0004D\u001bF\u0011D\u0000OTX\u0015[\u0011\fT\u0011S\u0016\u0012Y\u0006\u0016\u0011@\u0011X\u0000\u0016S\u0013\u0007\u0011"), arg0);
                    it2 = it;
                } else if (m215C(value)) {
                    Object obj;
                    if (value == null) {
                        obj = BuildConfig.VERSION_NAME;
                    } else {
                        obj = value;
                    }
                    if (obj.getClass().equals(URL.class)) {
                        obj = ((URL) obj).toString();
                    }
                    if (obj.getClass().equals(Date.class)) {
                        obj = bb.m115C((Date) obj);
                    }
                    if (obj.getClass().equals(Boolean.class)) {
                        obj = obj.toString().toLowerCase();
                    }
                    hashMap.put(entry.getKey(), obj);
                    it2 = it;
                } else {
                    vd.m717C(3, kl.m464C("\u000e]1R+Z#\u00131R+F\"\u0013!\\5\u00137A(C\"A3Jg\u0014b@`\u0013(UgV1V)Gg\u0014b@`"), entry.getKey(), arg0);
                    it2 = it;
                }
            }
        }
        if (hashMap.isEmpty()) {
            map = null;
        } else {
            map = hashMap;
        }
        long C = m211C();
        if (C < 0) {
            C = 0;
        }
        xi xiVar = new xi(arg0, C, map);
        int size;
        if (xiVar.m788C() != null) {
            size = xiVar.m788C().size();
        } else {
            size = 1;
        }
        if (ye.m826C().m869H() <= 0 || r0 + m211C() <= ye.m826C().m869H()) {
            synchronized (this.f116f) {
                this.f116f.add(xiVar);
            }
            return;
        }
        vd.m717C(3, wc.m725C("\u007f\u0013X\u001bD\u001dX\u0013\u0016\u0011@\u0011X\u0000\u0016S\u0013\u0007\u0011TE\u001dX\u0017STO\u001bCTS\fU\u0011S\u0010S\u0010\u0016\u0000^\u0011\u0016\u0015[\u001bC\u001aBTY\u0012\u0016\u0011@\u0011X\u0000ETW\u0002W\u001dZ\u0015T\u0018ST_\u001a\u0016\rY\u0001DTF\u0018W\u001a\u0018"), xiVar.m788C());
    }

    public od m219C() {
        return this.f119j;
    }

    public String m220C() {
        return this.f110B;
    }
}
