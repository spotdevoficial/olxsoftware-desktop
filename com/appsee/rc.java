package com.appsee;

import android.annotation.TargetApi;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import com.facebook.internal.Utility;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* compiled from: ub */
class rc {
    private static rc f405l;
    private HashMap<WeakReference<View>, Rect> f406A;
    private HashSet<WeakReference<View>> f407G;

    private /* synthetic */ boolean m612C(int arg0, int arg1) {
        return (arg0 & arg1) == arg1;
    }

    public void m617C(View arg0) {
        synchronized (this.f407G) {
            Collection arrayList = new ArrayList();
            Iterator it = this.f407G.iterator();
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                if (weakReference.get() == null || ((View) weakReference.get()).equals(arg0)) {
                    arrayList.add(weakReference);
                }
            }
            this.f407G.removeAll(arrayList);
        }
    }

    private /* synthetic */ boolean m613C(View arg0) {
        v C = kb.m432C().m452C(arg0);
        if (C == null) {
            return false;
        }
        Window C2 = C.m560C();
        if (C2 == null) {
            return false;
        }
        LayoutParams attributes = C2.getAttributes();
        if (attributes == null || (attributes.flags & Utility.DEFAULT_STREAM_BUFFER_SIZE) == 0 || ye.m826C().m864E()) {
            return false;
        }
        this.f406A.put(null, ab.m39C(arg0));
        return true;
    }

    private /* synthetic */ boolean m614C(View arg0, List<View> arg1) {
        int size = arg1.size() - 1;
        int i = size;
        while (size >= 0) {
            View view = (View) arg1.get(i);
            if (arg0.getRootView() == view) {
                break;
            } else if (ab.m39C(view).contains(ab.m39C(arg0))) {
                return true;
            } else {
                size = i - 1;
                i = size;
            }
        }
        return false;
    }

    @TargetApi(11)
    private /* synthetic */ void m611C(View arg0, List<View> arg1) {
        if (arg0 instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) arg0;
            int i = 0;
            int i2 = 0;
            while (i < viewGroup.getChildCount()) {
                i = i2 + 1;
                m611C(viewGroup.getChildAt(i2), (List) arg1);
                i2 = i;
            }
        }
        if (m615H(arg0)) {
            synchronized (this.f406A) {
                if (arg0.isShown()) {
                    if (VERSION.SDK_INT >= 11 && arg0.getAlpha() <= 0.0f) {
                        return;
                    } else if (ye.m826C().m901W() || (ab.m97a(arg0) && !m611C(arg0, (List) arg1))) {
                        this.f406A.put(new WeakReference(arg0), ab.m39C(arg0));
                    }
                }
            }
        }
    }

    public HashMap<WeakReference<View>, Rect> m616C() {
        return this.f406A;
    }

    public static synchronized rc m610C() {
        rc rcVar;
        synchronized (rc.class) {
            if (f405l == null) {
                f405l = new rc();
            }
            rcVar = f405l;
        }
        return rcVar;
    }

    public void m620H(View arg0) {
        synchronized (this.f407G) {
            this.f407G.add(new WeakReference(arg0));
        }
    }

    private /* synthetic */ boolean m615H(View arg0) {
        if (ye.m826C().m939n() && EditText.class.isInstance(arg0)) {
            EditText editText = (EditText) arg0;
            if (m612C(editText.getInputType(), 129) || m612C(editText.getInputType(), 145) || m612C(editText.getInputType(), 18) || m612C(editText.getInputType(), 225)) {
                return true;
            }
        }
        if (ye.m826C().m836A() && (EditText.class.isInstance(arg0) || AutoCompleteTextView.class.isInstance(arg0))) {
            return true;
        }
        List H = ye.m826C().m869H();
        if (!(H == null || H.isEmpty())) {
            Iterator it = H.iterator();
            loop0:
            while (true) {
                Iterator it2 = it;
                while (it2.hasNext()) {
                    String str = (String) it.next();
                    if (str.startsWith(nc.m524C("'3%,7"))) {
                        Object obj = str.split(kl.m464C("}"))[1];
                        try {
                            Class cls = Class.forName(obj);
                            if (cls != null && cls.isInstance(arg0)) {
                                return true;
                            }
                        } catch (ClassNotFoundException e) {
                            vd.m717C(1, nc.m524C("\u001c%1*00\u007f\"6*;d<(>7,d0\"\u007f0&4:dz7"), obj);
                            it2 = it;
                        }
                    }
                }
                break loop0;
            }
        }
        synchronized (this.f407G) {
            Iterator it3 = this.f407G.iterator();
            while (it3.hasNext()) {
                WeakReference weakReference = (WeakReference) it3.next();
                if (weakReference.get() != null && ((View) weakReference.get()).equals(arg0)) {
                    return true;
                }
            }
            return false;
        }
    }

    private /* synthetic */ rc() {
        this.f407G = new HashSet();
        this.f406A = new HashMap();
    }

    public void m618C(List<View> arg0) throws Exception {
        synchronized (this.f406A) {
            this.f406A.clear();
        }
        if (fc.m211C().m211C() && arg0 != null && !arg0.isEmpty()) {
            int i = 0;
            int i2 = 0;
            while (i < arg0.size()) {
                View view = (View) arg0.get(i2);
                if (!m613C(view) && view.isShown()) {
                    m611C(view, (List) arg0);
                }
                i = i2 + 1;
                i2 = i;
            }
        }
    }

    public boolean m619C() {
        List<String> H = ye.m826C().m869H();
        if (!(H == null || H.isEmpty())) {
            for (String str : H) {
                String str2;
                if (str2.startsWith(nc.m524C(",'-!:*"))) {
                    str2 = str2.split(kl.m464C("}"))[1];
                    wc C = zc.m965C().m965C();
                    if (C != null && C.m726C().equalsIgnoreCase(str2)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
