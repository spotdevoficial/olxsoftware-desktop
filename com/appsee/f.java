package com.appsee;

import android.annotation.TargetApi;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import com.urbanairship.C1608R;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.CountDownLatch;

/* renamed from: com.appsee.k */
class f {
    private static final int f215E = 100;
    private static f f216H = null;
    private static final int f217e = 10;
    private static final int f218k = 2000;
    private Thread f219A;
    private long f220B;
    private long f221C;
    private Dimension f222D;
    private List<vc> f223F;
    private xb f224G;
    private qi f225I;
    private Dimension f226J;
    private final Rect f227K;
    private String f228L;
    private int f229M;
    private int f230O;
    private boolean f231R;
    private qi f232S;
    private final List<String> f233a;
    private pd f234b;
    private boolean f235c;
    private Paint f236d;
    private Exception f237f;
    private boolean f238g;
    private final List<Class<?>> f239h;
    private boolean f240i;
    private qi f241j;
    private mb f242l;
    private long f243m;
    private boolean f244n;
    private qi f245o;
    private final HashMap<View, Rect> f246p;
    private long f247q;
    private int f248r;
    private final Object f249t;
    private bd f250v;

    @TargetApi(14)
    private /* synthetic */ boolean m405C(Object[] arg0) throws Exception {
        if (arg0 == null || arg0.length == 0) {
            return false;
        }
        Object obj;
        int length = arg0.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            View view = (View) arg0[i2];
            if (ab.m90H(view, SurfaceView.class) || (VERSION.SDK_INT >= 14 && ab.m90H(view, TextureView.class))) {
                obj = 1;
                break;
            }
            i = i2 + 1;
            i2 = i;
        }
        obj = null;
        if (obj != null) {
            if (this.f220B == 0) {
                this.f220B = wn.m741C();
            }
            if (wn.m741C() - this.f220B < 750) {
                return true;
            }
        }
        this.f220B = 0;
        return false;
    }

    private /* synthetic */ void m416h() {
        while (this.f240i) {
            try {
                long C = wn.m741C();
                m413K();
                m388A();
                C = ((long) (vd.f504D / this.f230O)) - (wn.m741C() - C);
                if (C > 0) {
                    Thread.sleep(C);
                }
            } catch (Throwable e) {
                ue.m680C(e, nc.m524C("\u0019%+%3d:6-+-d6*\u007f26 :+\u007f076:%;j"));
                return;
            }
        }
        m388A();
        vd.m724H(1, nc.m524C("\u0002011 \u007f6:'06;-1#\u007f7++/d,-8*>("));
        m408H();
    }

    private /* synthetic */ void m410H(vc arg0) {
        this.f241j.m607K();
        m400C(arg0);
        this.f241j.m606H();
        this.f225I.m607K();
        if (this.f247q == 0) {
            this.f247q = arg0.m705C();
        }
        long C = (arg0.m705C() - this.f247q) * 1000;
        bd C2 = arg0.m705C();
        if (C > this.f221C || this.f221C == 0) {
            try {
                this.f224G.m781C(C2, C);
                this.f229M++;
                this.f221C = C;
            } catch (Throwable e) {
                ue.m680C(e, nc.m524C("\u00111%=(:d++\u007f3--+!\u007f&*\"9!-d++\u007f26 :+"));
            }
        }
        arg0.m710C(null);
        this.f234b.m555C(C2);
        this.f225I.m606H();
    }

    public boolean m428a() {
        return this.f244n;
    }

    private /* synthetic */ void m401C(vc arg0, Canvas arg1, View arg2) {
        int save = arg1.save();
        try {
            m398C(ab.m39C(arg2), arg0.m705C(), arg1);
            arg2.draw(arg1);
        } finally {
            arg1.restoreToCount(save);
        }
    }

    public void m420C(boolean arg0) {
        this.f235c = arg0;
    }

    private /* synthetic */ vc m392C() throws Exception {
        vc vcVar = new vc();
        vcVar.m709C(wn.m741C());
        vcVar.m711C(wn.m741C().m973C());
        vcVar.m710C(this.f234b.m553C());
        return vcVar.m705C() == null ? null : vcVar;
    }

    private /* synthetic */ void m411H(vc arg0, Canvas arg1, View arg2) {
        if (arg2 != null) {
            ViewParent parent = arg2.getParent();
            if (parent != null && (parent instanceof ViewGroup)) {
                ViewGroup viewGroup = (ViewGroup) parent;
                int i = 0;
                int i2 = 0;
                while (i < viewGroup.getChildCount()) {
                    View childAt = viewGroup.getChildAt(i2);
                    if (childAt != null && childAt.isShown() && !childAt.equals(arg2) && childAt.getWidth() > 2 && childAt.getHeight() > 2) {
                        m401C(arg0, arg1, childAt);
                    }
                    i = i2 + 1;
                    i2 = i;
                }
            }
        }
    }

    private /* synthetic */ void m397C() {
        this.f239h.clear();
        this.f233a.clear();
        List<String> C = ye.m826C().m826C();
        if (C != null) {
            for (String str : C) {
                try {
                    this.f239h.add(Class.forName(str));
                } catch (ClassNotFoundException e) {
                    vd.m717C(1, nc.m524C("\u001c%1*00\u007f\"6*;d1+\u007f6:*;!-d+=/!edz7"), str);
                    this.f233a.add(str);
                }
            }
        }
    }

    public bd m422H() {
        bd bdVar;
        synchronized (this.f249t) {
            if (this.f250v == null) {
                try {
                    this.f250v = this.f234b.m553C();
                } catch (Exception e) {
                    this.f250v = null;
                }
            } else {
                vd.m716C(1, nc.m524C("\u000b6&-1#\u007f00d/13(\u007f0(-<!\u007f\u0003\u0013d=19\":6\u007f\"06\u007f*0d-!>70*"));
            }
            bdVar = this.f250v;
        }
        return bdVar;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ void m388A() {
        /*
        r3 = this;
    L_0x0000:
        r1 = r3.f223F;
        monitor-enter(r1);
        r0 = r3.f223F;	 Catch:{ all -> 0x001b }
        r0 = r0.isEmpty();	 Catch:{ all -> 0x001b }
        if (r0 == 0) goto L_0x000d;
    L_0x000b:
        monitor-exit(r1);	 Catch:{ all -> 0x001b }
        return;
    L_0x000d:
        r0 = r3.f223F;	 Catch:{ all -> 0x001b }
        r2 = 0;
        r0 = r0.remove(r2);	 Catch:{ all -> 0x001b }
        r0 = (com.appsee.vc) r0;	 Catch:{ all -> 0x001b }
        monitor-exit(r1);	 Catch:{ all -> 0x001b }
        r3.m410H(r0);
        goto L_0x0000;
    L_0x001b:
        r0 = move-exception;
        monitor-exit(r1);	 Catch:{ all -> 0x001b }
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.k.A():void");
    }

    private /* synthetic */ void m402C(vc arg0, List<View> arg1) throws Exception {
        if (arg0 != null) {
            vc arg02;
            this.f246p.clear();
            for (Entry entry : rc.m610C().m610C().entrySet()) {
                View view;
                if (entry.getKey() != null) {
                    view = (View) ((WeakReference) entry.getKey()).get();
                } else {
                    view = null;
                }
                Rect rect = (Rect) entry.getValue();
                Rect C = ab.m39C(view);
                if (view == null || C.equals(rect) || ye.m826C().m941q()) {
                    this.f246p.put(view, entry.getValue());
                } else {
                    this.f246p.put(view, new Rect(Math.min(rect.left, C.left), Math.min(rect.top, C.top), Math.max(rect.right, C.right), Math.max(rect.bottom, C.bottom)));
                }
            }
            if (ye.m826C().m899R()) {
                f this;
                rc.m610C().m618C((List) arg1);
                HashSet hashSet = new HashSet();
                for (Entry entry2 : rc.m610C().m610C().entrySet()) {
                    Object obj;
                    if (entry2.getKey() != null) {
                        obj = (View) ((WeakReference) entry2.getKey()).get();
                    } else {
                        obj = null;
                    }
                    if (obj != null) {
                        hashSet.add(obj);
                        if (!this.f246p.containsKey(obj)) {
                            this.f246p.put(null, new Rect(0, 0, this.f222D.getWidth(), this.f222D.getHeight()));
                            this = this;
                            break;
                        }
                    }
                }
                this = this;
                for (Entry entry22 : this.f246p.entrySet()) {
                    if (!hashSet.contains(entry22.getKey())) {
                        this.f246p.put(null, new Rect(0, 0, this.f222D.getWidth(), this.f222D.getHeight()));
                        arg02 = arg0;
                        break;
                    }
                }
            }
            arg02 = arg0;
            arg02.m712C(this.f246p);
            List list = this.f223F;
            synchronized (list) {
            }
            List list2;
            if (this.f223F.size() > f217e) {
                ue.m679C(nc.m524C("\u0016)>#:d.1:1:d++0d=-8h\u007f74-/46*8j"));
                list2 = list;
            } else {
                this.f223F.add(arg0);
                list2 = list;
            }
        }
    }

    public void m429c() {
        this.f240i = false;
        this.f221C = 0;
        this.f229M = 0;
        this.f231R = true;
        if (this.f242l != null) {
            this.f242l.m509C();
        }
        this.f245o.m608a();
        this.f241j.m608a();
        this.f225I.m608a();
        this.f232S.m608a();
        if (this.f234b != null) {
            this.f234b.m553C();
            m391C();
        }
    }

    private /* synthetic */ void m406D() throws Exception {
        m402C(m391C(), null);
    }

    private /* synthetic */ f() {
        this.f249t = new Object();
        this.f220B = 0;
        this.f246p = new HashMap();
        this.f227K = new Rect();
        this.f245o = bc.m123C().m124C(nc.m524C("\f'-!:*,,00"));
        this.f241j = bc.m123C().m124C(nc.m524C("\f6 :\u00126!(7"));
        this.f225I = bc.m123C().m124C(nc.m524C("\u00126 :+\u001e4/!1 "));
        this.f232S = bc.m123C().m124C(nc.m524C("\u0011+1\u0003\u0013\t6<"));
        this.f223F = new ArrayList();
        this.f239h = new ArrayList();
        this.f233a = new ArrayList();
        this.f236d = new Paint();
        this.f240i = false;
        this.f236d.setColor(-16777216);
    }

    private /* synthetic */ void m408H() throws Exception {
        vc C = m391C();
        long j = ((this.f221C / 1000) + this.f247q) + ((long) (vd.f504D / this.f230O));
        if (C != null) {
            C.m709C(j);
            m410H(C);
        }
        while (this.f229M <= this.f230O * 2) {
            if (!Thread.currentThread().isInterrupted()) {
                C = m391C();
                if (C != null) {
                    j += (long) (vd.f504D / this.f230O);
                    C.m709C(j);
                    vd.m717C(1, nc.m524C("\u0005; 6*8d9+-': \u007f!24+=\u007f\"-%2!\u007f%+~z "), Long.valueOf(C.m705C()));
                    m410H(C);
                } else {
                    vd.m724H(2, nc.m524C("\u0005; 6*8d:):68!1'&d=19\":6\u007f\"06\u007f!1 6*8d)-;!0d86>':\"*(3="));
                    this.f234b.m557a();
                }
            } else {
                return;
            }
        }
        if (!Thread.currentThread().isInterrupted()) {
            try {
                this.f224G.m783H();
            } catch (Throwable e) {
                ue.m680C(e, nc.m524C("\u0001-606\u007f\"6*677-1#\u007f26 :+\u007f6:'06;-1#"));
                m417j();
            }
            if (!Thread.currentThread().isInterrupted()) {
                m412J();
            }
        }
    }

    private /* synthetic */ boolean m404C(List<View> arg0) {
        String str;
        View view;
        String str2;
        View view2;
        Object obj;
        if (ye.m826C().m903Y() || ab.m52C((List) arg0) == null) {
            str = null;
        } else {
            str = String.format(nc.m524C("6**1-1#\u007f%1-2%+-0*\u007f :0:'+!;dwa,m"), new Object[]{ab.m97a(view)});
        }
        if (ye.m826C().m868G() || ab.m84H((List) arg0) == null) {
            str2 = str;
        } else {
            str2 = String.format(nc.m524C("-11*6*8d\r-/43!\u007f :0:'+!;dwa,m"), new Object[]{ab.m97a(view2)});
        }
        if (!(this.f239h.isEmpty() && this.f233a.isEmpty())) {
            for (View view3 : arg0) {
                List C;
                if (!this.f239h.isEmpty()) {
                    List H = ab.m86H(view3, this.f239h);
                    if (!(H == null || H.isEmpty())) {
                        view2 = (View) H.get(0);
                        if (view2 == null && !this.f233a.isEmpty()) {
                            C = ab.m49C(view3, this.f233a);
                            if (!(C == null || C.isEmpty())) {
                                obj = (View) C.get(0);
                                continue;
                                if (obj != null) {
                                    str2 = String.format(nc.m524C("\u0016:*;!-\u00174-/d)-:3\u007f :0:'+!;dwa,m"), new Object[]{obj.getClass().getName()});
                                    obj = str2;
                                    break;
                                }
                            }
                        }
                        view3 = view2;
                        continue;
                        if (obj != null) {
                            str2 = String.format(nc.m524C("\u0016:*;!-\u00174-/d)-:3\u007f :0:'+!;dwa,m"), new Object[]{obj.getClass().getName()});
                            obj = str2;
                            break;
                        }
                    }
                }
                view2 = null;
                C = ab.m49C(view3, this.f233a);
                obj = (View) C.get(0);
                continue;
                if (obj != null) {
                    str2 = String.format(nc.m524C("\u0016:*;!-\u00174-/d)-:3\u007f :0:'+!;dwa,m"), new Object[]{obj.getClass().getName()});
                    obj = str2;
                    break;
                }
            }
        }
        str = str2;
        if (str2 == null) {
            return false;
        }
        vd.m717C(1, nc.m524C("\f/64/-1#\u007f6:*;!--1#\u007f26!(d=!<%*7:dz7"), obj);
        return true;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m423H(boolean r10) throws java.lang.Exception {
        /*
        r9 = this;
        r6 = 0;
        r3 = 300; // 0x12c float:4.2E-43 double:1.48E-321;
        r1 = 100;
        r2 = 0;
        r8 = 1;
        r4 = r9.f240i;	 Catch:{ all -> 0x00ec }
        if (r4 != 0) goto L_0x0076;
    L_0x000b:
        r4 = "\u001c%3(: \u007f\"6*677\u0016:'06;-1#\u007f37!1d1++d-!<+- 6*8j";
        r4 = com.appsee.nc.m524C(r4);	 Catch:{ all -> 0x00ec }
        com.appsee.ue.m679C(r4);	 Catch:{ all -> 0x00ec }
        r4 = r9.f242l;
        if (r4 == 0) goto L_0x001f;
    L_0x0018:
        r4 = r9.f242l;
        r4.m509C();
        r9.f242l = r6;
    L_0x001f:
        r9.f244n = r8;
        r9.f240i = r2;
        r4 = "\t>64!;d-!<+- 6*8dbd\u0011\u000b";
        r4 = com.appsee.nc.m524C(r4);
        com.appsee.vd.m724H(r8, r4);
    L_0x002c:
        r0 = r9;
    L_0x002d:
        r4 = r0.f238g;
        if (r4 != 0) goto L_0x0052;
    L_0x0031:
        if (r2 >= r1) goto L_0x0052;
    L_0x0033:
        r4 = "\u0013>-+-1#\u007f\"06\u007f26 :+\u007f00d=!\u007f6:%;=";
        r4 = com.appsee.nc.m524C(r4);
        r2 = r2 + 1;
        com.appsee.vd.m724H(r8, r4);
        r4 = 100;
        java.lang.Thread.sleep(r4);
        r4 = r9.f224G;
        if (r4 == 0) goto L_0x002c;
    L_0x0047:
        r4 = r9.f224G;
        r4 = r4.m778C();
        if (r4 == 0) goto L_0x002c;
    L_0x004f:
        r0 = r9;
        r1 = r3;
        goto L_0x002d;
    L_0x0052:
        r9.m426a();
        r9.m429c();
        if (r10 == 0) goto L_0x005d;
    L_0x005a:
        r9.m425I();
    L_0x005d:
        if (r2 < r1) goto L_0x0075;
    L_0x005f:
        r1 = r9.f219A;
        r1.interrupt();
        r9.m417j();
        r1 = new java.lang.Exception;
        r1.<init>();
        r2 = "\u000b-2!01+d(%606*8d9+-d)-;!0d++\u007f&:d-!> &j";
        r2 = com.appsee.nc.m524C(r2);
        com.appsee.ue.m683H(r1, r2);
    L_0x0075:
        return;
    L_0x0076:
        r4 = r9.f245o;	 Catch:{ all -> 0x00ec }
        r4.m604C();	 Catch:{ all -> 0x00ec }
        r4 = r9.f241j;	 Catch:{ all -> 0x00ec }
        r4.m604C();	 Catch:{ all -> 0x00ec }
        r4 = r9.f225I;	 Catch:{ all -> 0x00ec }
        r4.m604C();	 Catch:{ all -> 0x00ec }
        r4 = r9.f232S;	 Catch:{ all -> 0x00ec }
        r4.m604C();	 Catch:{ all -> 0x00ec }
        r4 = r9.f242l;
        if (r4 == 0) goto L_0x0095;
    L_0x008e:
        r4 = r9.f242l;
        r4.m509C();
        r9.f242l = r6;
    L_0x0095:
        r9.f244n = r8;
        r9.f240i = r2;
        r4 = "\t>64!;d-!<+- 6*8dbd\u0011\u000b";
        r4 = com.appsee.nc.m524C(r4);
        com.appsee.vd.m724H(r8, r4);
    L_0x00a2:
        r0 = r9;
    L_0x00a3:
        r4 = r0.f238g;
        if (r4 != 0) goto L_0x00c8;
    L_0x00a7:
        if (r2 >= r1) goto L_0x00c8;
    L_0x00a9:
        r4 = "\u0013>-+-1#\u007f\"06\u007f26 :+\u007f00d=!\u007f6:%;=";
        r4 = com.appsee.nc.m524C(r4);
        r2 = r2 + 1;
        com.appsee.vd.m724H(r8, r4);
        r4 = 100;
        java.lang.Thread.sleep(r4);
        r4 = r9.f224G;
        if (r4 == 0) goto L_0x00a2;
    L_0x00bd:
        r4 = r9.f224G;
        r4 = r4.m778C();
        if (r4 == 0) goto L_0x00a2;
    L_0x00c5:
        r0 = r9;
        r1 = r3;
        goto L_0x00a3;
    L_0x00c8:
        r9.m426a();
        r9.m429c();
        if (r10 == 0) goto L_0x00d3;
    L_0x00d0:
        r9.m425I();
    L_0x00d3:
        if (r2 < r1) goto L_0x0075;
    L_0x00d5:
        r1 = r9.f219A;
        r1.interrupt();
        r9.m417j();
        r1 = new java.lang.Exception;
        r1.<init>();
        r2 = "\u000b-2!01+d(%606*8d9+-d)-;!0d++\u007f&:d-!> &j";
        r2 = com.appsee.nc.m524C(r2);
        com.appsee.ue.m683H(r1, r2);
        goto L_0x0075;
    L_0x00ec:
        r4 = move-exception;
        r5 = r9.f242l;
        if (r5 == 0) goto L_0x00f8;
    L_0x00f1:
        r5 = r9.f242l;
        r5.m509C();
        r9.f242l = r6;
    L_0x00f8:
        r9.f244n = r8;
        r9.f240i = r2;
        r5 = "\t>64!;d-!<+- 6*8dbd\u0011\u000b";
        r5 = com.appsee.nc.m524C(r5);
        com.appsee.vd.m724H(r8, r5);
    L_0x0105:
        r0 = r9;
    L_0x0106:
        r5 = r0.f238g;
        if (r5 != 0) goto L_0x012b;
    L_0x010a:
        if (r2 >= r1) goto L_0x012b;
    L_0x010c:
        r5 = "\u0013>-+-1#\u007f\"06\u007f26 :+\u007f00d=!\u007f6:%;=";
        r5 = com.appsee.nc.m524C(r5);
        r2 = r2 + 1;
        com.appsee.vd.m724H(r8, r5);
        r6 = 100;
        java.lang.Thread.sleep(r6);
        r5 = r9.f224G;
        if (r5 == 0) goto L_0x0105;
    L_0x0120:
        r5 = r9.f224G;
        r5 = r5.m778C();
        if (r5 == 0) goto L_0x0105;
    L_0x0128:
        r0 = r9;
        r1 = r3;
        goto L_0x0106;
    L_0x012b:
        r9.m426a();
        r9.m429c();
        if (r10 == 0) goto L_0x0136;
    L_0x0133:
        r9.m425I();
    L_0x0136:
        if (r2 < r1) goto L_0x014e;
    L_0x0138:
        r1 = r9.f219A;
        r1.interrupt();
        r9.m417j();
        r1 = new java.lang.Exception;
        r1.<init>();
        r2 = "\u000b-2!01+d(%606*8d9+-d)-;!0d++\u007f&:d-!> &j";
        r2 = com.appsee.nc.m524C(r2);
        com.appsee.ue.m683H(r1, r2);
    L_0x014e:
        throw r4;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.k.H(boolean):void");
    }

    public bd m419C() {
        bd bdVar;
        synchronized (this.f249t) {
            bdVar = this.f250v;
            this.f250v = null;
        }
        return bdVar;
    }

    private /* synthetic */ void m412J() {
        this.f224G = null;
        this.f237f = null;
        vd.m724H(1, nc.m524C("\t-;!0d,%)!;j"));
        m429c();
        this.f238g = true;
    }

    private /* synthetic */ void m398C(Rect arg0, ml arg1, Canvas arg2) {
        f this;
        int i = 0;
        switch (f.f511l[arg1.ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                this = this;
                i = 90;
                break;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                this = this;
                i = 270;
                break;
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                this = this;
                i = 180;
                break;
            default:
                this = this;
                break;
        }
        arg2.scale(((float) this.f222D.getWidth()) / ((float) this.f226J.getWidth()), ((float) this.f222D.getHeight()) / ((float) this.f226J.getHeight()));
        if (i > 0) {
            float width;
            arg2.rotate((float) i, ((float) arg0.width()) / 2.0f, ((float) arg0.height()) / 2.0f);
            if (i == 90) {
                width = ((float) (arg0.width() - arg0.height())) / 2.0f;
                arg2.translate(width, width);
                arg2.translate((float) arg0.left, (float) (arg0.bottom - this.f226J.getWidth()));
            }
            if (i == 270) {
                width = ((float) (arg0.width() - arg0.height())) / -2.0f;
                arg2.translate(width, width);
                arg2.translate((float) (arg0.right - this.f226J.getHeight()), (float) arg0.top);
            }
            if (i == 180) {
                arg2.translate((float) (arg0.right - this.f226J.getWidth()), (float) (arg0.bottom - this.f226J.getHeight()));
                return;
            }
            return;
        }
        arg2.translate((float) arg0.left, (float) arg0.top);
    }

    private /* synthetic */ void m413K() {
        try {
            long C = wn.m741C();
            if (C - this.f243m > 2000) {
                vd.m717C(1, nc.m524C("\u0012!2+-=\u007f\u0011,%8!edzjm\"za"), Float.valueOf(wn.m771a()));
                this.f243m = C;
            }
        } catch (Exception e) {
        }
    }

    private /* synthetic */ void m400C(vc arg0) {
        if (arg0.m705C() != null && !arg0.m705C().isEmpty()) {
            bd C = arg0.m705C();
            if (C != null) {
                try {
                    C.m133H();
                    Canvas C2;
                    int save;
                    try {
                        C2 = C.m129C();
                        for (Rect rect : arg0.m705C()) {
                            save = C2.save();
                            m398C(rect, arg0.m705C(), C2);
                            this.f227K.set(0, 0, rect.width(), rect.height());
                            C2.drawRect(this.f227K, this.f236d);
                            C2.restoreToCount(save);
                        }
                        C.m129C();
                    } catch (Throwable th) {
                        C.m129C();
                    }
                } catch (Throwable e) {
                    ue.m680C(e, nc.m524C("\u0007>*1++d;6>3\u007f7:*,!+-)!\u007f6:'+7"));
                }
            }
        }
    }

    public void m425I() {
        this.f247q = 0;
    }

    public void m427a(boolean arg0) {
        this.f231R = arg0;
    }

    private /* synthetic */ void m414M() throws Exception {
        wn.m741C().m981a();
        Object[] C = kb.m432C().m432C();
        List C2 = ab.m60C(C);
        rc.m610C().m618C(C2);
        if (!ye.m826C().m897Q() && lb.m478C().m478C()) {
            return;
        }
        if (this.f235c || rc.m610C().m610C()) {
            m406D();
        } else if (m405C(C)) {
            vd.m724H(1, nc.m524C("\f/64/-1#\u007f26 :+\u007f\"-%2!"));
        } else {
            m403C(C2);
        }
    }

    public boolean m421C() {
        return this.f235c;
    }

    public void m430i() throws Exception {
        if (this.f240i) {
            ue.m679C(nc.m524C("\u001c%3(: \u007f4-!/%-!\u001e*;\u0017+%-0\r!<+- 6*8d(,:*\u007f%36:%;=\u007f6:'06;-1#"));
            return;
        }
        if (this.f226J == null) {
            this.f226J = wn.m741C().m974C(true);
        }
        this.f222D = new Dimension(ye.m826C().m836A(), ye.m826C().m826C());
        this.f248r = ye.m826C().m904a() * hp.f178c;
        this.f230O = (int) ye.m826C().m904a();
        m429c();
        m425I();
        f this;
        if (this.f234b == null) {
            this.f234b = new pd(2, this.f222D.getWidth(), this.f222D.getHeight());
            this = this;
        } else {
            this.f234b.m556H(2, this.f222D.getWidth(), this.f222D.getHeight());
            this = this;
        }
        synchronized (this.f223F) {
            this.f223F.clear();
        }
        this.f228L = hp.m342H(String.format(nc.m524C("a,jz7"), new Object[]{ye.m826C().m904a(), hp.f185l})).getAbsolutePath();
        vd.m717C(1, nc.m524C("\u00126 :+\u007f4>07dbdz7"), this.f228L);
        CountDownLatch countDownLatch = new CountDownLatch(1);
        this.f219A = new Thread(new f(this, countDownLatch), nc.m524C("\u001e4/7:!\t-;!0\u0014-+<!,706"));
        this.f240i = true;
        this.f238g = false;
        this.f219A.start();
        countDownLatch.await();
        if (this.f237f != null) {
            throw this.f237f;
        }
        m391C();
        this.f242l = new mb(new f(this), vd.f504D / this.f230O);
        this.f242l.m510H();
    }

    private /* synthetic */ void m403C(List<View> arg0) throws Exception {
        int i = 1;
        if (arg0 != null && !arg0.isEmpty()) {
            this.f245o.m607K();
            vc vcVar = new vc();
            vcVar.m709C(wn.m741C());
            vcVar.m711C(wn.m741C().m973C());
            if (!m403C((List) arg0)) {
                vc vcVar2;
                int i2;
                bd C = m391C();
                if (C != null) {
                    vcVar.m710C(C);
                    vcVar2 = vcVar;
                    i2 = 0;
                } else {
                    vcVar.m710C(this.f234b.m553C());
                    vcVar2 = vcVar;
                    i2 = 1;
                }
                if (vcVar2.m705C() == null) {
                    return;
                }
                if (vcVar.m705C().m129C()) {
                    try {
                        Canvas C2 = vcVar.m705C().m129C();
                        for (View view : arg0) {
                            if (null != null) {
                                if (this.f231R) {
                                    this.f232S.m607K();
                                    m411H(vcVar, C2, null);
                                    this.f232S.m606H();
                                }
                                i2 = 1;
                            } else if (i2 != 0) {
                                m401C(vcVar, C2, view);
                            }
                        }
                    } catch (Throwable e) {
                        if (vcVar.m705C() != null) {
                            pd pdVar = this.f234b;
                            i = vcVar.m705C();
                            pdVar.m555C(i);
                        }
                        ue.m680C(e, nc.m524C("\u0001-606\u007f -%(-1#\u007f26 :+\u007f\"-%2!"));
                        i = 0;
                        if (i != 0) {
                            m402C(vcVar, (List) arg0);
                        }
                        this.f245o.m606H();
                        return;
                    } finally {
                        vcVar = vcVar.m705C();
                        vcVar.m129C();
                    }
                    if (i != 0) {
                        m402C(vcVar, (List) arg0);
                    }
                    this.f245o.m606H();
                    return;
                }
                vd.m724H(1, nc.m524C("\u0007>*1++d3+</\u007f&*\"9!-d9+-d,'-!:*,,00\u007f6:*;!--1#"));
                m402C(vcVar, (List) arg0);
            }
        }
    }

    public void m426a() {
        this.f244n = false;
    }

    public long m418C() {
        return this.f247q;
    }

    public static synchronized f m391C() {
        f fVar;
        synchronized (f.class) {
            if (f216H == null) {
                f216H = new f();
            }
            fVar = f216H;
        }
        return fVar;
    }

    public boolean m424H() {
        return this.f240i;
    }

    private /* synthetic */ void m417j() {
        vd.m724H(2, nc.m524C("\u001b!3!+-1#\u007f26 :+\u007f\"6(:jqj"));
        hp.m322C(new File(this.f228L));
    }
}
