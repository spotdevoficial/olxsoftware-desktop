package com.appsee;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: u */
class zb {
    private long f642A;
    private short f643B;
    private short f644D;
    private boolean f645G;
    private wb f646M;
    private short f647l;

    public static String m951C(String arg0) {
        int length = arg0.length();
        char[] cArr = new char[length];
        length--;
        int i = length;
        while (length >= 0) {
            int i2 = i - 1;
            cArr[i] = (char) (arg0.charAt(i) ^ 28);
            if (i2 < 0) {
                break;
            }
            length = i2 - 1;
            cArr[i2] = (char) (arg0.charAt(i2) ^ 47);
            i = length;
        }
        return new String(cArr);
    }

    public zb(short arg0, short arg1, short arg2, long arg3, wb arg4, boolean arg5) {
        this.f643B = arg0;
        this.f644D = arg1;
        this.f647l = arg2;
        this.f642A = arg3;
        this.f646M = arg4;
        this.f645G = arg5;
    }

    public boolean m960C() {
        return this.f645G;
    }

    public short m961H() {
        return this.f643B;
    }

    public void m959C(boolean arg0) {
        this.f645G = arg0;
    }

    public void m956C(long arg0) {
        this.f642A = arg0;
    }

    public short m963a() {
        return this.f647l;
    }

    public short m955C() {
        return this.f644D;
    }

    public JSONObject m954C() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(qi.m603C("3"), this.f643B);
        jSONObject.put(fc.m213C("b"), this.f646M.ordinal());
        jSONObject.put(qi.m603C(","), this.f644D);
        jSONObject.put(fc.m213C("~"), this.f647l);
        jSONObject.put(qi.m603C(" "), this.f642A);
        if (this.f645G) {
            jSONObject.put(fc.m213C("l"), 1);
        }
        return jSONObject;
    }

    public void m957C(wb arg0) {
        this.f646M = arg0;
    }

    public long m952C() {
        return this.f642A;
    }

    public void m964a(short arg0) {
        this.f644D = arg0;
    }

    public void m962H(short arg0) {
        this.f643B = arg0;
    }

    public void m958C(short arg0) {
        this.f647l = arg0;
    }

    public wb m953C() {
        return this.f646M;
    }
}
