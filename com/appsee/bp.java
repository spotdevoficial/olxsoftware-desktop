package com.appsee;

import android.app.Application;
import android.app.Instrumentation;
import android.os.Build.VERSION;

/* compiled from: hc */
class bp {
    private static Application f57A = null;
    public static final String f58l = "APPSEE_NO_CONTEXT";

    bp() {
    }

    public static synchronized boolean m152C() {
        boolean z;
        synchronized (bp.class) {
            if (f57A == null) {
                try {
                    Class cls = Class.forName(zb.m951C("NrKn@uK2Nl_2n\u007f[uYu[e{t]yNx"));
                    if (VERSION.SDK_INT >= 9) {
                        f57A = (Application) cls.getMethod(zb.m951C("Li]nJr[]_lCuL}[u@r"), new Class[0]).invoke(null, (Object[]) null);
                    } else {
                        f57A = (Application) cls.getMethod(zb.m951C("Hy[]_lCuL}[u@r"), new Class[0]).invoke(cls.getMethod(zb.m951C("\u007fZn]yAhn\u007f[uYu[e{t]yNx"), new Class[0]).invoke(null, (Object[]) null), new Object[0]);
                    }
                } catch (Exception e) {
                    z = false;
                }
            }
            if (f57A != null) {
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public static boolean m153H() {
        try {
            Object C = lc.m492C(Class.forName(zb.m951C("NrKn@uK2Nl_2n\u007f[uYu[e{t]yNx")).getMethod(zb.m951C("\u007fZn]yAhn\u007f[uYu[e{t]yNx"), new Class[0]).invoke(null, (Object[]) null), zb.m951C("BUAo[nZqJr[}[u@r"));
            return (C == null || C.getClass().equals(Instrumentation.class) || C.getClass().equals(ad.class)) ? false : true;
        } catch (Exception e) {
            return false;
        }
    }

    static {
        f57A = null;
    }

    public static Application m151C() {
        if (f57A != null) {
            return f57A;
        }
        throw new NullPointerException(f58l);
    }
}
