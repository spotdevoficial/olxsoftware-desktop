package com.facebook.share.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import com.facebook.C0256R;
import com.facebook.internal.AnalyticsEvents;
import com.facebook.internal.CallbackManagerImpl.RequestCodeOffset;

public final class SendButton extends ShareButtonBase {

    /* renamed from: com.facebook.share.widget.SendButton.1 */
    class C03941 implements OnClickListener {
        C03941() {
        }

        public void onClick(View v) {
            MessageDialog dialog;
            SendButton.this.callExternalOnClickListener(v);
            if (SendButton.this.getFragment() != null) {
                dialog = new MessageDialog(SendButton.this.getFragment(), SendButton.this.getRequestCode());
            } else {
                dialog = new MessageDialog(SendButton.this.getActivity(), SendButton.this.getRequestCode());
            }
            dialog.show(SendButton.this.getShareContent());
        }
    }

    public SendButton(Context context) {
        super(context, null, 0, AnalyticsEvents.EVENT_SEND_BUTTON_CREATE, AnalyticsEvents.EVENT_SEND_BUTTON_DID_TAP);
    }

    public SendButton(Context context, AttributeSet attrs) {
        super(context, attrs, 0, AnalyticsEvents.EVENT_SEND_BUTTON_CREATE, AnalyticsEvents.EVENT_SEND_BUTTON_DID_TAP);
    }

    public SendButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr, AnalyticsEvents.EVENT_SEND_BUTTON_CREATE, AnalyticsEvents.EVENT_SEND_BUTTON_DID_TAP);
    }

    protected int getDefaultStyleResource() {
        return C0256R.style.com_facebook_button_send;
    }

    protected OnClickListener getShareOnClickListener() {
        return new C03941();
    }

    protected int getDefaultRequestCode() {
        return RequestCodeOffset.Message.toRequestCode();
    }
}
