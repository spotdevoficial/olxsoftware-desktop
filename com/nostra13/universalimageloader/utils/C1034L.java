package com.nostra13.universalimageloader.utils;

import android.util.Log;
import com.nostra13.universalimageloader.core.ImageLoader;

/* renamed from: com.nostra13.universalimageloader.utils.L */
public final class C1034L {
    private static volatile boolean writeDebugLogs;
    private static volatile boolean writeLogs;

    static {
        writeDebugLogs = false;
        writeLogs = true;
    }

    public static void writeDebugLogs(boolean writeDebugLogs) {
        writeDebugLogs = writeDebugLogs;
    }

    public static void m1717d(String message, Object... args) {
        if (writeDebugLogs) {
            C1034L.log(3, null, message, args);
        }
    }

    public static void m1720i(String message, Object... args) {
        C1034L.log(4, null, message, args);
    }

    public static void m1721w(String message, Object... args) {
        C1034L.log(5, null, message, args);
    }

    public static void m1719e(Throwable ex) {
        C1034L.log(6, ex, null, new Object[0]);
    }

    public static void m1718e(String message, Object... args) {
        C1034L.log(6, null, message, args);
    }

    private static void log(int priority, Throwable ex, String message, Object... args) {
        if (writeLogs) {
            String log;
            if (args.length > 0) {
                message = String.format(message, args);
            }
            if (ex == null) {
                log = message;
            } else {
                String logMessage;
                if (message == null) {
                    logMessage = ex.getMessage();
                } else {
                    logMessage = message;
                }
                String logBody = Log.getStackTraceString(ex);
                log = String.format("%1$s\n%2$s", new Object[]{logMessage, logBody});
            }
            Log.println(priority, ImageLoader.TAG, log);
        }
    }
}
