package com.urbanairship;

import android.support.v7.appcompat.C0086R;

/* renamed from: com.urbanairship.R */
public final class C1608R {

    /* renamed from: com.urbanairship.R.animator */
    public static final class animator {
        public static final int ua_iam_slide_in_bottom = 2131034119;
        public static final int ua_iam_slide_in_top = 2131034120;
        public static final int ua_iam_slide_out_bottom = 2131034121;
        public static final int ua_iam_slide_out_top = 2131034122;
    }

    /* renamed from: com.urbanairship.R.attr */
    public static final class attr {
        public static final int bannerActionButtonTextAppearance = 2130771974;
        public static final int bannerDismissButtonDrawable = 2130771972;
        public static final int bannerFontPath = 2130771968;
        public static final int bannerNoDismissButton = 2130771973;
        public static final int bannerPrimaryColor = 2130771970;
        public static final int bannerSecondaryColor = 2130771971;
        public static final int bannerTextAppearance = 2130771969;
        public static final int cameraBearing = 2130772099;
        public static final int cameraTargetLat = 2130772100;
        public static final int cameraTargetLng = 2130772101;
        public static final int cameraTilt = 2130772102;
        public static final int cameraZoom = 2130772103;
        public static final int cardBackgroundColor = 2130772039;
        public static final int cardCornerRadius = 2130772040;
        public static final int cardElevation = 2130772041;
        public static final int cardMaxElevation = 2130772042;
        public static final int cardPreventCornerOverlap = 2130772044;
        public static final int cardUseCompatPadding = 2130772043;
        public static final int circleCrop = 2130772097;
        public static final int contentPadding = 2130772045;
        public static final int contentPaddingBottom = 2130772049;
        public static final int contentPaddingLeft = 2130772046;
        public static final int contentPaddingRight = 2130772047;
        public static final int contentPaddingTop = 2130772048;
        public static final int imageAspectRatio = 2130772096;
        public static final int imageAspectRatioAdjust = 2130772095;
        public static final int inAppMessageBannerStyle = 2130771978;
        public static final int liteMode = 2130772104;
        public static final int mapType = 2130772098;
        public static final int mixed_content_mode = 2130772309;
        public static final int optCardBackgroundColor = 2130771975;
        public static final int optCardCornerRadius = 2130771976;
        public static final int optCardElevation = 2130771977;
        public static final int uiCompass = 2130772105;
        public static final int uiMapToolbar = 2130772113;
        public static final int uiRotateGestures = 2130772106;
        public static final int uiScrollGestures = 2130772107;
        public static final int uiTiltGestures = 2130772108;
        public static final int uiZoomControls = 2130772109;
        public static final int uiZoomGestures = 2130772110;
        public static final int useViewLifecycle = 2130772111;
        public static final int zOrderOnTop = 2130772112;
    }

    /* renamed from: com.urbanairship.R.color */
    public static final class color {
        public static final int cardview_dark_background = 2131492903;
        public static final int cardview_light_background = 2131492904;
        public static final int cardview_shadow_end_color = 2131492905;
        public static final int cardview_shadow_start_color = 2131492906;
        public static final int common_action_bar_splitter = 2131492934;
        public static final int common_signin_btn_dark_text_default = 2131492935;
        public static final int common_signin_btn_dark_text_disabled = 2131492936;
        public static final int common_signin_btn_dark_text_focused = 2131492937;
        public static final int common_signin_btn_dark_text_pressed = 2131492938;
        public static final int common_signin_btn_default_background = 2131492939;
        public static final int common_signin_btn_light_text_default = 2131492940;
        public static final int common_signin_btn_light_text_disabled = 2131492941;
        public static final int common_signin_btn_light_text_focused = 2131492942;
        public static final int common_signin_btn_light_text_pressed = 2131492943;
        public static final int common_signin_btn_text_dark = 2131493105;
        public static final int common_signin_btn_text_light = 2131493106;
        public static final int ua_iam_primary = 2131492864;
        public static final int ua_iam_secondary = 2131492865;
    }

    /* renamed from: com.urbanairship.R.drawable */
    public static final class drawable {
        public static final int common_full_open_on_phone = 2130837597;
        public static final int common_ic_googleplayservices = 2130837598;
        public static final int common_signin_btn_icon_dark = 2130837599;
        public static final int common_signin_btn_icon_disabled_dark = 2130837600;
        public static final int common_signin_btn_icon_disabled_focus_dark = 2130837601;
        public static final int common_signin_btn_icon_disabled_focus_light = 2130837602;
        public static final int common_signin_btn_icon_disabled_light = 2130837603;
        public static final int common_signin_btn_icon_focus_dark = 2130837604;
        public static final int common_signin_btn_icon_focus_light = 2130837605;
        public static final int common_signin_btn_icon_light = 2130837606;
        public static final int common_signin_btn_icon_normal_dark = 2130837607;
        public static final int common_signin_btn_icon_normal_light = 2130837608;
        public static final int common_signin_btn_icon_pressed_dark = 2130837609;
        public static final int common_signin_btn_icon_pressed_light = 2130837610;
        public static final int common_signin_btn_text_dark = 2130837611;
        public static final int common_signin_btn_text_disabled_dark = 2130837612;
        public static final int common_signin_btn_text_disabled_focus_dark = 2130837613;
        public static final int common_signin_btn_text_disabled_focus_light = 2130837614;
        public static final int common_signin_btn_text_disabled_light = 2130837615;
        public static final int common_signin_btn_text_focus_dark = 2130837616;
        public static final int common_signin_btn_text_focus_light = 2130837617;
        public static final int common_signin_btn_text_light = 2130837618;
        public static final int common_signin_btn_text_normal_dark = 2130837619;
        public static final int common_signin_btn_text_normal_light = 2130837620;
        public static final int common_signin_btn_text_pressed_dark = 2130837621;
        public static final int common_signin_btn_text_pressed_light = 2130837622;
        public static final int ic_close_white_18dp = 2130837661;
        public static final int ic_notification_button_accept = 2130837681;
        public static final int ic_notification_button_cart = 2130837682;
        public static final int ic_notification_button_decline = 2130837683;
        public static final int ic_notification_button_download = 2130837684;
        public static final int ic_notification_button_follow = 2130837685;
        public static final int ic_notification_button_happy = 2130837686;
        public static final int ic_notification_button_remind = 2130837687;
        public static final int ic_notification_button_sad = 2130837688;
        public static final int ic_notification_button_share = 2130837689;
        public static final int ic_notification_button_thumbs_down = 2130837690;
        public static final int ic_notification_button_thumbs_up = 2130837691;
        public static final int ic_notification_button_unfollow = 2130837692;
        public static final int powered_by_google_dark = 2130837741;
        public static final int powered_by_google_light = 2130837742;
        public static final int ua_iam_background = 2130837759;
    }

    /* renamed from: com.urbanairship.R.id */
    public static final class id {
        public static final int action_button = 2131558901;
        public static final int action_buttons = 2131558904;
        public static final int action_divider = 2131558885;
        public static final int adjust_height = 2131558445;
        public static final int adjust_width = 2131558446;
        public static final int alert = 2131558903;
        public static final int always_allow = 2131558467;
        public static final int close = 2131558902;
        public static final int compatibility_mode = 2131558468;
        public static final int hybrid = 2131558447;
        public static final int in_app_message = 2131558900;
        public static final int never_allow = 2131558469;
        public static final int none = 2131558417;
        public static final int normal = 2131558413;
        public static final int satellite = 2131558448;
        public static final int terrain = 2131558449;
    }

    /* renamed from: com.urbanairship.R.layout */
    public static final class layout {
        public static final int ua_fragment_iam = 2130903227;
        public static final int ua_fragment_iam_card = 2130903228;
        public static final int ua_iam_button = 2130903229;
        public static final int ua_iam_content = 2130903230;
    }

    /* renamed from: com.urbanairship.R.string */
    public static final class string {
        public static final int common_android_wear_notification_needs_update_text = 2131165213;
        public static final int common_android_wear_update_text = 2131165214;
        public static final int common_android_wear_update_title = 2131165215;
        public static final int common_google_play_services_enable_button = 2131165217;
        public static final int common_google_play_services_enable_text = 2131165218;
        public static final int common_google_play_services_enable_title = 2131165219;
        public static final int common_google_play_services_error_notification_requested_by_msg = 2131165220;
        public static final int common_google_play_services_install_button = 2131165221;
        public static final int common_google_play_services_install_text_phone = 2131165222;
        public static final int common_google_play_services_install_text_tablet = 2131165223;
        public static final int common_google_play_services_install_title = 2131165224;
        public static final int common_google_play_services_invalid_account_text = 2131165225;
        public static final int common_google_play_services_invalid_account_title = 2131165226;
        public static final int common_google_play_services_needs_enabling_title = 2131165227;
        public static final int common_google_play_services_network_error_text = 2131165228;
        public static final int common_google_play_services_network_error_title = 2131165229;
        public static final int common_google_play_services_notification_needs_update_title = 2131165230;
        public static final int common_google_play_services_notification_ticker = 2131165231;
        public static final int common_google_play_services_sign_in_failed_text = 2131165232;
        public static final int common_google_play_services_sign_in_failed_title = 2131165233;
        public static final int common_google_play_services_unknown_issue = 2131165234;
        public static final int common_google_play_services_unsupported_text = 2131165235;
        public static final int common_google_play_services_unsupported_title = 2131165236;
        public static final int common_google_play_services_update_button = 2131165237;
        public static final int common_google_play_services_update_text = 2131165238;
        public static final int common_google_play_services_update_title = 2131165239;
        public static final int common_open_on_phone = 2131165242;
        public static final int common_signin_button_text = 2131165243;
        public static final int common_signin_button_text_long = 2131165244;
        public static final int ua_notification_button_accept = 2131165246;
        public static final int ua_notification_button_buy_now = 2131165247;
        public static final int ua_notification_button_decline = 2131165248;
        public static final int ua_notification_button_dislike = 2131165249;
        public static final int ua_notification_button_download = 2131165250;
        public static final int ua_notification_button_follow = 2131165251;
        public static final int ua_notification_button_less_like = 2131165252;
        public static final int ua_notification_button_like = 2131165253;
        public static final int ua_notification_button_more_like = 2131165254;
        public static final int ua_notification_button_no = 2131165255;
        public static final int ua_notification_button_opt_in = 2131165256;
        public static final int ua_notification_button_opt_out = 2131165257;
        public static final int ua_notification_button_remind = 2131165258;
        public static final int ua_notification_button_share = 2131165259;
        public static final int ua_notification_button_shop_now = 2131165260;
        public static final int ua_notification_button_unfollow = 2131165261;
        public static final int ua_notification_button_yes = 2131165262;
        public static final int ua_share_dialog_title = 2131165263;
    }

    /* renamed from: com.urbanairship.R.style */
    public static final class style {
        public static final int Base_Widget_UrbanAirship_InAppMessage_Banner = 2131296285;
        public static final int Base_Widget_UrbanAirship_InAppMessage_Banner_ActionButton = 2131296286;
        public static final int f1294x11ef105c = 2131296287;
        public static final int Base_Widget_UrbanAirship_InAppMessage_Banner_DismissButton = 2131296288;
        public static final int Base_Widget_UrbanAirship_InAppMessage_Banner_Divider = 2131296289;
        public static final int Base_Widget_UrbanAirship_InAppMessage_Banner_Text = 2131296290;
        public static final int CardView = 2131296460;
        public static final int CardView_Dark = 2131296461;
        public static final int CardView_Light = 2131296462;
        public static final int InAppMessage_Banner = 2131296292;
        public static final int InAppMessage_Banner_TextAppearance = 2131296293;
        public static final int Widget_UrbanAirship_InAppMessage_Banner = 2131296302;
        public static final int Widget_UrbanAirship_InAppMessage_Banner_ActionButton = 2131296303;
        public static final int Widget_UrbanAirship_InAppMessage_Banner_ActionButtonContainer = 2131296304;
        public static final int Widget_UrbanAirship_InAppMessage_Banner_Card = 2131296305;
        public static final int Widget_UrbanAirship_InAppMessage_Banner_DismissButton = 2131296306;
        public static final int Widget_UrbanAirship_InAppMessage_Banner_Divider = 2131296307;
        public static final int Widget_UrbanAirship_InAppMessage_Banner_Text = 2131296308;
    }

    /* renamed from: com.urbanairship.R.styleable */
    public static final class styleable {
        public static final int[] BannerView;
        public static final int BannerView_bannerActionButtonTextAppearance = 6;
        public static final int BannerView_bannerDismissButtonDrawable = 4;
        public static final int BannerView_bannerFontPath = 0;
        public static final int BannerView_bannerNoDismissButton = 5;
        public static final int BannerView_bannerPrimaryColor = 2;
        public static final int BannerView_bannerSecondaryColor = 3;
        public static final int BannerView_bannerTextAppearance = 1;
        public static final int[] CardView;
        public static final int CardView_cardBackgroundColor = 3;
        public static final int CardView_cardCornerRadius = 4;
        public static final int CardView_cardElevation = 5;
        public static final int CardView_cardMaxElevation = 6;
        public static final int CardView_cardPreventCornerOverlap = 8;
        public static final int CardView_cardUseCompatPadding = 7;
        public static final int CardView_contentPadding = 9;
        public static final int CardView_contentPaddingBottom = 13;
        public static final int CardView_contentPaddingLeft = 10;
        public static final int CardView_contentPaddingRight = 11;
        public static final int CardView_contentPaddingTop = 12;
        public static final int CardView_optCardBackgroundColor = 0;
        public static final int CardView_optCardCornerRadius = 1;
        public static final int CardView_optCardElevation = 2;
        public static final int[] LoadingImageView;
        public static final int LoadingImageView_circleCrop = 2;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 0;
        public static final int[] MapAttrs;
        public static final int MapAttrs_cameraBearing = 1;
        public static final int MapAttrs_cameraTargetLat = 2;
        public static final int MapAttrs_cameraTargetLng = 3;
        public static final int MapAttrs_cameraTilt = 4;
        public static final int MapAttrs_cameraZoom = 5;
        public static final int MapAttrs_liteMode = 6;
        public static final int MapAttrs_mapType = 0;
        public static final int MapAttrs_uiCompass = 7;
        public static final int MapAttrs_uiMapToolbar = 15;
        public static final int MapAttrs_uiRotateGestures = 8;
        public static final int MapAttrs_uiScrollGestures = 9;
        public static final int MapAttrs_uiTiltGestures = 10;
        public static final int MapAttrs_uiZoomControls = 11;
        public static final int MapAttrs_uiZoomGestures = 12;
        public static final int MapAttrs_useViewLifecycle = 13;
        public static final int MapAttrs_zOrderOnTop = 14;
        public static final int[] Theme;
        public static final int Theme_inAppMessageBannerStyle = 2;
        public static final int[] UAWebView;
        public static final int UAWebView_mixed_content_mode = 0;

        static {
            BannerView = new int[]{C1608R.attr.bannerFontPath, C1608R.attr.bannerTextAppearance, C1608R.attr.bannerPrimaryColor, C1608R.attr.bannerSecondaryColor, C1608R.attr.bannerDismissButtonDrawable, C1608R.attr.bannerNoDismissButton, C1608R.attr.bannerActionButtonTextAppearance};
            CardView = new int[]{C1608R.attr.optCardBackgroundColor, C1608R.attr.optCardCornerRadius, C1608R.attr.optCardElevation, C1608R.attr.cardBackgroundColor, C1608R.attr.cardCornerRadius, C1608R.attr.cardElevation, C1608R.attr.cardMaxElevation, C1608R.attr.cardUseCompatPadding, C1608R.attr.cardPreventCornerOverlap, C1608R.attr.contentPadding, C1608R.attr.contentPaddingLeft, C1608R.attr.contentPaddingRight, C1608R.attr.contentPaddingTop, C1608R.attr.contentPaddingBottom};
            LoadingImageView = new int[]{C1608R.attr.imageAspectRatioAdjust, C1608R.attr.imageAspectRatio, C1608R.attr.circleCrop};
            MapAttrs = new int[]{C1608R.attr.mapType, C1608R.attr.cameraBearing, C1608R.attr.cameraTargetLat, C1608R.attr.cameraTargetLng, C1608R.attr.cameraTilt, C1608R.attr.cameraZoom, C1608R.attr.liteMode, C1608R.attr.uiCompass, C1608R.attr.uiRotateGestures, C1608R.attr.uiScrollGestures, C1608R.attr.uiTiltGestures, C1608R.attr.uiZoomControls, C1608R.attr.uiZoomGestures, C1608R.attr.useViewLifecycle, C1608R.attr.zOrderOnTop, C1608R.attr.uiMapToolbar};
            Theme = new int[]{16842839, 16842926, C1608R.attr.inAppMessageBannerStyle, C0086R.attr.windowActionBar, C0086R.attr.windowNoTitle, C0086R.attr.windowActionBarOverlay, C0086R.attr.windowActionModeOverlay, C0086R.attr.windowFixedWidthMajor, C0086R.attr.windowFixedHeightMinor, C0086R.attr.windowFixedWidthMinor, C0086R.attr.windowFixedHeightMajor, C0086R.attr.windowMinWidthMajor, C0086R.attr.windowMinWidthMinor, C0086R.attr.actionBarTabStyle, C0086R.attr.actionBarTabBarStyle, C0086R.attr.actionBarTabTextStyle, C0086R.attr.actionOverflowButtonStyle, C0086R.attr.actionOverflowMenuStyle, C0086R.attr.actionBarPopupTheme, C0086R.attr.actionBarStyle, C0086R.attr.actionBarSplitStyle, C0086R.attr.actionBarTheme, C0086R.attr.actionBarWidgetTheme, C0086R.attr.actionBarSize, C0086R.attr.actionBarDivider, C0086R.attr.actionBarItemBackground, C0086R.attr.actionMenuTextAppearance, C0086R.attr.actionMenuTextColor, C0086R.attr.actionModeStyle, C0086R.attr.actionModeCloseButtonStyle, C0086R.attr.actionModeBackground, C0086R.attr.actionModeSplitBackground, C0086R.attr.actionModeCloseDrawable, C0086R.attr.actionModeCutDrawable, C0086R.attr.actionModeCopyDrawable, C0086R.attr.actionModePasteDrawable, C0086R.attr.actionModeSelectAllDrawable, C0086R.attr.actionModeShareDrawable, C0086R.attr.actionModeFindDrawable, C0086R.attr.actionModeWebSearchDrawable, C0086R.attr.actionModePopupWindowStyle, C0086R.attr.textAppearanceLargePopupMenu, C0086R.attr.textAppearanceSmallPopupMenu, C0086R.attr.dialogTheme, C0086R.attr.dialogPreferredPadding, C0086R.attr.listDividerAlertDialog, C0086R.attr.actionDropDownStyle, C0086R.attr.dropdownListPreferredItemHeight, C0086R.attr.spinnerDropDownItemStyle, C0086R.attr.homeAsUpIndicator, C0086R.attr.actionButtonStyle, C0086R.attr.buttonBarStyle, C0086R.attr.buttonBarButtonStyle, C0086R.attr.selectableItemBackground, C0086R.attr.selectableItemBackgroundBorderless, C0086R.attr.borderlessButtonStyle, C0086R.attr.dividerVertical, C0086R.attr.dividerHorizontal, C0086R.attr.activityChooserViewStyle, C0086R.attr.toolbarStyle, C0086R.attr.toolbarNavigationButtonStyle, C0086R.attr.popupMenuStyle, C0086R.attr.popupWindowStyle, C0086R.attr.editTextColor, C0086R.attr.editTextBackground, C0086R.attr.textAppearanceSearchResultTitle, C0086R.attr.textAppearanceSearchResultSubtitle, C0086R.attr.textColorSearchUrl, C0086R.attr.searchViewStyle, C0086R.attr.listPreferredItemHeight, C0086R.attr.listPreferredItemHeightSmall, C0086R.attr.listPreferredItemHeightLarge, C0086R.attr.listPreferredItemPaddingLeft, C0086R.attr.listPreferredItemPaddingRight, C0086R.attr.dropDownListViewStyle, C0086R.attr.listPopupWindowStyle, C0086R.attr.textAppearanceListItem, C0086R.attr.textAppearanceListItemSmall, C0086R.attr.panelBackground, C0086R.attr.panelMenuListWidth, C0086R.attr.panelMenuListTheme, C0086R.attr.listChoiceBackgroundIndicator, C0086R.attr.colorPrimary, C0086R.attr.colorPrimaryDark, C0086R.attr.colorAccent, C0086R.attr.colorControlNormal, C0086R.attr.colorControlActivated, C0086R.attr.colorControlHighlight, C0086R.attr.colorButtonNormal, C0086R.attr.colorSwitchThumbNormal, C0086R.attr.alertDialogStyle, C0086R.attr.alertDialogButtonGroupStyle, C0086R.attr.alertDialogCenterButtons, C0086R.attr.alertDialogTheme, C0086R.attr.textColorAlertDialogListItem, C0086R.attr.buttonBarPositiveButtonStyle, C0086R.attr.buttonBarNegativeButtonStyle, C0086R.attr.buttonBarNeutralButtonStyle, C0086R.attr.autoCompleteTextViewStyle, C0086R.attr.buttonStyle, C0086R.attr.buttonStyleSmall, C0086R.attr.checkboxStyle, C0086R.attr.checkedTextViewStyle, C0086R.attr.editTextStyle, C0086R.attr.radioButtonStyle, C0086R.attr.ratingBarStyle, C0086R.attr.spinnerStyle, C0086R.attr.switchStyle};
            int[] iArr = new int[MapAttrs_cameraBearing];
            iArr[MapAttrs_mapType] = C1608R.attr.mixed_content_mode;
            UAWebView = iArr;
        }
    }
}
