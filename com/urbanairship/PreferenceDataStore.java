package com.urbanairship;

import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import com.urbanairship.util.UAStringUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public final class PreferenceDataStore {
    Executor executor;
    private final List<PreferenceChangeListener> listeners;
    private final Map<String, Preference> preferences;
    private UrbanAirshipResolver resolver;

    private class Preference {
        private final String key;
        private String value;

        /* renamed from: com.urbanairship.PreferenceDataStore.Preference.1 */
        class C16051 implements Runnable {
            final /* synthetic */ String val$value;

            C16051(String str) {
                this.val$value = str;
            }

            public void run() {
                Preference.this.writeValue(this.val$value);
            }
        }

        /* renamed from: com.urbanairship.PreferenceDataStore.Preference.2 */
        class C16072 extends ContentObserver {

            /* renamed from: com.urbanairship.PreferenceDataStore.Preference.2.1 */
            class C16061 implements Runnable {
                C16061() {
                }

                public void run() {
                    Preference.this.syncValue();
                }
            }

            C16072(Handler x0) {
                super(x0);
            }

            public void onChange(boolean selfChange) {
                super.onChange(selfChange);
                Logger.verbose("PreferenceDataStore - Preference updated:" + Preference.this.key);
                PreferenceDataStore.this.executor.execute(new C16061());
            }
        }

        Preference(String key, String value) {
            this.key = key;
            this.value = value;
            registerObserver();
        }

        String get() {
            String str;
            synchronized (this) {
                str = this.value;
            }
            return str;
        }

        void put(String value) {
            setValue(value);
            PreferenceDataStore.this.executor.execute(new C16051(value));
        }

        boolean putSync(String value) {
            boolean z;
            synchronized (this) {
                if (writeValue(value)) {
                    setValue(value);
                    z = true;
                } else {
                    z = false;
                }
            }
            return z;
        }

        private void setValue(String value) {
            synchronized (this) {
                if (UAStringUtil.equals(value, this.value)) {
                    return;
                }
                this.value = value;
                PreferenceDataStore.this.onPreferenceChanged(this.key);
            }
        }

        private boolean writeValue(String value) {
            boolean z = true;
            synchronized (this) {
                if (value == null) {
                    Logger.verbose("PreferenceDataStore - Removing preference: " + this.key);
                    if (PreferenceDataStore.this.resolver.delete(UrbanAirshipProvider.getPreferencesContentUri(), "_id = ?", new String[]{this.key}) < 0) {
                        z = false;
                    }
                } else {
                    Logger.verbose("PreferenceDataStore - Saving preference: " + this.key + " value: " + value);
                    ContentValues values = new ContentValues();
                    values.put("_id", this.key);
                    values.put("value", value);
                    if (PreferenceDataStore.this.resolver.insert(UrbanAirshipProvider.getPreferencesContentUri(), values) == null) {
                        z = false;
                    }
                }
            }
            return z;
        }

        void syncValue() {
            Cursor cursor = null;
            try {
                synchronized (this) {
                    cursor = PreferenceDataStore.this.resolver.query(UrbanAirshipProvider.getPreferencesContentUri(), new String[]{"value"}, "_id = ?", new String[]{this.key}, null);
                }
                if (cursor != null) {
                    setValue(cursor.moveToFirst() ? cursor.getString(0) : null);
                } else {
                    Logger.debug("PreferenceDataStore - Unable to get preference " + this.key + " from" + " database. Falling back to cached value.");
                }
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        private void registerObserver() {
            ContentObserver observer = new C16072(null);
            PreferenceDataStore.this.resolver.registerContentObserver(Uri.withAppendedPath(UrbanAirshipProvider.getPreferencesContentUri(), this.key), true, observer);
        }
    }

    public interface PreferenceChangeListener {
        void onPreferenceChange(String str);
    }

    PreferenceDataStore(Context context) {
        this(new UrbanAirshipResolver(context));
    }

    PreferenceDataStore(UrbanAirshipResolver resolver) {
        this.executor = Executors.newSingleThreadExecutor();
        this.preferences = new HashMap();
        this.listeners = new ArrayList();
        this.resolver = resolver;
    }

    public void addListener(PreferenceChangeListener listener) {
        if (listener != null) {
            synchronized (this.listeners) {
                this.listeners.add(listener);
            }
        }
    }

    void loadAll() {
        Cursor cursor = this.resolver.query(UrbanAirshipProvider.getPreferencesContentUri(), null, null, null, null);
        if (cursor != null) {
            int keyIndex = cursor.getColumnIndex("_id");
            int valueIndex = cursor.getColumnIndex("value");
            while (cursor.moveToNext()) {
                String key = cursor.getString(keyIndex);
                this.preferences.put(key, new Preference(key, cursor.getString(valueIndex)));
            }
            cursor.close();
        }
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        String value = getPreference(key).get();
        return value == null ? defaultValue : Boolean.valueOf(value).booleanValue();
    }

    public String getString(String key, String defaultValue) {
        String value = getPreference(key).get();
        return value == null ? defaultValue : value;
    }

    public long getLong(String key, long defaultValue) {
        String value = getPreference(key).get();
        if (value != null) {
            try {
                defaultValue = Long.parseLong(value);
            } catch (NumberFormatException e) {
            }
        }
        return defaultValue;
    }

    public int getInt(String key, int defaultValue) {
        String value = getPreference(key).get();
        if (value != null) {
            try {
                defaultValue = Integer.parseInt(value);
            } catch (NumberFormatException e) {
            }
        }
        return defaultValue;
    }

    public void remove(String key) {
        put(key, null);
    }

    public void put(String key, Object value) {
        getPreference(key).put(value == null ? null : String.valueOf(value));
    }

    public boolean putSync(String key, Object value) {
        return getPreference(key).putSync(value == null ? null : String.valueOf(value));
    }

    private void onPreferenceChanged(String key) {
        synchronized (this.listeners) {
            for (PreferenceChangeListener listener : this.listeners) {
                listener.onPreferenceChange(key);
            }
        }
    }

    private Preference getPreference(String key) {
        Preference preference;
        synchronized (this.preferences) {
            if (this.preferences.containsKey(key)) {
                preference = (Preference) this.preferences.get(key);
            } else {
                preference = new Preference(key, null);
                this.preferences.put(key, preference);
            }
        }
        return preference;
    }
}
