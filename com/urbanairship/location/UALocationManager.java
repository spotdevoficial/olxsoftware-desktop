package com.urbanairship.location;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.support.v4.content.LocalBroadcastManager;
import android.util.SparseArray;
import com.urbanairship.BaseManager;
import com.urbanairship.C1608R;
import com.urbanairship.Logger;
import com.urbanairship.PreferenceDataStore;
import com.urbanairship.PreferenceDataStore.PreferenceChangeListener;
import com.urbanairship.UAirship;
import com.urbanairship.location.LocationRequestOptions.Builder;
import java.util.ArrayList;
import java.util.List;

public class UALocationManager extends BaseManager {
    private final Context context;
    private boolean isBound;
    private boolean isSubscribed;
    private final List<LocationListener> locationListeners;
    private final Messenger messenger;
    private int nextSingleLocationRequestId;
    LocationPreferences preferences;
    private ServiceConnection serviceConnection;
    private Messenger serviceMessenger;
    private final SparseArray<SingleLocationRequest> singleLocationRequests;

    /* renamed from: com.urbanairship.location.UALocationManager.1 */
    class C16481 implements ServiceConnection {
        C16481() {
        }

        public void onServiceConnected(ComponentName className, IBinder service) {
            Logger.verbose("Location service connected.");
            UALocationManager.this.onServiceConnected(service);
        }

        public void onServiceDisconnected(ComponentName className) {
            Logger.verbose("Location service disconnected.");
            UALocationManager.this.onServiceDisconnected();
        }
    }

    /* renamed from: com.urbanairship.location.UALocationManager.2 */
    class C16492 implements PreferenceChangeListener {
        C16492() {
        }

        public void onPreferenceChange(String key) {
            UALocationManager.this.updateServiceConnection();
        }
    }

    /* renamed from: com.urbanairship.location.UALocationManager.3 */
    class C16513 implements Runnable {

        /* renamed from: com.urbanairship.location.UALocationManager.3.1 */
        class C16501 extends BroadcastReceiver {
            C16501() {
            }

            public void onReceive(Context context, Intent intent) {
                UALocationManager.this.updateServiceConnection();
            }
        }

        C16513() {
        }

        public void run() {
            IntentFilter filter = new IntentFilter();
            filter.addAction("com.urbanairship.analytics.APP_FOREGROUND");
            filter.addAction("com.urbanairship.analytics.APP_BACKGROUND");
            LocalBroadcastManager.getInstance(UALocationManager.this.context).registerReceiver(new C16501(), filter);
            UALocationManager.this.updateServiceConnection();
        }
    }

    /* renamed from: com.urbanairship.location.UALocationManager.4 */
    class C16524 extends ResultReceiver {
        C16524(Handler x0) {
            super(x0);
        }

        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if (resultCode == LocationService.RESULT_LOCATION_UPDATES_STARTED) {
                onUpdatesStarted();
            } else {
                onUpdatesStopped();
            }
        }

        private void onUpdatesStarted() {
            synchronized (UALocationManager.this.locationListeners) {
                if (!UALocationManager.this.locationListeners.isEmpty()) {
                    if (UALocationManager.this.isBound) {
                        UALocationManager.this.subscribeUpdates();
                    } else {
                        UALocationManager.this.bindService();
                    }
                }
            }
        }

        private void onUpdatesStopped() {
            UALocationManager.this.unsubscribeUpdates();
            synchronized (UALocationManager.this.singleLocationRequests) {
                if (UALocationManager.this.singleLocationRequests.size() == 0) {
                    UALocationManager.this.unbindService();
                }
            }
        }
    }

    private static class IncomingHandler extends Handler {
        IncomingHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            UALocationManager manager = UAirship.shared().getLocationManager();
            Location location;
            switch (msg.what) {
                case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                    location = msg.obj;
                    if (location != null) {
                        synchronized (manager.locationListeners) {
                            for (LocationListener listener : manager.locationListeners) {
                                listener.onLocationChanged(location);
                            }
                            break;
                        }
                    }
                case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                    location = (Location) msg.obj;
                    int requestId = msg.arg1;
                    synchronized (manager.singleLocationRequests) {
                        PendingLocationResult request = (PendingLocationResult) manager.singleLocationRequests.get(requestId);
                        if (request != null) {
                            request.setResult(location);
                            manager.singleLocationRequests.remove(requestId);
                            manager.updateServiceConnection();
                        }
                        break;
                    }
                default:
                    super.handleMessage(msg);
            }
        }
    }

    private class SingleLocationRequest extends PendingLocationResult {
        private LocationRequestOptions options;
        private int requestId;
        final /* synthetic */ UALocationManager this$0;

        protected void onCancel() {
            if (!isDone()) {
                this.this$0.sendMessage(6, this.requestId, null);
            }
            synchronized (this.this$0.singleLocationRequests) {
                this.this$0.singleLocationRequests.remove(this.requestId);
            }
        }

        synchronized void sendLocationRequest() {
            if (!isDone()) {
                this.this$0.sendMessage(5, this.requestId, LocationService.createRequestOptionsBundle(this.options));
            }
        }
    }

    public UALocationManager(Context context, PreferenceDataStore preferenceDataStore) {
        this.nextSingleLocationRequestId = 1;
        this.singleLocationRequests = new SparseArray();
        this.locationListeners = new ArrayList();
        this.serviceConnection = new C16481();
        this.context = context.getApplicationContext();
        this.preferences = new LocationPreferences(preferenceDataStore);
        this.messenger = new Messenger(new IncomingHandler(Looper.getMainLooper()));
        this.preferences.setListener(new C16492());
    }

    protected void init() {
        new Handler(Looper.getMainLooper()).postDelayed(new C16513(), 1000);
    }

    public boolean isLocationUpdatesEnabled() {
        return this.preferences.isLocationUpdatesEnabled();
    }

    public boolean isBackgroundLocationAllowed() {
        return this.preferences.isBackgroundLocationAllowed();
    }

    public LocationRequestOptions getLocationRequestOptions() {
        LocationRequestOptions options = this.preferences.getLocationRequestOptions();
        if (options == null) {
            return new Builder().create();
        }
        return options;
    }

    private void updateServiceConnection() {
        if (this.context.startService(new Intent(this.context, LocationService.class).setAction("com.urbanairship.location.ACTION_CHECK_LOCATION_UPDATES").putExtra("com.urbanairship.location.EXTRA_RESULT_RECEIVER", new C16524(new Handler(Looper.getMainLooper())))) == null) {
            Logger.error("Unable to start location service. Check that the location service is added to the manifest.");
        }
    }

    private synchronized void bindService() {
        if (!this.isBound) {
            Logger.verbose("UALocationManager - Binding to location service.");
            Context context = UAirship.getApplicationContext();
            if (context.bindService(new Intent(context, LocationService.class), this.serviceConnection, 1)) {
                this.isBound = true;
            } else {
                Logger.error("Unable to bind to location service. Check that the location service is added to the manifest.");
            }
        }
    }

    private synchronized void subscribeUpdates() {
        if (!this.isSubscribed && sendMessage(1, 0, null)) {
            Logger.info("Subscribing to continuous location updates.");
            this.isSubscribed = true;
        }
    }

    private synchronized void unsubscribeUpdates() {
        if (this.isSubscribed) {
            Logger.info("Unsubscribing from continuous location updates.");
            sendMessage(2, 0, null);
            this.isSubscribed = false;
            updateServiceConnection();
        }
    }

    private synchronized void unbindService() {
        if (this.isBound) {
            Logger.verbose("UALocationManager - Unbinding to location service.");
            UAirship.getApplicationContext().unbindService(this.serviceConnection);
            this.isBound = false;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void onServiceConnected(android.os.IBinder r4) {
        /*
        r3 = this;
        monitor-enter(r3);
        r1 = new android.os.Messenger;	 Catch:{ all -> 0x002b }
        r1.<init>(r4);	 Catch:{ all -> 0x002b }
        r3.serviceMessenger = r1;	 Catch:{ all -> 0x002b }
        r2 = r3.singleLocationRequests;	 Catch:{ all -> 0x002b }
        monitor-enter(r2);	 Catch:{ all -> 0x002b }
        r0 = 0;
    L_0x000c:
        r1 = r3.singleLocationRequests;	 Catch:{ all -> 0x0028 }
        r1 = r1.size();	 Catch:{ all -> 0x0028 }
        if (r0 >= r1) goto L_0x0022;
    L_0x0014:
        r1 = r3.singleLocationRequests;	 Catch:{ all -> 0x0028 }
        r1 = r1.valueAt(r0);	 Catch:{ all -> 0x0028 }
        r1 = (com.urbanairship.location.UALocationManager.SingleLocationRequest) r1;	 Catch:{ all -> 0x0028 }
        r1.sendLocationRequest();	 Catch:{ all -> 0x0028 }
        r0 = r0 + 1;
        goto L_0x000c;
    L_0x0022:
        monitor-exit(r2);	 Catch:{ all -> 0x0028 }
        r3.updateServiceConnection();	 Catch:{ all -> 0x002b }
        monitor-exit(r3);
        return;
    L_0x0028:
        r1 = move-exception;
        monitor-exit(r2);	 Catch:{ all -> 0x0028 }
        throw r1;	 Catch:{ all -> 0x002b }
    L_0x002b:
        r1 = move-exception;
        monitor-exit(r3);
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.urbanairship.location.UALocationManager.onServiceConnected(android.os.IBinder):void");
    }

    private synchronized void onServiceDisconnected() {
        this.serviceMessenger = null;
        this.isSubscribed = false;
    }

    private boolean sendMessage(int what, int arg1, Bundle data) {
        if (this.serviceMessenger == null) {
            return false;
        }
        Message message = Message.obtain(null, what, arg1, 0);
        if (data != null) {
            message.setData(data);
        }
        message.replyTo = this.messenger;
        try {
            this.serviceMessenger.send(message);
            return true;
        } catch (RemoteException e) {
            Logger.debug("UALocationManager - Remote exception when sending message to location service");
            return false;
        }
    }
}
