package com.urbanairship.location;

import android.app.PendingIntent;
import android.location.Location;
import com.urbanairship.PendingResult;

interface LocationAdapter {
    void cancelLocationUpdates(PendingIntent pendingIntent);

    boolean connect();

    void disconnect();

    void requestLocationUpdates(LocationRequestOptions locationRequestOptions, PendingIntent pendingIntent);

    PendingResult<Location> requestSingleLocation(LocationRequestOptions locationRequestOptions);
}
