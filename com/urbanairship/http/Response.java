package com.urbanairship.http;

import java.util.List;
import java.util.Map;

public class Response {
    private long lastModified;
    private String responseBody;
    private Map<String, List<String>> responseHeaders;
    private String responseMessage;
    private int status;

    public static class Builder {
        private long lastModified;
        private String responseBody;
        private Map<String, List<String>> responseHeaders;
        private String responseMessage;
        private int status;

        public Builder(int status) {
            this.lastModified = 0;
            this.status = status;
        }

        public Builder setResponseMessage(String responseMessage) {
            this.responseMessage = responseMessage;
            return this;
        }

        public Builder setResponseBody(String responseBody) {
            this.responseBody = responseBody;
            return this;
        }

        public Builder setResponseHeaders(Map<String, List<String>> responseHeaders) {
            this.responseHeaders = responseHeaders;
            return this;
        }

        public Builder setLastModified(long lastModified) {
            this.lastModified = lastModified;
            return this;
        }

        public Response create() {
            Response response = new Response();
            response.status = this.status;
            response.responseBody = this.responseBody;
            response.responseHeaders = this.responseHeaders;
            response.responseMessage = this.responseMessage;
            response.lastModified = this.lastModified;
            return response;
        }
    }

    private Response() {
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Response: ");
        builder.append("ResponseBody: ");
        if (this.responseBody != null) {
            builder.append(this.responseBody);
        }
        builder.append(" ResponseHeaders: ");
        if (this.responseHeaders != null) {
            builder.append(this.responseHeaders.toString());
        }
        builder.append(" ResponseMessage: ");
        if (this.responseMessage != null) {
            builder.append(this.responseMessage);
        }
        builder.append(" Status: ").append(Integer.toString(this.status));
        return builder.toString();
    }

    public int getStatus() {
        return this.status;
    }

    public String getResponseBody() {
        return this.responseBody;
    }

    public long getLastModifiedTime() {
        return this.lastModified;
    }

    public Map<String, List<String>> getResponseHeaders() {
        return this.responseHeaders;
    }
}
