package com.urbanairship;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import com.urbanairship.util.DataManager;

class PreferencesDataManager extends DataManager {
    public PreferencesDataManager(Context context) {
        super(context, "ua_preferences.db", 1);
    }

    protected void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS preferences (_id TEXT PRIMARY KEY, value TEXT);");
    }

    protected void bindValuesToSqlLiteStatment(SQLiteStatement statement, ContentValues values) {
        bind(statement, 1, values.getAsString("_id"));
        bind(statement, 2, values.getAsString("value"));
    }

    protected SQLiteStatement getInsertStatement(String table, SQLiteDatabase db) {
        return db.compileStatement(buildInsertStatement(table, "_id", "value"));
    }

    protected void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS preferences");
        onCreate(db);
    }
}
