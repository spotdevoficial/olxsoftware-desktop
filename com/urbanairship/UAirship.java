package com.urbanairship;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Looper;
import com.urbanairship.actions.ActionRegistry;
import com.urbanairship.amazon.ADMUtils;
import com.urbanairship.analytics.Analytics;
import com.urbanairship.google.GCMUtils;
import com.urbanairship.google.PlayServicesUtils;
import com.urbanairship.js.Whitelist;
import com.urbanairship.location.UALocationManager;
import com.urbanairship.push.PushManager;
import com.urbanairship.push.iam.InAppMessageManager;
import com.urbanairship.richpush.RichPushManager;
import com.urbanairship.util.ManifestUtils;
import java.util.ArrayList;
import java.util.List;

public class UAirship {
    private static final Object airshipLock;
    static Application application;
    static volatile boolean isFlying;
    static volatile boolean isTakingOff;
    private static List<CancelableOperation> pendingAirshipRequests;
    static UAirship sharedAirship;
    ActionRegistry actionRegistry;
    AirshipConfigOptions airshipConfigOptions;
    Analytics analytics;
    ApplicationMetrics applicationMetrics;
    InAppMessageManager inAppMessageManager;
    UALocationManager locationManager;
    PreferenceDataStore preferenceDataStore;
    PushManager pushManager;
    RichPushManager richPushManager;
    Whitelist whitelist;

    public interface OnReadyCallback {
        void onAirshipReady(UAirship uAirship);
    }

    /* renamed from: com.urbanairship.UAirship.1 */
    static class C16091 extends CancelableOperation {
        final /* synthetic */ OnReadyCallback val$callback;

        C16091(Looper x0, OnReadyCallback onReadyCallback) {
            this.val$callback = onReadyCallback;
            super(x0);
        }

        public void onRun() {
            UAirship airship = UAirship.shared();
            if (this.val$callback != null && airship != null) {
                this.val$callback.onAirshipReady(airship);
            }
        }
    }

    /* renamed from: com.urbanairship.UAirship.2 */
    static class C16102 implements Runnable {
        final /* synthetic */ Application val$application;
        final /* synthetic */ AirshipConfigOptions val$options;
        final /* synthetic */ OnReadyCallback val$readyCallback;

        C16102(Application application, AirshipConfigOptions airshipConfigOptions, OnReadyCallback onReadyCallback) {
            this.val$application = application;
            this.val$options = airshipConfigOptions;
            this.val$readyCallback = onReadyCallback;
        }

        public void run() {
            UAirship.executeTakeOff(this.val$application, this.val$options, this.val$readyCallback);
        }
    }

    static {
        airshipLock = new Object();
        isFlying = false;
        isTakingOff = false;
    }

    UAirship(Context context, AirshipConfigOptions airshipConfigOptions, PreferenceDataStore preferenceDataStore) {
        this.airshipConfigOptions = airshipConfigOptions;
        this.preferenceDataStore = preferenceDataStore;
        this.analytics = new Analytics(context, preferenceDataStore, airshipConfigOptions);
        this.applicationMetrics = new ApplicationMetrics(context, preferenceDataStore);
        this.richPushManager = new RichPushManager(context, preferenceDataStore);
        this.locationManager = new UALocationManager(context, preferenceDataStore);
        this.inAppMessageManager = new InAppMessageManager(preferenceDataStore);
        this.pushManager = new PushManager(context, preferenceDataStore);
        this.whitelist = Whitelist.createDefaultWhitelist(airshipConfigOptions);
        this.actionRegistry = new ActionRegistry();
    }

    public static UAirship shared() {
        UAirship uAirship;
        synchronized (airshipLock) {
            if (isFlying) {
                uAirship = sharedAirship;
            } else if (isTakingOff) {
                try {
                    airshipLock.wait();
                } catch (InterruptedException e) {
                    Logger.error("Failed to wait for UAirship instance.", e);
                }
                if (isFlying) {
                    uAirship = sharedAirship;
                } else {
                    uAirship = null;
                }
            } else {
                throw new IllegalStateException("Take off must be called before shared()");
            }
        }
        return uAirship;
    }

    public static Cancelable shared(OnReadyCallback callback) {
        return shared(callback, null);
    }

    public static Cancelable shared(OnReadyCallback callback, Looper looper) {
        CancelableOperation cancelableOperation = new C16091(looper, callback);
        synchronized (airshipLock) {
            if (isFlying) {
                cancelableOperation.run();
            } else {
                if (pendingAirshipRequests == null) {
                    pendingAirshipRequests = new ArrayList();
                }
                pendingAirshipRequests.add(cancelableOperation);
            }
        }
        return cancelableOperation;
    }

    public static void takeOff(Application application, AirshipConfigOptions options, OnReadyCallback readyCallback) {
        if (application == null) {
            throw new IllegalArgumentException("Application argument must not be null");
        }
        if (Looper.myLooper() == null || Looper.getMainLooper() != Looper.myLooper()) {
            Logger.error("takeOff() must be called on the main thread!");
        } else if (VERSION.SDK_INT < 16) {
            try {
                Class.forName("android.os.AsyncTask");
            } catch (ClassNotFoundException e) {
                Logger.error("AsyncTask workaround failed.", e);
            }
        }
        synchronized (airshipLock) {
            if (isFlying || isTakingOff) {
                Logger.error("You can only call takeOff() once.");
                return;
            }
            Logger.info("Airship taking off!");
            isTakingOff = true;
            application = application;
            UrbanAirshipProvider.init();
            if (VERSION.SDK_INT >= 14) {
                Analytics.registerLifeCycleCallbacks(application);
                InAppMessageManager.registerLifeCycleCallbacks(application);
            }
            new Thread(new C16102(application, options, readyCallback)).start();
        }
    }

    private static void executeTakeOff(Application application, AirshipConfigOptions options, OnReadyCallback readyCallback) {
        if (options == null) {
            options = AirshipConfigOptions.loadDefaultOptions(application.getApplicationContext());
        }
        if (options.isValid()) {
            Logger.logLevel = options.getLoggerLevel();
            Logger.TAG = getAppName() + " - UALib";
            Logger.info("Airship taking off!");
            Logger.info("Airship log level: " + Logger.logLevel);
            Logger.info("UA Version: " + getVersion() + " / App key = " + options.getAppKey() + " Production = " + options.inProduction);
            PreferenceDataStore preferenceDataStore = new PreferenceDataStore(application.getApplicationContext());
            preferenceDataStore.loadAll();
            sharedAirship = new UAirship(application.getApplicationContext(), options, preferenceDataStore);
            sharedAirship.init();
            String currentVersion = getVersion();
            String previousVersion = preferenceDataStore.getString("com.urbanairship.application.device.LIBRARY_VERSION", null);
            if (!(previousVersion == null || previousVersion.equals(currentVersion))) {
                Logger.info("Urban Airship library changed from " + previousVersion + " to " + currentVersion + ".");
            }
            preferenceDataStore.put("com.urbanairship.application.device.LIBRARY_VERSION", getVersion());
            if (!options.inProduction) {
                sharedAirship.validateManifest();
            }
            synchronized (airshipLock) {
                isFlying = true;
                isTakingOff = false;
                Logger.info("Airship ready!");
                if (readyCallback != null) {
                    readyCallback.onAirshipReady(sharedAirship);
                }
                if (pendingAirshipRequests != null) {
                    for (Runnable pendingRequest : new ArrayList(pendingAirshipRequests)) {
                        pendingRequest.run();
                    }
                    pendingAirshipRequests = null;
                }
                airshipLock.notifyAll();
            }
            return;
        }
        synchronized (airshipLock) {
            isTakingOff = false;
            airshipLock.notifyAll();
        }
        Logger.error("AirshipConfigOptions are not valid. Unable to take off! Check your airshipconfig.properties file for the errors listed above.");
        throw new IllegalArgumentException("Application configuration is invalid.");
    }

    public static String getPackageName() {
        return application.getPackageName();
    }

    public static String getUrbanAirshipPermission() {
        return application.getPackageName() + ".permission.UA_DATA";
    }

    public static PackageManager getPackageManager() {
        return application.getPackageManager();
    }

    public static PackageInfo getPackageInfo() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (NameNotFoundException e) {
            Logger.warn("UAirship - Unable to get package info.", e);
            return null;
        }
    }

    public static ApplicationInfo getAppInfo() {
        return application.getApplicationInfo();
    }

    public static String getAppName() {
        if (getAppInfo() != null) {
            return getPackageManager().getApplicationLabel(getAppInfo()).toString();
        }
        return null;
    }

    public static Context getApplicationContext() {
        if (application != null) {
            return application.getApplicationContext();
        }
        throw new IllegalStateException("TakeOff must be called first.");
    }

    public static boolean isFlying() {
        return isFlying;
    }

    public static boolean isTakingOff() {
        return isTakingOff;
    }

    public static String getVersion() {
        return "6.0.0";
    }

    private void init() {
        this.richPushManager.init();
        this.pushManager.init();
        this.locationManager.init();
        this.inAppMessageManager.init();
        this.actionRegistry.registerDefaultActions();
    }

    public AirshipConfigOptions getAirshipConfigOptions() {
        return this.airshipConfigOptions;
    }

    public PushManager getPushManager() {
        return this.pushManager;
    }

    public RichPushManager getRichPushManager() {
        return this.richPushManager;
    }

    public UALocationManager getLocationManager() {
        return this.locationManager;
    }

    public InAppMessageManager getInAppMessageManager() {
        return this.inAppMessageManager;
    }

    public Analytics getAnalytics() {
        return this.analytics;
    }

    public ApplicationMetrics getApplicationMetrics() {
        return this.applicationMetrics;
    }

    public Whitelist getWhitelist() {
        return this.whitelist;
    }

    public ActionRegistry getActionRegistry() {
        return this.actionRegistry;
    }

    public int getPlatformType() {
        int platform = this.preferenceDataStore.getInt("com.urbanairship.application.device.PLATFORM", -1);
        if (platform == -1) {
            if (ADMUtils.isADMAvailable()) {
                Logger.info("ADM available. Setting platform to Amazon.");
                platform = 1;
            } else if (PlayServicesUtils.isGooglePlayStoreAvailable()) {
                Logger.info("Google Play Store available. Setting platform to Android.");
                platform = 2;
            } else if ("amazon".equalsIgnoreCase(Build.MANUFACTURER)) {
                Logger.info("Build.MANUFACTURER is AMAZON. Setting platform to Amazon.");
                platform = 1;
            } else {
                Logger.info("Defaulting platform to Android.");
                platform = 2;
            }
            this.preferenceDataStore.put("com.urbanairship.application.device.PLATFORM", Integer.valueOf(platform));
        }
        return platform;
    }

    private void validateManifest() {
        ManifestUtils.validateManifest(this.airshipConfigOptions);
        switch (sharedAirship.getPlatformType()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                if (this.airshipConfigOptions.isTransportAllowed("ADM")) {
                    ADMUtils.validateManifest();
                } else {
                    Logger.error("Amazon platform detected but ADM transport is disabled. The device will not be able to receive push notifications.");
                }
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                if (this.airshipConfigOptions.isTransportAllowed("GCM")) {
                    GCMUtils.validateManifest(this.airshipConfigOptions);
                } else {
                    Logger.error("Android platform detected but GCM transport is disabled. The device will not be able to receive push notifications.");
                }
            default:
        }
    }
}
