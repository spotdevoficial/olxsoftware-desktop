package com.urbanairship.widget;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.UAirship;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.impl.auth.BasicScheme;

public class LandingPageWebView extends UAWebView {
    public LandingPageWebView(Context context) {
        super(context);
    }

    public LandingPageWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LandingPageWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @TargetApi(21)
    public LandingPageWebView(Context context, AttributeSet attrs, int defStyle, int defResStyle) {
        super(context, attrs, defStyle, defResStyle);
    }

    @SuppressLint({"NewApi"})
    public void loadUrl(String url) {
        if (url == null || !url.startsWith(UAirship.shared().getAirshipConfigOptions().landingPageContentURL)) {
            super.loadUrl(url);
            return;
        }
        if (VERSION.SDK_INT >= 8) {
            AirshipConfigOptions options = UAirship.shared().getAirshipConfigOptions();
            Header credentialHeader = BasicScheme.authenticate(new UsernamePasswordCredentials(options.getAppKey(), options.getAppSecret()), "UTF-8", false);
            HashMap<String, String> headers = new HashMap();
            headers.put(credentialHeader.getName(), credentialHeader.getValue());
            super.loadUrl(url, headers);
        } else {
            super.loadUrl(url);
        }
        setClientAuthRequest(url);
    }

    @TargetApi(8)
    public void loadUrl(String url, Map<String, String> additionalHttpHeaders) {
        super.loadUrl(url, additionalHttpHeaders);
        if (url != null && url.startsWith(UAirship.shared().getAirshipConfigOptions().landingPageContentURL)) {
            setClientAuthRequest(url);
        }
    }

    private void setClientAuthRequest(String url) {
        AirshipConfigOptions options = UAirship.shared().getAirshipConfigOptions();
        setClientAuthRequest(url, options.getAppKey(), options.getAppSecret());
    }
}
