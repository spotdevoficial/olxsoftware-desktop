package com.urbanairship.widget;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.HttpAuthHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.urbanairship.C1608R;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.actions.ActionArguments;
import com.urbanairship.actions.ActionCompletionCallback;
import com.urbanairship.actions.ActionResult;
import com.urbanairship.actions.ActionResult.Status;
import com.urbanairship.actions.ActionRunRequestFactory;
import com.urbanairship.actions.ActionValue;
import com.urbanairship.actions.Situation;
import com.urbanairship.js.NativeBridge;
import com.urbanairship.json.JsonException;
import com.urbanairship.json.JsonValue;
import com.urbanairship.richpush.RichPushMessage;
import com.urbanairship.util.UriUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import org.json.JSONObject;

public class UAWebViewClient extends WebViewClient {
    private static SimpleDateFormat dateFormatter;
    private ActionCompletionCallback actionCompletionCallback;
    private final ActionRunRequestFactory actionRunRequestFactory;
    private Map<String, Credentials> authRequestCredentials;

    /* renamed from: com.urbanairship.widget.UAWebViewClient.1 */
    class C16861 implements ActionCompletionCallback {
        C16861() {
        }

        public void onFinish(ActionArguments arguments, ActionResult result) {
            synchronized (this) {
                if (UAWebViewClient.this.actionCompletionCallback != null) {
                    UAWebViewClient.this.actionCompletionCallback.onFinish(arguments, result);
                }
            }
        }
    }

    /* renamed from: com.urbanairship.widget.UAWebViewClient.2 */
    class C16872 implements ActionCompletionCallback {
        final /* synthetic */ String val$callbackKey;
        final /* synthetic */ String val$name;
        final /* synthetic */ WebView val$webView;

        C16872(String str, WebView webView, String str2) {
            this.val$name = str;
            this.val$webView = webView;
            this.val$callbackKey = str2;
        }

        public void onFinish(ActionArguments arguments, ActionResult result) {
            String errorMessage = null;
            switch (C16883.$SwitchMap$com$urbanairship$actions$ActionResult$Status[result.getStatus().ordinal()]) {
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    errorMessage = String.format("Action %s not found", new Object[]{this.val$name});
                    break;
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    errorMessage = String.format("Action %s rejected its arguments", new Object[]{this.val$name});
                    break;
                case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                    if (result.getException() == null) {
                        errorMessage = String.format("Action %s failed with unspecified error", new Object[]{this.val$name});
                        break;
                    }
                    errorMessage = result.getException().getMessage();
                    break;
            }
            UAWebViewClient.this.triggerCallback(this.val$webView, errorMessage, result.getValue(), this.val$callbackKey);
            synchronized (this) {
                if (UAWebViewClient.this.actionCompletionCallback != null) {
                    UAWebViewClient.this.actionCompletionCallback.onFinish(arguments, result);
                }
            }
        }
    }

    /* renamed from: com.urbanairship.widget.UAWebViewClient.3 */
    static /* synthetic */ class C16883 {
        static final /* synthetic */ int[] $SwitchMap$com$urbanairship$actions$ActionResult$Status;

        static {
            $SwitchMap$com$urbanairship$actions$ActionResult$Status = new int[Status.values().length];
            try {
                $SwitchMap$com$urbanairship$actions$ActionResult$Status[Status.ACTION_NOT_FOUND.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$urbanairship$actions$ActionResult$Status[Status.REJECTED_ARGUMENTS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$urbanairship$actions$ActionResult$Status[Status.EXECUTION_ERROR.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    private static class Credentials {
        final String password;
        final String username;

        Credentials(String username, String password) {
            this.username = username;
            this.password = password;
        }
    }

    public UAWebViewClient() {
        this(new ActionRunRequestFactory());
    }

    UAWebViewClient(ActionRunRequestFactory actionRunRequestFactory) {
        this.authRequestCredentials = new HashMap();
        this.actionRunRequestFactory = actionRunRequestFactory;
    }

    public void onClose(WebView webView) {
        webView.getRootView().dispatchKeyEvent(new KeyEvent(0, 4));
        webView.getRootView().dispatchKeyEvent(new KeyEvent(1, 4));
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String url) {
        return interceptUrl(webView, url);
    }

    public void onLoadResource(WebView webView, String url) {
        interceptUrl(webView, url);
    }

    private boolean interceptUrl(WebView webView, String url) {
        if (webView == null || url == null) {
            return false;
        }
        Uri uri = Uri.parse(url);
        if (uri.getHost() == null || !uri.getScheme().equals("uairship") || !isWhiteListed(webView.getUrl())) {
            return false;
        }
        Logger.verbose("Intercepting: " + url);
        String host = uri.getHost();
        boolean z = true;
        switch (host.hashCode()) {
            case -1507513413:
                if (host.equals("run-actions")) {
                    z = true;
                    break;
                }
                break;
            case -189575524:
                if (host.equals("run-basic-actions")) {
                    z = false;
                    break;
                }
                break;
            case 94756344:
                if (host.equals("close")) {
                    z = true;
                    break;
                }
                break;
            case 716793782:
                if (host.equals("android-run-action-cb")) {
                    z = true;
                    break;
                }
                break;
        }
        switch (z) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                Logger.info("Running run basic actions command for URL: " + url);
                runActions(webView, decodeActionArguments(uri, true));
                return true;
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                Logger.info("Running run actions command for URL: " + url);
                runActions(webView, decodeActionArguments(uri, false));
                return true;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                Logger.info("Running run actions command with callback for URL: " + url);
                List<String> paths = uri.getPathSegments();
                if (paths.size() == 3) {
                    Logger.info("Action: " + ((String) paths.get(0)) + ", Args: " + ((String) paths.get(1)) + ", Callback: " + ((String) paths.get(2)));
                    runAction(webView, (String) paths.get(0), (String) paths.get(1), (String) paths.get(2));
                } else {
                    Logger.error("Unable to run action, invalid number of arguments.");
                }
                return true;
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                Logger.info("Running close command for URL: " + url);
                onClose(webView);
                return true;
            default:
                Logger.warn("Unrecognized command: " + uri.getHost() + " for URL: " + url);
                return false;
        }
    }

    private void runActions(WebView webView, Map<String, List<ActionValue>> arguments) {
        if (arguments != null) {
            Bundle metadata = new Bundle();
            RichPushMessage message = getMessage(webView);
            if (message != null) {
                metadata.putString("com.urbanairship.RICH_PUSH_ID_METADATA", message.getMessageId());
            }
            for (String actionName : arguments.keySet()) {
                for (ActionValue arg : (List) arguments.get(actionName)) {
                    this.actionRunRequestFactory.createActionRequest(actionName).setValue(arg).setMetadata(metadata).setSituation(Situation.WEB_VIEW_INVOCATION).run(new C16861());
                }
            }
        }
    }

    private void runAction(WebView webView, String name, String value, String callbackKey) {
        try {
            ActionValue actionValue = new ActionValue(JsonValue.parseString(value));
            Bundle metadata = new Bundle();
            RichPushMessage message = getMessage(webView);
            if (message != null) {
                metadata.putString("com.urbanairship.RICH_PUSH_ID_METADATA", message.getMessageId());
            }
            this.actionRunRequestFactory.createActionRequest(name).setMetadata(metadata).setValue(actionValue).setSituation(Situation.WEB_VIEW_INVOCATION).run(new C16872(name, webView, callbackKey));
        } catch (JsonException e) {
            Logger.warn("Unable to parse action argument value: " + value, e);
            triggerCallback(webView, "Unable to decode arguments payload", new ActionValue(), callbackKey);
        }
    }

    @SuppressLint({"NewAPI"})
    private void triggerCallback(WebView webView, String error, ActionValue resultValue, String callbackKey) {
        String errorString;
        String callbackString = String.format("'%s'", new Object[]{callbackKey});
        if (error == null) {
            errorString = "null";
        } else {
            errorString = String.format(Locale.US, "new Error(%s)", new Object[]{JSONObject.quote(error)});
        }
        String resultValueString = resultValue.toString();
        String finishAction = String.format(Locale.US, "UAirship.finishAction(%s, %s, %s);", new Object[]{errorString, resultValueString, callbackString});
        if (VERSION.SDK_INT >= 19) {
            webView.evaluateJavascript(finishAction, null);
        } else {
            webView.loadUrl("javascript:" + finishAction);
        }
    }

    private Map<String, List<ActionValue>> decodeActionArguments(Uri uri, boolean basicEncoding) {
        Map<String, List<String>> options = UriUtils.getQueryParameters(uri);
        if (options == null) {
            return null;
        }
        Map<String, List<ActionValue>> decodedActions = new HashMap();
        for (String actionName : options.keySet()) {
            List<ActionValue> decodedActionArguments = new ArrayList();
            if (options.get(actionName) == null) {
                Logger.warn("No arguments to decode for actionName: " + actionName);
                return null;
            }
            for (String arg : (List) options.get(actionName)) {
                JsonValue jsonValue;
                if (basicEncoding) {
                    try {
                        jsonValue = JsonValue.wrap(arg);
                    } catch (JsonException e) {
                        Logger.warn("Invalid json. Unable to create action argument " + actionName + " with args: " + arg, e);
                        return null;
                    }
                }
                jsonValue = JsonValue.parseString(arg);
                decodedActionArguments.add(new ActionValue(jsonValue));
            }
            decodedActions.put(actionName, decodedActionArguments);
        }
        if (!decodedActions.isEmpty()) {
            return decodedActions;
        }
        Logger.warn("Error no action names are present in the actions key set");
        return null;
    }

    public void onPageFinished(WebView view, String url) {
        if (view != null) {
            if (isWhiteListed(url)) {
                Logger.info("Loading UrbanAirship Javascript interface.");
                injectJavascriptInterface(view);
                return;
            }
            Logger.debug("UAWebViewClient - " + url + " is not a white listed URL. Urban Airship Javascript interface will not be accessible.");
        }
    }

    private boolean isWhiteListed(String url) {
        return UAirship.shared().getWhitelist().isWhitelisted(url);
    }

    public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
        Credentials credentials = (Credentials) this.authRequestCredentials.get(host);
        if (credentials != null) {
            handler.proceed(credentials.username, credentials.password);
        }
    }

    void addAuthRequestCredentials(String expectedAuthHost, String username, String password) {
        this.authRequestCredentials.put(expectedAuthHost, new Credentials(username, password));
    }

    void removeAuthRequestCredentials(String expectedAuthHost) {
        this.authRequestCredentials.remove(expectedAuthHost);
    }

    @SuppressLint({"NewAPI"})
    private void injectJavascriptInterface(WebView webView) {
        String messageId;
        long sentDateMS;
        RichPushMessage message = getMessage(webView);
        if (dateFormatter == null) {
            dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ", Locale.US);
            dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        }
        StringBuilder sb = new StringBuilder().append("var _UAirship = {};");
        StringBuilder append = sb.append(createGetter("getDeviceModel", Build.MODEL));
        String str = "getMessageId";
        if (message != null) {
            messageId = message.getMessageId();
        } else {
            messageId = null;
        }
        append = append.append(createGetter(str, messageId));
        str = "getMessageTitle";
        if (message != null) {
            messageId = message.getTitle();
        } else {
            messageId = null;
        }
        append = append.append(createGetter(str, messageId));
        str = "getMessageSentDate";
        if (message != null) {
            messageId = dateFormatter.format(message.getSentDate());
        } else {
            messageId = null;
        }
        StringBuilder append2 = append.append(createGetter(str, messageId));
        String str2 = "getMessageSentDateMS";
        if (message != null) {
            sentDateMS = message.getSentDateMS();
        } else {
            sentDateMS = -1;
        }
        append2.append(createGetter(str2, sentDateMS)).append(createGetter("getUserId", UAirship.shared().getRichPushManager().getRichPushUser().getId()));
        sb.append(NativeBridge.getJavaScriptSource());
        String javaScript = sb.toString();
        if (VERSION.SDK_INT >= 19) {
            webView.evaluateJavascript(javaScript, null);
        } else {
            webView.loadUrl("javascript:" + javaScript);
        }
    }

    private String createGetter(String functionName, String value) {
        value = value == null ? "null" : JSONObject.quote(value);
        return String.format(Locale.US, "_UAirship.%s = function(){return %s;};", new Object[]{functionName, value});
    }

    private String createGetter(String functionName, long value) {
        return String.format(Locale.US, "_UAirship.%s = function(){return %d;};", new Object[]{functionName, Long.valueOf(value)});
    }

    private RichPushMessage getMessage(WebView webView) {
        if (webView instanceof RichPushMessageWebView) {
            return ((RichPushMessageWebView) webView).getCurrentMessage();
        }
        return null;
    }
}
