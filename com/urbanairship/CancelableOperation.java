package com.urbanairship;

import android.os.Handler;
import android.os.Looper;

abstract class CancelableOperation implements Cancelable, Runnable {
    private Handler handler;
    private final Runnable internalRunnable;
    private boolean isCanceled;
    private boolean isFinished;
    private boolean isRunning;

    /* renamed from: com.urbanairship.CancelableOperation.1 */
    class C16021 implements Runnable {
        C16021() {
        }

        public void run() {
            synchronized (CancelableOperation.this) {
                if (CancelableOperation.this.isDone()) {
                    return;
                }
                CancelableOperation.this.onRun();
                CancelableOperation.this.isFinished = true;
            }
        }
    }

    /* renamed from: com.urbanairship.CancelableOperation.2 */
    class C16032 implements Runnable {
        C16032() {
        }

        public void run() {
            CancelableOperation.this.onCancel();
        }
    }

    protected abstract void onRun();

    CancelableOperation(Looper looper) {
        this.isFinished = false;
        this.isRunning = false;
        this.isCanceled = false;
        if (looper != null) {
            this.handler = new Handler(looper);
        } else {
            this.handler = Looper.myLooper() != null ? new Handler(Looper.myLooper()) : new Handler(Looper.getMainLooper());
        }
        this.internalRunnable = new C16021();
    }

    public final void cancel() {
        synchronized (this) {
            if (!isDone()) {
                this.isCanceled = true;
                this.handler.removeCallbacks(this.internalRunnable);
                this.handler.post(new C16032());
            }
        }
    }

    public final void run() {
        synchronized (this) {
            if (isDone() || this.isRunning) {
                return;
            }
            this.isRunning = true;
            this.handler.post(this.internalRunnable);
        }
    }

    public final boolean isDone() {
        boolean z;
        synchronized (this) {
            z = this.isFinished || this.isCanceled;
        }
        return z;
    }

    protected void onCancel() {
    }
}
