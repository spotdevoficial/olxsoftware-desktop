package com.urbanairship;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;
import com.urbanairship.UAirship.OnReadyCallback;

public abstract class Autopilot implements OnReadyCallback {
    public static synchronized void automaticTakeOff(Application application) {
        synchronized (Autopilot.class) {
            if (!(UAirship.isFlying() || UAirship.isTakingOff())) {
                String classname = null;
                try {
                    classname = application.getPackageManager().getApplicationInfo(application.getPackageName(), 128).metaData.getString("com.urbanairship.autopilot");
                } catch (NameNotFoundException e) {
                    Log.e("Urban Airship Autopilot", "Failed to load meta-data, NameNotFound: " + e.getMessage());
                } catch (NullPointerException e2) {
                    Log.e("Urban Airship Autopilot", "Failed to load meta-data, NullPointer: " + e2.getMessage());
                }
                if (classname == null) {
                    Log.e("Urban Airship Autopilot", "Unable to takeOff automatically");
                } else {
                    try {
                        try {
                            Autopilot instance = (Autopilot) Class.forName(classname).newInstance();
                            if (instance == null) {
                                Log.e("Urban Airship Autopilot", "Unable to instantiate the defined Autopilot instance. Instance is null.");
                            } else {
                                AirshipConfigOptions options = instance.createAirshipConfigOptions(application);
                                if (UAirship.isFlying() || UAirship.isTakingOff()) {
                                    Log.e("Urban Airship Autopilot", "Airship is flying before autopilot is able to take off. Make sureAutoPilot.onCreateAirshipConfig is not calling takeOff directly.");
                                }
                                UAirship.takeOff(application, options, instance);
                            }
                        } catch (IllegalAccessException e3) {
                            Log.e("Urban Airship Autopilot", "Unable to instantiate the defined Autopilot instance. IllegalAccessException: " + e3.getMessage());
                        } catch (InstantiationException e4) {
                            Log.e("Urban Airship Autopilot", "Unable to instantiate the defined Autopilot instance. InstantiationException: " + e4.getMessage());
                        } catch (ClassCastException e5) {
                            Log.e("Urban Airship Autopilot", "Unable to instantiate the defined Autopilot instance. ClassCastException: " + e5.getMessage());
                        }
                    } catch (ClassNotFoundException e6) {
                        Log.e("Urban Airship Autopilot", "Unable to load the defined Autopilot instance. ClassNotFoundException: " + e6.getMessage());
                    }
                }
            }
        }
    }

    public static void automaticTakeOff(Context context) {
        automaticTakeOff((Application) context.getApplicationContext());
    }

    public AirshipConfigOptions createAirshipConfigOptions(Context context) {
        return null;
    }
}
