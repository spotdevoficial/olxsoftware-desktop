package com.urbanairship.json;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.facebook.BuildConfig;
import com.urbanairship.Logger;
import com.urbanairship.util.UAStringUtil;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.json.JSONTokener;

public class JsonValue implements Parcelable {
    public static final Creator<JsonValue> CREATOR;
    public static final JsonValue NULL;
    private final Object value;

    /* renamed from: com.urbanairship.json.JsonValue.1 */
    static class C16391 implements Creator<JsonValue> {
        C16391() {
        }

        public JsonValue createFromParcel(Parcel in) {
            try {
                return JsonValue.parseString(in.readString());
            } catch (JsonException e) {
                Logger.error("JsonValue - Unable to create JsonValue from parcel.", e);
                return null;
            }
        }

        public JsonValue[] newArray(int size) {
            return new JsonValue[size];
        }
    }

    static {
        NULL = new JsonValue(null);
        CREATOR = new C16391();
    }

    private JsonValue(Object value) {
        this.value = value;
    }

    public String getString() {
        return getString(null);
    }

    public String getString(String defaultValue) {
        if (isNull()) {
            return null;
        }
        return this.value instanceof String ? (String) this.value : defaultValue;
    }

    public double getDouble(double defaultValue) {
        if (isNull()) {
            return defaultValue;
        }
        if (this.value instanceof Double) {
            return ((Double) this.value).doubleValue();
        }
        if (this.value instanceof Number) {
            return ((Number) this.value).doubleValue();
        }
        return defaultValue;
    }

    public long getLong(long defaultValue) {
        if (isNull()) {
            return defaultValue;
        }
        if (this.value instanceof Long) {
            return ((Long) this.value).longValue();
        }
        if (this.value instanceof Number) {
            return ((Number) this.value).longValue();
        }
        return defaultValue;
    }

    public boolean getBoolean(boolean defaultValue) {
        if (!isNull() && (this.value instanceof Boolean)) {
            return ((Boolean) this.value).booleanValue();
        }
        return defaultValue;
    }

    public JsonList getList() {
        if (isNull() || !(this.value instanceof JsonList)) {
            return null;
        }
        return (JsonList) this.value;
    }

    public JsonMap getMap() {
        if (isNull() || !(this.value instanceof JsonMap)) {
            return null;
        }
        return (JsonMap) this.value;
    }

    public boolean isNull() {
        return this.value == null;
    }

    public static JsonValue parseString(String jsonString) throws JsonException {
        if (UAStringUtil.isEmpty(jsonString)) {
            return NULL;
        }
        try {
            return wrap(new JSONTokener(jsonString).nextValue());
        } catch (JSONException e) {
            throw new JsonException("Unable to parse string", e);
        }
    }

    public boolean equals(Object object) {
        if (!(object instanceof JsonValue)) {
            return false;
        }
        JsonValue o = (JsonValue) object;
        if (isNull()) {
            return o.isNull();
        }
        return this.value.equals(o.value);
    }

    public int hashCode() {
        if (this.value != null) {
            return this.value.hashCode() + 527;
        }
        return 17;
    }

    public String toString() {
        if (isNull()) {
            return "null";
        }
        try {
            if (this.value instanceof String) {
                return JSONObject.quote((String) this.value);
            }
            if (this.value instanceof Number) {
                return JSONObject.numberToString((Number) this.value);
            }
            if ((this.value instanceof JsonMap) || (this.value instanceof JsonList)) {
                return this.value.toString();
            }
            return String.valueOf(this.value);
        } catch (JSONException e) {
            Logger.error("JsonValue - Failed to create JSON String.", e);
            return BuildConfig.VERSION_NAME;
        }
    }

    void write(JSONStringer stringer) throws JSONException {
        if (isNull()) {
            stringer.value(null);
        } else if (this.value instanceof JsonList) {
            ((JsonList) this.value).write(stringer);
        } else if (this.value instanceof JsonMap) {
            ((JsonMap) this.value).write(stringer);
        } else {
            stringer.value(this.value);
        }
    }

    public static JsonValue wrap(Object object, JsonValue defaultValue) {
        try {
            defaultValue = wrap(object);
        } catch (JsonException e) {
        }
        return defaultValue;
    }

    public static JsonValue wrap(Object object) throws JsonException {
        if (object == null || object == JSONObject.NULL) {
            return NULL;
        }
        if (object instanceof JsonValue) {
            return (JsonValue) object;
        }
        if ((object instanceof JsonMap) || (object instanceof JsonList) || (object instanceof Boolean) || (object instanceof Integer) || (object instanceof Long) || (object instanceof String)) {
            return new JsonValue(object);
        }
        if (object instanceof JsonSerializable) {
            JsonValue jsonValue = ((JsonSerializable) object).toJsonValue();
            if (jsonValue == null) {
                jsonValue = NULL;
            }
            return jsonValue;
        } else if ((object instanceof Byte) || (object instanceof Short)) {
            return new JsonValue(Integer.valueOf(((Number) object).intValue()));
        } else {
            if (object instanceof Character) {
                return new JsonValue(((Character) object).toString());
            }
            if (object instanceof Float) {
                return new JsonValue(Double.valueOf(((Number) object).doubleValue()));
            }
            if (object instanceof Double) {
                Double d = (Double) object;
                if (!d.isInfinite() && !d.isNaN()) {
                    return new JsonValue(object);
                }
                throw new JsonException("Invalid Double value: " + d);
            }
            try {
                if (object instanceof JSONArray) {
                    return wrapJSONArray((JSONArray) object);
                }
                if (object instanceof JSONObject) {
                    return wrapJSONObject((JSONObject) object);
                }
                if (object instanceof Collection) {
                    return wrapCollection((Collection) object);
                }
                if (object.getClass().isArray()) {
                    return wrapArray(object);
                }
                if (object instanceof Map) {
                    return wrapMap((Map) object);
                }
                throw new JsonException("Illegal object: " + object);
            } catch (JsonException exception) {
                throw exception;
            } catch (Exception exception2) {
                throw new JsonException("Failed to wrap value.", exception2);
            }
        }
    }

    private static JsonValue wrapArray(Object array) throws JsonException {
        int length = Array.getLength(array);
        List<JsonValue> list = new ArrayList(length);
        for (int i = 0; i < length; i++) {
            list.add(wrap(Array.get(array, i)));
        }
        return new JsonValue(new JsonList(list));
    }

    private static JsonValue wrapCollection(Collection collection) throws JsonException {
        List<JsonValue> list = new ArrayList();
        for (Object obj : collection) {
            list.add(wrap(obj));
        }
        return new JsonValue(new JsonList(list));
    }

    private static JsonValue wrapMap(Map<?, ?> map) throws JsonException {
        Map<String, JsonValue> jsonValueMap = new HashMap();
        for (Entry entry : map.entrySet()) {
            if (entry.getKey() instanceof String) {
                jsonValueMap.put((String) entry.getKey(), wrap(entry.getValue()));
            } else {
                throw new JsonException("Only string map keys are accepted.");
            }
        }
        return new JsonValue(new JsonMap(jsonValueMap));
    }

    private static JsonValue wrapJSONArray(JSONArray jsonArray) throws JsonException {
        List<JsonValue> list = new ArrayList(jsonArray.length());
        for (int i = 0; i < jsonArray.length(); i++) {
            if (jsonArray.isNull(i)) {
                list.add(NULL);
            } else {
                list.add(wrap(jsonArray.opt(i)));
            }
        }
        return new JsonValue(new JsonList(list));
    }

    private static JsonValue wrapJSONObject(JSONObject jsonObject) throws JsonException {
        Map<String, JsonValue> jsonValueMap = new HashMap();
        if (jsonObject == null || jsonObject.length() == 0) {
            return new JsonValue(jsonValueMap);
        }
        Iterator iterator = jsonObject.keys();
        while (iterator.hasNext()) {
            String key = (String) iterator.next();
            if (jsonObject.isNull(key)) {
                jsonValueMap.put(key, NULL);
            } else {
                jsonValueMap.put(key, wrap(jsonObject.opt(key)));
            }
        }
        return new JsonValue(new JsonMap(jsonValueMap));
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(toString());
    }
}
