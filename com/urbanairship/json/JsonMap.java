package com.urbanairship.json;

import com.facebook.BuildConfig;
import com.urbanairship.Logger;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONStringer;

public class JsonMap {
    private Map<String, JsonValue> map;

    public JsonMap(Map<String, JsonValue> map) {
        this.map = map == null ? new HashMap() : new HashMap(map);
    }

    public boolean containsKey(String key) {
        return this.map.containsKey(key);
    }

    public Set<Entry<String, JsonValue>> entrySet() {
        return this.map.entrySet();
    }

    public JsonValue get(String key) {
        return (JsonValue) this.map.get(key);
    }

    public JsonValue opt(String key) {
        JsonValue value = get(key);
        return value != null ? value : JsonValue.NULL;
    }

    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object instanceof JsonMap) {
            return this.map.equals(((JsonMap) object).map);
        }
        return false;
    }

    public int hashCode() {
        return this.map.hashCode();
    }

    public String toString() {
        try {
            JSONStringer stringer = new JSONStringer();
            write(stringer);
            return stringer.toString();
        } catch (JSONException e) {
            Logger.error("JsonMap - Failed to create JSON String.", e);
            return BuildConfig.VERSION_NAME;
        }
    }

    void write(JSONStringer stringer) throws JSONException {
        stringer.object();
        for (Entry<String, JsonValue> entry : entrySet()) {
            stringer.key((String) entry.getKey());
            ((JsonValue) entry.getValue()).write(stringer);
        }
        stringer.endObject();
    }
}
