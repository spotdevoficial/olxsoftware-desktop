package com.urbanairship.google;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build.VERSION;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.urbanairship.UAirship;

public class PlayServicesUtils {
    public static int MISSING_PLAY_SERVICE_DEPENDENCY;
    private static Boolean isFusedLocationDependencyAvailable;
    private static Boolean isGoogleCloudMessagingDependencyAvailable;
    private static Boolean isGooglePlayServicesDependencyAvailable;

    static {
        MISSING_PLAY_SERVICE_DEPENDENCY = -1;
    }

    public static int isGooglePlayServicesAvailable(Context context) {
        if (isGooglePlayServicesDependencyAvailable()) {
            return GooglePlayServicesUtilWrapper.isGooglePlayServicesAvailable(context);
        }
        return MISSING_PLAY_SERVICE_DEPENDENCY;
    }

    public static boolean isGooglePlayServicesDependencyAvailable() {
        if (isGooglePlayServicesDependencyAvailable == null) {
            if (VERSION.SDK_INT < 8) {
                isGooglePlayServicesDependencyAvailable = Boolean.valueOf(false);
            } else {
                try {
                    Class.forName("com.google.android.gms.common.GooglePlayServicesUtil");
                    isGooglePlayServicesDependencyAvailable = Boolean.valueOf(true);
                } catch (ClassNotFoundException e) {
                    isGooglePlayServicesDependencyAvailable = Boolean.valueOf(false);
                }
            }
        }
        return isGooglePlayServicesDependencyAvailable.booleanValue();
    }

    public static boolean isGoogleCloudMessagingDependencyAvailable() {
        if (isGoogleCloudMessagingDependencyAvailable == null) {
            if (isGooglePlayServicesDependencyAvailable()) {
                try {
                    Class.forName("com.google.android.gms.gcm.GoogleCloudMessaging");
                    isGoogleCloudMessagingDependencyAvailable = Boolean.valueOf(true);
                } catch (ClassNotFoundException e) {
                    isGoogleCloudMessagingDependencyAvailable = Boolean.valueOf(false);
                }
            } else {
                isGoogleCloudMessagingDependencyAvailable = Boolean.valueOf(false);
            }
        }
        return isGoogleCloudMessagingDependencyAvailable.booleanValue();
    }

    public static boolean isFusedLocationDepdendencyAvailable() {
        if (isFusedLocationDependencyAvailable == null) {
            if (isGooglePlayServicesDependencyAvailable()) {
                try {
                    Class.forName("com.google.android.gms.location.LocationServices");
                    isFusedLocationDependencyAvailable = Boolean.valueOf(true);
                } catch (ClassNotFoundException e) {
                    isFusedLocationDependencyAvailable = Boolean.valueOf(false);
                }
            } else {
                isFusedLocationDependencyAvailable = Boolean.valueOf(false);
            }
        }
        return isFusedLocationDependencyAvailable.booleanValue();
    }

    public static boolean isGooglePlayStoreAvailable() {
        for (PackageInfo packageInfo : UAirship.getPackageManager().getInstalledPackages(0)) {
            if (!packageInfo.packageName.equals(GooglePlayServicesUtil.GOOGLE_PLAY_STORE_PACKAGE)) {
                if (packageInfo.packageName.equals("com.google.market")) {
                }
            }
            return true;
        }
        return false;
    }
}
