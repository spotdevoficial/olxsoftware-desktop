package com.urbanairship.google;

import android.content.Context;
import com.google.android.gms.common.GooglePlayServicesUtil;

class GooglePlayServicesUtilWrapper {
    public static int isGooglePlayServicesAvailable(Context context) {
        return GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
    }
}
