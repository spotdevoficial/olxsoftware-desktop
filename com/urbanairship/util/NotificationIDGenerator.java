package com.urbanairship.util;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;

public class NotificationIDGenerator {
    private static int range;
    private static int start;

    static {
        start = vd.f504D;
        range = 40;
    }

    private static SharedPreferences getPreferences() {
        return UAirship.getApplicationContext().getSharedPreferences("com.urbanairship.notificationidgenerator", 0);
    }

    private static void putInt(String key, int value) {
        Editor editor = getPreferences().edit();
        editor.putInt(key, value);
        editor.commit();
    }

    private static int getInt(String key, int defvalue) {
        return getPreferences().getInt(key, defvalue);
    }

    public static int nextID() {
        int id = getInt("count", start) + 1;
        int nextId = id;
        if (nextId < start + range) {
            Logger.verbose("NotificationIDGenerator - Incrementing notification ID count");
            putInt("count", nextId);
        } else {
            Logger.verbose("NotificationIDGenerator - Resetting notification ID count");
            putInt("count", start);
        }
        Logger.verbose("NotificationIDGenerator - Notification ID: " + id);
        return id;
    }
}
