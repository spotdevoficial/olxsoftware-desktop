package com.urbanairship.util;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;

public class Network {
    public static boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) UAirship.getApplicationContext().getSystemService("connectivity");
        if (cm != null) {
            NetworkInfo info = cm.getActiveNetworkInfo();
            if (info == null || !info.isConnected()) {
                return false;
            }
            return true;
        }
        Logger.error("Error fetching network info.");
        return false;
    }
}
