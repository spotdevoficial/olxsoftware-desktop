package com.urbanairship.util;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.CoreActivity;
import com.urbanairship.CoreReceiver;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.UrbanAirshipProvider;
import com.urbanairship.actions.ActionActivity;
import com.urbanairship.actions.ActionService;
import com.urbanairship.analytics.EventService;
import com.urbanairship.location.LocationService;
import com.urbanairship.push.BaseIntentReceiver;
import com.urbanairship.push.GCMPushReceiver;
import com.urbanairship.push.PushService;
import com.urbanairship.richpush.RichPushUpdateService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ManifestUtils {
    private static final String[] BASE_INTENT_RECEIVER_ACTIONS;

    /* renamed from: com.urbanairship.util.ManifestUtils.1 */
    static class C16851 extends HashMap<Class, ComponentInfo> {
        C16851() {
            put(EventService.class, ManifestUtils.getServiceInfo(EventService.class));
            put(PushService.class, ManifestUtils.getServiceInfo(PushService.class));
            put(RichPushUpdateService.class, ManifestUtils.getServiceInfo(RichPushUpdateService.class));
            put(ActionService.class, ManifestUtils.getServiceInfo(ActionService.class));
            put(LocationService.class, ManifestUtils.getServiceInfo(LocationService.class));
            put(CoreReceiver.class, ManifestUtils.getReceiverInfo(CoreReceiver.class));
            put(GCMPushReceiver.class, ManifestUtils.getReceiverInfo(GCMPushReceiver.class));
            put(UrbanAirshipProvider.class, ManifestUtils.getProviderInfo(UrbanAirshipProvider.getAuthorityString()));
            put(ActionActivity.class, ManifestUtils.getActivityInfo(ActionActivity.class));
            put(CoreActivity.class, ManifestUtils.getActivityInfo(CoreActivity.class));
        }
    }

    static {
        BASE_INTENT_RECEIVER_ACTIONS = new String[]{"com.urbanairship.push.RECEIVED", "com.urbanairship.push.OPENED", "com.urbanairship.push.CHANNEL_UPDATED", "com.urbanairship.push.DISMISSED"};
    }

    public static void checkRequiredPermission(String permission) {
        if (-1 == UAirship.getPackageManager().checkPermission(permission, UAirship.getPackageName())) {
            Logger.error("AndroidManifest.xml missing required permission: " + permission);
        }
    }

    public static boolean isPermissionGranted(String permission) {
        return UAirship.getPackageManager().checkPermission(permission, UAirship.getPackageName()) == 0;
    }

    public static ComponentInfo getServiceInfo(Class service) {
        try {
            return UAirship.getPackageManager().getServiceInfo(new ComponentName(UAirship.getPackageName(), service.getCanonicalName()), 128);
        } catch (Exception e) {
            return null;
        }
    }

    public static ActivityInfo getActivityInfo(Class activity) {
        try {
            return UAirship.getPackageManager().getActivityInfo(new ComponentName(UAirship.getPackageName(), activity.getCanonicalName()), 128);
        } catch (Exception e) {
            return null;
        }
    }

    public static ComponentInfo getReceiverInfo(Class receiver) {
        try {
            return UAirship.getPackageManager().getReceiverInfo(new ComponentName(UAirship.getPackageName(), receiver.getCanonicalName()), 128);
        } catch (Exception e) {
            return null;
        }
    }

    public static ComponentInfo getProviderInfo(String authorityString) {
        return UAirship.getPackageManager().resolveContentProvider(authorityString, 0);
    }

    public static boolean isPermissionKnown(String permission) {
        try {
            UAirship.getPackageManager().getPermissionInfo(permission, 0);
            return true;
        } catch (NameNotFoundException e) {
            return false;
        }
    }

    public static void validateManifest(AirshipConfigOptions airshipConfigOptions) {
        checkRequiredPermission("android.permission.INTERNET");
        checkRequiredPermission("android.permission.ACCESS_NETWORK_STATE");
        if (isPermissionKnown(UAirship.getUrbanAirshipPermission())) {
            checkRequiredPermission(UAirship.getUrbanAirshipPermission());
        } else {
            Logger.error("AndroidManifest.xml does not define and require permission: " + UAirship.getUrbanAirshipPermission());
        }
        Map<Class, ComponentInfo> componentInfoMap = getUrbanAirshipComponentInfoMap();
        if (componentInfoMap.get(CoreReceiver.class) == null) {
            Logger.error("AndroidManifest.xml missing required receiver: " + CoreReceiver.class.getCanonicalName());
        } else {
            ComponentInfo coreInfo = (ComponentInfo) componentInfoMap.get(CoreReceiver.class);
            ResolveInfo coreResolveInfo = null;
            for (ResolveInfo info : UAirship.getPackageManager().queryBroadcastReceivers(new Intent("com.urbanairship.push.OPENED").addCategory(UAirship.getPackageName()), 0)) {
                if (!(info.activityInfo == null || info.activityInfo.name == null)) {
                    if (info.activityInfo.name.equals(coreInfo.name)) {
                        coreResolveInfo = info;
                    }
                }
            }
            if (coreResolveInfo == null) {
                Logger.error("AndroidManifest.xml's " + CoreReceiver.class.getCanonicalName() + " declaration missing required intent-filter: <intent-filter android:priority=\"-999\">" + "<action android:name=\"" + "com.urbanairship.push.OPENED" + "\"/>" + "<category android:name=\"" + UAirship.getPackageName() + "\"/></intent-filter>");
            } else if (coreResolveInfo.priority != -999) {
                Logger.error("CoreReceiver's intent filter priority should be set to -999 in order to let the application launch any activities before Urban Airship performs any actions or falls back to launching the application launch intent.");
            }
        }
        ActivityInfo[] receivers = null;
        try {
            receivers = UAirship.getPackageManager().getPackageInfo(UAirship.getPackageName(), 2).receivers;
        } catch (Exception e) {
            Logger.error("Unable to query the application's receivers.", e);
        }
        if (receivers != null) {
            for (ActivityInfo info2 : receivers) {
                try {
                    if (BaseIntentReceiver.class.isAssignableFrom(Class.forName(info2.name))) {
                        validateBaseIntentReceiver(info2);
                    }
                } catch (ClassNotFoundException e2) {
                    Logger.debug("ManifestUtils - Unable to find class: " + info2.name, e2);
                }
            }
        }
        if (componentInfoMap.get(CoreActivity.class) == null) {
            Logger.error("AndroidManifest.xml missing required activity: " + CoreActivity.class.getCanonicalName());
        }
        if (airshipConfigOptions.analyticsEnabled && componentInfoMap.get(EventService.class) == null) {
            Logger.error("AndroidManifest.xml missing required service: " + EventService.class.getCanonicalName());
        }
        if (componentInfoMap.get(PushService.class) == null) {
            Logger.error("AndroidManifest.xml missing required service: " + PushService.class.getCanonicalName());
        }
        if (componentInfoMap.get(RichPushUpdateService.class) == null) {
            Logger.error("AndroidManifest.xml missing required service: " + RichPushUpdateService.class.getCanonicalName());
        }
        if (componentInfoMap.get(ActionService.class) == null) {
            Logger.error("AndroidManifest.xml missing required service: " + ActionService.class.getCanonicalName());
        }
        if (componentInfoMap.get(ActionActivity.class) == null) {
            Logger.warn("AndroidManifest.xml missing ActionActivity.  Action.startActivityForResult will not work.");
        }
        if (UAirship.getPackageManager().resolveActivity(new Intent("com.urbanairship.actions.SHOW_LANDING_PAGE_INTENT_ACTION", Uri.parse("http://")).setPackage(UAirship.getPackageName()).addFlags(268435456).addCategory("android.intent.category.DEFAULT"), 0) == null) {
            Logger.warn("AndroidManifest.xml missing activity with an intent filter for action com.urbanairship.actions.SHOW_LANDING_PAGE_INTENT_ACTION, category android.intent.category.DEFAULT, and data with scheme http.  Landing page action may not function properly.");
        }
        if (UAirship.getPackageManager().resolveActivity(new Intent("com.urbanairship.actions.SHOW_LANDING_PAGE_INTENT_ACTION", Uri.parse("https://")).setPackage(UAirship.getPackageName()).addFlags(268435456).addCategory("android.intent.category.DEFAULT"), 0) == null) {
            Logger.error("AndroidManifest.xml missing activity with an intent filter for action com.urbanairship.actions.SHOW_LANDING_PAGE_INTENT_ACTION, category android.intent.category.DEFAULT, and data with scheme https Landing page action may not function properly.");
        }
        String processName;
        if (UAirship.getAppInfo() == null) {
            processName = UAirship.getPackageName();
        } else {
            processName = UAirship.getAppInfo().processName;
        }
        for (Class component : componentInfoMap.keySet()) {
            ComponentInfo info3 = (ComponentInfo) componentInfoMap.get(component);
            if (!(info3 == null || processName.equals(info3.processName))) {
                Logger.warn("A separate process is detected for: " + component.getCanonicalName() + ". In the " + "AndroidManifest.xml, remove the android:process attribute.");
            }
        }
        if (componentInfoMap.get(UrbanAirshipProvider.class) == null) {
            throw new IllegalStateException("Unable to resolve UrbanAirshipProvider. Please check that the provider is defined in your AndroidManifest.xml, and that the authority string is set to  \"YOUR_PACKAGENAME.urbanairship.provider\"");
        }
    }

    private static Map<Class, ComponentInfo> getUrbanAirshipComponentInfoMap() {
        return new C16851();
    }

    private static void validateBaseIntentReceiver(ActivityInfo info) {
        if (info.exported) {
            Logger.error("Receiver " + info.name + " is exported. This might " + "allow outside applications to message the receiver. Make sure the intent is protected by a " + "permission or prevent the receiver from being exported.");
        }
        List<String> missingActions = new ArrayList();
        for (String action : BASE_INTENT_RECEIVER_ACTIONS) {
            boolean resolved = false;
            for (ResolveInfo resolveInfo : UAirship.getPackageManager().queryBroadcastReceivers(new Intent(action).addCategory(UAirship.getPackageName()), 0)) {
                if (resolveInfo.activityInfo != null && resolveInfo.activityInfo.name != null && resolveInfo.activityInfo.name.equals(info.name)) {
                    resolved = true;
                    break;
                }
            }
            if (!resolved) {
                missingActions.add(action);
            }
        }
        if (!missingActions.isEmpty()) {
            Logger.error("Receiver " + info.name + " unable to receive intents for actions: " + missingActions);
            StringBuilder sb = new StringBuilder();
            sb.append("Update the manifest entry for ").append(info.name).append(" to:").append("\n<receiver android:name=\"").append(info.name).append("\" exported=\"false\">").append("\n\t<intent-filter> ");
            for (String action2 : BASE_INTENT_RECEIVER_ACTIONS) {
                sb.append("\n\t\t<action android:name=\"").append(action2).append("\" />");
            }
            sb.append("\n\t\t<!-- Replace ${applicationId} with ").append(UAirship.getPackageName()).append(" if not using Android Gradle plugin -->").append("\n\t\t<category android:name=\"${applicationId}\" />").append("\n\t</intent-filter>").append("\n</receiver>");
            Logger.error(sb.toString());
        }
    }
}
