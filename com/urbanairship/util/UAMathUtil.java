package com.urbanairship.util;

public abstract class UAMathUtil {
    public static int constrain(int value, int min, int max) {
        if (value > max) {
            return max;
        }
        if (value < min) {
            return min;
        }
        return value;
    }
}
