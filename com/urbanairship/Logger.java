package com.urbanairship;

import android.util.Log;
import com.facebook.BuildConfig;

public class Logger {
    public static String TAG;
    public static int logLevel;

    static {
        logLevel = 6;
        TAG = "UALib";
    }

    public static void warn(String s) {
        if (logLevel <= 5 && s != null) {
            Log.w(TAG, s);
        }
    }

    public static void warn(String s, Throwable t) {
        if (logLevel <= 5 && s != null && t != null) {
            Log.w(TAG, s, t);
        }
    }

    public static void verbose(String s) {
        if (logLevel <= 2 && s != null) {
            Log.v(TAG, s);
        }
    }

    public static void debug(String s) {
        if (logLevel <= 3 && s != null) {
            Log.d(TAG, s);
        }
    }

    public static void debug(String s, Throwable t) {
        if (logLevel <= 3 && s != null && t != null) {
            Log.d(TAG, s, t);
        }
    }

    public static void info(String s) {
        if (logLevel <= 4 && s != null) {
            Log.i(TAG, s);
        }
    }

    public static void error(String s) {
        if (logLevel <= 6 && s != null) {
            Log.e(TAG, s);
        }
    }

    public static void error(Throwable t) {
        if (logLevel <= 6 && t != null) {
            Log.e(TAG, BuildConfig.VERSION_NAME, t);
        }
    }

    public static void error(String s, Throwable t) {
        if (logLevel <= 6 && s != null && t != null) {
            Log.e(TAG, s, t);
        }
    }
}
