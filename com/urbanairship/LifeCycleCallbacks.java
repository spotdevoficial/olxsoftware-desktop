package com.urbanairship;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;

@TargetApi(14)
public abstract class LifeCycleCallbacks {
    private ActivityLifecycleCallbacks activityLifecycleCallbacks;
    private final Application application;
    private boolean isRegistered;

    /* renamed from: com.urbanairship.LifeCycleCallbacks.1 */
    class C16041 implements ActivityLifecycleCallbacks {
        C16041() {
        }

        public void onActivityPaused(Activity activity) {
            LifeCycleCallbacks.this.onActivityPaused(activity);
        }

        public void onActivityResumed(Activity activity) {
            LifeCycleCallbacks.this.onActivityResumed(activity);
        }

        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        }

        public void onActivityDestroyed(Activity activity) {
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        }

        public void onActivityStarted(Activity activity) {
            LifeCycleCallbacks.this.onActivityStarted(activity);
        }

        public void onActivityStopped(Activity activity) {
            LifeCycleCallbacks.this.onActivityStopped(activity);
        }
    }

    public LifeCycleCallbacks(Application application) {
        this.isRegistered = false;
        this.application = application;
        this.activityLifecycleCallbacks = new C16041();
    }

    public void register() {
        if (!this.isRegistered) {
            this.application.registerActivityLifecycleCallbacks(this.activityLifecycleCallbacks);
            this.isRegistered = true;
        }
    }

    public void onActivityStopped(Activity activity) {
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
    }

    public void onActivityResumed(Activity activity) {
    }
}
