package com.urbanairship.amazon;

import com.urbanairship.Logger;

public class ADMUtils {
    private static Boolean isADMAvailable;

    public static boolean isADMAvailable() {
        if (isADMAvailable != null) {
            return isADMAvailable.booleanValue();
        }
        try {
            Class.forName("com.amazon.device.messaging.ADM");
            isADMAvailable = Boolean.valueOf(true);
        } catch (ClassNotFoundException e) {
            isADMAvailable = Boolean.valueOf(false);
        }
        return isADMAvailable.booleanValue();
    }

    public static boolean isADMSupported() {
        return isADMAvailable() && ADMWrapper.isSupported();
    }

    public static void validateManifest() {
        if (isADMAvailable()) {
            ADMWrapper.validateManifest();
        } else {
            Logger.warn("ADM is not available on this device.");
        }
    }
}
