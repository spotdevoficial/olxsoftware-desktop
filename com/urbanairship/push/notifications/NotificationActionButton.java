package com.urbanairship.push.notifications;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat.Action;
import android.support.v4.app.NotificationCompat.Action.Extender;
import com.facebook.BuildConfig;
import com.urbanairship.CoreActivity;
import com.urbanairship.CoreReceiver;
import com.urbanairship.push.PushMessage;
import java.util.List;
import java.util.UUID;

public class NotificationActionButton {
    private final String description;
    private final Bundle extras;
    private final int iconId;
    private final String id;
    private final boolean isForegroundAction;
    private final int labelId;
    private List<LocalizableRemoteInput> remoteInputs;

    public static class Builder {
        private String buttonId;
        private String description;
        private List<Extender> extenders;
        private int iconId;
        private boolean isForegroundAction;
        private int labelId;
        private List<LocalizableRemoteInput> remoteInputs;

        public Builder(String buttonId) {
            this.labelId = 0;
            this.iconId = 0;
            this.isForegroundAction = true;
            this.buttonId = buttonId;
        }

        public Builder setLabel(int labelId) {
            this.labelId = labelId;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder setIcon(int iconId) {
            this.iconId = iconId;
            return this;
        }

        public Builder setPerformsInForeground(boolean isForegroundAction) {
            this.isForegroundAction = isForegroundAction;
            return this;
        }

        public NotificationActionButton build() {
            android.support.v4.app.NotificationCompat.Action.Builder builder = new android.support.v4.app.NotificationCompat.Action.Builder(this.iconId, null, null);
            if (this.extenders != null) {
                for (Extender extender : this.extenders) {
                    builder.extend(extender);
                }
            }
            Action action = builder.build();
            return new NotificationActionButton(action.icon, this.labelId, this.description, action.getExtras(), this.isForegroundAction, this.remoteInputs, null);
        }
    }

    private NotificationActionButton(String id, int iconId, int labelId, String description, Bundle extras, boolean isForegroundAction, List<LocalizableRemoteInput> remoteInputs) {
        this.id = id;
        this.labelId = labelId;
        this.iconId = iconId;
        this.extras = extras;
        this.description = description;
        this.isForegroundAction = isForegroundAction;
        this.remoteInputs = remoteInputs;
    }

    public String getDescription() {
        return this.description;
    }

    public String getId() {
        return this.id;
    }

    public int getLabel() {
        return this.labelId;
    }

    public int getIcon() {
        return this.iconId;
    }

    public boolean isForegroundAction() {
        return this.isForegroundAction;
    }

    Action createAndroidNotificationAction(Context context, String actionsPayload, PushMessage message, int notificationId) {
        PendingIntent actionPendingIntent;
        String label = this.labelId > 0 ? context.getString(this.labelId) : BuildConfig.VERSION_NAME;
        Intent intent = new Intent("com.urbanairship.ACTION_NOTIFICATION_BUTTON_OPENED_PROXY").addCategory(UUID.randomUUID().toString()).putExtra("com.urbanairship.push.EXTRA_PUSH_MESSAGE", message).putExtra("com.urbanairship.push.NOTIFICATION_ID", notificationId).putExtra("com.urbanairship.push.EXTRA_NOTIFICATION_BUTTON_ID", this.id).putExtra("com.urbanairship.push.EXTRA_NOTIFICATION_BUTTON_ACTIONS_PAYLOAD", actionsPayload).putExtra("com.urbanairship.push.EXTRA_NOTIFICATION_BUTTON_FOREGROUND", this.isForegroundAction).putExtra("com.urbanairship.push.EXTRA_NOTIFICATION_ACTION_BUTTON_DESCRIPTION", this.description == null ? label : this.description);
        if (this.isForegroundAction) {
            intent.setClass(context, CoreActivity.class);
            actionPendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        } else {
            intent.setClass(context, CoreReceiver.class);
            actionPendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        }
        android.support.v4.app.NotificationCompat.Action.Builder actionBuilder = new android.support.v4.app.NotificationCompat.Action.Builder(this.iconId, label, actionPendingIntent).addExtras(this.extras);
        if (this.remoteInputs != null) {
            for (LocalizableRemoteInput remoteInput : this.remoteInputs) {
                actionBuilder.addRemoteInput(remoteInput.createRemoteInput(context));
            }
        }
        return actionBuilder.build();
    }
}
