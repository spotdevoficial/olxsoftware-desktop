package com.urbanairship.push;

import com.amazon.device.messaging.ADM;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.amazon.ADMUtils;

class ADMRegistrar {
    public static boolean register() {
        if (ADMUtils.isADMSupported()) {
            new ADM(UAirship.getApplicationContext()).startRegister();
            return true;
        }
        Logger.error("ADM is not supported on this device.");
        return false;
    }
}
