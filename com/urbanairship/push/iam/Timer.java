package com.urbanairship.push.iam;

import android.os.Handler;
import android.os.SystemClock;

abstract class Timer {
    private long elapsedTimeMs;
    private final Handler handler;
    private boolean isStarted;
    private long remainingTimeMs;
    private long startTimeMs;
    private final Runnable trigger;

    /* renamed from: com.urbanairship.push.iam.Timer.1 */
    class C16711 implements Runnable {
        C16711() {
        }

        public void run() {
            if (Timer.this.isStarted) {
                Timer.this.stop();
                Timer.this.onFinish();
            }
        }
    }

    protected abstract void onFinish();

    Timer(long milliseconds) {
        this.handler = new Handler();
        this.trigger = new C16711();
        this.remainingTimeMs = milliseconds;
    }

    void start() {
        if (!this.isStarted) {
            this.isStarted = true;
            this.startTimeMs = SystemClock.elapsedRealtime();
            if (this.remainingTimeMs > 0) {
                this.handler.postDelayed(this.trigger, this.remainingTimeMs);
            } else {
                this.handler.post(this.trigger);
            }
        }
    }

    void stop() {
        if (this.isStarted) {
            this.elapsedTimeMs = SystemClock.elapsedRealtime() - this.startTimeMs;
            this.isStarted = false;
            this.handler.removeCallbacks(this.trigger);
            this.remainingTimeMs = Math.max(0, this.remainingTimeMs - (SystemClock.elapsedRealtime() - this.startTimeMs));
        }
    }

    long getRunTime() {
        if (this.isStarted) {
            return (this.elapsedTimeMs + SystemClock.elapsedRealtime()) - this.startTimeMs;
        }
        return this.elapsedTimeMs;
    }
}
