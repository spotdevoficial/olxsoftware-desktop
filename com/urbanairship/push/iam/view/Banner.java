package com.urbanairship.push.iam.view;

import com.urbanairship.push.notifications.NotificationActionButton;
import com.urbanairship.push.notifications.NotificationActionButtonGroup;

public interface Banner {

    public interface OnDismissClickListener {
        void onDismissClick();
    }

    public interface OnActionClickListener {
        void onActionClick(NotificationActionButton notificationActionButton);
    }

    void setNotificationActionButtonGroup(NotificationActionButtonGroup notificationActionButtonGroup);

    void setOnActionClickListener(OnActionClickListener onActionClickListener);

    void setOnDismissClickListener(OnDismissClickListener onDismissClickListener);

    void setPrimaryColor(int i);

    void setSecondaryColor(int i);

    void setText(CharSequence charSequence);
}
