package com.urbanairship.push.iam.view;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.support.v4.widget.ViewDragHelper.Callback;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.FrameLayout;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import com.urbanairship.Logger;

public class SwipeDismissViewLayout extends FrameLayout {
    private ViewDragHelper dragHelper;
    private Listener listener;
    private float minFlingVelocity;

    public interface Listener {
        void onDismissed(View view);

        void onDragStateChanged(View view, int i);
    }

    /* renamed from: com.urbanairship.push.iam.view.SwipeDismissViewLayout.1 */
    class C16741 implements OnPreDrawListener {
        final /* synthetic */ float val$yFraction;

        C16741(float f) {
            this.val$yFraction = f;
        }

        public boolean onPreDraw() {
            SwipeDismissViewLayout.this.setYFraction(this.val$yFraction);
            SwipeDismissViewLayout.this.getViewTreeObserver().removeOnPreDrawListener(this);
            return true;
        }
    }

    /* renamed from: com.urbanairship.push.iam.view.SwipeDismissViewLayout.2 */
    class C16752 implements OnPreDrawListener {
        final /* synthetic */ float val$xFraction;

        C16752(float f) {
            this.val$xFraction = f;
        }

        public boolean onPreDraw() {
            SwipeDismissViewLayout.this.setXFraction(this.val$xFraction);
            SwipeDismissViewLayout.this.getViewTreeObserver().removeOnPreDrawListener(this);
            return true;
        }
    }

    private class ViewDragCallback extends Callback {
        private View capturedView;
        private float dragPercent;
        private boolean isDismissed;
        private int startLeft;
        private int startTop;

        private ViewDragCallback() {
            this.dragPercent = 0.0f;
            this.isDismissed = false;
        }

        public boolean tryCaptureView(View view, int i) {
            return this.capturedView == null;
        }

        public int clampViewPositionHorizontal(View child, int left, int dx) {
            return left;
        }

        public int clampViewPositionVertical(View child, int top, int dy) {
            return child.getTop();
        }

        public void onViewCaptured(View view, int activePointerId) {
            this.capturedView = view;
            this.startTop = view.getTop();
            this.startLeft = view.getLeft();
            this.dragPercent = 0.0f;
            this.isDismissed = false;
        }

        @SuppressLint({"NewApi"})
        public void onViewPositionChanged(View view, int left, int top, int dx, int dy) {
            int range = SwipeDismissViewLayout.this.getWidth() / 2;
            int moved = Math.abs(left - this.startLeft);
            if (range > 0) {
                this.dragPercent = ((float) moved) / ((float) range);
            }
            if (VERSION.SDK_INT > 11) {
                view.setAlpha(MediaUploadState.IMAGE_PROGRESS_UPLOADED - this.dragPercent);
                SwipeDismissViewLayout.this.invalidate();
            }
        }

        public void onViewDragStateChanged(int state) {
            if (this.capturedView != null) {
                synchronized (this) {
                    if (SwipeDismissViewLayout.this.listener != null) {
                        SwipeDismissViewLayout.this.listener.onDragStateChanged(this.capturedView, state);
                    }
                    if (state == 0) {
                        if (this.isDismissed) {
                            if (SwipeDismissViewLayout.this.listener != null) {
                                SwipeDismissViewLayout.this.listener.onDismissed(this.capturedView);
                            }
                            SwipeDismissViewLayout.this.removeView(this.capturedView);
                        }
                        this.capturedView = null;
                    }
                }
            }
        }

        public void onViewReleased(View view, float xv, float yv) {
            boolean isSwipeLeft;
            boolean z = false;
            if (Math.abs(xv) <= SwipeDismissViewLayout.this.minFlingVelocity) {
                isSwipeLeft = this.startLeft < view.getLeft();
            } else if (xv > 0.0f) {
                isSwipeLeft = true;
            } else {
                isSwipeLeft = false;
            }
            if (this.dragPercent >= 0.75f || (Math.abs(xv) > SwipeDismissViewLayout.this.minFlingVelocity && this.dragPercent > MediaUploadState.IMAGE_PROGRESS_RESIZED)) {
                z = true;
            }
            this.isDismissed = z;
            if (this.isDismissed) {
                SwipeDismissViewLayout.this.dragHelper.settleCapturedViewAt(this.startLeft - (isSwipeLeft ? -view.getWidth() : view.getWidth()), this.startTop);
            } else {
                SwipeDismissViewLayout.this.dragHelper.settleCapturedViewAt(this.startLeft, this.startTop);
            }
            SwipeDismissViewLayout.this.invalidate();
        }
    }

    public SwipeDismissViewLayout(Context context) {
        super(context);
        init(context);
    }

    public SwipeDismissViewLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SwipeDismissViewLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    @TargetApi(21)
    public SwipeDismissViewLayout(Context context, AttributeSet attrs, int defStyle, int defResStyle) {
        super(context, attrs, defStyle, defResStyle);
        init(context);
    }

    private void init(Context context) {
        if (!isInEditMode()) {
            this.minFlingVelocity = (float) ViewConfiguration.get(context).getScaledMinimumFlingVelocity();
            this.dragHelper = ViewDragHelper.create(this, new ViewDragCallback());
        }
    }

    public void setMinFlingVelocity(float minFlingVelocity) {
        this.minFlingVelocity = minFlingVelocity;
    }

    public float getMinFlingVelocity() {
        return this.minFlingVelocity;
    }

    public void setListener(Listener listener) {
        synchronized (this) {
            this.listener = listener;
        }
    }

    @TargetApi(11)
    public float getYFraction() {
        int height = getHeight();
        if (height == 0) {
            return 0.0f;
        }
        return getTranslationY() / ((float) height);
    }

    @TargetApi(11)
    public void setYFraction(float yFraction) {
        if (getVisibility() == 0 && getHeight() == 0) {
            getViewTreeObserver().addOnPreDrawListener(new C16741(yFraction));
            return;
        }
        setTranslationY(((float) getHeight()) * yFraction);
    }

    @TargetApi(11)
    public float getXFraction() {
        int width = getWidth();
        if (width == 0) {
            return 0.0f;
        }
        return getTranslationX() / ((float) width);
    }

    @TargetApi(11)
    public void setXFraction(float xFraction) {
        if (getVisibility() == 0 && getHeight() == 0) {
            getViewTreeObserver().addOnPreDrawListener(new C16752(xFraction));
            return;
        }
        setTranslationX(((float) getWidth()) * xFraction);
    }

    public void computeScroll() {
        super.computeScroll();
        if (this.dragHelper != null && this.dragHelper.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.dragHelper.shouldInterceptTouchEvent(event) || super.onInterceptTouchEvent(event)) {
            Logger.error("onInterceptTouchEvent " + event);
            return true;
        }
        if (this.dragHelper.getViewDragState() == 0 && MotionEventCompat.getActionMasked(event) == 2 && this.dragHelper.checkTouchSlop(1)) {
            View child = this.dragHelper.findTopChildUnder((int) event.getX(), (int) event.getY());
            if (!(child == null || ViewCompat.canScrollHorizontally(child, this.dragHelper.getTouchSlop()))) {
                this.dragHelper.captureChildView(child, MotionEventCompat.getPointerId(event, 0));
                if (this.dragHelper.getViewDragState() != 1) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    public boolean onTouchEvent(MotionEvent event) {
        this.dragHelper.processTouchEvent(event);
        return this.dragHelper.getCapturedView() != null;
    }
}
