package com.urbanairship.push.iam;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.facebook.share.internal.ShareConstants;
import com.urbanairship.C1608R;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.actions.ActionRunRequest;
import com.urbanairship.actions.ActionValue;
import com.urbanairship.actions.Situation;
import com.urbanairship.push.iam.view.Banner;
import com.urbanairship.push.iam.view.Banner.OnActionClickListener;
import com.urbanairship.push.iam.view.Banner.OnDismissClickListener;
import com.urbanairship.push.iam.view.SwipeDismissViewLayout;
import com.urbanairship.push.notifications.NotificationActionButton;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@TargetApi(14)
public class InAppMessageFragment extends Fragment {
    private static Boolean isCardViewAvailable;
    private boolean dismissOnRecreate;
    private boolean isDismissed;
    private final List<Listener> listeners;
    private InAppMessage message;
    private Timer timer;

    /* renamed from: com.urbanairship.push.iam.InAppMessageFragment.1 */
    class C16581 extends Timer {
        C16581(long x0) {
            super(x0);
        }

        protected void onFinish() {
            InAppMessageFragment.this.dismiss(true);
            UAirship.shared().getAnalytics().addEvent(ResolutionEvent.createTimedOutResolutionEvent(InAppMessageFragment.this.message, InAppMessageFragment.this.timer.getRunTime()));
        }
    }

    /* renamed from: com.urbanairship.push.iam.InAppMessageFragment.2 */
    class C16592 implements com.urbanairship.push.iam.view.SwipeDismissViewLayout.Listener {
        C16592() {
        }

        public void onDismissed(View view) {
            InAppMessageFragment.this.dismiss(false);
            UAirship.shared().getAnalytics().addEvent(ResolutionEvent.createUserDismissedResolutionEvent(InAppMessageFragment.this.message, InAppMessageFragment.this.timer.getRunTime()));
        }

        public void onDragStateChanged(View view, int state) {
            switch (state) {
                case C1608R.styleable.MapAttrs_mapType /*0*/:
                    InAppMessageFragment.this.timer.start();
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    InAppMessageFragment.this.timer.stop();
                default:
            }
        }
    }

    /* renamed from: com.urbanairship.push.iam.InAppMessageFragment.3 */
    class C16603 implements OnClickListener {
        C16603() {
        }

        public void onClick(View v) {
            InAppMessageFragment.this.dismiss(true);
            InAppMessageFragment.this.runActions(InAppMessageFragment.this.message.getClickActionValues(), Situation.FOREGROUND_NOTIFICATION_ACTION_BUTTON);
            UAirship.shared().getAnalytics().addEvent(ResolutionEvent.createClickedResolutionEvent(InAppMessageFragment.this.message, InAppMessageFragment.this.timer.getRunTime()));
        }
    }

    /* renamed from: com.urbanairship.push.iam.InAppMessageFragment.4 */
    class C16614 implements OnDismissClickListener {
        C16614() {
        }

        public void onDismissClick() {
            InAppMessageFragment.this.dismiss(true);
            UAirship.shared().getAnalytics().addEvent(ResolutionEvent.createUserDismissedResolutionEvent(InAppMessageFragment.this.message, InAppMessageFragment.this.timer.getRunTime()));
        }
    }

    /* renamed from: com.urbanairship.push.iam.InAppMessageFragment.5 */
    class C16625 implements OnActionClickListener {
        C16625() {
        }

        public void onActionClick(NotificationActionButton actionButton) {
            Logger.info("In-app message button clicked: " + actionButton.getId());
            InAppMessageFragment.this.dismiss(true);
            InAppMessageFragment.this.runActions(InAppMessageFragment.this.message.getButtonActionValues(actionButton.getId()), actionButton.isForegroundAction() ? Situation.FOREGROUND_NOTIFICATION_ACTION_BUTTON : Situation.BACKGROUND_NOTIFICATION_ACTION_BUTTON);
            UAirship.shared().getAnalytics().addEvent(ResolutionEvent.createButtonClickedResolutionEvent(InAppMessageFragment.this.getActivity(), InAppMessageFragment.this.message, actionButton, InAppMessageFragment.this.timer.getRunTime()));
        }
    }

    public interface Listener {
        void onFinish(InAppMessageFragment inAppMessageFragment);

        void onPause(InAppMessageFragment inAppMessageFragment);

        void onResume(InAppMessageFragment inAppMessageFragment);
    }

    public InAppMessageFragment() {
        this.listeners = new ArrayList();
    }

    public static Bundle createArgs(InAppMessage message, int dismissAnimation) {
        Bundle args = new Bundle();
        args.putParcelable(ShareConstants.WEB_DIALOG_PARAM_MESSAGE, message);
        args.putInt("dismiss_animation", dismissAnimation);
        return args;
    }

    public final void addListener(Listener listener) {
        synchronized (this.listeners) {
            this.listeners.add(listener);
        }
    }

    public void onCreate(Bundle savedInstance) {
        boolean z;
        super.onCreate(savedInstance);
        setRetainInstance(true);
        this.message = (InAppMessage) getArguments().getParcelable(ShareConstants.WEB_DIALOG_PARAM_MESSAGE);
        if (savedInstance == null || !savedInstance.getBoolean("dismissed", false)) {
            z = false;
        } else {
            z = true;
        }
        this.isDismissed = z;
        this.timer = new C16581(this.message.getDuration() == null ? 15000 : this.message.getDuration().longValue());
        if (savedInstance != null && savedInstance.getBoolean("dismiss_on_recreate", false) != this.dismissOnRecreate) {
            Logger.debug("InAppMessageFragment - Dismissing on recreate.");
            dismiss(true);
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("dismissed", this.isDismissed);
        outState.putBoolean("dismiss_on_recreate", this.dismissOnRecreate);
    }

    public void onResume() {
        super.onResume();
        this.timer.start();
        synchronized (this.listeners) {
            Iterator i$ = new ArrayList(this.listeners).iterator();
            while (i$.hasNext()) {
                ((Listener) i$.next()).onResume(this);
            }
        }
    }

    public void onPause() {
        super.onPause();
        this.timer.stop();
        synchronized (this.listeners) {
            Iterator i$ = new ArrayList(this.listeners).iterator();
            while (i$.hasNext()) {
                ((Listener) i$.next()).onPause(this);
            }
        }
    }

    @SuppressLint({"NewAPI"})
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (this.message == null || this.message.getAlert() == null) {
            dismiss(false);
            return null;
        }
        SwipeDismissViewLayout view = (SwipeDismissViewLayout) inflater.inflate(checkCardViewDependencyAvailable() ? C1608R.layout.ua_fragment_iam_card : C1608R.layout.ua_fragment_iam, container, false);
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        layoutParams.gravity = this.message.getPosition() == 1 ? 48 : 80;
        view.setLayoutParams(layoutParams);
        view.setListener(new C16592());
        FrameLayout bannerView = (FrameLayout) view.findViewById(C1608R.id.in_app_message);
        if (this.message.getClickActionValues().isEmpty()) {
            bannerView.setClickable(false);
            bannerView.setForeground(null);
        } else {
            bannerView.setOnClickListener(new C16603());
        }
        Banner banner = (Banner) bannerView;
        banner.setOnDismissClickListener(new C16614());
        banner.setOnActionClickListener(new C16625());
        if (this.message.getPrimaryColor() != null) {
            banner.setPrimaryColor(this.message.getPrimaryColor().intValue());
        }
        if (this.message.getSecondaryColor() != null) {
            banner.setSecondaryColor(this.message.getSecondaryColor().intValue());
        }
        banner.setText(this.message.getAlert());
        banner.setNotificationActionButtonGroup(UAirship.shared().getPushManager().getNotificationActionGroup(this.message.getButtonGroupId()));
        return view;
    }

    public void dismiss(boolean animate) {
        this.timer.stop();
        if (!this.isDismissed) {
            synchronized (this.listeners) {
                Iterator i$ = new ArrayList(this.listeners).iterator();
                while (i$.hasNext()) {
                    ((Listener) i$.next()).onFinish(this);
                }
            }
            this.isDismissed = true;
            if (getActivity() != null) {
                getActivity().getFragmentManager().beginTransaction().setCustomAnimations(0, animate ? getArguments().getInt("dismiss_animation", 0) : 0).remove(this).commit();
            }
        }
    }

    public boolean isDismissed() {
        return this.isDismissed;
    }

    public InAppMessage getMessage() {
        return this.message;
    }

    void setDismissOnRecreate(boolean dismissOnRecreate) {
        this.dismissOnRecreate = dismissOnRecreate;
    }

    private static boolean checkCardViewDependencyAvailable() {
        if (isCardViewAvailable == null) {
            try {
                Class.forName("android.support.v7.widget.CardView");
                isCardViewAvailable = Boolean.valueOf(true);
            } catch (ClassNotFoundException e) {
                isCardViewAvailable = Boolean.valueOf(false);
            }
        }
        return isCardViewAvailable.booleanValue();
    }

    private void runActions(Map<String, ActionValue> actionValueMap, Situation situation) {
        if (actionValueMap != null) {
            for (Entry<String, ActionValue> entry : actionValueMap.entrySet()) {
                ActionRunRequest.createRequest((String) entry.getKey()).setValue((ActionValue) entry.getValue()).setSituation(situation).run();
            }
        }
    }
}
