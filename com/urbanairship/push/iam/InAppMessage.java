package com.urbanairship.push.iam;

import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.facebook.internal.ServerProtocol;
import com.facebook.share.internal.ShareConstants;
import com.urbanairship.Logger;
import com.urbanairship.actions.ActionValue;
import com.urbanairship.json.JsonException;
import com.urbanairship.json.JsonMap;
import com.urbanairship.json.JsonSerializable;
import com.urbanairship.json.JsonValue;
import com.urbanairship.util.DateUtils;
import com.urbanairship.util.UAStringUtil;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

public class InAppMessage implements Parcelable, JsonSerializable {
    public static final Creator<InAppMessage> CREATOR;
    private String alert;
    private Map<String, Map<String, ActionValue>> buttonActionValues;
    private String buttonGroupId;
    private Map<String, ActionValue> clickActionValues;
    private Long durationMilliseconds;
    private long expiryMS;
    private JsonMap extras;
    private String id;
    private int position;
    private Integer primaryColor;
    private Integer secondaryColor;

    /* renamed from: com.urbanairship.push.iam.InAppMessage.1 */
    static class C16571 implements Creator<InAppMessage> {
        C16571() {
        }

        public InAppMessage createFromParcel(Parcel in) {
            InAppMessage inAppMessage = new InAppMessage();
            inAppMessage.id = in.readString();
            inAppMessage.alert = in.readString();
            inAppMessage.expiryMS = in.readLong();
            inAppMessage.position = in.readInt();
            if (in.readByte() == (byte) 1) {
                inAppMessage.durationMilliseconds = Long.valueOf(in.readLong());
            }
            if (in.readByte() == (byte) 1) {
                inAppMessage.primaryColor = Integer.valueOf(in.readInt());
            }
            if (in.readByte() == (byte) 1) {
                inAppMessage.secondaryColor = Integer.valueOf(in.readInt());
            }
            try {
                inAppMessage.extras = JsonValue.parseString(in.readString()).getMap();
            } catch (JsonException e) {
                Logger.error("InAppMessage - unable to parse extras from parcel.");
                inAppMessage.extras = new JsonMap(null);
            }
            inAppMessage.buttonGroupId = in.readString();
            inAppMessage.buttonActionValues = new HashMap();
            in.readMap(inAppMessage.buttonActionValues, ActionValue.class.getClassLoader());
            inAppMessage.clickActionValues = new HashMap();
            in.readMap(inAppMessage.clickActionValues, ActionValue.class.getClassLoader());
            return inAppMessage;
        }

        public InAppMessage[] newArray(int size) {
            return new InAppMessage[size];
        }
    }

    public static class Builder {
        private String alert;
        private Map<String, Map<String, ActionValue>> buttonActionValues;
        private String buttonGroupId;
        private Map<String, ActionValue> clickActionValues;
        private Long durationMilliseconds;
        private Long expiryMS;
        private JsonMap extras;
        private String id;
        private int position;
        private Integer primaryColor;
        private Integer secondaryColor;

        public Builder() {
            this.buttonActionValues = new HashMap();
            this.position = 0;
        }

        public Builder(InAppMessage message) {
            this.buttonActionValues = new HashMap();
            this.position = 0;
            this.id = message.id;
            this.buttonGroupId = message.buttonGroupId;
            this.alert = message.alert;
            this.expiryMS = Long.valueOf(message.expiryMS);
            this.durationMilliseconds = message.durationMilliseconds;
            this.position = message.position;
            this.clickActionValues = new HashMap(message.clickActionValues);
            this.buttonActionValues = new HashMap(message.buttonActionValues);
            this.extras = message.extras;
            this.primaryColor = message.primaryColor;
            this.secondaryColor = message.secondaryColor;
        }

        public Builder setId(String id) {
            this.id = id;
            return this;
        }

        public Builder setExpiry(Long milliseconds) {
            this.expiryMS = milliseconds;
            return this;
        }

        public Builder setExtras(JsonMap extras) {
            this.extras = extras;
            return this;
        }

        public Builder setClickActionValues(Map<String, ActionValue> actionValues) {
            if (actionValues == null) {
                this.clickActionValues = null;
            } else {
                this.clickActionValues = new HashMap(actionValues);
            }
            return this;
        }

        public Builder setButtonActionValues(String buttonId, Map<String, ActionValue> actionValues) {
            if (actionValues == null) {
                this.buttonActionValues.remove(buttonId);
            } else {
                this.buttonActionValues.put(buttonId, new HashMap(actionValues));
            }
            return this;
        }

        public Builder setButtonGroupId(String buttonGroupId) {
            this.buttonGroupId = buttonGroupId;
            return this;
        }

        public Builder setAlert(String alert) {
            this.alert = alert;
            return this;
        }

        public Builder setDuration(Long milliseconds) {
            if (milliseconds == null || milliseconds.longValue() > 0) {
                this.durationMilliseconds = milliseconds;
                return this;
            }
            throw new IllegalArgumentException("Duration must be greater than 0 milliseconds");
        }

        public Builder setPosition(int position) {
            if (position == 1 || position == 0) {
                this.position = position;
                return this;
            }
            throw new IllegalArgumentException("Position must be either InAppMessage.POSITION_BOTTOM or InAppMessage.POSITION_TOP.");
        }

        public Builder setPrimaryColor(Integer color) {
            this.primaryColor = color;
            return this;
        }

        public Builder setSecondaryColor(Integer color) {
            this.secondaryColor = color;
            return this;
        }

        public InAppMessage create() {
            InAppMessage message = new InAppMessage();
            message.expiryMS = this.expiryMS == null ? System.currentTimeMillis() + 2592000000L : this.expiryMS.longValue();
            message.id = this.id;
            message.extras = this.extras == null ? new JsonMap(null) : this.extras;
            message.alert = this.alert;
            message.durationMilliseconds = this.durationMilliseconds;
            message.buttonGroupId = this.buttonGroupId;
            message.buttonActionValues = this.buttonActionValues == null ? new HashMap() : this.buttonActionValues;
            message.clickActionValues = this.clickActionValues == null ? new HashMap() : this.clickActionValues;
            message.position = this.position;
            message.primaryColor = this.primaryColor;
            message.secondaryColor = this.secondaryColor;
            return message;
        }
    }

    InAppMessage() {
    }

    public long getExpiry() {
        return this.expiryMS;
    }

    public boolean isExpired() {
        return System.currentTimeMillis() > this.expiryMS;
    }

    public String getId() {
        return this.id;
    }

    public String getAlert() {
        return this.alert;
    }

    public Map<String, ActionValue> getClickActionValues() {
        return Collections.unmodifiableMap(this.clickActionValues);
    }

    public Map<String, ActionValue> getButtonActionValues(String buttonId) {
        if (this.buttonActionValues.containsKey(buttonId)) {
            return Collections.unmodifiableMap((Map) this.buttonActionValues.get(buttonId));
        }
        return null;
    }

    public String getButtonGroupId() {
        return this.buttonGroupId;
    }

    public Long getDuration() {
        return this.durationMilliseconds;
    }

    public int getPosition() {
        return this.position;
    }

    public Integer getPrimaryColor() {
        return this.primaryColor;
    }

    public Integer getSecondaryColor() {
        return this.secondaryColor;
    }

    static {
        CREATOR = new C16571();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.alert);
        dest.writeLong(this.expiryMS);
        dest.writeInt(this.position);
        if (this.durationMilliseconds == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(this.durationMilliseconds.longValue());
        }
        if (this.primaryColor == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(this.primaryColor.intValue());
        }
        if (this.secondaryColor == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(this.secondaryColor.intValue());
        }
        dest.writeString(this.extras.toString());
        dest.writeString(this.buttonGroupId);
        dest.writeMap(this.buttonActionValues);
        dest.writeMap(this.clickActionValues);
    }

    public int describeContents() {
        return 0;
    }

    public static InAppMessage parseJson(String json) throws JsonException {
        JsonMap inAppJson = JsonValue.parseString(json).getMap();
        if (inAppJson == null) {
            return null;
        }
        JsonMap displayJson = inAppJson.opt(ServerProtocol.DIALOG_PARAM_DISPLAY).getMap();
        JsonMap actionsJson = inAppJson.opt("actions").getMap();
        if (displayJson != null) {
            if ("banner".equals(displayJson.opt("type").getString())) {
                Builder builder = new Builder();
                builder.setId(inAppJson.opt(ShareConstants.WEB_DIALOG_PARAM_ID).getString()).setExtras(inAppJson.opt("extra").getMap()).setAlert(displayJson.opt("alert").getString()).setPrimaryColor(parseColor(displayJson.opt("primary_color").getString())).setSecondaryColor(parseColor(displayJson.opt("secondary_color").getString()));
                long duration;
                if (displayJson.containsKey("duration_ms")) {
                    duration = displayJson.get("duration_ms").getLong(0);
                    if (duration > 0) {
                        builder.setDuration(Long.valueOf(duration));
                    }
                } else {
                    duration = displayJson.opt("duration").getLong(0);
                    if (duration > 0) {
                        builder.setDuration(Long.valueOf(TimeUnit.SECONDS.toMillis(duration)));
                    }
                }
                if (inAppJson.containsKey("expiry_ms")) {
                    builder.setExpiry(Long.valueOf(inAppJson.get("expiry_ms").getLong(System.currentTimeMillis() + 2592000000L)));
                } else if (inAppJson.containsKey("expiry")) {
                    builder.setExpiry(Long.valueOf(DateUtils.parseIso8601(inAppJson.opt("expiry").getString(), System.currentTimeMillis() + 2592000000L)));
                }
                if ("top".equalsIgnoreCase(displayJson.opt("position").getString())) {
                    builder.setPosition(1);
                } else {
                    builder.setPosition(0);
                }
                if (actionsJson != null) {
                    JsonMap clickActionsJson = actionsJson.opt("on_click").getMap();
                    if (clickActionsJson != null) {
                        Map<String, ActionValue> clickActions = new HashMap();
                        for (Entry<String, JsonValue> entry : clickActionsJson.entrySet()) {
                            clickActions.put(entry.getKey(), new ActionValue((JsonValue) entry.getValue()));
                        }
                        builder.setClickActionValues(clickActions);
                    }
                    builder.setButtonGroupId(actionsJson.opt("button_group").getString());
                    JsonMap buttonActionsJson = actionsJson.opt("button_actions").getMap();
                    if (buttonActionsJson != null) {
                        for (Entry<String, JsonValue> entry2 : buttonActionsJson.entrySet()) {
                            String buttonId = (String) entry2.getKey();
                            JsonMap actionJson = buttonActionsJson.opt(buttonId).getMap();
                            Map<String, ActionValue> actions = new HashMap();
                            for (Entry<String, JsonValue> buttonEntry : actionJson.entrySet()) {
                                actions.put(buttonEntry.getKey(), new ActionValue((JsonValue) buttonEntry.getValue()));
                            }
                            builder.setButtonActionValues(buttonId, actions);
                        }
                    }
                }
                return builder.create();
            }
        }
        Logger.error("InAppMessage - Unable to parse json: " + json);
        return null;
    }

    private static Integer parseColor(String colorString) {
        Integer num = null;
        if (!UAStringUtil.isEmpty(colorString)) {
            try {
                num = Integer.valueOf(Color.parseColor(colorString));
            } catch (IllegalArgumentException e) {
                Logger.warn("InAppMessage - Unable to parse color: " + colorString, e);
            }
        }
        return num;
    }

    public JsonValue toJsonValue() {
        Map<String, Object> inApp = new HashMap();
        inApp.put(ShareConstants.WEB_DIALOG_PARAM_ID, this.id);
        inApp.put("expiry_ms", Long.valueOf(this.expiryMS));
        inApp.put("extra", this.extras);
        Map<String, Object> display = new HashMap();
        inApp.put(ServerProtocol.DIALOG_PARAM_DISPLAY, display);
        display.put("type", "banner");
        display.put("alert", this.alert);
        display.put("position", this.position == 1 ? "top" : "bottom");
        if (this.durationMilliseconds != null) {
            display.put("duration_ms", this.durationMilliseconds);
        }
        if (this.primaryColor != null) {
            display.put("primary_color", String.format(Locale.US, "#%06X", new Object[]{Integer.valueOf(this.primaryColor.intValue() & 16777215)}));
        }
        if (this.secondaryColor != null) {
            display.put("secondary_color", String.format(Locale.US, "#%06X", new Object[]{Integer.valueOf(this.secondaryColor.intValue() & 16777215)}));
        }
        Map<String, Object> actions = new HashMap();
        inApp.put("actions", actions);
        actions.put("on_click", this.clickActionValues);
        actions.put("button_group", this.buttonGroupId);
        actions.put("button_actions", this.buttonActionValues);
        return JsonValue.wrap(inApp, null);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r9) {
        /*
        r8 = this;
        r1 = 1;
        r2 = 0;
        if (r8 != r9) goto L_0x0005;
    L_0x0004:
        return r1;
    L_0x0005:
        r3 = r9 instanceof com.urbanairship.push.iam.InAppMessage;
        if (r3 != 0) goto L_0x000b;
    L_0x0009:
        r1 = r2;
        goto L_0x0004;
    L_0x000b:
        r0 = r9;
        r0 = (com.urbanairship.push.iam.InAppMessage) r0;
        r3 = r8.id;
        if (r3 != 0) goto L_0x0066;
    L_0x0012:
        r3 = r0.id;
        if (r3 != 0) goto L_0x0064;
    L_0x0016:
        r3 = r8.alert;
        if (r3 != 0) goto L_0x0071;
    L_0x001a:
        r3 = r0.alert;
        if (r3 != 0) goto L_0x0064;
    L_0x001e:
        r3 = r8.buttonGroupId;
        if (r3 != 0) goto L_0x007c;
    L_0x0022:
        r3 = r0.buttonGroupId;
        if (r3 != 0) goto L_0x0064;
    L_0x0026:
        r3 = r8.extras;
        if (r3 != 0) goto L_0x0087;
    L_0x002a:
        r3 = r0.extras;
        if (r3 != 0) goto L_0x0064;
    L_0x002e:
        r3 = r8.clickActionValues;
        if (r3 != 0) goto L_0x0092;
    L_0x0032:
        r3 = r0.clickActionValues;
        if (r3 != 0) goto L_0x0064;
    L_0x0036:
        r3 = r8.buttonActionValues;
        if (r3 != 0) goto L_0x009d;
    L_0x003a:
        r3 = r0.buttonActionValues;
        if (r3 != 0) goto L_0x0064;
    L_0x003e:
        r3 = r8.primaryColor;
        if (r3 != 0) goto L_0x00a8;
    L_0x0042:
        r3 = r0.primaryColor;
        if (r3 != 0) goto L_0x0064;
    L_0x0046:
        r3 = r8.secondaryColor;
        if (r3 != 0) goto L_0x00b3;
    L_0x004a:
        r3 = r0.secondaryColor;
        if (r3 != 0) goto L_0x0064;
    L_0x004e:
        r3 = r8.durationMilliseconds;
        if (r3 != 0) goto L_0x00be;
    L_0x0052:
        r3 = r0.durationMilliseconds;
        if (r3 != 0) goto L_0x0064;
    L_0x0056:
        r3 = r8.position;
        r4 = r0.position;
        if (r3 != r4) goto L_0x0064;
    L_0x005c:
        r4 = r8.expiryMS;
        r6 = r0.expiryMS;
        r3 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1));
        if (r3 == 0) goto L_0x0004;
    L_0x0064:
        r1 = r2;
        goto L_0x0004;
    L_0x0066:
        r3 = r8.id;
        r4 = r0.id;
        r3 = r3.equals(r4);
        if (r3 == 0) goto L_0x0064;
    L_0x0070:
        goto L_0x0016;
    L_0x0071:
        r3 = r8.alert;
        r4 = r0.alert;
        r3 = r3.equals(r4);
        if (r3 == 0) goto L_0x0064;
    L_0x007b:
        goto L_0x001e;
    L_0x007c:
        r3 = r8.buttonGroupId;
        r4 = r0.buttonGroupId;
        r3 = r3.equals(r4);
        if (r3 == 0) goto L_0x0064;
    L_0x0086:
        goto L_0x0026;
    L_0x0087:
        r3 = r8.extras;
        r4 = r0.extras;
        r3 = r3.equals(r4);
        if (r3 == 0) goto L_0x0064;
    L_0x0091:
        goto L_0x002e;
    L_0x0092:
        r3 = r8.clickActionValues;
        r4 = r0.clickActionValues;
        r3 = r3.equals(r4);
        if (r3 == 0) goto L_0x0064;
    L_0x009c:
        goto L_0x0036;
    L_0x009d:
        r3 = r8.buttonActionValues;
        r4 = r0.buttonActionValues;
        r3 = r3.equals(r4);
        if (r3 == 0) goto L_0x0064;
    L_0x00a7:
        goto L_0x003e;
    L_0x00a8:
        r3 = r8.primaryColor;
        r4 = r0.primaryColor;
        r3 = r3.equals(r4);
        if (r3 == 0) goto L_0x0064;
    L_0x00b2:
        goto L_0x0046;
    L_0x00b3:
        r3 = r8.secondaryColor;
        r4 = r0.secondaryColor;
        r3 = r3.equals(r4);
        if (r3 == 0) goto L_0x0064;
    L_0x00bd:
        goto L_0x004e;
    L_0x00be:
        r3 = r8.durationMilliseconds;
        r4 = r0.durationMilliseconds;
        r3 = r3.equals(r4);
        if (r3 == 0) goto L_0x0064;
    L_0x00c8:
        goto L_0x0056;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.urbanairship.push.iam.InAppMessage.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((((((((((((((((this.id == null ? 0 : this.id.hashCode()) + 403) * 31) + (this.alert == null ? 0 : this.alert.hashCode())) * 31) + (this.buttonGroupId == null ? 0 : this.buttonGroupId.hashCode())) * 31) + (this.extras == null ? 0 : this.extras.hashCode())) * 31) + (this.clickActionValues == null ? 0 : this.clickActionValues.hashCode())) * 31) + (this.buttonActionValues == null ? 0 : this.buttonActionValues.hashCode())) * 31) + (this.secondaryColor == null ? 0 : this.secondaryColor.intValue())) * 31) + (this.primaryColor == null ? 0 : this.primaryColor.intValue())) * 31;
        if (this.durationMilliseconds != null) {
            i = Long.valueOf(this.durationMilliseconds.longValue()).hashCode();
        }
        return ((((hashCode + i) * 31) + this.position) * 31) + Long.valueOf(this.expiryMS).hashCode();
    }
}
