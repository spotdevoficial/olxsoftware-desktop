package com.urbanairship.push;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.BroadcastReceiver.PendingResult;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import com.urbanairship.Autopilot;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.UAirship.OnReadyCallback;
import com.urbanairship.util.UAStringUtil;

public class GCMPushReceiver extends BroadcastReceiver {

    /* renamed from: com.urbanairship.push.GCMPushReceiver.1 */
    class C16541 implements OnReadyCallback {
        final /* synthetic */ Context val$context;
        final /* synthetic */ Intent val$intent;
        final /* synthetic */ PendingResult val$pendingResult;

        C16541(Context context, Intent intent, PendingResult pendingResult) {
            this.val$context = context;
            this.val$intent = intent;
            this.val$pendingResult = pendingResult;
        }

        public void onAirshipReady(UAirship airship) {
            GCMPushReceiver.this.handleGCMReceived(airship, this.val$context, this.val$intent);
            this.val$pendingResult.finish();
        }
    }

    @SuppressLint({"NewApi"})
    public void onReceive(Context context, Intent intent) {
        Autopilot.automaticTakeOff(context);
        if (intent != null && intent.getAction() != null) {
            Logger.verbose("GCMPushReceiver - Received intent: " + intent.getAction());
            if (!"com.google.android.c2dm.intent.RECEIVE".equals(intent.getAction())) {
                return;
            }
            if (VERSION.SDK_INT >= 11) {
                PendingResult pendingResult = goAsync();
                if (isOrderedBroadcast()) {
                    pendingResult.setResultCode(-1);
                }
                UAirship.shared(new C16541(context, intent, pendingResult));
                return;
            }
            handleGCMReceived(UAirship.shared(), context, intent);
            if (isOrderedBroadcast()) {
                setResultCode(-1);
            }
        }
    }

    private void handleGCMReceived(UAirship airship, Context context, Intent intent) {
        if (airship.getPlatformType() != 2) {
            Logger.error("GCMPushReceiver - Received intent from invalid transport acting as GCM.");
        } else if (UAStringUtil.isEmpty(airship.getPushManager().getGcmId())) {
            Logger.error("GCMPushReceiver - Received intent from GCM without registering.");
        } else {
            String sender = intent.getStringExtra("from");
            if (sender != null && !sender.equals(airship.getAirshipConfigOptions().gcmSender)) {
                Logger.info("Ignoring GCM message from sender: " + sender);
            } else if ("deleted_messages".equals(intent.getStringExtra("message_type"))) {
                Logger.info("GCM deleted " + intent.getStringExtra("total_deleted") + " pending messages.");
            } else {
                Logger.debug("GCMPushReceiver - Received push: " + intent);
                PushService.startServiceWithWakeLock(context, new Intent("com.urbanairship.push.ACTION_PUSH_RECEIVED").putExtras(intent.getExtras()));
            }
        }
    }
}
