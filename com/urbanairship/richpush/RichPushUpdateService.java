package com.urbanairship.richpush;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.ResultReceiver;
import com.urbanairship.Autopilot;
import com.urbanairship.C1608R;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.util.UAStringUtil;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RichPushUpdateService extends IntentService {
    RichPushResolver resolver;
    UserAPIClient userClient;

    public RichPushUpdateService() {
        super("RichPushUpdateService");
    }

    public void onCreate() {
        super.onCreate();
        Autopilot.automaticTakeOff(getApplicationContext());
        this.userClient = new UserAPIClient();
        this.resolver = new RichPushResolver(getApplicationContext());
    }

    protected void onHandleIntent(Intent intent) {
        if (intent != null && intent.getAction() != null) {
            Logger.verbose("RichPushUpdateService - Received intent: " + intent.getAction());
            ResultReceiver receiver = (ResultReceiver) intent.getParcelableExtra("com.urbanairship.richpush.RESULT_RECEIVER");
            if ("com.urbanairship.richpush.MESSAGES_UPDATE".equals(intent.getAction())) {
                if (RichPushUser.isCreated()) {
                    messagesUpdate(receiver);
                    return;
                }
                Logger.debug("RichPushUpdateService - User has not been created, canceling messages update");
                respond(receiver, false);
            } else if ("com.urbanairship.richpush.USER_UPDATE".equals(intent.getAction())) {
                userUpdate(receiver);
            }
        }
    }

    private void respond(ResultReceiver receiver, boolean status, Bundle bundle) {
        if (receiver != null) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            if (status) {
                receiver.send(0, bundle);
            } else {
                receiver.send(1, bundle);
            }
        }
    }

    private void respond(ResultReceiver receiver, boolean status) {
        respond(receiver, status, null);
    }

    private void messagesUpdate(ResultReceiver receiver) {
        respond(receiver, updateMessages());
        handleReadMessages();
        handleDeletedMessages();
    }

    private void userUpdate(ResultReceiver receiver) {
        boolean success;
        if (RichPushUser.isCreated()) {
            success = updateUser();
        } else {
            success = createUser();
        }
        respond(receiver, success);
    }

    private boolean createUser() {
        try {
            JSONObject payload = createNewUserPayload();
            Logger.info("Creating Rich Push user.");
            UserResponse response = this.userClient.createUser(payload);
            if (response == null) {
                return false;
            }
            if (getUser().setUser(response.getUserId(), response.getUserToken())) {
                Logger.info("Rich Push user created.");
                getUser().setLastUpdateTime(System.currentTimeMillis());
                return true;
            }
            Logger.warn("Rich Push user creation failed.");
            return false;
        } catch (JSONException e) {
            Logger.error("Exception constructing JSON data when creating user.", e);
            return false;
        }
    }

    private boolean updateUser() {
        if (UAStringUtil.isEmpty(UAirship.shared().getPushManager().getChannelId())) {
            Logger.debug("RichPushUpdateService - No Channel. Skipping Rich Push user update.");
            return false;
        }
        Logger.info("Updating Rich Push user.");
        try {
            if (this.userClient.updateUser(createUpdateUserPayload(), getUser().getId(), getUser().getPassword())) {
                Logger.info("Rich Push user updated.");
                getUser().setLastUpdateTime(System.currentTimeMillis());
                return true;
            }
            getUser().setLastUpdateTime(0);
            return false;
        } catch (JSONException e) {
            Logger.error("Exception constructing JSON data when updating user.", e);
            return false;
        }
    }

    private JSONObject createNewUserPayload() throws JSONException {
        JSONObject payload = new JSONObject();
        String channelId = UAirship.shared().getPushManager().getChannelId();
        if (!UAStringUtil.isEmpty(channelId)) {
            JSONArray array = new JSONArray();
            array.put(channelId);
            payload.putOpt(getPayloadChannelsKey(), array);
        }
        return payload;
    }

    private JSONObject createUpdateUserPayload() throws JSONException {
        JSONObject payload = new JSONObject();
        JSONObject channelPayload = new JSONObject();
        JSONArray channels = new JSONArray();
        channels.put(UAirship.shared().getPushManager().getChannelId());
        channelPayload.put("add", channels);
        payload.put(getPayloadChannelsKey(), channelPayload);
        return payload;
    }

    private String getPayloadChannelsKey() {
        switch (UAirship.shared().getPlatformType()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "amazon_channels";
            default:
                return "android_channels";
        }
    }

    private void handleDeletedMessages() {
        Set<String> idsToDelete = getMessageIdsFromCursor(this.resolver.getDeletedMessages());
        if (idsToDelete != null && idsToDelete.size() > 0) {
            Logger.verbose("RichPushUpdateService - Found " + idsToDelete.size() + " messages to delete.");
            if (deleteMessagesOnServer(idsToDelete)) {
                this.resolver.deleteMessages(idsToDelete);
            }
        }
    }

    private void handleReadMessages() {
        Set<String> idsToUpdate = getMessageIdsFromCursor(this.resolver.getReadUpdatedMessages());
        if (idsToUpdate != null && idsToUpdate.size() > 0) {
            Logger.verbose("RichPushUpdateService - Found " + idsToUpdate.size() + " messages to mark read.");
            if (markMessagesReadOnServer(idsToUpdate)) {
                ContentValues values = new ContentValues();
                values.put("unread_orig", Integer.valueOf(0));
                this.resolver.updateMessages(idsToUpdate, values);
            }
        }
    }

    private boolean updateMessages() {
        RichPushUser user = getUser();
        MessageListResponse response = this.userClient.getMessages(user.getId(), user.getPassword(), user.getLastMessageRefreshTime());
        Logger.info("Refreshing inbox messages.");
        if (response == null) {
            Logger.debug("RichPushUpdateService - Inbox message list request failed.");
            return false;
        }
        Logger.debug("RichPushUpdateService - Inbox message list request received: " + response.getStatus());
        switch (response.getStatus()) {
            case 200:
                ContentValues[] serverMessages = response.getServerMessages();
                if (serverMessages == null) {
                    Logger.info("Inbox message list is empty.");
                } else {
                    Logger.info("Received " + serverMessages.length + " inbox messages.");
                    updateInbox(serverMessages);
                    user.setLastMessageRefreshTime(response.getLastModifiedTimeMS());
                }
                return true;
            case 304:
                Logger.info("Inbox messages already up-to-date. ");
                return true;
            default:
                Logger.info("Unable to update inbox messages.");
                return false;
        }
    }

    private boolean deleteMessagesOnServer(Set<String> deletedIds) {
        return this.userClient.deleteMessages(buildMessagesPayload("delete", deletedIds), getUser().getId(), getUser().getPassword());
    }

    private boolean markMessagesReadOnServer(Set<String> readIds) {
        return this.userClient.markMessagesRead(buildMessagesPayload("mark_as_read", readIds), getUser().getId(), getUser().getPassword());
    }

    private JSONObject buildMessagesPayload(String root, Set<String> ids) {
        try {
            JSONObject payload = new JSONObject();
            payload.put(root, new JSONArray());
            String userId = getUser().getId();
            for (String id : ids) {
                payload.accumulate(root, formatUrl("api/user/%s/messages/message/%s/", new String[]{userId, id}));
            }
            Logger.verbose(payload.toString());
            return payload;
        } catch (JSONException e) {
            Logger.info(e.getMessage());
            return null;
        }
    }

    private String formatUrl(String urlFormat, String[] urlParams) {
        return getHostUrl() + String.format(urlFormat, (Object[]) urlParams);
    }

    private RichPushUser getUser() {
        return UAirship.shared().getRichPushManager().getRichPushUser();
    }

    private String getHostUrl() {
        return UAirship.shared().getAirshipConfigOptions().hostURL;
    }

    private void updateInbox(ContentValues[] serverMessages) {
        List<ContentValues> messagesToInsert = new ArrayList();
        HashSet<String> serverMessageIds = new HashSet();
        for (ContentValues message : serverMessages) {
            String messageId = message.getAsString("message_id");
            serverMessageIds.add(messageId);
            if (this.resolver.updateMessage(messageId, message) != 1) {
                messagesToInsert.add(message);
            }
        }
        if (messagesToInsert.size() > 0) {
            ContentValues[] messageArray = new ContentValues[messagesToInsert.size()];
            messagesToInsert.toArray(messageArray);
            this.resolver.insertMessages(messageArray);
        }
        Set<String> allIds = getMessageIdsFromCursor(this.resolver.getAllMessages());
        if (allIds != null) {
            allIds.removeAll(serverMessageIds);
            UAirship.shared().getRichPushManager().getRichPushInbox().deleteMessages(allIds);
        }
        UAirship.shared().getRichPushManager().getRichPushInbox().updateCache();
    }

    private Set<String> getMessageIdsFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        Set<String> ids = new HashSet(cursor.getCount());
        int messageIdIndex = -1;
        while (cursor.moveToNext()) {
            if (messageIdIndex == -1) {
                messageIdIndex = cursor.getColumnIndex("message_id");
            }
            ids.add(cursor.getString(messageIdIndex));
        }
        cursor.close();
        return ids;
    }
}
