package com.urbanairship.richpush;

import android.database.Cursor;
import android.os.Bundle;
import com.facebook.share.internal.ShareConstants;
import com.urbanairship.Logger;
import com.urbanairship.util.DateUtils;
import com.urbanairship.util.UAStringUtil;
import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public class RichPushMessage implements Comparable<RichPushMessage> {
    boolean deleted;
    Long expirationMS;
    Bundle extras;
    String messageBodyUrl;
    String messageId;
    String messageReadUrl;
    String messageUrl;
    JSONObject rawMessageJSON;
    long sentMS;
    String title;
    boolean unreadClient;
    boolean unreadOrigin;

    RichPushMessage(String messageId) {
        this.deleted = false;
        this.messageId = messageId;
    }

    static RichPushMessage messageFromCursor(Cursor cursor) throws JSONException {
        boolean z;
        boolean z2 = true;
        RichPushMessage message = new RichPushMessage(cursor.getString(cursor.getColumnIndex("message_id")));
        message.messageUrl = cursor.getString(cursor.getColumnIndex("message_url"));
        message.messageBodyUrl = cursor.getString(cursor.getColumnIndex("message_body_url"));
        message.messageReadUrl = cursor.getString(cursor.getColumnIndex("message_read_url"));
        if (cursor.getInt(cursor.getColumnIndex("unread")) == 1) {
            z = true;
        } else {
            z = false;
        }
        message.unreadClient = z;
        if (cursor.getInt(cursor.getColumnIndex("unread_orig")) == 1) {
            z = true;
        } else {
            z = false;
        }
        message.unreadOrigin = z;
        message.extras = jsonToBundle(new JSONObject(cursor.getString(cursor.getColumnIndex("extra"))));
        message.title = cursor.getString(cursor.getColumnIndex(ShareConstants.WEB_DIALOG_PARAM_TITLE));
        message.sentMS = getMillisecondsFromTimeStamp(cursor.getString(cursor.getColumnIndex("timestamp")), Long.valueOf(System.currentTimeMillis())).longValue();
        if (cursor.getInt(cursor.getColumnIndex("deleted")) != 1) {
            z2 = false;
        }
        message.deleted = z2;
        String rawMessageJSON = cursor.getString(cursor.getColumnIndex("raw_message_object"));
        message.rawMessageJSON = rawMessageJSON == null ? new JSONObject() : new JSONObject(rawMessageJSON);
        message.expirationMS = getMillisecondsFromTimeStamp(cursor.getString(cursor.getColumnIndex("expiration_timestamp")), null);
        return message;
    }

    static Long getMillisecondsFromTimeStamp(String timeStamp, Long defaultValue) {
        if (!UAStringUtil.isEmpty(timeStamp)) {
            try {
                defaultValue = Long.valueOf(DateUtils.parseIso8601(timeStamp));
            } catch (ParseException e) {
                Logger.error("RichPushMessage - Couldn't parse message date: " + timeStamp + ", defaulting to: " + defaultValue + ".");
            }
        }
        return defaultValue;
    }

    private static Bundle jsonToBundle(JSONObject json) throws JSONException {
        Bundle extras = new Bundle();
        if (json != null) {
            Iterator<String> keysIterator = json.keys();
            while (keysIterator.hasNext()) {
                String key = (String) keysIterator.next();
                extras.putString(key, json.getString(key));
            }
        }
        return extras;
    }

    public String getMessageId() {
        return this.messageId;
    }

    public String getTitle() {
        return this.title;
    }

    public boolean isRead() {
        return !this.unreadClient;
    }

    public Date getSentDate() {
        return new Date(this.sentMS);
    }

    public long getSentDateMS() {
        return this.sentMS;
    }

    public boolean isExpired() {
        return this.expirationMS != null && System.currentTimeMillis() >= this.expirationMS.longValue();
    }

    public boolean isDeleted() {
        return this.deleted;
    }

    public int compareTo(RichPushMessage another) {
        return getMessageId().compareTo(another.getMessageId());
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof RichPushMessage)) {
            return false;
        }
        RichPushMessage that = (RichPushMessage) o;
        if (this == that) {
            return true;
        }
        if (areObjectsEqual(this.messageId, that.messageId) && areObjectsEqual(this.messageBodyUrl, that.messageBodyUrl) && areObjectsEqual(this.messageReadUrl, that.messageReadUrl) && areObjectsEqual(this.messageUrl, that.messageUrl) && areObjectsEqual(this.extras, that.extras) && this.unreadClient == that.unreadClient && this.sentMS == that.sentMS) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        if (this.unreadClient) {
            i = 0;
        } else {
            i = 1;
        }
        i = (i + 629) * 37;
        if (!this.deleted) {
            i2 = 1;
        }
        return ((i + i2) * 37) + this.messageId.hashCode();
    }

    private boolean areObjectsEqual(Object a, Object b) {
        if (a == null && b == null) {
            return true;
        }
        if (a == null || b == null) {
            return false;
        }
        return a.equals(b);
    }
}
