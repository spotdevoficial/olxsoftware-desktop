package com.urbanairship.richpush;

import com.urbanairship.Logger;
import com.urbanairship.PreferenceDataStore;
import com.urbanairship.util.UAStringUtil;
import java.io.UnsupportedEncodingException;

class RichPushUserPreferences {
    private PreferenceDataStore preferenceDataStore;

    RichPushUserPreferences(PreferenceDataStore preferenceDataStore) {
        this.preferenceDataStore = preferenceDataStore;
        String password = preferenceDataStore.getString("com.urbanairship.user.PASSWORD", null);
        if (!UAStringUtil.isEmpty(password)) {
            if (preferenceDataStore.putSync("com.urbanairship.user.USER_TOKEN", encode(password, preferenceDataStore.getString("com.urbanairship.user.ID", null)))) {
                preferenceDataStore.put("com.urbanairship.user.PASSWORD", null);
            }
        }
    }

    public void setUserCredentials(String userId, String userToken) {
        this.preferenceDataStore.put("com.urbanairship.user.ID", userId);
        this.preferenceDataStore.put("com.urbanairship.user.USER_TOKEN", encode(userToken, userId));
    }

    public String getUserId() {
        return this.preferenceDataStore.getString("com.urbanairship.user.ID", null);
    }

    public String getUserToken() {
        return decode(this.preferenceDataStore.getString("com.urbanairship.user.USER_TOKEN", null), getUserId());
    }

    public long getLastMessageRefreshTime() {
        return this.preferenceDataStore.getLong("com.urbanairship.user.LAST_MESSAGE_REFRESH_TIME", 0);
    }

    public void setLastMessageRefreshTime(long timeMs) {
        this.preferenceDataStore.put("com.urbanairship.user.LAST_MESSAGE_REFRESH_TIME", Long.valueOf(timeMs));
    }

    public long getLastUpdateTime() {
        return this.preferenceDataStore.getLong("com.urbanairship.user.LAST_UPDATE_TIME", 0);
    }

    public void setLastUpdateTime(long timeMs) {
        this.preferenceDataStore.put("com.urbanairship.user.LAST_UPDATE_TIME", Long.valueOf(timeMs));
    }

    private String encode(String input, String key) {
        if (UAStringUtil.isEmpty(input) || UAStringUtil.isEmpty(key)) {
            return null;
        }
        byte[] bytes = xor(input.getBytes(), key.getBytes());
        StringBuilder hexHash = new StringBuilder();
        int len$ = bytes.length;
        for (int i$ = 0; i$ < len$; i$++) {
            hexHash.append(String.format("%02x", new Object[]{Byte.valueOf(arr$[i$])}));
        }
        return hexHash.toString();
    }

    private String decode(String encodedString, String key) {
        if (UAStringUtil.isEmpty(encodedString) || UAStringUtil.isEmpty(key)) {
            return null;
        }
        int length = encodedString.length();
        if (length % 2 != 0) {
            return null;
        }
        try {
            byte[] decodedBytes = new byte[(length / 2)];
            for (int i = 0; i < length; i += 2) {
                decodedBytes[i / 2] = Byte.parseByte(encodedString.substring(i, i + 2), 16);
            }
            return new String(xor(decodedBytes, key.getBytes()), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Logger.error("RichPushUserPreferences - Unable to decode string. " + e.getMessage());
            return null;
        } catch (NumberFormatException e2) {
            Logger.error("RichPushUserPreferences - String contains invalid hex numbers. " + e2.getMessage());
            return null;
        }
    }

    private byte[] xor(byte[] a, byte[] b) {
        byte[] out = new byte[a.length];
        for (int i = 0; i < a.length; i++) {
            out[i] = (byte) (a[i] ^ b[i % b.length]);
        }
        return out;
    }
}
