package com.urbanairship.richpush;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import com.facebook.appevents.AppEventsConstants;
import com.urbanairship.UrbanAirshipProvider;
import com.urbanairship.UrbanAirshipResolver;
import com.urbanairship.util.UAStringUtil;
import java.util.Collection;
import java.util.Set;

class RichPushResolver extends UrbanAirshipResolver {
    public RichPushResolver(Context context) {
        super(context);
    }

    Cursor getAllMessages() {
        return query(UrbanAirshipProvider.getRichPushContentUri(), null, null, null, "timestamp DESC");
    }

    Cursor getReadUpdatedMessages() {
        return query(UrbanAirshipProvider.getRichPushContentUri(), null, "unread = ? AND unread <> unread_orig", new String[]{AppEventsConstants.EVENT_PARAM_VALUE_NO}, null);
    }

    Cursor getDeletedMessages() {
        return query(UrbanAirshipProvider.getRichPushContentUri(), null, "deleted = ?", new String[]{AppEventsConstants.EVENT_PARAM_VALUE_YES}, null);
    }

    int markMessagesDeleted(Set<String> messageIds) {
        ContentValues values = new ContentValues();
        values.put("deleted", Boolean.valueOf(true));
        return updateMessages(messageIds, values);
    }

    int deleteMessages(Set<String> messageIds) {
        int numberOfmessageIds = messageIds.size();
        return delete(appendMessageIdsToUri(messageIds), "message_id IN ( " + UAStringUtil.repeat("?", numberOfmessageIds, ", ") + " )", (String[]) messageIds.toArray(new String[numberOfmessageIds]));
    }

    int insertMessages(ContentValues[] values) {
        return bulkInsert(UrbanAirshipProvider.getRichPushContentUri(), values);
    }

    int updateMessage(String messageId, ContentValues values) {
        return update(appendMessageIdToUri(messageId), values, "message_id = ?", new String[]{messageId});
    }

    int updateMessages(Set<String> messageIds, ContentValues values) {
        int numberOfmessageIds = messageIds.size();
        return update(appendMessageIdsToUri(messageIds), values, "message_id IN ( " + UAStringUtil.repeat("?", numberOfmessageIds, ", ") + " )", (String[]) messageIds.toArray(new String[numberOfmessageIds]));
    }

    private Uri appendMessageIdsToUri(Collection<String> ids) {
        return Uri.withAppendedPath(UrbanAirshipProvider.getRichPushContentUri(), UAStringUtil.join(ids, "|"));
    }

    private Uri appendMessageIdToUri(String id) {
        return Uri.withAppendedPath(UrbanAirshipProvider.getRichPushContentUri(), id);
    }
}
