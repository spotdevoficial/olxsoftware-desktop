package com.urbanairship.richpush;

import android.content.ContentValues;

class MessageListResponse {
    private final long lastModifiedTimeMS;
    private final ContentValues[] serverMessages;
    private final int status;

    public MessageListResponse(ContentValues[] serverMessages, int status, long lastModifiedTimeMS) {
        this.serverMessages = serverMessages;
        this.status = status;
        this.lastModifiedTimeMS = lastModifiedTimeMS;
    }

    public ContentValues[] getServerMessages() {
        return this.serverMessages;
    }

    public int getStatus() {
        return this.status;
    }

    public long getLastModifiedTimeMS() {
        return this.lastModifiedTimeMS;
    }
}
