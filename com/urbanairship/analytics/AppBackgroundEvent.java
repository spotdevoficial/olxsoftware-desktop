package com.urbanairship.analytics;

import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.util.UAStringUtil;
import org.json.JSONException;
import org.json.JSONObject;

class AppBackgroundEvent extends Event {
    AppBackgroundEvent(long timeMS) {
        super(timeMS);
    }

    public final String getType() {
        return "app_background";
    }

    protected final JSONObject getEventData() {
        JSONObject data = new JSONObject();
        try {
            data.put("connection_type", getConnectionType());
            String subtype = getConnectionSubType();
            if (!UAStringUtil.isEmpty(subtype)) {
                data.put("connection_subtype", subtype);
            }
            data.put("push_id", UAirship.shared().getAnalytics().getConversionSendId());
        } catch (JSONException e) {
            Logger.error("AppBackgroundEvent - Error constructing JSON data.", e);
        }
        return data;
    }
}
