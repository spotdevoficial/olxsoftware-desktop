package com.urbanairship.analytics;

import com.urbanairship.http.Response;
import com.urbanairship.util.UAMathUtil;
import java.util.List;

class EventResponse {
    private final Response response;

    public EventResponse(Response response) {
        this.response = response;
    }

    public int getStatus() {
        return this.response.getStatus();
    }

    public Integer getMaxTotalSize() {
        if (this.response.getResponseHeaders() != null) {
            List<String> headerList = (List) this.response.getResponseHeaders().get("X-UA-Max-Total");
            if (headerList != null && headerList.size() > 0) {
                return Integer.valueOf(UAMathUtil.constrain(Integer.parseInt((String) headerList.get(0)), 10240, 5242880));
            }
        }
        return Integer.valueOf(10240);
    }

    public Integer getMaxBatchSize() {
        if (this.response.getResponseHeaders() != null) {
            List<String> headerList = (List) this.response.getResponseHeaders().get("X-UA-Max-Batch");
            if (headerList != null && headerList.size() > 0) {
                return Integer.valueOf(UAMathUtil.constrain(Integer.parseInt((String) headerList.get(0)), hp.f178c, 512000));
            }
        }
        return Integer.valueOf(hp.f178c);
    }

    public Integer getMaxWait() {
        if (this.response.getResponseHeaders() != null) {
            List<String> headerList = (List) this.response.getResponseHeaders().get("X-UA-Max-Wait");
            if (headerList != null && headerList.size() > 0) {
                return Integer.valueOf(UAMathUtil.constrain(Integer.parseInt((String) headerList.get(0)), 604800000, 1209600000));
            }
        }
        return Integer.valueOf(604800000);
    }

    public Integer getMinBatchInterval() {
        if (this.response.getResponseHeaders() != null) {
            List<String> headerList = (List) this.response.getResponseHeaders().get("X-UA-Min-Batch-Interval");
            if (headerList != null && headerList.size() > 0) {
                return Integer.valueOf(UAMathUtil.constrain(Integer.parseInt((String) headerList.get(0)), 60000, 604800000));
            }
        }
        return Integer.valueOf(60000);
    }
}
