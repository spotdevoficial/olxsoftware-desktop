package com.mobileapptracker;

import android.util.Log;
import com.facebook.AccessToken;
import com.facebook.BuildConfig;
import com.facebook.internal.NativeProtocol;
import com.facebook.internal.ServerProtocol;
import com.facebook.share.internal.ShareConstants;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.mobileapptracker.b */
final class C1014b {
    private static Parameters f1266a;

    public static synchronized String m1711a() {
        String stringBuilder;
        synchronized (C1014b.class) {
            f1266a = Parameters.getInstance();
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("connection_type=" + f1266a.getConnectionType());
            C1014b.m1715a(stringBuilder2, "age", f1266a.getAge());
            C1014b.m1715a(stringBuilder2, "altitude", f1266a.getAltitude());
            C1014b.m1715a(stringBuilder2, "android_id", f1266a.getAndroidId());
            C1014b.m1715a(stringBuilder2, "app_ad_tracking", f1266a.getAppAdTrackingEnabled());
            C1014b.m1715a(stringBuilder2, NativeProtocol.BRIDGE_ARG_APP_NAME_STRING, f1266a.getAppName());
            C1014b.m1715a(stringBuilder2, "app_version", f1266a.getAppVersion());
            C1014b.m1715a(stringBuilder2, "country_code", f1266a.getCountryCode());
            C1014b.m1715a(stringBuilder2, "currency_code", f1266a.getCurrencyCode());
            C1014b.m1715a(stringBuilder2, "device_brand", f1266a.getDeviceBrand());
            C1014b.m1715a(stringBuilder2, "device_carrier", f1266a.getDeviceCarrier());
            C1014b.m1715a(stringBuilder2, "device_cpu_type", f1266a.getDeviceCpuType());
            C1014b.m1715a(stringBuilder2, "device_cpu_subtype", f1266a.getDeviceCpuSubtype());
            C1014b.m1715a(stringBuilder2, "device_model", f1266a.getDeviceModel());
            C1014b.m1715a(stringBuilder2, "device_id", f1266a.getDeviceId());
            C1014b.m1715a(stringBuilder2, "attribute_sub1", f1266a.getEventAttribute1());
            C1014b.m1715a(stringBuilder2, "attribute_sub2", f1266a.getEventAttribute2());
            C1014b.m1715a(stringBuilder2, "attribute_sub3", f1266a.getEventAttribute3());
            C1014b.m1715a(stringBuilder2, "attribute_sub4", f1266a.getEventAttribute4());
            C1014b.m1715a(stringBuilder2, "attribute_sub5", f1266a.getEventAttribute5());
            C1014b.m1715a(stringBuilder2, "content_id", f1266a.getEventContentId());
            C1014b.m1715a(stringBuilder2, "content_type", f1266a.getEventContentType());
            C1014b.m1715a(stringBuilder2, "date1", f1266a.getEventDate1());
            C1014b.m1715a(stringBuilder2, "date2", f1266a.getEventDate2());
            C1014b.m1715a(stringBuilder2, "level", f1266a.getEventLevel());
            C1014b.m1715a(stringBuilder2, "quantity", f1266a.getEventQuantity());
            C1014b.m1715a(stringBuilder2, "rating", f1266a.getEventRating());
            C1014b.m1715a(stringBuilder2, "search_string", f1266a.getEventSearchString());
            C1014b.m1715a(stringBuilder2, "existing_user", f1266a.getExistingUser());
            C1014b.m1715a(stringBuilder2, "facebook_user_id", f1266a.getFacebookUserId());
            C1014b.m1715a(stringBuilder2, "gender", f1266a.getGender());
            C1014b.m1715a(stringBuilder2, "google_aid", f1266a.getGoogleAdvertisingId());
            C1014b.m1715a(stringBuilder2, "google_ad_tracking_disabled", f1266a.getGoogleAdTrackingLimited());
            C1014b.m1715a(stringBuilder2, "google_user_id", f1266a.getGoogleUserId());
            C1014b.m1715a(stringBuilder2, "insdate", f1266a.getInstallDate());
            C1014b.m1715a(stringBuilder2, "installer", f1266a.getInstaller());
            C1014b.m1715a(stringBuilder2, "install_log_id", f1266a.getInstallLogId());
            C1014b.m1715a(stringBuilder2, "install_referrer", f1266a.getInstallReferrer());
            C1014b.m1715a(stringBuilder2, "is_paying_user", f1266a.getIsPayingUser());
            C1014b.m1715a(stringBuilder2, "language", f1266a.getLanguage());
            C1014b.m1715a(stringBuilder2, "last_open_log_id", f1266a.getLastOpenLogId());
            C1014b.m1715a(stringBuilder2, "latitude", f1266a.getLatitude());
            C1014b.m1715a(stringBuilder2, "longitude", f1266a.getLongitude());
            C1014b.m1715a(stringBuilder2, "mac_address", f1266a.getMacAddress());
            C1014b.m1715a(stringBuilder2, "mat_id", f1266a.getMatId());
            C1014b.m1715a(stringBuilder2, "mobile_country_code", f1266a.getMCC());
            C1014b.m1715a(stringBuilder2, "mobile_network_code", f1266a.getMNC());
            C1014b.m1715a(stringBuilder2, "open_log_id", f1266a.getOpenLogId());
            C1014b.m1715a(stringBuilder2, "os_version", f1266a.getOsVersion());
            C1014b.m1715a(stringBuilder2, "sdk_plugin", f1266a.getPluginName());
            C1014b.m1715a(stringBuilder2, "android_purchase_status", f1266a.getPurchaseStatus());
            C1014b.m1715a(stringBuilder2, "advertiser_ref_id", f1266a.getRefId());
            C1014b.m1715a(stringBuilder2, "revenue", f1266a.getRevenue());
            C1014b.m1715a(stringBuilder2, "screen_density", f1266a.getScreenDensity());
            C1014b.m1715a(stringBuilder2, "screen_layout_size", f1266a.getScreenWidth() + "x" + f1266a.getScreenHeight());
            C1014b.m1715a(stringBuilder2, "sdk_version", f1266a.getSdkVersion());
            C1014b.m1715a(stringBuilder2, "truste_tpid", f1266a.getTRUSTeId());
            C1014b.m1715a(stringBuilder2, "twitter_user_id", f1266a.getTwitterUserId());
            C1014b.m1715a(stringBuilder2, "update_log_id", f1266a.getUpdateLogId());
            C1014b.m1715a(stringBuilder2, "conversion_user_agent", f1266a.getUserAgent());
            C1014b.m1715a(stringBuilder2, "user_email", f1266a.getUserEmail());
            C1014b.m1715a(stringBuilder2, AccessToken.USER_ID_KEY, f1266a.getUserId());
            C1014b.m1715a(stringBuilder2, "user_name", f1266a.getUserName());
            stringBuilder = stringBuilder2.toString();
        }
        return stringBuilder;
    }

    public static synchronized String m1712a(String str, Encryption encryption) {
        String bytesToHex;
        synchronized (C1014b.class) {
            StringBuilder stringBuilder = new StringBuilder(str);
            Parameters instance = Parameters.getInstance();
            f1266a = instance;
            if (instance != null) {
                String googleAdvertisingId = f1266a.getGoogleAdvertisingId();
                if (!(googleAdvertisingId == null || str.contains("&google_aid="))) {
                    C1014b.m1715a(stringBuilder, "google_aid", googleAdvertisingId);
                    C1014b.m1715a(stringBuilder, "google_ad_tracking_disabled", f1266a.getGoogleAdTrackingLimited());
                }
                googleAdvertisingId = f1266a.getInstallReferrer();
                if (!(googleAdvertisingId == null || str.contains("&install_referrer="))) {
                    C1014b.m1715a(stringBuilder, "install_referrer", googleAdvertisingId);
                }
            }
            if (!str.contains("&system_date=")) {
                C1014b.m1715a(stringBuilder, "system_date", Long.toString(new Date().getTime() / 1000));
            }
            try {
                bytesToHex = Encryption.bytesToHex(encryption.encrypt(stringBuilder.toString()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return bytesToHex;
    }

    public static String m1713a(boolean z, boolean z2, boolean z3) {
        f1266a = Parameters.getInstance();
        StringBuilder append = new StringBuilder("https://").append(f1266a.getAdvertiserId()).append(".");
        if (z) {
            append.append("debug.engine.mobileapptracking.com");
        } else {
            append.append("engine.mobileapptracking.com");
        }
        append.append("/serve?ver=").append(f1266a.getSdkVersion());
        append.append("&transaction_id=").append(UUID.randomUUID().toString());
        C1014b.m1715a(append, ServerProtocol.DIALOG_PARAM_SDK_VERSION, "android");
        C1014b.m1715a(append, NativeProtocol.WEB_DIALOG_ACTION, f1266a.getAction());
        C1014b.m1715a(append, "advertiser_id", f1266a.getAdvertiserId());
        C1014b.m1715a(append, "site_event_id", f1266a.getEventId());
        C1014b.m1715a(append, "site_event_name", f1266a.getEventName());
        C1014b.m1715a(append, "package_name", f1266a.getPackageName());
        C1014b.m1715a(append, "referral_source", f1266a.getReferralSource());
        C1014b.m1715a(append, "referral_url", f1266a.getReferralUrl());
        C1014b.m1715a(append, "site_id", f1266a.getSiteId());
        C1014b.m1715a(append, "tracking_id", f1266a.getTrackingId());
        if (z2) {
            append.append("&attr_set=1");
        }
        C1014b.m1715a(append, "publisher_id", f1266a.getPublisherId());
        C1014b.m1715a(append, "offer_id", f1266a.getOfferId());
        C1014b.m1715a(append, "publisher_ref_id", f1266a.getPublisherReferenceId());
        C1014b.m1715a(append, "publisher_sub_publisher", f1266a.getPublisherSubPublisher());
        C1014b.m1715a(append, "publisher_sub_site", f1266a.getPublisherSubSite());
        C1014b.m1715a(append, "publisher_sub_campaign", f1266a.getPublisherSubCampaign());
        C1014b.m1715a(append, "publisher_sub_adgroup", f1266a.getPublisherSubAdgroup());
        C1014b.m1715a(append, "publisher_sub_ad", f1266a.getPublisherSubAd());
        C1014b.m1715a(append, "publisher_sub_keyword", f1266a.getPublisherSubKeyword());
        C1014b.m1715a(append, "advertiser_sub_publisher", f1266a.getAdvertiserSubPublisher());
        C1014b.m1715a(append, "advertiser_sub_site", f1266a.getAdvertiserSubSite());
        C1014b.m1715a(append, "advertiser_sub_campaign", f1266a.getAdvertiserSubCampaign());
        C1014b.m1715a(append, "advertiser_sub_adgroup", f1266a.getAdvertiserSubAdgroup());
        C1014b.m1715a(append, "advertiser_sub_ad", f1266a.getAdvertiserSubAd());
        C1014b.m1715a(append, "advertiser_sub_keyword", f1266a.getAdvertiserSubKeyword());
        C1014b.m1715a(append, "publisher_sub1", f1266a.getPublisherSub1());
        C1014b.m1715a(append, "publisher_sub2", f1266a.getPublisherSub2());
        C1014b.m1715a(append, "publisher_sub3", f1266a.getPublisherSub3());
        C1014b.m1715a(append, "publisher_sub4", f1266a.getPublisherSub4());
        C1014b.m1715a(append, "publisher_sub5", f1266a.getPublisherSub5());
        String allowDuplicates = f1266a.getAllowDuplicates();
        if (allowDuplicates != null && Integer.parseInt(allowDuplicates) == 1) {
            append.append("&skip_dup=1");
        }
        if (z) {
            append.append("&debug=1");
        }
        if (z3) {
            append.append("&post_conversion=1");
        }
        return append.toString();
    }

    public static synchronized JSONObject m1714a(JSONArray jSONArray, String str, String str2) {
        JSONObject jSONObject;
        synchronized (C1014b.class) {
            jSONObject = new JSONObject();
            if (jSONArray != null) {
                try {
                    jSONObject.put(ShareConstants.WEB_DIALOG_PARAM_DATA, jSONArray);
                } catch (JSONException e) {
                    Log.d("MobileAppTracker", "Could not build JSON for event items or verification values");
                    e.printStackTrace();
                }
            }
            if (str != null) {
                jSONObject.put("store_iap_data", str);
            }
            if (str2 != null) {
                jSONObject.put("store_iap_signature", str2);
            }
        }
        return jSONObject;
    }

    private static synchronized void m1715a(StringBuilder stringBuilder, String str, String str2) {
        synchronized (C1014b.class) {
            if (str2 != null) {
                if (!str2.equals(BuildConfig.VERSION_NAME)) {
                    try {
                        stringBuilder.append(new StringBuilder(Identifier.PARAMETER_SEPARATOR).append(str).append(Identifier.PARAMETER_ASIGNMENT).append(URLEncoder.encode(str2, "UTF-8")).toString());
                    } catch (UnsupportedEncodingException e) {
                        Log.w("MobileAppTracker", "failed encoding value " + str2 + " for key " + str);
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
