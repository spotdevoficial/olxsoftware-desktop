package com.mobileapptracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import java.net.URLDecoder;

public class Tracker extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            try {
                if (intent.getAction().equals("com.android.vending.INSTALL_REFERRER")) {
                    String stringExtra = intent.getStringExtra("referrer");
                    if (stringExtra != null) {
                        stringExtra = URLDecoder.decode(stringExtra, "UTF-8");
                        Log.d("MobileAppTracker", "MAT received referrer " + stringExtra);
                        context.getSharedPreferences("mat_referrer", 0).edit().putString("referrer", stringExtra).commit();
                        MobileAppTracker instance = MobileAppTracker.getInstance();
                        if (instance != null) {
                            instance.m1708a();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
