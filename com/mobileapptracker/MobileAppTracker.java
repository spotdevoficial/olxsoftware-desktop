package com.mobileapptracker;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import com.facebook.BuildConfig;
import com.facebook.GraphResponse;
import com.facebook.internal.AnalyticsEvents;
import com.mobileapptracker.MATEventQueue.Add;
import com.mobileapptracker.MATEventQueue.Dump;
import com.schibsted.scm.nextgenapp.models.submodels.Setting;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MobileAppTracker {
    private static volatile MobileAppTracker f1207g;
    private final String f1208a;
    private C1015c f1209b;
    private Encryption f1210c;
    private boolean f1211d;
    private boolean f1212e;
    protected MATEventQueue eventQueue;
    private ExecutorService f1213f;
    protected boolean initialized;
    protected Context mContext;
    protected MATTestRequest matRequest;
    protected MATResponse matResponse;
    protected Parameters params;

    static {
        f1207g = null;
    }

    protected MobileAppTracker() {
        this.f1208a = "heF9BATUfWuISyO8";
    }

    private synchronized void m1706a(Object obj, JSONArray jSONArray, double d, String str, String str2, String str3, String str4, boolean z) {
        if (this.initialized) {
            dumpQueue();
            this.params.setAction("conversion");
            Date date = new Date();
            if (obj instanceof String) {
                if (!obj.equals("close")) {
                    if (obj.equals("open") || obj.equals("install") || obj.equals("update") || obj.equals("session")) {
                        if (z) {
                            this.params.setAction("install");
                        } else {
                            this.params.setAction("session");
                        }
                        date = new Date(date.getTime() + 5000);
                    } else {
                        this.params.setEventName((String) obj);
                    }
                }
            } else if (obj instanceof Integer) {
                this.params.setEventId(Integer.toString(((Integer) obj).intValue()));
            } else {
                Log.d("MobileAppTracker", "Received invalid event name or id value, not measuring event");
            }
            this.params.setRevenue(Double.toString(d));
            if (d > 0.0d) {
                this.params.setIsPayingUser(Integer.toString(1));
            }
            this.params.setCurrencyCode(str);
            this.params.setRefId(str2);
            String a = C1014b.m1713a(this.f1211d, this.f1212e, z);
            String a2 = C1014b.m1711a();
            JSONObject a3 = C1014b.m1714a(jSONArray, str3, str4);
            if (this.matRequest != null) {
                this.matRequest.constructedRequest(a, a2, a3);
            }
            addEventToQueue(a, a2, a3, date);
            dumpQueue();
            if (this.matResponse != null) {
                this.matResponse.enqueuedActionWithRefId(str2);
            }
            this.params.resetAfterRequest();
        }
    }

    private void m1707c() {
        Date date = null;
        while (this.params == null) {
            if (date == null) {
                date = new Date(new Date().getTime() + 1000);
            }
            if (date.before(new Date())) {
                Log.w("MobileAppTracker", "after waiting 1 s, params is still null -- will probably lead to NullPointerException");
                return;
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
            }
        }
    }

    public static synchronized MobileAppTracker getInstance() {
        MobileAppTracker mobileAppTracker;
        synchronized (MobileAppTracker.class) {
            mobileAppTracker = f1207g;
        }
        return mobileAppTracker;
    }

    public static boolean isOnline(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    final void m1708a() {
        m1706a("install", null, 0.0d, null, null, null, null, true);
    }

    protected void addEventToQueue(String link, String data, JSONObject postBody, Date runDate) {
        ExecutorService executorService = this.f1213f;
        MATEventQueue mATEventQueue = this.eventQueue;
        mATEventQueue.getClass();
        executorService.execute(new Add(mATEventQueue, link, data, postBody, runDate));
    }

    protected void dumpQueue() {
        if (isOnline(this.mContext)) {
            ExecutorService executorService = this.f1213f;
            MATEventQueue mATEventQueue = this.eventQueue;
            mATEventQueue.getClass();
            executorService.execute(new Dump(mATEventQueue));
        }
    }

    public String getOpenLogId() {
        m1707c();
        return this.params.getOpenLogId();
    }

    protected boolean makeRequest(String link, String data, JSONObject postBody) {
        boolean z = false;
        if (this.f1211d) {
            Log.d("MobileAppTracker", "Sending event to server...");
        }
        JSONObject a = this.f1209b.m1716a(link + "&data=" + C1014b.m1712a(data, this.f1210c), postBody, this.f1211d);
        if (a == null) {
            if (this.matResponse == null) {
                return true;
            }
            this.matResponse.didFailWithError(a);
            return true;
        } else if (a.has(GraphResponse.SUCCESS_KEY)) {
            if (this.matResponse != null) {
                try {
                    if (a.getString(GraphResponse.SUCCESS_KEY).equals(Setting.TRUE)) {
                        z = true;
                    }
                    if (z) {
                        this.matResponse.didSucceedWithData(a);
                    } else {
                        this.matResponse.didFailWithError(a);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            try {
                if (a.getString("site_event_type").equals("open")) {
                    String string = a.getString("log_id");
                    if (getOpenLogId().equals(BuildConfig.VERSION_NAME)) {
                        this.params.setOpenLogId(string);
                    }
                    this.params.setLastOpenLogId(string);
                }
            } catch (JSONException e2) {
            }
            if (!this.f1211d) {
                return true;
            }
            Log.d("MobileAppTracker", "Server response: " + a.toString());
            if (a.length() <= 0) {
                return true;
            }
            try {
                JSONObject jSONObject;
                if (a.has("log_action") && !a.getString("log_action").equals("null")) {
                    jSONObject = a.getJSONObject("log_action");
                    if (!jSONObject.has("conversion")) {
                        return true;
                    }
                    jSONObject = jSONObject.getJSONObject("conversion");
                    if (!jSONObject.has(AnalyticsEvents.PARAMETER_SHARE_DIALOG_CONTENT_STATUS)) {
                        return true;
                    }
                    if (jSONObject.getString(AnalyticsEvents.PARAMETER_SHARE_DIALOG_CONTENT_STATUS).equals("rejected")) {
                        Log.d("MobileAppTracker", "Event was rejected by server: status code " + jSONObject.getString("status_code"));
                        return true;
                    }
                    Log.d("MobileAppTracker", "Event was accepted by server");
                    return true;
                } else if (!a.has("options")) {
                    return true;
                } else {
                    jSONObject = a.getJSONObject("options");
                    if (!jSONObject.has("conversion_status")) {
                        return true;
                    }
                    Log.d("MobileAppTracker", "Event was " + jSONObject.getString("conversion_status") + " by server");
                    return true;
                }
            } catch (JSONException e3) {
                Log.d("MobileAppTracker", "Server response status could not be parsed");
                e3.printStackTrace();
                return true;
            }
        } else {
            if (this.f1211d) {
                Log.d("MobileAppTracker", "Request failed, event will remain in queue");
            }
            return false;
        }
    }
}
