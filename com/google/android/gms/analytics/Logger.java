package com.google.android.gms.analytics;

@Deprecated
public interface Logger {
    @Deprecated
    void error(String str);

    @Deprecated
    int getLogLevel();

    @Deprecated
    void setLogLevel(int i);

    @Deprecated
    void warn(String str);
}
