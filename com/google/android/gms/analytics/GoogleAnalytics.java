package com.google.android.gms.analytics;

import android.content.Context;
import android.util.Log;
import com.google.android.gms.analytics.internal.zzae;
import com.google.android.gms.analytics.internal.zzan;
import com.google.android.gms.analytics.internal.zzf;
import com.google.android.gms.analytics.internal.zzy;
import com.google.android.gms.common.internal.zzx;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class GoogleAnalytics extends zza {
    private static List<Runnable> zzKt;
    private boolean zzKu;
    private Set<Object> zzKv;
    private boolean zzKx;
    private volatile boolean zzKy;
    private boolean zzKz;
    private boolean zzpr;

    static {
        zzKt = new ArrayList();
    }

    public GoogleAnalytics(zzf context) {
        super(context);
        this.zzKv = new HashSet();
    }

    public static GoogleAnalytics getInstance(Context context) {
        return zzf.zzX(context).zzis();
    }

    private zzan zzhA() {
        return zzhp().zzhA();
    }

    public static void zzhx() {
        synchronized (GoogleAnalytics.class) {
            if (zzKt != null) {
                for (Runnable run : zzKt) {
                    run.run();
                }
                zzKt = null;
            }
        }
    }

    public boolean getAppOptOut() {
        return this.zzKy;
    }

    public String getClientId() {
        zzx.zzci("getClientId can not be called from the main thread");
        return zzhp().zziv().zzjd();
    }

    @Deprecated
    public Logger getLogger() {
        return zzae.getLogger();
    }

    public boolean isDryRunEnabled() {
        return this.zzKx;
    }

    public boolean isInitialized() {
        return this.zzpr && !this.zzKu;
    }

    public Tracker newTracker(String trackingId) {
        Tracker tracker;
        synchronized (this) {
            tracker = new Tracker(zzhp(), trackingId, null);
            tracker.zza();
        }
        return tracker;
    }

    public void setDryRun(boolean dryRun) {
        this.zzKx = dryRun;
    }

    @Deprecated
    public void setLogger(Logger logger) {
        zzae.setLogger(logger);
        if (!this.zzKz) {
            Log.i((String) zzy.zzNa.get(), "GoogleAnalytics.setLogger() is deprecated. To enable debug logging, please run:\nadb shell setprop log.tag." + ((String) zzy.zzNa.get()) + " DEBUG");
            this.zzKz = true;
        }
    }

    public void zza() {
        zzhw();
        this.zzpr = true;
    }

    void zzhw() {
        zzan zzhA = zzhA();
        if (zzhA.zzkc()) {
            getLogger().setLogLevel(zzhA.getLogLevel());
        }
        if (zzhA.zzkg()) {
            setDryRun(zzhA.zzkh());
        }
        if (zzhA.zzkc()) {
            Logger logger = zzae.getLogger();
            if (logger != null) {
                logger.setLogLevel(zzhA.getLogLevel());
            }
        }
    }
}
