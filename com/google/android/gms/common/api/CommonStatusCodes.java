package com.google.android.gms.common.api;

import com.facebook.login.widget.ProfilePictureView;
import com.schibsted.scm.nextgenapp.C1061R;
import com.urbanairship.C1608R;

public class CommonStatusCodes {
    public static String getStatusCodeString(int statusCode) {
        switch (statusCode) {
            case ProfilePictureView.CUSTOM /*-1*/:
                return "SUCCESS_CACHE";
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "SUCCESS";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "SERVICE_MISSING";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "SERVICE_VERSION_UPDATE_REQUIRED";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "SERVICE_DISABLED";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "SIGN_IN_REQUIRED";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "INVALID_ACCOUNT";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "RESOLUTION_REQUIRED";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "NETWORK_ERROR";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "INTERNAL_ERROR";
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return "SERVICE_INVALID";
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return "DEVELOPER_ERROR";
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                return "LICENSE_CHECK_FAILED";
            case C1608R.styleable.MapAttrs_useViewLifecycle /*13*/:
                return "ERROR_OPERATION_FAILED";
            case C1608R.styleable.MapAttrs_zOrderOnTop /*14*/:
                return "INTERRUPTED";
            case C1608R.styleable.MapAttrs_uiMapToolbar /*15*/:
                return "TIMEOUT";
            case C1061R.styleable.Toolbar_maxButtonHeight /*16*/:
                return "CANCELED";
            case 3000:
                return "AUTH_API_INVALID_CREDENTIALS";
            case 3001:
                return "AUTH_API_ACCESS_FORBIDDEN";
            case 3002:
                return "AUTH_API_CLIENT_ERROR";
            case 3003:
                return "AUTH_API_SERVER_ERROR";
            case 3004:
                return "AUTH_TOKEN_ERROR";
            case 3005:
                return "AUTH_URL_RESOLUTION";
            default:
                return "unknown status code: " + statusCode;
        }
    }
}
