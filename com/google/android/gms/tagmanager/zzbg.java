package com.google.android.gms.tagmanager;

public final class zzbg {
    static zzbh zzaQG;
    static int zzaQH;

    static {
        zzaQG = new zzy();
    }

    public static void m1107e(String message) {
        zzaQG.m1109e(message);
    }

    public static int getLogLevel() {
        return zzaQH;
    }

    public static void m1108v(String message) {
        zzaQG.m1110v(message);
    }

    public static void zzaD(String str) {
        zzaQG.zzaD(str);
    }

    public static void zzaE(String str) {
        zzaQG.zzaE(str);
    }

    public static void zzb(String str, Throwable th) {
        zzaQG.zzb(str, th);
    }

    public static void zzd(String str, Throwable th) {
        zzaQG.zzd(str, th);
    }
}
