package com.google.android.gms.ads.doubleclick;

import com.google.android.gms.ads.internal.client.zzx;
import com.google.android.gms.ads.internal.client.zzx.zza;

public final class PublisherAdRequest {
    public static final String DEVICE_ID_EMULATOR;
    private final zzx zznN;

    public static final class Builder {
        private final zza zznO;

        public Builder() {
            this.zznO = new zza();
        }

        public Builder addCustomTargeting(String key, String value) {
            this.zznO.zzb(key, value);
            return this;
        }

        public PublisherAdRequest build() {
            return new PublisherAdRequest();
        }
    }

    static {
        DEVICE_ID_EMULATOR = zzx.DEVICE_ID_EMULATOR;
    }

    private PublisherAdRequest(Builder builder) {
        this.zznN = new zzx(builder.zznO);
    }

    public zzx zzaF() {
        return this.zznN;
    }
}
