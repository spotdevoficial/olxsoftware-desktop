package com.google.android.gms.internal;

import java.io.IOException;

public abstract class zzrx {
    protected volatile int zzbcl;

    public zzrx() {
        this.zzbcl = -1;
    }

    public static final void zza(zzrx com_google_android_gms_internal_zzrx, byte[] bArr, int i, int i2) {
        try {
            zzrq zzb = zzrq.zzb(bArr, i, i2);
            com_google_android_gms_internal_zzrx.zza(zzb);
            zzb.zzDj();
        } catch (Throwable e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }

    public static final byte[] zzf(zzrx com_google_android_gms_internal_zzrx) {
        byte[] bArr = new byte[com_google_android_gms_internal_zzrx.zzDx()];
        zza(com_google_android_gms_internal_zzrx, bArr, 0, bArr.length);
        return bArr;
    }

    public /* synthetic */ Object clone() throws CloneNotSupportedException {
        return zzDm();
    }

    public String toString() {
        return zzry.zzg(this);
    }

    protected int zzB() {
        return 0;
    }

    public zzrx zzDm() throws CloneNotSupportedException {
        return (zzrx) super.clone();
    }

    public int zzDw() {
        if (this.zzbcl < 0) {
            zzDx();
        }
        return this.zzbcl;
    }

    public int zzDx() {
        int zzB = zzB();
        this.zzbcl = zzB;
        return zzB;
    }

    public void zza(zzrq com_google_android_gms_internal_zzrq) throws IOException {
    }
}
