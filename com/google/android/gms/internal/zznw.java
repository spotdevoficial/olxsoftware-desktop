package com.google.android.gms.internal;

import com.facebook.BuildConfig;
import java.io.IOException;

public interface zznw {

    public static final class zza extends zzrr<zza> {
        public zza[] zzaAm;

        public static final class zza extends zzrr<zza> {
            private static volatile zza[] zzaAn;
            public int viewId;
            public String zzaAo;
            public String zzaAp;

            public zza() {
                zzvq();
            }

            public static zza[] zzvp() {
                if (zzaAn == null) {
                    synchronized (zzrv.zzbck) {
                        if (zzaAn == null) {
                            zzaAn = new zza[0];
                        }
                    }
                }
                return zzaAn;
            }

            public boolean equals(Object o) {
                if (o == this) {
                    return true;
                }
                if (!(o instanceof zza)) {
                    return false;
                }
                zza com_google_android_gms_internal_zznw_zza_zza = (zza) o;
                if (this.zzaAo == null) {
                    if (com_google_android_gms_internal_zznw_zza_zza.zzaAo != null) {
                        return false;
                    }
                } else if (!this.zzaAo.equals(com_google_android_gms_internal_zznw_zza_zza.zzaAo)) {
                    return false;
                }
                if (this.zzaAp == null) {
                    if (com_google_android_gms_internal_zznw_zza_zza.zzaAp != null) {
                        return false;
                    }
                } else if (!this.zzaAp.equals(com_google_android_gms_internal_zznw_zza_zza.zzaAp)) {
                    return false;
                }
                return this.viewId == com_google_android_gms_internal_zznw_zza_zza.viewId ? zza((zzrr) com_google_android_gms_internal_zznw_zza_zza) : false;
            }

            public int hashCode() {
                int i = 0;
                int hashCode = ((this.zzaAo == null ? 0 : this.zzaAo.hashCode()) + 527) * 31;
                if (this.zzaAp != null) {
                    i = this.zzaAp.hashCode();
                }
                return ((((hashCode + i) * 31) + this.viewId) * 31) + zzDk();
            }

            protected int zzB() {
                int zzB = super.zzB();
                if (!this.zzaAo.equals(BuildConfig.VERSION_NAME)) {
                    zzB += zzrq.zzl(1, this.zzaAo);
                }
                if (!this.zzaAp.equals(BuildConfig.VERSION_NAME)) {
                    zzB += zzrq.zzl(2, this.zzaAp);
                }
                return this.viewId != 0 ? zzB + zzrq.zzB(3, this.viewId) : zzB;
            }

            public void zza(zzrq com_google_android_gms_internal_zzrq) throws IOException {
                if (!this.zzaAo.equals(BuildConfig.VERSION_NAME)) {
                    com_google_android_gms_internal_zzrq.zzb(1, this.zzaAo);
                }
                if (!this.zzaAp.equals(BuildConfig.VERSION_NAME)) {
                    com_google_android_gms_internal_zzrq.zzb(2, this.zzaAp);
                }
                if (this.viewId != 0) {
                    com_google_android_gms_internal_zzrq.zzz(3, this.viewId);
                }
                super.zza(com_google_android_gms_internal_zzrq);
            }

            public zza zzvq() {
                this.zzaAo = BuildConfig.VERSION_NAME;
                this.zzaAp = BuildConfig.VERSION_NAME;
                this.viewId = 0;
                this.zzbca = null;
                this.zzbcl = -1;
                return this;
            }
        }

        public zza() {
            zzvo();
        }

        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof zza)) {
                return false;
            }
            zza com_google_android_gms_internal_zznw_zza = (zza) o;
            return zzrv.equals(this.zzaAm, com_google_android_gms_internal_zznw_zza.zzaAm) ? zza((zzrr) com_google_android_gms_internal_zznw_zza) : false;
        }

        public int hashCode() {
            return ((zzrv.hashCode(this.zzaAm) + 527) * 31) + zzDk();
        }

        protected int zzB() {
            int zzB = super.zzB();
            if (this.zzaAm != null && this.zzaAm.length > 0) {
                for (zzrx com_google_android_gms_internal_zzrx : this.zzaAm) {
                    if (com_google_android_gms_internal_zzrx != null) {
                        zzB += zzrq.zzc(1, com_google_android_gms_internal_zzrx);
                    }
                }
            }
            return zzB;
        }

        public void zza(zzrq com_google_android_gms_internal_zzrq) throws IOException {
            if (this.zzaAm != null && this.zzaAm.length > 0) {
                for (zzrx com_google_android_gms_internal_zzrx : this.zzaAm) {
                    if (com_google_android_gms_internal_zzrx != null) {
                        com_google_android_gms_internal_zzrq.zza(1, com_google_android_gms_internal_zzrx);
                    }
                }
            }
            super.zza(com_google_android_gms_internal_zzrq);
        }

        public zza zzvo() {
            this.zzaAm = zza.zzvp();
            this.zzbca = null;
            this.zzbcl = -1;
            return this;
        }
    }
}
