package com.google.android.gms.maps;

import android.os.RemoteException;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.RuntimeRemoteException;

public final class CameraUpdateFactory {
    private static ICameraUpdateFactoryDelegate zzaFo;

    public static CameraUpdate newLatLngZoom(LatLng latLng, float zoom) {
        try {
            return new CameraUpdate(zzwC().newLatLngZoom(latLng, zoom));
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public static void zza(ICameraUpdateFactoryDelegate iCameraUpdateFactoryDelegate) {
        zzaFo = (ICameraUpdateFactoryDelegate) zzx.zzv(iCameraUpdateFactoryDelegate);
    }

    private static ICameraUpdateFactoryDelegate zzwC() {
        return (ICameraUpdateFactoryDelegate) zzx.zzb(zzaFo, (Object) "CameraUpdateFactory is not initialized");
    }
}
