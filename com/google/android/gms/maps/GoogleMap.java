package com.google.android.gms.maps;

import android.os.RemoteException;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.maps.internal.IGoogleMapDelegate;
import com.google.android.gms.maps.internal.zzi.zza;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.RuntimeRemoteException;

public final class GoogleMap {
    private final IGoogleMapDelegate zzaFp;

    /* renamed from: com.google.android.gms.maps.GoogleMap.8 */
    class C08928 extends zza {
        final /* synthetic */ OnMapClickListener zzaFB;
        final /* synthetic */ GoogleMap zzaFs;

        C08928(GoogleMap googleMap, OnMapClickListener onMapClickListener) {
            this.zzaFs = googleMap;
            this.zzaFB = onMapClickListener;
        }

        public void onMapClick(LatLng point) {
            this.zzaFB.onMapClick(point);
        }
    }

    public interface OnMapClickListener {
        void onMapClick(LatLng latLng);
    }

    protected GoogleMap(IGoogleMapDelegate map) {
        this.zzaFp = (IGoogleMapDelegate) zzx.zzv(map);
    }

    public final Circle addCircle(CircleOptions options) {
        try {
            return new Circle(this.zzaFp.addCircle(options));
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final void moveCamera(CameraUpdate update) {
        try {
            this.zzaFp.moveCamera(update.zzwB());
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final void setOnMapClickListener(OnMapClickListener listener) {
        if (listener == null) {
            try {
                this.zzaFp.setOnMapClickListener(null);
                return;
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }
        this.zzaFp.setOnMapClickListener(new C08928(this, listener));
    }

    public final void setPadding(int left, int top, int right, int bottom) {
        try {
            this.zzaFp.setPadding(left, top, right, bottom);
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }
}
