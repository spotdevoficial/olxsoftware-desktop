package com.google.ads.conversiontracking;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: com.google.ads.conversiontracking.n */
public class C0453n implements ServiceConnection {
    boolean f768a;
    private final BlockingQueue<IBinder> f769b;

    public C0453n() {
        this.f768a = false;
        this.f769b = new LinkedBlockingQueue();
    }

    public IBinder m1085a() throws InterruptedException {
        if (this.f768a) {
            throw new IllegalStateException();
        }
        this.f768a = true;
        return (IBinder) this.f769b.take();
    }

    public void onServiceConnected(ComponentName name, IBinder service) {
        try {
            this.f769b.put(service);
        } catch (InterruptedException e) {
        }
    }

    public void onServiceDisconnected(ComponentName name) {
    }
}
