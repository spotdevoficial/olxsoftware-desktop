package com.google.ads.conversiontracking;

import android.content.Context;
import android.net.http.AndroidHttpClient;
import android.util.Log;
import com.google.ads.conversiontracking.C0444g.C0442c;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;

/* renamed from: com.google.ads.conversiontracking.e */
public class C0435e {
    private final Object f708a;
    private final List<C0431d> f709b;
    private Context f710c;
    private boolean f711d;
    private boolean f712e;
    private C0437f f713f;

    /* renamed from: com.google.ads.conversiontracking.e.1 */
    class C04321 implements Runnable {
        final /* synthetic */ C0431d f703a;
        final /* synthetic */ C0435e f704b;

        C04321(C0435e c0435e, C0431d c0431d) {
            this.f704b = c0435e;
            this.f703a = c0431d;
        }

        public void run() {
            this.f704b.m1004a(this.f703a);
        }
    }

    /* renamed from: com.google.ads.conversiontracking.e.a */
    private class C0433a implements Runnable {
        final /* synthetic */ C0435e f705a;

        private C0433a(C0435e c0435e) {
            this.f705a = c0435e;
        }

        public void run() {
            synchronized (this.f705a.f708a) {
                if (this.f705a.f712e && C0444g.m1067d(this.f705a.f710c) && !this.f705a.f711d) {
                    this.f705a.f709b.addAll(this.f705a.f713f.m1010a(100));
                    C0444g.m1066c(this.f705a.f710c);
                    this.f705a.f711d = true;
                    this.f705a.f708a.notify();
                    return;
                }
            }
        }
    }

    /* renamed from: com.google.ads.conversiontracking.e.b */
    public class C0434b implements Runnable {
        protected long f706a;
        final /* synthetic */ C0435e f707b;

        public C0434b(C0435e c0435e) {
            this.f707b = c0435e;
            this.f706a = 0;
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            r6 = this;
            r5 = 0;
            r0 = r6.f707b;	 Catch:{ InterruptedException -> 0x002d }
            r1 = 1;
            r0.f712e = r1;	 Catch:{ InterruptedException -> 0x002d }
        L_0x0007:
            r0 = r6.f707b;	 Catch:{ InterruptedException -> 0x002d }
            r1 = r0.f708a;	 Catch:{ InterruptedException -> 0x002d }
            monitor-enter(r1);	 Catch:{ InterruptedException -> 0x002d }
        L_0x000e:
            r0 = r6.f707b;	 Catch:{ all -> 0x002a }
            r0 = r0.f709b;	 Catch:{ all -> 0x002a }
            r0 = r0.isEmpty();	 Catch:{ all -> 0x002a }
            if (r0 == 0) goto L_0x003b;
        L_0x001a:
            r0 = r6.f707b;	 Catch:{ all -> 0x002a }
            r2 = 0;
            r0.f711d = r2;	 Catch:{ all -> 0x002a }
            r0 = r6.f707b;	 Catch:{ all -> 0x002a }
            r0 = r0.f708a;	 Catch:{ all -> 0x002a }
            r0.wait();	 Catch:{ all -> 0x002a }
            goto L_0x000e;
        L_0x002a:
            r0 = move-exception;
            monitor-exit(r1);	 Catch:{ all -> 0x002a }
            throw r0;	 Catch:{ InterruptedException -> 0x002d }
        L_0x002d:
            r0 = move-exception;
            r0 = "GoogleConversionReporter";
            r1 = "Dispatch thread is interrupted.";
            android.util.Log.w(r0, r1);
            r0 = r6.f707b;
            r0.f712e = r5;
            return;
        L_0x003b:
            r0 = r6.f707b;	 Catch:{ all -> 0x002a }
            r2 = 1;
            r0.f711d = r2;	 Catch:{ all -> 0x002a }
            r0 = r6.f707b;	 Catch:{ all -> 0x002a }
            r0 = r0.f709b;	 Catch:{ all -> 0x002a }
            r2 = 0;
            r0 = r0.remove(r2);	 Catch:{ all -> 0x002a }
            r0 = (com.google.ads.conversiontracking.C0431d) r0;	 Catch:{ all -> 0x002a }
            monitor-exit(r1);	 Catch:{ all -> 0x002a }
            if (r0 == 0) goto L_0x0007;
        L_0x0051:
            r1 = r6.f707b;	 Catch:{ InterruptedException -> 0x002d }
            r1 = r1.f710c;	 Catch:{ InterruptedException -> 0x002d }
            r2 = r0.f699e;	 Catch:{ InterruptedException -> 0x002d }
            r3 = r0.f700f;	 Catch:{ InterruptedException -> 0x002d }
            r4 = r0.f696b;	 Catch:{ InterruptedException -> 0x002d }
            r1 = com.google.ads.conversiontracking.C0444g.m1061a(r1, r2, r3, r4);	 Catch:{ InterruptedException -> 0x002d }
            if (r1 != 0) goto L_0x006d;
        L_0x0063:
            r1 = r6.f707b;	 Catch:{ InterruptedException -> 0x002d }
            r1 = r1.f713f;	 Catch:{ InterruptedException -> 0x002d }
            r1.m1011a(r0);	 Catch:{ InterruptedException -> 0x002d }
            goto L_0x0007;
        L_0x006d:
            r1 = r6.f707b;	 Catch:{ InterruptedException -> 0x002d }
            r1 = r1.m1004a(r0);	 Catch:{ InterruptedException -> 0x002d }
            r2 = 2;
            if (r1 != r2) goto L_0x0084;
        L_0x0076:
            r1 = r6.f707b;	 Catch:{ InterruptedException -> 0x002d }
            r1 = r1.f713f;	 Catch:{ InterruptedException -> 0x002d }
            r1.m1011a(r0);	 Catch:{ InterruptedException -> 0x002d }
            r0 = 0;
            r6.f706a = r0;	 Catch:{ InterruptedException -> 0x002d }
            goto L_0x0007;
        L_0x0084:
            if (r1 != 0) goto L_0x0099;
        L_0x0086:
            r1 = r6.f707b;	 Catch:{ InterruptedException -> 0x002d }
            r1 = r1.f713f;	 Catch:{ InterruptedException -> 0x002d }
            r1.m1015c(r0);	 Catch:{ InterruptedException -> 0x002d }
            r6.m994a();	 Catch:{ InterruptedException -> 0x002d }
            r0 = r6.f706a;	 Catch:{ InterruptedException -> 0x002d }
            java.lang.Thread.sleep(r0);	 Catch:{ InterruptedException -> 0x002d }
            goto L_0x0007;
        L_0x0099:
            r1 = r6.f707b;	 Catch:{ InterruptedException -> 0x002d }
            r1 = r1.f713f;	 Catch:{ InterruptedException -> 0x002d }
            r1.m1015c(r0);	 Catch:{ InterruptedException -> 0x002d }
            r0 = 0;
            r6.f706a = r0;	 Catch:{ InterruptedException -> 0x002d }
            goto L_0x0007;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.ads.conversiontracking.e.b.run():void");
        }

        private void m994a() {
            if (this.f706a == 0) {
                this.f706a = 1000;
            } else {
                this.f706a = Math.min(this.f706a * 2, 60000);
            }
        }
    }

    public C0435e(Context context) {
        this.f708a = new Object();
        this.f711d = true;
        this.f712e = false;
        this.f710c = context;
        this.f713f = new C0437f(context);
        this.f709b = new LinkedList();
        new Thread(new C0434b(this)).start();
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(1);
        long b = (C0444g.m1062b(context) + 300000) - C0444g.m1044a();
        Runnable c0433a = new C0433a();
        if (b <= 0) {
            b = 0;
        }
        scheduledThreadPoolExecutor.scheduleAtFixedRate(c0433a, b, 300000, TimeUnit.MILLISECONDS);
    }

    public void m1006a(String str, C0442c c0442c, boolean z, boolean z2, boolean z3) {
        C0431d c0431d = new C0431d(str, c0442c, z, z2);
        synchronized (this.f708a) {
            if (z3) {
                this.f713f.m1013b(c0431d);
                if (this.f712e && C0444g.m1067d(this.f710c)) {
                    this.f709b.add(c0431d);
                    this.f711d = true;
                    this.f708a.notify();
                }
                return;
            }
            m1005a(new C04321(this, c0431d));
        }
    }

    protected void m1005a(Runnable runnable) {
        new Thread(runnable).start();
    }

    protected int m1004a(C0431d c0431d) {
        HttpUriRequest httpGet;
        Throwable e;
        AndroidHttpClient newInstance = AndroidHttpClient.newInstance(System.getProperty("http.agent"), this.f710c);
        try {
            Log.i("GoogleConversionReporter", "Pinging: " + c0431d.f701g);
            httpGet = new HttpGet(c0431d.f701g);
            try {
                HttpResponse execute = newInstance.execute(httpGet);
                int statusCode = execute.getStatusLine().getStatusCode();
                Log.i("GoogleConversionReporter", "Receive response code " + statusCode);
                HttpEntity entity = execute.getEntity();
                if (entity != null) {
                    entity.consumeContent();
                }
                int i = statusCode == 200 ? 2 : 1;
                if (i == 2) {
                    m997b(c0431d);
                }
                newInstance.close();
                return i;
            } catch (IOException e2) {
                e = e2;
                try {
                    Log.e("GoogleConversionReporter", "Error sending ping", e);
                    if (httpGet != null) {
                        try {
                            httpGet.abort();
                        } catch (UnsupportedOperationException e3) {
                        }
                    }
                    newInstance.close();
                    return 0;
                } catch (Throwable th) {
                    newInstance.close();
                }
            }
        } catch (IOException e4) {
            e = e4;
            httpGet = null;
            Log.e("GoogleConversionReporter", "Error sending ping", e);
            if (httpGet != null) {
                httpGet.abort();
            }
            newInstance.close();
            return 0;
        }
    }

    private void m997b(C0431d c0431d) {
        if (!c0431d.f696b && c0431d.f695a) {
            C0444g.m1057a(this.f710c, c0431d.f699e, c0431d.f700f);
        }
    }
}
