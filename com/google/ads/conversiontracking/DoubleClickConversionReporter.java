package com.google.ads.conversiontracking;

import android.content.Context;

public class DoubleClickConversionReporter extends AdWordsConversionReporter {
    public DoubleClickConversionReporter(Context applicationContext, String conversionId, String label, String value, boolean isRepeatable) {
        super(applicationContext, conversionId, label, value, isRepeatable);
    }
}
