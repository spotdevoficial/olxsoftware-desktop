package com.google.ads.conversiontracking;

import android.content.Intent;
import android.net.Uri;
import com.facebook.share.internal.ShareConstants;
import com.google.android.gms.common.GooglePlayServicesUtil;

/* renamed from: com.google.ads.conversiontracking.o */
public class C0454o {
    private static final Uri f770a;
    private static final Uri f771b;

    static {
        f770a = Uri.parse("http://plus.google.com/");
        f771b = f770a.buildUpon().appendPath("circles").appendPath("find").build();
    }

    public static Intent m1086a() {
        return new Intent("android.settings.DATE_SETTINGS");
    }

    public static Intent m1087a(String str) {
        Uri fromParts = Uri.fromParts("package", str, null);
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(fromParts);
        return intent;
    }

    public static Intent m1088b(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(C0454o.m1090d(str));
        intent.setPackage(GooglePlayServicesUtil.GOOGLE_PLAY_STORE_PACKAGE);
        intent.addFlags(524288);
        return intent;
    }

    public static Intent m1089c(String str) {
        Uri parse = Uri.parse("bazaar://search?q=pname:" + str);
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(parse);
        intent.setFlags(524288);
        return intent;
    }

    private static Uri m1090d(String str) {
        return Uri.parse("market://details").buildUpon().appendQueryParameter(ShareConstants.WEB_DIALOG_PARAM_ID, str).build();
    }
}
