package com.google.ads.conversiontracking;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.urbanairship.C1608R;

/* renamed from: com.google.ads.conversiontracking.q */
public interface C0456q extends IInterface {

    /* renamed from: com.google.ads.conversiontracking.q.a */
    public static abstract class C0458a extends Binder implements C0456q {

        /* renamed from: com.google.ads.conversiontracking.q.a.a */
        private static class C0457a implements C0456q {
            private IBinder f772a;

            C0457a(IBinder iBinder) {
                this.f772a = iBinder;
            }

            public String m1096a() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                    this.f772a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    String readString = obtain2.readString();
                    return readString;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String m1097a(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                    obtain.writeString(str);
                    this.f772a.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                    String readString = obtain2.readString();
                    return readString;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m1098a(String str, boolean z) throws RemoteException {
                int i = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                    obtain.writeString(str);
                    if (z) {
                        i = 1;
                    }
                    obtain.writeInt(i);
                    this.f772a.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean m1099a(boolean z) throws RemoteException {
                boolean z2 = true;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                    obtain.writeInt(z ? 1 : 0);
                    this.f772a.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z2 = false;
                    }
                    obtain2.recycle();
                    obtain.recycle();
                    return z2;
                } catch (Throwable th) {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.f772a;
            }
        }

        public static C0456q m1100a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof C0456q)) ? new C0457a(iBinder) : (C0456q) queryLocalInterface;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            boolean z = false;
            String a;
            switch (code) {
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    data.enforceInterface("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                    a = m1092a();
                    reply.writeNoException();
                    reply.writeString(a);
                    return true;
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    int i;
                    data.enforceInterface("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                    boolean a2 = m1095a(data.readInt() != 0);
                    reply.writeNoException();
                    if (a2) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                    data.enforceInterface("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                    a = m1093a(data.readString());
                    reply.writeNoException();
                    reply.writeString(a);
                    return true;
                case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                    data.enforceInterface("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                    a = data.readString();
                    if (data.readInt() != 0) {
                        z = true;
                    }
                    m1094a(a, z);
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    String m1092a() throws RemoteException;

    String m1093a(String str) throws RemoteException;

    void m1094a(String str, boolean z) throws RemoteException;

    boolean m1095a(boolean z) throws RemoteException;
}
