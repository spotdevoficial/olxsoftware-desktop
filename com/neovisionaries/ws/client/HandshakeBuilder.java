package com.neovisionaries.ws.client;

import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Set;

class HandshakeBuilder {
    private List<WebSocketExtension> mExtensions;
    private List<String[]> mHeaders;
    private final String mHost;
    private String mKey;
    private final String mPath;
    private Set<String> mProtocols;
    private boolean mSecure;
    private final URI mUri;
    private String mUserInfo;

    public HandshakeBuilder(boolean secure, String userInfo, String host, String path) {
        this.mSecure = secure;
        this.mUserInfo = userInfo;
        this.mHost = host;
        this.mPath = path;
        String str = "%s://%s%s";
        Object[] objArr = new Object[3];
        objArr[0] = secure ? "wss" : "ws";
        objArr[1] = host;
        objArr[2] = path;
        this.mUri = URI.create(String.format(str, objArr));
    }

    public boolean containsProtocol(String protocol) {
        if (this.mProtocols == null) {
            return false;
        }
        return this.mProtocols.contains(protocol);
    }

    public boolean containsExtension(String extensionName) {
        if (this.mExtensions == null) {
            return false;
        }
        for (WebSocketExtension extension : this.mExtensions) {
            if (extension.getName().equals(extensionName)) {
                return true;
            }
        }
        return false;
    }

    public void setKey(String key) {
        this.mKey = key;
    }

    public String build() {
        StringBuilder builder = new StringBuilder().append("GET ").append(this.mPath).append(" HTTP/1.1").append("\r\n").append("Host: ").append(this.mHost).append("\r\n").append("Connection: Upgrade").append("\r\n").append("Upgrade: websocket").append("\r\n").append("Sec-WebSocket-Version: 13").append("\r\n").append("Sec-WebSocket-Key: ").append(this.mKey).append("\r\n");
        append(builder, "Sec-WebSocket-Protocol", this.mProtocols);
        append(builder, "Sec-WebSocket-Extensions", this.mExtensions);
        append(builder, this.mHeaders);
        if (!(this.mUserInfo == null || this.mUserInfo.length() == 0)) {
            builder.append("Authorization: Basic ").append(Base64.encode(this.mUserInfo)).append("\r\n");
        }
        return builder.append("\r\n").toString();
    }

    private static void append(StringBuilder builder, String name, Collection<?> values) {
        if (values != null && values.size() != 0) {
            builder.append(name).append(": ");
            join(builder, values, ", ");
            builder.append("\r\n");
        }
    }

    private static void join(StringBuilder builder, Collection<?> values, String delimiter) {
        boolean first = true;
        for (Object value : values) {
            if (first) {
                first = false;
            } else {
                builder.append(delimiter);
            }
            builder.append(value.toString());
        }
    }

    private static void append(StringBuilder builder, List<String[]> pairs) {
        if (pairs != null && pairs.size() != 0) {
            for (String[] pair : pairs) {
                builder.append(pair[0]).append(": ").append(pair[1]).append("\r\n");
            }
        }
    }
}
