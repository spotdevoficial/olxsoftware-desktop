package com.neovisionaries.ws.client;

import com.schibsted.scm.nextgenapp.C1061R;
import com.urbanairship.C1608R;

class Token {
    public static boolean isValid(String token) {
        if (token == null || token.length() == 0) {
            return false;
        }
        int len = token.length();
        for (int i = 0; i < len; i++) {
            if (isSeparator(token.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static boolean isSeparator(char ch) {
        switch (ch) {
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
            case C1061R.styleable.Theme_actionModeCloseDrawable /*32*/:
            case C1061R.styleable.Theme_actionModeCopyDrawable /*34*/:
            case C1061R.styleable.Theme_actionModePopupWindowStyle /*40*/:
            case C1061R.styleable.Theme_textAppearanceLargePopupMenu /*41*/:
            case C1061R.styleable.Theme_dialogPreferredPadding /*44*/:
            case C1061R.styleable.Theme_dropdownListPreferredItemHeight /*47*/:
            case C1061R.styleable.Theme_activityChooserViewStyle /*58*/:
            case C1061R.styleable.Theme_toolbarStyle /*59*/:
            case C1061R.styleable.Theme_toolbarNavigationButtonStyle /*60*/:
            case C1061R.styleable.Theme_popupMenuStyle /*61*/:
            case C1061R.styleable.Theme_popupWindowStyle /*62*/:
            case C1061R.styleable.Theme_editTextColor /*63*/:
            case C1061R.styleable.Theme_editTextBackground /*64*/:
            case C1061R.styleable.Theme_alertDialogButtonGroupStyle /*91*/:
            case C1061R.styleable.Theme_alertDialogCenterButtons /*92*/:
            case C1061R.styleable.Theme_alertDialogTheme /*93*/:
            case '{':
            case '}':
                return true;
            default:
                return false;
        }
    }

    public static String unquote(String text) {
        if (text == null) {
            return null;
        }
        int len = text.length();
        return (len >= 2 && text.charAt(0) == '\"' && text.charAt(len - 1) == '\"') ? unescape(text.substring(1, len - 1)) : text;
    }

    public static String unescape(String text) {
        if (text == null) {
            return null;
        }
        if (text.indexOf(92) < 0) {
            return text;
        }
        int len = text.length();
        boolean escaped = false;
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < len; i++) {
            char ch = text.charAt(i);
            if (ch != '\\' || escaped) {
                escaped = false;
                builder.append(ch);
            } else {
                escaped = true;
            }
        }
        return builder.toString();
    }
}
