package com.kontagent.queue;

import com.kontagent.KontagentLog;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class MessageStackMonitor implements Observer {
    private final Map messages;
    private IMessageStackMonitorListener monitorListener;
    private String sessionId;

    public MessageStackMonitor(String str) {
        this.messages = new HashMap();
        this.sessionId = str;
    }

    public void addMessage(Message message) {
        if (message != null) {
            KontagentLog.m1113d(String.format("Adding message: %s", new Object[]{message.toString()}));
            message.addObserver(this);
            this.messages.put(message.getId(), message);
            if (this.monitorListener != null) {
                this.monitorListener.onMessageAdded(this, message);
            }
        }
    }

    public Message getMessageById(Long l) {
        return (Message) this.messages.get(l);
    }

    public void removeMessage(Long l) {
        Message message = (Message) this.messages.remove(l);
        if (this.monitorListener != null && message != null) {
            KontagentLog.m1113d(String.format("Removing message: %s", new Object[]{message.toString()}));
            this.monitorListener.onMessageRemoved(this, message);
            message.deleteObserver(this);
        }
    }

    public MessageStackMonitor setMonitorListener(IMessageStackMonitorListener iMessageStackMonitorListener) {
        this.monitorListener = iMessageStackMonitorListener;
        return this;
    }

    public void syncMessagesWithInternalDatabaseOnStartup(List list) {
        if (list != null) {
            for (Message message : list) {
                this.messages.put(message.getId(), message);
            }
        }
    }

    public void update(Observable observable, Object obj) {
        Message message = (Message) observable;
        if (this.monitorListener != null && message != null) {
            KontagentLog.m1113d(String.format("Updating message: %s", new Object[]{message.toString()}));
            this.monitorListener.onMessageStatusChanged(this, message);
        }
    }
}
