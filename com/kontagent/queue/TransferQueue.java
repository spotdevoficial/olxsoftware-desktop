package com.kontagent.queue;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.kontagent.KontagentLog;
import com.kontagent.connectivity.ConnectivityListener;
import com.kontagent.connectivity.ConnectivityTracker;
import com.kontagent.network.asynchttpclient.AsyncHttpClient;
import com.kontagent.network.asynchttpclient.AsyncHttpResponseHandler;
import com.kontagent.util.Waiter;
import com.urbanairship.C1608R;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class TransferQueue implements ConnectivityListener, IKTQueue {
    private static final String TAG;
    private final long ERROR_CASE_SEND_OPERATION_INTERVAL;
    private final int MSG_CLEAR_MESSAGE_QUEUE;
    private final int MSG_ENQUEUE_MESSAGE;
    private final int MSG_PROCESS_QUEUE;
    private final int MSG_QUIT;
    private final int MSG_REMOVE_MESSAGE;
    private final long RETRY_CASE_SEND_OPERATION_INTERVAL;
    private final ConnectivityTracker connectivityTracker;
    private final Context context;
    private DBHelper dbHelper;
    private String dbName;
    private boolean hasNetwork;
    private final AsyncHttpClient httpClient;
    private Handler mHandler;
    private RunnerThread mRunner;
    private int maxQueueSize;
    private String queueId;
    private final Runnable queueProcessTimer;
    private boolean runnerIsRunning;
    private ITransferQueueListener transferQueueListener;

    /* renamed from: com.kontagent.queue.TransferQueue.1 */
    class C10041 implements Runnable {
        C10041() {
        }

        public void run() {
            TransferQueue.this.doProcessQueue();
        }
    }

    public class DBHelper {
        private static final String TAG;
        private final SQLiteDatabase db;
        private final SQLiteStatement deleteStmt;
        private final SQLiteStatement insertStmt;

        class OpenHelper extends SQLiteOpenHelper {
            OpenHelper(Context context, String str) {
                super(context, str, null, 3);
            }

            public void onCreate(SQLiteDatabase sQLiteDatabase) {
                sQLiteDatabase.execSQL("CREATE TABLE api_calls (id INTEGER PRIMARY KEY, url TEXT, name TEXT, session_id TEXT, timestamp TEXT, msg_id INTEGER)");
            }

            public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            }
        }

        static {
            TAG = DBHelper.class.getSimpleName();
        }

        public DBHelper(Context context, String str) {
            this.db = new OpenHelper(context, str).getWritableDatabase();
            this.insertStmt = this.db.compileStatement("insert into api_calls(url,name,session_id,timestamp,msg_id) values (?,?,?,?,?)".toUpperCase(Locale.US));
            this.deleteStmt = this.db.compileStatement("delete from api_calls where msg_id=?");
        }

        public void close() {
            try {
                if (this.insertStmt != null) {
                    this.insertStmt.close();
                } else {
                    KontagentLog.m1113d("Unable to close insert statement.");
                }
                if (this.deleteStmt != null) {
                    this.deleteStmt.close();
                } else {
                    KontagentLog.m1113d("Unable to close delete statement.");
                }
                if (this.db != null) {
                    this.db.close();
                } else {
                    KontagentLog.m1113d("Unable to close database");
                }
            } catch (Throwable e) {
                KontagentLog.m1116e(String.format("Error in close() : %s", new Object[]{e.getMessage()}), e);
            }
        }

        public void deleteAll() {
            try {
                this.db.delete("api_calls", null, null);
            } catch (Throwable e) {
                Log.e(TAG, "Failed to delete offline records.", e);
            }
        }

        public long insert(Message message) {
            long j = -1;
            try {
                this.insertStmt.bindString(1, message.getUrl());
                this.insertStmt.bindString(2, message.getName());
                this.insertStmt.bindString(3, message.getSessionId());
                this.insertStmt.bindString(4, message.getTimestamp());
                this.insertStmt.bindLong(5, message.getId().longValue());
                j = this.insertStmt.executeInsert();
            } catch (Throwable e) {
                KontagentLog.m1116e(String.format("Error in insert() with message=%s: %s", new Object[]{message.toString(), e.getMessage()}), e);
            }
            return j;
        }

        public Message peek() {
            Message message;
            Throwable e;
            try {
                Cursor rawQuery = this.db.rawQuery("select id,url,name,session_id,timestamp,msg_id from api_calls where id=(select min(id) from api_calls)", null);
                if (rawQuery == null) {
                    return null;
                }
                message = rawQuery.moveToFirst() ? new Message(rawQuery.getString(1), rawQuery.getString(2), rawQuery.getString(3), rawQuery.getString(4), Long.valueOf(rawQuery.getLong(5))) : null;
                try {
                    if (rawQuery.isClosed()) {
                        return message;
                    }
                    rawQuery.close();
                    return message;
                } catch (Exception e2) {
                    e = e2;
                    KontagentLog.m1116e(String.format("Error in peek() : %s", new Object[]{e.getMessage()}), e);
                    return message;
                }
            } catch (Throwable e3) {
                e = e3;
                message = null;
                KontagentLog.m1116e(String.format("Error in peek() : %s", new Object[]{e.getMessage()}), e);
                return message;
            }
        }

        public List peekAll() {
            List arrayList = new ArrayList();
            try {
                Cursor rawQuery = this.db.rawQuery("select url,name,session_id,timestamp,msg_id from api_calls", null);
                if (rawQuery != null) {
                    if (rawQuery.moveToFirst()) {
                        do {
                            arrayList.add(new Message(rawQuery.getString(0), rawQuery.getString(1), rawQuery.getString(2), rawQuery.getString(3), Long.valueOf(rawQuery.getLong(4))));
                        } while (rawQuery.moveToNext());
                    }
                    if (!rawQuery.isClosed()) {
                        rawQuery.close();
                    }
                }
            } catch (Throwable e) {
                KontagentLog.m1116e(String.format("Error in peekAll() : %s", new Object[]{e.getMessage()}), e);
            }
            return arrayList;
        }

        public int queueSize() {
            Throwable e;
            int i;
            try {
                Cursor rawQuery = this.db.rawQuery("select count(*) from api_calls", null);
                if (rawQuery == null) {
                    return 0;
                }
                i = rawQuery.moveToFirst() ? rawQuery.getInt(0) : 0;
                try {
                    if (rawQuery.isClosed()) {
                        return i;
                    }
                    rawQuery.close();
                    return i;
                } catch (Exception e2) {
                    e = e2;
                    KontagentLog.m1116e(String.format("Error in queueSize() : %s", new Object[]{e.getMessage()}), e);
                    return i;
                }
            } catch (Throwable e3) {
                e = e3;
                i = 0;
                KontagentLog.m1116e(String.format("Error in queueSize() : %s", new Object[]{e.getMessage()}), e);
                return i;
            }
        }

        public void removeMessage(Long l) {
            try {
                this.deleteStmt.bindLong(1, l.longValue());
                this.deleteStmt.execute();
            } catch (Throwable e) {
                KontagentLog.m1116e(String.format("Error in removeMessage() with ID=%s: %s", new Object[]{l, e.getMessage()}), e);
            }
        }
    }

    class KTHttpResponseHandler extends AsyncHttpResponseHandler {
        public KTHttpResponseHandler(Message message) {
            super(message);
        }

        public void onMessageFailure(Message message) {
            KontagentLog.m1117i(String.format("FAILED to complete HTTP operation => re-set timer to %s seconds", new Object[]{Long.valueOf(10000)}));
            if (!(TransferQueue.this.transferQueueListener == null || message == null)) {
                TransferQueue.this.transferQueueListener.queueDidTransferElementFailed(TransferQueue.this, message.getId());
            }
            TransferQueue.this.setupTimer(10000);
        }

        public void onMessageSuccess(Message message) {
            KontagentLog.m1117i("HTTP operation completed successfully => schedule processing of next operation...");
            if (message != null) {
                TransferQueue.this.stopTimer();
                TransferQueue.this.removeMessage(message.getId());
            }
        }
    }

    class RunnerThread extends Thread {

        /* renamed from: com.kontagent.queue.TransferQueue.RunnerThread.1 */
        class C10051 extends Handler {
            C10051() {
            }

            public void handleMessage(Message message) {
                switch (message.what) {
                    case C1608R.styleable.MapAttrs_mapType /*0*/:
                        TransferQueue.this.doProcessQueue();
                    case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                        TransferQueue.this.stopTimer();
                        TransferQueue.this.connectivityTracker.stopTracking();
                        TransferQueue.this.connectivityTracker.setListener(null);
                        TransferQueue.this.httpClient.cancelRequests(TransferQueue.this.context, true);
                        TransferQueue.this.dbHelper.close();
                        TransferQueue.this.dbHelper = null;
                        TransferQueue.this.quitLooper();
                    case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                        TransferQueue.this.doEnqueueMessage((Message) message.obj);
                    case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                        TransferQueue.this.doRemoveMessage((Long) message.obj);
                    case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                        TransferQueue.this.doClear();
                    default:
                }
            }
        }

        private RunnerThread() {
            super("KontagentTransferQueueThread");
        }

        public void run() {
            Looper.prepare();
            TransferQueue.this.mHandler = new C10051();
            KontagentLog.m1113d("Starting queue processing loop...");
            int i = 0;
            do {
                try {
                    KontagentLog.m1113d("Instantiating SQLite DB");
                    TransferQueue.this.dbHelper = new DBHelper(TransferQueue.this.context, TransferQueue.this.dbName);
                    break;
                } catch (Throwable e) {
                    KontagentLog.m1113d("Error instantiating SQLite DB on try: " + i);
                    i++;
                    if (i >= 3) {
                        KontagentLog.m1116e("Error instantiating SQLite DB after 3 tries. Messages will not be enqueued.", e);
                        TransferQueue.this.quitLooper();
                    }
                    try {
                        Thread.sleep(500);
                        continue;
                    } catch (InterruptedException e2) {
                        continue;
                    }
                    if (i >= 3) {
                    }
                }
            } while (i >= 3);
            Waiter.getInstance().notifyOperationCompleted();
            if (TransferQueue.this.transferQueueListener != null) {
                TransferQueue.this.transferQueueListener.queueDidStart(TransferQueue.this);
            }
            Looper.loop();
            KontagentLog.m1114d(TransferQueue.TAG, String.format("Message queue [id=%s] has been stopped.", new Object[]{TransferQueue.this.queueId}));
            if (TransferQueue.this.transferQueueListener != null) {
                TransferQueue.this.transferQueueListener.queueDidStop(TransferQueue.this);
            }
        }

        public synchronized void start() {
            super.start();
            TransferQueue.this.runnerIsRunning = true;
        }
    }

    static {
        TAG = TransferQueue.class.getSimpleName();
    }

    public TransferQueue(Context context, String str) {
        this.ERROR_CASE_SEND_OPERATION_INTERVAL = 10000;
        this.RETRY_CASE_SEND_OPERATION_INTERVAL = 1000;
        this.MSG_PROCESS_QUEUE = 0;
        this.MSG_QUIT = 1;
        this.MSG_ENQUEUE_MESSAGE = 2;
        this.MSG_REMOVE_MESSAGE = 3;
        this.MSG_CLEAR_MESSAGE_QUEUE = 5;
        this.maxQueueSize = 500;
        this.queueProcessTimer = new C10041();
        if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("queueId must be either not null or have positive length");
        }
        this.context = context;
        this.queueId = str;
        this.connectivityTracker = new ConnectivityTracker(this.context);
        this.hasNetwork = true;
        this.httpClient = new AsyncHttpClient();
        this.httpClient.setThreadPool(new ThreadPoolExecutor(1, 1, 0, TimeUnit.MILLISECONDS, new LinkedBlockingQueue()));
        this.runnerIsRunning = false;
        this.dbName = this.queueId;
    }

    private void doClear() {
        KontagentLog.m1114d(TAG, "clear()");
        if (this.runnerIsRunning) {
            this.dbHelper.deleteAll();
        }
    }

    private void doEnqueueMessage(Message message) {
        KontagentLog.m1113d("Trying to enqueue a message: " + message.toString());
        if (!this.runnerIsRunning) {
            return;
        }
        if (queueSize() > this.maxQueueSize) {
            KontagentLog.m1120w("Cannot enqueue a message - queue is full. Dropped message: " + message.toString(), null);
        } else if (this.dbHelper.insert(message) == -1) {
            KontagentLog.m1116e("Cannot enqueue a message - failed to insert a row into internal DB. Dropped message: " + message.toString(), null);
        } else {
            if (this.transferQueueListener != null) {
                this.transferQueueListener.queueDidAddMessage(this, message);
            }
            setupTimer(2000);
        }
    }

    private void doProcessQueue() {
        KontagentLog.m1113d("doProcessQueue()");
        if (!this.runnerIsRunning) {
            return;
        }
        if (!this.hasNetwork) {
            KontagentLog.m1118i(TAG, "No network available, scheduling timer...");
            setupTimer(10000);
        } else if (this.httpClient.isRunning()) {
            setupTimer(1000);
        } else {
            Message peek = this.dbHelper.peek();
            if (peek != null) {
                KontagentLog.m1118i(TAG, String.format("Processing message: [session ID = %s, message ID = %s, URL = %s]", new Object[]{peek.getSessionId(), peek.getId(), peek.getUrl()}));
                this.httpClient.get(peek.getUrl(), new KTHttpResponseHandler(peek));
            } else if (this.transferQueueListener != null) {
                this.transferQueueListener.queueDidFinishedProcessing(this);
            }
        }
    }

    private void doRemoveMessage(Long l) {
        if (this.runnerIsRunning) {
            KontagentLog.m1113d("Trying to remove a message id: " + l.toString());
            this.dbHelper.removeMessage(l);
            if (this.transferQueueListener != null) {
                this.transferQueueListener.queueDidRemoveMessage(this, l);
            }
            doProcessQueue();
        }
    }

    private void processQueue() {
        if (this.mHandler != null) {
            this.mHandler.sendEmptyMessage(0);
        }
    }

    private void quitLooper() {
        KontagentLog.m1114d(TAG, "quitLooper() invoked");
        this.mHandler.getLooper().quit();
        this.mHandler = null;
        this.mRunner = null;
        this.runnerIsRunning = false;
    }

    private void removeMessage(Long l) {
        if (this.mHandler != null) {
            Message message = new Message();
            message.what = 3;
            message.obj = l;
            this.mHandler.sendMessage(message);
        }
    }

    private void setupTimer(long j) {
        stopTimer();
        if (this.mHandler != null) {
            this.mHandler.postDelayed(this.queueProcessTimer, j);
        }
    }

    private void stopTimer() {
        if (this.mHandler != null) {
            KontagentLog.m1114d(TAG, "Stopping queue processing timer");
            this.mHandler.removeCallbacks(this.queueProcessTimer);
        }
    }

    public void enqueue(Message message) {
        if (this.mHandler != null) {
            Message message2 = new Message();
            message2.what = 2;
            message2.obj = message;
            this.mHandler.sendMessage(message2);
            return;
        }
        KontagentLog.m1116e("Unable to enqueue a message! Run loop hasn't been yet started", null);
    }

    public void onConnectivityChanged(ConnectivityTracker connectivityTracker, boolean z) {
        if (this.transferQueueListener != null) {
            this.transferQueueListener.queueDidReachabilityChanged(z);
        }
        this.hasNetwork = z;
        processQueue();
    }

    public List peekAll() {
        return this.dbHelper == null ? null : this.dbHelper.peekAll();
    }

    public int queueSize() {
        return (this.mRunner == null || this.mHandler == null || this.runnerIsRunning) ? this.dbHelper.queueSize() : 0;
    }

    public TransferQueue setTransferQueueListener(ITransferQueueListener iTransferQueueListener) {
        this.transferQueueListener = iTransferQueueListener;
        KontagentLog.m1114d(TAG, String.format("New transfer queue listener set: %s", new Object[]{iTransferQueueListener}));
        return this;
    }

    public void start() {
        KontagentLog.m1114d(TAG, "start()");
        this.connectivityTracker.startTracking();
        this.connectivityTracker.setListener(this);
        if (this.mRunner == null) {
            this.mRunner = new RunnerThread();
            this.mRunner.start();
            KontagentLog.m1113d("mRunner.start()");
        }
        processQueue();
    }
}
