package com.kontagent.connectivity;

public interface ConnectivityListener {
    void onConnectivityChanged(ConnectivityTracker connectivityTracker, boolean z);
}
