package com.kontagent.util;

import java.util.Map;

public interface AnalyticsMessageTransformer {
    void transform(String str, Map map);
}
