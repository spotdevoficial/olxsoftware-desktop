package com.kontagent.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.facebook.share.internal.ShareConstants;
import com.kontagent.KontagentLog;
import java.util.Map;
import org.json.JSONObject;

public class SharedPreferencesMigrationTransformer implements AnalyticsMessageTransformer {
    private static final String TAG;
    private final String mApiKey;
    private final SharedPreferences mCurrentSharedPreferences;
    private final SharedPreferences mLegacySharedPreferences;
    private boolean mShouldTransform;

    static {
        TAG = SharedPreferencesMigrationTransformer.class.getSimpleName();
    }

    public SharedPreferencesMigrationTransformer(Context context, String str) {
        boolean z;
        this.mLegacySharedPreferences = context.getSharedPreferences("kontagent", 0);
        this.mCurrentSharedPreferences = context.getSharedPreferences("Kontagent", 0);
        this.mApiKey = str;
        String string = this.mLegacySharedPreferences.getString(senderIdKey(str), null);
        CharSequence string2 = this.mCurrentSharedPreferences.getString(senderIdKey(str), null);
        if (!TextUtils.isEmpty(string)) {
            if (TextUtils.isEmpty(string2) || string.equals(string2)) {
                migrateSenderId(str, string);
            } else {
                z = true;
                this.mShouldTransform = z;
                if (this.mShouldTransform) {
                    KontagentLog.m1114d(TAG, String.format("Detected Remote Sender Id Migration (%s) -> (%s)", new Object[]{string, string2}));
                }
            }
        }
        z = false;
        this.mShouldTransform = z;
        if (this.mShouldTransform) {
            KontagentLog.m1114d(TAG, String.format("Detected Remote Sender Id Migration (%s) -> (%s)", new Object[]{string, string2}));
        }
    }

    public static String applicationAddedKey(String str) {
        return String.format("%s_%s", new Object[]{str, "is_apa_sent"});
    }

    private void migrateSenderId(String str, String str2) {
        KontagentLog.m1114d(TAG, String.format("Detected Local Sender Id Migration (%s) -> (%s)", new Object[]{str2, str2}));
        this.mLegacySharedPreferences.edit().clear().commit();
        this.mCurrentSharedPreferences.edit().putString(senderIdKey(str), str2).putBoolean(applicationAddedKey(str), true).commit();
        KontagentLog.m1114d(TAG, "Local Sender Id Migration Completed:" + this.mCurrentSharedPreferences);
    }

    public static String senderIdKey(String str) {
        return String.format("%s.%s", new Object[]{"keySessionSenderId", str});
    }

    public void transform(String str, Map map) {
        if ("pgr".equals(str) && this.mShouldTransform) {
            CharSequence string = this.mLegacySharedPreferences.getString(senderIdKey(this.mApiKey), null);
            if (!TextUtils.isEmpty(string)) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("_oldsid", string);
                    map.put(ShareConstants.WEB_DIALOG_PARAM_DATA, Base64.encodeToString(jSONObject.toString().getBytes(), 0));
                    if (this.mLegacySharedPreferences.edit().clear().commit()) {
                        KontagentLog.m1114d(TAG, "Remote Sender Id Migration Completed");
                        this.mShouldTransform = false;
                    }
                } catch (Throwable e) {
                    KontagentLog.m1116e("Unable to convert legacy sender id to json", e);
                }
            }
        }
    }
}
