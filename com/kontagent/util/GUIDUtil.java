package com.kontagent.util;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import com.facebook.BuildConfig;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.kontagent.KontagentLog;
import com.kontagent.deps.bE;
import com.kontagent.deps.bj;
import com.kontagent.deps.bs;
import com.kontagent.facebook.KontagentFBLib;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class GUIDUtil {
    private static String androidId;
    private static String bundleID;

    /* renamed from: com.kontagent.util.GUIDUtil.1 */
    final class C10121 implements bE {
        final /* synthetic */ Context val$context;

        C10121(Context context) {
            this.val$context = context;
        }

        public final void call(bs bsVar) {
            try {
                if (GUIDUtil.isGooglePlayAvailable()) {
                    bsVar.onNext(AdvertisingIdClient.getAdvertisingIdInfo(this.val$context).getId());
                    bsVar.onCompleted();
                    return;
                }
                bsVar.onError(new IllegalStateException("Google Play Services not found!"));
            } catch (Throwable e) {
                bsVar.onError(e);
            }
        }
    }

    private static String byteArrayToHexString(byte[] bArr) {
        StringBuilder stringBuilder = new StringBuilder();
        for (byte b : bArr) {
            stringBuilder.append(Integer.toString((b & 255) + 256, 16).substring(1));
        }
        return stringBuilder.toString();
    }

    public static String createDeviceIdInformation(Context context) {
        StringBuilder stringBuilder = new StringBuilder();
        context.getSystemService("phone");
        androidId = Secure.getString(context.getContentResolver(), "android_id");
        bundleID = context.getPackageName();
        if (androidId != null) {
            stringBuilder.append(androidId.trim()).append(",");
            String md5Hex = md5Hex(androidId.toLowerCase());
            if (md5Hex != BuildConfig.VERSION_NAME) {
                stringBuilder.append(md5Hex).append(",");
            }
            md5Hex = md5Hex(androidId.toUpperCase());
            if (md5Hex != BuildConfig.VERSION_NAME) {
                stringBuilder.append(md5Hex).append(",");
            }
            md5Hex = sha1Hex(androidId.toLowerCase());
            if (md5Hex != BuildConfig.VERSION_NAME) {
                stringBuilder.append(md5Hex).append(",");
            }
            md5Hex = sha1Hex(androidId.toUpperCase());
            if (md5Hex != BuildConfig.VERSION_NAME) {
                stringBuilder.append(md5Hex).append(",");
            }
            md5Hex = sha256Hex(androidId.toLowerCase());
            if (md5Hex != BuildConfig.VERSION_NAME) {
                stringBuilder.append(md5Hex).append(",");
            }
            md5Hex = sha256Hex(androidId.toUpperCase());
            if (md5Hex != BuildConfig.VERSION_NAME) {
                stringBuilder.append(md5Hex).append(",");
            }
        }
        return stringBuilder.toString();
    }

    public static Long generateSenderId() {
        return Long.valueOf(Math.abs(UUID.randomUUID().getLeastSignificantBits()));
    }

    public static String generateShortTrackingId(Context context, String str, String str2) {
        if (context != null && !TextUtils.isEmpty(str)) {
            return generateShortTrackingId(context, str, str2, getDeviceMacAddress(context));
        }
        KontagentLog.m1119w("Both context and apiKey params must be not null");
        return null;
    }

    private static String generateShortTrackingId(Context context, String str, String str2, String str3) {
        StringBuilder stringBuilder = new StringBuilder();
        if (str3 != null) {
            stringBuilder.append(hashTrackingTag(str3, str) + ",");
        }
        Object pendingCookie = KontagentFBLib.getPendingCookie();
        if (!TextUtils.isEmpty(pendingCookie)) {
            stringBuilder.append(hashTrackingTag(pendingCookie, str) + ",");
        }
        if (str2 != null) {
            stringBuilder.append(hashTrackingTag(str2, str) + ",");
        }
        String createDeviceIdInformation = createDeviceIdInformation(context);
        if (createDeviceIdInformation != null) {
            stringBuilder.append(createDeviceIdInformation);
        }
        return (stringBuilder.length() <= 0 || stringBuilder.charAt(stringBuilder.length() - 1) != ',') ? stringBuilder.toString() : stringBuilder.substring(0, stringBuilder.length() - 1);
    }

    public static String getAndroidId(Context context) {
        if (!TextUtils.isEmpty(androidId)) {
            return androidId;
        }
        String string = Secure.getString(context.getContentResolver(), "android_id");
        androidId = string;
        return string;
    }

    public static String getBundleId(Context context) {
        if (!TextUtils.isEmpty(bundleID)) {
            return bundleID;
        }
        String packageName = context.getPackageName();
        bundleID = packageName;
        return packageName;
    }

    private static String getDeviceMacAddress(Context context) {
        Object macAddress;
        try {
            macAddress = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
        } catch (SecurityException e) {
            macAddress = Secure.getString(context.getContentResolver(), "android_id");
        }
        if (TextUtils.isEmpty(macAddress)) {
            KontagentLog.m1120w("Device's MAC address is null - possibly due to running under emulator", null);
        }
        return macAddress;
    }

    public static bj getGoogleAID(Context context) {
        return bj.m1505a(new C10121(context));
    }

    public static String hashTrackingTag(String str, String str2) {
        return md5Hex(String.format("%s%s", new Object[]{sanitizeTrackingTag(str), str2}));
    }

    public static boolean isGooglePlayAvailable() {
        try {
            Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient");
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    public static String md5Hex(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            try {
                return byteArrayToHexString(instance.digest());
            } catch (Exception e) {
                e.printStackTrace();
                return BuildConfig.VERSION_NAME;
            }
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
        }
    }

    public static String sanitizeTrackingTag(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        KontagentLog.m1113d(String.format("SANITIZED CUSTOM ID(%s)=%s)", new Object[]{str, str.replaceAll("[^01234567890abcdefABCDEF]", BuildConfig.VERSION_NAME).toUpperCase()}));
        return str.replaceAll("[^01234567890abcdefABCDEF]", BuildConfig.VERSION_NAME).toUpperCase();
    }

    private static String sha1Hex(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.update(str.getBytes());
            try {
                return byteArrayToHexString(instance.digest());
            } catch (Exception e) {
                e.printStackTrace();
                return BuildConfig.VERSION_NAME;
            }
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
        }
    }

    private static String sha256Hex(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            instance.update(str.getBytes());
            try {
                return byteArrayToHexString(instance.digest());
            } catch (Exception e) {
                e.printStackTrace();
                return BuildConfig.VERSION_NAME;
            }
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
        }
    }
}
