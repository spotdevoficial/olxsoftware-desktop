package com.kontagent.util;

public class NetworkConnectivityError extends Exception {
    public NetworkConnectivityError(String str) {
        super(str);
    }

    public NetworkConnectivityError(String str, Throwable th) {
        super(str, th);
    }

    public NetworkConnectivityError(Throwable th) {
        super(th);
    }
}
