package com.kontagent.configuration;

import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;

public class DynamicConfiguration {
    static final DynamicConfiguration SDEFAULT_CONFIG;
    final boolean mIsSDKEnabled;
    final String mProductionApiServerUrl;
    final String mTestApiServerUrl;

    static {
        SDEFAULT_CONFIG = new DynamicConfiguration("api.geo.kontagent.net", "test-server.kontagent.com", true);
    }

    public DynamicConfiguration(String str, String str2, boolean z) {
        if (TextUtils.isEmpty(str)) {
            this.mProductionApiServerUrl = SDEFAULT_CONFIG.getProductionApiServerUrl();
        } else {
            this.mProductionApiServerUrl = str;
        }
        if (TextUtils.isEmpty(str2)) {
            this.mTestApiServerUrl = SDEFAULT_CONFIG.getTestApiServerUrl();
        } else {
            this.mTestApiServerUrl = str2;
        }
        this.mIsSDKEnabled = z;
    }

    public static final boolean toBoolean(String str, boolean z) {
        return AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(str) ? true : AppEventsConstants.EVENT_PARAM_VALUE_NO.equals(str) ? false : z;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        DynamicConfiguration dynamicConfiguration = (DynamicConfiguration) obj;
        if (this.mIsSDKEnabled != dynamicConfiguration.mIsSDKEnabled) {
            return false;
        }
        if (this.mProductionApiServerUrl == null ? dynamicConfiguration.mProductionApiServerUrl != null : !this.mProductionApiServerUrl.equals(dynamicConfiguration.mProductionApiServerUrl)) {
            return false;
        }
        if (this.mTestApiServerUrl != null) {
            if (this.mTestApiServerUrl.equals(dynamicConfiguration.mTestApiServerUrl)) {
                return true;
            }
        } else if (dynamicConfiguration.mTestApiServerUrl == null) {
            return true;
        }
        return false;
    }

    public String getProductionApiServerUrl() {
        return this.mProductionApiServerUrl;
    }

    public String getTestApiServerUrl() {
        return this.mTestApiServerUrl;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.mTestApiServerUrl != null ? this.mTestApiServerUrl.hashCode() : 0) + ((this.mProductionApiServerUrl != null ? this.mProductionApiServerUrl.hashCode() : 0) * 31)) * 31;
        if (this.mIsSDKEnabled) {
            i = 1;
        }
        return hashCode + i;
    }

    public boolean isSDKEnabled() {
        return this.mIsSDKEnabled;
    }
}
