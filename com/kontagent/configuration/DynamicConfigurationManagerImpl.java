package com.kontagent.configuration;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.kontagent.deps.bE;
import com.kontagent.deps.bj;
import com.kontagent.deps.bs;
import com.kontagent.deps.ca;
import java.util.concurrent.ConcurrentHashMap;

public class DynamicConfigurationManagerImpl implements IDynamicConfigurationManager {
    private final Context mApplicationContext;
    private final ConcurrentHashMap mConfigurationMap;
    private final IDynamicConfigurationService mConfigurationService;

    /* renamed from: com.kontagent.configuration.DynamicConfigurationManagerImpl.1 */
    class C09451 implements bE {
        final /* synthetic */ String val$apiKey;
        final /* synthetic */ int val$version;

        C09451(String str, int i) {
            this.val$apiKey = str;
            this.val$version = i;
        }

        public void call(DynamicConfiguration dynamicConfiguration) {
            DynamicConfigurationManagerImpl.this.onConfigurationUpdated(this.val$apiKey, this.val$version, dynamicConfiguration);
        }
    }

    /* renamed from: com.kontagent.configuration.DynamicConfigurationManagerImpl.2 */
    class C09462 implements bE {
        final /* synthetic */ String val$apiKey;
        final /* synthetic */ String val$compositeKey;
        final /* synthetic */ int val$version;

        C09462(String str, int i, String str2) {
            this.val$apiKey = str;
            this.val$version = i;
            this.val$compositeKey = str2;
        }

        public void call(bs bsVar) {
            Object access$100 = DynamicConfigurationManagerImpl.this.loadConfigFromCache(this.val$apiKey, this.val$version);
            if (access$100 != null) {
                DynamicConfigurationManagerImpl.this.mConfigurationMap.put(this.val$compositeKey, access$100);
            } else {
                access$100 = DynamicConfiguration.SDEFAULT_CONFIG;
            }
            bsVar.onNext(access$100);
            bsVar.onCompleted();
        }
    }

    public DynamicConfigurationManagerImpl(Context context, IDynamicConfigurationService iDynamicConfigurationService) {
        this.mConfigurationMap = new ConcurrentHashMap();
        this.mApplicationContext = context;
        this.mConfigurationService = iDynamicConfigurationService;
    }

    private bj createLoadFromLocalCacheObservable(String str, int i) {
        return bj.m1505a(new C09462(str, i, String.format("%s.%d", new Object[]{str, Integer.valueOf(i)})));
    }

    private DynamicConfiguration loadConfigFromCache(String str, int i) {
        SharedPreferences sharedPreferences = this.mApplicationContext.getSharedPreferences(String.format("%s.%d", new Object[]{str, Integer.valueOf(i)}), 0);
        return new DynamicConfiguration(sharedPreferences.getString("as", DynamicConfiguration.SDEFAULT_CONFIG.getProductionApiServerUrl()), sharedPreferences.getString("ts", DynamicConfiguration.SDEFAULT_CONFIG.getTestApiServerUrl()), sharedPreferences.getBoolean("kt", DynamicConfiguration.SDEFAULT_CONFIG.isSDKEnabled()));
    }

    private void onConfigurationUpdated(String str, int i, DynamicConfiguration dynamicConfiguration) {
        ca c = getConfiguration(str, i).m1519c();
        if (!dynamicConfiguration.equals((DynamicConfiguration) c.m1568a(c.f1097a.m1515b()))) {
            storeDynamicConfig(dynamicConfiguration, str, i);
        }
        this.mConfigurationMap.put(String.format("%s.%d", new Object[]{str, Integer.valueOf(i)}), dynamicConfiguration);
    }

    private void storeDynamicConfig(DynamicConfiguration dynamicConfiguration, String str, int i) {
        Editor edit = this.mApplicationContext.getSharedPreferences(String.format("%s.%d", new Object[]{str, Integer.valueOf(i)}), 0).edit();
        edit.putString("as", dynamicConfiguration.getProductionApiServerUrl());
        edit.putString("ts", dynamicConfiguration.getTestApiServerUrl());
        edit.putBoolean("kt", dynamicConfiguration.isSDKEnabled());
        edit.commit();
    }

    private bj watchConfigurationUpdates(bj bjVar, String str, int i) {
        return bjVar.m1516b(new C09451(str, i));
    }

    public bj getConfiguration(String str, int i) {
        Object obj = (DynamicConfiguration) this.mConfigurationMap.get(String.format("%s.%d", new Object[]{str, Integer.valueOf(i)}));
        return obj == null ? createLoadFromLocalCacheObservable(str, i) : bj.m1507a(obj);
    }

    public bj sync(String str, int i) {
        return watchConfigurationUpdates(this.mConfigurationService.fetch(str, i), str, i);
    }
}
