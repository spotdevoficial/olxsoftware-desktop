package com.kontagent.fingerprint;

import android.app.Application;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.kontagent.KontagentLog;

public class FingerprintBuilder {
    private static final String TAG;
    private final Application mApplication;
    private final IFingerprintIPHelper mIPHelper;

    static {
        TAG = FingerprintBuilder.class.getSimpleName();
    }

    public FingerprintBuilder(Application application, IFingerprintIPHelper iFingerprintIPHelper) {
        this.mApplication = application;
        this.mIPHelper = iFingerprintIPHelper;
    }

    private void withApplicationInfo(Fingerprint fingerprint) {
        try {
            PackageManager packageManager = this.mApplication.getPackageManager();
            CharSequence applicationLabel = packageManager.getApplicationLabel(packageManager.getApplicationInfo(this.mApplication.getPackageName(), 0));
            if (applicationLabel != null) {
                fingerprint.appName = applicationLabel.toString();
            }
            fingerprint.appVersion = packageManager.getPackageInfo(this.mApplication.getPackageName(), 0).versionName;
        } catch (Throwable e) {
            KontagentLog.m1115e(TAG, "Unable to retrieve network operator.", e);
        }
    }

    private void withCarrierName(Fingerprint fingerprint) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.mApplication.getSystemService("phone");
            if (telephonyManager != null) {
                fingerprint.carrier = telephonyManager.getNetworkOperatorName();
            }
        } catch (SecurityException e) {
            KontagentLog.m1120w(TAG, "Unable to retrieve network operator.");
        }
    }

    public Fingerprint build() {
        Fingerprint fingerprint = new Fingerprint();
        fingerprint.osVersion = String.valueOf(VERSION.SDK_INT);
        fingerprint.deviceModel = Build.MODEL;
        fingerprint.deviceName = Build.DEVICE;
        withApplicationInfo(fingerprint);
        withCarrierName(fingerprint);
        fingerprint.ipAddress = this.mIPHelper.getIpAddress();
        WindowManager windowManager = (WindowManager) this.mApplication.getSystemService("window");
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        fingerprint.displayWidth = String.valueOf(displayMetrics.widthPixels);
        fingerprint.displayHeight = String.valueOf(displayMetrics.heightPixels);
        fingerprint.language = this.mApplication.getResources().getConfiguration().locale.toString();
        return fingerprint;
    }
}
