package com.kontagent.fingerprint;

import android.app.Application;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import android.text.format.Formatter;
import com.kontagent.KontagentLog;
import com.urbanairship.C1608R;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import org.apache.http.conn.util.InetAddressUtils;

public class FingerprintIpHelperImpl implements IFingerprintIPHelper {
    private static final String TAG;
    private final Application mApplication;

    static {
        TAG = FingerprintServiceImpl.class.getSimpleName();
    }

    public FingerprintIpHelperImpl(Application application) {
        this.mApplication = application;
    }

    public String getIpAddress() {
        String str = null;
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.mApplication.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            switch (activeNetworkInfo.getType()) {
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    try {
                        int ipAddress = ((WifiManager) this.mApplication.getSystemService("wifi")).getConnectionInfo().getIpAddress();
                        str = ipAddress != 0 ? Formatter.formatIpAddress(ipAddress) : null;
                        break;
                    } catch (Throwable e) {
                        KontagentLog.m1115e(TAG, "Unable to retrieve WIFI state.", e);
                        break;
                    }
            }
        }
        if (TextUtils.isEmpty(str)) {
            try {
                Iterator it = Collections.list(NetworkInterface.getNetworkInterfaces()).iterator();
                while (it.hasNext()) {
                    Enumeration inetAddresses = ((NetworkInterface) it.next()).getInetAddresses();
                    while (inetAddresses.hasMoreElements()) {
                        InetAddress inetAddress = (InetAddress) inetAddresses.nextElement();
                        if (!inetAddress.isLoopbackAddress() && InetAddressUtils.isIPv4Address(inetAddress.getHostAddress())) {
                            str = inetAddress.getHostAddress();
                        }
                    }
                }
            } catch (Throwable e2) {
                KontagentLog.m1115e(TAG, "Unable to retrieve ip address from network interface", e2);
            }
        }
        return str;
    }
}
