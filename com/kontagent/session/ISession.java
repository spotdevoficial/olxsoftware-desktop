package com.kontagent.session;

import java.util.Map;

public interface ISession {
    void customEvent(String str, Map map);

    boolean isStarted();

    void sendDeviceInformation(Map map);

    void setCustomId(String str);

    void setSenderId(String str);

    boolean start();

    void stop();

    void stopHeartbeatTimer();
}
