package com.kontagent.deps;

/* renamed from: com.kontagent.deps.I */
public final class C0955I extends av {
    private byte[] f813e;
    private byte[] f814f;

    C0955I() {
    }

    final av m1194a() {
        return new C0955I();
    }

    final void m1195a(C0994v c0994v) {
        this.f813e = c0994v.m1679f();
        if (c0994v.m1672a() > 0) {
            this.f814f = c0994v.m1679f();
        }
    }

    final void m1196a(C0996x c0996x, C0987o c0987o, boolean z) {
        c0996x.m1690b(this.f813e);
        if (this.f814f != null) {
            c0996x.m1690b(this.f814f);
        }
    }

    final String m1197b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(av.m1147a(this.f813e, true));
        if (this.f814f != null) {
            stringBuffer.append(" ");
            stringBuffer.append(av.m1147a(this.f814f, true));
        }
        return stringBuffer.toString();
    }
}
