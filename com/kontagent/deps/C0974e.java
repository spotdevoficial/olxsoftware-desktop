package com.kontagent.deps;

import java.io.ByteArrayOutputStream;
import java.net.SocketTimeoutException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.HashSet;
import java.util.Set;

/* renamed from: com.kontagent.deps.e */
public class C0974e {
    protected long f919a;
    protected SelectionKey f920b;

    protected C0974e(SelectableChannel selectableChannel, long j) {
        Selector selector = null;
        this.f919a = j;
        try {
            selector = Selector.open();
            selectableChannel.configureBlocking(false);
            this.f920b = selectableChannel.register(selector, 1);
        } catch (Throwable th) {
            if (selector != null) {
                selector.close();
            }
            selectableChannel.close();
        }
    }

    public static String m1325a(byte[] bArr) {
        return new StringBuffer().append(bArr[0] & 255).append(".").append(bArr[1] & 255).append(".").append(bArr[2] & 255).append(".").append(bArr[3] & 255).toString();
    }

    static void m1326a(long j) {
        if (j < 0 || j > 2147483647L) {
            throw new C0956J(j);
        }
    }

    protected static void m1327a(String str, byte[] bArr) {
        if (an.m1435a("verbosemsg")) {
            System.err.println(bi.m1546a(str, bArr));
        }
    }

    public static void m1328a(Throwable th) {
        if (th instanceof bA) {
            throw ((bA) th);
        } else if (th instanceof bz) {
            Throwable cause = ((bz) th).getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            }
            throw ((bz) th);
        } else if (th instanceof StackOverflowError) {
            throw ((StackOverflowError) th);
        } else if (th instanceof VirtualMachineError) {
            throw ((VirtualMachineError) th);
        } else if (th instanceof ThreadDeath) {
            throw ((ThreadDeath) th);
        } else if (th instanceof LinkageError) {
            throw ((LinkageError) th);
        }
    }

    public static void m1329a(Throwable th, Throwable th2) {
        Set hashSet = new HashSet();
        int i = 0;
        while (th.getCause() != null) {
            int i2 = i + 1;
            if (i < 25) {
                th = th.getCause();
                if (!hashSet.contains(th.getCause())) {
                    hashSet.add(th.getCause());
                    i = i2;
                }
            } else {
                return;
            }
        }
        try {
            break;
            th.initCause(th2);
        } catch (Throwable th3) {
        }
    }

    protected static void m1330a(SelectionKey selectionKey, long j) {
        long currentTimeMillis = j - System.currentTimeMillis();
        int i = 0;
        if (currentTimeMillis > 0) {
            i = selectionKey.selector().select(currentTimeMillis);
        } else if (currentTimeMillis == 0) {
            i = selectionKey.selector().selectNow();
        }
        if (i == 0) {
            throw new SocketTimeoutException();
        }
    }

    public static String m1331b(byte[] bArr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (byte b : bArr) {
            short s = (short) (b & 255);
            byte b2 = (byte) (s & 15);
            byteArrayOutputStream.write("0123456789ABCDEF".charAt((byte) (s >> 4)));
            byteArrayOutputStream.write("0123456789ABCDEF".charAt(b2));
        }
        return new String(byteArrayOutputStream.toByteArray());
    }

    void m1332a() {
        this.f920b.selector().close();
        this.f920b.channel().close();
    }
}
