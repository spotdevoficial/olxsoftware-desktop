package com.kontagent.deps;

public final class aF extends av {
    private int f904e;
    private int f905f;
    private byte[] f906g;

    aF() {
    }

    final av m1300a() {
        return new aF();
    }

    final void m1301a(C0994v c0994v) {
        this.f904e = c0994v.m1675b();
        this.f905f = c0994v.m1675b();
        this.f906g = c0994v.m1678e();
    }

    final void m1302a(C0996x c0996x, C0987o c0987o, boolean z) {
        c0996x.m1689b(this.f904e);
        c0996x.m1689b(this.f905f);
        c0996x.m1686a(this.f906g);
    }

    final String m1303b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.f904e);
        stringBuffer.append(" ");
        stringBuffer.append(this.f905f);
        stringBuffer.append(" ");
        stringBuffer.append(C0974e.m1331b(this.f906g));
        return stringBuffer.toString();
    }
}
