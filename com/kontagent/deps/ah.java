package com.kontagent.deps;

import java.util.BitSet;

public final class ah extends av {
    private ai f963e;
    private BitSet f964f;

    ah() {
    }

    final av m1401a() {
        return new ah();
    }

    final void m1402a(C0994v c0994v) {
        this.f963e = new ai(c0994v);
        this.f964f = new BitSet();
        int a = c0994v.m1672a();
        for (int i = 0; i < a; i++) {
            int b = c0994v.m1675b();
            for (int i2 = 0; i2 < 8; i2++) {
                if (((1 << (7 - i2)) & b) != 0) {
                    this.f964f.set((i << 3) + i2);
                }
            }
        }
    }

    final void m1403a(C0996x c0996x, C0987o c0987o, boolean z) {
        this.f963e.m1423a(c0996x, null, z);
        int length = this.f964f.length();
        int i = 0;
        int i2 = 0;
        while (i2 < length) {
            int i3 = (this.f964f.get(i2) ? 1 << (7 - (i2 % 8)) : 0) | i;
            if (i2 % 8 == 7 || i2 == length - 1) {
                c0996x.m1689b(i3);
                i3 = 0;
            }
            i2++;
            i = i3;
        }
    }

    final String m1404b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.f963e);
        int length = this.f964f.length();
        for (int i = 0; i < length; i = (short) (i + 1)) {
            if (this.f964f.get(i)) {
                stringBuffer.append(" ");
                stringBuffer.append(aT.m1361b(i));
            }
        }
        return stringBuffer.toString();
    }
}
