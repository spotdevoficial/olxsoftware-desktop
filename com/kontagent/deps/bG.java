package com.kontagent.deps;

import java.util.concurrent.TimeUnit;

public final class bG implements bE {
    final bj f1001a;
    private long f1002b;
    private TimeUnit f1003c;
    private bq f1004d;

    public bG(bj bjVar, long j, TimeUnit timeUnit, bq bqVar) {
        this.f1001a = bjVar;
        this.f1002b = j;
        this.f1003c = timeUnit;
        this.f1004d = bqVar;
    }

    public final /* synthetic */ void call(Object obj) {
        bs bsVar = (bs) obj;
        Object a = this.f1004d.m1547a();
        bsVar.m1487a(a);
        a.m1495a(new bH(this, bsVar), this.f1002b, this.f1003c);
    }
}
