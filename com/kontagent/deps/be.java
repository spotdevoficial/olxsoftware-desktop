package com.kontagent.deps;

import com.urbanairship.C1608R;
import java.io.IOException;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.List;

public final class be {
    SocketAddress f1051a;
    long f1052b;
    List f1053c;
    private ai f1054d;
    private int f1055e;
    private int f1056f;
    private long f1057g;
    private boolean f1058h;
    private SocketAddress f1059i;
    private aL f1060j;
    private aN f1061k;
    private aO f1062l;
    private int f1063m;
    private long f1064n;
    private long f1065o;
    private av f1066p;
    private List f1067q;

    private be() {
        this.f1052b = 900000;
    }

    be(ai aiVar, int i, long j, boolean z, SocketAddress socketAddress, aN aNVar) {
        this.f1052b = 900000;
        this.f1059i = socketAddress;
        this.f1061k = aNVar;
        if (aiVar.m1424a()) {
            this.f1054d = aiVar;
        } else {
            try {
                this.f1054d = ai.m1407a(aiVar, ai.f965a);
            } catch (aj e) {
                throw new IllegalArgumentException("ZoneTransferIn: name too long");
            }
        }
        this.f1055e = 252;
        this.f1056f = 1;
        this.f1057g = 0;
        this.f1058h = false;
        this.f1063m = 0;
    }

    private static long m1531a(av avVar) {
        return ((aC) avVar).m1288d();
    }

    private static C0970X m1532a(byte[] bArr) {
        try {
            return new C0970X(bArr);
        } catch (IOException e) {
            if (e instanceof bb) {
                throw ((bb) e);
            }
            throw new bb("Error parsing message");
        }
    }

    private void m1533a(String str) {
        if (an.m1435a("verbose")) {
            System.out.println(new StringBuffer().append(this.f1054d).append(": ").append(str).toString());
        }
    }

    private void m1534b() {
        if (!this.f1058h) {
            m1536b("server doesn't support IXFR");
        }
        m1533a("falling back to AXFR");
        this.f1055e = 252;
        this.f1063m = 0;
    }

    private void m1535b(av avVar) {
        while (true) {
            int h = avVar.m1160h();
            long j;
            bf bfVar;
            switch (this.f1063m) {
                case C1608R.styleable.MapAttrs_mapType /*0*/:
                    if (h != 6) {
                        m1536b("missing initial SOA");
                    }
                    this.f1066p = avVar;
                    this.f1064n = m1531a(avVar);
                    if (this.f1055e == 251) {
                        j = this.f1064n;
                        long j2 = this.f1057g;
                        if (j < 0 || j > 4294967295L) {
                            throw new IllegalArgumentException(new StringBuffer().append(j).append(" out of range").toString());
                        } else if (j2 < 0 || j2 > 4294967295L) {
                            throw new IllegalArgumentException(new StringBuffer().append(j2).append(" out of range").toString());
                        } else {
                            j -= j2;
                            if (j >= 4294967295L) {
                                j -= 4294967296L;
                            } else if (j < -4294967295L) {
                                j += 4294967296L;
                            }
                            if (((int) j) <= 0) {
                                m1533a("up to date");
                                this.f1063m = 7;
                                return;
                            }
                        }
                    }
                    this.f1063m = 1;
                    return;
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    if (this.f1055e != 251 || h != 6 || m1531a(avVar) != this.f1057g) {
                        this.f1053c = new ArrayList();
                        this.f1053c.add(this.f1066p);
                        m1533a("got nonincremental response");
                        this.f1063m = 6;
                        break;
                    }
                    this.f1067q = new ArrayList();
                    m1533a("got incremental response");
                    this.f1063m = 2;
                    break;
                    break;
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    bfVar = new bf((byte) 0);
                    this.f1067q.add(bfVar);
                    m1531a(avVar);
                    bfVar.f1069b.add(avVar);
                    this.f1063m = 3;
                    return;
                case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                    if (h == 6) {
                        this.f1065o = m1531a(avVar);
                        this.f1063m = 4;
                        break;
                    }
                    ((bf) this.f1067q.get(this.f1067q.size() - 1)).f1069b.add(avVar);
                    return;
                case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                    bfVar = (bf) this.f1067q.get(this.f1067q.size() - 1);
                    m1531a(avVar);
                    bfVar.f1068a.add(avVar);
                    this.f1063m = 5;
                    return;
                case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                    if (h == 6) {
                        j = m1531a(avVar);
                        if (j != this.f1064n) {
                            if (j == this.f1065o) {
                                this.f1063m = 2;
                                break;
                            }
                            m1536b(new StringBuffer("IXFR out of sync: expected serial ").append(this.f1065o).append(" , got ").append(j).toString());
                        } else {
                            this.f1063m = 7;
                            return;
                        }
                    }
                    ((bf) this.f1067q.get(this.f1067q.size() - 1)).f1068a.add(avVar);
                    return;
                case C1608R.styleable.MapAttrs_liteMode /*6*/:
                    if (h != 1 || avVar.m1162j() == this.f1056f) {
                        this.f1053c.add(avVar);
                        if (h == 6) {
                            this.f1063m = 7;
                            return;
                        }
                        return;
                    }
                    return;
                case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                    m1536b("extra data");
                    return;
                default:
                    m1536b("invalid state");
                    return;
            }
        }
    }

    private static void m1536b(String str) {
        throw new bd(str);
    }

    private void m1537c() {
        try {
            if (this.f1060j != null) {
                this.f1060j.m1332a();
            }
        } catch (IOException e) {
        }
    }

    private void m1538d() {
        while (true) {
            av a = av.m1142a(this.f1054d, this.f1055e, this.f1056f);
            C0970X c0970x = new C0970X();
            c0970x.m1249a().m1184c(0);
            c0970x.m1250a(a, 0);
            if (this.f1055e == 251) {
                c0970x.m1250a(new aC(this.f1054d, this.f1056f, 0, ai.f965a, ai.f965a, this.f1057g, 0, 0, 0, 0), 2);
            }
            if (this.f1061k != null) {
                this.f1061k.m1348a(c0970x, null);
                this.f1062l = new aO(this.f1061k, c0970x.m1254c());
            }
            this.f1060j.m1338c(c0970x.m1255c(65535));
            while (this.f1063m != 7) {
                int a2;
                byte[] b = this.f1060j.m1337b();
                C0970X a3 = m1532a(b);
                if (a3.m1249a().m1183c() == 0 && this.f1062l != null) {
                    a3.m1254c();
                    aO aOVar = this.f1062l;
                    aP c = a3.m1254c();
                    aOVar.f931c++;
                    if (aOVar.f931c == 1) {
                        a2 = aOVar.f929a.m1347a(a3, b, aOVar.f933e);
                        if (a2 == 0) {
                            b = c.m1356n();
                            C0996x c0996x = new C0996x();
                            c0996x.m1691c(b.length);
                            aOVar.f930b.m1541a(c0996x.m1692c());
                            aOVar.f930b.m1541a(b);
                        }
                        aOVar.f933e = c;
                    } else {
                        if (c != null) {
                            a3.m1249a().m1187e(3);
                        }
                        byte[] a4 = a3.m1249a().m1180a();
                        if (c != null) {
                            a3.m1249a().m1186d(3);
                        }
                        aOVar.f930b.m1541a(a4);
                        aOVar.f930b.m1542a(b, a4.length, c == null ? b.length - a4.length : a3.f862a - a4.length);
                        if (c != null) {
                            aOVar.f932d = aOVar.f931c;
                            aOVar.f933e = c;
                            if (c.m1159g().equals(aN.m1345c(aOVar.f929a)) && c.m1353d().equals(aN.m1346d(aOVar.f929a))) {
                                C0996x c0996x2 = new C0996x();
                                long time = c.m1354e().getTime() / 1000;
                                time &= 4294967295L;
                                c0996x2.m1691c((int) (time >> 32));
                                c0996x2.m1685a(time);
                                c0996x2.m1691c(c.m1355m());
                                aOVar.f930b.m1541a(c0996x2.m1692c());
                                if (aOVar.f930b.m1544b(c.m1356n())) {
                                    bg bgVar = aOVar.f930b;
                                    bgVar.f1070a.reset();
                                    bgVar.f1070a.update(bgVar.f1071b);
                                    c0996x2 = new C0996x();
                                    c0996x2.m1691c(c.m1356n().length);
                                    aOVar.f930b.m1541a(c0996x2.m1692c());
                                    aOVar.f930b.m1541a(c.m1356n());
                                    a3.f863b = 1;
                                    a2 = 0;
                                } else {
                                    if (an.m1435a("verbose")) {
                                        System.err.println("BADSIG failure");
                                    }
                                    a3.f863b = 4;
                                    a2 = 16;
                                }
                            } else {
                                if (an.m1435a("verbose")) {
                                    System.err.println("BADKEY failure");
                                }
                                a3.f863b = 4;
                                a2 = 17;
                            }
                        } else {
                            if ((aOVar.f931c - aOVar.f932d >= 100 ? 1 : null) != null) {
                                a3.f863b = 4;
                                a2 = 1;
                            } else {
                                a3.f863b = 2;
                                a2 = 0;
                            }
                        }
                    }
                    if (a2 != 0) {
                        m1536b("TSIG failure");
                    }
                }
                av[] a5 = a3.m1251a(1);
                if (this.f1063m == 0) {
                    a2 = a3.m1258f();
                    if (a2 != 0) {
                        if (this.f1055e == 251 && a2 == 4) {
                            m1534b();
                        } else {
                            m1536b(au.m1458a(a2));
                        }
                    }
                    a = a3.m1252b();
                    if (!(a == null || a.m1160h() == this.f1055e)) {
                        m1536b("invalid question section");
                    }
                    if (a5.length == 0 && this.f1055e == 251) {
                        m1534b();
                    }
                }
                for (av b2 : a5) {
                    m1535b(b2);
                }
                if (!(this.f1063m != 7 || this.f1062l == null || a3.m1256d())) {
                    m1536b("last message must be signed");
                }
            }
            return;
        }
    }

    public final List m1539a() {
        try {
            this.f1060j = new aL(System.currentTimeMillis() + this.f1052b);
            if (this.f1051a != null) {
                this.f1060j.m1335a(this.f1051a);
            }
            this.f1060j.m1336b(this.f1059i);
            m1538d();
            return this.f1053c != null ? this.f1053c : this.f1067q;
        } finally {
            m1537c();
        }
    }
}
