package com.kontagent.deps;

/* renamed from: com.kontagent.deps.E */
public final class C0951E extends av {
    private byte[] f799e;
    private byte[] f800f;
    private byte[] f801g;

    C0951E() {
    }

    final av m1165a() {
        return new C0951E();
    }

    final void m1166a(C0994v c0994v) {
        this.f800f = c0994v.m1679f();
        this.f799e = c0994v.m1679f();
        this.f801g = c0994v.m1679f();
        try {
            double parseDouble = Double.parseDouble(av.m1147a(this.f800f, false));
            double parseDouble2 = Double.parseDouble(av.m1147a(this.f799e, false));
            if (parseDouble < -90.0d || parseDouble > 90.0d) {
                throw new IllegalArgumentException(new StringBuffer("illegal longitude ").append(parseDouble).toString());
            } else if (parseDouble2 < -180.0d || parseDouble2 > 180.0d) {
                throw new IllegalArgumentException(new StringBuffer("illegal latitude ").append(parseDouble2).toString());
            }
        } catch (IllegalArgumentException e) {
            throw new bb(e.getMessage());
        }
    }

    final void m1167a(C0996x c0996x, C0987o c0987o, boolean z) {
        c0996x.m1690b(this.f800f);
        c0996x.m1690b(this.f799e);
        c0996x.m1690b(this.f801g);
    }

    final String m1168b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(av.m1147a(this.f800f, true));
        stringBuffer.append(" ");
        stringBuffer.append(av.m1147a(this.f799e, true));
        stringBuffer.append(" ");
        stringBuffer.append(av.m1147a(this.f801g, true));
        return stringBuffer.toString();
    }
}
