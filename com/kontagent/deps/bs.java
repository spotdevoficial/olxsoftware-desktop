package com.kontagent.deps;

import java.util.LinkedList;

public abstract class bs implements bp, bt {
    public final bZ f1008a;

    public bs() {
        this.f1008a = new bZ();
    }

    public bs(bs bsVar) {
        this.f1008a = bsVar.f1008a;
    }

    public final void m1486a() {
        this.f1008a.m1521a();
    }

    public final void m1487a(bt btVar) {
        bZ bZVar = this.f1008a;
        synchronized (bZVar) {
            if (!bZVar.f1046b) {
                if (bZVar.f1045a == null) {
                    bZVar.f1045a = new LinkedList();
                }
                bZVar.f1045a.add(btVar);
                btVar = null;
            }
        }
        if (btVar != null) {
            btVar.m1485a();
        }
    }
}
