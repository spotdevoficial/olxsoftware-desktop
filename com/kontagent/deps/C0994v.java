package com.kontagent.deps;

/* renamed from: com.kontagent.deps.v */
public final class C0994v {
    byte[] f1182a;
    int f1183b;
    int f1184c;
    int f1185d;
    int f1186e;

    public C0994v(byte[] bArr) {
        this.f1182a = bArr;
        this.f1183b = 0;
        this.f1184c = this.f1182a.length;
        this.f1185d = -1;
        this.f1186e = -1;
    }

    private void m1671b(int i) {
        if (i > m1672a()) {
            throw new bb("end of input");
        }
    }

    public final int m1672a() {
        return this.f1184c - this.f1183b;
    }

    public final void m1673a(byte[] bArr, int i, int i2) {
        m1671b(i2);
        System.arraycopy(this.f1182a, this.f1183b, bArr, i, i2);
        this.f1183b += i2;
    }

    public final byte[] m1674a(int i) {
        m1671b(i);
        Object obj = new byte[i];
        System.arraycopy(this.f1182a, this.f1183b, obj, 0, i);
        this.f1183b += i;
        return obj;
    }

    public final int m1675b() {
        m1671b(1);
        byte[] bArr = this.f1182a;
        int i = this.f1183b;
        this.f1183b = i + 1;
        return bArr[i] & 255;
    }

    public final int m1676c() {
        m1671b(2);
        byte[] bArr = this.f1182a;
        int i = this.f1183b;
        this.f1183b = i + 1;
        int i2 = bArr[i] & 255;
        byte[] bArr2 = this.f1182a;
        int i3 = this.f1183b;
        this.f1183b = i3 + 1;
        return (i2 << 8) + (bArr2[i3] & 255);
    }

    public final long m1677d() {
        m1671b(4);
        byte[] bArr = this.f1182a;
        int i = this.f1183b;
        this.f1183b = i + 1;
        int i2 = bArr[i] & 255;
        byte[] bArr2 = this.f1182a;
        int i3 = this.f1183b;
        this.f1183b = i3 + 1;
        i = bArr2[i3] & 255;
        byte[] bArr3 = this.f1182a;
        int i4 = this.f1183b;
        this.f1183b = i4 + 1;
        i3 = bArr3[i4] & 255;
        byte[] bArr4 = this.f1182a;
        int i5 = this.f1183b;
        this.f1183b = i5 + 1;
        return ((((long) (i << 16)) + (((long) i2) << 24)) + ((long) (i3 << 8))) + ((long) (bArr4[i5] & 255));
    }

    public final byte[] m1678e() {
        int a = m1672a();
        Object obj = new byte[a];
        System.arraycopy(this.f1182a, this.f1183b, obj, 0, a);
        this.f1183b = a + this.f1183b;
        return obj;
    }

    public final byte[] m1679f() {
        m1671b(1);
        byte[] bArr = this.f1182a;
        int i = this.f1183b;
        this.f1183b = i + 1;
        return m1674a(bArr[i] & 255);
    }
}
