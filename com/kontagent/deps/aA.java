package com.kontagent.deps;

import java.util.Date;

abstract class aA extends av {
    private int f883e;
    private int f884f;
    private int f885g;
    private long f886h;
    private Date f887i;
    private Date f888j;
    private int f889k;
    private ai f890l;
    private byte[] f891m;

    protected aA() {
    }

    final void m1279a(C0994v c0994v) {
        this.f883e = c0994v.m1676c();
        this.f884f = c0994v.m1675b();
        this.f885g = c0994v.m1675b();
        this.f886h = c0994v.m1677d();
        this.f887i = new Date(c0994v.m1677d() * 1000);
        this.f888j = new Date(c0994v.m1677d() * 1000);
        this.f889k = c0994v.m1676c();
        this.f890l = new ai(c0994v);
        this.f891m = c0994v.m1678e();
    }

    final void m1280a(C0996x c0996x, C0987o c0987o, boolean z) {
        c0996x.m1691c(this.f883e);
        c0996x.m1689b(this.f884f);
        c0996x.m1689b(this.f885g);
        c0996x.m1685a(this.f886h);
        c0996x.m1685a(this.f887i.getTime() / 1000);
        c0996x.m1685a(this.f888j.getTime() / 1000);
        c0996x.m1691c(this.f889k);
        this.f890l.m1423a(c0996x, null, z);
        c0996x.m1686a(this.f891m);
    }

    final String m1281b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(aT.m1361b(this.f883e));
        stringBuffer.append(" ");
        stringBuffer.append(this.f884f);
        stringBuffer.append(" ");
        stringBuffer.append(this.f885g);
        stringBuffer.append(" ");
        stringBuffer.append(this.f886h);
        stringBuffer.append(" ");
        if (an.m1435a("multiline")) {
            stringBuffer.append("(\n\t");
        }
        stringBuffer.append(C0950D.m1138a(this.f887i));
        stringBuffer.append(" ");
        stringBuffer.append(C0950D.m1138a(this.f888j));
        stringBuffer.append(" ");
        stringBuffer.append(this.f889k);
        stringBuffer.append(" ");
        stringBuffer.append(this.f890l);
        if (an.m1435a("multiline")) {
            stringBuffer.append("\n");
            stringBuffer.append(C0987o.m1657a(this.f891m, 64, "\t", true));
        } else {
            stringBuffer.append(" ");
            stringBuffer.append(C0987o.m1656a(this.f891m));
        }
        return stringBuffer.toString();
    }

    public final int m1282d() {
        return this.f883e;
    }
}
