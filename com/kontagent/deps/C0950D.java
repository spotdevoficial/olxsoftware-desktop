package com.kontagent.deps;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/* renamed from: com.kontagent.deps.D */
final class C0950D {
    private static NumberFormat f792a;
    private static NumberFormat f793b;

    static {
        NumberFormat decimalFormat = new DecimalFormat();
        f792a = decimalFormat;
        decimalFormat.setMinimumIntegerDigits(2);
        decimalFormat = new DecimalFormat();
        f793b = decimalFormat;
        decimalFormat.setMinimumIntegerDigits(4);
        f793b.setGroupingUsed(false);
    }

    public static String m1138a(Date date) {
        Calendar gregorianCalendar = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
        StringBuffer stringBuffer = new StringBuffer();
        gregorianCalendar.setTime(date);
        stringBuffer.append(f793b.format((long) gregorianCalendar.get(1)));
        stringBuffer.append(f792a.format((long) (gregorianCalendar.get(2) + 1)));
        stringBuffer.append(f792a.format((long) gregorianCalendar.get(5)));
        stringBuffer.append(f792a.format((long) gregorianCalendar.get(11)));
        stringBuffer.append(f792a.format((long) gregorianCalendar.get(12)));
        stringBuffer.append(f792a.format((long) gregorianCalendar.get(13)));
        return stringBuffer.toString();
    }
}
