package com.kontagent.deps;

/* renamed from: com.kontagent.deps.q */
public final class C0989q {
    private static C0971Y f1176a;

    static {
        C0971Y c0990r = new C0990r();
        f1176a = c0990r;
        c0990r.m1263a(1, "IN");
        f1176a.m1263a(3, "CH");
        f1176a.m1268b(3, "CHAOS");
        f1176a.m1263a(4, "HS");
        f1176a.m1268b(4, "HESIOD");
        f1176a.m1263a(254, "NONE");
        f1176a.m1263a(255, "ANY");
    }

    public static void m1659a(int i) {
        if (i < 0 || i > 65535) {
            throw new C0956J(i);
        }
    }

    public static String m1660b(int i) {
        return f1176a.m1269d(i);
    }
}
