package com.kontagent.deps;

import com.urbanairship.C1608R;
import java.net.InetAddress;

/* renamed from: com.kontagent.deps.H */
public final class C0954H extends av {
    private int f808e;
    private int f809f;
    private int f810g;
    private Object f811h;
    private byte[] f812i;

    C0954H() {
    }

    final av m1190a() {
        return new C0954H();
    }

    final void m1191a(C0994v c0994v) {
        this.f808e = c0994v.m1675b();
        this.f809f = c0994v.m1675b();
        this.f810g = c0994v.m1675b();
        switch (this.f809f) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                this.f811h = null;
                break;
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                this.f811h = InetAddress.getByAddress(c0994v.m1674a(4));
                break;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                this.f811h = InetAddress.getByAddress(c0994v.m1674a(16));
                break;
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                this.f811h = new ai(c0994v);
                break;
            default:
                throw new bb("invalid gateway type");
        }
        if (c0994v.m1672a() > 0) {
            this.f812i = c0994v.m1678e();
        }
    }

    final void m1192a(C0996x c0996x, C0987o c0987o, boolean z) {
        c0996x.m1689b(this.f808e);
        c0996x.m1689b(this.f809f);
        c0996x.m1689b(this.f810g);
        switch (this.f809f) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                c0996x.m1686a(((InetAddress) this.f811h).getAddress());
                break;
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                ((ai) this.f811h).m1423a(c0996x, null, z);
                break;
        }
        if (this.f812i != null) {
            c0996x.m1686a(this.f812i);
        }
    }

    final String m1193b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.f808e);
        stringBuffer.append(" ");
        stringBuffer.append(this.f809f);
        stringBuffer.append(" ");
        stringBuffer.append(this.f810g);
        stringBuffer.append(" ");
        switch (this.f809f) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                stringBuffer.append(".");
                break;
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                stringBuffer.append(((InetAddress) this.f811h).getHostAddress());
                break;
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                stringBuffer.append(this.f811h);
                break;
        }
        if (this.f812i != null) {
            stringBuffer.append(" ");
            stringBuffer.append(C0987o.m1656a(this.f812i));
        }
        return stringBuffer.toString();
    }
}
