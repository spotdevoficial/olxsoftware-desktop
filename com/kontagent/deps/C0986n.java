package com.kontagent.deps;

/* renamed from: com.kontagent.deps.n */
final class C0986n implements C0984m {
    private int f1167a;
    private ai f1168b;
    private int f1169c;
    private int f1170d;

    public C0986n(ai aiVar, int i, aC aCVar, int i2, long j) {
        this.f1168b = aiVar;
        this.f1167a = i;
        long j2 = 0;
        if (aCVar != null) {
            j2 = aCVar.m1289e();
        }
        this.f1169c = i2;
        this.f1170d = C0982j.m1633a(j2, j);
    }

    public final int m1653a(int i) {
        return this.f1169c - i;
    }

    public final boolean m1654a() {
        return ((int) (System.currentTimeMillis() / 1000)) >= this.f1170d;
    }

    public final int m1655b() {
        return this.f1167a;
    }

    public final String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        if (this.f1167a == 0) {
            stringBuffer.append(new StringBuffer("NXDOMAIN ").append(this.f1168b).toString());
        } else {
            stringBuffer.append(new StringBuffer("NXRRSET ").append(this.f1168b).append(" ").append(aT.m1361b(this.f1167a)).toString());
        }
        stringBuffer.append(" cl = ");
        stringBuffer.append(this.f1169c);
        return stringBuffer.toString();
    }
}
