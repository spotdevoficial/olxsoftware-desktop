package com.kontagent.deps;

import java.util.Date;

public final class aN {
    private static ai f928a;

    static {
        f928a = ai.m1414b("HMAC-MD5.SIG-ALG.REG.INT.");
        ai.m1414b("hmac-sha1.");
        ai.m1414b("hmac-sha256.");
    }

    static String m1343a(aN aNVar) {
        return null;
    }

    static byte[] m1344b(aN aNVar) {
        return null;
    }

    static ai m1345c(aN aNVar) {
        return null;
    }

    static ai m1346d(aN aNVar) {
        return null;
    }

    public final int m1347a(C0970X c0970x, byte[] bArr, aP aPVar) {
        c0970x.f863b = 4;
        aP c = c0970x.m1254c();
        bg bgVar = new bg(null, null);
        if (c == null) {
            return 1;
        }
        if (c.m1159g().equals(null) && c.m1353d().equals(null)) {
            if (Math.abs(System.currentTimeMillis() - c.m1354e().getTime()) > 1000 * ((long) c.m1355m())) {
                if (an.m1435a("verbose")) {
                    System.err.println("BADTIME failure");
                }
                return 18;
            }
            C0996x c0996x;
            if (!(aPVar == null || c.m1357o() == 17 || c.m1357o() == 16)) {
                c0996x = new C0996x();
                c0996x.m1691c(aPVar.m1356n().length);
                bgVar.m1541a(c0996x.m1692c());
                bgVar.m1541a(aPVar.m1356n());
            }
            c0970x.m1249a().m1187e(3);
            byte[] a = c0970x.m1249a().m1180a();
            c0970x.m1249a().m1186d(3);
            bgVar.m1541a(a);
            bgVar.m1542a(bArr, a.length, c0970x.f862a - a.length);
            c0996x = new C0996x();
            c.m1159g().m1421a(c0996x);
            c0996x.m1691c(c.c);
            c0996x.m1685a(c.d);
            c.m1353d().m1421a(c0996x);
            long time = c.m1354e().getTime() / 1000;
            time &= 4294967295L;
            c0996x.m1691c((int) (time >> 32));
            c0996x.m1685a(time);
            c0996x.m1691c(c.m1355m());
            c0996x.m1691c(c.m1357o());
            if (c.m1358p() != null) {
                c0996x.m1691c(c.m1358p().length);
                c0996x.m1686a(c.m1358p());
            } else {
                c0996x.m1691c(0);
            }
            bgVar.m1541a(c0996x.m1692c());
            if (bgVar.m1544b(c.m1356n())) {
                c0970x.f863b = 1;
                return 0;
            }
            if (an.m1435a("verbose")) {
                System.err.println("BADSIG failure");
            }
            return 16;
        }
        if (an.m1435a("verbose")) {
            System.err.println("BADKEY failure");
        }
        return 17;
    }

    public final void m1348a(C0970X c0970x, aP aPVar) {
        aP aPVar2 = null;
        byte[] g = c0970x.m1259g();
        Date date = new Date();
        bg bgVar = new bg(null, null);
        int b = an.m1436b("tsigfudge");
        if (b < 0 || b > 32767) {
            b = 300;
        }
        if (aPVar2 != null) {
            C0996x c0996x = new C0996x();
            c0996x.m1691c(aPVar2.m1356n().length);
            if (bgVar != null) {
                bgVar.m1541a(c0996x.m1692c());
                bgVar.m1541a(aPVar2.m1356n());
            }
        }
        if (bgVar != null) {
            bgVar.m1541a(g);
        }
        C0996x c0996x2 = new C0996x();
        ai aiVar = null;
        aiVar.m1421a(c0996x2);
        c0996x2.m1691c(255);
        c0996x2.m1685a(0);
        aiVar = null;
        aiVar.m1421a(c0996x2);
        long time = date.getTime() / 1000;
        time &= 4294967295L;
        c0996x2.m1691c((int) (time >> 32));
        c0996x2.m1685a(time);
        c0996x2.m1691c(b);
        c0996x2.m1691c(0);
        c0996x2.m1691c(0);
        if (bgVar != null) {
            bgVar.m1541a(c0996x2.m1692c());
        }
        c0970x.m1250a(new aP(null, 255, 0, null, date, b, bgVar != null ? bgVar.m1543a() : new byte[0], c0970x.m1249a().m1181b(), 0, null), 3);
        c0970x.f863b = 3;
    }
}
