package com.kontagent.deps;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/* renamed from: com.kontagent.deps.j */
public final class C0982j {
    public C0983k f1161a;
    private int f1162b;
    private int f1163c;

    public C0982j() {
        this(1);
    }

    public C0982j(int i) {
        this.f1162b = -1;
        this.f1163c = -1;
        this.f1161a = new C0983k(50000);
    }

    private static int m1632a(int i, boolean z) {
        if (i == 1) {
            return z ? 4 : 3;
        } else {
            if (i == 2) {
                return !z ? 3 : 4;
            } else {
                if (i == 3) {
                    return 1;
                }
                throw new IllegalArgumentException("getCred: invalid section");
            }
        }
    }

    static int m1633a(long j, long j2) {
        if (j2 >= 0 && j2 < j) {
            j = j2;
        }
        long currentTimeMillis = (System.currentTimeMillis() / 1000) + j;
        return (currentTimeMillis < 0 || currentTimeMillis > 2147483647L) ? Integer.MAX_VALUE : (int) currentTimeMillis;
    }

    private synchronized C0984m m1634a(ai aiVar, Object obj, int i, int i2) {
        C0984m c0984m;
        if (i == 255) {
            throw new IllegalArgumentException("oneElement(ANY)");
        }
        if (obj instanceof List) {
            List list = (List) obj;
            for (int i3 = 0; i3 < list.size(); i3++) {
                c0984m = (C0984m) list.get(i3);
                if (c0984m.m1650b() == i) {
                    break;
                }
            }
            c0984m = null;
        } else {
            C0984m c0984m2 = (C0984m) obj;
            c0984m = c0984m2.m1650b() == i ? c0984m2 : null;
        }
        if (c0984m == null) {
            c0984m = null;
        } else if (c0984m.m1649a()) {
            m1636a(aiVar, i);
            c0984m = null;
        } else if (c0984m.m1648a(i2) < 0) {
            c0984m = null;
        }
        return c0984m;
    }

    private synchronized Object m1635a(ai aiVar) {
        return this.f1161a.get(aiVar);
    }

    private synchronized void m1636a(ai aiVar, int i) {
        Object obj = this.f1161a.get(aiVar);
        if (obj != null) {
            if (obj instanceof List) {
                List list = (List) obj;
                int i2 = 0;
                while (i2 < list.size()) {
                    if (((C0984m) list.get(i2)).m1650b() == i) {
                        list.remove(i2);
                        if (list.size() == 0) {
                            this.f1161a.remove(aiVar);
                        }
                    } else {
                        i2++;
                    }
                }
            } else if (((C0984m) obj).m1650b() == i) {
                this.f1161a.remove(aiVar);
            }
        }
    }

    private synchronized void m1637a(ai aiVar, int i, aC aCVar, int i2) {
        long k = aCVar != null ? aCVar.m1163k() : 0;
        C0984m b = m1642b(aiVar, i, 0);
        if (k != 0) {
            if (b != null) {
                if (b.m1648a(i2) <= 0) {
                    b = null;
                }
            }
            if (b == null) {
                m1638a(aiVar, new C0986n(aiVar, i, aCVar, i2, (long) this.f1162b));
            }
        } else if (b != null && b.m1648a(i2) <= 0) {
            m1636a(aiVar, i);
        }
    }

    private synchronized void m1638a(ai aiVar, C0984m c0984m) {
        Object obj = this.f1161a.get(aiVar);
        if (obj == null) {
            this.f1161a.put(aiVar, c0984m);
        } else {
            int b = c0984m.m1650b();
            if (obj instanceof List) {
                List list = (List) obj;
                for (int i = 0; i < list.size(); i++) {
                    if (((C0984m) list.get(i)).m1650b() == b) {
                        list.set(i, c0984m);
                        break;
                    }
                }
                list.add(c0984m);
            } else {
                C0984m c0984m2 = (C0984m) obj;
                if (c0984m2.m1650b() == b) {
                    this.f1161a.put(aiVar, c0984m);
                } else {
                    LinkedList linkedList = new LinkedList();
                    linkedList.add(c0984m2);
                    linkedList.add(c0984m);
                    this.f1161a.put(aiVar, linkedList);
                }
            }
        }
    }

    private synchronized void m1639a(as asVar, int i) {
        long f = asVar.m1455f();
        ai d = asVar.m1453d();
        int b = asVar.m1451b();
        C0984m b2 = m1642b(d, b, 0);
        if (f != 0) {
            if (b2 != null) {
                if (b2.m1648a(i) <= 0) {
                    b2 = null;
                }
            }
            if (b2 == null) {
                C0984m c0984m;
                if (asVar instanceof C0985l) {
                    c0984m = (C0985l) asVar;
                } else {
                    Object c0985l = new C0985l(asVar, i, (long) this.f1163c);
                }
                m1638a(d, c0984m);
            }
        } else if (b2 != null && b2.m1648a(i) <= 0) {
            m1636a(d, b);
        }
    }

    private static void m1640a(as asVar, Set set) {
        if (asVar.m1456g().m1157c() != null) {
            Iterator c = asVar.m1452c();
            while (c.hasNext()) {
                ai c2 = ((av) c.next()).m1157c();
                if (c2 != null) {
                    set.add(c2);
                }
            }
        }
    }

    private synchronized C0984m[] m1641a(Object obj) {
        C0984m[] c0984mArr;
        if (obj instanceof List) {
            List list = (List) obj;
            c0984mArr = (C0984m[]) list.toArray(new C0984m[list.size()]);
        } else {
            c0984mArr = new C0984m[]{(C0984m) obj};
        }
        return c0984mArr;
    }

    private synchronized C0984m m1642b(ai aiVar, int i, int i2) {
        Object a;
        a = m1635a(aiVar);
        return a == null ? null : m1634a(aiVar, a, i, 0);
    }

    private synchronized aH m1643c(ai aiVar, int i, int i2) {
        aH aHVar;
        int c = aiVar.m1427c();
        int i3 = c;
        while (i3 > 0) {
            Object obj = i3 == 1 ? 1 : null;
            Object obj2 = i3 == c ? 1 : null;
            ai aiVar2 = obj != null ? ai.f965a : obj2 != null ? aiVar : new ai(aiVar, c - i3);
            Object obj3 = this.f1161a.get(aiVar2);
            if (obj3 != null) {
                C0984m c0984m;
                if (obj2 != null && i == 255) {
                    aH aHVar2 = new aH(6);
                    C0984m[] a = m1641a(obj3);
                    int i4 = 0;
                    int i5 = 0;
                    while (i5 < a.length) {
                        int i6;
                        c0984m = a[i5];
                        if (c0984m.m1649a()) {
                            m1636a(aiVar2, c0984m.m1650b());
                            i6 = i4;
                        } else if (!(c0984m instanceof C0985l) || c0984m.m1648a(i2) < 0) {
                            i6 = i4;
                        } else {
                            aHVar2.m1308a((C0985l) c0984m);
                            i6 = i4 + 1;
                        }
                        i5++;
                        i4 = i6;
                    }
                    if (i4 > 0) {
                        aHVar = aHVar2;
                        break;
                    }
                }
                if (obj2 == null) {
                    c0984m = m1634a(aiVar2, obj3, 39, i2);
                    if (c0984m != null && (c0984m instanceof C0985l)) {
                        aHVar = new aH(5, (C0985l) c0984m);
                        break;
                    }
                }
                c0984m = m1634a(aiVar2, obj3, i, i2);
                if (c0984m == null || !(c0984m instanceof C0985l)) {
                    if (c0984m == null) {
                        c0984m = m1634a(aiVar2, obj3, 5, i2);
                        if (c0984m != null && (c0984m instanceof C0985l)) {
                            aHVar = new aH(4, (C0985l) c0984m);
                            break;
                        }
                    }
                    aHVar = new aH(2);
                    break;
                }
                aH aHVar3 = new aH(6);
                aHVar3.m1308a((C0985l) c0984m);
                aHVar = aHVar3;
                break;
                c0984m = m1634a(aiVar2, obj3, 2, i2);
                if (c0984m == null || !(c0984m instanceof C0985l)) {
                    if (!(obj2 == null || m1634a(aiVar2, obj3, 0, i2) == null)) {
                        aHVar = aH.m1307a(1);
                        break;
                    }
                }
                aHVar = new aH(3, (C0985l) c0984m);
                break;
            }
            i3--;
        }
        aHVar = aH.m1307a(0);
        return aHVar;
    }

    public final aH m1644a(C0970X c0970x) {
        boolean b = c0970x.m1249a().m1182b(5);
        av b2 = c0970x.m1252b();
        int c = c0970x.m1249a().m1183c();
        Object obj = null;
        boolean a = an.m1435a("verbosecache");
        if ((c != 0 && c != 3) || b2 == null) {
            return null;
        }
        aH aHVar;
        ai g = b2.m1159g();
        int h = b2.m1160h();
        int j = b2.m1162j();
        Set hashSet = new HashSet();
        as[] b3 = c0970x.m1253b(1);
        int i = 0;
        ai aiVar = g;
        aH aHVar2 = null;
        while (i < b3.length) {
            Object obj2;
            ai aiVar2;
            if (b3[i].m1454e() == j) {
                int b4 = b3[i].m1451b();
                ai d = b3[i].m1453d();
                int a2 = C0982j.m1632a(1, b);
                if ((b4 == h || h == 255) && d.equals(aiVar)) {
                    m1639a(b3[i], a2);
                    obj2 = 1;
                    if (aiVar == g) {
                        if (aHVar2 == null) {
                            aHVar2 = new aH(6);
                        }
                        aHVar2.m1308a(b3[i]);
                    }
                    C0982j.m1640a(b3[i], hashSet);
                    aiVar2 = aiVar;
                    i++;
                    aiVar = aiVar2;
                    obj = obj2;
                } else if (b4 == 5 && d.equals(aiVar)) {
                    m1639a(b3[i], a2);
                    r17 = aiVar == g ? new aH(4, b3[i]) : aHVar2;
                    obj2 = obj;
                    aiVar2 = ((C0981i) b3[i].m1456g()).a_();
                    aHVar2 = r17;
                    i++;
                    aiVar = aiVar2;
                    obj = obj2;
                } else if (b4 == 39 && aiVar.m1425a(d)) {
                    m1639a(b3[i], a2);
                    aHVar = aiVar == g ? new aH(5, b3[i]) : aHVar2;
                    try {
                        r17 = aHVar;
                        obj2 = obj;
                        aiVar2 = aiVar.m1420a((C0993u) b3[i].m1456g());
                        aHVar2 = r17;
                        i++;
                        aiVar = aiVar2;
                        obj = obj2;
                    } catch (aj e) {
                    }
                }
            }
            obj2 = obj;
            aiVar2 = aiVar;
            i++;
            aiVar = aiVar2;
            obj = obj2;
        }
        aHVar = aHVar2;
        as[] b5 = c0970x.m1253b(2);
        as asVar = null;
        as asVar2 = null;
        int i2 = 0;
        while (i2 < b5.length) {
            if (b5[i2].m1451b() == 6 && aiVar.m1425a(b5[i2].m1453d())) {
                asVar2 = b5[i2];
            } else if (b5[i2].m1451b() == 2 && aiVar.m1425a(b5[i2].m1453d())) {
                asVar = b5[i2];
            }
            i2++;
        }
        if (obj == null) {
            int i3 = c == 3 ? 0 : h;
            if (c == 3 || asVar2 != null || asVar == null) {
                i2 = C0982j.m1632a(2, b);
                aC aCVar = null;
                if (asVar2 != null) {
                    aCVar = (aC) asVar2.m1456g();
                }
                m1637a(aiVar, i3, aCVar, i2);
                if (aHVar == null) {
                    aHVar = aH.m1307a(c == 3 ? 1 : 2);
                }
            } else {
                m1639a(asVar, C0982j.m1632a(2, b));
                C0982j.m1640a(asVar, hashSet);
                if (aHVar == null) {
                    aHVar = new aH(3, asVar);
                }
            }
        } else if (c == 0 && asVar != null) {
            m1639a(asVar, C0982j.m1632a(2, b));
            C0982j.m1640a(asVar, hashSet);
        }
        as[] b6 = c0970x.m1253b(3);
        int i4 = 0;
        while (i4 < b6.length) {
            int b7 = b6[i4].m1451b();
            if ((b7 == 1 || b7 == 28 || b7 == 38) && hashSet.contains(b6[i4].m1453d())) {
                m1639a(b6[i4], C0982j.m1632a(3, b));
            }
            i4++;
        }
        if (!a) {
            return aHVar;
        }
        System.out.println(new StringBuffer("addMessage: ").append(aHVar).toString());
        return aHVar;
    }

    public final aH m1645a(ai aiVar, int i, int i2) {
        return m1643c(aiVar, i, i2);
    }

    public final synchronized void m1646a() {
        this.f1161a.clear();
    }

    public final String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        synchronized (this) {
            for (Object a : this.f1161a.values()) {
                C0984m[] a2 = m1641a(a);
                for (Object append : a2) {
                    stringBuffer.append(append);
                    stringBuffer.append("\n");
                }
            }
        }
        return stringBuffer.toString();
    }
}
