package com.kontagent.deps;

public final class bi {
    private static final char[] f1076a;

    static {
        f1076a = "0123456789ABCDEF".toCharArray();
    }

    public static String m1546a(String str, byte[] bArr) {
        int length = bArr.length;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(new StringBuffer().append(length).append("b").toString());
        if (str != null) {
            stringBuffer.append(new StringBuffer(" (").append(str).append(")").toString());
        }
        stringBuffer.append(':');
        int length2 = (stringBuffer.toString().length() + 8) & -8;
        stringBuffer.append('\t');
        int i = (80 - length2) / 3;
        int i2 = 0;
        while (i2 < length) {
            int i3;
            if (i2 != 0 && i2 % i == 0) {
                stringBuffer.append('\n');
                for (i3 = 0; i3 < length2 / 8; i3++) {
                    stringBuffer.append('\t');
                }
            }
            i3 = bArr[i2] & 255;
            stringBuffer.append(f1076a[i3 >> 4]);
            stringBuffer.append(f1076a[i3 & 15]);
            stringBuffer.append(' ');
            i2++;
        }
        stringBuffer.append('\n');
        return stringBuffer.toString();
    }
}
