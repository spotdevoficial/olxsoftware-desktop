package com.kontagent.deps;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

public final class cA implements bt {
    private static cB f1090b;
    private static AtomicReferenceFieldUpdater f1091c;
    volatile cB f1092a;

    static {
        f1090b = new cB(false, cC.m1563a());
        f1091c = AtomicReferenceFieldUpdater.newUpdater(cA.class, cB.class, "a");
    }

    public cA() {
        this.f1092a = f1090b;
    }

    public final void m1558a() {
        cB cBVar;
        do {
            cBVar = this.f1092a;
            if (!cBVar.f1093a) {
            } else {
                return;
            }
        } while (!f1091c.compareAndSet(this, cBVar, cBVar.m1561a()));
        cBVar.f1094b.m1485a();
    }

    public final void m1559a(bt btVar) {
        if (btVar == null) {
            throw new IllegalArgumentException("Subscription can not be null");
        }
        cB cBVar;
        do {
            cBVar = this.f1092a;
            if (cBVar.f1093a) {
                btVar.m1485a();
                return;
            }
        } while (!f1091c.compareAndSet(this, cBVar, cBVar.m1562a(btVar)));
    }

    public final boolean m1560b() {
        return this.f1092a.f1093a;
    }
}
