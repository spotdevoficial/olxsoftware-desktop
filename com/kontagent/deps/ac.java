package com.kontagent.deps;

public final class ac extends av {
    private int f949e;
    private int f950f;
    private int f951g;
    private byte[] f952h;

    ac() {
    }

    final av m1383a() {
        return new ac();
    }

    final void m1384a(C0994v c0994v) {
        this.f949e = c0994v.m1675b();
        this.f950f = c0994v.m1675b();
        this.f951g = c0994v.m1676c();
        int b = c0994v.m1675b();
        if (b > 0) {
            this.f952h = c0994v.m1674a(b);
        } else {
            this.f952h = null;
        }
    }

    final void m1385a(C0996x c0996x, C0987o c0987o, boolean z) {
        c0996x.m1689b(this.f949e);
        c0996x.m1689b(this.f950f);
        c0996x.m1691c(this.f951g);
        if (this.f952h != null) {
            c0996x.m1689b(this.f952h.length);
            c0996x.m1686a(this.f952h);
            return;
        }
        c0996x.m1689b(0);
    }

    final String m1386b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.f949e);
        stringBuffer.append(' ');
        stringBuffer.append(this.f950f);
        stringBuffer.append(' ');
        stringBuffer.append(this.f951g);
        stringBuffer.append(' ');
        if (this.f952h == null) {
            stringBuffer.append('-');
        } else {
            stringBuffer.append(C0974e.m1331b(this.f952h));
        }
        return stringBuffer.toString();
    }
}
