package com.kontagent.deps;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

abstract class aQ extends av {
    private List f899e;

    protected aQ() {
    }

    final void m1290a(C0994v c0994v) {
        this.f899e = new ArrayList(2);
        while (c0994v.m1672a() > 0) {
            this.f899e.add(c0994v.m1679f());
        }
    }

    final void m1291a(C0996x c0996x, C0987o c0987o, boolean z) {
        for (byte[] b : this.f899e) {
            c0996x.m1690b(b);
        }
    }

    final String m1292b() {
        StringBuffer stringBuffer = new StringBuffer();
        Iterator it = this.f899e.iterator();
        while (it.hasNext()) {
            stringBuffer.append(av.m1147a((byte[]) it.next(), true));
            if (it.hasNext()) {
                stringBuffer.append(" ");
            }
        }
        return stringBuffer.toString();
    }

    public final List m1293d() {
        List arrayList = new ArrayList(this.f899e.size());
        for (int i = 0; i < this.f899e.size(); i++) {
            arrayList.add(av.m1147a((byte[]) this.f899e.get(i), false));
        }
        return arrayList;
    }
}
