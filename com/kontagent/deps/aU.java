package com.kontagent.deps;

import java.util.HashMap;

final class aU extends C0971Y {
    private HashMap f942a;

    public aU() {
        super("Type", 2);
        m1265a("TYPE");
        this.f942a = new HashMap();
    }

    public final void m1364a(int i) {
        aT.m1360a(i);
    }

    public final void m1365a(int i, String str, av avVar) {
        super.m1263a(i, str);
        this.f942a.put(C0971Y.m1261c(i), avVar);
    }

    public final av m1366e(int i) {
        aT.m1360a(i);
        return (av) this.f942a.get(C0971Y.m1261c(i));
    }
}
