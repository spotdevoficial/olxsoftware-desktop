package com.kontagent.deps;

/* renamed from: com.kontagent.deps.s */
public final class C0991s extends av {
    private byte[] f1177e;

    C0991s() {
    }

    final av m1662a() {
        return new C0991s();
    }

    final void m1663a(C0994v c0994v) {
        this.f1177e = c0994v.m1678e();
    }

    final void m1664a(C0996x c0996x, C0987o c0987o, boolean z) {
        c0996x.m1686a(this.f1177e);
    }

    final String m1665b() {
        return C0987o.m1656a(this.f1177e);
    }
}
