package com.kontagent.deps;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class bS extends br implements bt {
    private final ScheduledExecutorService f1030a;
    private final ci f1031b;
    private volatile boolean f1032c;

    public bS(ThreadFactory threadFactory) {
        this.f1030a = Executors.newScheduledThreadPool(1, threadFactory);
        this.f1031b = cg.m1577a().m1581d();
    }

    public final bt m1496a(bD bDVar) {
        return m1497a(bDVar, 0, null);
    }

    public final bt m1497a(bD bDVar, long j, TimeUnit timeUnit) {
        return this.f1032c ? cC.m1563a() : m1499b(bDVar, j, timeUnit);
    }

    public final void m1498a() {
        this.f1032c = true;
        this.f1030a.shutdownNow();
    }

    public final bT m1499b(bD bDVar, long j, TimeUnit timeUnit) {
        ci ciVar = this.f1031b;
        Runnable bTVar = new bT(ci.m1582a(bDVar));
        bTVar.m1502a(j <= 0 ? this.f1030a.submit(bTVar) : this.f1030a.schedule(bTVar, j, timeUnit));
        return bTVar;
    }
}
