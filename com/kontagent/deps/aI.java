package com.kontagent.deps;

import com.facebook.appevents.AppEventsConstants;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.List;

public final class aI implements ax {
    private static String f915c;
    private static int f916d;
    private InetSocketAddress f917a;
    private long f918b;

    static {
        f915c = "localhost";
        f916d = 0;
    }

    public aI() {
        this(null);
    }

    public aI(String str) {
        this.f918b = 10000;
        if (str == null) {
            str = ay.m1468e().m1473b();
            if (str == null) {
                str = f915c;
            }
        }
        this.f917a = new InetSocketAddress(str.equals(AppEventsConstants.EVENT_PARAM_VALUE_NO) ? InetAddress.getLocalHost() : InetAddress.getByName(str), 53);
    }

    private static C0970X m1318a(byte[] bArr) {
        try {
            return new C0970X(bArr);
        } catch (IOException e) {
            IOException e2 = e;
            if (an.m1435a("verbose")) {
                e2.printStackTrace();
            }
            if (!(e2 instanceof bb)) {
                e2 = new bb("Error parsing message");
            }
            throw ((bb) e2);
        }
    }

    private C0970X m1319b(C0970X c0970x) {
        be beVar = new be(c0970x.m1252b().m1159g(), 252, 0, false, this.f917a, null);
        int i = (int) (this.f918b / 1000);
        if (i < 0) {
            throw new IllegalArgumentException("timeout cannot be negative");
        }
        beVar.f1052b = ((long) i) * 1000;
        beVar.f1051a = null;
        try {
            beVar.m1539a();
            List<av> list = beVar.f1053c;
            C0970X c0970x2 = new C0970X(c0970x.m1249a().m1181b());
            c0970x2.m1249a().m1177a(5);
            c0970x2.m1249a().m1177a(0);
            c0970x2.m1250a(c0970x.m1252b(), 0);
            for (av a : list) {
                c0970x2.m1250a(a, 1);
            }
            return c0970x2;
        } catch (bd e) {
            throw new bb(e.getMessage());
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.kontagent.deps.C0970X m1320a(com.kontagent.deps.C0970X r15) {
        /*
        r14 = this;
        r7 = 0;
        r0 = 0;
        r9 = 1;
        r1 = "verbose";
        r1 = com.kontagent.deps.an.m1435a(r1);
        if (r1 == 0) goto L_0x0039;
    L_0x000b:
        r1 = java.lang.System.err;
        r2 = new java.lang.StringBuffer;
        r3 = "Sending to ";
        r2.<init>(r3);
        r3 = r14.f917a;
        r3 = r3.getAddress();
        r3 = r3.getHostAddress();
        r2 = r2.append(r3);
        r3 = ":";
        r2 = r2.append(r3);
        r3 = r14.f917a;
        r3 = r3.getPort();
        r2 = r2.append(r3);
        r2 = r2.toString();
        r1.println(r2);
    L_0x0039:
        r1 = r15.m1249a();
        r1 = r1.m1185d();
        if (r1 != 0) goto L_0x0056;
    L_0x0043:
        r1 = r15.m1252b();
        if (r1 == 0) goto L_0x0056;
    L_0x0049:
        r1 = r1.m1160h();
        r2 = 252; // 0xfc float:3.53E-43 double:1.245E-321;
        if (r1 != r2) goto L_0x0056;
    L_0x0051:
        r0 = r14.m1319b(r15);
    L_0x0055:
        return r0;
    L_0x0056:
        r1 = r15.clone();
        r6 = r1;
        r6 = (com.kontagent.deps.C0970X) r6;
        r1 = 65535; // 0xffff float:9.1834E-41 double:3.23786E-319;
        r2 = r6.m1255c(r1);
        r1 = r6.m1257e();
        if (r1 != 0) goto L_0x008d;
    L_0x006a:
        r3 = 512; // 0x200 float:7.175E-43 double:2.53E-321;
    L_0x006c:
        r4 = java.lang.System.currentTimeMillis();
        r10 = r14.f918b;
        r4 = r4 + r10;
        r1 = r7;
    L_0x0074:
        r8 = r2.length;
        if (r8 <= r3) goto L_0x0122;
    L_0x0077:
        r8 = r9;
    L_0x0078:
        if (r8 == 0) goto L_0x0092;
    L_0x007a:
        r1 = r14.f917a;
        r1 = com.kontagent.deps.aL.m1334a(r0, r1, r2, r4);
    L_0x0080:
        r10 = r1.length;
        r11 = 12;
        if (r10 >= r11) goto L_0x0099;
    L_0x0085:
        r0 = new com.kontagent.deps.bb;
        r1 = "invalid DNS header - too short";
        r0.<init>(r1);
        throw r0;
    L_0x008d:
        r3 = r1.m1432d();
        goto L_0x006c;
    L_0x0092:
        r1 = r14.f917a;
        r1 = com.kontagent.deps.aX.m1372a(r0, r1, r2, r3, r4);
        goto L_0x0080;
    L_0x0099:
        r10 = r1[r7];
        r10 = r10 & 255;
        r10 = r10 << 8;
        r11 = r1[r9];
        r11 = r11 & 255;
        r10 = r10 + r11;
        r11 = r6.m1249a();
        r11 = r11.m1181b();
        if (r10 == r11) goto L_0x00de;
    L_0x00ae:
        r1 = new java.lang.StringBuffer;
        r12 = "invalid message id: expected ";
        r1.<init>(r12);
        r1 = r1.append(r11);
        r11 = "; got id ";
        r1 = r1.append(r11);
        r1 = r1.append(r10);
        r1 = r1.toString();
        if (r8 == 0) goto L_0x00cf;
    L_0x00c9:
        r0 = new com.kontagent.deps.bb;
        r0.<init>(r1);
        throw r0;
    L_0x00cf:
        r10 = "verbose";
        r10 = com.kontagent.deps.an.m1435a(r10);
        if (r10 == 0) goto L_0x011f;
    L_0x00d7:
        r10 = java.lang.System.err;
        r10.println(r1);
        r1 = r8;
        goto L_0x0074;
    L_0x00de:
        r10 = m1318a(r1);
        if (r0 == 0) goto L_0x010c;
    L_0x00e4:
        r11 = r6.m1254c();
        r1 = r0.m1347a(r10, r1, r11);
        r11 = "verbose";
        r11 = com.kontagent.deps.an.m1435a(r11);
        if (r11 == 0) goto L_0x010c;
    L_0x00f4:
        r11 = java.lang.System.err;
        r12 = new java.lang.StringBuffer;
        r13 = "TSIG verify: ";
        r12.<init>(r13);
        r1 = com.kontagent.deps.au.m1458a(r1);
        r1 = r12.append(r1);
        r1 = r1.toString();
        r11.println(r1);
    L_0x010c:
        if (r8 != 0) goto L_0x011c;
    L_0x010e:
        r1 = r10.m1249a();
        r8 = 6;
        r1 = r1.m1182b(r8);
        if (r1 == 0) goto L_0x011c;
    L_0x0119:
        r1 = r9;
        goto L_0x0074;
    L_0x011c:
        r0 = r10;
        goto L_0x0055;
    L_0x011f:
        r1 = r8;
        goto L_0x0074;
    L_0x0122:
        r8 = r1;
        goto L_0x0078;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kontagent.deps.aI.a(com.kontagent.deps.X):com.kontagent.deps.X");
    }

    public final Object m1321a(C0970X c0970x, az azVar) {
        Integer num;
        synchronized (this) {
            int i = f916d;
            f916d = i + 1;
            num = new Integer(i);
        }
        av b = c0970x.m1252b();
        String stringBuffer = new StringBuffer().append(getClass()).append(": ").append(b != null ? b.m1159g().toString() : "(none)").toString();
        Thread awVar = new aw(this, c0970x, num, azVar);
        awVar.setName(stringBuffer);
        awVar.setDaemon(true);
        awVar.start();
        return num;
    }

    public final void m1322a(int i) {
        m1323a(5, 0);
    }

    public final void m1323a(int i, int i2) {
        this.f918b = (((long) i) * 1000) + ((long) i2);
    }
}
