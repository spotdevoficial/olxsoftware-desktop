package com.kontagent.deps;

public final class aC extends av {
    private ai f892e;
    private ai f893f;
    private long f894g;
    private long f895h;
    private long f896i;
    private long f897j;
    private long f898k;

    aC() {
    }

    public aC(ai aiVar, int i, long j, ai aiVar2, ai aiVar3, long j2, long j3, long j4, long j5, long j6) {
        super(aiVar, 6, i, 0);
        this.f892e = av.m1141a(aiVar2);
        this.f893f = av.m1141a(aiVar3);
        this.f894g = av.m1140a("serial", j2);
        this.f895h = av.m1140a("refresh", 0);
        this.f896i = av.m1140a("retry", 0);
        this.f897j = av.m1140a("expire", 0);
        this.f898k = av.m1140a("minimum", 0);
    }

    final av m1284a() {
        return new aC();
    }

    final void m1285a(C0994v c0994v) {
        this.f892e = new ai(c0994v);
        this.f893f = new ai(c0994v);
        this.f894g = c0994v.m1677d();
        this.f895h = c0994v.m1677d();
        this.f896i = c0994v.m1677d();
        this.f897j = c0994v.m1677d();
        this.f898k = c0994v.m1677d();
    }

    final void m1286a(C0996x c0996x, C0987o c0987o, boolean z) {
        this.f892e.m1423a(c0996x, c0987o, z);
        this.f893f.m1423a(c0996x, c0987o, z);
        c0996x.m1685a(this.f894g);
        c0996x.m1685a(this.f895h);
        c0996x.m1685a(this.f896i);
        c0996x.m1685a(this.f897j);
        c0996x.m1685a(this.f898k);
    }

    final String m1287b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.f892e);
        stringBuffer.append(" ");
        stringBuffer.append(this.f893f);
        if (an.m1435a("multiline")) {
            stringBuffer.append(" (\n\t\t\t\t\t");
            stringBuffer.append(this.f894g);
            stringBuffer.append("\t; serial\n\t\t\t\t\t");
            stringBuffer.append(this.f895h);
            stringBuffer.append("\t; refresh\n\t\t\t\t\t");
            stringBuffer.append(this.f896i);
            stringBuffer.append("\t; retry\n\t\t\t\t\t");
            stringBuffer.append(this.f897j);
            stringBuffer.append("\t; expire\n\t\t\t\t\t");
            stringBuffer.append(this.f898k);
            stringBuffer.append(" )\t; minimum");
        } else {
            stringBuffer.append(" ");
            stringBuffer.append(this.f894g);
            stringBuffer.append(" ");
            stringBuffer.append(this.f895h);
            stringBuffer.append(" ");
            stringBuffer.append(this.f896i);
            stringBuffer.append(" ");
            stringBuffer.append(this.f897j);
            stringBuffer.append(" ");
            stringBuffer.append(this.f898k);
        }
        return stringBuffer.toString();
    }

    public final long m1288d() {
        return this.f894g;
    }

    public final long m1289e() {
        return this.f898k;
    }
}
