package com.kontagent.deps;

import com.facebook.BuildConfig;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Arrays;

public abstract class av implements Serializable, Cloneable, Comparable {
    private static final DecimalFormat f794e;
    protected ai f795a;
    protected int f796b;
    protected int f797c;
    protected long f798d;

    static {
        DecimalFormat decimalFormat = new DecimalFormat();
        f794e = decimalFormat;
        decimalFormat.setMinimumIntegerDigits(3);
    }

    protected av() {
    }

    av(ai aiVar, int i, int i2, long j) {
        if (aiVar.m1424a()) {
            aT.m1360a(i);
            C0989q.m1659a(i2);
            C0974e.m1326a(j);
            this.f795a = aiVar;
            this.f796b = i;
            this.f797c = i2;
            this.f798d = j;
            return;
        }
        throw new C0956J(aiVar);
    }

    static int m1139a(String str, int i) {
        if (i >= 0 && i <= 255) {
            return i;
        }
        throw new IllegalArgumentException(new StringBuffer("\"").append(str).append("\" ").append(i).append(" must be an unsigned 8 bit value").toString());
    }

    static long m1140a(String str, long j) {
        if (j >= 0 && j <= 4294967295L) {
            return j;
        }
        throw new IllegalArgumentException(new StringBuffer("\"").append(str).append("\" ").append(j).append(" must be an unsigned 32 bit value").toString());
    }

    static ai m1141a(ai aiVar) {
        if (aiVar.m1424a()) {
            return aiVar;
        }
        throw new C0956J(aiVar);
    }

    public static av m1142a(ai aiVar, int i, int i2) {
        return m1143a(aiVar, i, i2, 0);
    }

    private static av m1143a(ai aiVar, int i, int i2, long j) {
        if (aiVar.m1424a()) {
            aT.m1360a(i);
            C0989q.m1659a(i2);
            C0974e.m1326a(j);
            return m1144a(aiVar, i, i2, j, false);
        }
        throw new C0956J(aiVar);
    }

    private static final av m1144a(ai aiVar, int i, int i2, long j, boolean z) {
        av c;
        if (z) {
            c = aT.m1362c(i);
            c = c != null ? c.m1150a() : new aZ();
        } else {
            c = new C0998z();
        }
        c.f795a = aiVar;
        c.f796b = i;
        c.f797c = i2;
        c.f798d = j;
        return c;
    }

    static av m1145a(C0994v c0994v, int i, boolean z) {
        ai aiVar = new ai(c0994v);
        int c = c0994v.m1676c();
        int c2 = c0994v.m1676c();
        if (i == 0) {
            return m1143a(aiVar, c, c2, 0);
        }
        long d = c0994v.m1677d();
        int c3 = c0994v.m1676c();
        if (c3 == 0 && z) {
            return m1143a(aiVar, c, c2, d);
        }
        av a = m1144a(aiVar, c, c2, d, c0994v != null);
        if (c0994v == null) {
            return a;
        }
        if (c0994v.m1672a() < c3) {
            throw new bb("truncated record");
        } else if (c3 > c0994v.f1182a.length - c0994v.f1183b) {
            throw new IllegalArgumentException("cannot set active region past end of input");
        } else {
            c0994v.f1184c = c0994v.f1183b + c3;
            a.m1152a(c0994v);
            if (c0994v.m1672a() > 0) {
                throw new bb("invalid record length");
            }
            c0994v.f1184c = c0994v.f1182a.length;
            return a;
        }
    }

    protected static String m1146a(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("\\# ");
        stringBuffer.append(bArr.length);
        stringBuffer.append(" ");
        stringBuffer.append(C0974e.m1331b(bArr));
        return stringBuffer.toString();
    }

    protected static String m1147a(byte[] bArr, boolean z) {
        StringBuffer stringBuffer = new StringBuffer();
        if (z) {
            stringBuffer.append('\"');
        }
        for (byte b : bArr) {
            int i = b & 255;
            if (i < 32 || i >= 127) {
                stringBuffer.append('\\');
                stringBuffer.append(f794e.format((long) i));
            } else if (i == 34 || i == 92) {
                stringBuffer.append('\\');
                stringBuffer.append((char) i);
            } else {
                stringBuffer.append((char) i);
            }
        }
        if (z) {
            stringBuffer.append('\"');
        }
        return stringBuffer.toString();
    }

    static int m1148b(String str, int i) {
        if (i >= 0 && i <= 65535) {
            return i;
        }
        throw new IllegalArgumentException(new StringBuffer("\"").append(str).append("\" ").append(i).append(" must be an unsigned 16 bit value").toString());
    }

    private byte[] m1149d() {
        C0996x c0996x = new C0996x();
        m1154a(c0996x, null, true);
        return c0996x.m1692c();
    }

    abstract av m1150a();

    final void m1151a(long j) {
        this.f798d = j;
    }

    abstract void m1152a(C0994v c0994v);

    final void m1153a(C0996x c0996x, int i, C0987o c0987o) {
        this.f795a.m1422a(c0996x, c0987o);
        c0996x.m1691c(this.f796b);
        c0996x.m1691c(this.f797c);
        if (i != 0) {
            c0996x.m1685a(this.f798d);
            int i2 = c0996x.f1187a;
            c0996x.m1691c(0);
            m1154a(c0996x, c0987o, false);
            int i3 = (c0996x.f1187a - i2) - 2;
            c0996x.m1683a();
            c0996x.m1684a(i2);
            c0996x.m1691c(i3);
            c0996x.m1688b();
        }
    }

    abstract void m1154a(C0996x c0996x, C0987o c0987o, boolean z);

    public final boolean m1155a(av avVar) {
        return m1161i() == avVar.m1161i() && this.f797c == avVar.f797c && this.f795a.equals(avVar.f795a);
    }

    abstract String m1156b();

    public ai m1157c() {
        return null;
    }

    public int compareTo(Object obj) {
        int i = 0;
        av avVar = (av) obj;
        if (this == avVar) {
            return 0;
        }
        int compareTo = this.f795a.compareTo(avVar.f795a);
        if (compareTo != 0) {
            return compareTo;
        }
        compareTo = this.f797c - avVar.f797c;
        if (compareTo != 0) {
            return compareTo;
        }
        compareTo = this.f796b - avVar.f796b;
        if (compareTo != 0) {
            return compareTo;
        }
        byte[] d = m1149d();
        byte[] d2 = avVar.m1149d();
        while (i < d.length && i < d2.length) {
            compareTo = (d[i] & 255) - (d2[i] & 255);
            if (compareTo != 0) {
                return compareTo;
            }
            i++;
        }
        return d.length - d2.length;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof av)) {
            return false;
        }
        av avVar = (av) obj;
        return (this.f796b == avVar.f796b && this.f797c == avVar.f797c && this.f795a.equals(avVar.f795a)) ? Arrays.equals(m1149d(), avVar.m1149d()) : false;
    }

    public final String m1158f() {
        return m1156b();
    }

    public final ai m1159g() {
        return this.f795a;
    }

    public final int m1160h() {
        return this.f796b;
    }

    public int hashCode() {
        int i = 0;
        C0996x c0996x = new C0996x();
        this.f795a.m1421a(c0996x);
        c0996x.m1691c(this.f796b);
        c0996x.m1691c(this.f797c);
        c0996x.m1685a(0);
        int i2 = c0996x.f1187a;
        c0996x.m1691c(0);
        m1154a(c0996x, null, true);
        int i3 = (c0996x.f1187a - i2) - 2;
        c0996x.m1683a();
        c0996x.m1684a(i2);
        c0996x.m1691c(i3);
        c0996x.m1688b();
        byte[] c = c0996x.m1692c();
        int i4 = 0;
        while (i < c.length) {
            i4 += (i4 << 3) + (c[i] & 255);
            i++;
        }
        return i4;
    }

    public final int m1161i() {
        return this.f796b == 46 ? ((ar) this).m1282d() : this.f796b;
    }

    public final int m1162j() {
        return this.f797c;
    }

    public final long m1163k() {
        return this.f798d;
    }

    final av m1164l() {
        try {
            return (av) clone();
        } catch (CloneNotSupportedException e) {
            throw new IllegalStateException();
        }
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.f795a);
        if (stringBuffer.length() < 8) {
            stringBuffer.append("\t");
        }
        if (stringBuffer.length() < 16) {
            stringBuffer.append("\t");
        }
        stringBuffer.append("\t");
        if (an.m1435a("BINDTTL")) {
            long j = this.f798d;
            C0974e.m1326a(j);
            StringBuffer stringBuffer2 = new StringBuffer();
            long j2 = j % 60;
            j /= 60;
            long j3 = j % 60;
            j /= 60;
            long j4 = j % 24;
            j /= 24;
            long j5 = j % 7;
            j /= 7;
            if (j > 0) {
                stringBuffer2.append(new StringBuffer().append(j).append("W").toString());
            }
            if (j5 > 0) {
                stringBuffer2.append(new StringBuffer().append(j5).append("D").toString());
            }
            if (j4 > 0) {
                stringBuffer2.append(new StringBuffer().append(j4).append("H").toString());
            }
            if (j3 > 0) {
                stringBuffer2.append(new StringBuffer().append(j3).append("M").toString());
            }
            if (j2 > 0 || (j == 0 && j5 == 0 && j4 == 0 && j3 == 0)) {
                stringBuffer2.append(new StringBuffer().append(j2).append("S").toString());
            }
            stringBuffer.append(stringBuffer2.toString());
        } else {
            stringBuffer.append(this.f798d);
        }
        stringBuffer.append("\t");
        if (!(this.f797c == 1 && an.m1435a("noPrintIN"))) {
            stringBuffer.append(C0989q.m1660b(this.f797c));
            stringBuffer.append("\t");
        }
        stringBuffer.append(aT.m1361b(this.f796b));
        String b = m1156b();
        if (!b.equals(BuildConfig.VERSION_NAME)) {
            stringBuffer.append("\t");
            stringBuffer.append(b);
        }
        return stringBuffer.toString();
    }
}
