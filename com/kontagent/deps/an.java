package com.kontagent.deps;

import com.schibsted.scm.nextgenapp.models.submodels.Setting;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public final class an {
    private static Map f979a;

    static {
        try {
            String property = System.getProperty("dnsjava.options");
            if (property != null) {
                StringTokenizer stringTokenizer = new StringTokenizer(property, ",");
                while (stringTokenizer.hasMoreTokens()) {
                    property = stringTokenizer.nextToken();
                    int indexOf = property.indexOf(61);
                    if (indexOf == -1) {
                        if (f979a == null) {
                            f979a = new HashMap();
                        }
                        f979a.put(property.toLowerCase(), Setting.TRUE);
                    } else {
                        String substring = property.substring(0, indexOf);
                        property = property.substring(indexOf + 1);
                        if (f979a == null) {
                            f979a = new HashMap();
                        }
                        f979a.put(substring.toLowerCase(), property.toLowerCase());
                    }
                }
            }
        } catch (SecurityException e) {
        }
    }

    public static boolean m1435a(String str) {
        return (f979a == null || f979a.get(str.toLowerCase()) == null) ? false : true;
    }

    public static int m1436b(String str) {
        String str2 = f979a == null ? null : (String) f979a.get(str.toLowerCase());
        if (str2 != null) {
            try {
                int parseInt = Integer.parseInt(str2);
                if (parseInt > 0) {
                    return parseInt;
                }
            } catch (NumberFormatException e) {
            }
        }
        return -1;
    }
}
