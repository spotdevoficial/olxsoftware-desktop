package com.kontagent.deps;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public final class bg {
    public MessageDigest f1070a;
    public byte[] f1071b;
    private byte[] f1072c;

    public bg(String str, byte[] bArr) {
        try {
            this.f1070a = MessageDigest.getInstance(str);
            m1540c(bArr);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException(new StringBuffer("unknown digest algorithm ").append(str).toString());
        }
    }

    private void m1540c(byte[] bArr) {
        if (bArr.length > 64) {
            bArr = this.f1070a.digest(bArr);
            this.f1070a.reset();
        }
        this.f1071b = new byte[64];
        this.f1072c = new byte[64];
        int i = 0;
        while (i < bArr.length) {
            this.f1071b[i] = (byte) (bArr[i] ^ 54);
            this.f1072c[i] = (byte) (bArr[i] ^ 92);
            i++;
        }
        while (i < 64) {
            this.f1071b[i] = (byte) 54;
            this.f1072c[i] = (byte) 92;
            i++;
        }
        this.f1070a.update(this.f1071b);
    }

    public final void m1541a(byte[] bArr) {
        this.f1070a.update(bArr);
    }

    public final void m1542a(byte[] bArr, int i, int i2) {
        this.f1070a.update(bArr, i, i2);
    }

    public final byte[] m1543a() {
        byte[] digest = this.f1070a.digest();
        this.f1070a.reset();
        this.f1070a.update(this.f1072c);
        return this.f1070a.digest(digest);
    }

    public final boolean m1544b(byte[] bArr) {
        return Arrays.equals(bArr, m1543a());
    }
}
