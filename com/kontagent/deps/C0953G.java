package com.kontagent.deps;

import java.util.Random;

/* renamed from: com.kontagent.deps.G */
public final class C0953G implements Cloneable {
    private static Random f804d;
    private int f805a;
    private int f806b;
    private int[] f807c;

    static {
        f804d = new Random();
    }

    public C0953G() {
        m1173e();
    }

    public C0953G(int i) {
        m1173e();
        if (i < 0 || i > 65535) {
            throw new IllegalArgumentException(new StringBuffer("DNS message ID ").append(i).append(" is out of range").toString());
        }
        this.f805a = i;
    }

    C0953G(C0994v c0994v) {
        this(c0994v.m1676c());
        this.f806b = c0994v.m1676c();
        for (int i = 0; i < this.f807c.length; i++) {
            this.f807c[i] = c0994v.m1676c();
        }
    }

    private void m1173e() {
        this.f807c = new int[4];
        this.f806b = 0;
        this.f805a = -1;
    }

    private String m1174f() {
        StringBuffer stringBuffer = new StringBuffer();
        int i = 0;
        while (i < 16) {
            if (C0953G.m1175h(i) && m1182b(i)) {
                stringBuffer.append(C0949C.m1136a(i));
                stringBuffer.append(" ");
            }
            i++;
        }
        return stringBuffer.toString();
    }

    private static boolean m1175h(int i) {
        return i >= 0 && i <= 15 && C0949C.m1137b(i);
    }

    private static void m1176i(int i) {
        if (!C0953G.m1175h(i)) {
            throw new IllegalArgumentException(new StringBuffer("invalid flag bit ").append(i).toString());
        }
    }

    public final void m1177a(int i) {
        C0953G.m1176i(i);
        this.f806b |= 1 << (15 - i);
    }

    final void m1178a(int i, int i2) {
        if (i2 < 0 || i2 > 65535) {
            throw new IllegalArgumentException(new StringBuffer("DNS section count ").append(i2).append(" is out of range").toString());
        }
        this.f807c[i] = i2;
    }

    final void m1179a(C0996x c0996x) {
        c0996x.m1691c(m1181b());
        c0996x.m1691c(this.f806b);
        for (int c : this.f807c) {
            c0996x.m1691c(c);
        }
    }

    public final byte[] m1180a() {
        C0996x c0996x = new C0996x();
        m1179a(c0996x);
        return c0996x.m1692c();
    }

    public final int m1181b() {
        if (this.f805a >= 0) {
            return this.f805a;
        }
        int i;
        synchronized (this) {
            if (this.f805a < 0) {
                this.f805a = f804d.nextInt(65535);
            }
            i = this.f805a;
        }
        return i;
    }

    public final boolean m1182b(int i) {
        C0953G.m1176i(i);
        return (this.f806b & (1 << (15 - i))) != 0;
    }

    public final int m1183c() {
        return this.f806b & 15;
    }

    public final void m1184c(int i) {
        this.f806b &= 34815;
        this.f806b = this.f806b;
    }

    public final Object clone() {
        C0953G c0953g = new C0953G();
        c0953g.f805a = this.f805a;
        c0953g.f806b = this.f806b;
        System.arraycopy(this.f807c, 0, c0953g.f807c, 0, this.f807c.length);
        return c0953g;
    }

    public final int m1185d() {
        return (this.f806b >> 11) & 15;
    }

    final void m1186d(int i) {
        if (this.f807c[i] == 65535) {
            throw new IllegalStateException("DNS section count cannot be incremented");
        }
        int[] iArr = this.f807c;
        iArr[i] = iArr[i] + 1;
    }

    final void m1187e(int i) {
        if (this.f807c[3] == 0) {
            throw new IllegalStateException("DNS section count cannot be decremented");
        }
        int[] iArr = this.f807c;
        iArr[3] = iArr[3] - 1;
    }

    public final int m1188f(int i) {
        return this.f807c[i];
    }

    final String m1189g(int i) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(";; ->>HEADER<<- ");
        stringBuffer.append(new StringBuffer("opcode: ").append(am.m1434a(m1185d())).toString());
        stringBuffer.append(new StringBuffer(", status: ").append(au.m1458a(i)).toString());
        stringBuffer.append(new StringBuffer(", id: ").append(m1181b()).toString());
        stringBuffer.append("\n");
        stringBuffer.append(new StringBuffer(";; flags: ").append(m1174f()).toString());
        stringBuffer.append("; ");
        for (int i2 = 0; i2 < 4; i2++) {
            stringBuffer.append(new StringBuffer().append(aG.m1304a(i2)).append(": ").append(this.f807c[i2]).append(" ").toString());
        }
        return stringBuffer.toString();
    }

    public final String toString() {
        return m1189g(this.f806b & 15);
    }
}
