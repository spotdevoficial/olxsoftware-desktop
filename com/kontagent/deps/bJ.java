package com.kontagent.deps;

final class bJ extends bs {
    private boolean f1009b;
    private /* synthetic */ bs f1010c;
    private /* synthetic */ bI f1011d;

    bJ(bI bIVar, bs bsVar, bs bsVar2) {
        this.f1011d = bIVar;
        this.f1010c = bsVar2;
        super(bsVar);
        this.f1009b = false;
    }

    public final void onCompleted() {
        if (!this.f1009b) {
            try {
                this.f1011d.f1007a.onCompleted();
                this.f1009b = true;
                this.f1010c.onCompleted();
            } catch (Throwable th) {
                onError(th);
            }
        }
    }

    public final void onError(Throwable th) {
        C0974e.m1328a(th);
        if (!this.f1009b) {
            this.f1009b = true;
            try {
                this.f1011d.f1007a.onError(th);
                this.f1010c.onError(th);
            } catch (Throwable th2) {
                this.f1010c.onError(th2);
            }
        }
    }

    public final void onNext(Object obj) {
        if (!this.f1009b) {
            try {
                this.f1011d.f1007a.onNext(obj);
                this.f1010c.onNext(obj);
            } catch (Throwable th) {
                onError(bB.m1480a(th, obj));
            }
        }
    }
}
