package com.kontagent.deps;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class as implements Serializable {
    private List f985a;
    private short f986b;
    private short f987c;

    public as() {
        this.f985a = new ArrayList(1);
        this.f986b = (short) 0;
        this.f987c = (short) 0;
    }

    public as(as asVar) {
        synchronized (asVar) {
            this.f985a = (List) ((ArrayList) asVar.f985a).clone();
            this.f986b = asVar.f986b;
            this.f987c = asVar.f987c;
        }
    }

    public as(av avVar) {
        this();
        m1449b(avVar);
    }

    private static String m1447a(Iterator it) {
        StringBuffer stringBuffer = new StringBuffer();
        while (it.hasNext()) {
            av avVar = (av) it.next();
            stringBuffer.append("[");
            stringBuffer.append(avVar.m1158f());
            stringBuffer.append("]");
            if (it.hasNext()) {
                stringBuffer.append(" ");
            }
        }
        return stringBuffer.toString();
    }

    private synchronized Iterator m1448a(boolean z, boolean z2) {
        Iterator it;
        int i = 0;
        synchronized (this) {
            int size = this.f985a.size();
            short s = z ? size - this.f986b : this.f986b;
            if (s == (short) 0) {
                it = Collections.EMPTY_LIST.iterator();
            } else {
                if (!z) {
                    i = size - this.f986b;
                } else if (z2) {
                    if (this.f987c >= s) {
                        this.f987c = (short) 0;
                    }
                    i = this.f987c;
                    this.f987c = (short) (i + 1);
                }
                List arrayList = new ArrayList(s);
                if (z) {
                    arrayList.addAll(this.f985a.subList(i, s));
                    if (i != 0) {
                        arrayList.addAll(this.f985a.subList(0, i));
                    }
                } else {
                    arrayList.addAll(this.f985a.subList(i, size));
                }
                it = arrayList.iterator();
            }
        }
        return it;
    }

    private void m1449b(av avVar) {
        if (avVar instanceof ar) {
            this.f985a.add(avVar);
            this.f986b = (short) (this.f986b + 1);
        } else if (this.f986b == (short) 0) {
            this.f985a.add(avVar);
        } else {
            this.f985a.add(this.f985a.size() - this.f986b, avVar);
        }
    }

    public final synchronized void m1450a(av avVar) {
        if (this.f985a.size() == 0) {
            m1449b(avVar);
        } else {
            av g = m1456g();
            if (avVar.m1155a(g)) {
                if (avVar.m1163k() != g.m1163k()) {
                    if (avVar.m1163k() > g.m1163k()) {
                        avVar = avVar.m1164l();
                        avVar.m1151a(g.m1163k());
                    } else {
                        for (int i = 0; i < this.f985a.size(); i++) {
                            g = ((av) this.f985a.get(i)).m1164l();
                            g.m1151a(avVar.m1163k());
                            this.f985a.set(i, g);
                        }
                    }
                }
                if (!this.f985a.contains(avVar)) {
                    m1449b(avVar);
                }
            } else {
                throw new IllegalArgumentException("record does not match rrset");
            }
        }
    }

    public final int m1451b() {
        return m1456g().m1161i();
    }

    public final synchronized Iterator m1452c() {
        return m1448a(true, true);
    }

    public final ai m1453d() {
        return m1456g().m1159g();
    }

    public final int m1454e() {
        return m1456g().m1162j();
    }

    public final synchronized long m1455f() {
        return m1456g().m1163k();
    }

    public final synchronized av m1456g() {
        if (this.f985a.size() == 0) {
            throw new IllegalStateException("rrset is empty");
        }
        return (av) this.f985a.get(0);
    }

    public String toString() {
        if (this.f985a == null) {
            return "{empty}";
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("{ ");
        stringBuffer.append(new StringBuffer().append(m1456g().m1159g()).append(" ").toString());
        stringBuffer.append(new StringBuffer().append(m1455f()).append(" ").toString());
        stringBuffer.append(new StringBuffer().append(C0989q.m1660b(m1456g().m1162j())).append(" ").toString());
        stringBuffer.append(new StringBuffer().append(aT.m1361b(m1456g().m1161i())).append(" ").toString());
        stringBuffer.append(m1447a(m1448a(true, false)));
        if (this.f986b > (short) 0) {
            stringBuffer.append(" sigs: ");
            stringBuffer.append(m1447a(m1448a(false, false)));
        }
        stringBuffer.append(" }");
        return stringBuffer.toString();
    }
}
