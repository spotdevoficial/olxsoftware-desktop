package com.kontagent.deps;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/* renamed from: com.kontagent.deps.X */
public final class C0970X implements Cloneable {
    private static av[] f860f;
    private static as[] f861g;
    int f862a;
    int f863b;
    private C0953G f864c;
    private List[] f865d;
    private int f866e;

    static {
        f860f = new av[0];
        f861g = new as[0];
    }

    public C0970X() {
        this(new C0953G());
    }

    public C0970X(int i) {
        this(new C0953G(i));
    }

    private C0970X(C0953G c0953g) {
        this.f865d = new List[4];
        this.f864c = c0953g;
    }

    private C0970X(C0994v c0994v) {
        this(new C0953G(c0994v));
        boolean z = this.f864c.m1185d() == 5;
        boolean b = this.f864c.m1182b(6);
        int i = 0;
        while (i < 4) {
            try {
                int f = this.f864c.m1188f(i);
                if (f > 0) {
                    this.f865d[i] = new ArrayList(f);
                }
                for (int i2 = 0; i2 < f; i2++) {
                    int i3 = c0994v.f1183b;
                    av a = av.m1145a(c0994v, i, z);
                    this.f865d[i].add(a);
                    if (a.m1160h() == 250) {
                        this.f862a = i3;
                    }
                    if (a.m1160h() == 24) {
                        ((aB) a).m1282d();
                    }
                }
                i++;
            } catch (bb e) {
                if (!b) {
                    throw e;
                }
            }
        }
        this.f866e = c0994v.f1183b;
    }

    public C0970X(byte[] bArr) {
        this(new C0994v(bArr));
    }

    public static C0970X m1247a(av avVar) {
        C0970X c0970x = new C0970X();
        c0970x.f864c.m1184c(0);
        c0970x.f864c.m1177a(7);
        c0970x.m1250a(avVar, 0);
        return c0970x;
    }

    private String m1248d(int i) {
        if (i > 3) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        av[] a = m1251a(i);
        for (av avVar : a) {
            if (i == 0) {
                stringBuffer.append(new StringBuffer(";;\t").append(avVar.f795a).toString());
                stringBuffer.append(new StringBuffer(", type = ").append(aT.m1361b(avVar.f796b)).toString());
                stringBuffer.append(new StringBuffer(", class = ").append(C0989q.m1660b(avVar.f797c)).toString());
            } else {
                stringBuffer.append(avVar);
            }
            stringBuffer.append("\n");
        }
        return stringBuffer.toString();
    }

    public final C0953G m1249a() {
        return this.f864c;
    }

    public final void m1250a(av avVar, int i) {
        if (this.f865d[i] == null) {
            this.f865d[i] = new LinkedList();
        }
        this.f864c.m1186d(i);
        this.f865d[i].add(avVar);
    }

    public final av[] m1251a(int i) {
        if (this.f865d[i] == null) {
            return f860f;
        }
        List list = this.f865d[i];
        return (av[]) list.toArray(new av[list.size()]);
    }

    public final av m1252b() {
        List list = this.f865d[0];
        return (list == null || list.size() == 0) ? null : (av) list.get(0);
    }

    public final as[] m1253b(int i) {
        if (this.f865d[i] == null) {
            return f861g;
        }
        List linkedList = new LinkedList();
        av[] a = m1251a(i);
        Set hashSet = new HashSet();
        int i2 = 0;
        while (i2 < a.length) {
            ai g = a[i2].m1159g();
            if (hashSet.contains(g)) {
                for (int size = linkedList.size() - 1; size >= 0; size--) {
                    as asVar = (as) linkedList.get(size);
                    if (asVar.m1451b() == a[i2].m1161i() && asVar.m1454e() == a[i2].m1162j() && asVar.m1453d().equals(g)) {
                        asVar.m1450a(a[i2]);
                        Object obj = null;
                        break;
                    }
                }
            }
            int i3 = 1;
            if (obj != null) {
                linkedList.add(new as(a[i2]));
                hashSet.add(g);
            }
            i2++;
        }
        return (as[]) linkedList.toArray(new as[linkedList.size()]);
    }

    public final aP m1254c() {
        int f = this.f864c.m1188f(3);
        if (f == 0) {
            return null;
        }
        av avVar = (av) this.f865d[3].get(f - 1);
        return avVar.f796b != 250 ? null : (aP) avVar;
    }

    public final byte[] m1255c(int i) {
        C0996x c0996x = new C0996x();
        int i2 = c0996x.f1187a;
        this.f864c.m1179a(c0996x);
        C0987o c0987o = new C0987o();
        for (int i3 = 0; i3 < 4; i3++) {
            if (this.f865d[i3] != null) {
                int size = this.f865d[i3].size();
                int i4 = c0996x.f1187a;
                int i5 = 0;
                int i6 = 0;
                av avVar = null;
                while (i6 < size) {
                    av avVar2 = (av) this.f865d[i3].get(i6);
                    if (avVar != null) {
                        Object obj = (avVar2.m1161i() == avVar.m1161i() && avVar2.m1162j() == avVar.m1162j() && avVar2.m1159g().equals(avVar.m1159g())) ? 1 : null;
                        if (obj == null) {
                            i4 = c0996x.f1187a;
                            i5 = i6;
                        }
                    }
                    avVar2.m1153a(c0996x, i3, c0987o);
                    if (c0996x.f1187a > 65535) {
                        c0996x.m1684a(i4);
                        i5 = size - i5;
                        break;
                    }
                    i6++;
                    avVar = avVar2;
                }
                i5 = 0;
                if (i5 != 0) {
                    C0953G c0953g = (C0953G) this.f864c.clone();
                    if (i3 != 3) {
                        c0953g.m1177a(6);
                    }
                    c0953g.m1178a(i3, c0953g.m1188f(i3) - i5);
                    for (i5 = i3 + 1; i5 < 4; i5++) {
                        c0953g.m1178a(i5, 0);
                    }
                    c0996x.m1683a();
                    c0996x.m1684a(i2);
                    c0953g.m1179a(c0996x);
                    c0996x.m1688b();
                    this.f866e = c0996x.f1187a;
                    return c0996x.m1692c();
                }
            }
        }
        this.f866e = c0996x.f1187a;
        return c0996x.m1692c();
    }

    public final Object clone() {
        C0970X c0970x = new C0970X();
        for (int i = 0; i < this.f865d.length; i++) {
            if (this.f865d[i] != null) {
                c0970x.f865d[i] = new LinkedList(this.f865d[i]);
            }
        }
        c0970x.f864c = (C0953G) this.f864c.clone();
        c0970x.f866e = this.f866e;
        return c0970x;
    }

    public final boolean m1256d() {
        return this.f863b == 1;
    }

    public final ak m1257e() {
        av[] a = m1251a(3);
        for (int i = 0; i < a.length; i++) {
            if (a[i] instanceof ak) {
                return (ak) a[i];
            }
        }
        return null;
    }

    public final int m1258f() {
        int c = this.f864c.m1183c();
        ak e = m1257e();
        return e != null ? c + (e.m1433e() << 4) : c;
    }

    public final byte[] m1259g() {
        C0996x c0996x = new C0996x();
        this.f864c.m1179a(c0996x);
        C0987o c0987o = new C0987o();
        for (int i = 0; i < 4; i++) {
            if (this.f865d[i] != null) {
                for (int i2 = 0; i2 < this.f865d[i].size(); i2++) {
                    ((av) this.f865d[i].get(i2)).m1153a(c0996x, i, c0987o);
                }
            }
        }
        this.f866e = c0996x.f1187a;
        return c0996x.m1692c();
    }

    public final String toString() {
        int i = 1;
        int i2 = 0;
        StringBuffer stringBuffer = new StringBuffer();
        if (m1257e() != null) {
            stringBuffer.append(new StringBuffer().append(this.f864c.m1189g(m1258f())).append("\n").toString());
        } else {
            stringBuffer.append(new StringBuffer().append(this.f864c).append("\n").toString());
        }
        if (!(this.f863b == 3 || this.f863b == 1 || this.f863b == 4)) {
            i = 0;
        }
        if (i != 0) {
            stringBuffer.append(";; TSIG ");
            if (m1256d()) {
                stringBuffer.append("ok");
            } else {
                stringBuffer.append("invalid");
            }
            stringBuffer.append('\n');
        }
        while (i2 < 4) {
            if (this.f864c.m1185d() != 5) {
                stringBuffer.append(new StringBuffer(";; ").append(aG.m1305b(i2)).append(":\n").toString());
            } else {
                stringBuffer.append(new StringBuffer(";; ").append(aG.m1306c(i2)).append(":\n").toString());
            }
            stringBuffer.append(new StringBuffer().append(m1248d(i2)).append("\n").toString());
            i2++;
        }
        stringBuffer.append(new StringBuffer(";; Message size: ").append(this.f866e).append(" bytes").toString());
        return stringBuffer.toString();
    }
}
