package com.kontagent.deps;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: com.kontagent.deps.d */
public final class C0977d extends av {
    private List f1151e;

    C0977d() {
    }

    static boolean m1617a(int i, int i2) {
        return C0977d.m1618b(i, i2);
    }

    private static boolean m1618b(int i, int i2) {
        return (i2 < 0 || i2 >= 256) ? false : (i != 1 || i2 <= 32) ? i != 2 || i2 <= 128 : false;
    }

    final av m1619a() {
        return new C0977d();
    }

    final void m1620a(C0994v c0994v) {
        this.f1151e = new ArrayList(1);
        while (c0994v.m1672a() != 0) {
            int c = c0994v.m1676c();
            int b = c0994v.m1675b();
            int b2 = c0994v.m1675b();
            boolean z = (b2 & 128) != 0;
            byte[] a = c0994v.m1674a(b2 & -129);
            if (C0977d.m1618b(c, b)) {
                Object obj;
                if (c == 1 || c == 2) {
                    if (c == 1) {
                        b2 = 4;
                    } else if (c == 2) {
                        b2 = 16;
                    } else {
                        throw new IllegalArgumentException("unknown address family");
                    }
                    if (a.length > b2) {
                        throw new bb("invalid address length");
                    }
                    if (a.length != b2) {
                        obj = new byte[b2];
                        System.arraycopy(a, 0, obj, 0, a.length);
                        Object obj2 = obj;
                    }
                    obj = new C0978f(z, InetAddress.getByAddress(a), b);
                } else {
                    obj = new C0978f(c, z, a, b, (byte) 0);
                }
                this.f1151e.add(obj);
            } else {
                throw new bb("invalid prefix length");
            }
        }
    }

    final void m1621a(C0996x c0996x, C0987o c0987o, boolean z) {
        for (C0978f c0978f : this.f1151e) {
            int i;
            byte[] bArr;
            if (c0978f.f1152a == 1 || c0978f.f1152a == 2) {
                int length;
                byte[] address = ((InetAddress) c0978f.f1155d).getAddress();
                for (length = address.length - 1; length >= 0; length--) {
                    if (address[length] != null) {
                        length++;
                        break;
                    }
                }
                length = 0;
                byte[] bArr2 = address;
                i = length;
                bArr = bArr2;
            } else {
                bArr = (byte[]) c0978f.f1155d;
                i = bArr.length;
            }
            int i2 = c0978f.f1153b ? i | 128 : i;
            c0996x.m1691c(c0978f.f1152a);
            c0996x.m1689b(c0978f.f1154c);
            c0996x.m1689b(i2);
            c0996x.m1687a(bArr, 0, i);
        }
    }

    final String m1622b() {
        StringBuffer stringBuffer = new StringBuffer();
        Iterator it = this.f1151e.iterator();
        while (it.hasNext()) {
            stringBuffer.append((C0978f) it.next());
            if (it.hasNext()) {
                stringBuffer.append(" ");
            }
        }
        return stringBuffer.toString();
    }
}
