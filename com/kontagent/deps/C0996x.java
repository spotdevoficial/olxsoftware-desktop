package com.kontagent.deps;

/* renamed from: com.kontagent.deps.x */
public final class C0996x {
    int f1187a;
    private byte[] f1188b;
    private int f1189c;

    public C0996x() {
        this(32);
    }

    private C0996x(int i) {
        this.f1188b = new byte[32];
        this.f1187a = 0;
        this.f1189c = -1;
    }

    private static void m1681a(long j, int i) {
        long j2 = 1 << i;
        if (j < 0 || j > j2) {
            throw new IllegalArgumentException(new StringBuffer().append(j).append(" out of range for ").append(i).append(" bit value").toString());
        }
    }

    private void m1682d(int i) {
        if (this.f1188b.length - this.f1187a < i) {
            int length = this.f1188b.length << 1;
            if (length < this.f1187a + i) {
                length = this.f1187a + i;
            }
            Object obj = new byte[length];
            System.arraycopy(this.f1188b, 0, obj, 0, this.f1187a);
            this.f1188b = obj;
        }
    }

    public final void m1683a() {
        this.f1189c = this.f1187a;
    }

    public final void m1684a(int i) {
        if (i > this.f1187a) {
            throw new IllegalArgumentException("cannot jump past end of data");
        }
        this.f1187a = i;
    }

    public final void m1685a(long j) {
        C0996x.m1681a(j, 32);
        m1682d(4);
        byte[] bArr = this.f1188b;
        int i = this.f1187a;
        this.f1187a = i + 1;
        bArr[i] = (byte) ((int) ((j >>> 24) & 255));
        bArr = this.f1188b;
        i = this.f1187a;
        this.f1187a = i + 1;
        bArr[i] = (byte) ((int) ((j >>> 16) & 255));
        bArr = this.f1188b;
        i = this.f1187a;
        this.f1187a = i + 1;
        bArr[i] = (byte) ((int) ((j >>> 8) & 255));
        bArr = this.f1188b;
        i = this.f1187a;
        this.f1187a = i + 1;
        bArr[i] = (byte) ((int) (j & 255));
    }

    public final void m1686a(byte[] bArr) {
        m1687a(bArr, 0, bArr.length);
    }

    public final void m1687a(byte[] bArr, int i, int i2) {
        m1682d(i2);
        System.arraycopy(bArr, i, this.f1188b, this.f1187a, i2);
        this.f1187a += i2;
    }

    public final void m1688b() {
        if (this.f1189c < 0) {
            throw new IllegalStateException("no previous state");
        }
        this.f1187a = this.f1189c;
        this.f1189c = -1;
    }

    public final void m1689b(int i) {
        C0996x.m1681a((long) i, 8);
        m1682d(1);
        byte[] bArr = this.f1188b;
        int i2 = this.f1187a;
        this.f1187a = i2 + 1;
        bArr[i2] = (byte) i;
    }

    public final void m1690b(byte[] bArr) {
        if (bArr.length > 255) {
            throw new IllegalArgumentException("Invalid counted string");
        }
        m1682d(bArr.length + 1);
        byte[] bArr2 = this.f1188b;
        int i = this.f1187a;
        this.f1187a = i + 1;
        bArr2[i] = (byte) bArr.length;
        m1687a(bArr, 0, bArr.length);
    }

    public final void m1691c(int i) {
        C0996x.m1681a((long) i, 16);
        m1682d(2);
        byte[] bArr = this.f1188b;
        int i2 = this.f1187a;
        this.f1187a = i2 + 1;
        bArr[i2] = (byte) (i >>> 8);
        bArr = this.f1188b;
        i2 = this.f1187a;
        this.f1187a = i2 + 1;
        bArr[i2] = (byte) i;
    }

    public final byte[] m1692c() {
        Object obj = new byte[this.f1187a];
        System.arraycopy(this.f1188b, 0, obj, 0, this.f1187a);
        return obj;
    }
}
