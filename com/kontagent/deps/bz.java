package com.kontagent.deps;

public final class bz extends RuntimeException {
    public bz(String str, Throwable th) {
        super(str, th);
    }

    public bz(Throwable th) {
        super(th.getMessage(), th);
    }
}
