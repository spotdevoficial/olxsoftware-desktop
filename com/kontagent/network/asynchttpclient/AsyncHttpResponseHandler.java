package com.kontagent.network.asynchttpclient;

import android.os.Handler;
import android.os.Looper;
import com.kontagent.queue.Message;
import com.urbanairship.C1608R;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.util.EntityUtils;

public class AsyncHttpResponseHandler {
    private Handler handler;
    private Message message;

    /* renamed from: com.kontagent.network.asynchttpclient.AsyncHttpResponseHandler.1 */
    class C10031 extends Handler {
        C10031() {
        }

        public void handleMessage(android.os.Message message) {
            AsyncHttpResponseHandler.this.handleMessage(message);
        }
    }

    public AsyncHttpResponseHandler(Message message) {
        if (Looper.myLooper() != null) {
            this.message = message;
            this.handler = new C10031();
        }
    }

    protected void handleFailureMessage(Throwable th, String str) {
        onFailure(th, str);
        onMessageFailure(this.message);
    }

    protected void handleMessage(android.os.Message message) {
        switch (message.what) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                handleSuccessMessage((String) message.obj);
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                Object[] objArr = (Object[]) message.obj;
                handleFailureMessage((Throwable) objArr[0], (String) objArr[1]);
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                onStart();
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                onFinish();
            default:
        }
    }

    protected void handleSuccessMessage(String str) {
        onSuccess(str);
        onMessageSuccess(this.message);
    }

    protected android.os.Message obtainMessage(int i, Object obj) {
        if (this.handler != null) {
            return this.handler.obtainMessage(i, obj);
        }
        android.os.Message message = new android.os.Message();
        message.what = i;
        message.obj = obj;
        return message;
    }

    public void onFailure(Throwable th) {
    }

    public void onFailure(Throwable th, String str) {
        onFailure(th);
    }

    public void onFinish() {
    }

    public void onMessageFailure(Message message) {
    }

    public void onMessageSuccess(Message message) {
    }

    public void onStart() {
    }

    public void onSuccess(String str) {
    }

    protected void sendFailureMessage(Throwable th, String str) {
        sendMessage(obtainMessage(1, new Object[]{th, str}));
    }

    protected void sendFinishMessage() {
        sendMessage(obtainMessage(3, null));
    }

    protected void sendMessage(android.os.Message message) {
        if (this.handler != null) {
            this.handler.sendMessage(message);
        } else {
            handleMessage(message);
        }
    }

    void sendResponseMessage(HttpResponse httpResponse) {
        String str = null;
        StatusLine statusLine = httpResponse.getStatusLine();
        try {
            HttpEntity entity = httpResponse.getEntity();
            str = EntityUtils.toString(entity != null ? new BufferedHttpEntity(entity) : null);
        } catch (Throwable e) {
            sendFailureMessage(e, null);
        }
        if (statusLine.getStatusCode() >= 300) {
            sendFailureMessage(new HttpResponseException(statusLine.getStatusCode(), statusLine.getReasonPhrase()), str);
        } else {
            sendSuccessMessage(str);
        }
    }

    protected void sendStartMessage() {
        sendMessage(obtainMessage(2, null));
    }

    protected void sendSuccessMessage(String str) {
        sendMessage(obtainMessage(0, str));
    }
}
