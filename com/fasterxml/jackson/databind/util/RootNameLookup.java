package com.fasterxml.jackson.databind.util;

import com.fasterxml.jackson.core.io.SerializedString;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.type.ClassKey;
import java.io.Serializable;

public class RootNameLookup implements Serializable {
    protected transient LRUMap<ClassKey, SerializedString> _rootNames;

    public SerializedString findRootName(JavaType javaType, MapperConfig<?> mapperConfig) {
        return findRootName(javaType.getRawClass(), (MapperConfig) mapperConfig);
    }

    public SerializedString findRootName(Class<?> cls, MapperConfig<?> mapperConfig) {
        ClassKey classKey = new ClassKey(cls);
        synchronized (this) {
            String simpleName;
            if (this._rootNames == null) {
                this._rootNames = new LRUMap(20, 200);
            } else {
                SerializedString serializedString = (SerializedString) this._rootNames.get(classKey);
                if (serializedString != null) {
                    return serializedString;
                }
            }
            PropertyName findRootName = mapperConfig.getAnnotationIntrospector().findRootName(mapperConfig.introspectClassAnnotations((Class) cls).getClassInfo());
            if (findRootName == null || !findRootName.hasSimpleName()) {
                simpleName = cls.getSimpleName();
            } else {
                simpleName = findRootName.getSimpleName();
            }
            SerializedString serializedString2 = new SerializedString(simpleName);
            synchronized (this) {
                this._rootNames.put(classKey, serializedString2);
            }
            return serializedString2;
        }
    }
}
