package com.adobe.xmp.properties;

public interface XMPPropertyInfo {
    String getPath();

    Object getValue();
}
