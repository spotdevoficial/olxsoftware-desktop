package com.adobe.xmp.impl;

import com.adobe.xmp.XMPDateTime;
import com.adobe.xmp.XMPException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public final class ISO8601Converter {
    public static XMPDateTime parse(String str) throws XMPException {
        return parse(str, new XMPDateTimeImpl());
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.adobe.xmp.XMPDateTime parse(java.lang.String r11, com.adobe.xmp.XMPDateTime r12) throws com.adobe.xmp.XMPException {
        /*
        r1 = 1;
        r9 = 58;
        r2 = 0;
        r8 = 45;
        r7 = 5;
        com.adobe.xmp.impl.ParameterAsserts.assertNotNull(r11);
        r4 = new com.adobe.xmp.impl.ParseState;
        r4.<init>(r11);
        r0 = r4.ch(r2);
        r3 = 84;
        if (r0 == r3) goto L_0x0032;
    L_0x0017:
        r0 = r4.length();
        r3 = 2;
        if (r0 < r3) goto L_0x0024;
    L_0x001e:
        r0 = r4.ch(r1);
        if (r0 == r9) goto L_0x0032;
    L_0x0024:
        r0 = r4.length();
        r3 = 3;
        if (r0 < r3) goto L_0x005a;
    L_0x002b:
        r0 = 2;
        r0 = r4.ch(r0);
        if (r0 != r9) goto L_0x005a;
    L_0x0032:
        r3 = r1;
    L_0x0033:
        if (r3 != 0) goto L_0x00e0;
    L_0x0035:
        r0 = r4.ch(r2);
        if (r0 != r8) goto L_0x003e;
    L_0x003b:
        r4.skip();
    L_0x003e:
        r0 = "Invalid year in date string";
        r5 = 9999; // 0x270f float:1.4012E-41 double:4.94E-320;
        r0 = r4.gatherInt(r0, r5);
        r5 = r4.hasNext();
        if (r5 == 0) goto L_0x005c;
    L_0x004c:
        r5 = r4.ch();
        if (r5 == r8) goto L_0x005c;
    L_0x0052:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Invalid date string, after year";
        r0.<init>(r1, r7);
        throw r0;
    L_0x005a:
        r3 = r2;
        goto L_0x0033;
    L_0x005c:
        r5 = r4.ch(r2);
        if (r5 != r8) goto L_0x0063;
    L_0x0062:
        r0 = -r0;
    L_0x0063:
        r12.setYear(r0);
        r0 = r4.hasNext();
        if (r0 != 0) goto L_0x006d;
    L_0x006c:
        return r12;
    L_0x006d:
        r4.skip();
        r0 = "Invalid month in date string";
        r5 = 12;
        r0 = r4.gatherInt(r0, r5);
        r5 = r4.hasNext();
        if (r5 == 0) goto L_0x008c;
    L_0x007e:
        r5 = r4.ch();
        if (r5 == r8) goto L_0x008c;
    L_0x0084:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Invalid date string, after month";
        r0.<init>(r1, r7);
        throw r0;
    L_0x008c:
        r12.setMonth(r0);
        r0 = r4.hasNext();
        if (r0 == 0) goto L_0x006c;
    L_0x0095:
        r4.skip();
        r0 = "Invalid day in date string";
        r5 = 31;
        r0 = r4.gatherInt(r0, r5);
        r5 = r4.hasNext();
        if (r5 == 0) goto L_0x00b6;
    L_0x00a6:
        r5 = r4.ch();
        r6 = 84;
        if (r5 == r6) goto L_0x00b6;
    L_0x00ae:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Invalid date string, after day";
        r0.<init>(r1, r7);
        throw r0;
    L_0x00b6:
        r12.setDay(r0);
        r0 = r4.hasNext();
        if (r0 == 0) goto L_0x006c;
    L_0x00bf:
        r0 = r4.ch();
        r5 = 84;
        if (r0 != r5) goto L_0x00e7;
    L_0x00c7:
        r4.skip();
    L_0x00ca:
        r0 = "Invalid hour in date string";
        r3 = 23;
        r0 = r4.gatherInt(r0, r3);
        r3 = r4.ch();
        if (r3 == r9) goto L_0x00f1;
    L_0x00d8:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Invalid date string, after hour";
        r0.<init>(r1, r7);
        throw r0;
    L_0x00e0:
        r12.setMonth(r1);
        r12.setDay(r1);
        goto L_0x00bf;
    L_0x00e7:
        if (r3 != 0) goto L_0x00ca;
    L_0x00e9:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Invalid date string, missing 'T' after date";
        r0.<init>(r1, r7);
        throw r0;
    L_0x00f1:
        r12.setHour(r0);
        r4.skip();
        r0 = "Invalid minute in date string";
        r3 = 59;
        r0 = r4.gatherInt(r0, r3);
        r3 = r4.hasNext();
        if (r3 == 0) goto L_0x0129;
    L_0x0105:
        r3 = r4.ch();
        if (r3 == r9) goto L_0x0129;
    L_0x010b:
        r3 = r4.ch();
        r5 = 90;
        if (r3 == r5) goto L_0x0129;
    L_0x0113:
        r3 = r4.ch();
        r5 = 43;
        if (r3 == r5) goto L_0x0129;
    L_0x011b:
        r3 = r4.ch();
        if (r3 == r8) goto L_0x0129;
    L_0x0121:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Invalid date string, after minute";
        r0.<init>(r1, r7);
        throw r0;
    L_0x0129:
        r12.setMinute(r0);
        r0 = r4.ch();
        if (r0 != r9) goto L_0x01bd;
    L_0x0132:
        r4.skip();
        r0 = "Invalid whole seconds in date string";
        r3 = 59;
        r0 = r4.gatherInt(r0, r3);
        r3 = r4.hasNext();
        if (r3 == 0) goto L_0x0169;
    L_0x0143:
        r3 = r4.ch();
        r5 = 46;
        if (r3 == r5) goto L_0x0169;
    L_0x014b:
        r3 = r4.ch();
        r5 = 90;
        if (r3 == r5) goto L_0x0169;
    L_0x0153:
        r3 = r4.ch();
        r5 = 43;
        if (r3 == r5) goto L_0x0169;
    L_0x015b:
        r3 = r4.ch();
        if (r3 == r8) goto L_0x0169;
    L_0x0161:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Invalid date string, after whole seconds";
        r0.<init>(r1, r7);
        throw r0;
    L_0x0169:
        r12.setSecond(r0);
        r0 = r4.ch();
        r3 = 46;
        if (r0 != r3) goto L_0x01bd;
    L_0x0174:
        r4.skip();
        r0 = r4.pos();
        r3 = "Invalid fractional seconds in date string";
        r5 = 999999999; // 0x3b9ac9ff float:0.004723787 double:4.940656453E-315;
        r3 = r4.gatherInt(r3, r5);
        r5 = r4.ch();
        r6 = 90;
        if (r5 == r6) goto L_0x01a2;
    L_0x018c:
        r5 = r4.ch();
        r6 = 43;
        if (r5 == r6) goto L_0x01a2;
    L_0x0194:
        r5 = r4.ch();
        if (r5 == r8) goto L_0x01a2;
    L_0x019a:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Invalid date string, after fractional second";
        r0.<init>(r1, r7);
        throw r0;
    L_0x01a2:
        r5 = r4.pos();
        r0 = r5 - r0;
    L_0x01a8:
        r5 = 9;
        if (r0 <= r5) goto L_0x01b1;
    L_0x01ac:
        r3 = r3 / 10;
        r0 = r0 + -1;
        goto L_0x01a8;
    L_0x01b1:
        r5 = 9;
        if (r0 >= r5) goto L_0x01ba;
    L_0x01b5:
        r3 = r3 * 10;
        r0 = r0 + 1;
        goto L_0x01b1;
    L_0x01ba:
        r12.setNanoSecond(r3);
    L_0x01bd:
        r0 = r4.ch();
        r3 = 90;
        if (r0 != r3) goto L_0x01ec;
    L_0x01c5:
        r4.skip();
        r0 = r2;
        r1 = r2;
    L_0x01ca:
        r2 = r2 * 3600;
        r2 = r2 * 1000;
        r0 = r0 * 60;
        r0 = r0 * 1000;
        r0 = r0 + r2;
        r0 = r0 * r1;
        r1 = new java.util.SimpleTimeZone;
        r2 = "";
        r1.<init>(r0, r2);
        r12.setTimeZone(r1);
        r0 = r4.hasNext();
        if (r0 == 0) goto L_0x006c;
    L_0x01e4:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Invalid date string, extra chars at end";
        r0.<init>(r1, r7);
        throw r0;
    L_0x01ec:
        r0 = r4.hasNext();
        if (r0 == 0) goto L_0x0233;
    L_0x01f2:
        r0 = r4.ch();
        r2 = 43;
        if (r0 != r2) goto L_0x0214;
    L_0x01fa:
        r0 = r1;
    L_0x01fb:
        r4.skip();
        r1 = "Invalid time zone hour in date string";
        r2 = 23;
        r2 = r4.gatherInt(r1, r2);
        r1 = r4.ch();
        if (r1 == r9) goto L_0x0224;
    L_0x020c:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Invalid date string, after time zone hour";
        r0.<init>(r1, r7);
        throw r0;
    L_0x0214:
        r0 = r4.ch();
        if (r0 != r8) goto L_0x021c;
    L_0x021a:
        r0 = -1;
        goto L_0x01fb;
    L_0x021c:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Time zone must begin with 'Z', '+', or '-'";
        r0.<init>(r1, r7);
        throw r0;
    L_0x0224:
        r4.skip();
        r1 = "Invalid time zone minute in date string";
        r3 = 59;
        r1 = r4.gatherInt(r1, r3);
        r10 = r1;
        r1 = r0;
        r0 = r10;
        goto L_0x01ca;
    L_0x0233:
        r0 = r2;
        r1 = r2;
        goto L_0x01ca;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adobe.xmp.impl.ISO8601Converter.parse(java.lang.String, com.adobe.xmp.XMPDateTime):com.adobe.xmp.XMPDateTime");
    }

    public static String render(XMPDateTime xMPDateTime) {
        StringBuffer stringBuffer = new StringBuffer();
        DecimalFormat decimalFormat = new DecimalFormat("0000", new DecimalFormatSymbols(Locale.ENGLISH));
        stringBuffer.append(decimalFormat.format((long) xMPDateTime.getYear()));
        if (xMPDateTime.getMonth() == 0) {
            return stringBuffer.toString();
        }
        decimalFormat.applyPattern("'-'00");
        stringBuffer.append(decimalFormat.format((long) xMPDateTime.getMonth()));
        if (xMPDateTime.getDay() == 0) {
            return stringBuffer.toString();
        }
        stringBuffer.append(decimalFormat.format((long) xMPDateTime.getDay()));
        if (!(xMPDateTime.getHour() == 0 && xMPDateTime.getMinute() == 0 && xMPDateTime.getSecond() == 0 && xMPDateTime.getNanoSecond() == 0 && (xMPDateTime.getTimeZone() == null || xMPDateTime.getTimeZone().getRawOffset() == 0))) {
            stringBuffer.append('T');
            decimalFormat.applyPattern("00");
            stringBuffer.append(decimalFormat.format((long) xMPDateTime.getHour()));
            stringBuffer.append(':');
            stringBuffer.append(decimalFormat.format((long) xMPDateTime.getMinute()));
            if (!(xMPDateTime.getSecond() == 0 && xMPDateTime.getNanoSecond() == 0)) {
                double second = ((double) xMPDateTime.getSecond()) + (((double) xMPDateTime.getNanoSecond()) / 1.0E9d);
                decimalFormat.applyPattern(":00.#########");
                stringBuffer.append(decimalFormat.format(second));
            }
            if (xMPDateTime.getTimeZone() != null) {
                int offset = xMPDateTime.getTimeZone().getOffset(xMPDateTime.getCalendar().getTimeInMillis());
                if (offset == 0) {
                    stringBuffer.append('Z');
                } else {
                    int i = offset / 3600000;
                    offset = Math.abs((offset % 3600000) / 60000);
                    decimalFormat.applyPattern("+00;-00");
                    stringBuffer.append(decimalFormat.format((long) i));
                    decimalFormat.applyPattern(":00");
                    stringBuffer.append(decimalFormat.format((long) offset));
                }
            }
        }
        return stringBuffer.toString();
    }
}
