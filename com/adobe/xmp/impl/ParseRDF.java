package com.adobe.xmp.impl;

import com.adobe.xmp.XMPException;
import com.adobe.xmp.XMPMetaFactory;
import com.adobe.xmp.XMPSchemaRegistry;
import com.adobe.xmp.options.PropertyOptions;
import com.facebook.BuildConfig;
import com.urbanairship.C1608R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class ParseRDF {
    static final /* synthetic */ boolean $assertionsDisabled;

    static {
        $assertionsDisabled = !ParseRDF.class.desiredAssertionStatus();
    }

    private static XMPNode addChildNode(XMPMetaImpl xMPMetaImpl, XMPNode xMPNode, Node node, String str, boolean z) throws XMPException {
        XMPSchemaRegistry schemaRegistry = XMPMetaFactory.getSchemaRegistry();
        String namespaceURI = node.getNamespaceURI();
        if (namespaceURI != null) {
            boolean z2;
            boolean equals;
            boolean equals2;
            XMPNode xMPNode2;
            if ("http://purl.org/dc/1.1/".equals(namespaceURI)) {
                namespaceURI = "http://purl.org/dc/elements/1.1/";
            }
            String namespacePrefix = schemaRegistry.getNamespacePrefix(namespaceURI);
            if (namespacePrefix == null) {
                namespacePrefix = schemaRegistry.registerNamespace(namespaceURI, node.getPrefix() != null ? node.getPrefix() : "_dflt");
            }
            namespacePrefix = namespacePrefix + node.getLocalName();
            PropertyOptions propertyOptions = new PropertyOptions();
            if (z) {
                xMPNode = XMPNodeUtils.findSchemaNode(xMPMetaImpl.getRoot(), namespaceURI, "_dflt", true);
                xMPNode.setImplicit(false);
                if (schemaRegistry.findAlias(namespacePrefix) != null) {
                    xMPMetaImpl.getRoot().setHasAliases(true);
                    xMPNode.setHasAliases(true);
                    z2 = true;
                    equals = "rdf:li".equals(namespacePrefix);
                    equals2 = "rdf:value".equals(namespacePrefix);
                    xMPNode2 = new XMPNode(namespacePrefix, str, propertyOptions);
                    xMPNode2.setAlias(z2);
                    if (equals2) {
                        xMPNode.addChild(xMPNode2);
                    } else {
                        xMPNode.addChild(1, xMPNode2);
                    }
                    if (equals2) {
                        if (z && xMPNode.getOptions().isStruct()) {
                            xMPNode.setHasValueChild(true);
                        } else {
                            throw new XMPException("Misplaced rdf:value element", 202);
                        }
                    }
                    if (equals) {
                        if (xMPNode.getOptions().isArray()) {
                            throw new XMPException("Misplaced rdf:li element", 202);
                        }
                        xMPNode2.setName("[]");
                    }
                    return xMPNode2;
                }
            }
            z2 = false;
            equals = "rdf:li".equals(namespacePrefix);
            equals2 = "rdf:value".equals(namespacePrefix);
            xMPNode2 = new XMPNode(namespacePrefix, str, propertyOptions);
            xMPNode2.setAlias(z2);
            if (equals2) {
                xMPNode.addChild(1, xMPNode2);
            } else {
                xMPNode.addChild(xMPNode2);
            }
            if (equals2) {
                if (z) {
                }
                throw new XMPException("Misplaced rdf:value element", 202);
            }
            if (equals) {
                if (xMPNode.getOptions().isArray()) {
                    xMPNode2.setName("[]");
                } else {
                    throw new XMPException("Misplaced rdf:li element", 202);
                }
            }
            return xMPNode2;
        }
        throw new XMPException("XML namespace required for all elements and attributes", 202);
    }

    private static XMPNode addQualifierNode(XMPNode xMPNode, String str, String str2) throws XMPException {
        if ("xml:lang".equals(str)) {
            str2 = Utils.normalizeLangValue(str2);
        }
        XMPNode xMPNode2 = new XMPNode(str, str2, null);
        xMPNode.addQualifier(xMPNode2);
        return xMPNode2;
    }

    private static void fixupQualifiedNode(XMPNode xMPNode) throws XMPException {
        int i = 1;
        if ($assertionsDisabled || (xMPNode.getOptions().isStruct() && xMPNode.hasChildren())) {
            XMPNode child = xMPNode.getChild(1);
            if ($assertionsDisabled || "rdf:value".equals(child.getName())) {
                if (child.getOptions().getHasLanguage()) {
                    if (xMPNode.getOptions().getHasLanguage()) {
                        throw new XMPException("Redundant xml:lang for rdf:value element", 203);
                    }
                    XMPNode qualifier = child.getQualifier(1);
                    child.removeQualifier(qualifier);
                    xMPNode.addQualifier(qualifier);
                }
                while (i <= child.getQualifierLength()) {
                    xMPNode.addQualifier(child.getQualifier(i));
                    i++;
                }
                for (i = 2; i <= xMPNode.getChildrenLength(); i++) {
                    xMPNode.addQualifier(xMPNode.getChild(i));
                }
                if ($assertionsDisabled || xMPNode.getOptions().isStruct() || xMPNode.getHasValueChild()) {
                    xMPNode.setHasValueChild(false);
                    xMPNode.getOptions().setStruct(false);
                    xMPNode.getOptions().mergeWith(child.getOptions());
                    xMPNode.setValue(child.getValue());
                    xMPNode.removeChildren();
                    Iterator iterateChildren = child.iterateChildren();
                    while (iterateChildren.hasNext()) {
                        xMPNode.addChild((XMPNode) iterateChildren.next());
                    }
                    return;
                }
                throw new AssertionError();
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    private static int getRDFTermKind(Node node) {
        String localName = node.getLocalName();
        Object namespaceURI = node.getNamespaceURI();
        if (namespaceURI == null && (("about".equals(localName) || "ID".equals(localName)) && (node instanceof Attr) && "http://www.w3.org/1999/02/22-rdf-syntax-ns#".equals(((Attr) node).getOwnerElement().getNamespaceURI()))) {
            namespaceURI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
        }
        if ("http://www.w3.org/1999/02/22-rdf-syntax-ns#".equals(namespaceURI)) {
            if ("li".equals(localName)) {
                return 9;
            }
            if ("parseType".equals(localName)) {
                return 4;
            }
            if ("Description".equals(localName)) {
                return 8;
            }
            if ("about".equals(localName)) {
                return 3;
            }
            if ("resource".equals(localName)) {
                return 5;
            }
            if ("RDF".equals(localName)) {
                return 1;
            }
            if ("ID".equals(localName)) {
                return 2;
            }
            if ("nodeID".equals(localName)) {
                return 6;
            }
            if ("datatype".equals(localName)) {
                return 7;
            }
            if ("aboutEach".equals(localName)) {
                return 10;
            }
            if ("aboutEachPrefix".equals(localName)) {
                return 11;
            }
            if ("bagID".equals(localName)) {
                return 12;
            }
        }
        return 0;
    }

    private static boolean isCoreSyntaxTerm(int i) {
        return 1 <= i && i <= 7;
    }

    private static boolean isOldTerm(int i) {
        return 10 <= i && i <= 12;
    }

    private static boolean isPropertyElementName(int i) {
        return (i == 8 || isOldTerm(i) || isCoreSyntaxTerm(i)) ? false : true;
    }

    private static boolean isWhitespaceNode(Node node) {
        if (node.getNodeType() != (short) 3) {
            return false;
        }
        String nodeValue = node.getNodeValue();
        for (int i = 0; i < nodeValue.length(); i++) {
            if (!Character.isWhitespace(nodeValue.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    static XMPMetaImpl parse(Node node) throws XMPException {
        XMPMetaImpl xMPMetaImpl = new XMPMetaImpl();
        rdf_RDF(xMPMetaImpl, node);
        return xMPMetaImpl;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void rdf_EmptyPropertyElement(com.adobe.xmp.impl.XMPMetaImpl r10, com.adobe.xmp.impl.XMPNode r11, org.w3c.dom.Node r12, boolean r13) throws com.adobe.xmp.XMPException {
        /*
        r5 = 0;
        r4 = 0;
        r3 = 0;
        r2 = 0;
        r1 = 0;
        r0 = r12.hasChildNodes();
        if (r0 == 0) goto L_0x0015;
    L_0x000b:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Nested content not allowed with rdf:resource or property attributes";
        r2 = 202; // 0xca float:2.83E-43 double:1.0E-321;
        r0.<init>(r1, r2);
        throw r0;
    L_0x0015:
        r0 = 0;
        r6 = r0;
        r0 = r1;
    L_0x0018:
        r1 = r12.getAttributes();
        r1 = r1.getLength();
        if (r6 >= r1) goto L_0x00de;
    L_0x0022:
        r1 = r12.getAttributes();
        r1 = r1.item(r6);
        r7 = "xmlns";
        r8 = r1.getPrefix();
        r7 = r7.equals(r8);
        if (r7 != 0) goto L_0x018e;
    L_0x0036:
        r7 = r1.getPrefix();
        if (r7 != 0) goto L_0x0054;
    L_0x003c:
        r7 = "xmlns";
        r8 = r1.getNodeName();
        r7 = r7.equals(r8);
        if (r7 == 0) goto L_0x0054;
    L_0x0048:
        r1 = r2;
        r2 = r3;
        r3 = r4;
        r4 = r5;
    L_0x004c:
        r5 = r6 + 1;
        r6 = r5;
        r5 = r4;
        r4 = r3;
        r3 = r2;
        r2 = r1;
        goto L_0x0018;
    L_0x0054:
        r7 = getRDFTermKind(r1);
        switch(r7) {
            case 0: goto L_0x009e;
            case 1: goto L_0x005b;
            case 2: goto L_0x0065;
            case 3: goto L_0x005b;
            case 4: goto L_0x005b;
            case 5: goto L_0x006a;
            case 6: goto L_0x008b;
            default: goto L_0x005b;
        };
    L_0x005b:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Unrecognized attribute of empty property element";
        r2 = 202; // 0xca float:2.83E-43 double:1.0E-321;
        r0.<init>(r1, r2);
        throw r0;
    L_0x0065:
        r1 = r2;
        r2 = r3;
        r3 = r4;
        r4 = r5;
        goto L_0x004c;
    L_0x006a:
        if (r3 == 0) goto L_0x0076;
    L_0x006c:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Empty property element can't have both rdf:resource and rdf:nodeID";
        r2 = 202; // 0xca float:2.83E-43 double:1.0E-321;
        r0.<init>(r1, r2);
        throw r0;
    L_0x0076:
        if (r2 == 0) goto L_0x0082;
    L_0x0078:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Empty property element can't have both rdf:value and rdf:resource";
        r2 = 203; // 0xcb float:2.84E-43 double:1.003E-321;
        r0.<init>(r1, r2);
        throw r0;
    L_0x0082:
        r4 = 1;
        if (r2 != 0) goto L_0x018e;
    L_0x0085:
        r0 = r1;
        r1 = r2;
        r2 = r3;
        r3 = r4;
        r4 = r5;
        goto L_0x004c;
    L_0x008b:
        if (r4 == 0) goto L_0x0097;
    L_0x008d:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Empty property element can't have both rdf:resource and rdf:nodeID";
        r2 = 202; // 0xca float:2.83E-43 double:1.0E-321;
        r0.<init>(r1, r2);
        throw r0;
    L_0x0097:
        r1 = 1;
        r3 = r4;
        r4 = r5;
        r9 = r2;
        r2 = r1;
        r1 = r9;
        goto L_0x004c;
    L_0x009e:
        r7 = "value";
        r8 = r1.getLocalName();
        r7 = r7.equals(r8);
        if (r7 == 0) goto L_0x00ca;
    L_0x00aa:
        r7 = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
        r8 = r1.getNamespaceURI();
        r7 = r7.equals(r8);
        if (r7 == 0) goto L_0x00ca;
    L_0x00b6:
        if (r4 == 0) goto L_0x00c2;
    L_0x00b8:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Empty property element can't have both rdf:value and rdf:resource";
        r2 = 203; // 0xcb float:2.84E-43 double:1.003E-321;
        r0.<init>(r1, r2);
        throw r0;
    L_0x00c2:
        r0 = 1;
        r2 = r3;
        r3 = r4;
        r4 = r5;
        r9 = r0;
        r0 = r1;
        r1 = r9;
        goto L_0x004c;
    L_0x00ca:
        r7 = "xml:lang";
        r1 = r1.getNodeName();
        r1 = r7.equals(r1);
        if (r1 != 0) goto L_0x018e;
    L_0x00d6:
        r1 = 1;
        r9 = r2;
        r2 = r3;
        r3 = r4;
        r4 = r1;
        r1 = r9;
        goto L_0x004c;
    L_0x00de:
        r1 = "";
        r6 = addChildNode(r10, r11, r12, r1, r13);
        r3 = 0;
        if (r2 != 0) goto L_0x00e9;
    L_0x00e7:
        if (r4 == 0) goto L_0x0136;
    L_0x00e9:
        if (r0 == 0) goto L_0x0133;
    L_0x00eb:
        r1 = r0.getNodeValue();
    L_0x00ef:
        r6.setValue(r1);
        if (r2 != 0) goto L_0x018b;
    L_0x00f4:
        r1 = r6.getOptions();
        r2 = 1;
        r1.setURI(r2);
        r1 = r3;
    L_0x00fd:
        r2 = 0;
    L_0x00fe:
        r3 = r12.getAttributes();
        r3 = r3.getLength();
        if (r2 >= r3) goto L_0x018a;
    L_0x0108:
        r3 = r12.getAttributes();
        r3 = r3.item(r2);
        if (r3 == r0) goto L_0x0130;
    L_0x0112:
        r4 = "xmlns";
        r5 = r3.getPrefix();
        r4 = r4.equals(r5);
        if (r4 != 0) goto L_0x0130;
    L_0x011e:
        r4 = r3.getPrefix();
        if (r4 != 0) goto L_0x0142;
    L_0x0124:
        r4 = "xmlns";
        r5 = r3.getNodeName();
        r4 = r4.equals(r5);
        if (r4 == 0) goto L_0x0142;
    L_0x0130:
        r2 = r2 + 1;
        goto L_0x00fe;
    L_0x0133:
        r1 = "";
        goto L_0x00ef;
    L_0x0136:
        if (r5 == 0) goto L_0x018b;
    L_0x0138:
        r1 = r6.getOptions();
        r2 = 1;
        r1.setStruct(r2);
        r1 = 1;
        goto L_0x00fd;
    L_0x0142:
        r4 = getRDFTermKind(r3);
        switch(r4) {
            case 0: goto L_0x015d;
            case 1: goto L_0x0149;
            case 2: goto L_0x0130;
            case 3: goto L_0x0149;
            case 4: goto L_0x0149;
            case 5: goto L_0x0153;
            case 6: goto L_0x0130;
            default: goto L_0x0149;
        };
    L_0x0149:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Unrecognized attribute of empty property element";
        r2 = 202; // 0xca float:2.83E-43 double:1.0E-321;
        r0.<init>(r1, r2);
        throw r0;
    L_0x0153:
        r4 = "rdf:resource";
        r3 = r3.getNodeValue();
        addQualifierNode(r6, r4, r3);
        goto L_0x0130;
    L_0x015d:
        if (r1 != 0) goto L_0x016b;
    L_0x015f:
        r4 = r3.getNodeName();
        r3 = r3.getNodeValue();
        addQualifierNode(r6, r4, r3);
        goto L_0x0130;
    L_0x016b:
        r4 = "xml:lang";
        r5 = r3.getNodeName();
        r4 = r4.equals(r5);
        if (r4 == 0) goto L_0x0181;
    L_0x0177:
        r4 = "xml:lang";
        r3 = r3.getNodeValue();
        addQualifierNode(r6, r4, r3);
        goto L_0x0130;
    L_0x0181:
        r4 = r3.getNodeValue();
        r5 = 0;
        addChildNode(r10, r6, r3, r4, r5);
        goto L_0x0130;
    L_0x018a:
        return;
    L_0x018b:
        r1 = r3;
        goto L_0x00fd;
    L_0x018e:
        r1 = r2;
        r2 = r3;
        r3 = r4;
        r4 = r5;
        goto L_0x004c;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adobe.xmp.impl.ParseRDF.rdf_EmptyPropertyElement(com.adobe.xmp.impl.XMPMetaImpl, com.adobe.xmp.impl.XMPNode, org.w3c.dom.Node, boolean):void");
    }

    private static void rdf_LiteralPropertyElement(XMPMetaImpl xMPMetaImpl, XMPNode xMPNode, Node node, boolean z) throws XMPException {
        int i = 0;
        XMPNode addChildNode = addChildNode(xMPMetaImpl, xMPNode, node, null, z);
        for (int i2 = 0; i2 < node.getAttributes().getLength(); i2++) {
            Node item = node.getAttributes().item(i2);
            if (!("xmlns".equals(item.getPrefix()) || (item.getPrefix() == null && "xmlns".equals(item.getNodeName())))) {
                String namespaceURI = item.getNamespaceURI();
                String localName = item.getLocalName();
                if ("xml:lang".equals(item.getNodeName())) {
                    addQualifierNode(addChildNode, "xml:lang", item.getNodeValue());
                } else if (!("http://www.w3.org/1999/02/22-rdf-syntax-ns#".equals(namespaceURI) && ("ID".equals(localName) || "datatype".equals(localName)))) {
                    throw new XMPException("Invalid attribute for literal property element", 202);
                }
            }
        }
        String str = BuildConfig.VERSION_NAME;
        while (i < node.getChildNodes().getLength()) {
            item = node.getChildNodes().item(i);
            if (item.getNodeType() == (short) 3) {
                str = str + item.getNodeValue();
                i++;
            } else {
                throw new XMPException("Invalid child of literal property element", 202);
            }
        }
        addChildNode.setValue(str);
    }

    private static void rdf_NodeElement(XMPMetaImpl xMPMetaImpl, XMPNode xMPNode, Node node, boolean z) throws XMPException {
        int rDFTermKind = getRDFTermKind(node);
        if (rDFTermKind != 8 && rDFTermKind != 0) {
            throw new XMPException("Node element must be rdf:Description or typed node", 202);
        } else if (z && rDFTermKind == 0) {
            throw new XMPException("Top level typed node not allowed", 203);
        } else {
            rdf_NodeElementAttrs(xMPMetaImpl, xMPNode, node, z);
            rdf_PropertyElementList(xMPMetaImpl, xMPNode, node, z);
        }
    }

    private static void rdf_NodeElementAttrs(XMPMetaImpl xMPMetaImpl, XMPNode xMPNode, Node node, boolean z) throws XMPException {
        int i = 0;
        int i2 = 0;
        while (i < node.getAttributes().getLength()) {
            Node item = node.getAttributes().item(i);
            if (!("xmlns".equals(item.getPrefix()) || (item.getPrefix() == null && "xmlns".equals(item.getNodeName())))) {
                int rDFTermKind = getRDFTermKind(item);
                switch (rDFTermKind) {
                    case C1608R.styleable.MapAttrs_mapType /*0*/:
                        addChildNode(xMPMetaImpl, xMPNode, item, item.getNodeValue(), z);
                        break;
                    case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                    case C1608R.styleable.MapAttrs_liteMode /*6*/:
                        if (i2 <= 0) {
                            i2++;
                            if (z && rDFTermKind == 3) {
                                if (xMPNode.getName() != null && xMPNode.getName().length() > 0) {
                                    if (xMPNode.getName().equals(item.getNodeValue())) {
                                        break;
                                    }
                                    throw new XMPException("Mismatched top level rdf:about values", 203);
                                }
                                xMPNode.setName(item.getNodeValue());
                                break;
                            }
                        }
                        throw new XMPException("Mutally exclusive about, ID, nodeID attributes", 202);
                        break;
                    default:
                        throw new XMPException("Invalid nodeElement attribute", 202);
                }
            }
            i++;
        }
    }

    private static void rdf_NodeElementList(XMPMetaImpl xMPMetaImpl, XMPNode xMPNode, Node node) throws XMPException {
        for (int i = 0; i < node.getChildNodes().getLength(); i++) {
            Node item = node.getChildNodes().item(i);
            if (!isWhitespaceNode(item)) {
                rdf_NodeElement(xMPMetaImpl, xMPNode, item, true);
            }
        }
    }

    private static void rdf_ParseTypeCollectionPropertyElement() throws XMPException {
        throw new XMPException("ParseTypeCollection property element not allowed", 203);
    }

    private static void rdf_ParseTypeLiteralPropertyElement() throws XMPException {
        throw new XMPException("ParseTypeLiteral property element not allowed", 203);
    }

    private static void rdf_ParseTypeOtherPropertyElement() throws XMPException {
        throw new XMPException("ParseTypeOther property element not allowed", 203);
    }

    private static void rdf_ParseTypeResourcePropertyElement(XMPMetaImpl xMPMetaImpl, XMPNode xMPNode, Node node, boolean z) throws XMPException {
        XMPNode addChildNode = addChildNode(xMPMetaImpl, xMPNode, node, BuildConfig.VERSION_NAME, z);
        addChildNode.getOptions().setStruct(true);
        for (int i = 0; i < node.getAttributes().getLength(); i++) {
            Node item = node.getAttributes().item(i);
            if (!("xmlns".equals(item.getPrefix()) || (item.getPrefix() == null && "xmlns".equals(item.getNodeName())))) {
                String localName = item.getLocalName();
                String namespaceURI = item.getNamespaceURI();
                if ("xml:lang".equals(item.getNodeName())) {
                    addQualifierNode(addChildNode, "xml:lang", item.getNodeValue());
                } else if (!("http://www.w3.org/1999/02/22-rdf-syntax-ns#".equals(namespaceURI) && ("ID".equals(localName) || "parseType".equals(localName)))) {
                    throw new XMPException("Invalid attribute for ParseTypeResource property element", 202);
                }
            }
        }
        rdf_PropertyElementList(xMPMetaImpl, addChildNode, node, false);
        if (addChildNode.getHasValueChild()) {
            fixupQualifiedNode(addChildNode);
        }
    }

    private static void rdf_PropertyElement(XMPMetaImpl xMPMetaImpl, XMPNode xMPNode, Node node, boolean z) throws XMPException {
        int i = 0;
        if (isPropertyElementName(getRDFTermKind(node))) {
            NamedNodeMap attributes = node.getAttributes();
            List list = null;
            for (int i2 = 0; i2 < attributes.getLength(); i2++) {
                Node item = attributes.item(i2);
                if ("xmlns".equals(item.getPrefix()) || (item.getPrefix() == null && "xmlns".equals(item.getNodeName()))) {
                    if (r0 == null) {
                        list = new ArrayList();
                    }
                    list.add(item.getNodeName());
                }
            }
            if (r0 != null) {
                for (String removeNamedItem : r0) {
                    attributes.removeNamedItem(removeNamedItem);
                }
            }
            if (attributes.getLength() > 3) {
                rdf_EmptyPropertyElement(xMPMetaImpl, xMPNode, node, z);
                return;
            }
            int i3 = 0;
            while (i3 < attributes.getLength()) {
                Node item2 = attributes.item(i3);
                String localName = item2.getLocalName();
                String namespaceURI = item2.getNamespaceURI();
                String nodeValue = item2.getNodeValue();
                if ("xml:lang".equals(item2.getNodeName()) && (!"ID".equals(localName) || !"http://www.w3.org/1999/02/22-rdf-syntax-ns#".equals(namespaceURI))) {
                    i3++;
                } else if ("datatype".equals(localName) && "http://www.w3.org/1999/02/22-rdf-syntax-ns#".equals(namespaceURI)) {
                    rdf_LiteralPropertyElement(xMPMetaImpl, xMPNode, node, z);
                    return;
                } else if (!"parseType".equals(localName) || !"http://www.w3.org/1999/02/22-rdf-syntax-ns#".equals(namespaceURI)) {
                    rdf_EmptyPropertyElement(xMPMetaImpl, xMPNode, node, z);
                    return;
                } else if ("Literal".equals(nodeValue)) {
                    rdf_ParseTypeLiteralPropertyElement();
                    return;
                } else if ("Resource".equals(nodeValue)) {
                    rdf_ParseTypeResourcePropertyElement(xMPMetaImpl, xMPNode, node, z);
                    return;
                } else if ("Collection".equals(nodeValue)) {
                    rdf_ParseTypeCollectionPropertyElement();
                    return;
                } else {
                    rdf_ParseTypeOtherPropertyElement();
                    return;
                }
            }
            if (node.hasChildNodes()) {
                while (i < node.getChildNodes().getLength()) {
                    if (node.getChildNodes().item(i).getNodeType() != (short) 3) {
                        rdf_ResourcePropertyElement(xMPMetaImpl, xMPNode, node, z);
                        return;
                    }
                    i++;
                }
                rdf_LiteralPropertyElement(xMPMetaImpl, xMPNode, node, z);
                return;
            }
            rdf_EmptyPropertyElement(xMPMetaImpl, xMPNode, node, z);
            return;
        }
        throw new XMPException("Invalid property element name", 202);
    }

    private static void rdf_PropertyElementList(XMPMetaImpl xMPMetaImpl, XMPNode xMPNode, Node node, boolean z) throws XMPException {
        for (int i = 0; i < node.getChildNodes().getLength(); i++) {
            Node item = node.getChildNodes().item(i);
            if (!isWhitespaceNode(item)) {
                if (item.getNodeType() != (short) 1) {
                    throw new XMPException("Expected property element node not found", 202);
                }
                rdf_PropertyElement(xMPMetaImpl, xMPNode, item, z);
            }
        }
    }

    static void rdf_RDF(XMPMetaImpl xMPMetaImpl, Node node) throws XMPException {
        if (node.hasAttributes()) {
            rdf_NodeElementList(xMPMetaImpl, xMPMetaImpl.getRoot(), node);
            return;
        }
        throw new XMPException("Invalid attributes of rdf:RDF element", 202);
    }

    private static void rdf_ResourcePropertyElement(XMPMetaImpl xMPMetaImpl, XMPNode xMPNode, Node node, boolean z) throws XMPException {
        if (!z || !"iX:changes".equals(node.getNodeName())) {
            String namespaceURI;
            XMPNode addChildNode = addChildNode(xMPMetaImpl, xMPNode, node, BuildConfig.VERSION_NAME, z);
            for (int i = 0; i < node.getAttributes().getLength(); i++) {
                Node item = node.getAttributes().item(i);
                if (!("xmlns".equals(item.getPrefix()) || (item.getPrefix() == null && "xmlns".equals(item.getNodeName())))) {
                    String localName = item.getLocalName();
                    namespaceURI = item.getNamespaceURI();
                    if ("xml:lang".equals(item.getNodeName())) {
                        addQualifierNode(addChildNode, "xml:lang", item.getNodeValue());
                    } else if (!"ID".equals(localName) || !"http://www.w3.org/1999/02/22-rdf-syntax-ns#".equals(namespaceURI)) {
                        throw new XMPException("Invalid attribute for resource property element", 202);
                    }
                }
            }
            boolean z2 = false;
            for (int i2 = 0; i2 < node.getChildNodes().getLength(); i2++) {
                Node item2 = node.getChildNodes().item(i2);
                if (!isWhitespaceNode(item2)) {
                    if (item2.getNodeType() == (short) 1 && !z2) {
                        z2 = "http://www.w3.org/1999/02/22-rdf-syntax-ns#".equals(item2.getNamespaceURI());
                        namespaceURI = item2.getLocalName();
                        if (z2 && "Bag".equals(namespaceURI)) {
                            addChildNode.getOptions().setArray(true);
                        } else if (z2 && "Seq".equals(namespaceURI)) {
                            addChildNode.getOptions().setArray(true).setArrayOrdered(true);
                        } else if (z2 && "Alt".equals(namespaceURI)) {
                            addChildNode.getOptions().setArray(true).setArrayOrdered(true).setArrayAlternate(true);
                        } else {
                            addChildNode.getOptions().setStruct(true);
                            if (!(z2 || "Description".equals(namespaceURI))) {
                                String namespaceURI2 = item2.getNamespaceURI();
                                if (namespaceURI2 == null) {
                                    throw new XMPException("All XML elements must be in a namespace", 203);
                                }
                                addQualifierNode(addChildNode, "rdf:type", namespaceURI2 + ':' + namespaceURI);
                            }
                        }
                        rdf_NodeElement(xMPMetaImpl, addChildNode, item2, false);
                        if (addChildNode.getHasValueChild()) {
                            fixupQualifiedNode(addChildNode);
                        } else if (addChildNode.getOptions().isArrayAlternate()) {
                            XMPNodeUtils.detectAltText(addChildNode);
                        }
                        z2 = true;
                    } else if (z2) {
                        throw new XMPException("Invalid child of resource property element", 202);
                    } else {
                        throw new XMPException("Children of resource property element must be XML elements", 202);
                    }
                }
            }
            if (!z2) {
                throw new XMPException("Missing child of resource property element", 202);
            }
        }
    }
}
