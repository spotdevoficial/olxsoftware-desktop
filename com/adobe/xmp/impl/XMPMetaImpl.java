package com.adobe.xmp.impl;

import com.adobe.xmp.XMPException;
import com.adobe.xmp.XMPIterator;
import com.adobe.xmp.XMPMeta;
import com.adobe.xmp.XMPUtils;
import com.adobe.xmp.impl.xpath.XMPPathParser;
import com.adobe.xmp.options.IteratorOptions;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.C1061R;
import com.urbanairship.C1608R;
import java.util.Calendar;

public class XMPMetaImpl implements XMPMeta {
    static final /* synthetic */ boolean $assertionsDisabled;
    private String packetHeader;
    private XMPNode tree;

    static {
        $assertionsDisabled = !XMPMetaImpl.class.desiredAssertionStatus();
    }

    public XMPMetaImpl() {
        this.packetHeader = null;
        this.tree = new XMPNode(null, null, null);
    }

    public XMPMetaImpl(XMPNode xMPNode) {
        this.packetHeader = null;
        this.tree = xMPNode;
    }

    private Object evaluateNodeValue(int i, XMPNode xMPNode) throws XMPException {
        String value = xMPNode.getValue();
        switch (i) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return new Boolean(XMPUtils.convertToBoolean(value));
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return new Integer(XMPUtils.convertToInteger(value));
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return new Long(XMPUtils.convertToLong(value));
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return new Double(XMPUtils.convertToDouble(value));
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return XMPUtils.convertToDate(value);
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return XMPUtils.convertToDate(value).getCalendar();
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return XMPUtils.decodeBase64(value);
            default:
                return (value != null || xMPNode.getOptions().isCompositeProperty()) ? value : BuildConfig.VERSION_NAME;
        }
    }

    public Object clone() {
        return new XMPMetaImpl((XMPNode) this.tree.clone());
    }

    public Calendar getPropertyCalendar(String str, String str2) throws XMPException {
        return (Calendar) getPropertyObject(str, str2, 6);
    }

    protected Object getPropertyObject(String str, String str2, int i) throws XMPException {
        ParameterAsserts.assertSchemaNS(str);
        ParameterAsserts.assertPropName(str2);
        XMPNode findNode = XMPNodeUtils.findNode(this.tree, XMPPathParser.expandXPath(str, str2), false, null);
        if (findNode == null) {
            return null;
        }
        if (i == 0 || !findNode.getOptions().isCompositeProperty()) {
            return evaluateNodeValue(i, findNode);
        }
        throw new XMPException("Property must be simple when a value type is requested", C1061R.styleable.Theme_checkedTextViewStyle);
    }

    public String getPropertyString(String str, String str2) throws XMPException {
        return (String) getPropertyObject(str, str2, 0);
    }

    public XMPNode getRoot() {
        return this.tree;
    }

    public XMPIterator iterator() throws XMPException {
        return iterator(null, null, null);
    }

    public XMPIterator iterator(String str, String str2, IteratorOptions iteratorOptions) throws XMPException {
        return new XMPIteratorImpl(this, str, str2, iteratorOptions);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setLocalizedText(java.lang.String r11, java.lang.String r12, java.lang.String r13, java.lang.String r14, java.lang.String r15, com.adobe.xmp.options.PropertyOptions r16) throws com.adobe.xmp.XMPException {
        /*
        r10 = this;
        com.adobe.xmp.impl.ParameterAsserts.assertSchemaNS(r11);
        com.adobe.xmp.impl.ParameterAsserts.assertArrayName(r12);
        com.adobe.xmp.impl.ParameterAsserts.assertSpecificLang(r14);
        if (r13 == 0) goto L_0x0032;
    L_0x000b:
        r0 = com.adobe.xmp.impl.Utils.normalizeLangValue(r13);
        r1 = r0;
    L_0x0010:
        r4 = com.adobe.xmp.impl.Utils.normalizeLangValue(r14);
        r0 = com.adobe.xmp.impl.xpath.XMPPathParser.expandXPath(r11, r12);
        r2 = r10.tree;
        r3 = 1;
        r5 = new com.adobe.xmp.options.PropertyOptions;
        r6 = 7680; // 0x1e00 float:1.0762E-41 double:3.7944E-320;
        r5.<init>(r6);
        r5 = com.adobe.xmp.impl.XMPNodeUtils.findNode(r2, r0, r3, r5);
        if (r5 != 0) goto L_0x0035;
    L_0x0028:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Failed to find or create array node";
        r2 = 102; // 0x66 float:1.43E-43 double:5.04E-322;
        r0.<init>(r1, r2);
        throw r0;
    L_0x0032:
        r0 = 0;
        r1 = r0;
        goto L_0x0010;
    L_0x0035:
        r0 = r5.getOptions();
        r0 = r0.isArrayAltText();
        if (r0 != 0) goto L_0x0057;
    L_0x003f:
        r0 = r5.hasChildren();
        if (r0 != 0) goto L_0x008a;
    L_0x0045:
        r0 = r5.getOptions();
        r0 = r0.isArrayAlternate();
        if (r0 == 0) goto L_0x008a;
    L_0x004f:
        r0 = r5.getOptions();
        r2 = 1;
        r0.setArrayAltText(r2);
    L_0x0057:
        r3 = 0;
        r2 = 0;
        r6 = r5.iterateChildren();
    L_0x005d:
        r0 = r6.hasNext();
        if (r0 == 0) goto L_0x0193;
    L_0x0063:
        r0 = r6.next();
        r0 = (com.adobe.xmp.impl.XMPNode) r0;
        r7 = r0.hasQualifier();
        if (r7 == 0) goto L_0x0080;
    L_0x006f:
        r7 = "xml:lang";
        r8 = 1;
        r8 = r0.getQualifier(r8);
        r8 = r8.getName();
        r7 = r7.equals(r8);
        if (r7 != 0) goto L_0x0094;
    L_0x0080:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Language qualifier must be first";
        r2 = 102; // 0x66 float:1.43E-43 double:5.04E-322;
        r0.<init>(r1, r2);
        throw r0;
    L_0x008a:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Specified property is no alt-text array";
        r2 = 102; // 0x66 float:1.43E-43 double:5.04E-322;
        r0.<init>(r1, r2);
        throw r0;
    L_0x0094:
        r7 = "x-default";
        r8 = 1;
        r8 = r0.getQualifier(r8);
        r8 = r8.getValue();
        r7 = r7.equals(r8);
        if (r7 == 0) goto L_0x005d;
    L_0x00a5:
        r2 = 1;
        r3 = r0;
    L_0x00a7:
        if (r3 == 0) goto L_0x00b7;
    L_0x00a9:
        r0 = r5.getChildrenLength();
        r6 = 1;
        if (r0 <= r6) goto L_0x00b7;
    L_0x00b0:
        r5.removeChild(r3);
        r0 = 1;
        r5.addChild(r0, r3);
    L_0x00b7:
        r1 = com.adobe.xmp.impl.XMPNodeUtils.chooseLocalizedText(r5, r1, r4);
        r0 = 0;
        r0 = r1[r0];
        r0 = (java.lang.Integer) r0;
        r6 = r0.intValue();
        r0 = 1;
        r0 = r1[r0];
        r0 = (com.adobe.xmp.impl.XMPNode) r0;
        r1 = "x-default";
        r1 = r1.equals(r4);
        switch(r6) {
            case 0: goto L_0x00dc;
            case 1: goto L_0x00f6;
            case 2: goto L_0x0151;
            case 3: goto L_0x016e;
            case 4: goto L_0x0176;
            case 5: goto L_0x0188;
            default: goto L_0x00d2;
        };
    L_0x00d2:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Unexpected result from ChooseLocalizedText";
        r2 = 9;
        r0.<init>(r1, r2);
        throw r0;
    L_0x00dc:
        r0 = "x-default";
        com.adobe.xmp.impl.XMPNodeUtils.appendLangItem(r5, r0, r15);
        r0 = 1;
        if (r1 != 0) goto L_0x00e7;
    L_0x00e4:
        com.adobe.xmp.impl.XMPNodeUtils.appendLangItem(r5, r4, r15);
    L_0x00e7:
        if (r0 != 0) goto L_0x00f5;
    L_0x00e9:
        r0 = r5.getChildrenLength();
        r1 = 1;
        if (r0 != r1) goto L_0x00f5;
    L_0x00f0:
        r0 = "x-default";
        com.adobe.xmp.impl.XMPNodeUtils.appendLangItem(r5, r0, r15);
    L_0x00f5:
        return;
    L_0x00f6:
        if (r1 != 0) goto L_0x0114;
    L_0x00f8:
        if (r2 == 0) goto L_0x010f;
    L_0x00fa:
        if (r3 == r0) goto L_0x010f;
    L_0x00fc:
        if (r3 == 0) goto L_0x010f;
    L_0x00fe:
        r1 = r3.getValue();
        r4 = r0.getValue();
        r1 = r1.equals(r4);
        if (r1 == 0) goto L_0x010f;
    L_0x010c:
        r3.setValue(r15);
    L_0x010f:
        r0.setValue(r15);
        r0 = r2;
        goto L_0x00e7;
    L_0x0114:
        r1 = $assertionsDisabled;
        if (r1 != 0) goto L_0x0122;
    L_0x0118:
        if (r2 == 0) goto L_0x011c;
    L_0x011a:
        if (r3 == r0) goto L_0x0122;
    L_0x011c:
        r0 = new java.lang.AssertionError;
        r0.<init>();
        throw r0;
    L_0x0122:
        r4 = r5.iterateChildren();
    L_0x0126:
        r0 = r4.hasNext();
        if (r0 == 0) goto L_0x014a;
    L_0x012c:
        r0 = r4.next();
        r0 = (com.adobe.xmp.impl.XMPNode) r0;
        if (r0 == r3) goto L_0x0126;
    L_0x0134:
        r6 = r0.getValue();
        if (r3 == 0) goto L_0x0148;
    L_0x013a:
        r1 = r3.getValue();
    L_0x013e:
        r1 = r6.equals(r1);
        if (r1 == 0) goto L_0x0126;
    L_0x0144:
        r0.setValue(r15);
        goto L_0x0126;
    L_0x0148:
        r1 = 0;
        goto L_0x013e;
    L_0x014a:
        if (r3 == 0) goto L_0x0190;
    L_0x014c:
        r3.setValue(r15);
        r0 = r2;
        goto L_0x00e7;
    L_0x0151:
        if (r2 == 0) goto L_0x0168;
    L_0x0153:
        if (r3 == r0) goto L_0x0168;
    L_0x0155:
        if (r3 == 0) goto L_0x0168;
    L_0x0157:
        r1 = r3.getValue();
        r4 = r0.getValue();
        r1 = r1.equals(r4);
        if (r1 == 0) goto L_0x0168;
    L_0x0165:
        r3.setValue(r15);
    L_0x0168:
        r0.setValue(r15);
        r0 = r2;
        goto L_0x00e7;
    L_0x016e:
        com.adobe.xmp.impl.XMPNodeUtils.appendLangItem(r5, r4, r15);
        if (r1 == 0) goto L_0x0190;
    L_0x0173:
        r0 = 1;
        goto L_0x00e7;
    L_0x0176:
        if (r3 == 0) goto L_0x0182;
    L_0x0178:
        r0 = r5.getChildrenLength();
        r1 = 1;
        if (r0 != r1) goto L_0x0182;
    L_0x017f:
        r3.setValue(r15);
    L_0x0182:
        com.adobe.xmp.impl.XMPNodeUtils.appendLangItem(r5, r4, r15);
        r0 = r2;
        goto L_0x00e7;
    L_0x0188:
        com.adobe.xmp.impl.XMPNodeUtils.appendLangItem(r5, r4, r15);
        if (r1 == 0) goto L_0x0190;
    L_0x018d:
        r0 = 1;
        goto L_0x00e7;
    L_0x0190:
        r0 = r2;
        goto L_0x00e7;
    L_0x0193:
        r9 = r2;
        r2 = r3;
        r3 = r9;
        goto L_0x00a7;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adobe.xmp.impl.XMPMetaImpl.setLocalizedText(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, com.adobe.xmp.options.PropertyOptions):void");
    }

    public void setPacketHeader(String str) {
        this.packetHeader = str;
    }
}
