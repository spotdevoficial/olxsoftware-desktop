package com.adobe.xmp.impl.xpath;

import com.adobe.xmp.XMPException;
import com.adobe.xmp.XMPMetaFactory;
import com.adobe.xmp.impl.Utils;
import com.adobe.xmp.properties.XMPAliasInfo;
import com.schibsted.scm.nextgenapp.C1061R;

public final class XMPPathParser {
    public static XMPPath expandXPath(String str, String str2) throws XMPException {
        if (str == null || str2 == null) {
            throw new XMPException("Parameter must not be null", 4);
        }
        XMPPath xMPPath = new XMPPath();
        PathPosition pathPosition = new PathPosition();
        pathPosition.path = str2;
        parseRootNode(str, pathPosition, xMPPath);
        while (pathPosition.stepEnd < str2.length()) {
            pathPosition.stepBegin = pathPosition.stepEnd;
            skipPathDelimiter(str2, pathPosition);
            pathPosition.stepEnd = pathPosition.stepBegin;
            XMPPathSegment parseStructSegment = str2.charAt(pathPosition.stepBegin) != '[' ? parseStructSegment(pathPosition) : parseIndexSegment(pathPosition);
            if (parseStructSegment.getKind() == 1) {
                if (parseStructSegment.getName().charAt(0) == '@') {
                    parseStructSegment.setName("?" + parseStructSegment.getName().substring(1));
                    if (!"?xml:lang".equals(parseStructSegment.getName())) {
                        throw new XMPException("Only xml:lang allowed with '@'", C1061R.styleable.Theme_checkedTextViewStyle);
                    }
                }
                if (parseStructSegment.getName().charAt(0) == '?') {
                    pathPosition.nameStart++;
                    parseStructSegment.setKind(2);
                }
                verifyQualName(pathPosition.path.substring(pathPosition.nameStart, pathPosition.nameEnd));
            } else if (parseStructSegment.getKind() != 6) {
                continue;
            } else {
                if (parseStructSegment.getName().charAt(1) == '@') {
                    parseStructSegment.setName("[?" + parseStructSegment.getName().substring(2));
                    if (!parseStructSegment.getName().startsWith("[?xml:lang=")) {
                        throw new XMPException("Only xml:lang allowed with '@'", C1061R.styleable.Theme_checkedTextViewStyle);
                    }
                }
                if (parseStructSegment.getName().charAt(1) == '?') {
                    pathPosition.nameStart++;
                    parseStructSegment.setKind(5);
                    verifyQualName(pathPosition.path.substring(pathPosition.nameStart, pathPosition.nameEnd));
                }
            }
            xMPPath.add(parseStructSegment);
        }
        return xMPPath;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.adobe.xmp.impl.xpath.XMPPathSegment parseIndexSegment(com.adobe.xmp.impl.xpath.PathPosition r7) throws com.adobe.xmp.XMPException {
        /*
        r3 = 57;
        r2 = 48;
        r6 = 0;
        r5 = 93;
        r4 = 102; // 0x66 float:1.43E-43 double:5.04E-322;
        r0 = r7.stepEnd;
        r0 = r0 + 1;
        r7.stepEnd = r0;
        r0 = r7.path;
        r1 = r7.stepEnd;
        r0 = r0.charAt(r1);
        if (r2 > r0) goto L_0x006a;
    L_0x0019:
        r0 = r7.path;
        r1 = r7.stepEnd;
        r0 = r0.charAt(r1);
        if (r0 > r3) goto L_0x006a;
    L_0x0023:
        r0 = r7.stepEnd;
        r1 = r7.path;
        r1 = r1.length();
        if (r0 >= r1) goto L_0x0048;
    L_0x002d:
        r0 = r7.path;
        r1 = r7.stepEnd;
        r0 = r0.charAt(r1);
        if (r2 > r0) goto L_0x0048;
    L_0x0037:
        r0 = r7.path;
        r1 = r7.stepEnd;
        r0 = r0.charAt(r1);
        if (r0 > r3) goto L_0x0048;
    L_0x0041:
        r0 = r7.stepEnd;
        r0 = r0 + 1;
        r7.stepEnd = r0;
        goto L_0x0023;
    L_0x0048:
        r0 = new com.adobe.xmp.impl.xpath.XMPPathSegment;
        r1 = 3;
        r0.<init>(r6, r1);
    L_0x004e:
        r1 = r7.stepEnd;
        r2 = r7.path;
        r2 = r2.length();
        if (r1 >= r2) goto L_0x0062;
    L_0x0058:
        r1 = r7.path;
        r2 = r7.stepEnd;
        r1 = r1.charAt(r2);
        if (r1 == r5) goto L_0x0155;
    L_0x0062:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Missing ']' for array index";
        r0.<init>(r1, r4);
        throw r0;
    L_0x006a:
        r0 = r7.stepEnd;
        r1 = r7.path;
        r1 = r1.length();
        if (r0 >= r1) goto L_0x0091;
    L_0x0074:
        r0 = r7.path;
        r1 = r7.stepEnd;
        r0 = r0.charAt(r1);
        if (r0 == r5) goto L_0x0091;
    L_0x007e:
        r0 = r7.path;
        r1 = r7.stepEnd;
        r0 = r0.charAt(r1);
        r1 = 61;
        if (r0 == r1) goto L_0x0091;
    L_0x008a:
        r0 = r7.stepEnd;
        r0 = r0 + 1;
        r7.stepEnd = r0;
        goto L_0x006a;
    L_0x0091:
        r0 = r7.stepEnd;
        r1 = r7.path;
        r1 = r1.length();
        if (r0 < r1) goto L_0x00a3;
    L_0x009b:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Missing ']' or '=' for array index";
        r0.<init>(r1, r4);
        throw r0;
    L_0x00a3:
        r0 = r7.path;
        r1 = r7.stepEnd;
        r0 = r0.charAt(r1);
        if (r0 != r5) goto L_0x00ce;
    L_0x00ad:
        r0 = "[last()";
        r1 = r7.path;
        r2 = r7.stepBegin;
        r3 = r7.stepEnd;
        r1 = r1.substring(r2, r3);
        r0 = r0.equals(r1);
        if (r0 != 0) goto L_0x00c7;
    L_0x00bf:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Invalid non-numeric array index";
        r0.<init>(r1, r4);
        throw r0;
    L_0x00c7:
        r0 = new com.adobe.xmp.impl.xpath.XMPPathSegment;
        r1 = 4;
        r0.<init>(r6, r1);
        goto L_0x004e;
    L_0x00ce:
        r0 = r7.stepBegin;
        r0 = r0 + 1;
        r7.nameStart = r0;
        r0 = r7.stepEnd;
        r7.nameEnd = r0;
        r0 = r7.stepEnd;
        r0 = r0 + 1;
        r7.stepEnd = r0;
        r0 = r7.path;
        r1 = r7.stepEnd;
        r0 = r0.charAt(r1);
        r1 = 39;
        if (r0 == r1) goto L_0x00f6;
    L_0x00ea:
        r1 = 34;
        if (r0 == r1) goto L_0x00f6;
    L_0x00ee:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "Invalid quote in array selector";
        r0.<init>(r1, r4);
        throw r0;
    L_0x00f6:
        r1 = r7.stepEnd;
        r1 = r1 + 1;
        r7.stepEnd = r1;
    L_0x00fc:
        r1 = r7.stepEnd;
        r2 = r7.path;
        r2 = r2.length();
        if (r1 >= r2) goto L_0x0128;
    L_0x0106:
        r1 = r7.path;
        r2 = r7.stepEnd;
        r1 = r1.charAt(r2);
        if (r1 != r0) goto L_0x0140;
    L_0x0110:
        r1 = r7.stepEnd;
        r1 = r1 + 1;
        r2 = r7.path;
        r2 = r2.length();
        if (r1 >= r2) goto L_0x0128;
    L_0x011c:
        r1 = r7.path;
        r2 = r7.stepEnd;
        r2 = r2 + 1;
        r1 = r1.charAt(r2);
        if (r1 == r0) goto L_0x013a;
    L_0x0128:
        r0 = r7.stepEnd;
        r1 = r7.path;
        r1 = r1.length();
        if (r0 < r1) goto L_0x0147;
    L_0x0132:
        r0 = new com.adobe.xmp.XMPException;
        r1 = "No terminating quote for array selector";
        r0.<init>(r1, r4);
        throw r0;
    L_0x013a:
        r1 = r7.stepEnd;
        r1 = r1 + 1;
        r7.stepEnd = r1;
    L_0x0140:
        r1 = r7.stepEnd;
        r1 = r1 + 1;
        r7.stepEnd = r1;
        goto L_0x00fc;
    L_0x0147:
        r0 = r7.stepEnd;
        r0 = r0 + 1;
        r7.stepEnd = r0;
        r0 = new com.adobe.xmp.impl.xpath.XMPPathSegment;
        r1 = 6;
        r0.<init>(r6, r1);
        goto L_0x004e;
    L_0x0155:
        r1 = r7.stepEnd;
        r1 = r1 + 1;
        r7.stepEnd = r1;
        r1 = r7.path;
        r2 = r7.stepBegin;
        r3 = r7.stepEnd;
        r1 = r1.substring(r2, r3);
        r0.setName(r1);
        return r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adobe.xmp.impl.xpath.XMPPathParser.parseIndexSegment(com.adobe.xmp.impl.xpath.PathPosition):com.adobe.xmp.impl.xpath.XMPPathSegment");
    }

    private static void parseRootNode(String str, PathPosition pathPosition, XMPPath xMPPath) throws XMPException {
        while (pathPosition.stepEnd < pathPosition.path.length() && "/[*".indexOf(pathPosition.path.charAt(pathPosition.stepEnd)) < 0) {
            pathPosition.stepEnd++;
        }
        if (pathPosition.stepEnd == pathPosition.stepBegin) {
            throw new XMPException("Empty initial XMPPath step", C1061R.styleable.Theme_checkedTextViewStyle);
        }
        String verifyXPathRoot = verifyXPathRoot(str, pathPosition.path.substring(pathPosition.stepBegin, pathPosition.stepEnd));
        XMPAliasInfo findAlias = XMPMetaFactory.getSchemaRegistry().findAlias(verifyXPathRoot);
        if (findAlias == null) {
            xMPPath.add(new XMPPathSegment(str, Integer.MIN_VALUE));
            xMPPath.add(new XMPPathSegment(verifyXPathRoot, 1));
            return;
        }
        xMPPath.add(new XMPPathSegment(findAlias.getNamespace(), Integer.MIN_VALUE));
        XMPPathSegment xMPPathSegment = new XMPPathSegment(verifyXPathRoot(findAlias.getNamespace(), findAlias.getPropName()), 1);
        xMPPathSegment.setAlias(true);
        xMPPathSegment.setAliasForm(findAlias.getAliasForm().getOptions());
        xMPPath.add(xMPPathSegment);
        if (findAlias.getAliasForm().isArrayAltText()) {
            xMPPathSegment = new XMPPathSegment("[?xml:lang='x-default']", 5);
            xMPPathSegment.setAlias(true);
            xMPPathSegment.setAliasForm(findAlias.getAliasForm().getOptions());
            xMPPath.add(xMPPathSegment);
        } else if (findAlias.getAliasForm().isArray()) {
            xMPPathSegment = new XMPPathSegment("[1]", 3);
            xMPPathSegment.setAlias(true);
            xMPPathSegment.setAliasForm(findAlias.getAliasForm().getOptions());
            xMPPath.add(xMPPathSegment);
        }
    }

    private static XMPPathSegment parseStructSegment(PathPosition pathPosition) throws XMPException {
        pathPosition.nameStart = pathPosition.stepBegin;
        while (pathPosition.stepEnd < pathPosition.path.length() && "/[*".indexOf(pathPosition.path.charAt(pathPosition.stepEnd)) < 0) {
            pathPosition.stepEnd++;
        }
        pathPosition.nameEnd = pathPosition.stepEnd;
        if (pathPosition.stepEnd != pathPosition.stepBegin) {
            return new XMPPathSegment(pathPosition.path.substring(pathPosition.stepBegin, pathPosition.stepEnd), 1);
        }
        throw new XMPException("Empty XMPPath segment", C1061R.styleable.Theme_checkedTextViewStyle);
    }

    private static void skipPathDelimiter(String str, PathPosition pathPosition) throws XMPException {
        if (str.charAt(pathPosition.stepBegin) == '/') {
            pathPosition.stepBegin++;
            if (pathPosition.stepBegin >= str.length()) {
                throw new XMPException("Empty XMPPath segment", C1061R.styleable.Theme_checkedTextViewStyle);
            }
        }
        if (str.charAt(pathPosition.stepBegin) == '*') {
            pathPosition.stepBegin++;
            if (pathPosition.stepBegin >= str.length() || str.charAt(pathPosition.stepBegin) != '[') {
                throw new XMPException("Missing '[' after '*'", C1061R.styleable.Theme_checkedTextViewStyle);
            }
        }
    }

    private static void verifyQualName(String str) throws XMPException {
        int indexOf = str.indexOf(58);
        if (indexOf > 0) {
            String substring = str.substring(0, indexOf);
            if (Utils.isXMLNameNS(substring)) {
                if (XMPMetaFactory.getSchemaRegistry().getNamespaceURI(substring) == null) {
                    throw new XMPException("Unknown namespace prefix for qualified name", C1061R.styleable.Theme_checkedTextViewStyle);
                }
                return;
            }
        }
        throw new XMPException("Ill-formed qualified name", C1061R.styleable.Theme_checkedTextViewStyle);
    }

    private static void verifySimpleXMLName(String str) throws XMPException {
        if (!Utils.isXMLName(str)) {
            throw new XMPException("Bad XML name", C1061R.styleable.Theme_checkedTextViewStyle);
        }
    }

    private static String verifyXPathRoot(String str, String str2) throws XMPException {
        if (str == null || str.length() == 0) {
            throw new XMPException("Schema namespace URI is required", C1061R.styleable.Theme_checkboxStyle);
        } else if (str2.charAt(0) == '?' || str2.charAt(0) == '@') {
            throw new XMPException("Top level name must not be a qualifier", C1061R.styleable.Theme_checkedTextViewStyle);
        } else if (str2.indexOf(47) >= 0 || str2.indexOf(91) >= 0) {
            throw new XMPException("Top level name must be simple", C1061R.styleable.Theme_checkedTextViewStyle);
        } else {
            String namespacePrefix = XMPMetaFactory.getSchemaRegistry().getNamespacePrefix(str);
            if (namespacePrefix == null) {
                throw new XMPException("Unregistered schema namespace URI", C1061R.styleable.Theme_checkboxStyle);
            }
            int indexOf = str2.indexOf(58);
            if (indexOf < 0) {
                verifySimpleXMLName(str2);
                return namespacePrefix + str2;
            }
            verifySimpleXMLName(str2.substring(0, indexOf));
            verifySimpleXMLName(str2.substring(indexOf));
            namespacePrefix = str2.substring(0, indexOf + 1);
            String namespacePrefix2 = XMPMetaFactory.getSchemaRegistry().getNamespacePrefix(str);
            if (namespacePrefix2 == null) {
                throw new XMPException("Unknown schema namespace prefix", C1061R.styleable.Theme_checkboxStyle);
            } else if (namespacePrefix.equals(namespacePrefix2)) {
                return str2;
            } else {
                throw new XMPException("Schema namespace URI and prefix mismatch", C1061R.styleable.Theme_checkboxStyle);
            }
        }
    }
}
