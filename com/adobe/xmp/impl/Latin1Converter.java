package com.adobe.xmp.impl;

import com.urbanairship.C1608R;
import java.io.UnsupportedEncodingException;

public class Latin1Converter {
    public static ByteBuffer convert(ByteBuffer byteBuffer) {
        int i = 0;
        if (!"UTF-8".equals(byteBuffer.getEncoding())) {
            return byteBuffer;
        }
        byte[] bArr = new byte[8];
        ByteBuffer byteBuffer2 = new ByteBuffer((byteBuffer.length() * 4) / 3);
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i2 < byteBuffer.length()) {
            int charAt = byteBuffer.charAt(i2);
            switch (i3) {
                case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                    if (i4 > 0 && (charAt & 192) == 128) {
                        int i6 = i5 + 1;
                        bArr[i5] = (byte) charAt;
                        i4--;
                        if (i4 != 0) {
                            i5 = i6;
                            break;
                        }
                        byteBuffer2.append(bArr, 0, i6);
                        i3 = 0;
                        i5 = 0;
                        break;
                    }
                    byteBuffer2.append(convertToUTF8(bArr[0]));
                    i2 -= i5;
                    i3 = 0;
                    i5 = 0;
                    break;
                    break;
                default:
                    if (charAt >= 127) {
                        if (charAt < 192) {
                            byteBuffer2.append(convertToUTF8((byte) charAt));
                            break;
                        }
                        i4 = -1;
                        i3 = charAt;
                        while (i4 < 8 && (i3 & 128) == 128) {
                            i4++;
                            i3 <<= 1;
                        }
                        i3 = i5 + 1;
                        bArr[i5] = (byte) charAt;
                        i5 = i3;
                        i3 = 11;
                        break;
                    }
                    byteBuffer2.append((byte) charAt);
                    break;
                    break;
            }
            i2++;
        }
        if (i3 == 11) {
            while (i < i5) {
                byteBuffer2.append(convertToUTF8(bArr[i]));
                i++;
            }
        }
        return byteBuffer2;
    }

    private static byte[] convertToUTF8(byte b) {
        int i = b & 255;
        if (i >= 128) {
            if (i == 129 || i == 141 || i == 143 || i == 144 || i == 157) {
                try {
                    return new byte[]{(byte) 32};
                } catch (UnsupportedEncodingException e) {
                }
            } else {
                return new String(new byte[]{b}, "cp1252").getBytes("UTF-8");
            }
        }
        return new byte[]{b};
    }
}
