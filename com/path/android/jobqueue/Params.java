package com.path.android.jobqueue;

public class Params {
    private long delayMs;
    private String groupId;
    private boolean persistent;
    private int priority;
    private boolean requiresNetwork;

    public Params(int priority) {
        this.requiresNetwork = false;
        this.groupId = null;
        this.persistent = false;
        this.priority = priority;
    }

    public boolean doesRequireNetwork() {
        return this.requiresNetwork;
    }

    public String getGroupId() {
        return this.groupId;
    }

    public boolean isPersistent() {
        return this.persistent;
    }

    public int getPriority() {
        return this.priority;
    }

    public long getDelayMs() {
        return this.delayMs;
    }
}
