package com.path.android.jobqueue.log;

public interface CustomLogger {
    void m1722d(String str, Object... objArr);

    void m1723e(String str, Object... objArr);

    void m1724e(Throwable th, String str, Object... objArr);

    boolean isDebugEnabled();
}
