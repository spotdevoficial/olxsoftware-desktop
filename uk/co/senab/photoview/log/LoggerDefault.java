package uk.co.senab.photoview.log;

import android.util.Log;

public class LoggerDefault implements Logger {
    public int m1754d(String tag, String msg) {
        return Log.d(tag, msg);
    }

    public int m1755i(String tag, String msg) {
        return Log.i(tag, msg);
    }
}
