package de.keyboardsurfer.android.widget.crouton;

public class Configuration {
    public static final Configuration DEFAULT;
    final int durationInMilliseconds;
    final int inAnimationResId;
    final int outAnimationResId;

    public static class Builder {
        private int durationInMilliseconds;
        private int inAnimationResId;
        private int outAnimationResId;

        public Builder() {
            this.durationInMilliseconds = 3000;
            this.inAnimationResId = 0;
            this.outAnimationResId = 0;
        }

        public Builder setDuration(int duration) {
            this.durationInMilliseconds = duration;
            return this;
        }

        public Configuration build() {
            return new Configuration();
        }
    }

    static {
        DEFAULT = new Builder().setDuration(3000).build();
    }

    private Configuration(Builder builder) {
        this.durationInMilliseconds = builder.durationInMilliseconds;
        this.inAnimationResId = builder.inAnimationResId;
        this.outAnimationResId = builder.outAnimationResId;
    }

    public String toString() {
        return "Configuration{durationInMilliseconds=" + this.durationInMilliseconds + ", inAnimationResId=" + this.inAnimationResId + ", outAnimationResId=" + this.outAnimationResId + '}';
    }
}
