package de.keyboardsurfer.android.widget.crouton;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.FrameLayout;
import com.facebook.share.internal.ShareConstants;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

final class Manager extends Handler {
    private static Manager INSTANCE;
    private final Queue<Crouton> croutonQueue;

    /* renamed from: de.keyboardsurfer.android.widget.crouton.Manager.1 */
    class C16901 implements OnGlobalLayoutListener {
        final /* synthetic */ Crouton val$crouton;
        final /* synthetic */ View val$croutonView;

        C16901(View view, Crouton crouton) {
            this.val$croutonView = view;
            this.val$crouton = crouton;
        }

        @TargetApi(16)
        public void onGlobalLayout() {
            if (VERSION.SDK_INT < 16) {
                this.val$croutonView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            } else {
                this.val$croutonView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
            this.val$croutonView.startAnimation(this.val$crouton.getInAnimation());
            Manager.announceForAccessibilityCompat(this.val$crouton.getActivity(), this.val$crouton.getText());
            if (-1 != this.val$crouton.getConfiguration().durationInMilliseconds) {
                Manager.this.sendMessageDelayed(this.val$crouton, -1040155167, ((long) this.val$crouton.getConfiguration().durationInMilliseconds) + this.val$crouton.getInAnimation().getDuration());
            }
        }
    }

    private Manager() {
        this.croutonQueue = new LinkedBlockingQueue();
    }

    static synchronized Manager getInstance() {
        Manager manager;
        synchronized (Manager.class) {
            if (INSTANCE == null) {
                INSTANCE = new Manager();
            }
            manager = INSTANCE;
        }
        return manager;
    }

    void add(Crouton crouton) {
        this.croutonQueue.add(crouton);
        displayCrouton();
    }

    private void displayCrouton() {
        if (!this.croutonQueue.isEmpty()) {
            Crouton currentCrouton = (Crouton) this.croutonQueue.peek();
            if (currentCrouton.getActivity() == null) {
                this.croutonQueue.poll();
            }
            if (currentCrouton.isShowing()) {
                sendMessageDelayed(currentCrouton, 794631, calculateCroutonDuration(currentCrouton));
                return;
            }
            sendMessage(currentCrouton, -1040157475);
            if (currentCrouton.getLifecycleCallback() != null) {
                currentCrouton.getLifecycleCallback().onDisplayed();
            }
        }
    }

    private long calculateCroutonDuration(Crouton crouton) {
        return (((long) crouton.getConfiguration().durationInMilliseconds) + crouton.getInAnimation().getDuration()) + crouton.getOutAnimation().getDuration();
    }

    private void sendMessage(Crouton crouton, int messageId) {
        Message message = obtainMessage(messageId);
        message.obj = crouton;
        sendMessage(message);
    }

    private void sendMessageDelayed(Crouton crouton, int messageId, long delay) {
        Message message = obtainMessage(messageId);
        message.obj = crouton;
        sendMessageDelayed(message, delay);
    }

    public void handleMessage(Message message) {
        Crouton crouton = message.obj;
        if (crouton != null) {
            switch (message.what) {
                case -1040157475:
                    addCroutonToView(crouton);
                case -1040155167:
                    removeCrouton(crouton);
                    if (crouton.getLifecycleCallback() != null) {
                        crouton.getLifecycleCallback().onRemoved();
                    }
                case 794631:
                    displayCrouton();
                default:
                    super.handleMessage(message);
            }
        }
    }

    private void addCroutonToView(Crouton crouton) {
        if (!crouton.isShowing()) {
            View croutonView = crouton.getView();
            if (croutonView.getParent() == null) {
                LayoutParams params = croutonView.getLayoutParams();
                if (params == null) {
                    params = new MarginLayoutParams(-1, -2);
                }
                if (crouton.getViewGroup() == null) {
                    Activity activity = crouton.getActivity();
                    if (activity != null && !activity.isFinishing()) {
                        handleTranslucentActionBar((MarginLayoutParams) params, activity);
                        activity.addContentView(croutonView, params);
                    } else {
                        return;
                    }
                } else if (crouton.getViewGroup() instanceof FrameLayout) {
                    crouton.getViewGroup().addView(croutonView, params);
                } else {
                    crouton.getViewGroup().addView(croutonView, 0, params);
                }
            }
            croutonView.requestLayout();
            ViewTreeObserver observer = croutonView.getViewTreeObserver();
            if (observer != null) {
                observer.addOnGlobalLayoutListener(new C16901(croutonView, crouton));
            }
        }
    }

    @TargetApi(19)
    private void handleTranslucentActionBar(MarginLayoutParams params, Activity activity) {
        if (VERSION.SDK_INT >= 19 && (activity.getWindow().getAttributes().flags & 67108864) == 67108864) {
            View actionBarContainer = activity.findViewById(Resources.getSystem().getIdentifier("action_bar_container", ShareConstants.WEB_DIALOG_PARAM_ID, "android"));
            if (actionBarContainer != null) {
                params.topMargin = actionBarContainer.getBottom();
            }
        }
    }

    protected void removeCrouton(Crouton crouton) {
        View croutonView = crouton.getView();
        ViewGroup croutonParentView = (ViewGroup) croutonView.getParent();
        if (croutonParentView != null) {
            croutonView.startAnimation(crouton.getOutAnimation());
            Crouton removed = (Crouton) this.croutonQueue.poll();
            croutonParentView.removeView(croutonView);
            if (removed != null) {
                removed.detachActivity();
                removed.detachViewGroup();
                if (removed.getLifecycleCallback() != null) {
                    removed.getLifecycleCallback().onRemoved();
                }
                removed.detachLifecycleCallback();
            }
            sendMessageDelayed(crouton, 794631, crouton.getOutAnimation().getDuration());
        }
    }

    void clearCroutonsForActivity(Activity activity) {
        Iterator<Crouton> croutonIterator = this.croutonQueue.iterator();
        while (croutonIterator.hasNext()) {
            Crouton crouton = (Crouton) croutonIterator.next();
            if (crouton.getActivity() != null && crouton.getActivity().equals(activity)) {
                removeCroutonFromViewParent(crouton);
                removeAllMessagesForCrouton(crouton);
                croutonIterator.remove();
            }
        }
    }

    private void removeCroutonFromViewParent(Crouton crouton) {
        if (crouton.isShowing()) {
            ViewGroup parent = (ViewGroup) crouton.getView().getParent();
            if (parent != null) {
                parent.removeView(crouton.getView());
            }
        }
    }

    private void removeAllMessagesForCrouton(Crouton crouton) {
        removeMessages(-1040157475, crouton);
        removeMessages(794631, crouton);
        removeMessages(-1040155167, crouton);
    }

    public static void announceForAccessibilityCompat(Context context, CharSequence text) {
        if (VERSION.SDK_INT >= 4) {
            AccessibilityManager accessibilityManager = null;
            if (context != null) {
                accessibilityManager = (AccessibilityManager) context.getSystemService("accessibility");
            }
            if (accessibilityManager != null && accessibilityManager.isEnabled()) {
                int eventType;
                if (VERSION.SDK_INT < 16) {
                    eventType = 8;
                } else {
                    eventType = 16384;
                }
                AccessibilityEvent event = AccessibilityEvent.obtain(eventType);
                event.getText().add(text);
                event.setClassName(Manager.class.getName());
                event.setPackageName(context.getPackageName());
                accessibilityManager.sendAccessibilityEvent(event);
            }
        }
    }

    public String toString() {
        return "Manager{croutonQueue=" + this.croutonQueue + '}';
    }
}
