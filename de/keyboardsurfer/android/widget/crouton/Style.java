package de.keyboardsurfer.android.widget.crouton;

import android.graphics.drawable.Drawable;
import android.widget.ImageView.ScaleType;

public class Style {
    public static final Style ALERT;
    public static final Style CONFIRM;
    public static final Style INFO;
    final int backgroundColorResourceId;
    final int backgroundColorValue;
    final int backgroundDrawableResourceId;
    final Configuration configuration;
    final int gravity;
    final int heightDimensionResId;
    final int heightInPixels;
    final Drawable imageDrawable;
    final int imageResId;
    final ScaleType imageScaleType;
    final boolean isTileEnabled;
    final int paddingDimensionResId;
    final int paddingInPixels;
    final int textAppearanceResId;
    final int textColorResourceId;
    final int textColorValue;
    final int textShadowColorResId;
    final float textShadowDx;
    final float textShadowDy;
    final float textShadowRadius;
    final int textSize;
    final int widthDimensionResId;
    final int widthInPixels;

    public static class Builder {
        private int backgroundColorResourceId;
        private int backgroundColorValue;
        private int backgroundDrawableResourceId;
        private Configuration configuration;
        private int gravity;
        private int heightDimensionResId;
        private int heightInPixels;
        private Drawable imageDrawable;
        private int imageResId;
        private ScaleType imageScaleType;
        private boolean isTileEnabled;
        private int paddingDimensionResId;
        private int paddingInPixels;
        private int textAppearanceResId;
        private int textColorResourceId;
        private int textColorValue;
        private int textShadowColorResId;
        private float textShadowDx;
        private float textShadowDy;
        private float textShadowRadius;
        private int textSize;
        private int widthDimensionResId;
        private int widthInPixels;

        public Builder() {
            this.configuration = Configuration.DEFAULT;
            this.paddingInPixels = 10;
            this.backgroundColorResourceId = 17170450;
            this.backgroundDrawableResourceId = 0;
            this.backgroundColorValue = -1;
            this.isTileEnabled = false;
            this.textColorResourceId = 17170443;
            this.textColorValue = -1;
            this.heightInPixels = -2;
            this.widthInPixels = -1;
            this.gravity = 17;
            this.imageDrawable = null;
            this.imageResId = 0;
            this.imageScaleType = ScaleType.FIT_XY;
        }

        public Builder(Style baseStyle) {
            this.configuration = baseStyle.configuration;
            this.backgroundColorValue = baseStyle.backgroundColorValue;
            this.backgroundColorResourceId = baseStyle.backgroundColorResourceId;
            this.backgroundDrawableResourceId = baseStyle.backgroundDrawableResourceId;
            this.isTileEnabled = baseStyle.isTileEnabled;
            this.textColorResourceId = baseStyle.textColorResourceId;
            this.textColorValue = baseStyle.textColorValue;
            this.heightInPixels = baseStyle.heightInPixels;
            this.heightDimensionResId = baseStyle.heightDimensionResId;
            this.widthInPixels = baseStyle.widthInPixels;
            this.widthDimensionResId = baseStyle.widthDimensionResId;
            this.gravity = baseStyle.gravity;
            this.imageDrawable = baseStyle.imageDrawable;
            this.textSize = baseStyle.textSize;
            this.textShadowColorResId = baseStyle.textShadowColorResId;
            this.textShadowRadius = baseStyle.textShadowRadius;
            this.textShadowDx = baseStyle.textShadowDx;
            this.textShadowDy = baseStyle.textShadowDy;
            this.textAppearanceResId = baseStyle.textAppearanceResId;
            this.imageResId = baseStyle.imageResId;
            this.imageScaleType = baseStyle.imageScaleType;
            this.paddingInPixels = baseStyle.paddingInPixels;
            this.paddingDimensionResId = baseStyle.paddingDimensionResId;
        }

        public Builder setBackgroundColor(int backgroundColorResourceId) {
            this.backgroundColorResourceId = backgroundColorResourceId;
            return this;
        }

        public Builder setBackgroundColorValue(int backgroundColorValue) {
            this.backgroundColorValue = backgroundColorValue;
            return this;
        }

        public Style build() {
            return new Style();
        }
    }

    static {
        ALERT = new Builder().setBackgroundColorValue(-48060).build();
        CONFIRM = new Builder().setBackgroundColorValue(-6697984).build();
        INFO = new Builder().setBackgroundColorValue(-13388315).build();
    }

    private Style(Builder builder) {
        this.configuration = builder.configuration;
        this.backgroundColorResourceId = builder.backgroundColorResourceId;
        this.backgroundDrawableResourceId = builder.backgroundDrawableResourceId;
        this.isTileEnabled = builder.isTileEnabled;
        this.textColorResourceId = builder.textColorResourceId;
        this.textColorValue = builder.textColorValue;
        this.heightInPixels = builder.heightInPixels;
        this.heightDimensionResId = builder.heightDimensionResId;
        this.widthInPixels = builder.widthInPixels;
        this.widthDimensionResId = builder.widthDimensionResId;
        this.gravity = builder.gravity;
        this.imageDrawable = builder.imageDrawable;
        this.textSize = builder.textSize;
        this.textShadowColorResId = builder.textShadowColorResId;
        this.textShadowRadius = builder.textShadowRadius;
        this.textShadowDx = builder.textShadowDx;
        this.textShadowDy = builder.textShadowDy;
        this.textAppearanceResId = builder.textAppearanceResId;
        this.imageResId = builder.imageResId;
        this.imageScaleType = builder.imageScaleType;
        this.paddingInPixels = builder.paddingInPixels;
        this.paddingDimensionResId = builder.paddingDimensionResId;
        this.backgroundColorValue = builder.backgroundColorValue;
    }

    public String toString() {
        return "Style{configuration=" + this.configuration + ", backgroundColorResourceId=" + this.backgroundColorResourceId + ", backgroundDrawableResourceId=" + this.backgroundDrawableResourceId + ", backgroundColorValue=" + this.backgroundColorValue + ", isTileEnabled=" + this.isTileEnabled + ", textColorResourceId=" + this.textColorResourceId + ", textColorValue=" + this.textColorValue + ", heightInPixels=" + this.heightInPixels + ", heightDimensionResId=" + this.heightDimensionResId + ", widthInPixels=" + this.widthInPixels + ", widthDimensionResId=" + this.widthDimensionResId + ", gravity=" + this.gravity + ", imageDrawable=" + this.imageDrawable + ", imageResId=" + this.imageResId + ", imageScaleType=" + this.imageScaleType + ", textSize=" + this.textSize + ", textShadowColorResId=" + this.textShadowColorResId + ", textShadowRadius=" + this.textShadowRadius + ", textShadowDy=" + this.textShadowDy + ", textShadowDx=" + this.textShadowDx + ", textAppearanceResId=" + this.textAppearanceResId + ", paddingInPixels=" + this.paddingInPixels + ", paddingDimensionResId=" + this.paddingDimensionResId + '}';
    }
}
